<?php   
class ControllerCronEusolarVtigerCsomag extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;

        $this->load->model("cron/altalanos");
        $this->load->model('cron/eusolar_vtiger_csomag');
        $this->load->model('cron/eusolar_vtiger_products');
        if ($cron_id) {
            $input = $this->model_cron_altalanos->getInputElements($cron_id);
        }
        echo 'Inditás<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $products = $this->model_cron_eusolar_vtiger_csomag->getChangeCsomag();

        $this->model_cron_altalanos->addFuttatasEredmeny('Termékek összesen: '.count($products).' db');

        echo '<br><br>Termékek összesen: '.count($products).' db';



        if ($products) {
            foreach($products as $key=>$value) {
                if ($value['esemeny'] == 'felvitel') {

                    $tetelek = $this->model_cron_eusolar_vtiger_csomag->getCsomagTetelek($value['pricebookid']);
                    if ($tetelek) {
                        $vissza = $this->model_cron_eusolar_vtiger_products->WriteProducts($value['productid']);
                        $vissza = $this->model_cron_eusolar_vtiger_products->WriteProducts($value['pricebookid'],false);
                        foreach($tetelek as $tetel) {
                            $vissza = $this->model_cron_eusolar_vtiger_csomag->addCsomagTetel($value['pricebookid'],$tetel['productid'],$tetel['price']);
                        }
                        if (!empty($input['csomagarak_aktualizalasa'])) {
                            $vissza = $this->model_cron_eusolar_vtiger_csomag->csomagarAktualizalasa($value['pricebookid']);
                        }

                    }


                    $vissza = $this->model_cron_eusolar_vtiger_csomag->addCsomagTetel($value['pricebookid'],$value['productid'],$value['price']);

                } elseif ($value['esemeny'] == 'torles') {
                    $vissza = $this->model_cron_eusolar_vtiger_csomag->deleteCsomagTetel($value['pricebookid'],$value['productid']);
                    if (!empty($input['csomagarak_aktualizalasa'])) {
                        $vissza = $this->model_cron_eusolar_vtiger_csomag->csomagarAktualizalasa($value['pricebookid']);
                    }
                }
                $this->model_cron_eusolar_vtiger_csomag->deleteTrigger($value['pricebookid'], $value['productid']);

            }
        }

        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}
?>