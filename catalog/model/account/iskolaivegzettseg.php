<?php

class ModelAccountIskolaivegzettseg extends Model {
    public function getAll() {
        $iskolai_vegzettsegek = $this->config->get('vegzettseg');

        if ($iskolai_vegzettsegek) {
            foreach($iskolai_vegzettsegek as $key=>$value){
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $iskolai_vegzettsegek);

            return $iskolai_vegzettsegek;
        } else {
            return false;
        }
    }
}

?>