<?php

// Heading
$_['heading_title']         = 'Quotes';
$_['heading_modosit']       = 'Quotes';
$_['heading_arajanlat']     = 'Quotes';
$_['heading_azonosito']     = 'ID:';
$_['heading_neve']          = 'Name:';

$_['column_azonosito']      = 'ID';
$_['column_customer']       = 'Customer';
$_['column_name']           = 'Quote name';
$_['column_action']         = 'Operation';
$_['column_date_added']     = 'Create';
$_['column_date_modified']  = 'Last modified';
$_['column_arajanlat_keszito_adatai']  = 'Quoting header data';
$_['column_arajanlat_kero_adatai']  = 'Contracting data';

$_['text_account']          = 'Account';
$_['text_page']             = 'Page';
$_['text_delete']           = 'The deletion can not be undone! Are you sure you want this?';


$_['column_customer_email']     = 'E-mail';

$_['column_megyseg'] 	        = 'Amount Units';
$_['column_listaar'] 	        = 'List price';
$_['column_engedmeny_szazalek'] = 'Disc.%';
$_['column_engedmeny_ertek'] 	= 'Disc. unit price';
$_['column_ertek'] 	= 'Value';


$_['column_kep']            = 'Image';
$_['column_nev']            = 'Name';
$_['column_model']          = 'Modell';
$_['column_mennyiseg']      = 'Amount';
$_['column_egysegar']       = 'Unit price';
$_['column_ar_osszesen']    = 'Subtotal';
$_['column_total_osszesen'] = 'Total';

$_['button_filter']         = 'Filter';
$_['button_copy']           = 'Copy';
$_['button_save']           = 'Save';
$_['button_cancel']         = 'Cancel';
$_['button_szerkeszt']      = 'Edit';
$_['button_edit']           = 'Change';
$_['button_pdf']            = 'PDF download';




$_['fejlec_szekhely']           = 'Headquarters';
$_['fejlec_adoszam']            = 'Tax code:';
$_['fejlec_bank']               = 'Bank:';
$_['fejlec_bank_szamla']        = 'Bank account:';
$_['fejlec_swift_kod']          = 'Bank SWIFT code:';
$_['fejlec_iban_kod']           = 'Bank IBAN code:';

$_['fejlec_ajanlat_szam']       = 'Tender name:';
$_['fejlec_ajanlat_kelt']       = 'Tender date:';
$_['fejlec_szallitasi_hatarido']= 'Delivery time:';
$_['fejlec_fizetesi_hatarido']  = 'Payment deadline:';
$_['fejlec_fizetesi_mod']       = 'Payment methods';
$_['fejlec_szallito']           = 'Supplier';
$_['fejlec_vevo']               = 'Customer';

$_['fejlec_kuldo']              = 'Sender:';
$_['fejlec_telefon']            = 'Telefon number:';
$_['fejlec_email']              = 'Email:';





$_['error_uritsd_elobb']    = 'Quotation is in progress. Before editing, please, finish it! ';

// Text
$_['text_no_results']       = 'You have no quotation requests!';
$_['text_success']          = 'Success! The quotation is loaded! You can change the <span style = "cursor: pointer; text-decoration: underline" onclick = "ajanlatKeresMegtekintes ()"> ajanlatkeres </span> menu.';
$_['text_success_update']   = 'Success! The offer has been changed!';
$_['text_success_copy']   = 'Success! Copied the offer!';
$_['text_success_delete']   = 'Success! You\'ve deleted the selected product(s)!';

?>
