<?php

// Heading
$_['heading_title']         = 'Pre-orders';

// Text
$_['text_success']          = 'Success! Modified the pre-orders!';


// Column*/
$_['column_pre_order_id']     = 'Identification';
$_['column_customer']         = 'Customer';
$_['column_email']            = 'E-Mail';
$_['column_date_added']       = 'Pre-order date';
$_['column_date_modified']    = 'Last Updated on';
$_['column_product_name']     = 'Product name';
$_['column_model']            = 'Model';
$_['column_action']           = 'Operations';


$_['entry_pre_order_id']       = 'Pre-order ID:';
$_['entry_product_id']         = 'Product ID:';
$_['entry_model']              = 'Model:';
$_['entry_cikkszam']           = 'Item No.:';
$_['entry_product_name']       = 'Product name:';
$_['entry_gyarto']             = 'Manufacturer:';
$_['entry_megrendelo_name']    = 'Pre-Order Pre title:';
$_['entry_megrendelo_email']   = 'Pre-Order Pre email:';
$_['entry_megrendelo_uzenet']  = 'Pre-Order Pre message:';
$_['entry_date_added']         = 'Pre-order catalog:';
$_['entry_date_modified']      = 'Last Modified Date';
$_['entry_sajat_megjegyzes']   = 'Shop comments:';
$_['entry_telefon']            = 'telephone number:';

$_['error_permission']      = 'Warning: do not have permission to change the pre-order!';


?>
