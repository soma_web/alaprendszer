<?php
// Text
$_['text_subject']          = '%s - Kérdését megválaszolták';
$_['text_answered']         = 'Önnek válasza érkezett kérdésére, melyet a következő termékkel kapcsolatban tett fel: <a href="%s" style="color: #069; font-weight: bold;">%s</a>.';
$_['text_view']             = 'Megtekintheti a választ, ha ellátogat a termék oldalára: <a href="%s" style="color: #069; font-weight: bold;">%s</a>.';
$_['text_question_detail']  = 'Kérdés részletei';
$_['text_asked']            = '%s keltű kérdése:';
$_['text_answer']           = 'A válasz:';
//$_['text_powered_by']       = 'powered by';
$_['text_closing']          = 'Üdvözlettel,';
$_['text_sender']           = '%s';
?>
