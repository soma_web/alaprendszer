<?php
// Heading 
$_['modal_title'] = 'Offer Reminder';

// Text
$_['text_info']  = 'If you want to be notified before this special offer expire please fill form below.'; 
$_['text_set_alert']  = 'Set reminder'; 
$_['text_success']    = 'Reminder has been saved.'; 
$_['text_expire_date'] = 'Hurry up! This offer expire on <strong>%s</strong>'; 

// Entry
$_['entry_name'] = 'Name:';
$_['entry_email'] = 'Email:';
$_['entry_days_before'] = 'Remind me: (number of days before offer expire):';

// Error
$_['error_name']           = 'Name must be at least 3 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_days_before']    = 'How many days before offer expire you want to be notified?';
$_['error_only_days_remain'] = 'There are only <strong>%s days left</strong>.';
$_['error_this_is_day']    = 'You set reminder for today. I think it\'s better to buy now.';
?>