<?php
header('Content-type: text/html; charset=utf-8');
//header('Content-Type: text/html; charset=iso-8859-1');

require_once('config.php');
require_once('php/form.php');
require_once('php/old_database.php');
require_once('../lekerdezes.php');
require_once('../functions.php');


$old_query  = new Mysql_Lekeres(DB_HOSTNAME, DB_USERNAME,DB_PASSWORD, OLD_DB_DATABASE);
$query      = new Mysql_Lekeres(DB_HOSTNAME, DB_USERNAME,DB_PASSWORD, DB_DATABASE, true);

$old_database = new ModelOldDatabaseTabla($old_query);
$tables       = new ModelFormTabla($query);

if (isset($_GET['route'])) {
    if(strpos($_GET['route'],"/")){
        $route = explode("/",$_GET['route']);
        $directory = $route[0];
        if(is_dir($directory)){
            $file = $route[1];
            if(is_file($directory . "/" . $file . ".php")){
                if(isset($route[2])){
                    $function = $route[2];
                    require_once($directory. "/" .$file . ".php");
                    if(is_callable($function)){
                        call_user_func($function,"");
                    }
                }
                else{
                    require_once($directory. "/" .$file . ".php");
                }
            }
        }
    }
}

$tables->csvFiles();


if (isset($_GET['file']) && $_GET['file']) {
    $tables->csvBeolvas($_GET['file']);
} else {
    $tables->csvBeolvas();
}

require_once('tpl/form.tpl');

?>