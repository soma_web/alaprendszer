<?php
    require_once(DIR_SYSTEM . 'library/merchant.php'); //BUDAPESTBANK

class ControllerPaymentBudapestbank extends Controller {
    protected function index() {
        //$this->language->load('payment/pp_standard');
        $this->language->load('payment/budapestbank');

        $this->data['text_testmode'] = $this->language->get('text_testmode');

        $this->data['button_confirm'] = $this->language->get('button_confirm');

        //$this->data['testmode'] = $this->config->get('pp_standard_test');
        $this->data['testmode'] = $this->config->get('budapestbank_test');

        //if (!$this->config->get('budapestbank_test')) {
            //$this->data['action'] = 'https://www.paypal.com/cgi-bin/webscr';
            //$this->data['action'] = 'https://';
        //} else {
            //$this->data['action'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
            //$this->data['action'] = 'https://';
        //}

        //$this->data['action'] = $this->url->link('payment/budapestbank/buildconnection', '', 'SSL');

            $this->load->model('checkout/order');

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        if ($order_info) {
            $this->data['business'] = $this->config->get('budapestbank_email');
            $this->data['item_name'] = html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');
            $this->data['products'] = array();
            $bpbankdesc = '';

            foreach ($this->cart->getProducts() as $product) {
                $option_data = array();

                foreach ($product['option'] as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['option_value'];
                    } else {
                        $filename = $this->encryption->decrypt($option['option_value']);

                        $value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
                    }

                    $option_data[] = array(
                        'name'  => $option['name'],
                        'value' => (utf8_strlen($value) > 40 ? utf8_substr($value, 0, 40) . '...' : $value)
                    );
                }

                $bpbankdesc .= $product['name'].', ';

                $this->data['products'][] = array(
                    'name'     => $product['name'],
                    'model'    => $product['model'],
                    'price'    => $this->currency->format($product['price'], $order_info['currency_code'], false, false),
                    'quantity' => $product['quantity'],
                    'option'   => $option_data,
                    'weight'   => $product['weight']
                );
            }



            $this->data['discount_amount_cart'] = 0;

            $total = $this->currency->format($order_info['total'] - $this->cart->getSubTotal(), $order_info['currency_code'], false, false);

            if ($total > 0) {
                $this->data['products'][] = array(
                    'name'     => $this->language->get('text_total'),
                    'model'    => '',
                    'price'    => $total,
                    'quantity' => 1,
                    'option'   => array(),
                    'weight'   => 0
                );
            } else {
                $this->data['discount_amount_cart'] -= $total;
            }

            $this->data['total'] = str_replace( array(".", ","), array("", ""), $this->currency->format($order_info['total'], $order_info['currency_code'], false, false));

            $this->data['currency_code'] = $order_info['currency_code'];
            $this->data['first_name'] = html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8');
            $this->data['last_name'] = html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8');
            $this->data['address1'] = html_entity_decode($order_info['payment_address_1'], ENT_QUOTES, 'UTF-8');
            $this->data['address2'] = html_entity_decode($order_info['payment_address_2'], ENT_QUOTES, 'UTF-8');
            $this->data['city'] = html_entity_decode($order_info['payment_city'], ENT_QUOTES, 'UTF-8');
            $this->data['zip'] = html_entity_decode($order_info['payment_postcode'], ENT_QUOTES, 'UTF-8');
            $this->data['country'] = $order_info['payment_iso_code_2'];
            $this->data['email'] = $order_info['email'];
            $this->data['invoice'] = $this->session->data['order_id'] . ' - ' . html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8') . ' ' . html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8');
            $this->data['lc'] = $this->session->data['language'];

            $this->data['return']           = $_GET['route'] == "checkout/confirm_megrendelem" ? $this->url->link('checkout/success&megrendelem=1') : $this->url->link('checkout/success');
            $this->data['cancel_return']    = $_GET['route'] == "checkout/confirm_megrendelem" ? $this->url->link('checkout/checkout_megrendelem', '', 'SSL') : $this->url->link('checkout/checkout', '', 'SSL');

            $this->data['notify_url'] = $this->url->link('payment/budapestbank/callback', '', 'SSL');

            /*
            if (!$this->config->get('budapestbank_transaction')) {
                $this->data['paymentaction'] = 'authorization';
            } else {
                $this->data['paymentaction'] = 'sale';
            }
            */

            if ($this->config->get('budapestbank_transaction') == 'sms') {
                $this->data['action'] = $this->url->link('payment/budapestbank/buildconnection', '', 'SSL');
            } else if($this->config->get('budapestbank_transaction') == 'dms') {
                $this->data['action'] = $this->url->link('payment/budapestbank/buildconnectiondms', '', 'SSL');
            } else {
                $this->data['action'] = $this->url->link('payment/budapestbank/buildconnection', '', 'SSL');
            }


            $this->data['bpbankdesc'] = $this->language->get('text_invoice').$this->session->data['order_id'] . ' - ' . $this->language->get('text_name') . html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8') . ' ' . html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8').' - '.$this->language->get('text_products').rtrim($bpbankdesc, " , ");

            $this->data['custom'] = $this->encryption->encrypt($this->session->data['order_id']);

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/budapestbank.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/payment/budapestbank.tpl';
            } else {
                $this->template = 'default/template/payment/budapestbank.tpl';
            }

            $this->render();
        }
    }

    public function buildconnection() {

        $this->load->model('payment/budapestbank');
        $this->model_payment_budapestbank->checkDB(); //adatbazis ellenorzese

        //global $registry;
        //BUDAPESTBANK
            //$ecomm_server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
            //$ecomm_client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';

            /*
            if (!$this->config->get('budapestbank_test')) { //just test
                $server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
                $client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';
            } else {
                $server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
                $client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';
            }
            */
        //BUDAPESTBANK

        //BUDAPESTBANK

            //$url = ""; $keystore = ""; $keystorepassword = ""; $verbose = 0;
            // Merchant
            //$registry->set('merchant', new Merchant($url, $keystore, $keystorepassword, $verbose = 0) );
        //BUDAPESTBANK

        if($this->config->get('budapestbank_test')) {
            //!!!TEST MODE!!!!
            $ecomm_server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
            $ecomm_client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';
            //!!!TEST MODE!!!!
        } else {
            //!!! THIS IS NOT TEST MODE - WATCH !!! /*USE THIS WHEN YOU GO TO PRODUCTION SYSTEM, ALSO CHANGE KEYSTORE AND PASSWORD*/
            $ecomm_server_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler'
            $ecomm_client_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv/ecomm/ClientHandler'
            //!!! THIS IS NOT TEST MODE - WATCH !!!
        }

        //$cert_url             = '/home/samba/shares/www/budapestbankteszt/domain/certs/EA003720keystore.pem'; //full path to keystore file
        $cert_url             = DIR_SYSTEM . $this->config->get('budapestbank_cert_url');
        $cert_pass            = $this->config->get('budapestbank_cert_pass'); //keystore password

        //MYSQL config
//!!!!! DO NOT CREATE DATABASE OR TABLE YOURSELF, IT WILL BE DONE AUTOMATICALY. CHANGE ONLY USER, PASS, HOST. !!!!!
        //$db_user                =     'root';
        //$db_pass                =     '';
        //$db_host                =     'localhost';
        //$db_database            =     'bpbankteszt';
        //$db_table_transaction   =     'transaction';
        //$db_table_batch         =     'batch';
        //$db_table_error         =     'error';
        //MYSQL config

        //MYSQL
        //connect to MySQL server
        //$link = mysqli_connect($db_host, $db_user, $db_pass);
        //if (!$link) {
            //die(' ***Could not connect MySQL server: ' . mysql_error());
        //}

        //mysql_query("CREATE DATABASE /*!32312 IF NOT EXISTS*/ $db_database");


        // connect to DB
        //$db_selected = mysql_select_db($db_database, $link);
        //if (!$db_selected) {
            //die (' ***Could not connect MySQL DB: ' . mysql_error());
        //}
        //MYSQL


        //TESZT
        /*
        $amount       = $_POST['post_amount'];
        $currency     = $_POST['post_currency'];
        $ip           = $_POST['post_ip'];
        $description  = urlencode(htmlspecialchars($_POST['post_description'], ENT_QUOTES));
        $language     = $_POST['post_language'];
        */

        $amount       = $_POST['bpbank_total']; //'300000'
        $currency     = $this->config->get('budapestbank_deviza');
        $ip           = $this->config->get('budapestbank_client_ip');
        $description  = $_POST['bpbank_desc'];
        $language     = $this->config->get('budapestbank_client_language');

        $merchant = new Merchant($ecomm_server_url, $cert_url, $cert_pass, 1);

        $resp = $merchant->startSMSTrans($amount, $currency, $ip, $description, $language);

        if (substr($resp,0,14)=="TRANSACTION_ID") {
            $trans_id = substr($resp,16,28);
            $url = $ecomm_client_url."?trans_id=". urlencode($trans_id);

            //$sql = mysql_query("INSERT INTO $db_table_transaction VALUES ('', '$trans_id', '$amount', '$currency', '$ip', '$description', '$language', '---', '???', '???', '???', '???', now(), '$resp', '', '')");
            $sql = $this->model_payment_budapestbank->saveTransaction($trans_id, $amount, $currency, $ip, $description, $language, $resp);

            header("Location: $url");

            if (!$sql) {
                die('*** Invalid query: ' . mysql_error());
            }

        }else{
            echo $resp;
            $resp = htmlentities($resp, ENT_QUOTES);

            //$sql = mysql_query("INSERT INTO $db_table_error VALUES ('', now(), 'startsmstrans', '$resp')");
            $sql = $this->model_payment_budapestbank->saveBadTransaction($resp);

            if (!$sql) {
                die('*** Invalid query2: ' . mysql_error());
            }
        }
        //TESZT
    }

    public function buildconnectiondms() { //default is sms

        if($this->config->get('budapestbank_test')) {
            //!!!TEST MODE!!!!
            $ecomm_server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
            $ecomm_client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';
            //!!!TEST MODE!!!!
        } else {
            //!!! THIS IS NOT TEST MODE - WATCH !!! /*USE THIS WHEN YOU GO TO PRODUCTION SYSTEM, ALSO CHANGE KEYSTORE AND PASSWORD*/
            $ecomm_server_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler'
            $ecomm_client_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv/ecomm/ClientHandler'
            //!!! THIS IS NOT TEST MODE - WATCH !!!
        }

        $cert_url             = DIR_SYSTEM . $this->config->get('budapestbank_cert_url');
        $cert_pass            = $this->config->get('budapestbank_cert_pass'); //keystore password

        $amount       = $_POST['bpbank_total'];
        $currency     = $this->config->get('budapestbank_deviza');
        $ip           = $this->config->get('budapestbank_client_ip');
        $description  = $_POST['bpbank_desc'];
        $language     = $this->config->get('budapestbank_client_language');

        $merchant = new Merchant($ecomm_server_url, $cert_url, $cert_pass, 1);

        $resp = $merchant->startDMSAuth($amount, $currency, $ip, $description, $language);

        if (substr($resp,0,14)=="TRANSACTION_ID") {
            $trans_id = substr($resp,16,28);
            $url = $ecomm_client_url."?trans_id=". urlencode($trans_id);
            header("Location: $url");

            $sql = $this->db->query("INSERT INTO transaction VALUES ('', '$trans_id', '$amount', '$currency', '$ip', '$description', '$language', 'NO', '???', '???', '???', '???', now(), '$resp', '', '')");

            if (!$sql) {
                die('*** Invalid query1: ' . mysql_error());
            }
        }else{
            echo $resp;
            $resp = htmlentities($resp, ENT_QUOTES);
            $sql = $this->db->query("INSERT INTO error VALUES ('', now(), 'startDMSAuth', '$resp')");

            if (!$sql) {
                die('*** Invalid query2: ' . mysql_error());
            }
        }
    }

    public function returnok() {
        $this->load->model('payment/budapestbank');

        if($this->config->get('budapestbank_test')) {
            //!!!TEST MODE!!!!
            $ecomm_server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
            $ecomm_client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';
            //!!!TEST MODE!!!!
        } else {
            //!!! THIS IS NOT TEST MODE - WATCH !!! /*USE THIS WHEN YOU GO TO PRODUCTION SYSTEM, ALSO CHANGE KEYSTORE AND PASSWORD*/
            $ecomm_server_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler'
            $ecomm_client_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv/ecomm/ClientHandler'
            //!!! THIS IS NOT TEST MODE - WATCH !!!
        }


        $cert_url             = DIR_SYSTEM . $this->config->get('budapestbank_cert_url');
        $cert_pass            = $this->config->get('budapestbank_cert_pass'); //keystore password

        $trans_id = $_POST['trans_id'];
        //$var = ($_POST['var']); //getting additional parameters

        //$sql = mysql_query("SELECT client_ip_addr FROM $db_table_transaction WHERE `trans_id` = '$trans_id'");
        $sql = $this->model_payment_budapestbank->getTransaction($trans_id);

        if (!$sql) {
            die('*** Invalid query: ' . mysql_error());
        }else{
            $row = $sql->row;
            $client_ip_addr = $row; //$row[0]
        }

        $merchant = new Merchant($ecomm_server_url, $cert_url, $cert_pass, 1);

        $resp = $merchant->getTransResult(urlencode($trans_id), $client_ip_addr);

        if (strstr($resp, 'RESULT:')) {
            //$resp example RESULT: OK RESULT_CODE: 000 3DSECURE: NOTPARTICIPATED RRN: 915300393049 APPROVAL_CODE: 705368 CARD_NUMBER: 4***********9913


            if (strstr($resp, 'RESULT:')) {
                $result = explode('RESULT: ', $resp);
                $result = preg_split( '/\r\n|\r|\n/', $result[1] );
                $result = $result[0];
            }else{
                $result = '';
            }

            if (strstr($resp, 'RESULT_CODE:')) {
                $result_code = explode('RESULT_CODE: ', $resp);
                $result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
                $result_code = $result_code[0];
            }else{
                $result_code = '';
            }

            if (strstr($resp, '3DSECURE:')) {
                $result_3dsecure = explode('3DSECURE: ', $resp);
                $result_3dsecure = preg_split( '/\r\n|\r|\n/', $result_3dsecure[1] );
                $result_3dsecure = $result_3dsecure[0];
            }else{
                $result_3dsecure = '';
            }

            if (strstr($resp, 'CARD_NUMBER:')) {
                $card_number = explode('CARD_NUMBER: ', $resp);
                $card_number = preg_split( '/\r\n|\r|\n/', $card_number[1] );
                $card_number = $card_number[0];
            }else{
                $card_number = '';
            }

            /*
            $sql = mysql_query("UPDATE $db_table_transaction SET
              `result` = '$result',
              `result_code` = '$result_code',
              `result_3dsecure` = '$result_3dsecure',
              `card_number` = '$card_number',
              `response` = '$resp'
              WHERE `trans_id` = '$trans_id'");
            */
            $sql = $this->model_payment_budapestbank->updateTransaction($result,$result_code,$result_3dsecure,$card_number,$resp,$trans_id);

            if (!$sql) {
                die('*** Invalid query: ' . mysql_error());
            }

            //echo $resp; //kiirja a visszakapott cuccot

        }else{

            //echo $resp;
            $resp = htmlentities($resp, ENT_QUOTES);

            //$sql = mysql_query("INSERT INTO $db_table_error VALUES ('', now(), 'ReturnOkURL', '$resp')");
            $sql = $this->model_payment_budapestbank->errorTransaction($resp); //mysql_query("INSERT INTO $db_table_error VALUES ('', now(), 'ReturnOkURL', '$resp')")

            if (!$sql) {
                die('*** Invalid query2: ' . mysql_error());
            }

            $result_code = '';
        }

        ////SHOW RESULT TO USER

            //$this->language->load('account/logout');
            $this->language->load('payment/budapestbank');

            $this->document->setTitle($this->language->get('heading_title'));

            $this->data['breadcrumbs'] = array();

            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_home'),
                'href'      => $this->url->link('common/home'),
                'separator' => false
            );

            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_budapestbank'),
                'href'      => '#'/*$this->url->link('account/account', '', 'SSL')*/,
                'separator' => $this->language->get('text_separator')
            );

            /*
            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_logout'),
                'href'      => $this->url->link('account/logout', '', 'SSL'),
                'separator' => $this->language->get('text_separator')
            );
            */

            $this->data['heading_title'] = $this->language->get('heading_title');

            if($this->config->get('budapestbank_debug')) {
                    $this->data['text_message'] = 'Debug mode: <p>'.$resp.'<p>';
            } else {
                if($result_code == '000') {
                    $this->data['text_message'] = $this->language->get('text_message');
                } else {
                    $this->data['text_message'] = $this->language->get('text_error_message');
                }
            }

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/common/success.tpl';
            } else {
                $this->template = 'default/template/common/success.tpl';
            }

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());

        ////SHOW RESULT TO USER
    }

    public function returnfail() {
        $this->load->model('payment/budapestbank');

        if($this->config->get('budapestbank_test')) {
            //!!!TEST MODE!!!!
            $ecomm_server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
            $ecomm_client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';
            //!!!TEST MODE!!!!
        } else {
            //!!! THIS IS NOT TEST MODE - WATCH !!! /*USE THIS WHEN YOU GO TO PRODUCTION SYSTEM, ALSO CHANGE KEYSTORE AND PASSWORD*/
            $ecomm_server_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler'
            $ecomm_client_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv/ecomm/ClientHandler'
            //!!! THIS IS NOT TEST MODE - WATCH !!!
        }

        $cert_url             = DIR_SYSTEM . $this->config->get('budapestbank_cert_url');
        $cert_pass            = $this->config->get('budapestbank_cert_pass'); //keystore password

        $trans_id = $_POST['trans_id'];
        $error_msg = $_POST['error'];
        //$var = ($_POST['var']); //getting additional parameters
        //echo 'Tehnical error occurred! Please contact merchant! <br><br>'.$error_msg;


        //$sql = mysql_query("SELECT client_ip_addr FROM $db_table_transaction WHERE `trans_id` = '$trans_id'");
        $sql = $this->model_payment_budapestbank->getTransaction($trans_id);

        if (!$sql) {
            die('*** Invalid query1: ' . mysql_error());
        }else{
            $row = $sql->row;
            $client_ip_addr = $row; //$row[0]
        }

        $merchant = new Merchant($ecomm_server_url, $cert_url, $cert_pass, 1);

        $resp = $merchant->getTransResult(urlencode($trans_id), $client_ip_addr);
        $resp = htmlentities($resp, ENT_QUOTES);
        $resp = $error_msg.' + '.$resp;

        //$sql = mysql_query("INSERT INTO $db_table_error VALUES ('', now(), 'ReturnFailURL', '$resp')");
        $sql = $this->model_payment_budapestbank->errorTransaction($resp);

        if (!$sql) {
            die('*** Invalid query2: ' . mysql_error());
        }

        ////SHOW RESULT TO USER

            //$this->language->load('account/logout');
            $this->language->load('payment/budapestbank');

            $this->document->setTitle($this->language->get('heading_title'));

            $this->data['breadcrumbs'] = array();

            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_home'),
                'href'      => $this->url->link('common/home'),
                'separator' => false
            );

            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_budapestbank'),
                'href'      => '#'/*$this->url->link('account/account', '', 'SSL')*/,
                'separator' => $this->language->get('text_separator')
            );

            /*
            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_logout'),
                'href'      => $this->url->link('account/logout', '', 'SSL'),
                'separator' => $this->language->get('text_separator')
            );
            */

            $this->data['heading_title'] = $this->language->get('heading_title');

            $this->data['text_message'] = $this->language->get('text_error_message').'<br />'.$error_msg;

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/common/success.tpl';
            } else {
                $this->template = 'default/template/common/success.tpl';
            }

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());

        ////SHOW RESULT TO USER
    }

    //BUSINESS DAY CLOSURE

    public function close() {

        $this->load->model('payment/budapestbank');

        if($this->config->get('budapestbank_test')) {
            //!!!TEST MODE!!!!
            $ecomm_server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
            $ecomm_client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';
            //!!!TEST MODE!!!!
        } else {
            //!!! THIS IS NOT TEST MODE - WATCH !!! /*USE THIS WHEN YOU GO TO PRODUCTION SYSTEM, ALSO CHANGE KEYSTORE AND PASSWORD*/
            $ecomm_server_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler'
            $ecomm_client_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv/ecomm/ClientHandler'
            //!!! THIS IS NOT TEST MODE - WATCH !!!
        }

        $cert_url             = DIR_SYSTEM . $this->config->get('budapestbank_cert_url');
        $cert_pass            = $this->config->get('budapestbank_cert_pass');
        $db_table_batch       = 'batch';

        $merchant = new Merchant($ecomm_server_url, $cert_url, $cert_pass, 1);
        $resp = $merchant->closeDay();
        //$resp = 'RESULT: OK RESULT_CODE: 500 FLD_075: 4 FLD_076: 6 FLD_087: 40 FLD_088: 60'; //Tested - working
        //RESULT: OK RESULT_CODE: 500 FLD_075: 4 FLD_076: 6 FLD_087: 40 FLD_088: 60

        if (strstr($resp, 'RESULT:')) {
            $result = explode('RESULT: ', $resp);
            $result = preg_split( '/\r\n|\r|\n/', $result[1] );
            $result = $result[0];
        }else{
            $result = '';
        }

        if (strstr($resp, 'RESULT_CODE:')) {
            $result_code = explode('RESULT_CODE: ', $resp);
            $result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
            $result_code = $result_code[0];
        }else{
            $result_code = '';
        }

        if (strstr($resp, 'FLD_075:')) {
            $count_reversal = explode('FLD_075: ', $resp);
            $count_reversal = preg_split( '/\r\n|\r|\n/', $count_reversal[1] );
            $count_reversal = $count_reversal[0];
        }else{
            $count_reversal = '';
        }

        if (strstr($resp, 'FLD_076:')) {
            $count_transaction = explode('FLD_076: ', $resp);
            $count_transaction = preg_split( '/\r\n|\r|\n/', $count_transaction[1] );
            $count_transaction = $count_transaction[0];
        }else{
            $count_transaction = '';
        }

        if (strstr($resp, 'FLD_087:')) {
            $amount_reversal = explode('FLD_087: ', $resp);
            $amount_reversal = preg_split( '/\r\n|\r|\n/', $amount_reversal[1] );
            $amount_reversal = $amount_reversal[0];
        }else{
            $amount_reversal = '';
        }

        if (strstr($resp, 'FLD_088:')) {
            $amount_transaction = explode('FLD_088: ', $resp);
            $amount_transaction = preg_split( '/\r\n|\r|\n/', $amount_transaction[1] );
            $amount_transaction = $amount_transaction[0];
        }else{
            $amount_transaction = '';
        }

        $sql = $this->model_payment_budapestbank->insertCloseDay($db_table_batch, $result, $result_code, $count_reversal, $count_transaction, $amount_reversal, $amount_transaction, $resp);

        if (!$sql) {
            die('*** Invalid query: ' . mysql_error());
        }

        //echo $resp;
    }

    //BUSINESS DAY CLOSUE

    public function callback() { //not needed - later remove it
        if (isset($this->request->post['custom'])) {
            $order_id = $this->encryption->decrypt($this->request->post['custom']);
        } else {
            $order_id = 0;
        }

        $this->load->model('checkout/order');

        $order_info = $this->model_checkout_order->getOrder($order_id);

        if ($order_info) {
            $request = 'cmd=_notify-validate';

            foreach ($this->request->post as $key => $value) {
                $request .= '&' . $key . '=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
            }

            if (!$this->config->get('budapestbank_test')) {
                $curl = curl_init('https://');
            } else {
                $curl = curl_init('https://');
            }

            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $response = curl_exec($curl);

            if (!$response) {
                $this->log->write('BUDAPESTBANK :: CURL failed ' . curl_error($curl) . '(' . curl_errno($curl) . ')');
            }

            if ($this->config->get('budapestbank_debug')) {
                $this->log->write('BUDAPESTBANK :: IPN REQUEST: ' . $request);
                $this->log->write('BUDAPESTBANK :: IPN RESPONSE: ' . $response);
            }

            if ((strcmp($response, 'VERIFIED') == 0 || strcmp($response, 'UNVERIFIED') == 0) && isset($this->request->post['payment_status'])) {
                $order_status_id = $this->config->get('config_order_status_id');

                switch($this->request->post['payment_status']) {
                    case 'Canceled_Reversal':
                        $order_status_id = $this->config->get('budapestbank_canceled_reversal_status_id');
                        break;
                    case 'Completed':
                        if ((float)$this->request->post['mc_gross'] == $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false)) {
                            $order_status_id = $this->config->get('budapestbank_completed_status_id');
                        }
                        break;
                    case 'Denied':
                        $order_status_id = $this->config->get('budapestbank_denied_status_id');
                        break;
                    case 'Expired':
                        $order_status_id = $this->config->get('budapestbank_expired_status_id');
                        break;
                    case 'Failed':
                        $order_status_id = $this->config->get('budapestbank_failed_status_id');
                        break;
                    case 'Pending':
                        $order_status_id = $this->config->get('budapestbank_pending_status_id');
                        break;
                    case 'Processed':
                        $order_status_id = $this->config->get('budapestbank_processed_status_id');
                        break;
                    case 'Refunded':
                        $order_status_id = $this->config->get('budapestbank_refunded_status_id');
                        break;
                    case 'Reversed':
                        $order_status_id = $this->config->get('budapestbank_reversed_status_id');
                        break;
                    case 'Voided':
                        $order_status_id = $this->config->get('budapestbank_voided_status_id');
                        break;
                }

                if (!$order_info['order_status_id']) {
                    $this->model_checkout_order->confirm($order_id, $order_status_id);
                } else {
                    $this->model_checkout_order->update($order_id, $order_status_id);
                }
            } else {
                $this->model_checkout_order->confirm($order_id, $this->config->get('config_order_status_id'));
            }

            curl_close($curl);
        }
    }
}
?>