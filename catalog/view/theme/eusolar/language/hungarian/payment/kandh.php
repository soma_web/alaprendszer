<?php 
$_['text_success_customer'] 	= '<p>Megrendelését sikeresen felvettük! </p><p>A tranzakció azonosítója: %s <br />Engedélyszám: %s <br />Fizetett összeg: %s<br /></p><p>Ha utánvétes szállítást választott, a megrendelt termékek 1-4 munkanapon belül érkeznek meg rendeltetési helyükre. Az összeg megérkezése után 2-4 napon belül kiszállítjuk a termékeket.</p><p>Korábbi rendeléseit a <a href="%s">Fiókom</a> oldalon a <a href="%s">Korábbi rendelések</a> hivatkozásra kattintással tekintheti meg.</p><p>Kérjük, hogy kérdéseit a .... címen tegye fel.</p><p>Köszönjük, hogy nálunk vásárolt online!</p>';
$_['text_success_guest']    	= '<p>Megrendelését sikeresen felvettük!</p><p>Köszönjük, hogy nálunk vásárolt online! Amennyiben kérdése lenne, kérem <a href="%s">léplen velünk kapcsolatba</a>.</p>';

$_['text_failed_customer']  	= '<p>A banki fizetés sikertelen!</p><p>A tranzakció azonosítója: %s <br />Engedélyszám: %s <br />Fizetett összeg: %s <br /></p>';
$_['text_failed_guest']  		= '<p>A banki fizetés sikertelen!</p><p>A tranzakció azonosítója: %s <br />Engedélyszám: %s <br />Fizetett összeg: %s <br /></p>';

$_['text_basket']   			= 'Kosár';
$_['text_checkout'] 			= 'Fizetés';
$_['text_success_checkout']  	= 'Sikeres';
$_['heading_title_checkout'] 	= 'Megrendelését feldolgoztuk!';
$_['text_title'] 				= 'Online fizetés';

$_['button_checkout_again'] 	= 'Vásárlás újból';


/* visszajelzés felületen + emailben template */
$_['text_bankpayment_success'] 	= 'A bankkártyás fizetés sikeres.';
$_['text_bankpayment_failed'] 	= 'A bankkártyás fizetés sikertelen.';
$_['text_failedpayment_reason']	= 'A sikertelen fietés oka:';
$_['text_bankauth_number']		= 'A banki engedélyszám:';
$_['text_trid_number']			= 'Tranzakció azonosító:';
$_['text_amount']				= 'Összeg:';

/* hibás fizetési válaszok a banktól */
$_['text_unsuccessfull_payment']	= 'A fizetés eredménytelen (pl. nincs a számlán pénz)';
$_['text_expired_card']				= 'Lejárt fizetési kísélet (letelt a 25 perc)';
$_['text_error_during_payment']		= 'Hiba';
$_['text_cancel_payment']			= 'A kártyabirtokos "Mégsem" gombot nyomott';
$_['text_unknown_trid']				= 'Ismeretlen tranzakcióazonosító';
$_['text_noresult_yet']				= 'Még nincs eredmény, ismételt hívás szükséges';
$_['text_timeout_payment']			= 'A fizetés timeout miatt eredménytelen (bezárták a böngészőt vagy megszakadt a kapcsolat)';
?>