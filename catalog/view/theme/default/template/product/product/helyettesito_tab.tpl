<?php if (isset($products_helyettesito) && !empty($products_helyettesito)) { ?>

    <div id="tabhelyettesito" class="htabs">
        <div class="inner-htabs">
            <a  href="#tab-helyettesito"><?php echo $tab_helyettesito; ?> (<?php echo count($products_helyettesito); ?>) </a>
        </div>
    </div>


    <div id="tab-helyettesito" class="tab-content">
        <div class="box-product tab_kornyezet tab-helyettesito" style="width: 100%">
                <?php foreach ($products_helyettesito as $product) { ?>
                        <div class="termek_tabban">
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>
                <?php } ?>
        </div>
    </div>
<?php } ?>

<script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/bxslider/jquery.bxslider.css" />

<script>

    $('#tabhelyettesito a').tabs({
        bxSlidesOn: <?php echo isset($beallitas_termek_aloldal['korhinta_tart']) ? 1 : 0?>,
        minSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab_tart']) ? $beallitas_termek_aloldal['korhinta_darab_tart'] : 3?>,
        maxSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab_tart']) ? $beallitas_termek_aloldal['korhinta_darab_tart'] : 3?>,
        slideWidth: <?php echo isset($beallitas_termek_aloldal['korhinta_width_tart']) ? $beallitas_termek_aloldal['korhinta_width_tart'] : 260?>,
        slideMargin: <?php echo isset($beallitas_termek_aloldal['korhinta_margin_tart']) ? $beallitas_termek_aloldal['korhinta_margin_tart'] : 20?>,
        randomStart: <?php echo isset($beallitas_termek_aloldal['korhinta_random_tart']) ? "true" : "false" ?>,
        autoHidePager: true /*  Eltűnteti a nyilakat, ha minSlides értékénél kevesebb kép jelenik meg. */

    });

</script>

