<?php
$_['motto']          = array(
    'type'              => 'checkbox',
    'name'              => 'motto',
    'default'           => "0",
    'heading'           => 'Mottó megjelenítés'
);

$_['honlapom']          = array(
    'type'              => 'checkbox',
    'name'              => 'honlapom',
    'default'           => "0",
    'heading'           => 'Honlapom megjelenítés'
);
?>