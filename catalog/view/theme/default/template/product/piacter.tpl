<?php echo $header; ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>

<?php echo $column_left; ?><?php echo $column_right; ?>

    <div id="content"><?php echo $content_top; ?>


        <?php if ($thumb || $description) { ?>
            <div class="category-info">
                <?php if ($thumb) { ?>
                    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
                <?php } ?>
                <?php if ($description) { ?>
                    <h1><?php echo $heading_title; ?></h1>
                    <?php echo $description; ?>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if ($categories) { ?>
            <h3><?php //echo $text_refine; ?></h3>
            <div class="category-list">
                <?php if (count($categories) <= 5) { ?>
                    <ul>
                        <?php foreach ($categories as $category) { ?>
                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                <?php } else { ?>
                    <?php for ($i = 0; $i < count($categories);) { ?>
                        <ul>
                            <?php $j = $i + ceil(count($categories) / 4); ?>
                            <?php for (; $i < $j; $i++) { ?>
                                <?php if (isset($categories[$i])) { ?>
                                    <li><a href="<?php echo $categories[$i]['href']; ?>"><?php echo $categories[$i]['name']; ?></a></li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>






        <?php if ($products) { ?>
            <div class="product-filter">

                <div class="limit"><b><?php echo $text_limit; ?></b>
                    <select onchange="location = this.value;">
                        <?php foreach ($limits as $limits) {
                            $limits['href'].="&piacter=piacter"; ?>
                            <?php if ($limits['value'] == $limit) { ?>
                                <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="sort"><b><?php echo $text_sort; ?></b>
                    <select onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts) {
                            $sorts['href'].="&piacter=piacter"; ?>?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="pagination"><?php echo $pagination; ?></div>

                <table class="piacter">
                    <tr class="vilagos">
                        <td style="text-align: left"><?php echo $text_megnevezes; ?></td>
                        <td><?php echo $text_azonosito; ?></td>
                        <td><?php echo $text_netto_ar; ?></td>
                        <td><?php echo $text_brutto_ar; ?></td>
                        <!--<td><?php echo $text_suly; ?></td>-->
                        <td></td>
                    </tr>
                <?php
                $i=1;
                foreach ($products as $product) {
                    if (isset($product['quantity']) &&  $product['quantity'] > 0 || true){
                        if ($i==1){
                          $i=0;
                          $class = "vilagos";
                        }
                        else {
                            $i=1;
                            $class = "sotet";
                        } ?>

                        <? $product['href'].= "&piacter=piacter"; ?>
                        <tr class="<? echo $class?>">
                        <td style="text-align: left">
                            <a  id="a_szov"  style="text-decoration: none; cursor: default" title="<?php echo $product['name']; ?>">
                                <?php echo substr($product['name'],0,40); ?>
                            </a>
                        </td>
                        <td><?php echo $product['model']; ?></td>


                        <td><?php echo $product['tax']; ?></td>

                        <?php if ($product['price']) { ?>
                            <td>
                                <?php if (!$product['special']) { ?>
                                    <?php echo $product['price']; ?>
                                <?php } else { ?>
                                    <?php echo $product['special']; ?>
                                <?php } ?>

                            </td>
                        <?php } ?>
                        <!--<td><?php echo  number_format($product['weight'],2,"."," ") ?></td>-->
                        <td >
                            <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button1" />
                        </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </table>


        <?php } ?>

        <?php if (!$categories && !$products) { ?>
            <div class="content"><?php echo $text_empty; ?></div>
            <div class="buttons">
                <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
            </div>
        <?php } ?>
        <?php echo $content_bottom; ?></div>


<?php echo $footer; ?>