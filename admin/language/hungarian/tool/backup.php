<?php

// Heading
$_['heading_title']    = 'Biztonsági mentés / Visszaállítás';

// Text
$_['text_backup']      = 'Biztonsági mentés letöltése';
$_['text_success']     = 'Siker: Az adatbázis importálása sikerült!';

// Entry
$_['entry_restore']    = 'Biztonsági mentés visszaállítása:';
$_['entry_backup']     = 'Backup:';

// Error
$_['error_permission'] = 'Figyelmeztetés: A biztonsági mentések módosítása az Ön számára nem engedélyezett';
$_['error_empty']      = 'Figyelmeztetés: a feltöltött file üres!';
?>
