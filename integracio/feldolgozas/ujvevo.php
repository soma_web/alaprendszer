<?php
class Ujvevo{

    private $query;

    public function __construct($query) {
        $this->query = $query;
    }

    public function Vevo_Kuldes() {


        $sql = "SELECT * FROM sapnak_vevok";
        $query_vevok= $this->query->query($sql);
        if ($query_vevok->num_rows > 0){

           $xml="<BOM>\n";

            foreach($query_vevok->rows as $value){

                $xml .="  <BO>\n";
                $xml .="    <AdmInfo>\n";
                $xml .="      <Object>2</Object>\n";
                $xml .="      <Version>2</Version>\n";
                $xml .="    </AdmInfo>\n";


                $xml .= "    <BusinessPartners>\n";
                $xml .= "      <row>\n";
                $xml .= "        <CardCode>".$value['vevokod']."</CardCode>\n";
                $xml .= "        <CardName>".$value['vevo_neve']."</CardName>\n";
                $xml .= "        <CardType>cCustomer</CardType>\n";
                $xml .= "        <GroupCode>".$value['vevo_csoport']."</GroupCode>\n";    // csoporkod vagy SAP-s csoporkod ???
                $xml .= "        <PayTermsGrpCode>".$value['fizetesi_mod']."</PayTermsGrpCode>\n";
                $xml .= "        <Phone1>".$value['telefonszam']."</Phone1>\n";
                $xml .= "        <EmailAddress>".$value['mail']."</EmailAddress>\n";
                $xml .= "        <Notes></Notes>\n";
                $xml .= "        <AdditionalID>".$value['adoszam']."</AdditionalID>\n";
                $xml .= "      </row>\n";
                $xml .= "    </BusinessPartners>\n";

                $xml .= "    <BPAddresses>\n";
                $xml .= "      <row>\n";
                $xml .= "        <AddressType>bo_ShipTo</AddressType>\n";
                $xml .= "        <AddressName>Szállítási cím</AddressName>\n";
                $xml .= "        <Street>".$value['szall_utca']."</Street>\n";
                $xml .= "        <ZipCode>".$value['szall_iranyitoszam']."</ZipCode>\n";
                $xml .= "        <City>".$value['szall_varos']."</City>\n";
                $xml .= "        <Country>".$value['szall_orszagkod']."</Country>\n";
                $xml .= "      </row>\n";
                $xml .= "      <row>\n";
                $xml .= "        <AddressType>bo_BillTo</AddressType>\n";
                $xml .= "        <AddressName>Számlázási cím</AddressName>\n";
                $xml .= "        <Street>".$value['szaml_utca']."</Street>\n";
                $xml .= "        <ZipCode>".$value['szaml_iranyitoszam']."</ZipCode>\n";
                $xml .= "        <City>".$value['szaml_varos']."</City>\n";
                $xml .= "        <Country>".$value['szaml_orszagkod']."</Country>\n";
                $xml .= "      </row>\n";
                $xml .= "    </BPAddresses>\n";
                $xml .="  </BO>\n";
            }
            $xml .="</BOM>";

            $oOrders = "xml_export/oBusinessPartners_".date("Ymd_His").".xml";
            if (file_put_contents($oOrders,$xml)){
                $sql = "drop table if exists sapnak_vevok";
                $this->query->query($sql);
                echo "Az XML file az új vevőkről elkészült ...<br>";
            } else {
                echo "Hiba történt, az XML file a vevőkről nem készült el...<br>";
            }
        }
    }



}
?>