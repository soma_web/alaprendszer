<?php
// Text
$_['text_subject']  = '%s - Új jelszó';
$_['text_dear_customer'] = 'Kedves Vásárlónk!<br><br>';
$_['text_greeting'] = 'Weboldalunkon - <a href="%s">%s</a> - erre az e-mail címre jelszó emlékeztetőt igényeltek.<br>';
$_['text_password'] = 'A továbbiakban <strong>%s</strong> jelszóval tud belépni online áruházunkba. <br>Ezt az automatikusan generált jelszót az első bejelentkezéskor meg kell változtatni!';
?>