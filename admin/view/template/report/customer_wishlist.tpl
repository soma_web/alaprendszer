<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="nyomtat">
            <a onclick="window.print();" class="button"><?php echo $button_nyomtatas; ?></a>
            <a href='<?php echo $csv_be; ?>' class="button"><?php echo $button_exportalas; ?></a>
        </div>
    </div>

      <div class="content">
      <table class="list">
        <thead>
          <tr>
            <td class="left" style="max-width: 250px"><?php echo $column_product_name; ?></td>
            <td class="left"><?php echo $column_product_model; ?></td>
            <td class="left"><?php echo $column_product_price; ?></td>
            <td class="left"><?php echo $column_product_quantity; ?></td>
            <td class="left"><?php echo $column_total_customer; ?></td>
            <td class="right"><?php echo $column_action; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($products) { ?>
          <?php foreach ($products as $product) { ?>
              <tr>
                <td class="left"  style="max-width: 250px"><?php echo $product['product_name']; ?></td>
                <td class="left" style="position: relative"><?php echo $product['product_model']; ?>
                    <table style="display: none" class='ugyfelek' id="ugyfelek_<?php echo $product['product_id']?>">
                        <thead>
                            <tr>
                                <td><?php echo $column_customer?></td>
                                <td><?php echo $column_email?></td>
                                <td><?php echo $column_customer_group?></td>
                            </tr>
                        </thead>
                        <?php foreach ($product['customer'] as $customer) { ?>
                            <tr>
                                <td><?php echo $customer['customer']?></td>
                                <td><?php echo $customer['email']?></td>
                                <td><?php echo $customer['customer_group']?></td>
                            </tr>
                        <?php } ?>
                            <tr>
                                <td class="right" colspan="3">
                                    <a class="button" onclick="$('.ugyfelek').css('display','none');">Bezár</a>
                                </td>
                    </table>
                </td>
                <td class="left"><?php echo $product['product_price']; ?></td>
                <td class="left"><?php echo $product['product_quantity']; ?></td>
                <td class="left"><?php echo $product['total_customer']; ?>
                </td>
                <td class="right">
                  [ <a onclick="ugyfelMegmutat('<?php echo $product['product_id']?>');"><?php echo $product['action']['text']; ?></a> ]
                </td>

              </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<?php echo $footer; ?>

<script>
    function ugyfelMegmutat(product_id) {
        if ( $('#ugyfelek_'+product_id).css('display') == 'table') {
            $('.ugyfelek').css('display','none');
        } else {
            $('.ugyfelek').css('display','none');
            $('#ugyfelek_'+product_id).slideDown('slow');
        }
    }

</script>