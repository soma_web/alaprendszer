<div class="compare">
<a title="<?php echo isset($beallitas['osszehasonlitas']) ? $button_compare : ''; ?>" onclick="addToCompare('<?php echo $product['product_id']; ?>');">
    <?php if (isset($beallitas['osszehasonlitas_ikon']) && $beallitas['osszehasonlitas_ikon']) { ?>
        <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/compare_icon.png')) {
            $icon = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/compare_icon.png';
        } else if(file_exists(DIR_TEMPLATE_IMAGE.'default/image/compare_icon.png')) {
            $icon = DIR_TEMPLATE_IMAGE.'default/image/compare_icon.png';
        } else {
            $icon = "";
        }?>
        <img class="product-icon-compare" title="<? if (isset($button_compare)) { echo $button_compare; }?>"  src="<? echo $icon?>" >
    <?php }  ?>
    <?php if (!isset($beallitas['osszehasonlitas'])) { ?>
        <?php echo $button_compare; ?>
    <?php }  ?>
</a>
</div>
