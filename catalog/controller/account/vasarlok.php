<?php
class ControllerAccountVasarlok extends Controller {
    public function index() {
        $this->load->language('account/megnezett');

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', '', 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('account/megnezett', $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );


        $data = array(
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );


        $this->load->model('account/megnezett');
        $cOrderID = $this->model_account_megnezett->getcustormProduct();

        $product_viewed_total = $this->model_account_megnezett->getTotalProductsViewed($cOrderID);

        $product_views_total = $this->model_account_megnezett->getTotalProductViews($cOrderID);

        $this->data['products'] = array();

        $results = $this->model_account_megnezett->getProductsViewed($data,$cOrderID);

        foreach ($results as $result) {
            if ($result['viewed']) {
                $percent = round($result['viewed'] / $product_views_total * 100, 2);
            } else {
                $percent = 0;
            }

            $this->data['products'][] = array(
                'name'    => $result['name'],
                'model'   => $result['model'],
                'viewed'  => $result['viewed'],
                'percent' => $percent . '%'
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['column_name']      = $this->language->get('column_name');
        $this->data['column_model']     = $this->language->get('column_model');
        $this->data['column_viewed']    = $this->language->get('column_viewed');
        $this->data['column_percent']   = $this->language->get('column_percent');

        $this->data['button_reset']     = $this->language->get('button_reset');

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['reset'] = $this->url->link('account/megnezett/reset', $url, 'SSL');

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $pagination = new Pagination();
        $pagination->total = $product_viewed_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('account/megnezett', '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/megnezett.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/megnezett.tpl';
        } else {
            $this->template = 'default/template/account/megnezett.tpl';
        }


        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }

    public function reset() {
        $this->load->language('account/megnezett');

        $this->load->model('account/megnezett');
        $cOrderID = $this->model_account_megnezett->getcustormProduct();
        $this->model_account_megnezett->reset($cOrderID);

        $this->session->data['success'] = $this->language->get('text_success');

        $this->redirect($this->url->link('account/megnezett','', 'SSL'));
    }

}
?>
