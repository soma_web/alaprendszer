<?php
class ModelCsvKapcsolo extends Model {

    private $ertek=array();
    private $id;
    private $tabla;
    private $tablak;
    private $nyelvek=array();
    private $store;

    public function getFields($table) {

        $this->load->model("setting/store");
        $stores = $this->model_setting_store->getStores();

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX.$table);
        $product = array();




        foreach($oszlop->rows as $curoszlop) {
            $product[] = DB_PREFIX.$table.'.'.$curoszlop['Field'];
            if ($table == "option_szin_to_group") {
                if ($curoszlop['Field'] == 'option_szin_group_id') {
                    $product[] = DB_PREFIX.'option_szin_group_description.name';
                } elseif ($curoszlop['Field'] == 'option_szin_id') {
                    $product[] = DB_PREFIX.'option_szin_description.name';

                }
            }
        }


        $product[] = "Delete";




        return $product;
    }

    public function backup($tabla) {
        date_default_timezone_set('Europe/Budapest');

        $this->load->model("localisation/language");
        $nyelvek = $this->model_localisation_language->getLanguages();
        $filename = $tabla.'_'.date('Y-m-d_H:i:s', time()).'.csv';

        $sql = "SELECT ";

        $products   = array();
        $fejlec     = array();
        foreach($this->request->post['backup'] as $value) {
            if (strstr($value,".",1) == DB_PREFIX.$tabla) {
                $products[] = $value;
                $fejlec[]   = $tabla.'.'.substr(strstr($value,".",0),1);
            } elseif (strstr($value,".",1) == DB_PREFIX.$tabla."_description") {
                foreach($nyelvek as $nyelv) {
                    $products[] = "(SELECT ".$value." FROM ".DB_PREFIX.$tabla."_description WHERE ".DB_PREFIX.$tabla."_description.".$tabla."_id=".DB_PREFIX.$tabla.".".$tabla."_id AND language_id=".$nyelv['language_id'].") AS ".$nyelv['name'].'_'.substr(strstr($value,".",0),1);
                    $fejlec[]   = $tabla.'_description.'.substr(strstr($value,".",0),1).'-'.$nyelv['name'];

                }
            } elseif ($value == "Delete") {
                $products[] = "'N'";
                $fejlec[] = "Delete";
            } elseif ($tabla == 'option_szin_to_group') {
                $products[] = "(SELECT ".$value." FROM ".strstr($value,".",1)." WHERE "
                    .strstr($value,".",1).".".str_replace('description','id',strstr(str_replace(DB_PREFIX,'',$value),".",1))."=".DB_PREFIX.$tabla.".".str_replace('description','id',strstr(str_replace(DB_PREFIX,'',$value),".",1)).
                    " AND language_id=".$this->config->get('config_language_id').") AS ".str_replace(DB_PREFIX,'',strstr($value,'.',1))."_name";
                $fejlec[]   = str_replace(DB_PREFIX,'',strstr($value,".",1)).".name";

            }
        }


        $sql .= implode(',',$products);


        $sql .= " FROM ".DB_PREFIX.$tabla." WHERE 1";

        //SELECT `firstname` FROM `hungaro_customer` WHERE lcase(`firstname`) > 'b' AND substring(lcase(`firstname`),1,2) <= 'me' ORDER BY lcase(`firstname`) asc
        if (!empty($this->request->post['filter'])) {
            foreach($this->request->post['filter'] as $filter_key=>$value) {
                $mezo = explode('-',$filter_key);
                $present = substr(strstr($mezo[0],".",0),1);



                if ($mezo[1] == "tol" && $value){
                    if (ctype_digit($value)) {
                        $sql .=" AND ".$mezo[0]." >= ".$value;
                    } else {
                        $sql .=" AND LCASE(".$mezo[0].") LIKE '%" . $this->db->escape(utf8_strtolower($value)) . "%'";
                        //$sql .=" AND LCASE(".$mezo[0].") LIKE '%" . $this->db->escape(utf8_strtolower($value)) . "%'";
                    }

                } elseif ($mezo[1] == "ig" && $value ){
                    if (ctype_digit($value)) {
                        $sql .=" AND ".$mezo[0]." <= ".$value;
                    } else {
                        $sql .=" AND LCASE(".$mezo[0].") LIKE '%" . $this->db->escape(utf8_strtolower($value)) . "%'";
                    }

                } elseif ($mezo[1] == "select" && $value != "no" ){
                    $sql .=" AND ".$mezo[0]." = ".$value;
                }
            }
        }
        //$sql .= " ORDER BY ".DB_PREFIX.$tabla.".".$tabla."_id";
        $sql .= " ORDER BY ".DB_PREFIX.$fejlec[0];

        $sql .= " LIMIT ".(int)$this->request->post['limit_tol'].",";
        $sql .= $this->request->post['limit_db'] ? $this->request->post['limit_db'] : 1844674407370955161;
        $query = $this->db->query($sql);


        if ($query->num_rows > 0) {
            $handle = fopen('../csv/'.$filename, "w");
            //fputcsv ( $handle, $fejlec, ";", '"' ,"\\" );
            fputcsv ( $handle, $fejlec, ";", '"');

            foreach($query->rows as $fields) {
                foreach($fields as $key=>$value) {
                    $value = str_ireplace(array("\r","\n","\t",'"','','','','\\"'),'', $value);
                    $fields[$key] = $value;
                }
                //fputcsv ( $handle, $fields, ";", '"' ,"\\" );
                fputcsv ( $handle, $fields, ";", '"');
            }
            fclose($handle);

        }
        return '../csv/'.$filename;
    }

    public function restore($csv) {

        require_once(DIR_SYSTEM."library/Encoding.php");
        $encode = new Encoding();
        $csv = $encode->toUTF8($csv);

        $this->load->model("localisation/language");
        $this->nyelvek = $this->model_localisation_language->getLanguages();

        $sorok = str_getcsv($csv, "\n");
        $fejlec = str_getcsv($sorok[0], ";",'"','\\');

        $this->tabla = strstr($fejlec[0],".",1);

        $sql = "SHOW TABLES";
        $query = $this->db->query($sql);
        $this->store = false;
        foreach($query->rows as $value) {
            if (in_array(DB_PREFIX.$this->tabla."_to_store",$value)) {
                $this->store = true;
                break;
            }
        }


        if (!$this->formatumEllenorzes($fejlec)) return false;

        $van_torles = false;
        if ($torol_elem = array_keys($fejlec,"Delete")) {
            $torol_elem = $torol_elem[0];
            $van_torles = true;
        }
        $this->load->model("catalog/product");

        foreach($fejlec as $key=>$value) {
            if ($value != "Delete") {
                $this->tablak[strstr($value,".",1)][substr(strstr($value,".",0),1)] = $key;
            }
        }

        $i = 0;
        $sorok_szama = 0;
        foreach($sorok as $sor) {
            if ($i == 0) {
                $i++;
                continue;
            }
            if (!empty($sor)) {
                $this->ertek = str_getcsv($sor, ";",'"','\\');
                if ($van_torles &&  (strtoupper($this->ertek[$torol_elem]) == "I" || strtoupper($this->ertek[$torol_elem]) == "Y") ) {
                    $this->sorTorles($fejlec);
                    continue;
                }
                $this->letezesVizsgalat($fejlec);
            }
            $sorok_szama++;
        }

        return $sorok_szama;
    }

    public function letezesVizsgalat($fejlec) {

        $sql = "SELECT ".DB_PREFIX.$fejlec[0]." FROM ".DB_PREFIX.strstr($fejlec[0],'.',1)." WHERE ".DB_PREFIX.$fejlec[0]."='".$this->ertek[0]."' AND ".DB_PREFIX.$fejlec[2]."='".$this->ertek[2]."'";
        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $sql = "INSERT INTO ".DB_PREFIX.strstr($fejlec[0],'.',1)." SET ";
            $sql .= substr(strstr($fejlec[0],'.'),1)."= '%s',";
            $sql .= substr(strstr($fejlec[2],'.'),1)."= '%s'";
            $sql = sprintf($sql,$this->ertek[0],$this->ertek[2]);
            $query = $this->db->query($sql);
        }


        for ($i=0; $i < 3; $i=$i+2) {
            $sql = "SELECT ".substr(strstr($fejlec[$i],'.',0),1)." FROM ".DB_PREFIX.str_replace('_id','',substr(strstr($fejlec[$i],'.',0),1));
            $sql .= " WHERE ".substr(strstr($fejlec[$i],'.',0),1)."='".$this->ertek[$i]."'";
            $query = $this->db->query($sql);
            if ($query->num_rows == 0) {
                $sql = "INSERT INTO ".DB_PREFIX.str_replace('_id','',substr(strstr($fejlec[$i],'.',0),1));
                $sql .= " SET ".substr(strstr($fejlec[$i],'.',0),1)."='".$this->ertek[$i]."'";
                $query = $this->db->query($sql);
            }

            $sql = "SELECT ".DB_PREFIX.$fejlec[$i+1]." FROM ".DB_PREFIX.strstr($fejlec[$i+1],'.',1);
            $sql .= " WHERE ".substr(strstr($fejlec[$i],'.',0),1)."='".$this->ertek[$i]."' AND language_id='".$this->config->get('config_language_id')."'";
            $query = $this->db->query($sql);
            if ($query->num_rows == 0) {
                $sql1 = "INSERT INTO ".DB_PREFIX.strstr($fejlec[$i+1],'.',1)." SET ";
                $sql1 .= substr(strstr($fejlec[$i],'.',0),1)."='".$this->ertek[$i]."',";
                $sql1 .= " language_id='".$this->config->get('config_language_id')."',";
                $sql1 .= substr(strstr($fejlec[$i+1],'.',0),1)."='".$this->ertek[$i+1]."'";
                $query = $this->db->query($sql1);

            } else {
                $sql = " UPDATE ".DB_PREFIX.strstr($fejlec[$i+1],'.',1)." SET ";
                $sql .= DB_PREFIX.$fejlec[$i+1]."='".$this->ertek[$i+1]."'";
                $sql .= " WHERE ".substr(strstr($fejlec[$i],'.',0),1)."='".$this->ertek[$i]."' AND language_id=".$this->config->get('config_language_id');;
                $query = $this->db->query($sql);

            }
        }

        return true;
    }

    public function sorTorles($fejlec) {
        $sql = "DELETE FROM ".DB_PREFIX.strstr($fejlec[0],'.',1)." WHERE ".DB_PREFIX.$fejlec[0]."='".$this->ertek[0]."' AND ".DB_PREFIX.$fejlec[2]."='".$this->ertek[2]."'";
        $query = $this->db->query($sql);
    }

    public function formatumEllenorzes($fejlec) {

        $sql = "SHOW TABLES";
        $query = $this->db->query($sql);
        $mehet = false;
        $description = false;
        $i = 1;
        $count = count($fejlec);
        foreach($fejlec as $tabla) {
            if ($tabla == "Delete") {
                $mehet = true;
            } else {
                foreach($query->rows as $value) {
                    if (in_array(DB_PREFIX.strstr($tabla,'.',1),$value)) {
                        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX.strstr($tabla,'.',1));
                        foreach($oszlop->rows as $curoszlop) {
                            if ($curoszlop['Field'] == substr(strstr($tabla,'.',0),1)) {
                                $mehet = true;
                                break;
                            }
                        }
                        if ($mehet) {
                            break;
                        }
                    }
                }
            }
            if (!$mehet) {
                break;
            } elseif ($i < $count) {
                $mehet = false;
            }
            $i++;
        }
        $vissza = false;

        return $mehet;
    }

}