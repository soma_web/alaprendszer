<?php
$this->load->language('vatera/vatera');
$this->data['tab_vatera'] = $this->language->get('tab_vatera');


$this->data['vat_automatikus_kuldes']           = $this->language->get('vat_automatikus_kuldes');
$this->data['vat_vazonosito']                   = $this->language->get('vat_vazonosito');
$this->data['vat_termeknev']                    = $this->language->get('vat_termeknev');
$this->data['vat_alcim']                        = $this->language->get('vat_alcim');
$this->data['vat_alcim_help']                   = $this->language->get('vat_alcim_help');
$this->data['vat_darabszam']                    = $this->language->get('vat_darabszam');
$this->data['vat_darabszam_help']               = $this->language->get('vat_darabszam_help');
$this->data['vat_kikialtasiar']                 = $this->language->get('vat_kikialtasiar');
$this->data['vat_minimalar']                    = $this->language->get('vat_minimalar');
$this->data['vat_villamar']                     = $this->language->get('vat_villamar');
$this->data['vat_villamar_help']                = $this->language->get('vat_villamar_help');
$this->data['vat_ar_help']                      = $this->language->get('vat_ar_help');
$this->data['vat_licitlepcso']                  = $this->language->get('vat_licitlepcso');
$this->data['vat_licitlepcso_help']             = $this->language->get('vat_licitlepcso_help');
$this->data['vat_aukcio_inditasa']              = $this->language->get('vat_aukcio_inditasa');
$this->data['vat_aukcio_inditasa_help']         = $this->language->get('vat_aukcio_inditasa_help');
$this->data['vat_aukcio_idotartam']             = $this->language->get('vat_aukcio_idotartam');
$this->data['vat_aukcio_idotartam_help']        = $this->language->get('vat_aukcio_idotartam_help');
$this->data['vat_vatera_kategoria_kod']         = $this->language->get('vat_vatera_kategoria_kod');
$this->data['vat_vatera_kategoria_kod_help']    = $this->language->get('vat_vatera_kategoria_kod_help');
$this->data['vat_megye']                        = $this->language->get('vat_megye');
$this->data['vat_orszag']                       = $this->language->get('vat_orszag');
$this->data['vat_sablon_azonosito']             = $this->language->get('vat_sablon_azonosito');
$this->data['vat_sablon_azonosito_help']        = $this->language->get('vat_sablon_azonosito_help');
$this->data['vat_ujrainditasok_szama']          = $this->language->get('vat_ujrainditasok_szama');
$this->data['vat_ujrainditasok_szama_help']     = $this->language->get('vat_ujrainditasok_szama_help');
$this->data['vat_kiemelesek']                   = $this->language->get('vat_kiemelesek');
$this->data['vat_kiemelesek_help']              = $this->language->get('vat_kiemelesek_help');
$this->data['vat_termek_jellemzok']             = $this->language->get('vat_termek_jellemzok');
$this->data['vat_termek_jellemzok_help']        = $this->language->get('vat_termek_jellemzok_help');
$this->data['vat_garancia']                     = $this->language->get('vat_garancia');
$this->data['vat_garancia_help']                = $this->language->get('vat_garancia_help');
$this->data['vat_elfogadas_szazalek']           = $this->language->get('vat_elfogadas_szazalek');
$this->data['vat_elfogadas_szazalek_help']      = $this->language->get('vat_elfogadas_szazalek_help');
$this->data['vat_kepek1']                       = $this->language->get('vat_kepek1');
$this->data['vat_kepek2']                       = $this->language->get('vat_kepek2');
$this->data['vat_kepek3']                       = $this->language->get('vat_kepek3');
$this->data['vat_kepek4']                       = $this->language->get('vat_kepek4');
$this->data['vat_kinezeti_sablon']              = $this->language->get('vat_kinezeti_sablon');
$this->data['vat_kinezeti_sablon_help']         = $this->language->get('vat_kinezeti_sablon_help');
$this->data['vat_product_id']                   = $this->language->get('vat_product_id');
$this->data['vat_kategoria_specifikus']         = $this->language->get('vat_kategoria_specifikus');
$this->data['vat_kategoria_specifikus_help']    = $this->language->get('vat_kategoria_specifikus_help');
$this->data['vat_kepek5']                       = $this->language->get('vat_kepek5');
$this->data['vat_kepek6']                       = $this->language->get('vat_kepek6');
$this->data['vat_kepek7']                       = $this->language->get('vat_kepek7');
$this->data['vat_kepek8']                       = $this->language->get('vat_kepek8');
$this->data['vat_leiras']                       = $this->language->get('vat_leiras');
$this->data['vat_leiras_help']                  = $this->language->get('vat_leiras_help');

if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
$product_vatera = $this->model_catalog_product->getProductvatera($this->request->get['product_id']);
}


$this->data['vatvazonosito'] = VATERA_AZONOSITO_KOD;

if (isset($this->request->post['vattermeknev'])) {
    $this->data['vattermeknev'] = $this->request->post['vattermeknev'];
} elseif (!empty($product_info)) {
    $this->data['vattermeknev'] = $product_info['name'];
} else {
    $this->data['vattermeknev'] = '';
}

//$this->data['vattermeknev'] = $product_info['name'];

if (isset($this->request->post['vatalcim'])) {
$this->data['vatalcim'] = $this->request->post['vatalcim'];
} elseif (!empty($product_vatera)) {
$this->data['vatalcim'] = $product_vatera[0]['alcim'];
} else {
$this->data['vatalcim'] = '';
}

if (isset($this->request->post['vatdarabszam'])) {
    $this->data['vatdarabszam'] = $this->request->post['vatdarabszam'];
} elseif (!empty($product_info)) {
    $this->data['vatdarabszam'] = $product_info['quantity'];
} else {
    $this->data['vatdarabszam'] = '';
}
//$this->data['vatdarabszam'] = $product_info['quantity'];

if (isset($this->request->post['vatkikialtasiar'])) {
$this->data['vatkikialtasiar'] = $this->request->post['vatkikialtasiar'];
} elseif (!empty($product_vatera)) {
$this->data['vatkikialtasiar'] = $product_vatera[0]['kikialtasiar'];
} else {
$this->data['vatkikialtasiar'] = '';
}
if (isset($this->request->post['vatminimalar'])) {
$this->data['vatminimalar'] = $this->request->post['vatminimalar'];
} elseif (!empty($product_vatera)) {
$this->data['vatminimalar'] = $product_vatera[0]['minimalar'];
} else {
$this->data['vatminimalar'] = '';
}
if (isset($this->request->post['vatvillamar'])) {
$this->data['vatvillamar'] = $this->request->post['vatvillamar'];
} elseif (!empty($product_vatera)) {
$this->data['vatvillamar'] = $product_vatera[0]['villamar'];
} else {
$this->data['vatvillamar'] = '';
}
if (isset($this->request->post['vatlicitlepcso'])) {
$this->data['vatlicitlepcso'] = $this->request->post['vatlicitlepcso'];
} elseif (!empty($product_vatera)) {
$this->data['vatlicitlepcso'] = $product_vatera[0]['licitlepcso'];
} else {
$this->data['vatlicitlepcso'] = '';
}
if (isset($this->request->post['vataukcioinditasa'])) {
$this->data['vataukcioinditasa'] = $this->request->post['vataukcioinditasa'];
} elseif (!empty($product_vatera)) {
$this->data['vataukcioinditasa'] = $product_vatera[0]['aukcio_inditasa'];
} else {
$this->data['vataukcioinditasa'] = '';
}
if (isset($this->request->post['vataukcioidotartam'])) {
$this->data['vataukcioidotartam'] = $this->request->post['vataukcioidotartam'];
} elseif (!empty($product_vatera)) {
$this->data['vataukcioidotartam'] = $product_vatera[0]['aukcio_idotartam'];
} else {
$this->data['vataukcioidotartam'] = '';
}
if (isset($this->request->post['vatvaterakategoriakod'])) {
$this->data['vatvaterakategoriakod'] = $this->request->post['vatvaterakategoriakod'];
} elseif (!empty($product_vatera)) {
$this->data['vatvaterakategoriakod'] = $product_vatera[0]['vatera_kategoria_kod'];
} else {
$this->data['vatvaterakategoriakod'] = '';
}

if (isset($this->request->post['vatautomatikus'])) {
    $this->data['vatautomatikus'] = $this->request->post['vatautomatikus'];
} elseif (!empty($product_vatera)) {
    $this->data['vatautomatikus'] = $product_vatera[0]['vat_automatikus'];
} else {
    $this->data['vatautomatikus'] = '';
}


if (isset($this->request->post['vatmegye'])) {
$this->data['vatmegye'] = $this->request->post['vatmegye'];
} elseif (!empty($product_vatera)) {
$this->data['vatmegye'] = $product_vatera[0]['megye'];
} else {
$this->data['vatmegye'] = '';
}

if (isset($this->request->post['vatorszag'])) {
$this->data['vatorszag'] = $this->request->post['vatorszag'];
} elseif (!empty($product_vatera)) {
$this->data['vatorszag'] = $product_vatera[0]['orszag'];
} else {
$this->data['vatorszag'] = '';
}

if (isset($this->request->post['vatsablonazonosito'])) {
$this->data['vatsablonazonosito'] = $this->request->post['vatsablonazonosito'];
} elseif (!empty($product_vatera)) {
$this->data['vatsablonazonosito'] = $product_vatera[0]['sablon_azonosito'];
} else {
$this->data['vatsablonazonosito'] = '';
}
if (isset($this->request->post['vatujrainditasokszama'])) {
$this->data['vatujrainditasokszama'] = $this->request->post['vatujrainditasokszama'];
} elseif (!empty($product_vatera)) {
$this->data['vatujrainditasokszama'] = $product_vatera[0]['ujrainditasok_szama'];
} else {
$this->data['vatujrainditasokszama'] = '';
}
if (isset($this->request->post['vatkiemelesek'])) {
$this->data['vatkiemelesek'] = $this->request->post['vatkiemelesek'];
} elseif (!empty($product_vatera)) {
$this->data['vatkiemelesek'] = $product_vatera[0]['kiemelesek'];
} else {
$this->data['vatkiemelesek'] = '';
}
if (isset($this->request->post['vattermekjellemzok'])) {
$this->data['vattermekjellemzok'] = $this->request->post['vattermekjellemzok'];
} elseif (!empty($product_vatera)) {
$this->data['vattermekjellemzok'] = $product_vatera[0]['termek_jellemzok'];
} else {
$this->data['vattermekjellemzok'] = '';
}

if (isset($this->request->post['vatgarancia'])) {
$this->data['vatgarancia'] = $this->request->post['vatgarancia'];
} elseif (!empty($product_vatera)) {
$this->data['vatgarancia'] = $product_vatera[0]['garancia'];
} else {
$this->data['vatgarancia'] = '';
}
if (isset($this->request->post['vatelfogadasszazalek'])) {
$this->data['vatelfogadasszazalek'] = $this->request->post['vatelfogadasszazalek'];
} elseif (!empty($product_vatera)) {
$this->data['vatelfogadasszazalek'] = $product_vatera[0]['elfogadas_szazalek'];
} else {
$this->data['vatelfogadasszazalek'] = '';
}

    for ($i=0; $i < 8; $i++){
        $kepekvat = "vatkepek".(string)($i+1);
        if (!empty($product_info["image"]) && $i==0){
            $this->data[$kepekvat] = HTTP_IMAGE.$product_info["image"];
        } else{
            if (!empty($product_images[$i]["image"]) )
                $this->data[$kepekvat] = HTTP_IMAGE.$product_images[$i]["image"];
            else   $this->data[$kepekvat] = "";
        }
    }


if (isset($this->request->post['vatkinezetisablon'])) {
$this->data['vatkinezetisablon'] = $this->request->post['vatkinezetisablon'];
} elseif (!empty($product_vatera)) {
$this->data['vatkinezetisablon'] = $product_vatera[0]['kinezeti_sablon'];
} else {
$this->data['vatkinezetisablon'] = '';
}

if (isset($this->request->post['vatproductid'])) {
    $this->data['vatproductid'] = $this->request->post['vatproductid'];
} elseif (!empty($product_info)) {
    $this->data['vatproductid'] = $product_info['product_id'];
} else {
    $this->data['vatproductid'] = '';
}

//$this->data['vatproductid'] = $product_info['product_id'];

if (isset($this->request->post['vatkategoriaspecifikus'])) {
$this->data['vatkategoriaspecifikus'] = $this->request->post['vatkategoriaspecifikus'];
} elseif (!empty($product_vatera)) {
$this->data['vatkategoriaspecifikus'] = $product_vatera[0]['kategoria_specifikus'];
} else {
$this->data['vatkategoriaspecifikus'] = '';
}

if (isset($this->request->post['vatleiras'])) {
$this->data['vatleiras'] = $this->request->post['vatleiras'];
} elseif (!empty($product_vatera)) {
$this->data['vatleiras'] = $product_vatera[0]['leiras'];
} else {
$this->data['vatleiras'] = '';
}

$this->load->language('catalog/product');

?>