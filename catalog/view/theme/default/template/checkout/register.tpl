<div class="left">
    <h2><?php echo $text_your_details; ?></h2>
    <?php if (($this->config->get('megjelenit_regisztracio_vezeteknev') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_vezeteknev') == 1 && $ceges == 1 )) {?>
        <span class="required">*</span> <?php echo $entry_firstname; ?><br />
        <input type="text" name="firstname" value="" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_keresztnev') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_keresztnev') == 1 && $ceges == 1 )) {?>
        <span class="required">*</span> <?php echo $entry_lastname; ?><br />
        <input type="text" name="lastname" value="" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_email') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_email') == 1 && $ceges == 1 )) {?>
        <span class="required">*</span> <?php echo $entry_email; ?><br />
        <input type="text" name="email" value="" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_email_megerosites') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_email_megerosites') == 1 && $ceges == 1 )) {?>
        <span class="required">*</span> <?php echo $entry_email_again; ?><br />
        <input type="text" name="email_again" value="" class="large-field" />
        <br />
        <br />
    <?php } else { ?>
        <input type="hidden" name="email_again" value="" />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_telefon') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_telefon') == 1 && $ceges == 1 )) {?>
        <span class="required">*</span> <?php echo $entry_telephone; ?><br />
        <input type="text" name="telephone" value="" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_fax') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_fax') == 1 && $ceges == 1 )) {?>
        <?php echo $entry_fax; ?><br />
        <input type="text" name="fax" value="" class="large-field" />
        <br />
        <br />
    <?php } else { ?>
        <input type="hidden" name="fax" value="" />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_eletkor_regisztracio') == 1 && $ceges == 0) || ($this->config->get('megjelenit_eletkor_regisztracio_ceges') == 1 && $ceges == 1) ) {?>

        <span class="required">*</span> <?php echo $entry_eletkor; ?><br />
        <select name="eletkor" class="large-field">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($eletkors as $eletkora) { ?>
                <?php if ($eletkora['megnevezes'] == $eletkor) { ?>
                    <option value="<?php echo $eletkora['megnevezes']; ?>" selected="selected"><?php echo $eletkora['megnevezes']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $eletkora['megnevezes']; ?>"><?php echo $eletkora['megnevezes']; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_nem_regisztracio') == 1 && $ceges == 0 ) || ($this->config->get('megjelenit_nem_regisztracio_ceges') == 1 && $ceges == 1 ) ) {?>
        <span class="required">*</span> <?php echo $entry_nem; ?><br />
        <select name="nem"  class="large-field">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($nems as $value) { ?>
                <?php if ($value['ertek'] == $nem) { ?>
                    <option value="<?php echo $value['ertek']; ?>" selected="selected"><?php echo $value['nev']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $value['ertek']; ?>"><?php echo $value['nev']; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_iskolai_vegzettseg') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_iskolai_vegzettseg') == 1 && $ceges == 1 )) {?>

        <?php echo $entry_iskolai_vegzettseg; ?><br />
        <select name="iskolai_vegzettseg" class="large-field">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($iskolai_vegzettsegek as $value) { ?>
                <option value="<?php echo $value['megnevezes']; ?>"><?php echo $value['megnevezes']; ?></option>
            <?php } ?>
        </select>
        <br />
        <br />
    <?php } ?>

    <?php if ($this->config->get('megjelenit_regisztracio_weblap') == 1 ) { ?>
        <?php echo $entry_weblap; ?><br />
        <input type="text" name="weblap" value="" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <br />
    <br />
    <br />

    <h2><?php echo $text_your_password; ?></h2>
    <span class="required">*</span> <?php echo $entry_password; ?><br />
    <input type="password" name="password" value="" class="large-field" />
    <br />
    <br />
    <span class="required">*</span> <?php echo $entry_confirm; ?> <br />
    <input type="password" name="confirm" value="" class="large-field" />
    <br />
    <br />
    <br />
</div>
<div class="right">

    <?php if ( ($this->config->get('megjelenit_regisztracio_cegnev') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_cegnev') == 1 && $ceges == 1) ) {?>
        <h2><?php echo $text_your_address; ?></h2>
        <?php echo $entry_company; ?><br />
        <input type="text" name="company" value="" class="large-field" />
        <br />
        <br />
    <?php } else { ?>
        <input type="hidden" name="company" value=""  />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_adoszam') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_adoszam') == 1 && $ceges == 1) ) {?>
        <?php echo $entry_adoszam; ?><br />
        <input type="text" name="adoszam" value="" class="large-field" />
        <br />
        <br />
    <?php } else { ?>
        <input type="hidden" name="adoszam" value=""  />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_utca') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_utca') == 1 && $ceges == 1) ) {?>
        <span class="required">*</span> <?php echo $entry_address_1; ?><br />
        <input type="text" name="address_1" value="" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_cimadat_kiegeszito') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_cimadat_kiegeszito') == 1 && $ceges == 1) ) {?>
        <?php echo $entry_address_2; ?><br />
        <input type="text" name="address_2" value="" class="large-field" />
        <br />
        <br />
    <?php } else { ?>
        <input type="hidden" name="address_2" value=""  />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_varos') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_varos') == 1 && $ceges == 1) ) {?>
        <span class="required">*</span> <?php echo $entry_city; ?><br />
        <input type="text" name="city" value="" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_iranyitoszam') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_iranyitoszam') == 1 && $ceges == 1) ) {?>
        <span class="required">*</span> <?php echo $entry_postcode; ?><br />
        <input type="text" name="postcode" value="" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_orszag') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_orszag') == 1 && $ceges == 1) ) {?>
        <span class="required">*</span> <?php echo $entry_country; ?><br />
        <select name="country_id" class="large-field" onchange="$('#payment-address select[name=\'zone_id\']').load('index.php?route=checkout/register/zone&country_id=' + this.value);">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $country_id) { ?>
                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_megye') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_megye') == 1 && $ceges == 1) ) {?>
        <span class="required">*</span> <?php echo $entry_zone; ?><br />
        <select name="zone_id" class="large-field">
        </select>
        <br />
        <br />
        <br />
    <?php } ?>
</div>
<input type="hidden" name="ceges" value="<?php echo $ceges ?>">
<div style="clear: both; padding-top: 15px; border-top: 1px solid #EEEEEE;">
    <?php if (($this->config->get('megjelenit_regisztracioblokk_hirlevel') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracioblokk_ceges_hirlevel') == 1 && $ceges == 1 )) {?>
        <input type="checkbox" name="newsletter" value="1" id="newsletter" />
        <label for="newsletter"><?php echo $entry_newsletter; ?></label>
        <br />
        <?php if ( ($this->config->get('megjelenit_regisztracio_hirlevel_kategoriak') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_hirlevel_kategoriak') == 1 && $ceges == 1) ) {?>
            <div id="newslettercategories" class="newslettertd" style="display: none">
                <?php foreach($categories as $category) { ?>
                    <input class="css-checkbox" type="checkbox" name="newslettercategories[]" value="<?php echo $category['category_id']; ?>" /><?php echo $category['name']; ?><br />
                <?php } ?>
            </div>
        <?php } ?>
        <br />
    <?php } ?>
    <?php if ($shipping_required) { ?>
        <input type="checkbox" name="shipping_address" value="1" id="shipping" checked="checked" />
        <label for="shipping"><?php echo $entry_shipping; ?></label>
        <br />
    <?php } ?>
    <br />
    <br />
</div>
<?php if ($text_agree) { ?>
    <div class="buttons">
        <div class="teljes"><?php echo $text_agree; ?>
            <input type="checkbox"  class="agree" name="agree" value="1" />
            <input type="button" value="<?php echo $button_continue; ?>" id="button-register" class="button" />
        </div>
    </div>
<?php } else { ?>
    <div class="buttons">
        <div class="teljes"><input type="button" value="<?php echo $button_continue; ?>" id="button-register" class="button" /></div>
    </div>
<?php } ?>
<script type="text/javascript"><!--
    $('#payment-address select[name=\'zone_id\']').load('index.php?route=checkout/register/zone&country_id=<?php echo $country_id; ?>');
    //--></script>
<script type="text/javascript"><!--
    /*$('.colorbox').colorbox({
        width: 560,
        height: 560
    });*/
    //--></script>

<script type="text/javascript">
    $('#newsletter').on('change', function() {
        if (this.checked) {
            $('#newslettercategories').show();
        } else {
            $('#newslettercategories').hide();
        }
    });
</script>
<script type="text/javascript">
    $("[name=postcode]").autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: 'index.php?route=account/address/getpostcode&part=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.text,
                            value: item.id
                        }
                    }));
                }
            });
        },
        search: function(event) {
        },
        focus: function() {
        },
        select: function( event, ui ) {
            $.ajax({
                url: 'index.php?route=account/address/getcity&postcodeid=' +  encodeURIComponent(ui.item.value),
                dataType: 'json',
                success: function(json) {
                    debugger;
                    if(! json instanceof Object) {
                        return false;
                    }
                    var cityInput = $('[name=city]');
                    var postcode = $("[name=postcode]")
                    //var zoneInput = $('[name=zone_id]');
                    //var countryInput = $('[name=country_id]');
                    if(cityInput != undefined && cityInput.length != 0) {
                        cityInput.val(json.city);
                    }
                    if(postcode != undefined && postcode.length != 0) {
                        postcode.val(json.postcode);
                    }
                    /*
                     if(zoneInput != undefined && zoneInput.length != 0) {
                     var zoneSelect = zoneInput.find('option:contains("'+json['country']+'")');
                     if(zoneSelect != undefined && zoneSelect.length != 0) {
                     zoneSelect[0].selected = 'selected'
                     }
                     }
                     if(countryInput != undefined) {
                     countryInput.find('option:contains("Hungary")')[0].selected = 'selected';
                     }
                     */
                }
            });
            ui.item.value = $('[name=postcode]').val();
        }
    });
</script>