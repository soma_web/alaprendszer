<?php
// Heading 
$_['heading_title']                  = 'Checkout';

// Text
$_['text_brutto']                    = 'Brutto';
$_['text_netto']                     = 'Netto';
$_['text_cart']                      = 'Shopping Cart';
$_['text_checkout_option_number']    = 'Step 1';
$_['text_checkout_account_number']          = 'Step 2';
$_['text_checkout_payment_address_number']  = 'Step 2';
$_['text_checkout_shipping_address_number'] = 'Step 3';
$_['text_checkout_shipping_method_number']  = 'Step 4';
$_['text_checkout_payment_method_number']   = 'Step 5';
$_['text_checkout_confirm_number']          = 'Step 6';
$_['text_checkout_option']           = 'Checkout Options';
$_['text_checkout_account']          = 'Account &amp; Billing Details';
$_['text_checkout_payment_address']  = 'Billing Details';
$_['text_checkout_shipping_address'] = 'Delivery Details';
$_['text_checkout_shipping_method']  = 'Delivery Method';
$_['text_checkout_shipping_method_no_payment']  = 'Delivery and payment Method';
$_['text_checkout_payment_method']   = 'Payment Method';
$_['text_checkout_confirm']          = 'Confirm Order';
$_['text_checkout_confirm_no_payment']   = 'Confirm Order';
$_['text_modify']                    = 'Modify &raquo;';
$_['text_new_customer']              = 'New Customer';
$_['text_returning_customer']        = 'Returning Customer';
$_['text_checkout']                  = 'Checkout Options:';
$_['text_i_am_returning_customer']   = 'I am a returning customer';
$_['text_gls_csomagpontok']          = 'GLS Package Points:';
$_['text_register']                  = 'Register Account';
$_['text_guest']                     = 'Guest Checkout';
$_['text_register_account']          = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_forgotten']                 = 'Forgotten Password';
$_['text_your_details']              = 'Your Personal Details';
$_['text_your_address']              = 'Your Address';
$_['text_your_password']             = 'Your Password';
$_['text_agree']                     = 'Elolvastam és megértettem a feltételeket és az <a class="colorbox" href="%s" alt="%s"><b>%s</b></a>ot!';
$_['total_title']                    = 'All:';

$_['text_agree']                     = 'I have read and agree to the <a class="colorbox" href="%s" alt="%s"><b>%s</b></a>';
$_['text_agree_2']                   = 'I have read and agree to the <a class="colorbox" href="%s" alt="%s"><b>%s</b></a>';
$_['text_address_new']               = 'I want to use a new address';
$_['text_address_existing']          = 'I want to use an existing address';
$_['text_shipping_method']           = 'Please select the preferred shipping method to use on this order.';
$_['text_shipping_method_no_payment'] = 'Please select the method of delivery and payment of your order.';
$_['text_payment_method']            = 'Please select the preferred payment method to use on this order.';
$_['text_comments']                  = 'Add Comments About Your Order';

// Column
$_['column_name']                    = 'Product Name';
$_['column_model']                   = 'Model';
$_['column_cikkszam']	             = 'Item No.';

$_['column_quantity']                = 'Quantity';
$_['column_price']                   = 'Price';
$_['column_total']                   = 'Total';

$_['column_netto']                  = '<span>Unit Price </span></br><span style="font-size: 10px; ">(Before tax)</span>';
$_['column_netto_total_price']      = '<span>Total Price</span></br><span style="font-size: 10px; "> (Before tax)</span>';
$_['column_price_netto_ar']         = '<span>Unit Price</span></br><span style="font-size: 10px; "> (After tax)</span>';
$_['column_total_netto_ar']         = '<span>Total Price</span></br><span style="font-size: 10px; "> (After tax)</span>';
$_['termek_ar_osszes']              = '<span>All</span>';
$_['szallitas']                     = '<span>Transport</span>';
$_['szallitas_netto_ar']            = '<span>Price (Before tax)</span>';
$_['szallitas_brutto_ar']           = '<span>Price (After tax)</span>';

$_['netto_osszes']                     = '<span>Before tax Total</span>';
$_['afa_osszes']            = '<span>Tax Total</span>';
$_['fizetendo_osszes']           = '<span>Price Total</span>';

// Entry
$_['entry_email_address']            = 'E-Mail Address:';
$_['entry_email']                    = 'E-Mail:';
$_['entry_email_again']              = 'E-Mail Confirm:';
$_['entry_password']                 = 'Password:';
$_['entry_confirm']                  = 'Password Confirm:';
$_['entry_firstname']                = 'First Name:';
$_['entry_lastname']                 = 'Last Name:';
$_['entry_telephone']                = 'Telephone:';
$_['entry_fax']                      = 'Fax:';
$_['entry_company']                  = 'Company:';
$_['entry_adoszam']        = 'tax ID:';

$_['entry_szamlazasi_cim'] = 'Payment address:';
$_['entry_szalitasi_cim']  = 'Shipping address:';
$_['entry_megjegyzes']     = 'Comment:';
$_['entry_fizetesi_mod']   = 'Payment method:';
$_['entry_adatok']         = 'Data';

$_['entry_letoltesek']               = 'Download Selected coupons / Print:';
$_['entry_letoltesek_kep']           = 'Please do not change the selected coupons / Print size.';
$_['button_letoltes']                = 'Download';
$_['button_nyomtatas']               = 'Print';

$_['entry_address_1']                = 'Address 1:';
$_['entry_address_2']                = 'Address 2:';
$_['entry_postcode']                 = 'Post Code:';
$_['entry_city']                     = 'City:';
$_['entry_country']                  = 'Country:';
$_['entry_zone']                     = 'Region / State:';
$_['entry_newsletter']               = 'I wish to subscribe to the %s newsletter.';
$_['entry_shipping'] 	             = 'My delivery and billing addresses are the same.';
$_['entry_adoszam']                  = 'Tax number';


// Error
$_['error_warning']                  = 'There was a problem while trying to process your order! If the problem persists please try selecting a different payment method or you can contact the store owner by <a href="%s">clicking here</a>.';
$_['error_login']                    = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_exists']                   = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']                = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']                 = 'Last Name must be between 1 and 32 characters!';
$_['error_email']                    = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']                = 'Telephone must be between 3 and 32 characters!';
$_['error_password']                 = 'Password must be between 3 and 20 characters!';
$_['error_confirm']                  = 'Password confirmation does not match password!';
$_['error_address_1']                = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                     = 'City must be between 2 and 128 characters!';
$_['error_postcode']                 = 'Postcode must be between 2 and 10 characters!';
$_['error_country']                  = 'Please select a country!';
$_['error_zone']                     = 'Please select a region / state!';
$_['error_agree']                    = 'Warning: You must agree to the %s!';
$_['error_address']                  = 'Warning: You must select address!';
$_['error_shipping']                 = 'Warning: Shipping method required!';
$_['error_gls_nincs']                = 'Warning: Select a GSL package point!';
$_['error_no_shipping']              = 'Warning: No Shipping options are available. Please <a href="%s">contact us</a> for assistance!';
$_['error_payment']                  = 'Warning: Payment method required!';
$_['error_no_payment']               = 'Warning: No Payment options are available. Please <a href="%s">contact us</a> for assistance!';

$_['select_ferfi']         = 'Men';
$_['select_no']            = 'Women';
$_['entry_nem']            = 'Sex:';
$_['entry_eletkor']        = 'Age:';
$_['error_eletkor']        = 'Please select the age of';
$_['error_nem']            = 'Please select the gender of';
$_['entry_iskolai_vegzettseg']              = 'Educational attainment:';
$_['error_iskolai_vegzettseg']              = 'Please select the educational attainment of';
?>