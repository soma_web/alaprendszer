<table>
    <tr>
        <td title="megjelenit_header[penznem_kiiras]" style="width: 244px;"><?php echo $entry_penznem_kiiras; ?></td>
        <td style="width: 144px;">
            <?php if ($megjelenit_header['penznem_kiiras']) { ?>
                <input type="radio" name="megjelenit_header[penznem_kiiras]" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="megjelenit_header[penznem_kiiras]" value="0" />
                <?php echo $text_no; ?>
            <?php } else { ?>
                <input type="radio" name="megjelenit_header[penznem_kiiras]" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="megjelenit_header[penznem_kiiras]" value="0" checked="checked" />
                <?php echo $text_no; ?>
            <?php } ?>
        </td>
    </tr>

    <tr>
        <td title="megjelenit_header[modularis]" style="width: 244px;"><?php echo "Moduláris megjelenítés:"; ?></td>
        <td style="width: 144px;">
            <?php if ($megjelenit_header['modularis']) { ?>
                <input type="radio" name="megjelenit_header[modularis]" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="megjelenit_header[modularis]" value="0" />
                <?php echo $text_no; ?>
            <?php } else { ?>
                <input type="radio" name="megjelenit_header[modularis]" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="megjelenit_header[modularis]" value="0" checked="checked" />
                <?php echo $text_no; ?>
            <?php } ?>
        </td>
    </tr>
</table>


<script>
    function addCsoport(tombatad){


        var html = '<div class="csoportok" id="';
            var new_div_id = "csoport";
            var tomb = "[";
            for(i in tombatad){
                tomb += tombatad[i]+",";
                new_div_id += '_'+tombatad[i];
            }
            for (var kii=0; $("#"+new_div_id+"_"+kii).length > 0; kii++){
            }
            var old_div_id = new_div_id;
            new_div_id += '_'+kii;
            tomb += kii+"]";

            html += new_div_id+'"';

            html += ' style="padding-left: '+new_div_id.length+'px">';

            html += '<div class="csoportok_nyito"  onclick="csikiCsuki(this)">';
                var paratlan = true;
                for(i in tombatad){
                    html += "<span>";
                    if (paratlan) {
                        html += "Sor: ";
                        paratlan = false;
                    } else {
                        html += "Oszl.: ";
                        paratlan = true;
                    }
                    html += '<b>'+tombatad[i]+'</b>';
                    html += '</span>';
                }
                html += '<span>';
                if (paratlan) {
                    html += "Sor: ";
                } else {
                    html += "Oszl.: ";
                }
                html += '<b>'+kii+'</b>';
                html += "</span>";

                var new_kulcs = new_div_id.substr(8);
                var new_kulcs_past = "\'"+new_div_id.substr(8)+"\'";
                new_div_id = "#"+new_div_id;

                html += '<a onclick="$(\''+new_div_id+'\').remove();" class="button"><?php echo $button_remove ?></a>';
                html += '<a onclick="addCsoport('+tomb+')" class="button">Új csoport</a>';
                html += '<a onclick="addPaste('+new_kulcs_past+')" class="button beillesztes">Beillesztés</a>';

                html += '<a style="text-decoration: none; margin-right: 40px;">';
                html += 'Állapot: ';
                html += '<input type="radio" name="megjelenit_header_csoportok['+new_kulcs+'][default][status]" value="1" />';
                html += '<input type="radio" name="megjelenit_header_csoportok['+new_kulcs+'][default][status]" value="0" checked="checked"/>';
                html += '</a>';
                html += '<a class="input_tol_ig">Felbontás (tól -ig):';
                html += '<input type="text" name="megjelenit_header_csoportok['+new_kulcs+'][default][width_tol]" size="3" value="0"/>- ';
                html += '<input type="text" name="megjelenit_header_csoportok['+new_kulcs+'][default][width_ig]" size="3" value="3000" />';
                html += '</a>';
            html += '</div>';


            html += '<div class="csoport_beallitasok">';
                html += '<div>';
                    html += '<div class="scrollbox">';
                        <?php $class = 'odd'; ?>
                        <?php foreach ($megjelenit_header_csoportok_files as $fileskey=>$beallitdoboz) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            html += '<div class="<?php echo $class; ?>">';
                                html += '<input type="checkbox" name="megjelenit_header_csoportok['+new_kulcs+'][templates][<?php echo $fileskey?>][status]" value="1" />';
                                html += '<?php echo $fileskey; ?>';

                                <?php if (count($beallitdoboz) > 0) {?>
                                    <?php
                                    $key = $fileskey;
                                    $melyik_doboz = "megjelenit_header_csoportok[NULL_119][templates]"
                                    ?>
                                    html += '<span>';
                                    <?php foreach ($beallitdoboz as $keydoboz=>$tomb) { ?>
                                        <?php if(count($tomb) > 1) { ?>
                                            html += '<?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>';
                                        <?php } ?>
                                    <?php } ?>
                                    html += '</span>';
                                <?php } ?>
                                html += '<input class="sorrend" type="text" name="megjelenit_header_csoportok['+new_kulcs+'][templates][<?php echo $fileskey?>][sort_order]" value="0" size="2"/>';
                            html += '</div>';
                        <?php } ?>
                    html += '</div>';

                    html += '<br>';
                    html += '<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true)"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false)"><?php echo $text_unselect_all; ?></a>';
                    html += '<br>';
                html += '</div>';

                html += '<div class="kijeloles">';
                html += '<a onclick="addCopy('+tomb+');" class="button masolas">Kijelölés másolásra</a>';



                html += '</div>';

            html += '</div>';

        html += '</div>';
        html = html.replace(/NULL_119/g, new_kulcs);
        if ( $("#"+old_div_id).length > 0) {
            $("#"+old_div_id).append(html);
        } else {
            $("#ttab-header").append(html);
        }
    }
</script>

<a onclick="addCsoport();" class="button">Csoport hozzáadása</a>

<?php if (isset($megjelenit_header_csoportok) && $megjelenit_header_csoportok) {?>

    <?php $elemszam = 0?>

    <?php for($i=0; count($megjelenit_header_csoportok_kiegeszito) > $i; $i++) {?>

        <?php $key_aktualis = $megjelenit_header_csoportok_kiegeszito[$i];?>
        <?php $key_kovetkezo = isset($megjelenit_header_csoportok_kiegeszito[$i+1]) ? $megjelenit_header_csoportok_kiegeszito[$i+1] : $megjelenit_header_csoportok_kiegeszito[$i];?>
        <?php $csoportok = $megjelenit_header_csoportok[$key_aktualis]?>

        <?php $tomb_mostani = explode("_",$key_aktualis);?>
        <?php $tomb_kovetkezo = explode("_",$key_kovetkezo);?>
        <?php $kovetkezo_elemszam = count($tomb_kovetkezo);?>

        <?php if ($elemszam < count($tomb_mostani) || true) {?>
            <?php $elemszam = count($tomb_mostani);?>
        <?php } ?>


        <div class="csoportok" id="csoport_<?php echo $key_aktualis?>" style="padding-left: <?php echo (strlen($key_aktualis)+8)?>px" >
            <div class="csoportok_nyito"  onclick="csikiCsuki(this)">
                <?php $sor = true?>
                <?php foreach($tomb_mostani as $value) {?>
                    <?php if ($sor){?>
                        <span>Sor: <b><?php echo $value?></b>  </span>
                        <?php $sor = false;?>
                    <?php } else {?>
                        <span>Oszl.: <b><?php echo $value?></b>  </span>
                        <?php $sor = true;?>
                    <?php }?>
                <?php } ?>

                <?php
                $inttomb = array();
                foreach($tomb_mostani as $value){
                    $inttomb[] = (int)$value;
                }
                ?>

                <a onclick="$('#csoport<?php echo "_".$key_aktualis;?>').remove();" class="button"><?php echo $button_remove; ?></a>
                <a onclick="addCsoport(<?php echo json_encode($inttomb)?>);" class="button">Új csoport</a>
                <a onclick="addPaste('<?php echo $key_aktualis;?>');" class="button beillesztes">Beillesztés</a>

                <a style="text-decoration: none; margin-right: 40px;">
                    <?php echo $entry_status?>
                    <?php if (isset($megjelenit_header_csoportok[$key_aktualis]['default']['status']) && $megjelenit_header_csoportok[$key_aktualis]['default']['status'] == 1) {?>
                        <input type="radio" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][default][status]" value="1" checked="checked" />
                        <input type="radio" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][default][status]" value="0"  />
                    <?php } else {?>
                        <input type="radio" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][default][status]" value="1"  />
                        <input type="radio" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][default][status]" value="0" checked="checked" />
                    <?php } ?>
                </a>

                <a class="input_tol_ig">Felbontás (tól -ig):
                    <input type="text" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][default][width_tol]" size="3"
                           value="<?php echo isset($megjelenit_header_csoportok[$key_aktualis]['default']['width_tol']) ? $megjelenit_header_csoportok[$key_aktualis]['default']['width_tol'] : 0; ?>"/>-
                    <input type="text" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][default][width_ig]" size="3"
                           value="<?php echo isset($megjelenit_header_csoportok[$key_aktualis]['default']['width_ig']) ? $megjelenit_header_csoportok[$key_aktualis]['default']['width_ig'] : 3000; ?>" />
                </a>

            </div>

            <div class="csoport_beallitasok">
                <div>
                    <div class="scrollbox">
                        <?php $class = 'odd'; ?>
                        <?php foreach ($megjelenit_header_csoportok_files as $fileskey=>$beallitdoboz) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <div class="<?php echo $class; ?>">
                                <?php if (isset($megjelenit_header_csoportok[$key_aktualis]['templates'][$fileskey]['status']) && $megjelenit_header_csoportok[$key_aktualis]['templates'][$fileskey]['status'] == 1) { ?>
                                    <input type="checkbox" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][status]" value="1" checked="checked" />
                                    <?php echo $fileskey; ?>
                                <?php } else { ?>
                                    <input type="checkbox" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][status]" value="1" />
                                    <?php echo $fileskey; ?>
                                <?php } ?>

                                <?php if (count($beallitdoboz) > 0) {?>
                                    <?php
                                        $key = $fileskey;
                                        $melyik_doboz = "megjelenit_header_csoportok[$key_aktualis][templates]"
                                    ?>
                                    <span>
                                    <?php $doboz = isset($megjelenit_header_csoportok[$key_aktualis]['templates'][$fileskey]) ? $megjelenit_header_csoportok[$key_aktualis]['templates'][$fileskey] : ""; ?>
                                    <?php foreach ($beallitdoboz as $keydoboz=>$tomb) { ?>
                                        <?php if(count($tomb) > 1) { ?>

                                                <?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>
                                        <?php } ?>
                                    <?php } ?>
                                    </span>
                                <?php } ?>

                                <?php if (isset($megjelenit_header_csoportok[$key_aktualis]['templates'][$fileskey]['sort_order'])) { ?>
                                    <input class="sorrend" type="text" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][sort_order]" value="<?php echo $megjelenit_header_csoportok[$key_aktualis]['templates'][$fileskey]['sort_order']?>" size="2"/>
                                <?php } else { ?>
                                    <input class="sorrend" type="text" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][sort_order]" value="0" size="2"/>
                                <?php } ?>

                            </div>
                        <?php } ?>
                    </div>

                    <br>
                    <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
                    <br>
                </div>

                <!--<div class="scrollbox">
                    <?php $class = 'odd'; ?>
                    <?php foreach ($megjelenit_header_csoportok_files as $fileskey=>$beallitdoboz) { ?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <div class="<?php echo $class; ?>">
                            <?php if (isset($megjelenit_header_csoportok[$key_aktualis]['templates'][$fileskey]['sort_order'])) { ?>
                                <input type="text" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][sort_order]" value="<?php echo $megjelenit_header_csoportok[$key_aktualis]['templates'][$fileskey]['sort_order']?>" />
                            <?php } else { ?>
                                <input type="text" name="megjelenit_header_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][sort_order]" value="0" />
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>-->



                <div class="kijeloles">
                    <a onclick="addCopy('<?php echo $key_aktualis;?>');" class="button masolas">Kijelölés másolásra</a>

                </div>

            </div>

            <?php if ($kovetkezo_elemszam <= $elemszam ) {
                for($j=$kovetkezo_elemszam; $j<= $elemszam; $j++){
                    echo "</div>";
                }
            } ?>
    <?php } ?>
    <?php for($j=1; $j< $elemszam; $j++){
         echo "</div>";
    }?>


<?php } else {?>
<?php } ?>

<script>
    function addCopy(para){
        var eredmeny = $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[checked=checked],[type=text]');
        tarolo = new Array();

        for (var i=0; eredmeny.length > i; i++){
            tarolo[i] = eredmeny[i].name;
        }
        $(".masolas").css("display","none");
        $("#csoport_"+para+ " > .csoportok_nyito").css("color","blue");
        $(".beillesztes").fadeIn(300);
        $("#csoport_"+para+ " > .csoportok_nyito > .beillesztes").css("display","none");

        //if ($("#csoport_"+para+ " > .csoport_beallitasok").css("display") == "none") {
            $('.csoport_beallitasok').slideUp(300);
            //$("#csoport_"+para+ " > .csoport_beallitasok").slideDown(500);
        //}

    }

    function addPaste(para){
        var eredmeny = $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input");
        for (var i=0; eredmeny.length > i; i++){
            for (var j=0; tarolo.length > j; j++) {
                var type = $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("type");
                if (eredmeny[i].name.substr(eredmeny[i].name.indexOf("]")+1) == tarolo[j].substr(tarolo[j].indexOf("]")+1)) {

                    if (type == "checkbox"){
                        $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("checked",true);
                    } else if(type == "text") {

                        var tarolt_kulcs    = tarolo[j].substring(tarolo[j].indexOf("[")+1,tarolo[j].indexOf("]"));

                        var ertek = $("#csoport_"+tarolt_kulcs+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+tarolo[j]+'"]').val();
                        $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("value",ertek);
                    }
                    break;

                } else {
                    if (type == "checkbox"){
                        $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("checked",false);
                    }
                }
            }
        }
        $(".beillesztes").css("display","none");
        $(".masolas").fadeIn(300);
        $(".csoportok_nyito").css("color","#000");

        if ($("#csoport_"+para+ " > .csoport_beallitasok").css("display") == "none") {
            $('.csoport_beallitasok').css("display","none");
            $("#csoport_"+para+ " > .csoport_beallitasok").slideDown(500);
        }


    }

    function csikiCsuki(para){
        if (this.event.target == para || this.event.target.localName == "span") {
            if ($(para).next().css("display") != "none") {
                $(para).next().slideUp(500);
                $(para).removeClass("active");

            } else {
                $('.csoportok_nyito').removeClass("active");
                $('.csoport_beallitasok').hide();
                $(para).next().slideDown(500);
                $(para).addClass("active");

            }
        }
    }
</script>
