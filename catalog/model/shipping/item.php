<?php
class ModelShippingItem extends Model {
    function getQuote($address, $fizetesek="") {
		$this->load->language('shipping/item');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('item_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
		
		if (!$this->config->get('item_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		$method_data = array();
	
		if ($status) {

            $fizetesi_mod = array();

            if ($fizetesek){
                foreach($fizetesek as $value) {
                    if ( $this->config->get("item_payment_".$value['code']) &&  $this->config->get("item_payment_".$value['code']) == 1){
                        $fizetesi_mod[] = array(
                            'fizetesi_mod' => $value['code']
                        );
                    }
                }
            }
			$quote_data = array();
            if ($this->config->get('item_tax_class_id') > 0) {
                $query_tax = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$this->config->get('item_tax_class_id') . "'");
            }
            //$ado_title = isset($query_tax->row['title']) ? $query_tax->row['title'] :"";
            $ado_title = "";

      		$quote_data['item'] = array(
        		'code'         => 'item.item',
        		'title'        => $this->config->get('item_text') != null ? $this->config->get('item_text') :  $this->language->get('text_description'),
                'fizetesi_mod'  => $fizetesi_mod,
        		'cost'         => $this->config->get('item_cost') * $this->cart->countProducts(),
         		'tax_class_id' => $this->config->get('item_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('item_cost') * $this->cart->countProducts(), $this->config->get('item_tax_class_id'), $this->config->get('config_tax'))),
                'netto_text'   => $this->currency->format($this->config->get('item_cost'))  * $this->cart->countProducts(),
                'ado_title'    => $ado_title,
				'mail'         => $this->config->get('item_mail')
			);

      		$method_data = array(
        		'code'       => 'item',
        		'title'      => $this->config->get('item_header') != null ? $this->config->get('item_header') :  $this->language->get('text_description'),
                'fizetesi_mod'  => $fizetesi_mod,
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get('item_sort_order'),
        		'error'      => false
      		);
		}
	
		return $method_data;
	}
}
?>