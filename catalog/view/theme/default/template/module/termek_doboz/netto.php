<?php
$_['netto']          = array(
    'type'              => 'checkbox',
    'name'              => 'netto',
    'default'           => "0",
    'heading'           => '"Nettó" szöveg megjelenítés'
);

$_['null_off']          = array(
    'type'              => 'checkbox',
    'name'              => 'null_off',
    'default'           => "1",
    'heading'           => 'Nulla ne jelenjen meg'
);
$_['eredeti_ar_tiltas']          = array(
    'type'              => 'checkbox',
    'name'              => 'eredeti_ar_tiltas',
    'default'           => "1",
    'heading'           => 'Eredeti ár tiltás'
);

?>