<?php
class ModelSettingStore extends Model {
	public function getStores($data = array()) {
		$store_data = $this->cache->get('store');
	
		if (!$store_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store ORDER BY url");

			$store_data = $query->rows;
		
			$this->cache->set('store', $store_data);
		}
	 
		return $store_data;
	}

	public function storeImages($stores=array()){

		$sql = "SELECT * FROM " . DB_PREFIX . "setting WHERE (store_id = '0'";
		if ($stores) {
			foreach($stores as $store) {
				$sql .= " OR store_id ='".$store['store_id']."'";
			}
		}
		$sql .= ") AND `key` = 'config_aruhaz_image'";
		$sql .= " ORDER BY store_id ASC";
		$query = $this->db->query($sql);

		$vissza = array();
		foreach ($query->rows as $setting) {
			$vissza[] = array(
				'store_id'	=> $setting['store_id'],
				'image'		=> $setting['value'],
			);
		}


		return $vissza;
	}
}
?>