<style type="text/css">
    body {
        font-size: 16px;
        font-family: Helvetica, Arial, sans-serif;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: bottom center;
    }
    h1 {
        background-color: #0086BA;
        color: white;
        font-size: 2em;
        padding: 1em;
        margin:0;
        text-align: center;
        text-shadow: 0px 2px 3px rgba(0, 0, 0, 0.5);

    }
    h2 {
        background-color: #F9BA16;
        color: white;
        font-size: 1.5em;
        padding: 1em;
        margin:0;
        text-align: center;
        text-shadow: 0px 2px 3px rgba(0, 0, 0, 0.5);
    }

    h3 {
        font-size: 1.4em;
        padding: 1em 1em 0em 1em;
        text-align: center;
        color: #0086BA;
        border-bottom: 1px solid #F9BA16;
    }
    h4 {
        font-size: 1em;
        padding: 1em 1em 0em 1em;
        text-align: center;
        color: red;
    }
    #canvas {
        width: 100%;
        height: auto;
    }
    .cta-box {
        width: 30%;
        height: auto;
        /*
        position: absolute;
        top: 50px;
        right: 200px;
        */
        margin: 60px auto;
    }
    .cta-box a {
        text-decoration: none;
    }
    .cta {
        background: #FBBC15;
        background-image: -webkit-linear-gradient(top, #FBBC15, #eb9412);
        background-image: -moz-linear-gradient(top, #FBBC15, #eb9412);
        background-image: -ms-linear-gradient(top, #FBBC15, #eb9412);
        background-image: -o-linear-gradient(top, #FBBC15, #eb9412);
        background-image: linear-gradient(to bottom, #FBBC15, #eb9412);
        -webkit-border-radius: 60;
        -moz-border-radius: 60;
        border-radius: 60px;
        -webkit-box-shadow: 0px 2px 13px #666666;
        -moz-box-shadow: 0px 2px 13px #666666;
        box-shadow: 0px 2px 13px #666666;
        color: #ffffff;
        font-size: 4em;
        padding: 20px 30px 20px 30px;
        border: solid #ffffff 7px;
        text-decoration: none;
        text-align: center;
        cursor: pointer;
        text-shadow: 0px 2px 3px rgba(0, 0, 0, 0.5);
    }
    .cta:hover {
        background: #fa6617;
        background-image: -webkit-linear-gradient(top, #fa6617, #fa2617);
        background-image: -moz-linear-gradient(top, #fa6617, #fa2617);
        background-image: -ms-linear-gradient(top, #fa6617, #fa2617);
        background-image: -o-linear-gradient(top, #fa6617, #fa2617);
        background-image: linear-gradient(to bottom, #fa6617, #fa2617);
        text-decoration: none;
    }
    .felhivas {
        width: 225px;
        height: 150px;
        background: url('/facebook/img/nyeremenyjatek_folyamatban.png');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: top center;
        /*position: absolute;
        bottom: 200px;
        left: 100px;*/
        margin: 10px 0 0 100px;
        -ms-transform: rotate(-10deg);
        -webkit-transform: rotate(-10deg);
        transform: rotate(-10deg);
        -webkit-box-shadow: 0px 2px 6px #000000;
        -moz-box-shadow: 0px 2px 6px #000000;
        box-shadow: 0px 2px 6px #000000;
    }
    .felhivas:hover {
        -ms-transform: rotate(-10deg) scale(1.5);
        -webkit-transform: rotate(-10deg) scale(1.5);
        transform: rotate(-10deg);
        -webkit-transition: 0.3s;
        transition: 0.3s;
        transition-timing-function: ease-in-out;
    }
    .kosar.button {
        margin:0 auto;
        cursor: pointer;
        padding: 20px 40px;
        width:300px;
        font-size: 19px; color: rgb(255, 255, 255); line-height: 0; font-family: Verdana, sans-serif; font-weight: bold; box-shadow: rgb(102, 102, 102) 0px 2px 2px; transition: color 350ms ease-in-out, background-color 350ms ease-in-out; border: 0px; height: 30px; width: auto; background: rgb(27, 131, 190);
    }</style>
<script type="text/javascript">
    window.fbAsyncInit = function()
    {
        FB.Canvas.setSize();
    }
</script>
<div id="canvas">
    <h1>
        Hogyan juthatsz hozzá egy 1,2 millió Ft értékű napelem rendszerhez ingyen?</h1>
    <h2>
        Egy napelem rendszer igen drága beruházás….</h2>
    <p>
        Egy átlagos családi házhoz szükséges napelem rendszer több mint 1 millió forintba kerül, a fogyasztás növekedésével pedig egyre drágább. Épp ezért csak kevés ember engedheti meg magának, hogy napenergiát használjon otthonában.</p>
    <h3>
        <strong>Készülj okosan a pályázatokra</strong></h3>
    <p>
        Szerencsére van esély arra, hogy pályázati forrás segítségével telepíthess napelemet.</p>
    <p>
        Jelenleg nincs ilyen pályázat, de a kiírások bármikor változhatnak. Ezért bízzunk benne, hogy jövő év elején már meghirdetnek olyat, amely forrást biztosíthat az otthoni napelem rendszeredhez.<br />
        Ne aggódj azon, hogy nem tudod pontosan a pályázatok időpontját! Ez már csak akkor számít, amikor a beadási határidőket be kell tartani. Jelenleg csupán egy picit előre kell gondolkodnod. Elmondjuk miért: Amennyiben kiírnak egy pályázatot, a lakossági igény hirtelen megnő a napelem rendszerekre. A rengeteg igénybenyújtást a napelemmel foglalkozó cégek és a szolgáltató nem fogják győzni, ezért hamar kialakulnak a várólisták. Ilyen esetben szinte borítékolható, hogy lemaradsz a pályázatról. Erre bizonyíték a legutolsó kiírt pályázat, amely 2011. augusztus 15-én jelent meg a lakosság részére. Ezt két nap múlva le is zárták. Csupán 3-400 családi házra volt elegendő a keret.</p>
    <h3>
        <strong>Mit kell tenned?</strong></h3>
    <p>
        Egy biztos: <strong>ENGEDÉLYEZTETÉSI TERVVEL KELL RENDELKEZNED</strong>.</p>
    <p>
        Egy ilyen engedélyeztetési terv elkészülése 2 hónapig is eltarthat. A pályázatok azonban hamar lezárulnak…</p>
    <p>
        Egyetlen dologgal tudod bebiztosítani magad: ha már MOST rendelsz egy engedélyeztetési tervet. A terv nem évül el (kivéve Elmű-ÉMÁSZ esetében - de ott is 1 évig érvényes), ezért nem kell aggódnod, hogy mikor írják ki a pályázatot.</p>
    <h3>
        <strong>Terveztess, hogy nyerhess</strong></h3>
    <p>
        Idén már 350 sikeres engedélyeztetési tervet elkészítettünk, valamint mindhárom nagy áramszolgáltatóval (E.ON, Elmű-ÉMÁSZ, EDF DÉMÁSZ) kapcsolatban állunk. Nálunk a legjobb helyen vagy!</p>
    <p>
        Az engedélyeztetési tervben tulajdonképpen megtervezzük a házadhoz illő rendszert.</p>
    <p>
        Nem kell aggódnod, hogy a tervet visszautasítják, méghozzá két okból:</p>
    <ol>
        <li>
            Soha nem történt még olyan, hogy a mi engedélyeztetési tervünket visszautasították.</li>
        <li>
            A terv felülvizsgáló szervezetnek is az a célja, hogy minél több otthonba napelemek kerüljenek.</li>
    </ol>
    <p>
        Rendeld meg MOST a tervet, így részt tudsz venni <strong>NYEREMÉNYJÁTÉKUNKON</strong> is, ahol egy 2 kW-os (1,2 millió forint értékű) napelem rendszert nyerhetsz!</p>
    <p>
        Ha még idén, 2015-ben megrendeled az engedélyeztetési tervet, egyedi akció vár Rád:</p>
    <h4>
        85.000 Ft-os ár helyett <span style="font-size:2.5em;">50.000 Ft</span>-ért igényelhetsz engedélyeztetési tervet.</h4>
    <p>
        Rendeld meg még időben! 100%-os pénzvisszafizetési garanciát vállalunk arra az esetre, ha az engedélyeztetés nem sikerül.<br />
        Töltsd ki a megrendelő lapot adataiddal, és küldd el nekünk az engedélyeztetési terved igénylését!</p>
    <p>
        <strong>Adj esélyt magadnak, hogy örökre elfelejthesd villanyszámlád!</strong></p>
    <div class="product_sor_1 prod_center " style="margin: 0px; padding: 0px; font-size: 0px; box-sizing: border-box; color: rgb(88, 88, 88); font-family: Arial;">
        <div class="product_oszlop_1_1 short_desc " style="margin: 0px; padding: 0px; font-size: 12px; max-width: 660px; vertical-align: top; display: inline-block;">
            <div class="product_sor_1_1_2 valasztek-panel " style="margin: 0px; padding: 0px; text-align:center;">
                &nbsp;<a href="http://www.eu-solar.hu/jatekszabalyzat-nyerj-egy-teljesen-ingyenes-napelem-rendszert-otthonodba/" target="_blank">Játékszabályzat</a> | <a href="http://www.eu-solar.hu/adatvedelmi-nyilatkozat/" target="_blank">Adatvédelmi nyilatkozat</a></div>
            <div class="product_sor_1_1_3 kosar_sor " style="padding: 0px; text-align: right; font-size: 0px; margin: 0px !important;">
                &nbsp;</div>
            <div class="product_sor_1_1_4 kosar_sor " style="margin: 0px; padding: 0px; text-align: right; font-size: 0px;">
                &nbsp;</div>
        </div>
    </div>
    <div class="product_sor_4 szocialis " style="margin: 0px 0px 10px; padding: 0px; font-size: 0px; min-height: 39px; color: rgb(88, 88, 88); font-family: Arial;">
        <div class="product_oszlop_4_0  " style="margin: 7px 0px 0px; padding: 0px; font-size: 12px; max-width: 550px; display: inline-block; vertical-align: top;">
            &nbsp;</div>
        &nbsp;
        <div class="product_oszlop_4_1  " style="margin: 0px; padding: 0px; font-size: 12px; max-width: 550px; display: inline-block;">
            <div class="product_sor_4_1_0  " style="margin: 0px; padding: 0px;">
                <div class="addthis" style="margin: 0px 0px 0px auto; padding: 0px; display: table;">
                    <div class="addthis_sharing_toolbox" data-title="Kivitelezés, tervezés" data-url="http://eu.somaweb.eu/index.php?route=product/product&amp;product_id=12220" style="margin: 0px; padding: 0px;">
                        <div class="at-share-tbx-element addthis_32x32_style addthis-smartlayers addthis-animated at4-show" id="atstbx" style="margin: 0px; padding: 0px; animation-fill-mode: both; animation-duration: 1s; opacity: 1 !important;">
                            &nbsp;</div>
                        <div class="at-share-tbx-element addthis_32x32_style addthis-smartlayers addthis-animated at4-show" id="atstbx" style="margin: 0px; padding: 0px; animation-fill-mode: both; animation-duration: 1s; opacity: 1 !important;">
                            <input class="kosar button" style="" type="button" value="Megrendelem" /></div>
                        <div>
                            &nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product_sor_10  " style="margin: 0px; padding: 0px; color: rgb(88, 88, 88); font-family: Arial; font-size: 13px;">
        &nbsp;</div>
</div>
<p>
    &nbsp;</p>
