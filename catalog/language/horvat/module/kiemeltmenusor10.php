<?php

// Heading
$_['heading_title']  = 'Tavaszi ajánlatok';

// Text
$_['text_reviews']     = '%s vélemény alapján!';
$_['text_egypar']     = 'Pár db';
$_['text_empty']        = 'Ebben a kategóriában nincsenek termékek.';

$_['text_akcio_title']        = 'Akciós termék';
$_['text_pardarab_title']     = 'Pár darab';
$_['text_uj_title']           = 'Új termék';
?>
