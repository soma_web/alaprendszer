<?php
class ModelModuleCron extends Model {

    public function addCron($data) {
        $sql = "INSERT INTO ".DB_PREFIX."cron SET
            `program`	    = '".(!empty($data['program']) ? $data['program'] : '')."',
	        `rendszeresseg` = '".(!empty($data['rendszeresseg']) ? $data['rendszeresseg'] : '')."',
	        `idonkent`      = '".(!empty($data['idonkent']) ? $data['idonkent'] : '')."',
	        `status`        = '".(!empty($data['status']) ? $data['status'] : '')."'";
        $query = $this->db->query($sql);
        $cron_id = $this->db->getLastId();



        if ($data['rendszeresseg'] == 2) {
            if (!empty($data['naponta_idonkent'])) {
                foreach($data['naponta_idonkent'] as $idopont) {
                    $sql = "INSERT INTO ".DB_PREFIX."cron_pontosido SET pontosido='".$idopont."',  cron_id='".$cron_id."'";
                    $query = $this->db->query($sql);
                }
            }

        } elseif ($data['rendszeresseg'] == 3) {
            if (!empty($data['hetente_nap'])) {
                foreach($data['hetente_nap'] as $key=>$nap) {
                    if (!empty($nap['het_napja'])) {
                        if (!empty($nap['idopontok'])) {
                            foreach($nap['idopontok'] as $idopont) {
                                $sql = "INSERT INTO ".DB_PREFIX."cron_pontosido SET
                                    pontosido   ='".$idopont."',
                                    het_napja   ='".$nap['het_napja']."',
                                    cron_id     ='".$cron_id."'";
                                $query = $this->db->query($sql);
                            }
                        } else {
                            $sql = "INSERT INTO ".DB_PREFIX."cron_pontosido SET
                                    het_napja   ='".$nap['het_napja']."',
                                    cron_id     ='".$cron_id."'";
                            $query = $this->db->query($sql);
                        }
                    }
                }
            }
        }

        if (isset($data['program']))        unset ($data['program']);
        if (isset($data['rendszeresseg']))  unset ($data['rendszeresseg']);
        if (isset($data['idonkent']))       unset ($data['idonkent']);
        if (isset($data['status']))         unset ($data['status']);

        if ($data) {
            $seria = serialize($data);

            $sql = "UPDATE ".DB_PREFIX."cron SET
                    `input_elements`	    = '".$seria."'
	                WHERE cron_id='".$cron_id."'";
            $query = $this->db->query($sql);
        }

    }

    public function editCron($cron_id,$data) {
        $sql = "UPDATE ".DB_PREFIX."cron SET
                    `program`	    = '".(!empty($data['program']) ? $data['program'] : '')."',
	                `rendszeresseg` = '".(!empty($data['rendszeresseg']) ? $data['rendszeresseg'] : '')."',
	                `idonkent`      = '".(!empty($data['idonkent']) ? $data['idonkent'] : '')."',
	                `status`        = '".(!empty($data['status']) ? $data['status'] : '')."'
	            WHERE cron_id='".$cron_id."'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM ".DB_PREFIX."cron_pontosido WHERE cron_id='".$cron_id."'";
        $query = $this->db->query($sql);

        if ($data['rendszeresseg'] == 2) {
            if (!empty($data['naponta_idonkent'])) {
                foreach($data['naponta_idonkent'] as $idopont) {
                    $sql = "INSERT INTO ".DB_PREFIX."cron_pontosido SET pontosido='".$idopont."',  cron_id='".$cron_id."'";
                    $query = $this->db->query($sql);
                }
            }

        } elseif ($data['rendszeresseg'] == 3) {
            if (!empty($data['hetente_nap'])) {
                foreach($data['hetente_nap'] as $key=>$nap) {
                    if (!empty($nap['het_napja'])) {
                        if (!empty($nap['idopontok'])) {
                            foreach($nap['idopontok'] as $idopont) {
                                $sql = "INSERT INTO ".DB_PREFIX."cron_pontosido SET
                                    pontosido   ='".$idopont."',
                                    het_napja   ='".$nap['het_napja']."',
                                    cron_id     ='".$cron_id."'";
                                $query = $this->db->query($sql);
                            }
                        } else {
                            $sql = "INSERT INTO ".DB_PREFIX."cron_pontosido SET
                                    het_napja   ='".$nap['het_napja']."',
                                    cron_id     ='".$cron_id."'";
                            $query = $this->db->query($sql);
                        }
                    }

                }
            }
        }

        if (isset($data['program']))        unset ($data['program']);
        if (isset($data['rendszeresseg']))  unset ($data['rendszeresseg']);
        if (isset($data['idonkent']))       unset ($data['idonkent']);
        if (isset($data['status']))         unset ($data['status']);

        $seria = '';
        if ($data) {
            $seria = serialize($data);
        }

        $sql = "UPDATE ".DB_PREFIX."cron SET
                `input_elements`	    = '".$seria."'
	            WHERE cron_id='".$cron_id."'";
        $query = $this->db->query($sql);
    }

    public function getCrons($data = array()) {
        $sql = "SELECT * FROM ".DB_PREFIX."cron ";
        $sort_data = array(
            'cron_id',
            'program',
            'rendszeresseg',
            'status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY program";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }


        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }


        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCron($cron_id) {
        $sql  = "SELECT * FROM ".DB_PREFIX."cron WHERE cron_id='".$cron_id."'";
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $sql  = "SELECT pontosido, datum, het_napja FROM ".DB_PREFIX."cron_pontosido WHERE cron_id='".$cron_id."' ORDER BY het_napja,pontosido";
            $query_ido = $this->db->query($sql);
            if ($query_ido->num_rows > 0) {
                $vissza = array();
                foreach($query_ido->rows as $value) {
                    if ($query->row['rendszeresseg'] == 3) {
                        $kigyujt[$value['het_napja']][] = $value['pontosido'];

                    } elseif ($query->row['rendszeresseg'] == 4) {
                        $vissza[$value['datum']][] = $value['pontosido'];

                    } else {
                        $vissza[] = $value['pontosido'];

                    }
                }

                if ($query->row['rendszeresseg'] == 3) {
                    foreach($kigyujt as $key=>$value) {
                        $vissza[] = array(
                            'het_napja' => $key,
                            'idopontok' => $value
                        );
                    }
                    $vissza = $this->config->rendezes($vissza,'het_napja');
                }

                $query->row['idopontok'] = $vissza;

            }
        }

        return $query->row;
    }

    public function getTotalCrons() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "cron`");

        return $query->row['total'];
    }

    public function getRunCrons() {
        $vissza = array();

        $sql =  " SELECT program, cron_id FROM " . DB_PREFIX . "cron";
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            foreach($query->rows as $cron) {
                $sql  = " SELECT date_start,date_end FROM ". DB_PREFIX . "cron_allapot ";
                $sql .= " WHERE cron_id='".$cron['cron_id']."' ";
                $sql .= " ORDER BY date_start DESC LIMIT 0,1 ";
                $query_alapot = $this->db->query($sql);
                if ($query_alapot->num_rows > 0) {
                    $vissza[] = array(
                        'cron_id'   => $cron['cron_id'],
                        'program'    => $cron['program'],
                        'date_start' => $query_alapot->row['date_start'],
                        'date_end'   => $query_alapot->row['date_end'],
                        'status'     => $query_alapot->row['date_end'] ? 1 : 0,
                    );
                }

            }
        }
        return $vissza;

    }

    public function getRunResultCrons($cron_id, $data = array()) {

        $sql =  " SELECT * FROM " . DB_PREFIX . "cron_allapot WHERE cron_id='".$cron_id."'";

        $sql .=" ORDER BY cron_allapot_id DESC ";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;

    }

    public function getRunResultCronsTotal($cron_id) {

        $sql =  " SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cron_allapot WHERE cron_id='".$cron_id."'";
        $query = $this->db->query($sql);

        return $query->row['total'];

    }

    public function getTotalRunCrons() {
        $sql = "SELECT c.cron_id, ca.cron_allapot_id FROM ".DB_PREFIX."cron c INNER JOIN ".DB_PREFIX."cron_allapot ca ON (c.cron_id=ca.cron_id) GROUP BY c.cron_id";
        $query = $this->db->query($sql);

        return $query->num_rows;
    }

    public function deleteCron($cron_id) {

        $sql = "DELETE FROM ".DB_PREFIX."cron WHERE cron_id='".$cron_id."'";
        $query = $this->db->query($sql);
        if ($query) {
            $sql = "DELETE FROM ".DB_PREFIX."cron_allapot WHERE cron_id='".$cron_id."'";
            $query = $this->db->query($sql);

            if ($query) {
                $sql = "DELETE FROM ".DB_PREFIX."cron_pontosido WHERE cron_id='".$cron_id."'";
                $query = $this->db->query($sql);
            }
        }

        return $query;

    }
}
?>