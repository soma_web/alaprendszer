<?php
class ControllerShippingPostaPont extends Controller {
	private $error = array(); 

	public function index() {   
		$this->language->load('shipping/postapont');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('postapont', $this->request->post);		

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_none'] = $this->language->get('text_none');

		$this->data['entry_cost'] = $this->language->get('entry_cost');
		$this->data['entry_free_total'] = $this->language->get('entry_free_total');
		$this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_google_maps_api_key'] = $this->language->get('entry_google_maps_api_key');
		$this->data['google_maps_api_key_help'] = $this->language->get('google_maps_api_key_help');
		$this->data['entry_status_postapont'] = $this->language->get('entry_status_postapont');
		$this->data['entry_status_mol'] = $this->language->get('entry_status_mol');
		$this->data['entry_status_coop'] = $this->language->get('entry_status_coop');
		$this->data['entry_status_automata'] = $this->language->get('entry_status_automata');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/postapont', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('shipping/postapont', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['postapont_cost'])) {
			$this->data['postapont_cost'] = $this->request->post['postapont_cost'];
		} else {
			$this->data['postapont_cost'] = $this->config->get('postapont_cost');
		}
		
		if (isset($this->request->post['postapont_free_total'])) {
			$this->data['postapont_free_total'] = $this->request->post['postapont_free_total'];
		} else {
			$this->data['postapont_free_total'] = $this->config->get('postapont_free_total');
		}

		if (isset($this->request->post['postapont_tax_class_id'])) {
			$this->data['postapont_tax_class_id'] = $this->request->post['postapont_tax_class_id'];
		} else {
			$this->data['postapont_tax_class_id'] = $this->config->get('postapont_tax_class_id');
		}

		$this->load->model('localisation/tax_class');

		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		if (isset($this->request->post['postapont_geo_zone_id'])) {
			$this->data['postapont_geo_zone_id'] = $this->request->post['postapont_geo_zone_id'];
		} else {
			$this->data['postapont_geo_zone_id'] = $this->config->get('postapont_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['postapont_status'])) {
			$this->data['postapont_status'] = $this->request->post['postapont_status'];
		} else {
			$this->data['postapont_status'] = $this->config->get('postapont_status');
		}
		
		if (isset($this->request->post['status_postapont'])) {
			$this->data['status_postapont'] = $this->request->post['status_postapont'];
		} else {
			$this->data['status_postapont'] = $this->config->get('status_postapont');
		}
		if (isset($this->request->post['mol_status'])) {
			$this->data['mol_status'] = $this->request->post['mol_status'];
		} else {
			$this->data['mol_status'] = $this->config->get('mol_status');
		}
		if (isset($this->request->post['coop_status'])) {
			$this->data['coop_status'] = $this->request->post['coop_status'];
		} else {
			$this->data['coop_status'] = $this->config->get('coop_status');
		}
		if (isset($this->request->post['automata_status'])) {
			$this->data['automata_status'] = $this->request->post['automata_status'];
		} else {
			$this->data['automata_status'] = $this->config->get('automata_status');
		}

		if (isset($this->request->post['postapont_sort_order'])) {
			$this->data['postapont_sort_order'] = $this->request->post['postapont_sort_order'];
		} else {
			$this->data['postapont_sort_order'] = $this->config->get('postapont_sort_order');
		}
		
		if (isset($this->request->post['postapont_google_maps_api_key'])) {
			$this->data['postapont_google_maps_api_key'] = $this->request->post['postapont_google_maps_api_key'];
		} else {
			$this->data['postapont_google_maps_api_key'] = $this->config->get('postapont_google_maps_api_key');
		}

        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getInstalled('payment');
        $no_module = $this->model_setting_extension->getNoModule();



        foreach ($extensions as $key => $value) {
            if (!file_exists(DIR_APPLICATION . 'controller/payment/' . $value . '.php')) {
                $this->model_setting_extension->uninstall('payment', $value);

                unset($extensions[$key]);
            } elseif (in_array($value, $no_module)){
                unset($extensions[$key]);
            } else {
                $this->load->language('payment/' . $value);

                if (isset($this->request->post['postapont_payment_'.$value])) {
                    $this->data['postapont_payment_'.$value] = $this->request->post['postapont_payment_'.$value];
                } elseif ( $this->config->get('postapont_payment_'.$value)) {
                    $this->data['postapont_payment_'.$value] = $this->config->get('postapont_payment_'.$value);
                } else {
                    $this->data['postapont_payment_'.$value] = 0;
                }

                $this->data['payment']['payment_' . $value] = array(
                    'name'      => $this->language->get('heading_title'),
                    'valasztva' => $this->data['postapont_payment_'.$value]
                );


            }
        }

		$this->template = 'shipping/postapont.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/postapont')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>