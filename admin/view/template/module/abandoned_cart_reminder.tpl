<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
	<div id="tabs" class="htabs">
		<a id="acr-setting" href="#tab-setting"><?php echo $tab_setting; ?></a>
		<a id="acr-preview" href="#tab-preview"><?php echo $tab_preview; ?></a>
		<!--<a id="acr-help" href="#tab-help"><?php echo $tab_help; ?></a>-->
	</div>
  
    <div id="tab-setting">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		  <table class="form" id="acr">
			<tr>
				<td class="left"><?php echo $entry_secret_code; ?></td>
				<td class="left"><input type="text" name="abandoned_cart_reminder_secret_code" value="<?php echo $abandoned_cart_reminder_secret_code; ?>">
				<?php if ($error_secret_code) { ?>
				<span class="error"><?php echo $error_secret_code; ?></span>
				<?php } ?>
				</td>
			</tr>
			
			<tr>
				<td class="left"><?php echo $entry_delay; ?></td>
				<td class="left"><input type="text" name="abandoned_cart_reminder_delay" value="<?php echo $abandoned_cart_reminder_delay; ?>">
				<?php if ($error_delay) { ?>
				<span class="error"><?php echo $error_delay; ?></span>
				<?php } ?>
				</td>
			</tr>
			
			<tr>
				<td class="left"><?php echo $entry_max_reminders; ?></td>
				<td class="left"><input type="text" name="abandoned_cart_reminder_max_reminders" value="<?php echo $abandoned_cart_reminder_max_reminders; ?>">
				<?php if ($error_max_reminders) { ?>
				<span class="error"><?php echo $error_max_reminders; ?></span>
				<?php } ?>
				</td>
			</tr>
			
			<tr style="display: none;">
				<td class="left"><?php echo $entry_use_html_email; ?></td>
				<td><select name="abandoned_cart_reminder_use_html_email">
					<?php if ($abandoned_cart_reminder_use_html_email) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				</select>
				
				<?php if ($error_use_html_email){  ?>
				<span class="error"><?php echo $error_use_html_email; ?></span>
				<?php } ?>
				</td>
			</tr>

			<tr>
				<td class="left"><?php echo $entry_add_coupon; ?></td>
				<td class="left"><select name="abandoned_cart_reminder_add_coupon">
				<?php if ($abandoned_cart_reminder_add_coupon) {  ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>			
					<option value="0"><?php echo $text_disabled; ?></option>			
				<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>			
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
				</select></td>
			</tr>
			
			<tbody id="coupon-1" class="coupon">
				<tr>
					<td class="left"><?php echo $entry_coupon_type; ?></td>
					<td class="left"><select name="abandoned_cart_reminder_coupon_type">
					<?php if ($abandoned_cart_reminder_coupon_type) {  ?>
						<option value="1" selected="selected"><?php echo $text_coupon_percent; ?></option>			
						<option value="0"><?php echo $text_coupon_fixed; ?></option>			
					<?php } else { ?>
						<option value="1"><?php echo $text_coupon_percent; ?></option>			
						<option value="0" selected="selected"><?php echo $text_coupon_fixed; ?></option>
					<?php } ?>
					</select></td>
				</tr>
				
				<tr>
					<td class="left"><?php echo $entry_coupon_amount; ?></td>
					<td class="left"><input type="text" name="abandoned_cart_reminder_coupon_amount" value="<?php echo $abandoned_cart_reminder_coupon_amount; ?>">
					<?php if ($error_coupon_amount) { ?>
					<span class="error"><?php echo $error_coupon_amount; ?></span>
					<?php } ?>
					</td>
				</tr>
				
				<tr>
					<td class="left"><?php echo $entry_coupon_expire; ?></td>
					<td class="left"><input type="text" name="abandoned_cart_reminder_coupon_expire" value="<?php echo $abandoned_cart_reminder_coupon_expire; ?>">
					<?php if ($error_coupon_expire) { ?>
					<span class="error"><?php echo $error_coupon_expire; ?></span>
					<?php } ?>
					</td>
				</tr>
				
				<tr>
					<td class="left"><?php echo $entry_reward_limit; ?></td>
					<td class="left"><input type="text" name="abandoned_cart_reminder_reward_limit" value="<?php echo $abandoned_cart_reminder_reward_limit; ?>">
					</td>
				</tr>
				
			</tbody>
			
			<tr>
				<td class="left"><?php echo $entry_log_admin; ?></td>
				<td class="left"><select name="abandoned_cart_reminder_log_admin">
				<?php if ($abandoned_cart_reminder_log_admin) {  ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>			
					<option value="0"><?php echo $text_disabled; ?></option>			
				<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>			
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
				</select></td>
			</tr>
			
		  </table>
		</form>
	</div>

	<div id="tab-preview">
		<div class="attention"><?php echo $text_preview_info; ?></div>
		<div class="buttons">
			<a class="button" id="btn-preview" onclick="reminderShow('preview');"><?php echo $button_preview; ?></a>
			<a class="button" id="btn-send" onclick="reminderShow('send');" style="float:right;"><?php echo $button_send; ?></a>
		</div>
	</div>
	
	<!--<div id="tab-help">
		Changelog and HELP you can find  : <a href="http://oc-extensions.com/Abandoned-Cart-Reminder" target="blank">HERE</a><br /><br />
		If you need support email us at <strong>support@oc-extensions.com</strong><br /><br /><br />
		
		<u><strong>Become a Premium Member:</strong></u><br /><br />
		With Premium Membership you will can download all our products (past, present and future) starting with the payment date, until the same day and month, a year later. <br />
		Find more on <a href="http://www.oc-extensions.com">www.oc-extensions.com</a>
	</div>-->
	
  </div>
</div>

<script type="text/javascript"><!--	
$('#tabs a').tabs();

$('select[name=\'abandoned_cart_reminder_add_coupon\']').bind('change', function() {
	$('#acr .coupon').hide();
	
	$('#acr #coupon-' + $(this).attr('value').replace('_', '-')).show();
});

$('select[name=\'abandoned_cart_reminder_add_coupon\']').trigger('change');

function reminderShow(operation){
	$('#dialog').remove();
	
	var iframe_url = '<?php echo $front_base_url; ?>index.php?route=account/cron_acr&secret_code=<?php echo $abandoned_cart_reminder_secret_code; ?>&op_type=' + operation;
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px; background: #FFF;"><iframe src="' + iframe_url + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: 'Emlékeztetők',
		bgiframe: false,
		width: 800,
		height: 520,
		resizable: false,
		modal: true
	});
}
//--></script> 

<?php echo $footer; ?>