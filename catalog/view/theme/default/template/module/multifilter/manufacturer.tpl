
<?php if ( isset($manufacturers) && $manufacturers){ ?>

    <div class="box">
        <div class="box-heading reszek harmadik-filter"><?php echo $heading_title_gyarto; ?><img style="display: none"  src="catalog/view/theme/default/image/responsive/pos_r_opened.png"></div>
        <?php if ($beallitasok['gyarto']['teljes'] != 1) { ?>
            <div class="box-content">
                <ul class="box-filter box-filter-gyarto">
                    <?php foreach ($manufacturers as $manufacturer) { ?>
                        <?php if (!empty($manufacturer['name'])) { ?>
                            <?php if (in_array($manufacturer['manufacturer_id'], $filter_gyarto)) { ?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $manufacturer['manufacturer_id']; ?>" id="manufacturer<?php echo $manufacturer['manufacturer_id']; ?>"  checked="checked"/>
                                    <label class="css-label mac-style" for="manufacturer<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></label>
                                </li>
                            <?php }  else {?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $manufacturer['manufacturer_id']; ?>" id="manufacturer<?php echo $manufacturer['manufacturer_id']; ?>" />
                                    <label class="css-label mac-style" for="manufacturer<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></label>
                                </li>
                            <?php } ?>

                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        <?php } else {?>
            <div class="box-content">
                <ul class="box-filter box-filter-gyarto">
                    <?php foreach ($manufacturers_teljes as $manufacturer_teljes) { ?>
                        <?php if (in_array($manufacturer_teljes, $manufacturers)) { ?>
                            <?php if (in_array($manufacturer_teljes['manufacturer_id'], $filter_gyarto)) { ?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $manufacturer_teljes['manufacturer_id']; ?>" id="manufacturer<?php echo $manufacturer_teljes['manufacturer_id']; ?>"  checked="checked"/>
                                    <label class="css-label mac-style" for="manufacturer<?php echo $manufacturer_teljes['manufacturer_id']; ?>"><?php echo $manufacturer_teljes['name']; ?></label>
                                </li>
                            <?php } else {?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $manufacturer_teljes['manufacturer_id']; ?>" id="manufacturer<?php echo $manufacturer_teljes['manufacturer_id']; ?>" />
                                    <label class="css-label mac-style" for="manufacturer<?php echo $manufacturer_teljes['manufacturer_id']; ?>"><?php echo $manufacturer_teljes['name']; ?></label>
                                </li>
                            <?php } ?>
                        <?php } else {?>
                            <li><input class="css-checkbox" type="checkbox" value="<?php echo $manufacturer_teljes['manufacturer_id']; ?>" id="manufacturer<?php echo $manufacturer_teljes['manufacturer_id']; ?>" disabled="disabled"/>
                                <label class="css-label mac-style halvany" for="manufacturer<?php echo $manufacturer_teljes['manufacturer_id']; ?>" ><?php echo $manufacturer_teljes['name']; ?></label>
                            </li>
                        <?php } ?>

                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
    </div>
<?php }?>