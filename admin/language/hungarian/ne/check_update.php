<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Frissítések keresése';

// Text
$_['text_update_latest']					= 'Jó hír! Az Ön verziója naprakész:  Newsletter Enhancements - V3 - vQmod + Free Mega Template Pack - v%s';
$_['text_update_available']					= 'A v%s of Newsletter Enhancements - V3 - vQmod + Free Mega Template Pack új verziója elérhető. Kattintson <a href="http://www.opencart.com/index.php?route=extension/extension/info&extension_id=4776" target="_blank">ide</a> a letöltéshez!';

// Button 
$_['button_check']							= 'Ellenőrzés';

?>
