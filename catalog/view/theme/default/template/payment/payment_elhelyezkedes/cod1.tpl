<div class="buttons">
    <div class="right">
        <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="button" />
    </div>
</div>
<script type="text/javascript"><!--
    $('#button-confirm').bind('click', function() {
        $.ajax({
            type: 'GET',
            url: 'index.php?route=payment/payment_elhelyezkedes/cod1/confirm',
            beforeSend: function() {
                $('#button-confirm').attr('disabled', true);
                $('#button-confirm').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            success: function() {
                location = '<?php echo $continue; ?>';
            },
            complete: function() {
                $('#button-confirm').attr('disabled', false);
                $('.wait').remove();
            }
        });
    });
    //--></script>
