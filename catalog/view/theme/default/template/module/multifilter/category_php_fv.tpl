<?php

function category_children($para,$parts){
    if (isset($para['children']) && $para['children']) { ?>
        <ul>
            <?php foreach ($para['children'] as $child) { ?>
                <?php $van_category = in_array($child['category_id'],$parts) ? true : false; ?>

                <li class="menu-text-image-1" >


                    <?php if ($van_category){ ?>
                        <a href="<?php echo $child['href']; ?>" class="active">  <?php echo $child['name']; ?></a>

                    <?php } else {?>
                        <a href="<?php echo $child['href']; ?>">  <?php echo $child['name']; ?></a>
                        <?php if (!empty($child['children'])){ ?>
                            <img class="alkategoria_fokategoriaja" style="" src="catalog/view/theme/default/image/desc.png">
                        <?php } ?>
                    <?php } ?>

                    <?php if (isset($child["children"]) && $child["children"]){
                        category_children($child,$parts);
                    } ?>
                </li>
            <? } ?>
        </ul>
        <?
        return true;
    }
}