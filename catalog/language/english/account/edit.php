<?php
// Heading 
$_['heading_title']     = 'My Account Information';

// Text
$_['text_account']      = 'Account';
$_['text_edit']         = 'Edit Information';
$_['text_your_details'] = 'Your Personal Details';
$_['text_success']      = 'Success: Your account has been successfully updated.';

// Entry
$_['entry_adoszam']    = 'Tax Number:';
$_['entry_firstname']  = 'First Name:';
$_['entry_lastname']   = 'Last Name:';
$_['entry_email']      = 'E-Mail:';
$_['entry_telephone']  = 'Telephone:';
$_['entry_fax']        = 'Fax:';
$_['entry_adoszam']    = 'Tax number:';


// Error
$_['error_exists']     = 'Warning: E-Mail address is already registered!';
$_['error_firstname']  = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']   = 'Last Name must be between 1 and 32 characters!';
$_['error_email']      = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']  = 'Telephone must be between 3 and 32 characters!';
$_['entry_paypalemail']          = 'Paypal Account Email Id';

$_['select_ferfi']         = 'Men';
$_['select_no']            = 'Women';
$_['entry_nem']            = 'Sex:';
$_['entry_eletkor']        = 'Age:';
$_['error_eletkor']        = 'Please select the age of';
$_['error_nem']            = 'Please select the gender of';

$_['text_your_cegadatok']           = 'Company Informations';
$_['entry_iskolai_vegzettseg']      = 'Educational attainment';
$_['entry_vallalkozasi_forma']      = 'Business form:';
$_['entry_szekhely']                = 'Head office:';
$_['entry_ugyvezeto_neve']          = 'Manager name:';
$_['entry_ugyvezeto_telefonszama']  = 'Manager phone:';
$_['entry_company']  = 'Company name:';
$_['text_address']       = 'Modify your address book entries';

?>