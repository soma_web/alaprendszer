<?php echo $header; ?>

<?php
    function children($para,$margin) {
        if (isset($para['children']) && $para['children']) { ?>


            <?php foreach ($para['children'] as $child) { ?>

                <div style="margin-left: <?php echo $margin?>px">
                    <div class="category_sor">
                        <input type="checkbox" name="category_<?php echo $child['category_id'];?>" id="category_<?php echo $child['category_id'];?>"  class="category_chb" category_id="<?php echo $child['category_id']?>">
                        <label style=""  class="label_lenyit" ><?php echo $child['name']?></label>


                        <?php if (isset($child["children"]) && $child["children"]) { ?>
                            <img src="view/image/lock.png" class="img_lenyit" title="Lenyit"/>
                            </div>
                            <?php children($child,$margin+10); ?>
                        <?php } else { ?>
                            </div>
                        <?php } ?>
                </div>
            <?php } ?>
            <?php
            return true;
        }
    }
?>

<div id="content" class="termek_integracio_levalogatas">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>

    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="ajxMentes(  'index.php?route=module/termek_integracio/mentes',
                                        'index.php?route=module/termek_integracio<?php echo $url?>',
                                        'termek_integracio_id')"
                    class="button"><?php echo $button_save; ?></a>

                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
        <div class="send">
            <?php echo $text_magyarazat?>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                <tr>
                        <td><?php echo $entry_name; ?></td>
                        <td>
                            <input name="name" value="<?php echo $name?>">
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_status; ?></td>
                        <td>
                            <select name="status">
                                <?php if ($status) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_shipping ?></td>

                        <td>
                        <select name="shipping">
                            <?php foreach($shippings as $szallitas) { ?>
                                <option value="<?php echo $szallitas['file_name']?>" <?php echo $szallitas['file_name']==$shipping ? 'selected' : '';?>><?php echo $szallitas['name']?></option>
                            <?php } ?>
                        </select>
                        </td>
                    </tr>
                    <?php $display =  ($shipping == 'item_product' || $shipping == 'item_product_gls') ? '' : 'none';?>
                    <tr style="display:<?php echo $display?>" class="shipping_price">
                        <td><?php echo $entry_shipping_price ?></td>
                        <td>
                            <input name="shipping_price" value="<?php echo $shipping_price?>">
                        </td>
                    </tr>

                     <tr>
                        <td><?php echo $entry_shipping_time_in_stock ?></td>
                        <td><input name="shipping_time_in_stock" type="number" value="<?php echo $shipping_time_in_stock?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_shipping_time_not_stock ?></td>
                        <td><input name="shipping_time_not_stock" type="number" value="<?php echo $shipping_time_not_stock?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_warranty ?></td>
                        <td><input name="warranty" type="number" value="<?php echo $warranty?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_text_pieces ?></td>
                        <td><input name="pieces" type="number"  value="<?php echo $pieces?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_description ?></td>
                        <td><select name="description">
                            <option value="description" <?php echo $description=="description" ? 'selected' : '';?>><?php echo $text_description?></option>
                            <option value="short_description" <?php echo $description=="short_description" ? 'selected' : '';?>><?php echo $text_short_description?></option>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_pickpack ?></td>
                        <td><select name="pickpack">
                            <option value="1" <?php echo !empty($pickpack) ? 'selected' : '';?>><?php echo $text_yes?></option>
                            <option value="0" <?php echo empty($pickpack) ? 'selected' : '';?>><?php echo $text_no?></option>
                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_kapcsolodo ?></td>
                        <td>
                            <div id="engedelyezett_integraciok" class="scrollbox">
                                <?php $class = 'odd'; ?>
                                <?php foreach ($engedelyezett_integraciok as $integracio) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div class="<?php echo $class; ?>">
                                        <?php array_key_exists($integracio['type'],$connects) ? $checked = 'checked' : $checked = ''; ?>
                                        <input type="checkbox" name="connects[<?php echo $integracio['type']; ?>]" id="connects_<?php echo $integracio['type'];?>" class="integracio_chb" <?php echo $checked?>>
                                        <label for="connects_<?php echo $integracio['type']; ?>"><?php echo $integracio['name']; ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                </table>

                <table class="form levalogatas">

                    <tr>
                        <td><?php echo $entry_category; ?></td>
                        <td>
                            <div style="display: inline-block">
                                <div id="kategoria" class="scrollbox" style="">
                                    <div class="box-category" style="">
                                        <?php foreach ($categories as $category) { ?>
                                            <div class="fokategoria id_<?php echo $category['category_id'];?>">
                                                <div class="category_sor">
                                                    <input type="checkbox" name="category_<?php echo $category['category_id'];?>" id="category_<?php echo $category['category_id'];?>" class="category_chb" category_id="<?php echo $category['category_id']?>">
                                                    <label style=""  class="label_lenyit" ><?php echo $category['name']?></label>
                                                    <img src="view/image/lock.png" class="img_lenyit" title="Lenyit"/>
                                                </div>

                                                <?php children($category,10);?>
                                            </div>

                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div style="display: inline-block; vertical-align: top">
                                <img src="view/image/nagyitas.jpg" class="nagyito" onclick="Meretez(this,'kategoria')">
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_ar_tol_ig; ?></td>
                        <td>
                            <input type="text" name="termek_integracio_module[ar_tol]" class="post" value="<?php echo isset($modules['ar_tol']) ? $modules['ar_tol'] : 0?>" size="8" /> -
                            <input type="text" name="termek_integracio_module[ar_ig]" class="post" value="<?php echo isset($modules['ar_ig']) ? $modules['ar_ig'] : 0?>" size="8" />
                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_product_status; ?></td>
                        <td><select name="termek_integracio_module[product_status]" class="product_status">
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <option value="2"><?php echo $text_all; ?></option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <a onclick="levalogat()" class="button"><?php echo $button_levalogat; ?></a>
                        </td>
                    </tr>
                </table>

                <div id="termek_integracio">
                    <div class="termek_integracio">
                        <div class="fejlec">
                            <?php echo $entry_levalogatott; ?>
                        </div>
                        <div class="bezar">
                            <img src="view/image/close.png" class="img_close" title="Bezár"/>
                        </div>

                        <div id="termek_integracio_alap" class="scrollbox">
                            <?php $class = 'odd'; ?>
                            <?php if (isset($modules['altalanos']) && $modules['altalanos']) { ?>
                                <?php foreach ($modules['altalanos'] as $product) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div id="arajanlat_alap-product_<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>">
                                        <span><?php echo $product['name']; ?></span>
                                        <img src="view/image/success.png" class="img_success" title="Kiválaszt"/>
                                        <img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <a class="osszes">
                            <span class="not_underline"><?php echo $text_all_select_product.' 0 '.$text_darab; ?></span>
                            <span class="underline engedelyez" onclick="osszesEngedelyez(this,'altalanos')"><img src="view/image/success.png" title="Összes kiválasztása"/></span>
                            <span class="underline torlese" onclick="osszesTorlese('termek_integracio_alap')"><img src="view/image/delete.png" title="Összes eltávolítása"/></span>
                        </a>
                    </div>


                    <div class="termek_integracio">
                        <div class="fejlec">
                            <?php echo $entry_hozzaad_neve; ?>  <input type="text" name="hozzaad" value="" />
                            <?php echo $entry_hozzaad_model; ?>  <input type="text" name="hozzaad_model" value="" />
                        </div>
                        <div class="bezar">
                            <img src="view/image/close.png" class="img_close" title="Bezár"/>
                        </div>
                        <div id="termek_integracio_hozzaad" class="scrollbox">
                            <?php $class = 'odd'; ?>
                            <?php if (isset($modules['hozzaadott']) && $modules['hozzaadott']) { ?>
                                <?php foreach ($modules['hozzaadott'] as $product) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div id="arajanlat_hozzaadott-product_<?php echo $product['product_id']; ?>" class="<?php echo $class; ?> hozzaadott" product_id="<?php echo $product['product_id']?>">
                                        <span><?php echo $product['name']; ?></span>
                                        <img src="view/image/success.png" class="img_success" title="Kiválaszt"/>
                                        <img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <a class="osszes">
                            <span class="not_underline"><?php echo $text_all_select_product.' 0 '.$text_darab; ?></span>
                            <span class="underline engedelyez" onclick="osszesEngedelyez(this,'hozzaadott')"><img src="view/image/success.png" title="Összes kiválasztása"/></span>
                            <span class="underline torlese" onclick="osszesTorlese('termek_integracio_hozzaad')"><img src="view/image/delete.png" title="Összes eltávolítása"/></span>
                        </a>

                    </div>

                    <div style="display: inline-block">

                        <div class="termek_integracio">
                            <div class="fejlec">
                                <?php echo $entry_levalogatott; ?>
                            </div>
                            <div id="termek_integracio_vegleges" class="scrollbox">
                                <?php $class = 'odd'; ?>
                                <?php if (!empty($levalogatott) ) { ?>
                                    <?php foreach ($levalogatott as $product) { ?>
                                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                        <div id="arajanlat_levalogatott-product_<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>">
                                            <span><?php echo $product['name']; ?></span>
                                            <img src="view/image/delete.png" />
                                            <input type="hidden" class="levalogatott" name="levalogatott[]" value="<?php echo $product['product_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <a onclick="$(this).parent().find(':checkbox').attr('checked', true);">
                                <span class="not_underline"><?php echo $text_all_select_product.' '.count($levalogatott).' '.$text_darab;?></span>
                                <span class="underline torlese valogatott" onclick="osszesTorlese('termek_integracio_vegleges')"><img src="view/image/delete.png" title="Összes eltávolítása"/></span>

                            </a>
                        </div>
                    </div>
                    <div style="display: inline-block; vertical-align: top; position: relative; top: 20px;">
                        <img src="view/image/nagyitas.jpg" class="nagyito" onclick="Meretez(this,'termek_integracio_vegleges')">
                    </div>

                </div>
            </form>
        </div>
    </div>


    <script>

    termek_integracio_width = $('.termek_integracio').width();

    function levalogat() {
        filter_categorys = [];
        filter_ar = [];
        filter_levalogatott = [];

        $('.category_chb:checked').each(function(element) {
            filter_categorys.push($(this).attr('category_id'));
        });
        $('.post').each(function(element) {
            filter_ar.push(this.value);
        });
        $('.levalogatott').each(function(element) {
            filter_levalogatott.push(this.value);
        });
        filter_status = $('.product_status').val();

        $.ajax({
            url: 'index.php?route=module/termek_integracio/levalogat&token=<?php echo $token; ?>',
            type: 'POST',
            data:   'filter_categorys=' + filter_categorys.join(',') +
                    '&filter_ar='       + filter_ar.join(',')  +
                    '&filter_levalogatott='   + filter_levalogatott.join(',') +
                    '&filter_status='   + filter_status,

            dataType: 'json',
            beforeSend: function() {
                var height = $("body").height();
                var html = '';
                html += '<div class="takaro" style="height: '+height+'px">';
                    html += '<img class="wait" src="view/image/loading_tappancs.gif" alt="" />';
                html += '</div>';
                $("body").append(html);
                $(".takaro").slideDown("slow");

            },
            complete: function() {
                $(".takaro").slideUp("slow",function(){
                    $(".takaro").remove();
                });
            },
            success: function(json) {
                var html = "";
                var class_name = 'odd';
                for (var i=0; json['products'].length > i; i++) {
                    class_name = class_name == 'odd' ? 'even' : 'odd';
                    html += '<div id="arajanlat_alap-product_' +json['products'][i]['product_id']+ '" class="'+class_name+'">';
                        html += '<span>';
                            html += json['products'][i]['name'];
                        html += '</span>';
                        html += '<img src="view/image/success.png" class="img_success" title="Kiválaszt"/>';
                        html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
                    html += '</div>';

                }
                $("#termek_integracio_alap").html(html);
                $('#termek_integracio_alap').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+json['products'].length+"<?php echo ' '.$text_darab; ?>");

            },
            error: function(e) {
                debugger;
            }

        });
    }


    $(document).delegate('#termek_integracio_alap div .img_remove','click', function() {
        $(this).parent().remove();
        $('#termek_integracio_alap').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_alap > div').length+"<?php echo ' '.$text_darab; ?>");

        $('#termek_integracio_alap div:odd').attr('class', 'odd');
        $('#termek_integracio_alap div:even').attr('class', 'even');
    });
    function osszesTorlese(para) {
        debugger;
        $('#'+para+' div').remove();
        $('#'+para).parent().find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_alap > div').length+"<?php echo ' '.$text_darab; ?>");
    }

    $(document).delegate('#termek_integracio_alap div .img_success','click', function() {
        var product_id      = $(this).parent().attr('id').replace('arajanlat_alap-product_','');
        var product_name    = $(this).parent().children()[0].innerHTML;
        var html = "";
        html += '<div id="arajanlat_levalogatott-product_' +product_id+ '">';
            html += '<span>'+product_name+'</span>';
            html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
            html += '<input type="hidden"  class="levalogatott"  name="levalogatott[]" value="'+product_id+'" />';
        html += '</div>';
        $("#arajanlat_levalogatott-product_" +product_id).remove();
        $('#termek_integracio_alap').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_alap > div').length+"<?php echo ' '.$text_darab; ?>");

        $("#termek_integracio_vegleges").append(html);
        $('#termek_integracio_vegleges').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_vegleges > div').length+"<?php echo ' '.$text_darab; ?>");
        $('#termek_integracio_vegleges div:odd').attr('class', 'odd');
        $('#termek_integracio_vegleges div:even').attr('class', 'even');

        $(this).parent().remove();
        $('#termek_integracio_alap div:odd').attr('class', 'odd');
        $('#termek_integracio_alap div:even').attr('class', 'even');

    });




    $(document).delegate('#termek_integracio_hozzaad .img_remove','click', function() {
        $(this).parent().remove();
        $('#termek_integracio_hozzaad').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_hozzaad > div').length+"<?php echo ' '.$text_darab; ?>");

        $('#termek_integracio_hozzaad div:odd').attr('class', 'odd hozzaadott');
        $('#termek_integracio_hozzaad div:even').attr('class', 'even hozzaadott');
    });

    $(document).delegate('#termek_integracio_hozzaad .img_success','click', function() {
        $(this).parent().remove();

        $('#termek_integracio_hozzaad div:odd').attr('class', 'odd hozzaadott');
        $('#termek_integracio_hozzaad div:even').attr('class', 'even hozzaadott');



        var product_id      = $(this).parent().attr('id').replace('arajanlat_hozzaadott-product_','');
        var product_name    = $(this).parent().children()[0].innerHTML;

        var html = "";
        html += '<div id="arajanlat_levalogatott-product_' +product_id+ '">';
        html += '<span>'+product_name+'</span>';
            html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
            html += '<input type="hidden"  class="levalogatott"  name="levalogatott[]" value="'+product_id+'" />';
        html += '</div>';
        $("#arajanlat_levalogatott-product_" +product_id).remove();
        $('#termek_integracio_hozzaad').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_hozzaad > div').length+"<?php echo ' '.$text_darab; ?>");

        $("#termek_integracio_vegleges").append(html);
        $('#termek_integracio_vegleges').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_vegleges > div').length+"<?php echo ' '.$text_darab; ?>");

        $('#termek_integracio_vegleges div:odd').attr('class', 'odd');
        $('#termek_integracio_vegleges div:even').attr('class', 'even');
    });




    $(document).delegate('#termek_integracio_vegleges div img','click', function() {
        $(this).parent().remove();

        $('#termek_integracio_vegleges div:odd').attr('class', 'odd');
        $('#termek_integracio_vegleges div:even').attr('class', 'even');


        $('#termek_integracio_vegleges').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_vegleges > div').length+"<?php echo ' '.$text_darab; ?>");

    });

    $(document).delegate('input[name=\'hozzaad_model\']','click', function(e){$(this).autocomplete("search");});
    $(document).delegate('input[name=\'hozzaad\']','click', function(e){$(this).autocomplete("search");});


    $(document).delegate('input[name=\'hozzaad\']','click', function(e){
        $(this).autocomplete({


            delay: 0,
            source: function(request, response) {

                filter_kivetel = [];
                $('.hozzaadott').each(function(element) {
                    filter_kivetel.push($(this).attr('product_id'));
                });
                $('.levalogatott').each(function(element) {
                    filter_kivetel.push($(this).val());
                });

                $.ajax({
                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                    data: 'filter_kivetel=' + filter_kivetel.join(','),
                    type: 'post',
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item.name,
                                value: item.name,
                                ertek: item.product_id,
                                model: item.model
                            }
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $(this).blur();

                $('#arajanlat_hozzaadott-product_' + ui.item.ertek).remove();
                var html = '';
                   html ='<div id="arajanlat_hozzaadott-product_' + ui.item.ertek + '" product_id="'+ ui.item.ertek +'">';
                    html += '<span>'+ui.item.label+'</span>';
                    html += '<img src="view/image/success.png" class="img_success" title="Kiválaszt"/>';
                    html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
                html += '</div>';
                debugger;
                $('#termek_integracio_hozzaad').append(html);

                $('#termek_integracio_hozzaad').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_hozzaad > div').length+"<?php echo ' '.$text_darab; ?>");

                $('#termek_integracio_hozzaad div:odd').attr('class', 'odd hozzaadott');
                $('#termek_integracio_hozzaad div:even').attr('class', 'even hozzaadott');

                return false;
            }
        });
    });

    $(document).delegate('input[name=\'hozzaad_model\']','click', function(e){
        $(this).autocomplete({
            delay: 0,
            source: function(request, response) {
                filter_kivetel = [];
                $('.hozzaadott').each(function(element) {
                    filter_kivetel.push($(this).attr('product_id'));
                });
                 $('.levalogatott').each(function(element) {
                    filter_kivetel.push($(this).val());
                });

                $.ajax({
                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request.term),
                    data: 'filter_kivetel=' + filter_kivetel.join(','),
                    type: 'post',
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item.name,
                                value: item.name,
                                ertek: item.product_id,
                                model: item.model
                            }
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $(this).blur();
                $('#arajanlat_hozzaadott-product_' + ui.item.ertek).remove();
                var html = '';
                   html ='<div id="arajanlat_hozzaadott-product_' + ui.item.ertek + '" product_id="'+ ui.item.ertek +'">';

                    html += '<span>'+ui.item.label+'</span>';
                    html += '<img src="view/image/success.png" class="img_success" title="Kiválaszt"/>';
                    html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
                html += '</div>';
                $('#termek_integracio_hozzaad').append(html);

                $('#termek_integracio_hozzaad').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_hozzaad > div').length+"<?php echo ' '.$text_darab; ?>");

                $('#termek_integracio_hozzaad div:odd').attr('class', 'odd hozzaadott');
                $('#termek_integracio_hozzaad div:even').attr('class', 'even hozzaadott');

                return false;
            }
        });
    });

    $(document).delegate('.termek_integracio .img_close','click', function() {

        $(this).closest('.termek_integracio').css('overflow','hidden');

        $(this).closest('.termek_integracio').animate({
            width: "5%",
            opacity: 0.6,
            height: "50px"
        }, 700 );

        $(this).closest('.termek_integracio').find('.scrollbox').css('opacity','0.3');
        $(this).closest('.termek_integracio').find('.scrollbox').css('width','auto');
        $(this).closest('.termek_integracio').find('.fejlec').html('<img src="view/image/nagyitas.jpg" class="img_nagyit" title="Nagyitas"/>');
        $(this).closest('.termek_integracio').find('.fejlec').css('text-align','center');
        $(this).closest('.termek_integracio').find('.fejlec').css('padding','15px 0 0px 0');
        $(this).parent().html('');
    });


    $(document).delegate('.termek_integracio .img_nagyit','click', function() {
        $(this).closest('.termek_integracio').animate().css('overflow','visible');

        $(this).closest('.termek_integracio').animate({
            width: termek_integracio_width,
            opacity: 1,
            height: "375px",
            complete: function() {
                $(this).closest('.termek_integracio').css('overflow','visible');
            }
        }, 700 );

        //$(this).closest('.termek_integracio').animate().css('overflow','visible');


        $(this).closest('.termek_integracio').find('.scrollbox').css('opacity','1');
        $(this).closest('.termek_integracio').find('.scrollbox').css('width','auto');

        $(this).closest('.termek_integracio').find('.fejlec').css('text-align','left');
        $(this).closest('.termek_integracio').find('.fejlec').css('padding','');

        $(this).closest('.termek_integracio').find('.bezar').html('<img src="view/image/close.png" class="img_close" title="Bezár"/>');
        if (  $(this).closest('.termek_integracio').find('#termek_integracio_alap').length > 0 ) {
            $(this).closest('.termek_integracio').find('.fejlec').html('<?php echo $entry_levalogatott; ?>');
        } else {
            $(this).closest('.termek_integracio').find('.fejlec').html('<?php echo $entry_hozzaad_neve; ?>  <input type="text" name="hozzaad" value="" /> <?php echo $entry_hozzaad_model; ?>  <input type="text" name="hozzaad_model" value="" />');
        }


    });

    $('.box-category input[type="checkbox"]').bind('click',function(){
        if ( $(this).prop('checked') == true) {
            $(this).prop('checked', true);
            $(this).parent().parent().find('input').prop('checked', true);
        } else {
            $(this).prop('checked', false);
            $(this).parent().parent().find('input').prop('checked', false);
        }
    });

    $(document).delegate('.box-category .img_lenyit','click', function() {
        $(this).parent().parent().children('div').slideDown('fast');
        $(this).attr('src','view/image/lock-open.png');
        $(this).attr('class','img_bezar');
        $(this).attr('title','Bezar');
        $(this).prev().attr('class','label_bezar');
    });
    $(document).delegate('.box-category .label_lenyit','click', function() {
        $(this).parent().parent().children('div').slideDown('fast');
        $(this).next().attr('src','view/image/lock-open.png');
        $(this).next().attr('class','img_bezar');
        $(this).next().attr('title','Bezar');
        $(this).attr('class','label_bezar');
    });



    $(document).delegate('.box-category .img_bezar','click', function() {
        $(this).parent().parent().children('div').not(':first').slideUp('fast');
        $(this).attr('src','view/image/lock.png');
        $(this).attr('class','img_lenyit');
        $(this).attr('title','Lenyit');
        $(this).prev().attr('class','label_lenyit');

    });
    $(document).delegate('.box-category .label_bezar','click', function() {
        $(this).parent().parent().children('div').not(':first').slideUp('fast');
        $(this).next().attr('src','view/image/lock.png');
        $(this).next().attr('class','img_lenyit');
        $(this).next().attr('title','Lenyit');
        $(this).attr('class','label_lenyit');
    });

    function Meretez(para,para1) {
        if ( $("#"+para1).hasClass("nagyitott")) {
            $(".scrollbox").removeClass("nagyitott");
            $(para).attr("src","view/image/nagyitas.jpg");
            $('html,body').animate({scrollTop: $("#"+para1).offset().top - 40},'slow');
        } else {
            $(".scrollbox").removeClass("nagyitott");
            $("img.nagyito").attr("src","view/image/nagyitas.jpg");
            $(para).attr("src","view/image/kicsinyites.jpg");
            $("#"+para1).addClass("nagyitott");
            $('html,body').animate({scrollTop: $("#"+para1).offset().top - 50},'slow');
        }
    }

    function osszesEngedelyez(para) {

        var height = $("body").height();
        var html = '';
        html += '<div class="takaro" style="height: '+height+'px">';
            html += '<img class="wait" src="view/image/loading_tappancs.gif" alt="" />';
        html += '</div>';
        $("body").append(html);
        $(".takaro").slideDown("slow");


        var html = "";
        $(para).closest('.termek_integracio').find('.scrollbox div').each(function(element) {
            //var product_id      = $(this).find('input[type=hidden]').val();
            debugger;
            var product_id      = $(this).attr('id').replace('arajanlat_alap-product_','');
            product_id = product_id.replace('arajanlat_hozzaadott-product_','');

            var product_name    = $(this).find('span').html();

            html += '<div id="arajanlat_levalogatott-product_' +product_id+ '">';
            html += '<span>'+product_name+'</span>';
                html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
                html += '<input type="hidden"  class="levalogatott"  name="levalogatott[]" value="'+product_id+'" />';
            html += '</div>';
            $("#arajanlat_levalogatott-product_" +product_id).remove();
        });


        $("#termek_integracio_vegleges").append(html);
        $('#termek_integracio_vegleges div:odd').attr('class', 'odd');
        $('#termek_integracio_vegleges div:even').attr('class', 'even');

        $(para).closest('.termek_integracio').find('.scrollbox div').remove();
        $(para).closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' 0 '.$text_darab; ?>");

        $('#termek_integracio_vegleges').closest('.termek_integracio').find('.not_underline').text("<?php echo $text_all_select_product.' ';?>"+$('#termek_integracio_vegleges > div').length+"<?php echo ' '.$text_darab; ?>");

        $(".takaro").slideUp("slow",function(){
            $(".takaro").remove();
        });
    }

    $('select[name="shipping"]').on('change',function(){
        var ertek = $(this).val();
        if (ertek == 'item_product' || ertek == 'item_product_gls') {
            $('.shipping_price').fadeIn('slow');
        } else {
            $('.shipping_price').fadeOut('slow');
        }
    });

</script>
<?php require_once('view/javascript/common.php')?>

<?php echo $footer; ?>