<?php
class ControllerModulekiemeltmenusor3 extends Controller {
	public function index() {
		$this->language->load('module/kiemeltmenusor3'); 

      	$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
        $this->data['kaphato'] = $this->language->get('kaphato');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
        $this->data['button_bovebben'] = $this->language->get('button_bovebben');
        $this->data['text_tax'] = $this->language->get('text_tax');
        $this->data['text_netto'] = $this->language->get('text_netto');
        $this->data['module_name'] = "kiemeltmenusor3";
        $this->data['button_wishlist'] = $this->language->get('button_wishlist');
        $this->data['button_compare'] = $this->language->get('button_compare');
        $this->data['text_empty'] = $this->language->get('text_empty');
        $this->data['button_continue'] = $this->language->get('button_continue');

        $this->load->model('catalog/product');
		
		$this->load->model('tool/image');


        $setting = $this->config->get('kiemeltmenusor3_module_menusor');

        $this->data['aktiv_class'] = "kiemeltmenusor3";
        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $setting['fejlec'],
            'href'      => $this->url->link('module/kiemeltmenusor3'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['products'] = array();
		$products = explode(',', $this->config->get('kiemeltmenusor3_product'));
        $termekek = array();

        foreach ($products as $termek) {
            $termeksor = $this->model_catalog_product->getProduct($termek);
            $termekek[] = array(
                'id' => $termek,
                'ingyenes' => $termeksor['utalvany']
            );
        }

        $sortingSettings = array(
            0       =>  array(
                'orderby' =>  'ingyenes',
                'sortorder'     => 'ASC'
            )
        );

        $rendezo = new ArrayOfArrays($termekek);
        $rendezo->multiSorting($sortingSettings,true);
        $termekek = $rendezo->getArrayCopy();

        $products = array();
        foreach ($termekek as $value) {
            $products[] = $value['id'];
        }

        //ThemeGlobal.com - OpenCart Templates Club. Unlimited access to all of our themes for only $49
        if (isset($setting['position'])) {
            $this->data['position'] = $setting['position'];
        }
        $this->document->addScript('catalog/view/javascript/jquery/jquery.jcarousel.min22.js');

        if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/carousel22.css')) {
            $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/carousel22.css');
        } else {
            $this->document->addStyle('catalog/view/theme/default/stylesheet/carousel22.css');
        }
        //ThemeGlobal.com - OpenCart Templates Club. Unlimited access to all of our themes for only $49


        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => isset($setting['limit']) ? $setting['limit'] : 20
        );

        $result1 = $this->model_catalog_product->getProductsNew($data1);

        $this->data['position'] = isset($setting['position']) ? $setting['position'] : 0 ;
        if ($this->config->get('megjelenit_korhinta') == 1) {
            if (isset($setting['show']) ) {
                $this->data['show'] = $setting['show'];
            } else {
                $this->data['show'] = 0;
            }
        } else {
            $this->data['show'] = 0;
        }
        foreach ($products as $product_id) {
			$result = $this->model_catalog_product->getProduct($product_id);
            if ($result) {
                $this->data['products'][] = $this->product->productPreparation($result,$setting,$result1);
            }
		}
        $this->data['products'] = $this->config->rendezes( $this->data['products'], "kiemelt_sorrend","DESC","kiemelt_al_sorrend","DESC");

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/kiemeltmenusor.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/kiemeltmenusor.tpl';
		} else {
			$this->template = 'default/template/module/kiemeltmenusor.tpl';
		}

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
	}
}
?>