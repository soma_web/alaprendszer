<?php

// Heading
$_['heading_title']         = 'Ajánlatkérések';

// Text
$_['text_success']          = 'Siker! Módosította az előrendeléseket!';

$_['column_pre_order_id']     = 'Azonosító';
$_['column_customer']         = 'Ügyfél';
$_['column_email']            = 'E-Mail';
$_['column_date_added']       = 'Előrendelés dátuma';
$_['column_date_modified']    = 'Módosítás dátuma';
$_['column_product_name']     = 'Termék neve';
$_['column_model']            = 'Modell';
$_['column_action']           = 'Művelet';



$_['entry_pre_order_id']       = 'Előrendelés azonosító:';
$_['entry_product_id']         = 'Termék azonosító:';
$_['entry_model']              = 'Modell:';
$_['entry_cikkszam']           = 'Cikkszám:';
$_['entry_product_name']       = 'Termék neve:';
$_['entry_gyarto']             = 'Gyártó:';
$_['entry_megrendelo_name']    = 'Előrendelő neve:';
$_['entry_megrendelo_email']   = 'Előrendelő e-mail:';
$_['entry_megrendelo_uzenet']  = 'Előrendelő üzenete:';
$_['entry_date_added']         = 'Előrendelés dátuma:';
$_['entry_date_modified']      = 'Utolsó módosítás dátuma:';
$_['entry_sajat_megjegyzes']   = 'Áruházi megjegyzés:';
$_['entry_telefon']            = 'Telefonszám:';
$_['entry_varos']       = 'Város:';
$_['entry_iranyitoszam']= 'Irányítószám:';
$_['entry_ceg']         = 'Cég neve:';
$_['entry_adoszam']     = 'Cég adószáma:';
$_['entry_utca']        = 'Utca házszám:';

$_['error_permission']      = 'Figyelmeztetés: Nincs engedélye az előrendelés módosítására!';

?>
