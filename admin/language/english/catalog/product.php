<?php
// Heading
$_['heading_title']          = 'Products'; 

// Text  
$_['text_success']           = 'Success: You have modified products!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse Files';
$_['text_clear']             = 'Clear Image';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';


$_['text_szuro_csoport']    = 'Cashing place:';
$_['text_szuro']            = 'Redeem points:';
$_['text_kaphato']          = 'Redeemable:';
$_['text_utalvany_erteke']  = 'The voucher value:';
$_['text_fizetes_alatt']     = 'Payment under';
$_['text_kifizetve']         = 'Paid';

// Column
$_['column_garancia_ertek']  = 'Additional warranty';
$_['column_name']            = 'Product Name';
$_['column_date_modify']     = 'Last Updated on';
$_['column_date_added']      = 'Creation date';
$_['column_model']           = 'Model';
$_['column_cikkszam']        = 'Product item number:';
$_['column_cikkszam2']       = 'Item number:';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';
$_['column_kifuto']          = 'End-product';
$_['column_ujdonsag']        = 'Novelty';
$_['column_elorendeles']     = 'Pre-order';
$_['column_product_id']      = 'Product ID';

// Entry
$_['entry_helyettesito']     = 'Acceptable product:';
$_['entry_ajandek']          = 'Largesse:';
$_['entry_stock_date']       = 'Expected arrival:';
$_['entry_lizing']           = 'Lease:';
$_['entry_kifuto']           = 'Discontinued product:';
$_['entry_elorendeles']      = 'Only available:';
$_['entry_kiszereles']       = 'Packaging:';
$_['entry_kiszereles_mertekegyseg'] = 'Packaging Unit:';

$_['entry_ujdonsag']         = 'Novelty:';
$_['entry_garancia']         = 'Warranty:';
$_['entry_garancia_honap']   = 'Month';
$_['entry_garancia_ev']      = 'Year';
$_['entry_accessory']        = 'Recommended accessories:';
$_['entry_general_accessory']= 'Recommended general accessories:';
$_['entry_idoszak']          = 'Period';
$_['entry_idoszak_darab']    = 'Quantity';
$_['entry_kep_tiltas']       = 'Pub restricted:';
$_['entry_image_letoltheto'] = 'Related to download image:';
$_['entry_name']             = 'Product Name:';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'Description:';
$_['entry_store']            = 'Stores:';
$_['entry_keyword']          = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_fizetendo']        = 'Price:';
$_['entry_model']            = 'Model:';
$_['entry_cikkszam']         = 'Product number:';
$_['entry_cikkszam2']         = 'Product number 2:';
$_['entry_sku']              = 'SKU:';
$_['entry_upc']              = 'UPC:';
$_['entry_date_kaphato_ig']  = 'How available:';
$_['entry_eredeti_ar']       = 'Original price:';
$_['entry_utalvany']         = 'Voucher:';
$_['entry_termek_vevo']      = 'Related partners:';
$_['entry_kiemelt']          = 'Featured:';
$_['entry_short_description']= 'Short description:';
$_['entry_package']          = 'Package offer:';
$_['entry_test_description'] = 'Tests';
$_['entry_garancia_description']= 'Additional warranty description:';
$_['entry_szazalek_kedvezmeny']= 'Maximum discount is granted:';
$_['entry_size_color_required']= 'Size - color required:';
$_['entry_egyszer_kosarba']  = 'It can be made only once in basket:';


$_['entry_location']         = 'Location:';
$_['entry_manufacturer']     = 'Manufacturer:';
$_['entry_filter']           = 'Filter:';
$_['entry_shipping']         = 'Requires Shipping:';
$_['entry_date_available']   = 'Date Available:';
$_['entry_quantity']         = 'Quantity:';
$_['mennyisegi_egyseg']      = 'Quantitative device:';
$_['csomagolasi_egyseg']     = 'Packing unit:';
$_['csomagolasi_mennyiseg']  = 'Csomagolási_mennyiség:';
$_['entry_fizetes_status']   = 'Payment status:';


$_['elhelyezkedesek_kulonbseg_error']         = 'Warning! There is no free space in a period.';
$_['elhelyezkedes_alcsoport_kulonbseg_error'] = 'Warning! There is no free space in a period.';
$_['kiemelesek_kulonbseg_error']              = 'Warning! In that period there is no possibility for emphasis.';

$_['entry_minimum']          = 'Minimum Quantity:<br/><span class="help">Force a minimum ordered amount</span>';
$_['entry_stock_status']     = 'Out Of Stock Status:<br/><span class="help">Status shown when a product is out of stock</span>';
$_['entry_price']            = 'Price:';
$_['entry_tax_class']        = 'Tax Class:';
$_['entry_points']           = 'Points:<br/><span class="help">Number of points needed to buy this item. If you don\'t want this product to be purchased with points leave as 0.</span>';
$_['entry_option_points']    = 'Points:';
$_['entry_subtract']         = 'Subtract Stock:';
$_['entry_weight_class']     = 'Weight Class:';
$_['entry_weight']           = 'Weight:';
$_['entry_length']           = 'Length Class:';
$_['entry_dimension']        = 'Dimensions (L x W x H):';
$_['entry_image']            = 'Image:';
$_['entry_customer_group']   = 'Customer Group:';
$_['entry_date_start']       = 'Date Start:';
$_['entry_date_end']         = 'Date End:';
$_['entry_priority']         = 'Priority:';
$_['entry_attribute']        = 'Attribute:';
$_['entry_attribute_group']  = 'Attribute Group:';
$_['entry_text']             = 'Text:';
$_['entry_option']           = 'Option:';
$_['entry_option_value']     = 'Option Value:';
$_['entry_required']         = 'Required:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Sort Order:';
$_['entry_category']         = 'Categories:';
$_['entry_download']         = 'Downloads:';
$_['entry_related']          = 'Related Products:<br /><span class="help">(Autocomplete)</span>';
$_['entry_tag']          	 = 'Product Tags:<br /><span class="help">comma separated</span>';
$_['entry_reward']           = 'Reward Points:';
$_['entry_layout']           = 'Layout Override:';
$_['vevo_kedvezmeny']        = 'Customer:';
$_['entry_szazalek']         = 'Percentage understood Price:';
$_['entry_uj']               = 'New';
$_['entry_hasznalt']         = 'Used';
$_['entry_berelheto']        = 'Rent';
$_['entry_allapot']          = 'State';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 3 and less than 64 characters!';

$_['error_price']            = 'Please enter the price!';
$_['error_negativ_ar']       = 'The price must be greater than 0!';
$_['error_product_filter']   = 'Please select at least one sector!';
$_['error_date_ervenyes_ig'] = 'Please enter your date of validity!';
$_['error_letoltheto_kep']   = 'Please upload a coupon!';


// Elhelyezkedés

$_['entry_elhelyezkedes']           = 'Location I.:';
$_['entry_elhelyezkedes_alcsoport'] = 'Location II.:';
$_['entry_kiemelt_keret']           = 'Border:';
$_['entry_idoszak']          = 'Period';
$_['entry_idoszak_darab']    = 'Quantity';

$_['text_nap']               = 'Day';
$_['text_het']               = 'Seven';
$_['text_honap']             = 'Month';
$_['text_ev']                = 'Year';

$_['tab_kepek_helyszinenkent']      = 'Pictures by locations';
$_['entry_helyszin']                = 'Locations';
$_['entry_generalt_nev']            = 'Generated name';

$_['text_valasszon']                = '-- Choose --';
$_['text_import_valasszon']         = '-- Choose (Import) --';
$_['text_termekjellemzok']          = 'Product features';
$_['text_kapcsolodotermekek']       = 'Related products';
$_['text_ajanlott_tartozekok']      = 'Recommended accessories';
$_['text_altalanos_tartozekok']     = 'General accessories';
$_['text_engedelyezett']            = 'Allowed';
$_['text_letiltott']                = 'Banned';
$_['text_file_feltoltes']           = 'File upload';
$_['text_file_importalas']          = 'File import';
$_['text_kerem_toltsefel']          = 'Please upload the file!';

//Video fül
$_['button_add_video']            = 'Add video';
$_['button_add_pdf']              = 'Add pdf file';
$_['entry_video']                 = 'Video:';
$_['entry_pdf']                   = 'Pdf:';
$_['tab_videok']                  = 'Video';
$_['tab_pdf']                     = 'Pdf file upload';
$_['entry_video_link']            = 'Video URL';
$_['entry_pdf_link']              = 'Pdf URL';
$_['entry_video_title']           = 'Video title';
$_['entry_pdf_title']             = 'Pdf title';

?>