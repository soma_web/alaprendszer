<?php
class ModelTermekek {

    private $db;
    public $termek_tart = 0;

    public function __construct($query) {
        $this->db = $query;
    }

    public function termekek() {

        while ($query = $this->termekBeolvas()) {
            foreach($query as $value) {
                if ($value['image'] && substr($value['image'],0,14) ==  'data/videopart') {
                    $category = $this->getCategory($value['product_id']);
                    if ($category) {

                        if (!is_dir(DIR_IMAGE.'data/video_products')) {
                            mkdir(DIR_IMAGE.'data/video_products', 0755);
                        }
                        if (!is_dir(DIR_IMAGE.'data/video_products/'.$category)) {
                            mkdir(DIR_IMAGE.'data/video_products/'.$category, 0755);
                        }

                        $image = basename($value['image']);
                        if (file_exists(DIR_IMAGE.$value['image'])) {
                            rename(DIR_IMAGE.$value['image'],DIR_IMAGE.'data/video_products/'.$category.'/'.$image);
                        }
                        $sql = "UPDATE ".DB_PREFIX."product SET
                            image = 'data/video_products/".$category."/".$image."'
                            WHERE product_id = ".$value['product_id'];
                        $query_update = $this->db->query($sql);
                    }

                }

            }
        }

        $this->termek_tart = 0;

        while ($query = $this->termekKepekBeolvas()) {
            foreach($query as $value) {
                if ($value['image'] && substr($value['image'],0,14) ==  'data/videopart') {
                    $category = $this->getCategory($value['product_id']);
                    if ($category) {

                        if (!is_dir(DIR_IMAGE.'data/video_products')) {
                            mkdir(DIR_IMAGE.'data/video_products', 0755);
                        }
                        if (!is_dir(DIR_IMAGE.'data/video_products/'.$category)) {
                            mkdir(DIR_IMAGE.'data/video_products/'.$category, 0755);
                        }

                        $image = basename($value['image']);
                        if (file_exists(DIR_IMAGE.$value['image'])) {
                            rename(DIR_IMAGE.$value['image'],DIR_IMAGE.'data/video_products/'.$category.'/'.$image);
                        }
                        $sql = "UPDATE ".DB_PREFIX."product_image SET
                            image = 'data/video_products/".$category."/".$image."'
                            WHERE product_image_id = ".$value['product_image_id'];
                        $query_update = $this->db->query($sql);
                    }

                }

            }
        }


    }

    public function termekBeolvas() {
        $meddig = (int)$this->termek_tart+100;
        $sql = "SELECT product_id, image FROM ".DB_PREFIX."product LIMIT " . (int)$this->termek_tart . "," . $meddig;
        $query = $this->db->query($sql);
        $this->termek_tart += 100;

        return $query && $query->num_rows ? $query->rows : false;
    }

    public function termekKepekBeolvas() {
        $meddig = (int)$this->termek_tart+100;
        $sql = "SELECT product_image_id, product_id, image FROM ".DB_PREFIX."product_image LIMIT " . (int)$this->termek_tart . "," . $meddig;
        $query = $this->db->query($sql);
        $this->termek_tart += 100;

        return $query && $query->num_rows ? $query->rows : false;
    }

    public function getCategory($product_id) {
        $sql = "SELECT p2c.product_id, p2c.category_id, c.parent_id, cd.name FROM ".DB_PREFIX."product_to_category p2c
            INNER JOIN ".DB_PREFIX."category c ON (p2c.category_id = c.category_id)
            INNER JOIN ".DB_PREFIX."category_description cd ON (p2c.category_id = cd.category_id AND cd.language_id = 2)
            WHERE p2c.product_id =" .$product_id;
        $query = $this->db->query($sql);
        $vissza = false;

        if ($query) {
            foreach($query->rows as $value) {
                if($value['parent_id'] == 0) {
                    $vissza = $this->ekezetNelkul($value['name']);
                    return $vissza;
                }
            }

            while (true) {
                $sql = "SELECT category_id, parent_id FROM ".DB_PREFIX."category
                        WHERE category_id = " . $query->row['parent_id'];
                $query = $this->db->query($sql);
                if (!$query || $query->row['parent_id'] == 0) {
                    break;
                }
            }

            $sql = "SELECT c.category_id, c.parent_id, cd.name FROM ".DB_PREFIX."category c
            INNER JOIN ".DB_PREFIX."category_description cd ON (c.category_id = cd.category_id AND cd.language_id = 2)
            WHERE c.category_id =" .$query->row['category_id'];
            $query = $this->db->query($sql);
            if ($query) {
                $vissza = $this->ekezetNelkul($query->row['name']);
                return $vissza;
            }

        }

        return $vissza;
    }

    public function ekezetNelkul($name) {
        $CHARMAP = array(
            'ö' => 'o',
            'Ö' => 'O',
            'ó' => 'o',
            'Ó' => 'O',
            'ő' => 'o',
            'Ő' => 'O',
            'ú' => 'u',
            'Ú' => 'U',
            'ű' => 'u',
            'Ű' => 'U',
            'ü' => 'u',
            'Ü' => 'U',
            'á' => 'a',
            'Á' => 'A',
            'é' => 'e',
            'É' => 'E',
            'í' => 'i',
            'Í' => 'I',
        );

        $name = strtr($name, $CHARMAP);
        $name = str_replace(" ","_",$name);
        $name = str_replace("/","_",$name);
        $name = str_replace("'","_",$name);
        $name = str_replace('"',"_",$name);
        $name = str_replace(',',"",$name);
        return $name;
    }
}