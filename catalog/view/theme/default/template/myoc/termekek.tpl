<?php echo $header; ?>
<?php if(isset($error_customer_group)) { ?>
<div class="attention"><?php echo $error_customer_group; ?></div>
<?php } ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php  echo $content_top; ?>

    <h1><?php echo $heading_title; ?></h1>
    <?php if (!isset($error_customer_group)) { ?>
    <style type="text/css">
    .filter-control {
        padding: 5px 0;
        border-bottom: 1px solid #ddd;
        overflow: auto;
    }


    .fleft {
        float: left;
    }
    .fright {
        float: right;
    }

    </style>
    <div class="filter-control">
        <div class="fleft">
          <strong><?php echo $text_category; ?></strong> <select name="category_id" onchange="location = this.value;">
            <?php foreach($categories as $category){ ?>
            <option value="<?php echo $category['href']; ?>"<?php if($catid == $category['category_id']) { ?> selected="selected"<?php } ?>><?php echo $category['path']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="fright">
          <strong><?php echo $text_limit; ?></strong> <select name="limit" onchange="location = this.value;">
            <?php foreach($limits as $limit_value) { ?>
            <option value="<?php echo $limit_value['href']; ?>"<?php if($limit == $limit_value['value']) { ?> selected="selected"<?php } ?>><?php echo $limit_value['value']; ?></option>
            <?php } ?>
          </select>
        </div>
    </div>
    <div class="pagination"><?php echo $pagination; ?></div>

    <!--<div class="print-control">
         <div class="fright">
             <a style="padding: 4px 20px" href="<?php echo $print; ?>" class="button" target="_blank"><span><?php echo $text_print; ?></span></a>
         </div>
     </div>-->

    <div class="box-product">

        <?php if(!empty($products)) { ?>
            <?php foreach($products as $product) { ?>
                <div>
                    <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                    } else {
                        $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                    } ?>
                    <?php include($arkiir); ?>
                </div>
            <?php } ?>
        <?php } else { ?>
            <tr><td colspan="8" class="tcenter"><?php echo $text_empty; ?></td></tr>
        <?php } ?>
    </div>
      <div class="pagination"><?php echo $pagination; ?></div>
      <div style="position: relative; display: block; margin-bottom: 160px;"></div>

    <script type="text/javascript"><!--



    if(typeof $.fancybox == 'function') {
        $('.fancybox').fancybox({cyclic: true});
    }


    //--></script>
    <?php } else { ?>
        <div class="content"><?php echo $text_empty; ?></div>
        <div class="buttons">
            <div class="right"><a href="<?php echo $continue; ?>" class="button"><span><?php echo $button_continue; ?></span></a></div>
        </div>
    <?php } ?>
    <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>