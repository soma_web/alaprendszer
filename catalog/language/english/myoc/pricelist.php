<?php
// Heading
$_['heading_title']     	= 'Price List';

// Header Bar Title
$_['text_all_products'] 	= 'All Products';

// Text
$_['text_pricelist']    	= 'Price List';
$_['text_limit']        	= 'Products per Page:';
$_['text_empty']        	= 'Products not found.';
$_['text_category']     	= 'Category:';
$_['text_sku']          	= 'SKU:';
$_['text_upc']          	= 'UPC:';
$_['text_print']        	= 'Print';
$_['text_notfound']     	= 'Product Not Found!';
$_['text_discount']     	= '%s or more: %s';

//Table Columns
$_['column_number']     	= 'No.';
$_['column_image']      	= 'Image';
$_['column_name']       	= 'Name';
$_['column_model']      	= 'Model';
$_['column_rating']      	= 'Rating';
$_['column_price']      	= 'Price';
$_['column_stock']      	= 'Stock';
$_['column_qty']        	= 'Qty';
$_['column_action']     	= 'Action';

//Errors
$_['error_customer_group'] 	= 'Your current customer account does not have the necessary privileges to view the price list.';
?>