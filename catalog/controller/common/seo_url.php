<?php
class ControllerCommonSeoUrl extends Controller {
	public function index() {

        if($this->config->get('myocwpl_status') && $this->config->get('myocwpl_store') && in_array($this->config->get('config_store_id'), $this->config->get('myocwpl_store'))) {
            if(isset($this->request->get['_route_']) && (trim($this->request->get['_route_']) == 'product/pricelist')) {
                $this->request->get['route'] = 'myoc/pricelist';
                return $this->forward($this->request->get['route']);
            }
            if(isset($this->request->get['route']) && (trim($this->request->get['route']) == 'product/pricelist')) {
                $this->request->get['route'] = 'myoc/pricelist';
                return $this->forward($this->request->get['route']);
            }
            if(isset($this->request->get['_route_']) && (trim($this->request->get['_route_']) == 'product/pricelist/printable')) {
                $this->request->get['route'] = 'myoc/pricelist/printable';
                return $this->forward($this->request->get['route']);
            }
            if(isset($this->request->get['route']) && (trim($this->request->get['route']) == 'product/pricelist/printable')) {
                $this->request->get['route'] = 'myoc/pricelist/printable';
                return $this->forward($this->request->get['route']);
            }
        }

		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}
		
		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$parts = explode('/', $this->request->get['_route_']);

			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");

				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);


					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
						$this->request->request['product_id'] = $url[1];
					}
					
					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
							$this->request->request['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
							$this->request->request['path'] .= '_' . $url[1];
						}
					}	
					
					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
						$this->request->request['manufacturer_id'] = $url[1];
					}
					
					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
						$this->request->request['information_id'] = $url[1];
					}
				} else {
					$this->request->get['route'] = 'error/not_found';	
					$this->request->request['route'] = 'error/not_found';
				}
			}
			
			if (isset($this->request->get['product_id'])) {
				$this->request->get['route'] = 'product/product';
				$this->request->request['route'] = 'product/product';
			} elseif (isset($this->request->get['path'])) {
				$this->request->get['route'] = 'product/category';
				$this->request->request['route'] = 'product/category';
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$this->request->get['route'] = 'product/manufacturer/product';
				$this->request->request['route'] = 'product/manufacturer/product';
			} elseif (isset($this->request->get['information_id'])) {
				$this->request->get['route'] = 'information/information';
				$this->request->request['route'] = 'information/information';
			}

			if (isset($this->request->get['route'])) {
				return $this->forward($this->request->get['route']);
			}
		}
	}

	public function rewrite($link) {
		if ($this->config->get('config_seo_url')) {
			$url_data = parse_url(str_replace('&amp;', '&', $link));
		
			$url = ''; 
			
			$data = array();
			
			parse_str($url_data['query'], $data);
			
			foreach ($data as $key => $value) {
				if (isset($data['route'])) {
					if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/product' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')) {
                        if (isset($this->session->data['keyword_all'][$this->db->escape($key . '=' . (int)$value)])) {
                            $query = $this->session->data['keyword_all'][$this->db->escape($key . '=' . (int)$value)];
                        } else {
                            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
                            $this->session->data['keyword_all'][$this->db->escape($key . '=' . (int)$value)] = $query;
                        }
					
						if ($query->num_rows) {
							$url .= '/' . $query->row['keyword'];
							
							unset($data[$key]);
						}					
					} elseif ($key == 'path') {
						$categories = explode('_', $value);
						
						foreach ($categories as $category) {
                            if (isset($this->session->data['category_keyword']['category_id='.$category])) {
                                $query = $this->session->data['category_keyword']['category_id='.$category];
                            } else {
                                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");
                                $this->session->data['category_keyword']['category_id=' . $category] = $query;
                            }
					
							if ($query->num_rows) {
								$url .= '/' . $query->row['keyword'];
							}							
						}
						
						unset($data[$key]);
					}
				}
			}
		
			if ($url) {
				unset($data['route']);
			
				$query = '';
			
				if ($data) {
					foreach ($data as $key => $value) {
						$query .= '&' . $key . '=' . $value;
					}
					
					if ($query) {
						$query = '?' . trim($query, '&');
					}
				}

				return $url_data['scheme'] . '://' . $url_data['host'] . (isset($url_data['port']) ? ':' . $url_data['port'] : '') . str_replace('/index.php', '', $url_data['path']) . $url . $query;
			} else {
				return $link;
			}
		} else {
			return $link;
		}		
	}	
}
?>