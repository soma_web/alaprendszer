<?php
// Text
$_['text_subject']  = '%s - Thank you for registering';
$_['text_welcome']  = 'Welcome and thank you for registering at %s!';
$_['text_login']    = 'Your account has now been created and you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_approval'] = 'Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_services'] = 'Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.';
$_['text_thanks']   = 'Thanks,';

$_['text_adatai']   = 'Entered data:';
$_['text_data_email']     = 'E-mail address: %s';
$_['text_data_jelszo']    = 'Password: %s';
$_['text_data_telefon']   = 'phone Number: %s';
$_['text_data_ceg']       = 'Company: %s';
$_['text_data_ado']       = 'tax Number: %s';
$_['text_data_iranyito']  = 'zip code: %s';
$_['text_data_varos']     = 'City: %s';
$_['text_data_utca']      = 'Street: %s';
$_['text_data_orszag']    = 'Country: %s';
?>