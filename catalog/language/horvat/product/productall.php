<?php
// Text
$_['text_refine']       = 'Keresés finomítása';
$_['text_product']      = 'Összes termék';
$_['text_error']        = 'Összes termék nem található';
$_['text_empty']        = 'Ebben a összes termékekben nincsenek kilistázandó termékek.';
$_['text_quantity']     = 'Menny.:';
$_['text_manufacturer'] = 'Márka:';
$_['text_model']        = 'Összes termék kódja:';
$_['text_points']       = 'Jutalom pontok:';
$_['text_price']        = 'Ár:';
$_['text_tax']          = 'Ex-adó:';
$_['text_reviews']      = '%s áttekintés alapján.';
$_['text_compare']      = 'Termék összehasonlítás (%s)';
$_['text_display']      = 'Nézet:';
$_['text_list']         = 'Lista';
$_['text_grid']         = 'Rács';

$_['text_sort']         = 'Rendezés:';
$_['text_default']      = 'Alapértelmezett';
$_['text_name_asc']     = 'Név szerint növekvő';
$_['text_name_desc']    = 'Név szerint csökkenő';
$_['text_price_asc']    = 'Olcsóbbtól a drágábbig';
$_['text_price_desc']   = 'Drágábbtól az olcsóbbig';
$_['text_rating_asc']   = 'Értékelés (Legrosszabbak elől)';
$_['text_rating_desc']  = 'Értékelés (Legjobbak elől)';
$_['text_model_asc']    = 'Cikkszám szerint növekvő';
$_['text_model_desc']   = 'Cikkszám szerint csökkenő';
?>