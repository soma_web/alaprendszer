<?php
class ModelShippingflat2 extends Model {
    function getQuote($address, $fizetesek="") {
		$this->load->language('shipping/flat2');


		$sql = "SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE ";

		if ($this->config->get('flat2_geo_zone_id')){
			$sql .= " geo_zone_id = '" . (int)$this->config->get('flat2_geo_zone_id') . "' AND ";
		}

		$sql .= " country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')";
		//$sql = "SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('flat2_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')";

		$query = $this->db->query($sql);

		if (!$this->config->get('flat2_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();


		if ($status) {
            $fizetesi_mod = array();

            if ($fizetesek){
                foreach($fizetesek as $value) {
                    if ( $this->config->get("flat2_payment_".$value['code']) &&  $this->config->get("flat2_payment_".$value['code']) == 1){
                        $fizetesi_mod[] = array(
                            'fizetesi_mod' => $value['code']
                        );
                    }
                }
            }
			$quote_data = array();
            if ($this->config->get('flat2_tax_class_id') > 0) {
                $query_tax = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$this->config->get('flat2_tax_class_id') . "'");
            }
            $ado_title = isset($query_tax->row['title']) ? $query_tax->row['title'] :"";
            /*if ($this->currency->format($this->tax->calculate($this->config->get('flat2_cost'), $this->config->get('flat2_tax_class_id'), $this->config->get('config_tax'))) ==
                $this->currency->format($this->config->get('flat2_cost')) )*/

      		$quote_data['flat2'] = array(
        		'code'         => 'flat2.flat2',
                'title'        =>  $this->config->get('flat2_text') != null ? $this->config->get('flat2_text') :  $this->language->get('text_description'),
        		'cost'         => $this->config->get('flat2_cost'),
                'fizetesi_mod'  => $fizetesi_mod,
        		'tax_class_id' => $this->config->get('flat2_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('flat2_cost'), $this->config->get('flat2_tax_class_id'), $this->config->get('config_tax'))),
                'netto_text'   => $this->currency->format($this->config->get('flat2_cost')),
                'ado_title'    => $ado_title,
				'mail'         => $this->config->get('flat2_mail')

            );

      		$method_data = array(
        		'code'       => 'flat2',
        		'title'      =>  $this->config->get('flat2_header') != null ? $this->config->get('flat2_header') :  $this->language->get('text_description'),
                'fizetesi_mod'  => $fizetesi_mod,
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get('flat2_sort_order'),
        		'error'      => false
      		);
		}
	
		return $method_data;
	}
}
?>