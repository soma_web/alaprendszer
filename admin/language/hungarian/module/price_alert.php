<?php
// Heading
$_['heading_title']                   = 'Árváltozás értesítő';

// Tab
$_['tab_setting']                     = 'Általános beállítások';
$_['tab_popup']                       = 'Felugró ablak beállításai';
$_['tab_customer_email']              = 'E-mail sablon (vásárlónak)';
$_['tab_admin_email']                 = 'E-mail sablon (webáruház tulajdonosnak)';
$_['tab_alert']           			  = 'Aktív értesítők';
$_['tab_history']         			  = 'Kiküldött értesítők';
$_['tab_help']           			  = 'Segítség';

// Column
$_['column_customer']     			  = 'Várásló';
$_['column_email']     			      = 'E-mail cím';
$_['column_product']     			  = 'Termék';
$_['column_price_seen']     		  = 'Jelenlegi ár';
$_['column_price_desired']     		  = 'Kívánt ár';
$_['column_date_added']     		  = 'Hozzáadás dátuma';
$_['column_sent']     		          = 'Kiküldés ideje';
$_['column_action']                   = 'Művelet';

// Button
$_['button_view_email']               = 'E-mail megtekintése';
$_['button_check_send']               = 'Ellenőrzés / Küldés';

// Text
$_['text_module']                     = 'Modulok';
$_['text_success']                    = 'Siker: módosította az Árváltozás értesítő modult!';
$_['text_status_attention']           = 'Fontos: HA az Állapot mező "Letiltott" állapotban van, az Árváltozás értesítő nem jelenik meg a termék aloldalon!';
$_['text_check_send']                 = 'Cron alkalmazás automatikusan ellenőrzi/kiküldi az értesítőket a parancsban meghatározott időpontban.<br />Manuális ellenőrzéshez és e-mail küldéshez nyomja meg az \'Ellenőrzés / Küldés\' gombot.';

// Entry
$_['entry_status']                    = 'Állapot:<span class="help">Árváltozás értesítő engedélyezése vagy tiltása</span>';
$_['entry_secret_code']               = 'Cron alkalmazás titkos kódja: <span class="help">Legalább 5 karakter! ( a-z, A-Z, 0-9 )</span>';
$_['entry_admin_notification']        = 'Webáruház értesítő (e-mail küldés):<span class="help">E-mail küldése a webáruház tulajdonosnak, amikor egy vásárló lead egy új árváltozás értesítő igényt.</span>';
$_['entry_use_html_email']            = 'Send Reminder with <a href="http://www.oc-extensions.com/HTML-Email">HTML Email Extension</a>:<span class="help"><br />If HTML Email Extension is not installed on your store then is used default html mail (like in old versions of this extensions)</span>';

$_['entry_popup_set_alert']           = 'Árváltozás értesítő szövege:<span class="help">Üzenet, amit a vásárló lát a termék aloldalon</span>';
$_['entry_popup_short_explanation']   = 'Rövid magyarázat:<span class="help">Az árváltozás értesítőről.</span>';
$_['entry_popup_name']                = 'Név címke:<span class="help">Pl.: Név:</span>';
$_['entry_popup_email']               = 'E-mail cím címke:<span class="help">Pl.: E-mail cím:</span>';
$_['entry_popup_desired_price']       = 'Kívánt ár címke:<span class="help">Pl.: Kívánt ár:</span>';
$_['entry_popup_button_set_alert']    = 'Elküldés gomb címke:<span class="help">Pl.: Küldés</span>';
$_['entry_popup_error_name']          = 'Név hiba:<span class="help">Pl.: A név mező kitöltése közelető!</help>';
$_['entry_popup_error_email']         = 'E-mail cím hiba:<span class="help">Pl.: Az e-mail cím érvénytelennek tűnik!</help>';
$_['entry_popup_error_desired_price'] = 'Kívánt ár hiba:<span class="help">Pl.: Kívánt ár nem tűnik érvényesnek!</help>';
$_['entry_popup_error_price_already'] = 'Kívánt ár már elérhető hiba:<span class="help">Pl.: A termék ára már a kívánt ár alatt van!</help>';
$_['entry_popup_success']             = 'Sikeres elküldés szövege:<span class="help">Pl.: Adatait rögzítettük! Értesítjük, amint a termék ára elérte a kívánt összeget!</help>';

$_['entry_subject']                   = 'Tárgy:';
$_['entry_message']                   = 'Üzenet:<span class="help"><br /><u>Speciális kulcsszavak:</u><br />{name} = vásárló neve<br />{products_list} = termék részletei<br />{store_name} = áruház neve</span>';
$_['entry_admin_message']             = 'Üzenet:<span class="help"><br /><u>Special Keywords:</u><br />{customer_name} = vásárló neve<br />{customer_email} = vásárló e-mail címe<br />{product_name} = termék neve<br />{product_name_with_link} = termék név hivatkozás formában<br />{product_price} = a termék vásárló által látható ára<br />{desired_price} = kívánt ár</span>';

// Entry - Alert list
$_['entry_customer']                  = 'Vásárló:';
$_['entry_email']                     = 'E-mail cím:';
$_['entry_product']                   = 'Termék:';
$_['entry_date_added']                = 'Hozzáadás dátuma:';

// Error
$_['error_permission']                = 'Figyelem: Önnek nincs jogosultsága az Árváltozás értesítő modul módosításához!';
$_['error_in_tab']                    = 'Kérem, ellenőrizze újra - hiba az adatokban %s';
$_['error_required']                  = 'A mező kitöltése kötelező!';
$_['error_secret_code']               = 'Titkos kód: legalább 5 karakter! ( a-z, A-Z, 0-9 )';
$_['error_html_email_not_installed']  = 'Az üzenetet nem lehet kiküldeni a<a href="http://www.oc-extensions.com/HTML-Email">HTML Email Modullal</a> mivel ez a modul nincs telepítve az Ön áruházában. Kérem, állítsa át Letiltottra!';
$_['error_subject']                   = 'E-mail tárgya - kötelező';
$_['error_message']                   = 'E-mail szövege - kötelező';
?>