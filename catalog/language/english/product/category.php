<?php
// Text
$_['text_refine']       = 'Refine Search';
$_['text_product']      = 'Products';
$_['text_error']        = 'Category not found!';
$_['text_empty']        = 'There are no products to list in this category.';
$_['text_quantity']     = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Product Code:'; 
$_['text_points']       = 'Reward Points:'; 
$_['text_price']        = 'Price:'; 
$_['text_tax']          = 'Ex Tax:';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_compare']      = 'Product Compare (%s)'; 
$_['text_display']      = 'Display:';
$_['text_list']         = 'List';
$_['text_grid']         = 'Grid';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Price (Low &gt; High)';
$_['text_price_desc']   = 'Price (High &gt; Low)';
$_['text_stock_in']     = 'Estimated arrival:';

$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Show:';
$_['text_megnevezes']       = 'Name';
$_['text_azonosito']        = 'Model';
$_['text_brutto_ar']        = 'Price';
$_['text_netto_ar']         = 'Netto Price';

$_['text_akcio_title']        = 'Special product';
$_['text_pardarab_title']     = 'Few pieces';
$_['text_uj_title']           = 'New product';
$_['text_ujdonsag_title']     = 'Newness';

$_['text_raktar']       = 'Per-store inventory:';
$_['text_garancia']= 'warranty';
$_['entry_garancia_honap']      = 'months';
$_['entry_garancia_ev']         = 'year';
$_['text_szallitas']= 'Shipping cost:';

// Ikonok szöveges (iconok_szoveges.tpl)
$_['text_uj']                = 'New';
$_['text_akcios']            = 'On Sale';
$_['text_pardarab']          = 'Few Pieces';
$_['text_mennyisegi']        = 'Volume Discount';
// Ikonok szöveges vége
?>