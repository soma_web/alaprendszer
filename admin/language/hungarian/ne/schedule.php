<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Ütemezett Hírlevél';

// Entry
$_['entry_yes']								= 'Igen';
$_['entry_no']								= 'Nem';
$_['entry_name']							= 'Ütemezés neve:';
$_['entry_recurrent']						= 'Ismétlődő:';
$_['entry_random']							= 'Véletlenszerű termékek:';
$_['entry_datetime']						= 'Dátum és idő  strat feladathoz:';
$_['entry_frequency']						= 'Gyakoriság:';
$_['entry_daytime']							= 'Feladat indításának napja és időpontja:';
$_['entry_active']							= 'Aktív:';
$_['entry_random_count']					= 'Véletlen termékek mennyisége a hirdetéshez:<br /><span class="help">Alapértelmezett érték 8</span>';
$_['entry_marketing']						= 'Marketing listák:';

// Text
$_['text_edit']								= 'Szerkesztés';
$_['text_marketing']						= 'Összes Marketing Feliratkozó';
$_['text_marketing_all']					= 'Összes Marketing';
$_['text_subscriber_all']					= 'Összes Hírlevél Feliratkozó & Marketing Feliratkozók';
$_['text_all']								= 'Összes Vevő & Marketing';
$_['text_success']							= 'Siker: A kiválasztott elemek törölve!';
$_['text_at']								= 'akkor';
$_['text_daily']							= 'Napi';
$_['text_weekly']							= 'Heti';
$_['text_monthly']							= 'Havi';
$_['text_monday']							= 'Hétfő';
$_['text_tuesday']							= 'Kedd';
$_['text_wednesday']						= 'Szerda';
$_['text_thursday']							= 'Csütörtök';
$_['text_friday']							= 'Péntek';
$_['text_saturday']							= 'Szombat';
$_['text_sunday']							= 'Vasárnap';
$_['text_success']							= 'Siker: A kiválasztott elemek törölve!';
$_['text_success_save']						= 'Ütemezés sikeresen mentve!';
$_['text_loading']							= 'Töltés...';
$_['text_default']							= 'Alapértelmezett';
$_['text_current_server_time']				= '<i>Jelenlegi szerver idő %s</i>';

// Column 
$_['column_actions']						= 'Akciók';
$_['column_name']							= 'Név';
$_['column_to']								= 'Irányába';
$_['column_recurrent']						= 'Visszatérő';
$_['column_active']							= 'Aktív';
$_['column_store']							= 'Áruház';

// Button
$_['button_save']							= 'Mentés';
$_['button_preview']						= 'Előnézet';

// Error
$_['error_name']							= 'Ütemezés neve kötelező!'

?>
