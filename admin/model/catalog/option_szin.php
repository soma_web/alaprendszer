<?php 
class ModelCatalogOptionSzin extends Model {
	public function addOptionSzin($data) {


        $sql = "INSERT INTO " . DB_PREFIX . "option_szin SET
		            sort_order  = '" . (int)$data['sort_order'] . "',
		            azonosito   = '" . (int)$data['azonosito'] . "',
		            szinkod     = '" . $data['szinkod'] . "'";

            $this->db->query($sql);
            $option_szin_id = $this->db->getLastId();

        foreach ($data['option_szin_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "option_szin_description SET
                        `option_szin_id`    = '" . (int)$option_szin_id . "',
                        `language_id`       = '" . $language_id . "',
                        `name`              = '" . $value['name'] . "'");
        }

        foreach ($data['szin_group'] as $option_szin_group_id) {
            $sql = "INSERT INTO " .DB_PREFIX. "option_szin_to_group SET
                    option_szin_group_id='".$option_szin_group_id."',
                    option_szin_id='".$option_szin_id."'";
            $query = $this->db->query($sql);
        }

		$sql = "UPDATE  " . DB_PREFIX . "option_value SET
			heading	= '" . $data['szinkod'] . "'
			 WHERE option_szin_id = '" . (int)$option_szin_id . "'";
		$this->db->query($sql);
    }


	public function editOptionSzin($option_szin_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "option_szin SET
		    sort_order  = '" . (int)$data['sort_order'] . "',
		    azonosito   = '" . (int)$data['sort_order'] . "',
		    szinkod     = '" . $data['szinkod'] . "'
		    WHERE option_szin_id = '" . (int)$option_szin_id . "'");


        $this->db->query("DELETE FROM " . DB_PREFIX . "option_szin_description WHERE option_szin_id = '" . (int)$option_szin_id . "'");

        foreach ($data['option_szin_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "option_szin_description SET
                        `option_szin_id`    = '" . (int)$option_szin_id . "',
                        `language_id`       = '" . $language_id . "',
                        `name`              = '" . $value['name'] . "'");
        }

        $sql = "DELETE FROM " .DB_PREFIX. "option_szin_to_group WHERE option_szin_id='".$option_szin_id."'";
        $query = $this->db->query($sql);

        foreach ($data['szin_group'] as $option_szin_group_id) {
            $sql = "INSERT INTO " .DB_PREFIX. "option_szin_to_group SET
                    option_szin_group_id='".$option_szin_group_id."',
                    option_szin_id='".$option_szin_id."'";
            $query = $this->db->query($sql);

        }

		$sql = "UPDATE  " . DB_PREFIX . "option_value SET
			heading	= '" . $data['szinkod'] . "'
			 WHERE option_szin_id = '" . (int)$option_szin_id . "'";
		$this->db->query($sql);
	}
	
	public function deleteOptionSzin($option_szin_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "option_szin WHERE option_szin_id = '" . (int)$option_szin_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "option_szin_description WHERE option_szin_id = '" . (int)$option_szin_id . "'");
	}
		
	public function getOptionSzin($option_szin_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_szin WHERE option_szin_id = '" . (int)$option_szin_id . "'");
		
		return $query->row;
	}
		
	public function getSzinGroups($option_szin_id) {
		$option_szin_group_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_szin_to_group WHERE option_szin_id = '" . (int)$option_szin_id . "'");

		foreach ($query->rows as $result) {
			$option_szin_group_data[] = $result['option_szin_group_id'];
		}

		return $option_szin_group_data;
	}


	public function getOptionSzinek($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "option_szin osz
		        LEFT JOIN " .DB_PREFIX. "option_szin_description oszd ON (osz.option_szin_id=oszd.option_szin_id) WHERE 1 ";
		$sql .= " AND language_id = '" . (int)$this->config->get('config_language_id') . "'";



        if (!empty($data['filter_szin'])) {
            $sql .= " AND LCASE(oszd.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_szin'])) . "%'";
        }

        if (!empty($data['filter_szinkod'])) {
            $sql .= " AND LCASE(osz.szinkod) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_szinkod'])) . "%'";
        }

		$sort_data = array(
			'name',
			'sort_order',
			'szinkod'
		);
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY LCASE(name)";
		}	
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $sorok = array();
            foreach($query->rows as $value) {
                if (isset($sorok[$value['option_szin_id']]) ) {
                    $sorok[$value['option_szin_id']]['name'][$value['language_id']] =  $value['name'];

                } else {
                    $sorok[$value['option_szin_id']] = array(
                        'option_szin_id'    => $value['option_szin_id'],
                        'szinkod'           => $value['szinkod'],
                        'azonosito'         => $value['azonosito'],
                        'name'              => array(
                                                    $value['language_id'] => $value['name']
                                                )
                    );
                }
            }
        }

		
		return $query->rows;
	}



    public function getOptionSzinekLanguage($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "option_szin osz
		        LEFT JOIN " .DB_PREFIX. "option_szin_description oszd ON (osz.option_szin_id=oszd.option_szin_id) WHERE 1 ";

        if (!empty($data['filter_szin'])) {
            $sql .= " AND LCASE(oszd.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_szin'])) . "%'";
        }

        if (!empty($data['filter_szinkod'])) {
            $sql .= " AND LCASE(osz.szinkod) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_szinkod'])) . "%'";
        }

        $sort_data = array(
            'name',
            'sort_order',
            'szinkod'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY LCASE(name)";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }
	
	public function getOptionSzinDescriptions($option_szin_id) {
		$option_szin_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_szin_description WHERE option_szin_id = '" . (int)$option_szin_id . "'");
		
		foreach ($query->rows as $result) {
            $option_szin_data[$result['language_id']] = array('name' => $result['name']);
		}
		
		return $option_szin_data;
	}
	
	public function getTotalOptionSzinek($data) {
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "option_szin osz
		        LEFT JOIN " .DB_PREFIX. "option_szin_description oszd ON (osz.option_szin_id=oszd.option_szin_id)
		        WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_szin'])) {
            $sql .= " AND LCASE(oszd.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_szin'])) . "%'";
        }

        if (!empty($data['filter_szinkod'])) {
            $sql .= " AND LCASE(osz.szinkod) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_szinkod'])) . "%'";
        }

      	$query = $this->db->query($sql);

		return $query->row['total'];
	}	
}
?>