<?php

// Heading
$_['heading_title']      = 'Banki átutalás';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Sikeresen módosította a banki átutalás részleteit!';

// Entry
$_['entry_bank']         = 'Banki átutalás utasításai:';
$_['entry_total']        = 'Min. összeg:<br /><span class="help">A pénztárban a rendelés értékének el kell érnie ezt az összeget, hogy aktívvá váljon!</span>';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';
$_['entry_vevocsoport']  = 'Szűkítés vevő csoportra:';
$_['entry_crm_fizetesi_mod']  = 'CRM : fizetési mód:';
$_['entry_crm_fizetesi_hatarido']  = 'CRM : fizetési határidő (nap):';

// Error
$_['error_permission']   = 'Figyelmeztetés: A bankátutalással történő fizetés adatainak módosítása az Ön számára nem engedélyezett!';
$_['error_bank']         = 'Szükség van a bankátutalás utasításaira!';
?>
