<?php 
class ControllerToolCsvExport extends Controller {

	public function csvSql($export_csoport="Transoflex",$feltetel="") {

        $beallitasok = $this->config->get("csv_beallitasok");
        foreach($beallitasok as $value) {
            if ($value['export_name']  == $export_csoport) {
                $csv_sor = $value;
                break;
            }
        }

        $tablak = array();
        $i = 1;
        foreach($csv_sor['tabla'] as $csv_tabla) {
            if (isset($csv_tabla['fotabla']) ){
                $fotabla['name'] = $csv_tabla['name'];
            } else {
                $tablak[$i]['name'] = $csv_tabla['name'];
                $tablak[$i]['on_kapcsolat'] = $csv_tabla['on_kapcsolat'];
                $tablak[$i]['where_feltetel'] = $csv_tabla['where_feltetel'];
                $tablak[$i]['kapcsolodas'] = isset($csv_tabla['kapcsolodas']) ? $csv_tabla['kapcsolodas'] : "";
            }
            $i++;
        }
        if (isset($fotabla) ){
            $tablak[0] = $fotabla;
            ksort($tablak);
        }

        $sql = "SELECT ";

        for($i=0; count($csv_sor['mezok']) > $i; $i++ ) {
            if (isset($csv_sor['mezok'][$i]['name'])) {
                $maganyosan = true;
                if (isset($csv_sor['mezok'][$i+1]['name']) ) {
                    if ($csv_sor['mezok'][$i+1]['sort_order'] == $csv_sor['mezok'][$i]['sort_order']) {
                        $maganyosan = false;
                        $sql .= "CONCAT(";
                        if ($csv_sor['mezok'][$i]['tizedes'] > 0) {
                            $sql .= "round(";
                        }
                        $sql .= $csv_sor['mezok'][$i]['name'];
                        if ($csv_sor['mezok'][$i]['tizedes'] > 0) {
                            $sql .= "," . ((int)$csv_sor['mezok'][$i]['tizedes']-1) . ") ";
                        }
                        $sql .= '," ",';
                        if ($csv_sor['mezok'][$i]['tizedes'] > 0) {
                            $sql .= "round(";
                        }
                        $sql .= $csv_sor['mezok'][$i+1]['name'];
                        if ($csv_sor['mezok'][$i]['tizedes'] > 0) {
                            $sql .= "," . ((int)$csv_sor['mezok'][$i]['tizedes']-1) . ") ";
                        }
                        $sql .= "), ";
                        $i++;
                    }
                }
                if ($maganyosan) {
                    if ($csv_sor['mezok'][$i]['tizedes'] > 0) {
                        $sql .= "round(";
                    }
                    $sql .= $csv_sor['mezok'][$i]['name'];
                    if ($csv_sor['mezok'][$i]['tizedes'] > 0) {
                        $sql .= "," . ((int)$csv_sor['mezok'][$i]['tizedes']-1) . ") ";
                    }
                    $sql .= ", ";
                }

            } elseif (isset($csv_sor['mezok'][$i]['allando']) ) {
                $sql .= "'".htmlspecialchars_decode($csv_sor['mezok'][$i]['allando'])."', ";
            }
        }


        $sql = substr($sql,0,-2);
        $sql .= " FROM ";
        $elso = true;
        foreach($tablak as $value){
            if ($elso) {
                $sql .= $value['name']." ";
                $elso = false;
            } else {
                if (!empty($value['on_kapcsolat']) ) {
                    $sql .= $value['kapcsolodas'] . " JOIN ";
                    $sql .= $value['name'];
                    $sql .= " ON (".$value['on_kapcsolat']." ";

                    if (!empty($value['where_feltetel']) ) {
                        $sql .= " AND " .$value['where_feltetel'].  " ";
                    }
                    $sql .= " ) ";
                }
            }
        }
        $sql .= $feltetel;

        if (!empty($csv_sor['order_by'])) {
            $sql .= " ORDER BY ".$csv_sor['order_by'];
        }
        $query = $this->db->query($sql);

        if (isset($csv_sor['kuldes']) && $csv_sor['kuldes'] == 2 ) {

            $url = $csv_sor['from_action'];

            $fields_string = '';
            $i= 0;
            foreach($query->row as $key=>$value) {
                $fields_string .= $csv_sor['mezok'][$i]['form_mezo_neve'].'='.$value.'&';
                $i++;
            }

            rtrim($fields_string, '&');

            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($query->row));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

            $result = curl_exec($ch);

            curl_close($ch);
        } else {


            $output = fopen(DIR_ARUHAZ."csv/".$export_csoport.".csv", "w");
            if (!$output) {
                $output = fopen("./../csv/".$export_csoport.".csv", "w");
            }

            if (!$output) {
                $output = fopen("../csv/".$export_csoport.".csv", "w");
            }

            if (!$output) {
                $output = fopen("./csv/".$export_csoport.".csv", "w");
            }

            if (!$output) {
                $output = fopen("/csv/".$export_csoport.".csv", "w");
            }


            $fejlec = array();

            for($i=0; count($csv_sor['mezok']) > $i; $i++) {
                $nev  = isset($csv_sor['mezok'][$i]['name']) ? explode(".",$csv_sor['mezok'][$i]['name']) : htmlspecialchars_decode($csv_sor['mezok'][$i]['allando']);
                $nev  =is_array($nev) ? $nev[1] : $nev;
                $nev1 = false;
                if (isset($csv_sor['mezok'][$i+1]['sort_order']) && $csv_sor['mezok'][$i]['sort_order'] == $csv_sor['mezok'][$i+1]['sort_order']) {
                    $nev1 = isset($csv_sor['mezok'][$i+1]['name']) ? explode(".",$csv_sor['mezok'][$i+1]['name']) : htmlspecialchars_decode($csv_sor['mezok'][$i+1]['allando']);
                    $nev1  =is_array($nev1) ? $nev1[1] : $nev1;
                    $i++;
                }
                if (isset($csv_sor['karakter_kodolas']) && $csv_sor['karakter_kodolas']) {
                    $kodolas = explode(",",$csv_sor['karakter_kodolas']);
                    $fejlec[] = iconv(count($kodolas) > 1 ? $kodolas[0] : "UTF-8", count($kodolas) > 1 ? $kodolas[1] : $kodolas[0], ($nev1 ? ($nev . " " . $nev1) : $nev));

                } else {
                    $fejlec[] = $nev1 ? ($nev . " " . $nev1) : $nev;
                }
            }


            $fejlec =  implode("@&@@&@", $fejlec);
            $fejlec = str_replace(";"," ",$fejlec);
            $fejlec = str_replace("@&@@&@",";",$fejlec);
            fputs($output, $fejlec."\n", strlen($fejlec)+1);

            foreach($query->rows as $product) {
                $newLine = implode("@&@@&@", $product);
                $newLine = str_replace(";"," ",$newLine);
                $newLine = str_replace("@&@@&@",";",$newLine);

                if (isset($csv_sor['karakter_kodolas']) && $csv_sor['karakter_kodolas']) {
                    $kodolas = explode(",",$csv_sor['karakter_kodolas']);
                    $newLine = iconv(count($kodolas) > 1 ? $kodolas[0] : "UTF-8", count($kodolas) > 1 ? $kodolas[1] : $kodolas[0], $newLine);
                }
                fputs($output, $newLine."\n", strlen($newLine)+1);
            }
            fclose($output);

            $file = DIR_ARUHAZ."csv/".$export_csoport.".csv";
            if (!file_exists($file)) {
                $file = "./../csv/".$export_csoport.".csv";
            }
            if (!file_exists($file)) {
                $file = "../csv/".$export_csoport.".csv";
            }
            if (!file_exists($file)) {
                $file = "/csv/".$export_csoport.".csv";
            }

            if (!file_exists($file)) {
                $file = "./csv/".$export_csoport.".csv";
            }


            if ($csv_sor['tarolas'] == 1) {
                if (file_exists($file)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename='.basename($file));
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));
                    readfile($file);
                    exit;
                }
            } elseif(!empty($csv_sor['tarolas_helye'])) {

                $orders = DIR_ARUHAZ.$csv_sor['tarolas_helye']."/vasarlas_".date("Ymd_His").".csv";
                $result=rename($file, $orders);
                if(!$result) {
                    $orders = "./../".$csv_sor['tarolas_helye']."/vasarlas_".date("Ymd_His").".csv";
                    $result=rename($file, $orders);
                }

                if(!$result) {
                    $orders = "../".$csv_sor['tarolas_helye']."/vasarlas_".date("Ymd_His").".csv";
                    $result=rename($file, $orders);
                }

                if(!$result) {
                    $orders = "/".$csv_sor['tarolas_helye']."/vasarlas_".date("Ymd_His").".csv";
                    $result=rename($file, $orders);
                }

                if(!$result) {
                    $orders = "./".$csv_sor['tarolas_helye']."/vasarlas_".date("Ymd_His").".csv";
                    $result=rename($file, $orders);
                }

            }
        }


    }

    public function csvFeltetel($feltetel="",$export_csoport="") {
        $feltetel = htmlspecialchars_decode($this->request->request['feltetel']);
        $feltetel = htmlspecialchars_decode($feltetel);
        $export_csoport = $this->request->request['export_csoport'];
        $this->csvSql($export_csoport,$feltetel);
        $this->redirect($_SERVER['HTTP_REFERER']);

    }

}
?>





