<?php 
class ControllerCheckoutRegisterMegrendelem extends Controller {
  	public function index() {
		$this->language->load('checkout/checkout');
		
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_your_details'] = $this->language->get('text_your_details');
		$this->data['text_your_address'] = $this->language->get('text_your_address');
		$this->data['text_your_password'] = $this->language->get('text_your_password');
				
		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_email_again'] = $this->language->get('entry_email_again');
		$this->data['entry_telephone'] = $this->language->get('entry_telephone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_adoszam'] = $this->language->get('entry_adoszam');
		$this->data['entry_address_1'] = $this->language->get('entry_address_1');
		$this->data['entry_address_2'] = $this->language->get('entry_address_2');
		$this->data['entry_postcode'] = $this->language->get('entry_postcode');
		$this->data['entry_city'] = $this->language->get('entry_city');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zone'] = $this->language->get('entry_zone');
		$this->data['entry_newsletter'] = sprintf($this->language->get('entry_newsletter'), $this->config->get('config_name'));
		$this->data['entry_password'] = $this->language->get('entry_password');
		$this->data['entry_confirm'] = $this->language->get('entry_confirm');
		$this->data['entry_shipping'] = $this->language->get('entry_shipping');
		$this->data['entry_adoszam'] = $this->language->get('entry_adoszam');

        $this->data['entry_nem'] = $this->language->get('entry_nem');
        $this->data['entry_eletkor'] = $this->language->get('entry_eletkor');
        $this->data['entry_iskolai_vegzettseg'] = $this->language->get('entry_iskolai_vegzettseg');

		$this->data['button_continue'] = $this->language->get('button_continue');

		$this->data['country_id'] = $this->config->get('config_country_id');

		$this->load->model('localisation/country');
		
		$this->data['countries'] = $this->model_localisation_country->getCountries();
        $this->data['eletkors'] = $this->model_localisation_country->getEletkor();

        $this->load->model('account/iskolaivegzettseg');

        $this->data['iskolai_vegzettsegek'] = $this->model_account_iskolaivegzettseg->getAll();

        $this->data['nems'][] = array(
            'ertek' => 1,
            'nev'   => $this->language->get('select_ferfi')
        );

        $this->data['nems'][] = array(
            'ertek' => 2,
            'nev'   => $this->language->get('select_no')
        );


		/*if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');
			
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
            $aszf = $this->model_catalog_information->getInformation(6);
			
			if ($information_info) {
                $this->data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/info', 'information_id=6', 'SSL'), $aszf['title'], $aszf['title'], $this->url->link('information/information/info', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$this->data['text_agree'] = '';
			}
		} else {
			$this->data['text_agree'] = '';
		}*/



        if ($this->config->get('config_account_id')) {

            $elolvastam_informaciok = $this->config->get('megjelenit_elolvastam_informaciok');
            $elolvastam = $this->config->get('megjelenit_elolvastam');
            $elolvastam = html_entity_decode($elolvastam[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
            $this->load->model('catalog/information');

            if ($elolvastam_informaciok) {
                $information_tomb = array();

                $elolvastam_informaciok =$this->config->rendezes($elolvastam_informaciok,"sort_order");
                foreach($elolvastam_informaciok as $elolvastam_informacio) {
                    $information_info = $this->model_catalog_information->getInformation($elolvastam_informacio['information_id']);
                    $information_tomb[] = $this->url->link('information/information/info', 'information_id='.$elolvastam_informacio['information_id'], 'SSL');
                    $information_tomb[] = $information_info['title'];
                    $information_tomb[] = $information_info['title'];

                }

                $this->data['text_agree'] = vsprintf($elolvastam, $information_tomb);
            }

        } else {
            $this->data['text_agree'] = '';
        }
		
		$this->data['shipping_required'] = $this->cart->hasShipping("megrendelem");

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/register.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/checkout/register.tpl';
            } else {
                $this->template = 'default/template/checkout/register.tpl';
            }

        $this->data['ceges'] = 0;

        $this->load->model('catalog/category');
        $this->data['categories'] = $this->model_catalog_category->getCategories(0);

		$this->response->setOutput($this->render());		
  	}
	
	public function validate() {
		$this->language->load('checkout/checkout');
		
		$this->load->model('account/customer');
		
		$json = array();
		
		// Validate if customer is already logged out.
		if ($this->customer->isLogged()) {
			$json['redirect'] = $this->url->link('checkout/checkout_megrendelem', '', 'SSL');
		}
		
		// Validate cart has products and has stock.
		if (!$this->cart->hasProducts("megrendelem") ) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}
		
		// Validate minimum quantity requirments.			
		$products = $this->cart->getProducts("megrendelem");
				
		foreach ($products as $product) {
			$product_total = 0;
				
			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}		
			
			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}				
		}
						
		if (!$json) {
            $ceges = ($this->request->post['ceges'] == 1);
            if ( ($this->config->get('megjelenit_regisztracio_vezeteknev') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_vezeteknev') == 1 && $ceges) ) {
                if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
                    $json['error']['firstname'] = $this->language->get('error_firstname');
                }
            }

            if (($this->config->get('megjelenit_regisztracio_keresztnev') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_keresztnev') == 1 && $ceges)) {
                if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
                    $json['error']['lastname'] = $this->language->get('error_lastname');
                }
            }

            if (($this->config->get('megjelenit_regisztracio_email') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_email') == 1 && $ceges)) {
                if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
                    $json['error']['email'] = $this->language->get('error_email');
                }
                if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
                    $json['error']['warning'] = $this->language->get('error_exists');
                }
                if (($this->config->get('megjelenit_regisztracio_email_megerosites') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_email_megerosites') == 1 && $ceges)) {
                    if ($this->request->post['email_again'] != $this->request->post['email']) {
                        $json['error']['email'] = $this->language->get('error_email');
                        $json['error']['email_again'] = $this->language->get('error_email');
                    }
                }
            }

            if (($this->config->get('megjelenit_regisztracio_telefon') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_telefon') == 1 && $ceges)) {
                if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
                    $json['error']['telephone'] = $this->language->get('error_telephone');
                }
            }

            if ( ($this->config->get('megjelenit_regisztracio_utca') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_utca') == 1 && $ceges) ) {
                if ((utf8_strlen($this->request->post['address_1']) < 3) || (utf8_strlen($this->request->post['address_1']) > 128)) {
                    $json['error']['address_1'] = $this->language->get('error_address_1');
                }
            }

            if ( ($this->config->get('megjelenit_regisztracio_varos') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_varos') == 1 && $ceges)) {
                if ((utf8_strlen($this->request->post['city']) < 2) || (utf8_strlen($this->request->post['city']) > 128)) {
                    $json['error']['city'] = $this->language->get('error_city');
                }
            }

            if ( ($this->config->get('megjelenit_regisztracio_orszag') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_orszag') == 1 && $ceges) ) {
                $this->load->model('localisation/country');

                $country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

                if ( ($this->config->get('megjelenit_regisztracio_iranyitoszam') == 1) && !$ceges || ($this->config->get('megjelenit_regisztracio_ceges_iranyitoszam') == 1 && $ceges) ) {
                    if ($country_info && $country_info['postcode_required'] && (utf8_strlen($this->request->post['postcode']) < 2) || (utf8_strlen($this->request->post['postcode']) > 10)) {
                        $json['error']['postcode'] = $this->language->get('error_postcode');
                    }
                }
            } else {
                if ( ($this->config->get('megjelenit_regisztracio_iranyitoszam') == 1) && !$ceges || ($this->config->get('megjelenit_regisztracio_ceges_iranyitoszam') == 1 && $ceges) ) {
                    if ((utf8_strlen($this->request->post['postcode']) < 2) || (utf8_strlen($this->request->post['postcode']) > 10)) {
                        $json['error']['postcode'] = $this->language->get('error_postcode');
                    }
                }
            }

            if ( ($this->config->get('megjelenit_regisztracio_orszag') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_orszag') == 1 && $ceges) ) {
                if ($this->request->post['country_id'] == '') {
                    $json['error']['country'] = $this->language->get('error_country');
                }
            }

            if ( ($this->config->get('megjelenit_regisztracio_megye') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_megye') == 1 && $ceges) ) {
                if ($this->request->post['zone_id'] == '') {
                    $json['error']['zone'] = $this->language->get('error_zone');
                }
            }

            if ( ($this->config->get('megjelenit_eletkor_regisztracio') == 1 && !$ceges) || ($this->config->get('megjelenit_eletkor_regisztracio_ceges') == 1 && $ceges) ) {
                if ($this->request->post['eletkor'] == '') {
                    $json['error']['eletkor'] = $this->language->get('error_eletkor');
                }
            }

            if ( ($this->config->get('megjelenit_nem_regisztracio') == 1 && !$ceges) || ($this->config->get('megjelenit_nem_regisztracio_ceges') == 1 && $ceges) ) {
                if ($this->request->post['nem'] == '') {
                    $json['error']['nem'] = $this->language->get('error_nem');
                }
            }
	
			if ((utf8_strlen($this->request->post['password']) < 3) || (utf8_strlen($this->request->post['password']) > 20)) {
				$json['error']['password'] = $this->language->get('error_password');
			}
	
			if ($this->request->post['confirm'] != $this->request->post['password']) {
				$json['error']['confirm'] = $this->language->get('error_confirm');
            }



            if ($this->config->get('config_account_id')) {

                if (!isset($this->request->post['agree']) && empty($json['error']['warning']) ) {
                    $this->load->model('catalog/information');
                    $elolvastam_informaciok = $this->config->get('megjelenit_elolvastam_informaciok');
                    foreach($elolvastam_informaciok as $elolvastam_informacio) {
                        break;
                    }
                    $information_info = $this->model_catalog_information->getInformation($elolvastam_informacio['information_id']);

                    $json['error']['warning']['agree'] = sprintf($this->language->get('error_agree'), $information_info['title']);
                }
            }
		}
		
		if (!$json) {
			$this->model_account_customer->addCustomer($this->request->post);
			
			$this->session->data['account'] = 'register';
			
			if (!$this->config->get('config_customer_approval')) {
				$this->customer->login($this->request->post['email'], $this->request->post['password']);
				
				$this->session->data['payment_address_id'] = $this->customer->getAddressId();
				
				if (!empty($this->request->post['shipping_address'])) {
					$this->session->data['shipping_address_id'] = $this->customer->getAddressId();
				}
			} else {
				$json['redirect'] = $this->url->link('account/success_megrendelem');
			}
			
			unset($this->session->data['guest']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);	
			unset($this->session->data['payment_methods']);
		}	
		
		$this->response->setOutput(json_encode($json));	
	}
	
  	public function zone() {
		$output = '<option value="">' . $this->language->get('text_select') . '</option>';
		
		$this->load->model('localisation/zone');

    	$results = $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']);
        
      	foreach ($results as $result) {
        	$output .= '<option value="' . $result['zone_id'] . '"';
	
	    	if (isset($this->request->get['zone_id']) && ($this->request->get['zone_id'] == $result['zone_id'])) {
	      		$output .= ' selected="selected"';
	    	}
	
	    	$output .= '>' . $result['name'] . '</option>';
    	} 
		
		if (!$results) {
		  	$output .= '<option value="0">' . $this->language->get('text_none') . '</option>';
		}
	
		$this->response->setOutput($output);
  	}	
}
?>