<?php 
class ControllerCheckoutCart extends Controller {
	private $error = array();
	
	public function index() {
		$this->language->load('checkout/cart');

		if (!isset($this->session->data['vouchers'])) {
			$this->session->data['vouchers'] = array();
		}
		
		// Update
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->cart->update($key, $value);
			}
			
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']); 
			unset($this->session->data['reward']);
			
			$this->redirect($this->url->link('checkout/cart'));  			
		}
       	
		// Remove
		if (isset($this->request->get['remove'])) {
			$this->cart->remove($this->request->get['remove']);
			
			unset($this->session->data['vouchers'][$this->request->get['remove']]);
			
			$this->session->data['success'] = $this->language->get('text_remove');
		
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']); 
			unset($this->session->data['reward']);  
								
			$this->redirect($this->url->link('checkout/cart'));
		}
			
		// Coupon    
		if (isset($this->request->post['coupon']) && $this->validateCoupon()) { 
			$this->session->data['coupon'] = $this->request->post['coupon'];
				
			$this->session->data['success'] = $this->language->get('text_coupon');
			
			$this->redirect($this->url->link('checkout/cart'));
		}
		
		// Voucher
		if (isset($this->request->post['voucher']) && $this->validateVoucher()) { 
			$this->session->data['voucher'] = $this->request->post['voucher'];
				
			$this->session->data['success'] = $this->language->get('text_voucher');
				
			$this->redirect($this->url->link('checkout/cart'));
		}

		// Reward
		if (isset($this->request->post['reward']) && $this->validateReward()) { 
			$this->session->data['reward'] = $this->request->post['reward'];
				
			$this->session->data['success'] = $this->language->get('text_reward');
				
			$this->redirect($this->url->link('checkout/cart'));
		}
		
		// Shipping
		if (isset($this->request->post['shipping_method']) && $this->validateShipping()) {
			$shipping = explode('.', $this->request->post['shipping_method']);
			
			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
			
			$this->session->data['success'] = $this->language->get('text_shipping');
			
			$this->redirect($this->url->link('checkout/cart'));
		}
		
		$this->document->setTitle($this->language->get('heading_title'));

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('common/home'),
        	'text'      => $this->language->get('text_home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('checkout/cart'),
        	'text'      => $this->language->get('heading_title'),
        	'separator' => $this->language->get('text_separator')
      	);


        $this->data['kosarvan'] = true;

        $csak_cart_info = false;
        if (isset($this->request->request['no_header']) && $this->request->request['no_header'] == 1)  {
            $csak_cart_info = true;
        }

        $erkezteto_oldal = false;
        if (isset($this->request->request['ekezteto_oldal']) && $this->request->request['ekezteto_oldal'] == 1)  {
            $erkezteto_oldal = true;
        }


        if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
			$points = $this->customer->getRewardPoints();
			
			$points_total = 0;
			
			foreach ($this->cart->getProducts() as $product) {
				if ($product['points']) {
					$points_total += $product['points'];
				}
			}		
				
      		$this->data['heading_title'] = $this->language->get('heading_title');
			
			$this->data['text_next'] = $this->language->get('text_next');
			$this->data['text_next_choice'] = $this->language->get('text_next_choice');
     		$this->data['text_use_coupon'] = $this->language->get('text_use_coupon');
			$this->data['text_use_voucher'] = $this->language->get('text_use_voucher');
			$this->data['text_use_reward'] = sprintf($this->language->get('text_use_reward'), $points);
			$this->data['text_shipping_estimate'] = $this->language->get('text_shipping_estimate');
			$this->data['text_shipping_detail'] = $this->language->get('text_shipping_detail');
			$this->data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$this->data['text_select'] = $this->language->get('text_select');
						
			$this->data['column_image'] = $this->language->get('column_image');
      		$this->data['column_name'] = $this->language->get('column_name');

      		$this->data['column_model'] = $this->language->get('column_model');
      		$this->data['shipping_checked'] = $this->language->get('shipping_checked');


            if ($this->config->get('megjelenit_form_admin_model') == 1) {
                $this->data['column_model'] = $this->language->get('column_model');
            } elseif ($this->config->get('megjelenit_form_admin_cikkszam') == 1) {
                $this->data['column_model'] = $this->language->get('column_cikkszam');
            } elseif ( $this->config->get('megjelenit_form_admin_cikkszam2') == 1) {
                $this->data['column_model'] = $this->language->get('column_cikkszam');
            } else {
                $this->data['column_model'] = "";
            }

      		$this->data['column_quantity'] = $this->language->get('column_quantity');
			$this->data['column_price'] = $this->language->get('column_price');
      		$this->data['column_total'] = $this->language->get('column_total');

            $this->data['megjelenit_kosar'] = $this->config->get('megjelenit_admin_kosar');
            $this->data['column_netto'] = $this->language->get('column_netto');
            $this->data['termek_ar_osszes'] = $this->language->get('termek_ar_osszes');
            $this->data['column_netto_total_price'] = $this->language->get('column_netto_total_price');
            $this->data['column_price_netto_ar'] = $this->language->get('column_price_netto_ar');
            $this->data['column_total_netto_ar'] = $this->language->get('column_total_netto_ar');

            $this->data['fizetendo_osszes'] = $this->language->get('fizetendo_osszes');
            $this->data['afa_osszes'] = $this->language->get('afa_osszes');
            $this->data['netto_osszes'] = $this->language->get('netto_osszes');


            $this->data['column_total'] = $this->language->get('column_total');
            $this->data['szallitas'] = $this->language->get('szallitas');
            $this->data['szallitas_netto_ar'] = $this->language->get('szallitas_netto_ar');
            $this->data['szallitas_brutto_ar'] = $this->language->get('szallitas_brutto_ar');

			
			$this->data['entry_coupon'] = $this->language->get('entry_coupon');
			$this->data['entry_voucher'] = $this->language->get('entry_voucher');
			$this->data['entry_reward'] = sprintf($this->language->get('entry_reward'), $points_total);
			$this->data['entry_country'] = $this->language->get('entry_country');
			$this->data['entry_zone'] = $this->language->get('entry_zone');
			$this->data['entry_postcode'] = $this->language->get('entry_postcode');
						
			$this->data['button_update'] = $this->language->get('button_update');
			$this->data['button_remove'] = $this->language->get('button_remove');
			$this->data['button_coupon'] = $this->language->get('button_coupon');
			$this->data['button_voucher'] = $this->language->get('button_voucher');
			$this->data['button_reward'] = $this->language->get('button_reward');
			$this->data['button_quote'] = $this->language->get('button_quote');
			$this->data['button_shipping'] = $this->language->get('button_shipping');			
      		$this->data['button_shopping'] = $this->language->get('button_shopping');
      		$this->data['button_checkout'] = $this->language->get('button_checkout');
			
			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
                $this->data['kosarvan'] = false;

            } elseif (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
      			$this->data['error_warning'] = $this->language->get('error_stock');
                $this->data['kosarvan'] = false;
			} else {
				$this->data['error_warning'] = '';
			}
			
			if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
				$this->data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$this->data['attention'] = '';
			}
						
			if (isset($this->session->data['success'])) {
				$this->data['success'] = $this->session->data['success'];
			
				unset($this->session->data['success']);
			} else {
				$this->data['success'] = '';
			}
			
			$this->data['action'] = $this->url->link('checkout/cart');   
						
			if ($this->config->get('config_cart_weight')) {
				$this->data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
			} else {
				$this->data['weight'] = '';
			}
						 
			$this->load->model('tool/image');
			
      		$this->data['products'] = array();
			
			$products = $this->cart->getProducts();

      		foreach ($products as $product) {
				$product_total = 0;
                $products_brutto = 0;
                $products_netto = 0;
				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}
                $product['minimum_warrning'] = true;
				if ($product['minimum'] > $product_total) {
					$this->data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
                    $product['minimum_warrning'] = false;
                    $this->data['kosarvan'] = false;
				}				
					
				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
					$image = '';
				}

				$option_data = array();

        		foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['option_value'];	
					} else {
						$filename = $this->encryption->decrypt($option['option_value']);
						
						$value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
					}
					
					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 40 ? utf8_substr($value, 0, 40) . '...' : $value)
					);
        		}


                if($product['utalvany'] == "1") {
                    $price = 0;
                    $total = 0;
                    $netto_price = $this->currency->format(0);
                    $netto_total_price = $this->currency->format(0);
                } else {
                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $total = $this->currency->format($this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $total = false;
                    }
                    $netto_price = $this->currency->format(($product['price']));
                    $netto_total_price = $this->currency->format(($product['total']));
                }

                if ($this->config->get('megjelenit_form_admin_model') == 1) {
                    $model = $product['model'];
                } elseif ($this->config->get('megjelenit_form_admin_cikkszam') == 1) {
                    $model = $product['cikkszam'];
                } elseif ( $this->config->get('megjelenit_form_admin_cikkszam2') == 1) {
                    $model = $product['cikkszam2'];
                } else {
                    $model = "";
                }

                $this->load->model("catalog/manufacturer");
                $manufacturer = $this->model_catalog_manufacturer->getManufacturer($product['manufacturer_id']);
                $manufacturer = isset($manufacturer['name']) && $manufacturer['name'] ? $manufacturer['name'] : "";

                $this->data['products'][] = array(
          			'key'               => $product['key'],
          			'thumb'             => $image,
					'name'              => $product['name'],
					'minimum_warrning'  => $product['minimum_warrning'],
          			'model'             => $model,
                    'cikkszam'          => $product['cikkszam'],
                    'manufacturer'      => $manufacturer,
                    'option'            => $option_data,
          			'quantity'          => $product['quantity'],
          			'stock'             => $product['stock'],
					'reward'            => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                    'netto_price'       => $netto_price,
                    'netto_total_price' => $netto_total_price,
					'price'             => $price,
					'total'             => $total,
					'href'              => $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'remove'            => $this->url->link('checkout/cart', 'remove=' . $product['key'])

				);
                foreach ($this->cart->getProducts() as $product) {
                    if($product['utalvany'] == "1") {
                        $products_brutto += 0;
                    } else {
                        $products_brutto += $this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'));
                    }
                $products_netto += $product['total'];
                }
      		}

            $products_brutto = isset($products_brutto) ? $products_brutto : 0;
            $products_netto =  isset($products_netto) ? $products_netto : 0;

			// Gift Voucher
			$this->data['vouchers'] = array();
			
			if (!empty($this->session->data['vouchers'])) {
                $voucher_osszesen = 0;
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$this->data['vouchers'][] = array(
						'key'         => $key,
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount']),
						'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)
					);
                    $voucher_osszesen += $voucher['amount'];
				}
                $products_brutto += $voucher_osszesen;
                $products_netto += $voucher_osszesen;
			}

            $this->data['products_brutto'] = $this->currency->format(($products_brutto));
            $this->data['products_netto'] = $this->currency->format(($products_netto));


            if (isset($this->request->post['next'])) {
				$this->data['next'] = $this->request->post['next'];
			} else {
				$this->data['next'] = '';
			}
						 
			$this->data['coupon_status'] = $this->config->get('coupon_status');
			
			if (isset($this->request->post['coupon'])) {
				$this->data['coupon'] = $this->request->post['coupon'];			
			} elseif (isset($this->session->data['coupon'])) {
				$this->data['coupon'] = $this->session->data['coupon'];
			} else {
				$this->data['coupon'] = '';
			}
			
			$this->data['voucher_status'] = $this->config->get('voucher_status');
			
			if (isset($this->request->post['voucher'])) {
				$this->data['voucher'] = $this->request->post['voucher'];				
			} elseif (isset($this->session->data['voucher'])) {
				$this->data['voucher'] = $this->session->data['voucher'];
			} else {
				$this->data['voucher'] = '';
			}
			
			$this->data['reward_status'] = ($points && $points_total && $this->config->get('reward_status'));
			
			if (isset($this->request->post['reward'])) {
				$this->data['reward'] = $this->request->post['reward'];				
			} elseif (isset($this->session->data['reward'])) {
				$this->data['reward'] = $this->session->data['reward'];
			} else {
				$this->data['reward'] = '';
			}

			$this->data['shipping_status'] = $this->config->get('shipping_status') && $this->cart->hasShipping();	
								
			if (isset($this->request->post['country_id'])) {
				$this->data['country_id'] = $this->request->post['country_id'];				
			} elseif (isset($this->session->data['guest']['shipping']['country_id'])) {
				$this->data['country_id'] = $this->session->data['guest']['shipping']['country_id'];			  	
			} else {
				$this->data['country_id'] = $this->config->get('config_country_id');
			}
				
			$this->load->model('localisation/country');
			
			$this->data['countries'] = $this->model_localisation_country->getCountries();
						
			if (isset($this->request->post['zone_id'])) {
				$this->data['zone_id'] = $this->request->post['zone_id'];				
			} elseif (isset($this->session->data['guest']['shipping']['zone_id'])) {
				$this->data['zone_id'] = $this->session->data['guest']['shipping']['zone_id'];			
			} else {
				$this->data['zone_id'] = '';
			}
			
			if (isset($this->request->post['postcode'])) {
				$this->data['postcode'] = $this->request->post['postcode'];				
			} elseif (isset($this->session->data['guest']['shipping']['postcode'])) {
				$this->data['postcode'] = $this->session->data['guest']['shipping']['postcode'];					
			} else {
				$this->data['postcode'] = '';
			}
			
			if (isset($this->request->post['shipping_method'])) {
				$this->data['shipping_method'] = $this->request->post['shipping_method'];				
			} elseif (isset($this->session->data['shipping_method'])) {
				$this->data['shipping_method'] = $this->session->data['shipping_method']['code']; 
			} else {
				$this->data['shipping_method'] = '';
			}

            $this->data['shipping_arak'] = array(
                "netto"  => isset($_SESSION['shipping_method']['netto_text']) ? $_SESSION['shipping_method']['netto_text'] : 0,
                "brutto" => isset($_SESSION['shipping_method']['text']) ? $_SESSION['shipping_method']['text'] : "",
                "title" => isset($_SESSION['shipping_method']['title']) ? $_SESSION['shipping_method']['title'] : "",
                "netto_value" => isset($_SESSION['shipping_method']['title']) ? $_SESSION['shipping_method']['title'] : "",
                "title" => isset($_SESSION['shipping_method']['title']) ? $_SESSION['shipping_method']['title'] : ""

            );


						
			// Totals
			$this->load->model('setting/extension');
			
			$total_data = array();					
			$total = 0;
			$taxes = $this->cart->getTaxes();
			
			$sort_order = array(); 
			
			$results = $this->model_setting_extension->getExtensions('total');
			
			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}
			
			array_multisort($sort_order, SORT_ASC, $results);
			
			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);
		
					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
				
				$sort_order = array(); 
			  
				foreach ($total_data as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}
	
				array_multisort($sort_order, SORT_ASC, $total_data);			
			}



			$this->data['totals'] = $total_data;

            $this->data['continue'] = $this->url->link('common/home');

            if ($erkezteto_oldal) {
    			$this->data['checkout'] = $this->url->link('checkout/checkout', 'ekezteto_oldal=1', 'SSL');
            } else {
			    $this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
            }

            if ($csak_cart_info) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart_info.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/checkout/cart_info.tpl';
                } else {
                    $this->template = 'default/template/checkout/cart_info.tpl';
                }
            } elseif ($erkezteto_oldal) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/erkezteto_cart.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/checkout/erkezteto_cart.tpl';
                } else {
                    $this->template = 'default/template/checkout/erkezteto_cart.tpl';
                }
            } else {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/checkout/cart.tpl';
                } else {
                    $this->template = 'default/template/checkout/cart.tpl';
                }
                $this->children = array(
                    'common/column_left',
                    'common/column_right',
                    'common/content_bottom',
                    'common/content_top',
                    'common/footer',
                    'common/header'
                );
            }

            $this->response->setOutput($this->render());
    	} else {
      		$this->data['heading_title'] = $this->language->get('heading_title');

      		$this->data['text_error'] = $this->language->get('text_empty');

      		$this->data['button_continue'] = $this->language->get('button_continue');
			
      		$this->data['continue'] = $this->url->link('common/home');

			unset($this->session->data['success']);

            if ($csak_cart_info) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/simple_not_found.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/error/simple_not_found.tpl';
                } else {
                    $this->template = 'default/template/error/simple_not_found.tpl';
                }
            } else {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
                } else {
                    $this->template = 'default/template/error/not_found.tpl';
                }
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
					
			$this->response->setOutput($this->render());			
    	}

  	}
	
	private function validateCoupon() {
		$this->load->model('checkout/coupon');
				
		$coupon_info = $this->model_checkout_coupon->getCoupon($this->request->post['coupon']);			
		
		if (!$coupon_info) {			
			$this->error['warning'] = $this->language->get('error_coupon');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}

    private function validateMennyiseg() {
        $this->load->model('checkout/coupon');

        $coupon_info = $this->model_checkout_coupon->getCoupon($this->request->post['coupon']);

        if (!$coupon_info) {
            $this->error['warning'] = $this->language->get('error_coupon');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
	
	private function validateVoucher() {
		$this->load->model('checkout/voucher');
				
		$voucher_info = $this->model_checkout_voucher->getVoucher($this->request->post['voucher']);			
		
		if (!$voucher_info) {			
			$this->error['warning'] = $this->language->get('error_voucher');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	private function validateReward() {
		$points = $this->customer->getRewardPoints();
		
		$points_total = 0;
		
		foreach ($this->cart->getProducts() as $product) {
			if ($product['points']) {
				$points_total += $product['points'];
			}
		}	
				
		if (empty($this->request->post['reward'])) {
			$this->error['warning'] = $this->language->get('error_reward');
		}
	
		if ($this->request->post['reward'] > $points) {
			$this->error['warning'] = sprintf($this->language->get('error_points'), $this->request->post['reward']);
		}
		
		if ($this->request->post['reward'] > $points_total) {
			$this->error['warning'] = sprintf($this->language->get('error_maximum'), $points_total);
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	private function validateShipping() {
		if (!empty($this->request->post['shipping_method'])) {
			$shipping = explode('.', $this->request->post['shipping_method']);
					
			if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {			
				$this->error['warning'] = $this->language->get('error_shipping');
			}
		} else {
			$this->error['warning'] = $this->language->get('error_shipping');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
								
	public function add() {
        $time_pre = microtime(true);

        $this->language->load('checkout/cart');
		
		$json = array();
        $redirect = false;

        $error = '';
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
						
		$product_info = $this->model_catalog_product->getProduct($product_id);

        $betolt = true;
        if ($product_info) {
            if ($product_info["utalvany"] == 1 || !empty($product_info['egyszer_kosarba'])){
                foreach ($_SESSION["cart"] as $key=>$value) {
                    if ($product_info["product_id"] == $key) {
                        $betolt = false;
                        break;
                    }
                }
            }
        }

        if (!$betolt) {
            $json['error']['marvan'] = $this->language->get('text_mar_van');

        }
		
		if ($product_info && $betolt) {
			if (isset($this->request->post['quantity'])) {
                $this->request->post['quantity'] = str_ireplace(',','.',$this->request->post['quantity']);

                $quantity = $this->request->post['quantity'];

                $eddigi_mennyiseg = 0;
                if ( isset($this->session->data['cart']) ) {
                    foreach($this->session->data['cart'] as $key=>$value) {
                        $key = explode(':',$key);
                        if ($key[0] == $product_id) {
                            $eddigi_mennyiseg += $value;
                        }
                    }
                }

                if ( ((float)$eddigi_mennyiseg + (float)$this->request->post['quantity']) < $product_info['minimum']) {
                    $json['error']['warning'] ="Minimálisan választható mennyiség: ".$product_info['minimum'].$product_info['megyseg'];
                    $redirect = true;
                    $error = $json['error']['warning'];
                }




			} else {
				$quantity = 1;
			}

            if (isset($this->request->post['quantity2'])) {
                $quantity2 = $this->request->post['quantity2'];
            }
														
			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();	
			}
			
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                if( $product_option['type'] != 'checkbox_qty') {
                    if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                        $json['error']['option'] = sprintf($this->language->get('error_required'), $product_option['name']);
                        $redirect = true;
                        $error = $json['error']['option'];

                        break;
                    }
                } else {
                    if ($this->request->get['termek_aloldal'] != "true" && $product_info['szin_meret_szukseges']) {
                        $json['error']['option'] = $this->language->get('error_meret_szin');
                        $redirect = true;
                        $error = $json['error']['option'];
                        break;
                    }
                }
            }

			if ($product_info['szin_meret_szukseges'] ) {

				if ($this->request->get['termek_aloldal'] == "true") {
					$mehet = false;

					if ($option) {
						foreach ($option as $valasztekok) {
							if ($valasztekok) {
								foreach ($valasztekok as $valasztek) {
									$valasztek = explode('|', $valasztek);
									$valasztek = $valasztek[0] ? $valasztek[0] : false;
									if ($valasztek) {


										foreach ($product_options as $product_option) {
											if ($product_option['type'] == "checkbox_qty") {
												foreach ($product_option['option_value'] as $value) {
													if ($value['product_option_value_id'] == $valasztek) {
														$mehet = true;
														break;
													}
												}
											}
											if ($mehet) break;
										}


									} else break;

									if ($mehet) break;
								}
							} else break;
						}
					}
					if (!$mehet) {
						$json['error']['option'] = $this->language->get('error_meret_szin');
						$redirect = true;
						$error = $json['error']['option'];
					}
				}
			}

            if (!$json) {
                if (isset($quantity2)) {
                    $this->cart->add($this->request->post['product_id'], $quantity2, $option);

                } else {
                    $this->cart->add($this->request->post['product_id'], $quantity, $option);
                }

                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);

                // Totals
                $this->load->model('setting/extension');
				
				$total_data = array();					
				$total = 0;
				$taxes = $this->cart->getTaxes();
				
				$sort_order = array(); 
				
				$results = $this->model_setting_extension->getExtensions('total');
				
				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}
				
				array_multisort($sort_order, SORT_ASC, $results);
				
				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('total/' . $result['code']);
			
						$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
					}
					
					$sort_order = array(); 
				  
					foreach ($total_data as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}
		
					array_multisort($sort_order, SORT_ASC, $total_data);			
				}
				
				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
			} elseif(isset($redirect) && $redirect && $this->request->get['termek_aloldal'] != "true") {
                $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']) . ($error ? ('&warning='.$error) : ''));
			}
		}
        $time_post = microtime(true);

        $exec_time = $time_post - $time_pre;
        //$this->log->write('index idö: '.$exec_time);

        $this->response->setOutput(json_encode($json));
	}


    public function addMegrendelem() {

        unset ($this->session->data['megrendelem']);
        $this->language->load('checkout/cart');

        $json = array();
        $redirect = false;

        $error = '';
        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];

            $this->load->model('catalog/product');
            $product_info = $this->model_catalog_product->getProduct($product_id);

        } else {
            $product_info = false;
        }





        if ($product_info) {

            if (isset($this->request->post['quantity'])) {
                $this->request->post['quantity'] = str_ireplace(',','.',$this->request->post['quantity']);
                $quantity = $this->request->post['quantity'];
                if ( (  (float)$this->request->post['quantity']) < $product_info['minimum']) {
                    $json['error']['warning'] ="Minimálisan választható mennyiség: ".$product_info['minimum'].$product_info['megyseg'];
                    $redirect = true;
                    $error = $json['error']['warning'];
                }
            } else {
                $quantity = 1;
            }

            if (isset($this->request->post['quantity2'])) {
                $quantity2 = $this->request->post['quantity2'];
            }

            if (isset($this->request->post['option'])) {
                $option = array_filter($this->request->post['option']);
            } else {
                $option = array();
            }

            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                if( $product_option['type'] != 'checkbox_qty') {
                    if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                        $json['error']['option'] = sprintf($this->language->get('error_required'), $product_option['name']);
                        $redirect = true;
                        $error = $json['error']['option'];

                        break;
                    }
                } else {
                    if ($this->request->get['termek_aloldal'] != "true" && $product_info['szin_meret_szukseges']) {
                        $json['error']['option'] = $this->language->get('error_meret_szin');
                        $redirect = true;
                        $error = $json['error']['option'];
                        break;
                    }
                }
            }

            if ($product_info['szin_meret_szukseges'] ) {

                if ($this->request->get['termek_aloldal'] == "true") {
                    $mehet = false;

                    if ($option) {
                        foreach ($option as $valasztekok) {
                            if ($valasztekok) {
                                foreach ($valasztekok as $valasztek) {
                                    $valasztek = explode('|', $valasztek);
                                    $valasztek = $valasztek[0] ? $valasztek[0] : false;
                                    if ($valasztek) {


                                        foreach ($product_options as $product_option) {
                                            if ($product_option['type'] == "checkbox_qty") {
                                                foreach ($product_option['option_value'] as $value) {
                                                    if ($value['product_option_value_id'] == $valasztek) {
                                                        $mehet = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if ($mehet) break;
                                        }


                                    } else break;

                                    if ($mehet) break;
                                }
                            } else break;
                        }
                    }
                    if (!$mehet) {
                        $json['error']['option'] = $this->language->get('error_meret_szin');
                        $redirect = true;
                        $error = $json['error']['option'];
                    }
                }
            }

            if (!$json) {
                if (isset($quantity2)) {
                    $this->cart->addMegrendelem($this->request->post['product_id'], $quantity2, $option);

                } else {
                    $this->cart->addMegrendelem($this->request->post['product_id'], $quantity, $option);
                }

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);

                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

                $json['redirect'] = $this->url->link('checkout/checkout_megrendelem');

            } elseif(!empty($redirect) && $this->request->get['termek_aloldal'] != "true") {
                $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']) . ($error ? ('&warning='.$error) : ''));
            }
        }

        $this->response->setOutput(json_encode($json));
    }
	
	public function quote() {

        $this->data['megjelenit_kosar'] = $this->config->get('megjelenit_admin_kosar');

        $this->language->load('checkout/cart');
		
		$json = array();


        // Totals
        $total_data = array();
        $total = 0;
        $taxes = $this->cart->getTaxes();

        $this->load->model('setting/extension');

        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get($result['code'] . '_status')) {
                $this->load->model('total/' . $result['code']);

                $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
            }
        }

		if (!$this->cart->hasProducts()) {
			$json['error']['warning'] = $this->language->get('error_product');				
		}				

		if (!$this->cart->hasShipping()) {
			$json['error']['warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));				
		}				
		
		if ($this->request->post['country_id'] == '') {
			$json['error']['country'] = $this->language->get('error_country');
		}

        if ($this->data['megjelenit_kosar']['cart_shipping_region'] == 1) {
            if ($this->request->post['zone_id'] == '') {
			    $json['error']['zone'] = $this->language->get('error_zone');
		    }
		}

		$this->load->model('localisation/country');
		
		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

        if ($this->data['megjelenit_kosar']['cart_shipping_postcode'] == 1) {
            if ($country_info && $country_info['postcode_required'] && (utf8_strlen($this->request->post['postcode']) < 2)
                || (utf8_strlen($this->request->post['postcode']) > 10)) {
                $json['error']['postcode'] = $this->language->get('error_postcode');
            }
		}

		if (!$json) {		
			$this->tax->setShippingAddress($this->request->post['country_id'], $this->request->post['zone_id']);
		
			$this->session->data['guest']['shipping']['country_id'] = $this->request->post['country_id'];
			$this->session->data['guest']['shipping']['zone_id'] = $this->request->post['zone_id'];
			$this->session->data['guest']['shipping']['postcode'] = $this->request->post['postcode'];
		
			if ($country_info) {
				$country = $country_info['name'];
				$iso_code_2 = $country_info['iso_code_2'];
				$iso_code_3 = $country_info['iso_code_3'];
				$address_format = $country_info['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';	
				$address_format = '';
			}
			
			$this->load->model('localisation/zone');
		
			$zone_info = $this->model_localisation_zone->getZone($this->request->post['zone_id']);
			
			if ($zone_info) {
				$zone = $zone_info['name'];
				$code = $zone_info['code'];
			} else {
				$zone = '';
				$code = '';
			}	
		 
			$address_data = array(
				'firstname'      => '',
				'lastname'       => '',
				'company'        => '',
				'adoszam'        => '',
				'address_1'      => '',
				'address_2'      => '',
				'postcode'       => $this->request->post['postcode'],
				'city'           => '',
				'zone_id'        => $this->request->post['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $code,
				'country_id'     => $this->request->post['country_id'],
				'country'        => $country,	
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format
			);
		
			$quote_data = array();

			$this->load->model('setting/extension');
			
			$results = $this->model_setting_extension->getExtensions('shipping');
			
			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('shipping/' . $result['code']);
					
					$quote = $this->{'model_shipping_' . $result['code']}->getQuote($address_data); 
		
					if ($quote) {
                        if ($result['code'] == "weight"){
                            $query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "biztositas WHERE biztositas_tol <= $total AND biztositas_ig >= $total");

                            foreach($quote['quote'] as $key=>$value){
                                if ($quote['quote'][$key]['biztositas'] == 1) {
                                    $quote['quote'][$key]['cost']+=$query1->row['biztositas_ertek'];

                                    $quote['quote'][$key]['text'] = $this->currency->format($this->tax->calculate($quote['quote'][$key]['cost'], $this->config->get('weight_tax_class_id'), $this->config->get('config_tax')));
                                    $quote['quote'][$key]['netto_text'] = $this->currency->format($quote['quote'][$key]['cost']);
                                }

                            }
                        }
						$quote_data[$result['code']] = array( 
							'title'      => $quote['title'],
							'quote'      => $quote['quote'], 
							'sort_order' => $quote['sort_order'],
							'error'      => $quote['error']
						);
					}
				}
			}



            if (isset($quote_data['item_product'])) {
                if ( isset($quote_data['item_product']['quote']['item_product']['szallitasi_mod']) ) {
                    $szallitasi = array();
                    foreach($quote_data['item_product']['quote']['item_product']['szallitasi_mod'] as $szallitas) {
                        $szallitasi[] = $szallitas['szallitasi_mod'];
                    }
                    foreach($quote_data as $key=>$szallitasok) {
                        if ( !in_array($key,$szallitasi) && $key != "item_product") {
                            unset($quote_data[$key]);
                        }
                    }
                }
            }

            if (isset($quote_data['item_product_gls'])) {
                if ( isset($quote_data['item_product_gls']['quote']['item_product_gls']['szallitasi_mod']) ) {
                    $szallitasi = array();
                    foreach($quote_data['item_product_gls']['quote']['item_product_gls']['szallitasi_mod'] as $szallitas) {
                        $szallitasi[] = $szallitas['szallitasi_mod'];
                    }
                    foreach($quote_data as $key=>$szallitasok) {
                        if ( !in_array($key,$szallitasi) && $key != "item_product_gls") {
                            unset($quote_data[$key]);
                        }
                    }
                }
            }

			$sort_order = array();

			foreach ($quote_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}
	
			array_multisort($sort_order, SORT_ASC, $quote_data);
			
			$this->session->data['shipping_methods'] = $quote_data;
			
			if ($this->session->data['shipping_methods']) {
				$json['shipping_method'] = $this->session->data['shipping_methods']; 
			} else {
				$json['error']['warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
			}				
		}	
		
		$this->response->setOutput(json_encode($json));						
	}
	
  	public function zone() {
		$output = '<option value="">' . $this->language->get('text_select') . '</option>';
		
		$this->load->model('localisation/zone');

    	$results = $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']);
        
      	foreach ($results as $result) {
        	$output .= '<option value="' . $result['zone_id'] . '"';
	
	    	if (isset($this->request->get['zone_id']) && ($this->request->get['zone_id'] == $result['zone_id'])) {
	      		$output .= ' selected="selected"';
	    	}
	
	    	$output .= '>' . $result['name'] . '</option>';
    	} 
		
		if (!$results) {
		  	$output .= '<option value="0">' . $this->language->get('text_none') . '</option>';
		}
	
		$this->response->setOutput($output);
  	}

    public function kosarban() {
        $json = array();

        if (!empty($_SESSION['cart'])) {
            $json['success'] = 'igen';
        }
        $this->response->setOutput(json_encode($json));

    }


}
?>