<?php
class ControllerModuleMultiSubCategory extends Controller {
    public  $children_data = array();

    protected function index($setting) {
        $this->language->load('module/category');



        $this->data['heading_title'] = $this->language->get('heading_title');

        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string)$this->request->get['path']);
        } else {
            $parts = array();
        }

        $subcategory=false;
        if ( isset($_REQUEST['path']) && !empty($_REQUEST['path']) ) {
            $subcategory = $parts[0];
        }

        if ($this->config->get('multi_sub_category_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

        $this->data['parts'] = $parts;
        if (isset($parts[0])) {
            $this->data['category_id'] = $parts[0];
        } else {
            $this->data['category_id'] = 0;
        }

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('module/multi_category');


        if (isset($_SESSION['categoryAll'])) {
            $this->data['categories'] = $_SESSION['categoryAll'];
        } else {
            $this->data['categories'] = array();
            $arr2 = $this->model_module_multi_category->getCategoriesAll(0);
            $this->data['categories'] = $this->buildTree($arr2, 0, "");
            $_SESSION['categoryAll'] = $this->data['categories'];
        }


        $sortingSettings = array(
            0       =>  array(
                'orderby' =>  'sort_order',
                'sortorder'     => 'ASC'
            )
        );

        $sortedBody = new ArrayOfArrays($this->data['categories']);
        $sortedBody->multiSorting($sortingSettings,true);
        $this->data['categories'] = $sortedBody->getArrayCopy();

        foreach( $this->data['categories'] as $key=>$value) {
            if ($subcategory && $value['category_id'] != $subcategory){
                unset ($this->data['categories'][$key]);
            }
        }

        foreach($this->data['categories'] as $key=> $value) {
            if (isset($value['children']) && count($value['children']) > 1 ) {
                $sortingSettings = array(
                    0       =>  array(
                        'orderby' =>  'sort_order',
                        'sortorder'     => 'ASC'
                    )
                );

                $sortedBody = new ArrayOfArrays($value['children']);
                $sortedBody->multiSorting($sortingSettings,true);
                $this->data['categories'][$key]['children'] = $sortedBody->getArrayCopy();

                foreach($this->data['categories'][$key]['children'] as $key1=> $value1) {
                    if (isset($value1['children']) && count($value1['children']) > 1 ) {
                        $sortingSettings = array(
                            0       =>  array(
                                'orderby' =>  'sort_order',
                                'sortorder'     => 'ASC'
                            )
                        );

                        $sortedBody = new ArrayOfArrays($value1['children']);
                        $sortedBody->multiSorting($sortingSettings,true);
                        $this->data['categories'][$key]['children'][$key1]['children'] = $sortedBody->getArrayCopy();

                        foreach($this->data['categories'][$key]['children'][$key1]['children'] as $key2=> $value2) {
                            if (isset($value2['children']) && count($value2['children']) > 1 ) {
                                $sortingSettings = array(
                                    0       =>  array(
                                        'orderby' =>  'sort_order',
                                        'sortorder'     => 'ASC'
                                    )
                                );

                                $sortedBody = new ArrayOfArrays($value2['children']);
                                $sortedBody->multiSorting($sortingSettings,true);
                                $this->data['categories'][$key]['children'][$key1]['children'][$key2]['children'] = $sortedBody->getArrayCopy();

                                foreach($this->data['categories'][$key]['children'][$key1]['children'][$key2]['children'] as $key3=> $value3) {
                                    if (isset($value3['children']) && count($value3['children']) > 1 ) {
                                        $sortingSettings = array(
                                            0       =>  array(
                                                'orderby' =>  'sort_order',
                                                'sortorder'     => 'ASC'
                                            )
                                        );

                                        $sortedBody = new ArrayOfArrays($value3['children']);
                                        $sortedBody->multiSorting($sortingSettings,true);
                                        $this->data['categories'][$key]['children'][$key1]['children'][$key2]['children'][$key3]['children'] = $sortedBody->getArrayCopy();

                                        foreach($this->data['categories'][$key]['children'][$key1]['children'][$key2]['children'][$key3]['children'] as $key4=> $value4) {
                                            if (isset($value4['children']) && count($value4['children']) > 1 ) {
                                                $sortingSettings = array(
                                                    0       =>  array(
                                                        'orderby' =>  'sort_order',
                                                        'sortorder'     => 'ASC'
                                                    )
                                                );

                                                $sortedBody = new ArrayOfArrays($value4['children']);
                                                $sortedBody->multiSorting($sortingSettings,true);
                                                $this->data['categories'][$key]['children'][$key1]['children'][$key2]['children'][$key3]['children'][$key4]['children'] = $sortedBody->getArrayCopy();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/multi_category.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/multi_category.tpl';
        } else {
            $this->template = 'default/template/module/multi_category.tpl';
        }

        $this->render();

    }

    function buildTree(array $elements, $parentId = 0, $path) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $ha_piacter="";
                if ($element['piacter'] == 1){
                    $ha_piacter.="&piacter=piacter";
                }

                $element['href'] =  $this->url->link('product/category', 'path=' . $path.$element['category_id'].$ha_piacter );
                $children = $this->buildTree($elements, $element['category_id'], $path.$element['category_id']."_");
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }



}
?>