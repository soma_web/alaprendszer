<?php if (isset($termekadatok['megjelenit_product_reward']) && !empty($termekadatok['megjelenit_product_reward'])) { ?>
    <div class="termekadatok termekadatok-jutalom" >
        <span class="termekadatok-jutalom-szoveg"><?php echo $termekadatok['megjelenit_product_reward']['text_name']; ?></span>
        <span class="termekadatok-jutalom-ertek"><?php echo $termekadatok['megjelenit_product_reward']['value']; ?></span>
    </div>
<?php } ?>