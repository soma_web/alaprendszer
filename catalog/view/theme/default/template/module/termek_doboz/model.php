<?php
$_['model_kotojel']          = array(
    'type'              => 'checkbox',
    'name'              => 'model_kotojel',
    'default'           => "1",
    'heading'           => 'Kötőjel a modell után'
);

$_['cikkszam']          = array(
    'type'              => 'checkbox',
    'name'              => 'cikkszam',
    'default'           => "1",
    'heading'           => 'Cikkszám'
);

$_['cikkszam_felirat']          = array(
    'type'              => 'checkbox',
    'name'              => 'cikkszam_felirat',
    'default'           => "1",
    'heading'           => 'Cikkszám felirat'
);

?>