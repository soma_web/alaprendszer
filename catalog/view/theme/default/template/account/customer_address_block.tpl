<div class="content">
    <table class="form">
        <?php if ( ($this->config->get('megjelenit_regisztracio_cimadat_cegnev') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_cimadat_cegnev') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><?php echo $entry_company; ?></td>
                <td><input type="text" name="company_cim" value="<?php echo $company_cim; ?>" /></td>
            </tr>
        <?php } ?>

        <?php if ( ($this->config->get('megjelenit_regisztracio_cimadat_adoszam') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_cimadat_adoszam') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><?php echo $entry_adoszam; ?></td>
                <td><input type="text" name="adoszam" value="<?php echo $adoszam; ?>" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_utca') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_utca') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
                <td><input type="text" name="address_1" value="<?php echo $address_1; ?>" />
                    <?php if ($error_address_1) { ?>
                        <span class="error"><?php echo $error_address_1; ?></span>
                    <?php } ?></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_cimadat_kiegeszito') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_cimadat_kiegeszito') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><?php echo $entry_address_2; ?></td>
                <td><input type="text" name="address_2" value="<?php echo $address_2; ?>" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_varos') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_varos') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_city; ?></td>
                <td><input type="text" name="city" value="<?php echo $city; ?>" />
                    <?php if ($error_city) { ?>
                        <span class="error"><?php echo $error_city; ?></span>
                    <?php } ?></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_iranyitoszam') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_iranyitoszam') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
                <td><input type="text" name="postcode" value="<?php echo $postcode; ?>" />
                    <?php if ($error_postcode) { ?>
                        <span class="error"><?php echo $error_postcode; ?></span>
                    <?php } ?></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_orszag') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_orszag') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_country; ?></td>
                <td><select name="country_id" onchange="$('select[name=\'zone_id\']').load('index.php?route=account/register/zone&country_id=' + this.value + '&zone_id=<?php echo $zone_id; ?>');">
                        <option value=""><?php echo $text_select; ?></option>
                        <?php foreach ($countries as $country) { ?>
                            <?php if ($country['country_id'] == $country_id) { ?>
                                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    <?php if ($error_country) { ?>
                        <span class="error"><?php echo $error_country; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_megye') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_megye') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
                <td><select name="zone_id">
                    </select>
                    <?php if ($error_zone) { ?>
                        <span class="error"><?php echo $error_zone; ?></span>
                    <?php } ?></td>
            </tr>
        <?php } ?>
    </table>
</div>