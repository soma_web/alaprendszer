<?php
class ModelCatalogCategory extends Model {
	public function getCategory($category_id) {
        $sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "category c
		    LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
		    LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)
		WHERE
		        c.category_id = '" . (int)$category_id . "'
		    AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
		    AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
		    AND c.status = '1'";
        $query = $this->db->query($sql);
		return $query->row;
	}
	
	public function getCategories($parent_id = 0, $termekreszurt=false, $attribute_id=false) {

        $sql ="SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
        LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)";



        $sql .=" WHERE c.parent_id = '" . (int)$parent_id . "
            ' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "
            ' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "
            '  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";





        $sql2 ="SELECT *, c.sort_order FROM " . DB_PREFIX . "category c
            LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
		    LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)
		    INNER JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.category_id = c.category_id)
		    LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = p2c.product_id)";

        if ($attribute_id) {
            $sql2 .= "INNER JOIN " . DB_PREFIX . "product_attribute pa ON (pa.product_id = p.product_id)";
        }

		$sql2 .=" WHERE c.parent_id = '" . (int)$parent_id . "'
		        AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
		        AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
		        AND c.status = '1'
		        AND p.status = '1' ";

        if ($attribute_id) {
            $sql2 .= " AND pa.attribute_id = '".$attribute_id."'";
        }




        $szuresek = $this->config->get('megjelenit_altalanos');

        if ($szuresek['mennyisegre_szur'] == 1) {
            $sql2 .= " AND p.quantity > 0 ";
        }
        if ($szuresek['ervenesseg_datumra_szur'] == 1) {
            $sql2 .= " AND ( p.date_ervenyes_ig = '' OR p.date_ervenyes_ig = '0000-00-00' OR p.date_ervenyes_ig >= NOW() ) ";
        }

        $sql2 .=  " GROUP BY c.category_id ORDER BY c.sort_order, LCASE(cd.name) ";

        if ($termekreszurt){
            $query = $this->db->query($sql2);

        } else {
            $query = $this->db->query($sql);

        }
		return $query->rows;
	}

	public function getCategoriesByParentId($category_id) {
		$category_data = array();
		
		$category_query = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$category_id . "'");
		
		foreach ($category_query->rows as $category) {
			$category_data[] = $category['category_id'];
			
			$children = $this->getCategoriesByParentId($category['category_id']);
			
			if ($children) {
				$category_data = array_merge($children, $category_data);
			}			
		}
		
		return $category_data;
	}
		
	public function getCategoryLayoutId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");
		
		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return $this->config->get('config_layout_category');
		}
	}
					
	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");
		
		return $query->row['total'];
	}

    public function getCategoryFilters($category_id) {
        $implode = array();

        $query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $implode[] = (int)$result['filter_id'];
        }


        $filter_group_data = array();

        if ($implode) {
            $filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

            foreach ($filter_group_query->rows as $filter_group) {
                $filter_data = array();

                $filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int)$filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

                foreach ($filter_query->rows as $filter) {
                    $filter_data[] = array(
                        'filter_id' => $filter['filter_id'],
                        'name'      => $filter['name']
                    );
                }

                if ($filter_data) {
                    $filter_group_data[] = array(
                        'filter_group_id' => $filter_group['filter_group_id'],
                        'name'            => $filter_group['name'],
                        'filter'          => $filter_data
                    );
                }
            }
        }

        return $filter_group_data;
    }

    public function getFilterGroups() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter_group_description WHERE language_id = '" . $this->config->get('config_language_id') . "'");

    return $query->rows;
    }

    public function getFilterSelect() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter_select WHERE language_id = '" . $this->config->get('config_language_id') . "'");

        return $query->rows;
    }

}
?>