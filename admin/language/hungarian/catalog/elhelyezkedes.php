<?php

// Heading
$_['heading_title']       = 'Elhelyezkedés';
$_['button_insert']       = 'Beszúrás';
$_['button_delete']       = 'Törlés';
$_['error_warning']       = '';
$_['success']             = '';
$_['column_elhelyezkedes']  = 'Megjelenés csoportok';
$_['column_sort_order']   = 'Sorrend';
$_['column_action']       = 'Művelet';
$_['column_maximum']      = 'Max.';
$_['column_ara']          = 'Ára';
$_['column_status']       = 'Státusz';
$_['column_alcsoport_status']  = 'Alcsoport státusz';

$_['entry_keret_kiemeles']  = 'Keret kiemelése:';
$_['entry_hatter_kiemeles'] = 'Háttér kiemelése:';
$_['entry_nagymeret']       = 'Nagy méret:';
$_['entry_sorrend']         = 'Sorrend:';
$_['entry_darabszam']       = 'Maximális darabszám:';
$_['entry_ara']             = 'Ára:';
$_['entry_status']          = 'Státusz:';
$_['entry_alcsoport_status']  = 'Alcsoport státusz:';
$_['entry_tax_class']       = 'Adó osztály:';
$_['text_engedelyezett']    = 'Engedélyezett:';
$_['text_letiltott']        = 'Letiltott:';

?>