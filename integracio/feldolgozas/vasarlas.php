<?php
class Vasarlas{

    private $query;

    public function __construct($query) {
        $this->query = $query;
    }

    public function Termek_Vasarlas() {


        $sql = "SELECT * FROM sapnak_rendeles_termek";
        $query_rendeles= $this->query->query($sql);
        if (isset($query_rendeles->num_rows) && $query_rendeles->num_rows > 0){

            $xml="<BOM>\n";

            for ($i=0; $query_rendeles->num_rows > $i; $i++){
                $xml .="  <BO>\n";

                $xml .="    <AdmInfo>\n";
                $xml .="      <Object>112</Object>\n";
                $xml .="      <Version>2</Version>\n";
                $xml .="    </AdmInfo>\n";

                $csokkent=false;
                $sor= $query_rendeles->rows[$i];

                $sql = "SELECT * FROM  sapnak_rendeles_vevo WHERE rendeles_azonosito LIKE ".$sor['rendeles_azonosito'];
                $query_vevo= $this->query->query($sql);

                $xml .= "    <Documents>\n";
                $xml .= "      <row>\n";
                $xml .= "      <DocObjectCode>17</DocObjectCode>\n";
                foreach($query_vevo->row as $key => $value){
                    if ($key != "rendeles_azonosito"){
                        $xml .= "        <$key>";
                        $xml .=$value;
                        $xml .= "</$key>\n";
                    }
                }
                $xml .= "      </row>\n";
                $xml .= "    </Documents>\n";


                $xml .= "    <Document_Lines>\n";
                while ($sor['rendeles_azonosito'] == $query_rendeles->rows[$i]['rendeles_azonosito']){

                    $xml .= "      <row>\n";
                    foreach($query_rendeles->rows[$i] as $key => $value){
                        if ($key != "rendeles_azonosito"){
                            $xml .= "        <$key>";
                            $xml .=$value;
                            $xml .= "</$key>\n";
                        }
                    }
                    $i++;

                    if ($sor['rendeles_azonosito'] != $query_rendeles->rows[$i]['rendeles_azonosito']){
                        $csokkent=true;
                    }
                    $xml .= "      </row>\n";

                }
                $xml .= "    </Document_Lines>\n";

                if ($csokkent) $i--;
                $xml .="  </BO>\n";
            }
            $xml .="</BOM>";

            $oOrders = "CSV_files/oOrders_".date("Ymd_His").".xml";
            if (file_put_contents($oOrders,$xml)){
                chmod($oOrders,777);
                $sql = "drop table if exists sapnak_rendeles_termek";
                $this->query->query($sql);
                $sql = "drop table if exists sapnak_rendeles_vevo";
                $this->query->query($sql);
                echo "Az XML file a vásárlásról elkészült ...";
            }
            else
                echo "Hiba történt, az XML file a vásárlásról nem készült el...";
        }
    }


    public function Termek_Vasarlas_Vevo() {


        $sql = "SELECT * FROM sapnak_rendeles_vevo_reg";
        $query_vevok= $this->query->query($sql);
        if ($query_vevok->num_rows > 0){

            $xml="<BOM>\n";

            foreach($query_vevok->rows as $value){

                $xml .="  <BO>\n";
                $xml .="    <AdmInfo>\n";
                $xml .="      <Object>2</Object>\n";
                $xml .="      <Version>2</Version>\n";
                $xml .="    </AdmInfo>\n";


                $xml .= "    <BusinessPartners>\n";
                $xml .= "      <row>\n";
                $xml .= "        <CardCode>".$value['vevokod']."</CardCode>\n";
                $xml .= "        <CardName>".$value['vevo_neve']."</CardName>\n";
                $xml .= "        <CardType>cCustomer</CardType>\n";
                $xml .= "        <GroupCode>".$value['vevo_csoport']."</GroupCode>\n";    // csoporkod vagy SAP-s csoporkod ???
                $xml .= "        <PayTermsGrpCode>".$value['fizetesi_mod']."</PayTermsGrpCode>\n";
                $xml .= "        <Phone1>".$value['telefonszam']."</Phone1>\n";
                $xml .= "        <EmailAddress>".$value['mail']."</EmailAddress>\n";
                $xml .= "        <Notes></Notes>\n";
                $xml .= "        <AdditionalID>".$value['adoszam']."</AdditionalID>\n";
                $xml .= "      </row>\n";
                $xml .= "    </BusinessPartners>\n";

                $xml .= "    <BPAddresses>\n";
                $xml .= "      <row>\n";
                $xml .= "        <AddressType>bo_ShipTo</AddressType>\n";
                $xml .= "        <AddressName>Szállítási cím</AddressName>\n";
                $xml .= "        <Street>".$value['szall_utca']."</Street>\n";
                $xml .= "        <ZipCode>".$value['szall_iranyitoszam']."</ZipCode>\n";
                $xml .= "        <City>".$value['szall_varos']."</City>\n";
                $xml .= "        <Country>".$value['szall_orszagkod']."</Country>\n";
                $xml .= "      </row>\n";
                $xml .= "      <row>\n";
                $xml .= "        <AddressType>bo_BillTo</AddressType>\n";
                $xml .= "        <AddressName>Számlázási cím</AddressName>\n";
                $xml .= "        <Street>".$value['szaml_utca']."</Street>\n";
                $xml .= "        <ZipCode>".$value['szaml_iranyitoszam']."</ZipCode>\n";
                $xml .= "        <City>".$value['szaml_varos']."</City>\n";
                $xml .= "        <Country>".$value['szaml_orszagkod']."</Country>\n";
                $xml .= "      </row>\n";
                $xml .= "    </BPAddresses>\n";
                $xml .="  </BO>\n";
            }
            $xml .="</BOM>";

            $oOrders = "CSV_files/oBusinessPartners_".date("Ymd_His").".xml";
            if (file_put_contents($oOrders,$xml)){
                chmod($oOrders,777);
                $sql = "drop table if exists sapnak_rendeles_vevo_reg";
                $this->query->query($sql);
                echo "Az XML file az új vevőkről elkészült ...<br>";
            } else {
                echo "Hiba történt, az XML file a vevőkről nem készült el...<br>";
            }
        }
    }

}
?>