<?php
class ModelTotalSubTotalProductShipping extends Model {
	public function getTotal(&$total_data, &$total, &$taxes,$kosar="cart") {
		$this->load->language('total/sub_total_product_shipping');
		
		$sub_total_product_shipping = $this->cart->getSubTotalProductShipping($kosar);
		$sub_total = $this->cart->getSubTotal($kosar);

		if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$sub_total_product_shipping += $voucher['amount'];
			}
		}
		
		$total_data[] = array( 
			'code'       => 'sub_total_product_shipping',
			'title'      => $this->language->get('text_sub_total_product_shipping'),
			'text'       => $this->currency->format($sub_total_product_shipping),
			'value'      => $sub_total_product_shipping,
			'value_product'      => $sub_total,
			'sort_order' => $this->config->get('sub_total_product_shipping_sort_order')
		);
		
		$total += $sub_total;
	}
}
?>