<?php echo $header; ?>
<style>
    <?php if($this->request->request['route'] == 'product/compare' && count($products) > 5) { ?>
        .sidebar-jobb-fix { display: none; }
    <?php } ?>
</style>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <h1><?php echo $heading_title; ?></h1>
  <?php if ($products) { ?>

    <?php  $vansuly = false;?>
    <?php  $vanmeret = false;?>
  <table class="compare-info">
    <thead>
      <tr>
        <td class="compare-product" colspan="<?php echo count($products) + 1; ?>"><?php echo $text_product; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><?php echo $text_name; ?></td>
        <?php foreach ($products as $product) { ?>
        <td class="name"><a href="<?php echo $products[$product['product_id']]['href']; ?>"><?php echo $products[$product['product_id']]['name']; ?></a></td>
          <?php
            $suly = str_replace("kg","",$products[$product['product_id']]['weight']."");
            if ((int)$suly) {
              $vansuly = true;
            }
            $meret_length = str_replace("x","",$products[$product['product_id']]['length']."");
            $meret_width = str_replace("x","",$products[$product['product_id']]['width']."");
            $meret_height = str_replace("x","",$products[$product['product_id']]['height']."");

          if ((int)$meret_length || (int)$meret_width || (int)$meret_height) {
              $vanmeret = true;
            }
          ?>

        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_image; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php if ($products[$product['product_id']]['thumb']) { ?>
          <img src="<?php echo $products[$product['product_id']]['thumb']; ?>" alt="<?php echo $products[$product['product_id']]['name']; ?>" />
          <?php } ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_price; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php if ($products[$product['product_id']]['price']) { ?>
          <?php if (!$products[$product['product_id']]['special']) { ?>
          <?php echo $products[$product['product_id']]['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $products[$product['product_id']]['price']; ?></span> <span class="price-new"><?php echo $products[$product['product_id']]['special']; ?></span>
          <?php } ?>
          <?php } ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_model; ?></td>
        <?php foreach ($products as $product) { ?>
            <?php if (isset($megjelenit_product['product_cikkszam_termeknevben']) && $megjelenit_product['product_cikkszam_termeknevben']) { ?>
                <td><?php echo $products[$product['product_id']]['cikkszam']; ?></td>
            <?php } else { ?>
                <td><?php echo $products[$product['product_id']]['model']; ?></td>
            <?php } ?>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_manufacturer; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php echo $products[$product['product_id']]['manufacturer']; ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_availability; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php echo $products[$product['product_id']]['availability']; ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_rating; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><img src="catalog/view/theme/default/image/stars-<?php echo $products[$product['product_id']]['rating']; ?>.png" alt="<?php echo $products[$product['product_id']]['reviews']; ?>" /><br />
          <?php echo $products[$product['product_id']]['reviews']; ?></td>
        <?php } ?>
      </tr>
      <!--<tr>
        <td><?php echo $text_summary; ?></td>
        <?php foreach ($products as $product) { ?>
        <td class="description"><?php echo $products[$product['product_id']]['description']; ?></td>
        <?php } ?>
      </tr>-->
      <?php if ($vansuly) { ?>
        <tr>
          <td><?php echo $text_weight; ?></td>
          <?php foreach ($products as $product) { ?>
          <td><?php echo $products[$product['product_id']]['weight']; ?></td>
          <?php } ?>
        </tr>
      <?php } ?>
      <?php if ($vanmeret) { ?>
        <tr>
          <td><?php echo $text_dimension; ?></td>
          <?php foreach ($products as $product) { ?>
          <td><?php echo $products[$product['product_id']]['length']; ?> x <?php echo $products[$product['product_id']]['width']; ?> x <?php echo $products[$product['product_id']]['height']; ?></td>
          <?php } ?>
        </tr>
      <?php } ?>

    </tbody>
    <?php foreach ($attribute_groups as $attribute_group) { ?>
        <?php if(!empty($attribute_group['attribute'])) { ?>
            <thead>
              <tr>
                <td class="compare-attribute" colspan="<?php echo count($products) + 1; ?>"><?php echo $attribute_group['name']; ?></td>
              </tr>
            </thead>
            <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                <?php if (count($products) > 1 ) { ?>
                    <?php $ertek = false; ?>
                    <?php $class = "class='equal'"?>
                    <?php foreach ($products as $product) { ?>
                        <?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>
                            <?php $ertek = str_replace(' ', '', $products[$product['product_id']]['attribute'][$key]); ?>
                            <?php break; ?>
                        <?php } ?>
                    <?php } ?>

                    <?php foreach ($products as $product) { ?>
                        <?php if (!isset($products[$product['product_id']]['attribute'][$key])  || $ertek != str_replace(' ', '', $products[$product['product_id']]['attribute'][$key])) { ?>
                            <?php $class = "class='not-equal'"?>
                            <?php break; ?>
                        <?php } ?>
                    <?php } ?>

                <?php } else { ?>
                    <?php $class = "class='not-equal'"?>
                <?php } ?>
                <tbody>
                    <tr>
                        <td <?php echo $class?>><?php echo $attribute['name']; ?></td>
                        <?php foreach ($products as $product) { ?>
                            <?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>
                                <td <?php echo $class?>><?php echo $products[$product['product_id']]['attribute'][$key]; ?></td>
                            <?php } else { ?>
                                <td></td>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                </tbody>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    <tr>
      <td></td>
      <?php foreach ($products as $product) { ?>
      <td>
        <?php if(!isset($_SESSION['ar_atszamol']) || (isset($_SESSION['ar_atszamol']) && !$_SESSION['ar_atszamol']) ) { ?>
            <?php if (!empty($product['elorendeles'])) { ?>
                <input type="button" class='button' style="position: relative; top: -4px; " value="<?php echo $button_arajanlat; ?>"
                        onclick="arajanlatotKerek('<?php echo $product['product_id']; ?>')"/>

            <?php } elseif (!empty($product['megrendelem']) ) { ?>
                <input type="button" value="<?php echo $button_megrendelem; ?>" class="button arajanlat"
                       onclick="megrendelem('<?php echo $product['product_id']; ?>')"/>

            <?php } else { ?>
                <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />
            <?php } ?>
        <?php } else { ?>
            <input type="button" class="kosar button elorendeles" value="<?php echo $button_set_valos_ar; ?>"
                   onclick="ujratolt(0)"/>
        <?php }  ?>
      </td>
      <?php } ?>
    </tr>
    <tr>
      <td></td>
      <?php foreach ($products as $product) { ?>
      <td class="remove"><a href="<?php echo $product['remove']; ?>" class="button"><?php echo $button_remove; ?></a></td>
      <?php } ?>
    </tr>
  </table>
  <!--<div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>-->
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <!--<div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>-->
  <?php } ?>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>