<?php
// Heading 
$_['heading_title']  = 'Check voucher';

// Text
$_['text_account']   = 'Account';
$_['text_vonalkod']  = 'Barcode';
$_['text_yes']  = 'Yes';
$_['text_no']  = 'No';
$_['text_success']   = 'Success: The voucher is valid.';
$_['text_success_bevaltva']   = 'Success: The voucher is redeemed.';

// Entry
$_['entry_vonalkod']   = 'Barcode:';
$_['entry_vonalkod_bevaltasa']    = 'Cashing barcode:';

// Error
$_['error_vonalkod'] = 'The bar code may consist of 13 characters!';

$_['error_felhasznalva'] = 'Invalid bar code has been used !';
$_['error_nemletezo'] = 'No existing barcode !';
$_['error_nem_engedelyezett'] = 'The voucher is not enabled yet !';
$_['error_nincs_bevaltva'] = 'The voucher has not been redeemed !';
$_['error_nemjogosult'] = 'Existing barcode! The check is not authorized to!';

?>