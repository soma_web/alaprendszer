<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
          <a onclick="$('form').attr('action', '<?php echo $delete; ?>'); $('form').submit();" class="button"><?php echo $button_delete; ?></a>

      </div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

                <td class="left"><?php echo $column_megnevezes; ?></a></td>
                <td class="left"><?php echo $column_class; ?></a></td>
                <td class="left"><?php echo $column_sort_order; ?></a></td>
                <td class="left"><?php echo $column_maximum; ?></a></td>
                <td class="left"><?php echo $column_ara; ?></a></td>
                <td class="left"><?php echo $column_status; ?></a></td>
                <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>

          <tbody>
            <?php if ($kiemeleseks) { ?>
                <?php foreach ($kiemeleseks as $kiemelesek) { ?>
                    <tr>

                        <td style="text-align: center;">
                            <?php if ($kiemelesek['selected']) { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $kiemelesek['kiemelesek_id']; ?>" checked="checked" />
                            <?php } else { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $kiemelesek['kiemelesek_id']; ?>" />
                            <?php } ?>
                        </td>

                        <td class="left"><?php echo $kiemelesek['megnevezes']; ?></td>
                        <td class="left"><?php echo $kiemelesek['class']; ?></td>
                        <td class="left"><?php echo $kiemelesek['sort_order']; ?></td>
                        <td class="left"><?php echo $kiemelesek['maximum']; ?></td>
                        <td class="left"><?php echo $kiemelesek['ara']; ?></td>
                        <td class="left"><?php echo $kiemelesek['statusza']; ?></td>
                        <td class="right"><a href="<?php echo $kiemelesek['action']['href']; ?>"><?php echo $kiemelesek['action']['text']; ?></a></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td class="center" colspan="3"><?php echo $text_no_results; ?></td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
    </div>
  </div>
</div>

<?php echo $footer; ?>