<?php
// Heading
$_['heading_title']       = 'Abandoned Cart Reminder';

// Tab
$_['tab_setting']         = 'Setting';
$_['tab_preview']         = 'Preview';
$_['tab_help']            = 'Help';

// Button
$_['button_preview']      = 'Preview';
$_['button_send']         = 'Send now';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Abandoned Cart Reminder!';
$_['text_coupon_fixed']   = 'Fixed Amount';
$_['text_coupon_percent'] = 'Percentage';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_preview_info']   = 'Important: Press Save (before Preview) if you changed settings, otherwise will use old setting. Preview function choose random an reminder (from reminders that need to be delivered).';

// Entry
$_['entry_secret_code']   = 'Secret code: <span class="help">at least 5 characters! ( a-z, A-Z, 0-9 ) <br />Required for Cron Job</span>';
$_['entry_delay']         = 'Delay (days): <span class="help">number of days after cart is considered inactive and can be sent an reminder</span>';
$_['entry_max_reminders'] = 'No. max reminders: <span class="help">maximum number of reminders that can be sent is customer don\'t read/react.<br /> Recommended max = 3 </span>';
$_['entry_use_html_email']   = 'Send Reminder with <a href="http://www.oc-extensions.com/HTML-Email">HTML Email Extension</a>:<span class="help"><br />If HTML Email Extension is not installed on your store then is used default html mail (like in old versions of this extensions)</span>';
$_['entry_log_admin']     = 'Send Log to admin:<span class="help">admin receive an email with list of customers informed about abandoned cart</span>';
$_['entry_add_coupon']    = 'Add coupon: <span class="help">add coupon to encourage customer to buy</span>';
$_['entry_coupon_type']   = 'Coupon type:';
$_['entry_coupon_amount'] = 'Coupon amount:<span class="help">if Percentage = X% <br />if fixed = X [your courency]</span>'; 
$_['entry_coupon_expire'] = 'Coupon expire in (days):';
$_['entry_reward_limit']  = 'How many times customer can be rewarded with coupons?<span class="help">leave blank = unlimited</span>';


// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Abandoned Cart Reminder!';
$_['error_secret_code']   = 'Secret code: at least 5 characters! ( a-z, A-Z, 0-9 )';
$_['error_delay']         = 'Delay - required';
$_['error_html_email_not_installed'] = 'Review Invitation Emails can\'t be sent with <a href="http://www.oc-extensions.com/HTML-Email">HTML Email Extension</a> because this extension is not available on your store. Please set option to Disabled!';
$_['error_max_reminders'] = 'Max reminder - required';
$_['error_coupon_amount'] = 'Coupon amount - required';
$_['error_coupon_expire'] = 'Coupon expire - required';
?>