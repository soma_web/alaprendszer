<div id="ar_atszamolo" class="price-calculator" >
    <?php echo $text_kedvezmeny?>: <input class="price-calculator-input" type="text" name="ar_atszamolo" size="6"
                         value="<?php echo isset($this->session->data['ar_atszamol']) ? $this->session->data['ar_atszamol'] : 0; ?>"
                         onchange="ujratolt(this.value)"> %
</div>
<script>
    function ujratolt(para) {
        $.ajax({
            url: 'index.php?route=common/home/arUjraszamol',
            type: 'post',
            dataType: 'html',
            data: 'ar_atszamol='+para,

            complete: function(json) {

            },
            success: function(json) {
                <?php
                if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI']) {
                    $pozicio_start = strpos($_SERVER['REQUEST_URI'],'&amp;warning',0);
                    if ($pozicio_start == false) {
                        $request_url = $_SERVER['REQUEST_URI'];

                    } else {
                        $pozicio_end = strpos($_SERVER['REQUEST_URI'],'&amp;',$pozicio_start+1);
                        $request_url = substr($_SERVER['REQUEST_URI'],0,$pozicio_start);
                        if($pozicio_end) {
                            $request_url .= substr($_SERVER['REQUEST_URI'],$pozicio_end);
                        }

                    }
                }

                $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
                $protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]),0,strpos($_SERVER["SERVER_PROTOCOL"],'/')).$s;

                $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
                $ujrahiv = $protocol."://".$_SERVER['SERVER_NAME'].$port.$request_url;
                $ujrahiv = str_replace('amp;','',$ujrahiv);
                $ujrahiv_darabolt = explode('&',$ujrahiv);
                $array_key = array_search('esemeny_kivalto=szurok_torlese',$ujrahiv_darabolt);
                if ($array_key != false) {
                    $ujrahiv_darabolt[$array_key] = "esemeny_kivalto=";
                }
                $ujrahiv = implode('&',$ujrahiv_darabolt);
                echo '';
                ?>

                locationPost("<?php  echo $ujrahiv?>");
            },
            error: function(e) {
            }
        });
    }


</script>
