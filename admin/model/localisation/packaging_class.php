<?php
class ModelLocalisationPackagingClass extends Model {
	public function addPackagingClass($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "packaging_class SET value = '" . (float)$data['value'] . "'");
		
		$packaging_class_id = $this->db->getLastId();
		
		foreach ($data['packaging_class_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "packaging_class_description SET packaging_class_id = '" . (int)$packaging_class_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', unit = '" . $this->db->escape($value['unit']) . "'");
		}
		
		$this->cache->delete('packaging_class');
	}
	
	public function editPackagingClass($packaging_class_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "packaging_class SET value = '" . (float)$data['value'] . "' WHERE packaging_class_id = '" . (int)$packaging_class_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "packaging_class_description WHERE packaging_class_id = '" . (int)$packaging_class_id . "'");

		foreach ($data['packaging_class_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "packaging_class_description SET packaging_class_id = '" . (int)$packaging_class_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', unit = '" . $this->db->escape($value['unit']) . "'");
		}
		
		$this->cache->delete('packaging_class');	
	}
	
	public function deletePackagingClass($packaging_class_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "packaging_class WHERE packaging_class_id = '" . (int)$packaging_class_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "packaging_class_description WHERE packaging_class_id = '" . (int)$packaging_class_id . "'");	
		
		$this->cache->delete('packaging_class');
	}
	
	public function getPackagingClasses($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "packaging_class wc LEFT JOIN " . DB_PREFIX . "packaging_class_description wcd ON (wc.packaging_class_id = wcd.packaging_class_id) WHERE wcd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
			$sort_data = array(
				'title',
				'unit',
				'value'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY title";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
			
			$query = $this->db->query($sql);
	
			return $query->rows;			
		} else {
			$packaging_class_data = $this->cache->get('packaging_class.' . (int)$this->config->get('config_language_id'));

			if (!$packaging_class_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "packaging_class wc LEFT JOIN " . DB_PREFIX . "packaging_class_description wcd ON (wc.packaging_class_id = wcd.packaging_class_id) WHERE wcd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
	
				$packaging_class_data = $query->rows;
			
				$this->cache->set('packaging_class.' . (int)$this->config->get('config_language_id'), $packaging_class_data);
			}
			
			return $packaging_class_data;
		}
	}
	
	public function getPackagingClass($packaging_class_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "packaging_class wc LEFT JOIN " . DB_PREFIX . "packaging_class_description wcd ON (wc.packaging_class_id = wcd.packaging_class_id) WHERE wc.packaging_class_id = '" . (int)$packaging_class_id . "' AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		return $query->row;
	}

	public function getPackagingClassDescriptionByUnit($unit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "packaging_class_description WHERE unit = '" . $this->db->escape($unit) . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		return $query->row;
	}
	
	public function getPackagingClassDescriptions($packaging_class_id) {
		$packaging_class_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "packaging_class_description WHERE packaging_class_id = '" . (int)$packaging_class_id . "'");
				
		foreach ($query->rows as $result) {
			$packaging_class_data[$result['language_id']] = array(
				'title' => $result['title'],
				'unit'  => $result['unit']
			);
		}
		
		return $packaging_class_data;
	}
			
	public function getTotalPackagingClasses() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "packaging_class"); 
		
		return $query->row['total'];
	}		
}
?>