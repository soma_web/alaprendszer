begin
  declare efogadva int(19) default 0;



  IF (NEW.sales_stage = "90% kiszámlázva" AND OLD.sales_stage != "90% kiszámlázva") THEN

       SELECT potentialid INTO efogadva FROM vtiger_potentialscf WHERE potentialid = NEW.potentialid AND (cf_2518 = "1" OR cf_2524 = "1" OR cf_2526 = "1");
       if efogadva > 0 THEN

          INSERT INTO vtiger_salesorder90 set
              potentialid = NEW.potentialid,
              sales_stage = NEW.sales_stage;

        END IF;
  END IF;

END