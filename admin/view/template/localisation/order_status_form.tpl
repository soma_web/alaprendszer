<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> <?php echo $entry_name; ?></td>
            <td><?php foreach ($languages as $language) { ?>
              <input type="text" name="order_status[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($order_status[$language['language_id']]) ? $order_status[$language['language_id']]['name'] : ''; ?>" />
              <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
              <?php if (isset($error_name[$language['language_id']])) { ?>
              <span class="error"><?php echo $error_name[$language['language_id']]; ?></span><br />
              <?php } ?>
              <?php } ?>
            </td>
          </tr>
          <tr>
              <td>Automatikus számlaküldés esetén: <span class="help">szamlazz.hu -> számla típusa</span></td>
              <td>
                  <select name="szamla_tipusa">
                      <option value="0" <?php echo empty($szamla_tipusa) ? 'selected' : ''?>>Nem állít ki számlát</option>
                      <option value="1" <?php echo !empty($szamla_tipusa) && $szamla_tipusa == 1 ? 'selected' : ''?>>Normál számla</option>
                      <option value="2" <?php echo !empty($szamla_tipusa) && $szamla_tipusa == 2 ? 'selected' : ''?>>Díjbekérő</option>
                      <option value="3" <?php echo !empty($szamla_tipusa) && $szamla_tipusa == 3 ? 'selected' : ''?>>Előlegszámla</option>
                  </select>
              </td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>