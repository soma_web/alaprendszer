<?php
class ControllerModuleSpecialOfferReminder extends Controller {
	private $error = array(); 
	
	public function install(){
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "special_offer_reminder` (
				  `reminder_id` int(11) NOT NULL AUTO_INCREMENT,
				  `email` varchar(255) NOT NULL,
				  `name` varchar(255) NOT NULL,
				  `customer_group_id` int(11) NOT NULL,
				  `product_id` int(11) NOT NULL,
				  `days_before` int(11) NOT NULL,
				  PRIMARY KEY (`reminder_id`)
				);";
		
		$this->db->query($sql);	
	}
	
	/*
	public function uninstall(){
		$this->db->query("DROP TABLE `" . DB_PREFIX . "special_offer_reminder`");
	}
	*/
	
	public function index() {   
		$this->load->language('module/special_offer_reminder');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('tool/image');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('special_offer_reminder', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_heading_title'] = $this->language->get('text_heading_title');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_only_image'] = $this->language->get('text_only_image');
		$this->data['text_image_box'] = $this->language->get('text_image_box');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		
		$this->data['entry_secret_code'] = $this->language->get('entry_secret_code');
		$this->data['entry_image'] = $this->language->get('entry_image');
	    $this->data['entry_dimension'] = $this->language->get('entry_dimension');
	    $this->data['entry_display_type'] = $this->language->get('entry_display_type');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = array();
		}
				
		if (isset($this->error['secret_code'])) {
			$this->data['error_secret_code'] = $this->error['secret_code'];
		} else {
			$this->data['error_secret_code'] = '';
		}

		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = '';
		}	

		if (isset($this->error['dimension'])) {
			$this->data['error_dimension'] = $this->error['dimension'];
		} else {
			$this->data['error_dimension'] = '';
		}	
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/special_offer_reminder', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/special_offer_reminder', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		$this->data['languages'] = $languages;
		
		foreach($languages as $language){
			$key = 'special_offer_reminder_heading_title_' . $language['language_id'];
			if (isset($this->request->post[$key])){
				$this->data['box_heading_title'][$language['language_id']] = $this->request->post[$key]; 
			} elseif ( $this->config->get($key) ){
				$this->data['box_heading_title'][$language['language_id']] = $this->config->get($key); 
			} else {
				$this->data['box_heading_title'][$language['language_id']] = ' ';
			}
		}
		
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100,100);
		
		if (isset($this->request->post['special_offer_reminder_image'])) {
			$this->data['special_offer_reminder_image'] = $this->request->post['special_offer_reminder_image'];
			$this->data['special_offer_reminder_thumb'] = $this->model_tool_image->resize($this->request->post['special_offer_reminder_image'], 100,100);
			
		} elseif ($this->config->get('special_offer_reminder_image')) { 
			$this->data['special_offer_reminder_image'] = $this->config->get('special_offer_reminder_image');
			$this->data['special_offer_reminder_thumb'] = $this->model_tool_image->resize($this->config->get('special_offer_reminder_image'), 100,100);
			
		} else {
			$this->data['special_offer_reminder_image'] = '';
			$this->data['special_offer_reminder_thumb'] = $this->model_tool_image->resize('no_image.jpg', 100,100);
		}
		
		if (isset($this->request->post['special_offer_reminder_secret_code'])) {
			$this->data['special_offer_reminder_secret_code'] = $this->request->post['special_offer_reminder_secret_code'];
		} elseif ($this->config->get('special_offer_reminder_secret_code')) { 
			$this->data['special_offer_reminder_secret_code'] = $this->config->get('special_offer_reminder_secret_code');
		} else {
			$this->data['special_offer_reminder_secret_code'] = '';
		}
		
		if (isset($this->request->post['special_offer_reminder_width'])) {
			$this->data['special_offer_reminder_width'] = $this->request->post['special_offer_reminder_width'];
		} elseif ($this->config->get('special_offer_reminder_width')) { 
			$this->data['special_offer_reminder_width'] = $this->config->get('special_offer_reminder_width');
		} else {
			$this->data['special_offer_reminder_width'] = '';
		}
		
		if (isset($this->request->post['special_offer_reminder_height'])) {
			$this->data['special_offer_reminder_height'] = $this->request->post['special_offer_reminder_height'];
		} elseif ($this->config->get('special_offer_reminder_height')) { 
			$this->data['special_offer_reminder_height'] = $this->config->get('special_offer_reminder_height');
		} else {
			$this->data['special_offer_reminder_height'] = '';
		}
		
		if (isset($this->request->post['special_offer_reminder_display_type'])) {
			$this->data['special_offer_reminder_display_type'] = $this->request->post['special_offer_reminder_display_type'];
		} elseif ($this->config->get('special_offer_reminder_display_type')) { 
			$this->data['special_offer_reminder_display_type'] = $this->config->get('special_offer_reminder_display_type');
		} else {
			$this->data['special_offer_reminder_display_type'] = '';
		}
		
		$this->data['modules'] = array();
		
		if (isset($this->request->post['special_offer_reminder_module'])) {
			$this->data['modules'] = $this->request->post['special_offer_reminder_module'];
		} elseif ($this->config->get('special_offer_reminder_module')) { 
			$this->data['modules'] = $this->config->get('special_offer_reminder_module');
		}	
				
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		
		$this->data['token'] = $this->session->data['token'];
				
		$this->template = 'module/special_offer_reminder.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/special_offer_reminder')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (utf8_strlen($this->request->post['special_offer_reminder_secret_code']) < 5) {
			$this->error['secret_code'] = $this->language->get('error_secret_code');
		}	

		if (utf8_strlen($this->request->post['special_offer_reminder_image']) < 1) {
			$this->error['image'] = $this->language->get('error_image');
		}	
		
		if (utf8_strlen($this->request->post['special_offer_reminder_width']) < 1 || !is_numeric($this->request->post['special_offer_reminder_width']) ) {
			$this->error['dimension'] = $this->language->get('error_dimension');
		}
		
		if (utf8_strlen($this->request->post['special_offer_reminder_height']) < 1 || !is_numeric($this->request->post['special_offer_reminder_height']) ) {
			$this->error['dimension'] = $this->language->get('error_dimension');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>