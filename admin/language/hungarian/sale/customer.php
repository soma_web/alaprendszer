<?php

// Heading
$_['heading_title']           = 'Vásárló';

// Text
$_['text_success']            = 'Siker! Ön módosította a vásárlót.';
$_['text_default']          = 'Alapértelmezett';
$_['text_approved']           = 'Jóváhagyta %s fiókot!';
$_['text_wait']             = 'Kérem várjon!';
$_['text_balance']          = 'Egyenleg:';
$_['text_add_blacklist']          = 'Felvétel feketelistára';

$_['text_nem']                = 'Nem:';
$_['text_eletkor']            = 'Életkor:';
$_['text_ferfi']              = 'Férfi:';
$_['text_holgy']              = 'Nő:';

$_['error_eletkor']           = 'Kérem válassza ki az életkorát';
$_['error_nem']                = 'Kérem válassza ki a nemét';

// Column
$_['column_name']             = 'Vásárló neve';
$_['column_email']            = 'E-mail';
$_['column_customer_group']   = 'Vásárlói csoport';
$_['column_status']           = 'Állapot';
$_['column_login']          = 'Bejelentkezés a boltba';
$_['column_approved']         = 'Jóváhagyva';
$_['column_date_added']       = 'Hozzáadás dátuma';
$_['column_description']    = 'Részletek';
$_['column_amount']         = 'Összeg';
$_['column_points']         = 'Pontok';
$_['column_ip']             = 'IP';
$_['column_total']          = 'Összes fiók';
$_['column_action']         = 'Művelet';
$_['column_atkuldve']       = 'SAP részére';
$_['column_partner_kod']    = 'Partner kód';
$_['column_company']        = 'Cég neve';
$_['column_szazalek']       = 'Kedvezmény (%)';
$_['column_fizetesi_mod']   = 'Egyedi fizetési mód';

//integráció
$_['atkuldesre_var']        ='Átküldésre vár';
$_['importalt']             ='SAP-ból integrált';
$_['atkuldve']              ='Átküldve';
$_['ellenorzott']           ='Leellenőrizve';
$_['column_payemail']          = 'Paypal Email';


// Entry
$_['entry_adoszam']             = 'Adószám';
$_['entry_sap_kod']             = 'SAP azonosító kód';
$_['entry_fizetes_torles']      = 'Törlés';
$_['entry_fizetes_modositas']   = 'Módosítás';
$_['entry_fizetes_engedely']    = 'Engedélyezve';
$_['button_uj_fizetes']         = 'Új fizetési mód';
$_['entry_fizetes']             = 'Egyedi fizetési mód:';
$_['entry_firstname']           = 'Vezetéknév:';
$_['entry_lastname']            = 'Keresztnév:';
$_['entry_email']               = 'E-mail:';
$_['entry_telephone']           = 'Telefon:';
$_['entry_fax']                 = 'Fax:';
$_['entry_newsletter']          = 'Hírlevél:';
$_['entry_customer_group']      = 'Vásárlói csoport';
$_['entry_status']              = 'Állapot:';
$_['entry_password']            = 'Jelszó:';
$_['entry_confirm']             = 'Megerősítés:';
$_['entry_company']             = 'Cég:';
$_['entry_adoszam']             = 'Adószám:';
$_['entry_address_1']           = 'Cím 1:';
$_['entry_address_2']           = 'Cím 2:';
$_['entry_city']                = 'Város:';
$_['entry_postcode']            = 'Irányítószám:';
$_['entry_country']             = 'Ország:';
$_['entry_zone']                = 'Megye:';
$_['entry_default']             = 'Alapértelmezett cím:';
$_['entry_amount']              = 'Összeg:';
$_['entry_points']              = 'Pontok:';
$_['entry_description']         = 'Megjegyzés:';
$_['entry_szazalek']            = 'Százalákos kedvezmény';
$_['column_login']              = 'Belépés a felhasználóval';
$_['entry_nem']                 = 'Nem';
$_['entry_eletkor']             = 'Életkor';
$_['select_ferfi']              = 'Férfi';
$_['select_no']                 = 'Nő';
$_['entry_iskolai_vegzettseg']  = 'Iskolai végzettség';
$_['entry_vallalkozasi_forma']  = 'Vállalkozási forma:';
$_['entry_szekhely']            = 'Székhely:';
$_['entry_ugyvezeto_neve']      = 'Ügyvezető neve:';
$_['entry_ugyvezeto_telefonszama']  = 'Ügyvezető telefonszáma:';
$_['entry_partnerkod']          = 'Integrációs partnerkód:';
$_['entry_weblap']              = 'Weblap címe:';



// Error
$_['error_warning']         = 'Figyelem: Kérem gondosan ellenőrizze át az űrlapot!';
$_['error_permission']        = 'Figyelmeztetés! Nincs engedélye a vásárló módosítására.';
$_['error_exists']          = 'Figyelem: Ez az e-mail cím már regisztrálva van!';
$_['error_firstname']         = 'A keresztnév hossza 1 és 32 karakter közé essen!';
$_['error_lastname']          = 'A családnév hossza 1 és 32 karakter közé essen!';
$_['error_email']             = 'Az e-mail cím nem t?nik valósnak!';
$_['error_telephone']         = 'A telefonszám hossza 3 és 32 karakter közé essen!';
$_['error_password']          = 'A jelszó hossza 3 és 20 karakter közé essen!';
$_['error_confirm']           = 'A jelszó és megerősítése nem egyezik!';
$_['error_address_1']         = 'A cím hossza 3 és 128 karakter közé essen!';
$_['error_city']              = 'A város hossza 3 és 128 karakter közé essen!';
$_['error_postcode']        = 'Az irányítószám legalább 2 és legfeljebb 10 karakterből állhat!';
$_['error_country']           = 'Kérem válasszon országot!';
$_['error_zone']              = 'Kérem, válasszon megyét!';
$_['button_sap']              = 'Küldés SAP-nak';
$_['button_csv_export']       = 'CSV export';

$_['text_rendeles_datum']     = 'Dátum (tól-ig):';
$_['text_letoltes']           = 'Letöltés';


$_['entry_feltolto']          = 'Ügyfél jelleg:';
$_['select_feltolto']         = 'Partner / Értékesítő';
$_['select_altalanos']        = 'Magánszemély';

$_['entry_newsletter_categories'] = 'Hirlevél kategóriák';
$_['success_modositas']  = 'Sikerült! A módosítás megtörtént.';
$_['error_sql']  = 'Figyelem: Hiba történt az adatbázis módosítása során!';
$_['error_post']  = 'Figyelem: Hiba történt az adatátvitel során!';

?>
