<?php
class ModelCronWebshopMegrendelesToCrm extends Model {

	public function getOrders() {

        $sql = "SELECT * FROM ".DB_PREFIX."order WHERE order_status_id=1 AND (to_crm <> 1 OR to_crm IS NULL) ";
        $query = $this->db->query($sql);


        foreach($query->rows as $key=>$value) {
            $sql = "SELECT `code`, `value` FROM ".DB_PREFIX."order_total WHERE order_id='".$value['order_id']."'";
            $query_total = $this->db->query($sql);

            foreach($query_total->rows as $value_total) {
                $query->rows[$key][$value_total['code']] =  $value_total['value'];
            }


        }

        return $query->rows;

	}

    public function getOrderProducts($order_id) {
        $vissza = array();
        $sql = "SELECT * FROM ".DB_PREFIX."order_product WHERE order_id='".$order_id."'";
        $query = $this->db->query($sql);
        echo "Order products";
        print_r($query->rows);
        echo "<br>";


        foreach($query->rows as $value) {
            $sql = "SELECT crm_tipus, crm_category_id, crm_product_id FROM " . DB_PREFIX . "product WHERE product_id='" . $value['product_id'] . "'";
            $query_product = $this->db->query($sql);
            if (!empty($query_product->row['crm_tipus']) && !empty($query_product->row['crm_product_id'])) {
                $value['crm_tipus'] = $query_product->row['crm_tipus'];
                $value['crm_category_id'] = !empty($query_product->row['crm_category_id']) ? $query_product->row['crm_category_id'] : '';
                $value['crm_product_id'] = $query_product->row['crm_product_id'];
                $vissza[] = $value;
            }

        }
        print_r($vissza);

        return $vissza;

	}

    public function getOrderFizetes($payment_code) {
        $vissza = array();

        if ($payment_code == "bank_transfer") {
            $vissza['fizetesi_mod'] = $this->config->get('bank_transfer_crm_fizetesi_mod');
            $vissza['fizetesi_hatarido'] = $this->config->get('bank_transfer_crm_fizetesi_hatarido');
        }

        return $vissza;
    }

    public function writeCrm($data=array()) {


        if (count($this->db_crm)) {
            echo "<br>Van CRM kapcsolat";
            $contact_id = $this->getContactId($data['order_id']);
            echo "<br>contact_id: ".$contact_id;

            $accountid = $this->getAccountId($data);

            if ($contact_id) {
                $this->load->model("cron/altalanos");
                $crm_id = $this->model_cron_altalanos->getCrmId();
                echo "<br>crm_id: ".$crm_id;

                $modtrack_id = $this->model_cron_altalanos->getModtrackerId();
                echo "<br>modtrack_id: ".$modtrack_id;

                $salesno = $this->model_cron_altalanos->getCrmNo('SalesOrder');
                echo "<br>salesno: ".$salesno;

                $sql = "INSERT INTO vtiger_salesorder SET
                            salesorderid    = '".$crm_id."',
                            subject         = 'Webshop - megrendelés',
                            potentialid     = '0',
                            customerno      = '',
                            salesorder_no   = '".$salesno."',
                            quoteid         = '0',
                            duedate         = CURDATE() + INTERVAL ".(!empty($data['fizetes']['fizetesi_hatarido']) ? $data['fizetes']['fizetesi_hatarido'] : 4)." DAY,
                            vendorterms     = '',
                            contactid       = $contact_id,
                            vendorid        = '',
                            carrier         = '',
                            pending         = '',
                            `type`          = '',
                            adjustment      = '',
                            salescommission = '',
                            exciseduty      = '',
                            total           = '".$data['total']."',
                            subtotal        = '".$data['sub_total']."',
                            taxtype         = 'group',
                            discount_percent= '0',
                            discount_amount = '0',
                            s_h_amount      = '0',
                            accountid       = '".$accountid."',
                            terms_conditions= '".$data['comment']."',
                            purchaseorder   = '',
                            sostatus        = 'Díjbekérő',
                            currency_id     = '1',
                            conversion_rate = '1',
                            enable_recurring= '0',
                            pre_tax_total   = '".$data['sub_total']."',
                            s_h_percent     = '".(round($data['tax']/$data['sub_total'],2)*100)."'";

                $siker = $this->db_crm->query($sql);
                echo 'vtiger_salesorder: ' .$siker;




                $sql = "INSERT INTO vtiger_salesordercf SET
                            salesorderid    = '".$crm_id."',
                            cf_2435         = 'Továbbításra vár',
                            cf_2437         = '".(!empty($data['fizetes']['fizetesi_mod']) ? $data['fizetes']['fizetesi_mod'] : '')."'";
                $siker = $this->db_crm->query($sql);

                if ($this->config->get($data['payment_code'].'_crm_fizetesi_mod') != 'Kiegyenlített') {
                    $sql = "UPDATE vtiger_salesorder SET sostatus = 'Created' WHERE salesorderid = '" . $crm_id . "'";
                    $siker = $this->db_crm->query($sql);
                    $sql = "UPDATE vtiger_salesorder SET sostatus = 'Díjbekérő' WHERE salesorderid = '" . $crm_id . "'";
                    $siker = $this->db_crm->query($sql);

                } else {
                    $sql = "UPDATE vtiger_salesordercf SET
                            cf_2437 = 'Létrehozva',
                            cf_2435 = 'Továbbítva'
                          WHERE salesorderid = '" . $crm_id . "'";
                    $siker = $this->db_crm->query($sql);
                    echo 'Létrehozva';

                    $sql = "UPDATE vtiger_salesordercf SET
                            cf_2437 = 'Kiegyenlített'
                          WHERE salesorderid = '" . $crm_id . "'";
                    $siker = $this->db_crm->query($sql);
                    echo 'Kiegyenlített';

                }

                echo 'vtiger_salesordercf: ' .$siker;

                $sql = "INSERT INTO vtiger_crmentity SET
                            crmid           = '".$crm_id."',
	                        smcreatorid     = '20',
                            smownerid       = '20',
                            modifiedby      = '20',
                            setype          = 'SalesOrder',
                            description	    = '',
                            createdtime	    = NOW(),
                            modifiedtime    = NOW(),
                            viewedtime      = '',
                            status          = '',
                            version         = '0',
                            presence        = '1',
                            deleted         = '0',
                            label           = 'Webshop - megrendelés',
                            cf_view         = '',
                            cf_viewname	    = ''";
                $siker = $this->db_crm->query($sql);
                echo 'vtiger_crmentity: ' .$siker;

                $sql = "INSERT INTO vtiger_inventoryshippingrel SET
                          id        = '".$crm_id."',
                          shtax1    = '',
                          shtax2    = '',
                          shtax3    = ''";
                $siker = $this->db_crm->query($sql);
                echo 'vtiger_inventoryshippingrel: ' .$siker;

                $sql = "INSERT INTO vtiger_modtracker_basic SET
                            id          = '".$modtrack_id."',
                            crmid       = '".$crm_id."',
                            `module`    = 'SalesOrder',
                            whodid      = '20',
                            changedon   = NOW(),
                            status      = '2'";
                $siker = $this->db_crm->query($sql);
                echo 'vtiger_modtracker_basic: ' .$siker;

                $sql = "INSERT INTO vtiger_sobillads SET
                          sobilladdressid   = '".$crm_id."',
                          bill_city         = '".$data['payment_city']."',
                          bill_code         = '".$data['payment_postcode']."',
                          bill_country      = '".$data['payment_country']."',
                          bill_state        = '',
                          bill_street       = '".$data['payment_address_1']."',
                          bill_pobox        = ''";
                $siker = $this->db_crm->query($sql);
                echo 'vtiger_sobillads: ' .$siker;


                $sql = "INSERT INTO vtiger_soshipads SET
                          soshipaddressid   = '".$crm_id."',
                          ship_city         = '".$data['shipping_city']."',
                          ship_code         = '".$data['shipping_postcode']."',
                          ship_country      = '".$data['shipping_country']."',
                          ship_state        = '',
                          ship_street       = '".$data['shipping_address_1']."',
                          ship_pobox        = ''";
                $siker = $this->db_crm->query($sql);
                echo 'vtiger_sobillads: ' .$siker;


                if (!empty($data['product'])) {
                    foreach($data['product'] as $key=>$product) {
                        $lineitem_id = $this->model_cron_altalanos->getLineItemId();
                        $sql = "INSERT INTO vtiger_inventoryproductrel SET
                            id                = '".$crm_id."',
                            productid         = '".$product['crm_product_id']."',
                            sequence_no       = '".($key+1)."',
                            quantity          = '".$product['quantity']."',
                            listprice         = '".$product['price']."',
                            discount_percent  = '',
                            discount_amount   = '',
                            comment           = '',
                            description	      = '',
                            incrementondel    = '0',
                            lineitem_id       = '".$lineitem_id."',
                            tax1              = '',
                            tax2              = '',
                            tax3              = '',
                            tax4              = '',
                            tax5              = '".(round($product['tax']/$product['price'],2)*100)."'";
                        $siker = $this->db_crm->query($sql);
                        echo 'vtiger_inventoryproductrel: ' .$siker;
                    }
                }
            }
        } else {
            echo "Nincs CRM kapcsolat";

        }

        $sql  = "UPDATE ".DB_PREFIX."order SET to_crm    = 1 WHERE order_id = '".$data['order_id']. "'";
        $siker = $this->db->query($sql);

        return true;
    }

    public function getContactId($order_id) {
        $sql = "SELECT contactid FROM vtiger_contactscf WHERE cf_1510='".$order_id."'";
        $query = $this->db_crm->query($sql);

        return !empty($query->row['contactid']) ? $query->row['contactid'] : false;
    }

    public function getAccountId($data) {

        $accountid = 0;
        if (empty($data['payment_company']) && empty($data['payment_adoszam'])) {
            return $accountid;
        }
        $adoszam    = $data['payment_adoszam'];
        $ceg        = $data['payment_company'];




        if (!empty($adoszam)) {
            $sql = "SELECT a.accountid, a.account_no FROM vtiger_account a ";
            $sql .= " INNER JOIN vtiger_crmentity c ON (c.crmid=a.accountid) ";

            $sql .= " LEFT JOIN vtiger_accountscf acf ON (acf.accountid=a.accountid) ";
            $sql .= " WHERE acf.cf_843 LIKE '" . $adoszam . "'";
            $sql .= " AND c.deleted = 0";
            $query = $this->db_crm->query($sql);
            if ($query->row) {
                $accountid = $query->row['accountid'];
            }
        }

        if (!empty($ceg) && !$accountid) {
            $sql = "SELECT a.accountid, a.account_no FROM vtiger_account a ";
            $sql .= " INNER JOIN vtiger_crmentity c ON (c.crmid=a.accountid) ";

            $sql .= " WHERE a.accountname LIKE '" . $ceg . "'";
            $sql .= " AND c.deleted = 0";
            $query = $this->db_crm->query($sql);
            if ($query->row) {
                $accountid = $query->row['accountid'];
            } else {
                $accountid = $this->addNewAccount($data);
            }
        }

        return $accountid;
    }

    public function addNewAccount($data) {

        $this->load->model("cron/altalanos");
        $accountid = $this->model_cron_altalanos->getCrmId();
        $account_no = $this->model_cron_altalanos->getCrmNo('Accounts');

        $sql = "INSERT INTO vtiger_account SET
                    accountid           = '" . $accountid . "',
                    account_no          = '".$account_no."',
                    accountname         = '".$data['payment_company']."',
                    parentid            = '".(!empty($data['parentid']) ? $data['parentid'] : 0)."',
                    account_type        = '".(!empty($data['account_type']) ? $data['account_type'] : '')."',
                    industry            = '".(!empty($data['industry']) ? $data['industry'] : '')."',
                    annualrevenue       = '".(!empty($data['annualrevenue']) ? $data['annualrevenue'] : 0)."',
                    rating              = '".(!empty($data['rating']) ? $data['rating'] : '')."',
                    ownership           = '".(!empty($data['ownership']) ? $data['ownership'] : '')."',
                    siccode             = '".(!empty($data['siccode']) ? $data['siccode'] : '')."',
                    tickersymbol        = '".(!empty($data['tickersymbol']) ? $data['tickersymbol'] : '')."',

                    phone               = '".(!empty($data['telephone']) ? $data['telephone'] : '')."',
                    otherphone          = '".(!empty($data['otherphone']) ? $data['otherphone'] : '')."',
                    email1              = '".(!empty($data['email']) ? $data['email'] : '')."',
                    email2              = '".(!empty($data['email2']) ? $data['email2'] : '')."',
                    website             = '".(!empty($data['website']) ? $data['website'] : '')."',
                    fax                 = '".(!empty($data['fax']) ? $data['fax'] : '')."',

                    employees           = '".(!empty($data['employees']) ? $data['employees'] : 0)."',
                    emailoptout         = '".(!empty($data['emailoptout']) ? $data['emailoptout'] : 0)."',
                    notify_owner        = '".(!empty($data['notify_owner']) ? $data['notify_owner'] : 0)."',
                    isconvertedfromlead	= '".(!empty($data['isconvertedfromlead']) ? $data['isconvertedfromlead'] : 0)."'";
        $siker = $this->db_crm->query($sql);

        $sql = "INSERT INTO vtiger_accountscf SET
                    accountid   = '" . $accountid . "',
                    cf_843      = '".$data['payment_adoszam']."',
                    cf_985      = '".(!empty($data['firstname']) ? $data['firstname'] : '')."',
                    cf_987      = '".(!empty($data['lastname']) ? $data['lastname'] : '')."'";
        $siker = $this->db_crm->query($sql);


        $sql = "INSERT INTO vtiger_accountbillads SET
                    accountaddressid= '".$accountid."',
                    bill_city       = '".(!empty($data['payment_city'])    ? $data['payment_city'] : '')."',
                    bill_code       = '".(!empty($data['payment_postcode'])    ? $data['payment_postcode'] : '')."',
                    bill_country    = '".(!empty($data['payment_country']) ? $data['payment_country'] : '')."',
                    bill_state      = '".(!empty($data['payment_zone'])   ? $data['payment_zone'] : '')."',
                    bill_street     = '".(!empty($data['payment_address_1'])  ? $data['payment_address_1'] : '')."',
                    bill_pobox      = '".(!empty($data['bill_pobox'])   ? $data['bill_pobox'] : '')."'";
        $siker = $this->db_crm->query($sql);

        $sql = "INSERT INTO vtiger_accountshipads SET
                    accountaddressid= '".$accountid."',
                    ship_city       = '".(!empty($data['shipping_city'])    ? $data['shipping_city'] : '')."',
                    ship_code       = '".(!empty($data['shipping_postcode'])    ? $data['shipping_postcode'] : '')."',
                    ship_country    = '".(!empty($data['shipping_country']) ? $data['shipping_country'] : '')."',
                    ship_state      = '".(!empty($data['shipping_zone'])   ? $data['shipping_zone'] : '')."',
                    ship_street     = '".(!empty($data['shipping_address_1'])  ? $data['shipping_address_1'] : '')."',
                    ship_pobox      = '".(!empty($data['ship_pobox'])   ? $data['ship_pobox'] : '')."'";
        $siker = $this->db_crm->query($sql);


        $data['crmid']      = $accountid;
        $data['module']     = 'Accounts';

        $data['crmentity']  = array(
            'smcreatorid'   => 20,
            'smownerid'     => 20,
            'modifiedby'    => 20,
            'setype'        => 'Accounts',
            'description'   => '',
            'viewedtime'    => '',
            'status'        => '',
            'version'       => 0,
            'presence'      => 1,
            'deleted'       => 0,
            'label'         => (!empty($data['payment_company']) ? $data['payment_company'] : trim($data['firstname']).' '.trim($data['lastname'])),
            'cf_view'       => '',
            'cf_viewname'   => ''
        );
        $data['modbasic']  = array(

            'whodid'        => 20,
            'status'        => 2
        );

        $this->model_cron_altalanos->addEntity($data);

        return $accountid;
    }
}
?>