<?php   
class ControllerCronLehetosegekToEngedelyeztetes extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;

        $this->load->model("cron/altalanos");
        $this->load->model('cron/lehetosegek_to_engedelyeztetes');

        echo 'Elindítva<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $potentials = $this->model_cron_lehetosegek_to_engedelyeztetes->getPotentials();

        echo '<br><br>Vevőjelöltek összesen: '.count($potentials).' db';
        $this->model_cron_altalanos->addFuttatasEredmeny('Engedélyeztetés összesen: '.count($potentials).' db');


        if ($potentials) {

            foreach($potentials as $key=>$value) {
                $siker = true;

                $potential = $this->model_cron_lehetosegek_to_engedelyeztetes->getPotential($value['potentialid']);
                if ($potential) {
                    $potential['product'] = $this->model_cron_lehetosegek_to_engedelyeztetes->getProduct('393300');
                    if ($potential['product']) {
                        $siker = $this->model_cron_lehetosegek_to_engedelyeztetes->addPotential($potential);
                    }
                }
                if ($siker) {
                    $this->model_cron_lehetosegek_to_engedelyeztetes->deleteTriggerPotential($value);
                }
            }
        }


        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}

