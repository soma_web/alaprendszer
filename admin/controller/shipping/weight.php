<?php
class ControllerShippingWeight extends Controller { 
	private $error = array();
	
	public function index() {  
		$this->load->language('shipping/weight');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!isset($this->request->post['weight_biztositas'])){
                $this->request->post['weight_biztositas'] = 0;
            } else {
                $this->request->post['weight_biztositas'] = 1;
            }

            if (!isset($this->request->post['gls_csomagpont'])){
                $this->request->post['gls_csomagpont'] = 0;
            } else {
                $this->request->post['gls_csomagpont'] = 1;
            }

			$this->model_setting_setting->editSetting('weight', $this->request->post);	

			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}

        $this->data['megjelenit_gls'] = $this->config->get('megjelenit_gls');

		$this->data['heading_title']        = $this->language->get('heading_title');

		$this->data['text_none']            = $this->language->get('text_none');
		$this->data['text_enabled']         = $this->language->get('text_enabled');
		$this->data['text_disabled']        = $this->language->get('text_disabled');
		
		$this->data['entry_rate']           = $this->language->get('entry_rate');
		$this->data['entry_tax_class']      = $this->language->get('entry_tax_class');
		$this->data['entry_status']         = $this->language->get('entry_status');
		$this->data['entry_sort_order']     = $this->language->get('entry_sort_order');
		
		$this->data['button_save']          = $this->language->get('button_save');
		$this->data['button_cancel']        = $this->language->get('button_cancel');

		$this->data['tab_general']          = $this->language->get('tab_general');
		$this->data['text_biztositas']      = $this->language->get('text_biztositas');
		$this->data['text_gls_csomagpont']  = $this->language->get('text_gls_csomagpont');

        $this->data['entry_title_heading']  = $this->language->get('entry_title_heading');
        $this->data['entry_title_text']     = $this->language->get('entry_title_text');
		$this->data['entry_mail']           = $this->language->get('entry_mail');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/weight', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('shipping/weight', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'); 

		$this->load->model('localisation/geo_zone');
		
		$geo_zones = $this->model_localisation_geo_zone->getGeoZones();
		
		foreach ($geo_zones as $geo_zone) {
			if (isset($this->request->post['weight_' . $geo_zone['geo_zone_id'] . '_rate'])) {
				$this->data['weight_' . $geo_zone['geo_zone_id'] . '_rate'] = $this->request->post['weight_' . $geo_zone['geo_zone_id'] . '_rate'];
			} else {
				$this->data['weight_' . $geo_zone['geo_zone_id'] . '_rate'] = $this->config->get('weight_' . $geo_zone['geo_zone_id'] . '_rate');
			}		
			
			if (isset($this->request->post['weight_' . $geo_zone['geo_zone_id'] . '_status'])) {
				$this->data['weight_' . $geo_zone['geo_zone_id'] . '_status'] = $this->request->post['weight_' . $geo_zone['geo_zone_id'] . '_status'];
			} else {
				$this->data['weight_' . $geo_zone['geo_zone_id'] . '_status'] = $this->config->get('weight_' . $geo_zone['geo_zone_id'] . '_status');
			}		
		}
		
		$this->data['geo_zones'] = $geo_zones;

		if (isset($this->request->post['weight_tax_class_id'])) {
			$this->data['weight_tax_class_id'] = $this->request->post['weight_tax_class_id'];
		} else {
			$this->data['weight_tax_class_id'] = $this->config->get('weight_tax_class_id');
		}
		
		if (isset($this->request->post['weight_status'])) {
			$this->data['weight_status'] = $this->request->post['weight_status'];
		} else {
			$this->data['weight_status'] = $this->config->get('weight_status');
		}

        if (isset($this->request->post['gls_csomagpont'])) {
			$this->data['gls_csomagpont'] = $this->request->post['gls_csomagpont'];
		} else {
			$this->data['gls_csomagpont'] = $this->config->get('gls_csomagpont');
		}
		
		if (isset($this->request->post['weight_sort_order'])) {
			$this->data['weight_sort_order'] = $this->request->post['weight_sort_order'];
		} else {
			$this->data['weight_sort_order'] = $this->config->get('weight_sort_order');
		}

        if (isset($this->request->post['weight_biztositas'])) {
            $this->data['weight_biztositas'] = $this->request->post['weight_biztositas'];
        } else {
            $this->data['weight_biztositas'] = $this->config->get('weight_biztositas');
        }

        if (isset($this->request->post['weight_header'])) {
            $this->data['weight_header'] = $this->request->post['weight_header'];
        } else {
            $this->data['weight_header'] = $this->config->get('weight_header');
        }

        if (isset($this->request->post['weight_text'])) {
            $this->data['weight_text'] = $this->request->post['weight_text'];
        } else {
            $this->data['weight_text'] = $this->config->get('weight_text');
        }

		if (isset($this->request->post['weight_mail'])) {
			$this->data['weight_mail'] = $this->request->post['weight_mail'];
		} else {
			$this->data['weight_mail'] = $this->config->get('weight_mail');
		}

		$this->load->model('localisation/tax_class');
				
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getInstalled('payment');
        $no_module = $this->model_setting_extension->getNoModule();


        foreach ($extensions as $key => $value) {
            if (!file_exists(DIR_APPLICATION . 'controller/payment/' . $value . '.php')) {
                $this->model_setting_extension->uninstall('payment', $value);

                unset($extensions[$key]);
            } elseif (in_array($value, $no_module)){
                unset($extensions[$key]);
            } else {
                $this->load->language('payment/' . $value);

                if (isset($this->request->post['weight_payment_'.$value])) {
                    $this->data['weight_payment_'.$value] = $this->request->post['weight_payment_'.$value];
                } elseif ( $this->config->get('weight_payment_'.$value)) {
                    $this->data['weight_payment_'.$value] = $this->config->get('weight_payment_'.$value);
                } else {
                    $this->data['weight_payment_'.$value] = 0;
                }

                $this->data['payment']['payment_' . $value] = array(
                    'name'      => $this->language->get('heading_title'),
                    'valasztva' => $this->data['weight_payment_'.$value]
                );


            }
        }


		$this->template = 'shipping/weight.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
		
	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/weight')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>