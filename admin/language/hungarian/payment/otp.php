<?php

// Heading
$_['heading_title']      = 'OTP';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Sikeresen módosította az OTP modul részleteit!';
$_['text_otp'] 			= '<img src="view/image/otp_simple_logo.jpg" alt="OTP Bankkártyás fizetés" title="OTP Bankkártyás fizetés" style="border: 1px solid #EEEEEE; width: 30px" />';

// Entry
$_['entry_privkey']      = 'OTP kulcs értéke:<span class="help">pl.: #02299991</span>';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';
$_['entry_haromszereplos']      = 'A vonatkozó haromszereplosshop.conf file neve:<span class="help">pl.: haromszereplos_hadrianus.conf</span>';
$_['entry_otp_webshop_client']  = 'A vonatkozó otp_webshop_client.conf file neve:<span class="help">pl.: otp_webshop_client_hadrianus.conf</span>';
$_['entry_total']        = 'Min. összeg:<br /><span class="help">A pénztárban a rendelés értékének el kell érnie ezt az összeget, hogy aktívvá váljon!</span>';
$_['entry_crm_fizetesi_mod']  = 'CRM : fizetési mód:';
$_['entry_crm_fizetesi_hatarido']  = 'CRM : fizetési határidő (nap):';
// Error
$_['error_permission']   = 'Figyelmeztetés: Az OTP fizetés módosítása az Ön számára nem engedélyezett';
?>
