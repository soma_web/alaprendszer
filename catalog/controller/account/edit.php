<?php
class ControllerAccountEdit extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/edit', '', 'SSL');

			$this->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->language->load('account/edit');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('account/customer');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_account_customer->editCustomer($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('account/account', '', 'SSL'));
		}

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),     	
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),        	
        	'separator' => $this->language->get('text_separator')
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_edit'),
			'href'      => $this->url->link('account/edit', '', 'SSL'),       	
        	'separator' => $this->language->get('text_separator')
      	);
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_your_details'] = $this->language->get('text_your_details');

		$this->data['entry_adoszam'] = $this->language->get('entry_adoszam');
		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_telephone'] = $this->language->get('entry_telephone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_adoszam'] = $this->language->get('entry_adoszam');
        $this->data['entry_paypalemail'] = $this->language->get('entry_paypalemail');
        $this->data['entry_weblap'] = $this->language->get('entry_weblap');

        $this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_back'] = $this->language->get('button_back');

        $this->data['entry_nem'] = $this->language->get('entry_nem');
        $this->data['entry_eletkor'] = $this->language->get('entry_eletkor');
        $this->data['entry_iskolai_vegzettseg'] = $this->language->get('entry_iskolai_vegzettseg');
        $this->data['text_select'] = $this->language->get('text_select');
        $this->data['entry_vallalkozasi_forma'] = $this->language->get('entry_vallalkozasi_forma');
        $this->data['entry_szekhely'] = $this->language->get('entry_szekhely');
        $this->data['entry_ugyvezeto_neve'] = $this->language->get('entry_ugyvezeto_neve');
        $this->data['entry_ugyvezeto_telefonszama'] = $this->language->get('entry_ugyvezeto_telefonszama');
        $this->data['text_your_cegadatok'] = $this->language->get('text_your_cegadatok');
        $this->data['text_paypaltitle'] = $this->language->get('text_paypaltitle');
        $this->data['entry_company'] = $this->language->get('entry_company');
        $this->data['text_address']                     = $this->language->get('text_address');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['firstname'])) {
			$this->data['error_firstname'] = $this->error['firstname'];
		} else {
			$this->data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$this->data['error_lastname'] = $this->error['lastname'];
		} else {
			$this->data['error_lastname'] = '';
		}
		
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}	
		
		if (isset($this->error['telephone'])) {
			$this->data['error_telephone'] = $this->error['telephone'];
		} else {
			$this->data['error_telephone'] = '';
		}

        if (isset($this->error['eletkor'])) {
            $this->data['error_eletkor'] = $this->error['eletkor'];
        } else {
            $this->data['error_eletkor'] = '';
        }

        if (isset($this->error['nem'])) {
            $this->data['error_nem'] = $this->error['nem'];
        } else {
            $this->data['error_nem'] = '';
        }

		$this->data['action'] = $this->url->link('account/edit', '', 'SSL');

		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
		}

		if (isset($this->request->post['firstname'])) {
			$this->data['firstname'] = $this->request->post['firstname'];
		} elseif (isset($customer_info)) {
			$this->data['firstname'] = $customer_info['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$this->data['lastname'] = $this->request->post['lastname'];
		} elseif (isset($customer_info)) {
			$this->data['lastname'] = $customer_info['lastname'];
		} else {
			$this->data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (isset($customer_info)) {
			$this->data['email'] = $customer_info['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$this->data['telephone'] = $this->request->post['telephone'];
		} elseif (isset($customer_info)) {
			$this->data['telephone'] = $customer_info['telephone'];
		} else {
			$this->data['telephone'] = '';
		}

        if (isset($this->request->post['weblap'])) {
            $this->data['weblap'] = $this->request->post['weblap'];
        } elseif (isset($customer_info['weblap'])) {
            $this->data['weblap'] = $customer_info['weblap'];
        } else {
            $this->data['weblap'] = '';
        }

        if (isset($this->request->post['paypalemail'])) {
            $this->data['paypalemail'] = $this->request->post['paypalemail'];
        } elseif (isset($customer_info)) {
            $this->data['paypalemail'] = $customer_info['Paypal_email'];
        } else {
            $this->data['paypalemail'] = '';
        }

        if (isset($this->request->post['fax'])) {
			$this->data['fax'] = $this->request->post['fax'];
		} elseif (isset($customer_info)) {
			$this->data['fax'] = $customer_info['fax'];
		} else {
			$this->data['fax'] = '';
		}

        if (isset($this->request->post['feltolto'])) {
            $this->data['feltolto'] = $this->request->post['feltolto'];
        } elseif (isset($customer_info)) {
            $this->data['feltolto'] = $customer_info['feltolto'];
        } else {
            $this->data['feltolto'] = '0';
        }
        $this->data['ceges'] = $this->data['feltolto'];

        if (isset($this->request->post['company'])) {
            $this->data['company'] = $this->request->post['company'];
        } elseif (isset($customer_info)) {
            $this->data['company'] = $customer_info['company'];
        } else {
            $this->data['company'] = '';
        }
        if (isset($this->request->post['adoszam'])) {
            $this->data['adoszam'] = $this->request->post['adoszam'];
        } elseif (isset($customer_info)) {
            $this->data['adoszam'] = $customer_info['adoszam'];
        } else {
            $this->data['adoszam'] = '';
        }


        if (isset($this->request->post['nem'])) {
            $this->data['nem'] = $this->request->post['nem'];
        } elseif (isset($customer_info)) {
            $this->data['nem'] = $customer_info['nem'];
        } else {
            $this->data['nem'] = '';
        }

        if (isset($this->request->post['eletkor'])) {
            $this->data['eletkor'] = $this->request->post['eletkor'];
        } elseif (isset($customer_info)) {
            $this->data['eletkor'] = $customer_info['eletkor'];
        } else {
            $this->data['eletkor'] = '';
        }

        if (isset($this->request->post['iskolai_vegzettseg'])) {
            $this->data['iskolai_vegzettseg'] = $this->request->post['iskolai_vegzettseg'];
        } elseif (isset($customer_info)) {
            $this->data['iskolai_vegzettseg'] = $customer_info['iskolai_vegzettseg'];
        } else {
            $this->data['iskolai_vegzettseg'] = '';
        }

        if (isset($this->request->post['vallalkozasi_forma'])) {
            $this->data['vallalkozasi_forma'] = $this->request->post['vallalkozasi_forma'];
        }  elseif (isset($customer_info)) {
            $this->data['vallalkozasi_forma'] = $customer_info['vallalkozasi_forma'];
        } else {
            $this->data['vallalkozasi_forma'] = '';
        }

        if (isset($this->request->post['szekhely'])) {
            $this->data['szekhely'] = $this->request->post['szekhely'];
        }  elseif (isset($customer_info)) {
            $this->data['szekhely'] = $customer_info['szekhely'];
        } else {
            $this->data['szekhely'] = '';
        }

        if (isset($this->request->post['ugyvezeto_neve'])) {
            $this->data['ugyvezeto_neve'] = $this->request->post['ugyvezeto_neve'];
        }  elseif (isset($customer_info)) {
            $this->data['ugyvezeto_neve'] = $customer_info['ugyvezeto_neve'];
        } else {
            $this->data['ugyvezeto_neve'] = '';
        }

        if (isset($this->request->post['ugyvezeto_telefonszama'])) {
            $this->data['ugyvezeto_telefonszama'] = $this->request->post['ugyvezeto_telefonszama'];
        }  elseif (isset($customer_info)) {
            $this->data['ugyvezeto_telefonszama'] = $customer_info['ugyvezeto_telefonszama'];
        } else {
            $this->data['ugyvezeto_telefonszama'] = '';
        }

        if( $this->config->get("megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben") != null) {
            $this->data['ceges_cimmodositas_a_fiok_szerkeztesben'] = $this->config->get("megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben");
        } else {
            $this->data['ceges_cimmodositas_a_fiok_szerkeztesben'] = 0;
        }

        if($this->config->get("megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben") != null) {
            $this->data['cimmodositas_a_fiok_szerkeztesben'] = $this->config->get("megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben");
        } else {
            $this->data['cimmodositas_a_fiok_szerkeztesben'] = 0;
        }
        $this->data['address'] = $this->url->link('account/address', '', 'SSL');

        $this->load->model('localisation/country');
        $this->data['eletkors'] = $this->model_localisation_country->getEletkor();

        $this->load->model('account/iskolaivegzettseg');
        $this->data['iskolai_vegzettsegek'] = $this->model_account_iskolaivegzettseg->getAll();

        $this->data['nems'][] = array(
            'ertek' => 1,
            'nev'   => $this->language->get('select_ferfi')
        );

        $this->data['nems'][] = array(
            'ertek' => 2,
            'nev'   => $this->language->get('select_no')
        );


		$this->data['back'] = $this->url->link('account/account', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/edit.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/edit.tpl';
		} else {
			$this->template = 'default/template/account/edit.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
						
		$this->response->setOutput($this->render());	
	}

	private function validate() {
		if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}
		
		if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

        if ($this->config->get('megjelenit_eletkor_regisztracio') == 1 ) {
            if ($this->request->post['eletkor'] == '') {
                $this->error['eletkor'] = $this->language->get('error_eletkor');
            }
        }


        if ($this->config->get('megjelenit_nem_regisztracio') == 1 ) {
            if ($this->request->post['nem'] == '') {
                $this->error['nem'] = $this->language->get('error_nem');
            }
        }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>