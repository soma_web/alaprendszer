BEGIN

declare product_azonosito   int(11) default 0;
declare store_azonosito     int(11) default 0;
declare category_azonosito  int(11) default 0;
declare product_to_category_azonosito int(11) default 0;

call termek_felvitel(uj_TermekKod);

select product_id into product_azonosito from trodimp_aruhaz.product where cikkszam LIKE uj_TermekKod;

select category_id into category_azonosito from trodimp_aruhaz.category where kategoria_kod = uj_Kod limit 1;

if category_azonosito = 0 then
INSERT INTO trodimp_aruhaz.category set
kategoria_kod = uj_Kod,
date_added    = NOW();
select category_id into category_azonosito from trodimp_aruhaz.category where kategoria_kod = uj_Kod;
end if;

select product_id into product_to_category_azonosito from trodimp_aruhaz.product_to_category where product_id = product_azonosito and category_id = category_azonosito limit 1;
if product_to_category_azonosito = 0 then
INSERT INTO trodimp_aruhaz.product_to_category set  product_id=product_azonosito, category_id=category_azonosito;
end if;

END