<?php if ($products) { ?>
<div class="android-box">
  <div class="row-title no-mb <?php echo $show_class; ?>"><?php echo $heading_title; ?></div>
  <div class="row-content no-bt <?php echo $show_class; ?>">
    <div class="product-list" id="bestsellers_<?php echo $module; ?>">
      <?php foreach ($products as $product) { ?>
       <div class="borders-inside">

           <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
               $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
           } else {
               $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
           } ?>
           <?php include($arkiir); ?>


		</div>
      <?php } ?>
    </div>
  </div>
</div>
<?php } ?>