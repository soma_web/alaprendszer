<?php
class ModelCsvTermekek {

    private $db;
    public $csv_tartalom;
    public $product;
    private $csv_sor = array();

    public function __construct($query) {
        $this->db = $query;
    }

    /*public function csvBeolvas($data) {

        $kodolas = false;
        if (!empty($data['kodolas_rol'])){
            $kodolas = true;
            if (empty($data['kodolas_ra'])){
                $data['kodolas_ra'] = "UTF-8";
            }
        }

        if ($kodolas) {
            $fc = iconv($data['kodolas_rol'], $data['kodolas_ra'], file_get_contents($data['filename']));
            file_put_contents('import.csv', $fc);
            $handle = fopen('import.csv', "r");
        } else {
            $handle = fopen($data['filename'], "r");
        }

        if ($data['filefejlec'] == 1) {
            $fejlec = fgetcsv($handle, NULL, isset($data['elvalaszto']) ? $data['elvalaszto'] : ";");
        }

        if ($handle){
            $i = 0;
            while (($sor = fgetcsv($handle, NULL, isset($data['elvalaszto']) ? $data['elvalaszto'] : ";"))) {
                if (substr($sor[0],0,3) == "STR") {
                    $this->csv_tartalom[] = $sor;
                    $i++;
                    //if ($i > 100) break;
                }

                if (substr($sor[0],0,3) == "DAI") {
                    $this->csv_tartalom[] = $sor;
                    $i++;
                    //if ($i > 100) break;
                }

                if (substr($sor[0],0,3) == "JHK") {
                    $this->csv_tartalom[] = $sor;
                    $i++;
                    if ($i > 100) break;
                }
                $this->csv_tartalom[] = $sor;

                if ($i==0) {
                    $kodolva = mb_detect_encoding($sor);
                }
                if ($i==1) {
                    $kodolva1 = mb_detect_encoding($sor);
                }
                if ($i==2) {
                    $kodolva2 = mb_detect_encoding($sor);
                }

                $i++;


            }
        }
        fclose($handle);
        if ($kodolas) {
            unlink('import.csv');
        }

    }*/

    public function csvAdatbazisba($data,$csv_tartalom) {
        $this->csv_tartalom = $csv_tartalom;
        $szamlalo = 0;
        $azonosito = -1;

        $attribute_group_id = $this->attribute_groupLetezesVizsgalat();

        foreach($this->csv_tartalom as $key=>$this->csv_sor){

            if ( !empty($this->csv_sor[47]) && !strstr($this->csv_sor[47],"#") ) {
                $this->csv_sor[47] = "#".$this->csv_sor[47];
            }

            if ($azonosito != $this->csv_sor[0]) {
                $azonosito = $this->csv_sor[0];

                $product_id = $this->productLetezesVizsgalat();
                $this->product_to_Torol($product_id);

                $price = str_replace('Ft','',$this->csv_tartalom[$key][41]);
                $price = (int)str_replace(' ','',$price);

                for($i = $key; isset($this->csv_tartalom[$i][0]) && $this->csv_tartalom[$i][0] == $azonosito; $i++) {
                    $this->csv_tartalom[$i][41] = str_replace('Ft','',$this->csv_tartalom[$i][41]);
                    $this->csv_tartalom[$i][41] = str_replace(' ','',$this->csv_tartalom[$i][41]);

                    $price = (int)$this->csv_tartalom[$i][41] < $price ? (int)$this->csv_tartalom[$i][41] : $price;
                }
            }



            if ($this->csv_sor['25'] == 'Igen') {

                $product_id = $this->productLetezesVizsgalat();

                if ($product_id) {
                    /*if ($this->product) {
                        if (substr($this->csv_sor[0],0,3) == "DAI" || substr($this->csv_sor[0],0,3) == "JHK" || substr($this->csv_sor[0],0,3) == "RUS") {
                            $kep_azonosito = explode('/',$this->csv_sor['27']);
                            $kep_azonosito = isset($kep_azonosito[1]) ? $kep_azonosito[1] : '';
                            $utvonal = 'data/textil/'.substr($this->csv_sor[0],0,3).'_termekek/'.$kep_azonosito.'_master.jpg';

                            $image = $this->product['image'] ? $this->product['image'] : $utvonal;


                            if ($this->product['image'] && $this->product['image'] != $utvonal) {
                                $this->kiskepLetezesVizsgalat($utvonal,$product_id);
                                $details = glob(DIR_ARUHAZ.'image/data/textil/'.substr($this->csv_sor[0],0,3).'_termekek/'.$kep_azonosito.'_detail*');
                                if ($details) {
                                    foreach($details as $detail) {
                                        $image_info = pathinfo($detail);
                                        $utvonal = 'data/textil/'.substr($this->csv_sor[0],0,3).'_termekek/'.$image_info['basename'];
                                        $this->kiskepLetezesVizsgalat($utvonal,$product_id);
                                    }
                                }

                            }


                        } else {
                            $utvonal = 'data/textil/STR_termekek/'.$this->csv_sor[27];
                            $image = $this->product['image'] ? $this->product['image'] : $utvonal;
                            $image_info = pathinfo($image);
                            if ($this->product['image'] && $image_info['basename'] != $this->csv_sor[27]) {
                                if ($this->csv_sor[27]) {
                                    $this->kiskepLetezesVizsgalat($utvonal,$product_id);
                                }
                            }

                        }



                    } else {
                        if (substr($this->csv_sor[0],0,3) == "DAI" || substr($this->csv_sor[0],0,3) == "JHK" || substr($this->csv_sor[0],0,3) == "RUS") {
                            $kep_azonosito = explode('/',$this->csv_sor['27']);
                            $kep_azonosito = isset($kep_azonosito[1]) ? $kep_azonosito[1] : '';
                            $image = isset($kep_azonosito[1]) ? 'data/textil/'.substr($this->csv_sor[0],0,3).'_termekek/'.$kep_azonosito.'_master.jpg' : '';

                        } else {
                            $image = 'data/textil/STR_termekek/' .$this->csv_sor[27];
                        }
                    }

                    $sql = "UPDATE ".DB_PREFIX."product SET
                                `megyseg`           = '".$this->csv_sor[13]."',
                                `manufacturer_id`   = '".$this->manufacturerLetezesVizsgalat()."',
                                `image`             = '" .$image. "',
                                `price`             = '" .$price. "',
                                `kifuto`            = '".($this->csv_sor[32] == 'Igen' ? 1 : 0)."',
                                `status`            = '".($this->csv_sor[35] == 'Igen' ? 0 : 1)."',
                                `szin_meret_szukseges`  = 1,
                                `date_modified`     = NOW()
                         WHERE product_id = '".$product_id."'";*/

                    $sql = "UPDATE ".DB_PREFIX."product SET
                                `price`             = '" .$price. "',
                                `date_modified`     = NOW()
                         WHERE product_id = '".$product_id."'";
                    $query = $this->db->query($sql);
                    if ($query) $szamlalo ++;


                    $this->product_descriptionLetezesVizsgalat($product_id);

                    // Választék
                    $option_id          = $this->optionLetezesVizsgalat();
                    $option_szin_id     = $this->option_szinLetezesVizsgalat();
                    $option_value_id    = $this->option_valueLetezesVizsgalat($option_id,$option_szin_id);
                    $product_option_id  = $this->product_optionLetezesVizsgalat($product_id,$option_id);
                    $this->option_value_descriptionLetezesVizsgalat($option_id,$option_value_id);
                    $this->product_option_valueLetezesVizsgalat($product_id,$option_id,$option_value_id,$product_option_id,$option_szin_id,$price);

                    $this->option_szin_groupLetezesVizsgalat($option_szin_id);

                    if ($this->csv_sor[48]) {
                        $this->attributeLetezesVizsgalat($attribute_group_id,$product_id);
                    }

                    /*
                    // kategória
                    $fokategoriak = explode('/',$this->csv_sor[45]);
                    if ($fokategoriak) {
                        foreach($fokategoriak as $fokategoria) {
                            $fo_category_id = $this->kategoriaLetezesVizsgalat(trim($fokategoria));
                            $this->product_to_categoryLetezesVizsgalat($product_id,$fo_category_id);

                        }
                    }


                    if ($this->csv_sor[46]) {
                        $alkategoriak = explode('/',$this->csv_sor[46]);

                        if (count($alkategoriak) > 1 ) {
                            echo '';
                        }

                        foreach($alkategoriak as $alkategoria) {

                            if ($alkategoria && $alkategoria != '-' && $alkategoria != '-0') {
                                $category_id = $this->alkategoriaLetezesVizsgalat($fo_category_id,trim($alkategoria));
                                $this->product_to_categoryLetezesVizsgalat($product_id, $category_id);
                            }
                        }
                    }*/



                }
            }
        }


        echo count($this->csv_tartalom)." sorból ".$szamlalo. " sor bedolgozása sikerült !";
        //unlink ($data['filename']);

    }
/*
0   Cikkszám
1   Megnevezés
2   Megnevezés (angol)
3   Megnevezés (német)
8   Szöveges leírás
9   Szöveges leírás (angol)
10  Szöveges leírás (német)
13  Alap mértékegység
21  Márka
25  Webshopban?
27  Kiskép elérési útja
32  Kifutó?
35  Inaktív cikk? (zárolt)
44  Méret
42  Szín
4   Változatkód
41  Listaár
43  Színcsoport

45  Kategória
46  Alkategória
47  Színkód (HEX)
48 attribute



//5   Változatkód megnevezése
//6   Változatkód megnevezése (angol)
//7   Változatkód megnevezése (német)

//11  Származási ország
//12  VTSZ
//14  Szállító 1
//15  Másodlagos mértékegység
//16  Harmadlagos mértékegység
//17  Másodlagos mértékegység váltószám
//18  Harmadlagos mértékegység váltószám
//19  Alap mértékegység nettó súly
//20  Alap mértékegység bruttó súly
//22  Cikkcsoport
//23  Termékcsoport
//24  Kulcsszavak
//26  Kiskép neve               - üres
//28  Nagykép neve              - üres
//29  Nagykép elérési útja      - üres

//30  Bmp kép neve              - üres
//31  Bmp kép elérési útja      - üres
//33  Elfekvő?

//34  Akciós?
//36  Cikk engedmény csoport kód
//37  Minőségi kategória
//38  Primer?
//39  Törlendő?
//40  Beszer. ár*/




    public function productLetezesVizsgalat(){

        $sql = "SELECT * FROM ".DB_PREFIX."product WHERE model='".$this->csv_sor[0]."'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO ".DB_PREFIX."product SET
                `model`             = '".$this->csv_sor[0]."'";

            $query = $this->db->query($sql);
            $last_id = $this->db->getLastId();

            $store_id =  (substr($this->csv_sor[0],0,3) == "DAI" || substr($this->csv_sor[0],0,3) == "JHK" || substr($this->csv_sor[0],0,3) == "RUS" ? 0 : 1);
            $sql = "INSERT INTO ".DB_PREFIX."product_to_store SET
                                        product_id  = '".$last_id."',
                                        store_id  = '".$store_id."'";
            $query = $this->db->query($sql);
            $this->product = false;

        } else {
            $last_id = $query->row['product_id'];
            $this->product = $query->row;
        }

        return $last_id;

    }

    public function product_descriptionLetezesVizsgalat($product_id) {

        $this->leiras($product_id,1);
        $this->leiras($product_id,2);
        $this->leiras($product_id,3);


    }

    public function leiras($product_id,$language_id) {
        $sql = "SELECT product_id FROM ".DB_PREFIX."product_description WHERE product_id='".$product_id."' AND `language_id`='".$language_id."'";
        $query = $this->db->query($sql);

        $nev    = $language_id == 1 ? $this->csv_sor[2] : ($language_id == 2 ? $this->csv_sor[1] :  $this->csv_sor[3]);
        $leiras = $language_id == 1 ? $this->csv_sor[9] : ($language_id == 2 ? $this->csv_sor[8] :  $this->csv_sor[10]);

        if ($query->num_rows == 0) {
            $sql1 = sprintf("INSERT INTO " . DB_PREFIX . "product_description SET
                                        `product_id`  = '" .$product_id. "',
                                        `name`        = '%s',
                                        `description` = '%s',
                                        `language_id` = '".$language_id."'",  mysql_real_escape_string($nev),
                                                                                mysql_real_escape_string($leiras));

            $query = $this->db->query($sql1);
        } else {
            $sql1 = sprintf("UPDATE " . DB_PREFIX . "product_description SET
                                        `product_id`  = '" .$product_id. "',
                                        `name`        = '%s',
                                        `description` = '%s',
                                        `language_id` = '".$language_id."'
                                        WHERE
                                            product_id = '".$product_id."'
                                            AND language_id = '".$language_id."'",  mysql_real_escape_string($nev),
                                                                                mysql_real_escape_string($leiras));

            $query = $this->db->query($sql1);
        }
    }

    public function manufacturerLetezesVizsgalat() {

        $sql = "SELECT * FROM ".DB_PREFIX."manufacturer WHERE `name` LIKE '".$this->csv_sor[21]."'" ;
        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "manufacturer SET
                                            `name`  = '" . $this->csv_sor[21] . "'";
            $query = $this->db->query($sql);
            $manufacturer_id = $this->db->getLastId();
        } else {
            $manufacturer_id = $query->row['manufacturer_id'];
        }

        return $manufacturer_id;
    }

    public function kiskepLetezesVizsgalat($utvonal,$product_id) {

        $sql = "SELECT * FROM ".DB_PREFIX."product_image WHERE
            `image` LIKE '" .$utvonal."'
            AND product_id='" .$product_id. "'" ;

        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "product_image SET
                      `image`  = '" .$utvonal. "',
                     product_id='" .$product_id. "'" ;
            $query = $this->db->query($sql);
        }
    }


    /* Választék */

    public function optionLetezesVizsgalat() {
        $sql = "SELECT * FROM ".DB_PREFIX."option_description WHERE
            `name` LIKE '".$this->csv_sor[44]."'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "option SET
                      `type`  = 'checkbox_qty'";
            $query = $this->db->query($sql);
            $option_id = $this->db->getLastId();

            for ($i= 1; $i<=3; $i++) {
                $sql = "INSERT INTO " . DB_PREFIX . "option_description SET
                      `option_id`   = '".$option_id."',
                      `name`        = '".$this->csv_sor[44]."',
                      `language_id` = '".$i."'";
                $query = $this->db->query($sql);
            }

        } else {
            $option_id = $query->row['option_id'];
        }

        return $option_id;
    }

    public function product_optionLetezesVizsgalat($product_id, $option_id) {
        $sql = "SELECT * FROM ".DB_PREFIX."product_option WHERE
                `product_id` = '" .$product_id. "'
            AND option_id    = '" .$option_id."'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "product_option SET
                `product_id` = '" .$product_id. "',
                `option_id` = '" .$option_id. "'";
            $query = $this->db->query($sql);
            $product_option_id = $this->db->getLastId();
        } else {
            $product_option_id = $query->row['product_option_id'];
        }

        return $product_option_id;
    }

    public function option_szinLetezesVizsgalat() {
        $sql = "SELECT * FROM ".DB_PREFIX."option_szin WHERE
                `szinkod` = '" .$this->csv_sor[47]. "'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {

            $sql = "INSERT INTO " . DB_PREFIX . "option_szin SET
                `szinkod` = '" .$this->csv_sor[47]. "'";
            $query = $this->db->query($sql);

            $option_szin_id = $this->db->getLastId();

            for ($i= 1; $i<=3; $i++) {
                $sql = "INSERT INTO " . DB_PREFIX . "option_szin_description SET
                `option_szin_id` = '" .$option_szin_id. "',
                `language_id` = '" .$i. "',
                `name` = '" .$this->csv_sor[42]. "'";
                $query = $this->db->query($sql);
            }


        } else {
            $sql = "UPDATE ". DB_PREFIX . "option_szin_description SET
                `name` = '" .$this->csv_sor[42]. "'
                WHERE option_szin_id = '".$query->row['option_szin_id']."'";

            $option_szin_id = $query->row['option_szin_id'];
        }

        return $option_szin_id;
    }

    public function option_szin_groupLetezesVizsgalat($option_szin_id) {

        if ($this->csv_sor[43]) {
            $szin_group_names = explode('/',$this->csv_sor[43]);
            $option_szin_group_ids = array();

            foreach($szin_group_names as $szin_group_name) {
                $sql = "SELECT * FROM ".DB_PREFIX."option_szin_group_description WHERE
                        `name` = '" .$szin_group_name. "'";
                $query = $this->db->query($sql);

                if ($query->num_rows == 0) {

                    $sql = "INSERT INTO " . DB_PREFIX . "option_szin_group
                        SET `szinkod` = '".$this->csv_sor[47]."'";
                    $query = $this->db->query($sql);

                    $option_szin_group_id = $this->db->getLastId();
                    $option_szin_group_ids[] = $option_szin_group_id;
                    for ($i= 1; $i<=3; $i++) {
                        $sql = "INSERT INTO " . DB_PREFIX . "option_szin_group_description SET
                            `option_szin_group_id` = '" .$option_szin_group_id. "',
                            `language_id` = '" .$i. "',
                            `name` = '" .$szin_group_name. "'";
                        $query = $this->db->query($sql);
                    }

                } else {
                    $sql = "UPDATE ". DB_PREFIX . "option_szin_group_description SET
                        `name` = '" .$szin_group_name. "'
                        WHERE option_szin_group_id = '".$query->row['option_szin_group_id']."'";
                    $felvitel = $this->db->query($sql);

                    $option_szin_group_id = $query->row['option_szin_group_id'];
                    $option_szin_group_ids[] = $option_szin_group_id;
                }
            }

            $sql = "DELETE FROM " .DB_PREFIX. "option_szin_to_group WHERE option_szin_id='".$option_szin_id."'";
            $query = $this->db->query($sql);

            foreach ($option_szin_group_ids as $option_szin_group_id) {
                $sql = "INSERT INTO " .DB_PREFIX. "option_szin_to_group SET
                        option_szin_group_id='".$option_szin_group_id."',
                        option_szin_id='".$option_szin_id."'";
                $query = $this->db->query($sql);
            }
        }

        return;
    }



    public function option_valueLetezesVizsgalat($option_id,$option_szin_id) {
        $sql = "SELECT * FROM ".DB_PREFIX."option_value WHERE
                `option_id` = '" .$option_id. "'
                AND `option_szin_id` = '" .$option_szin_id. "'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "option_value SET
            `option_id`         = '" .$option_id. "',
            `option_szin_id`    = '" .$option_szin_id. "',
            `heading`           = '" .$this->csv_sor[47]. "',
            `azonosito`         = '" .$this->csv_sor[44]. "'";
            $query = $this->db->query($sql);
            $option_value_id = $this->db->getLastId();

        } else {
            $option_value_id = $query->row['option_value_id'];
            $sql1 = "UPDATE " . DB_PREFIX . "option_value SET
                                        `heading`        = '" .$this->csv_sor[47]. "'
                                        `azonosito`      = '" .$this->csv_sor[44]. "'
                                  WHERE option_szin_id = '".$option_szin_id."' AND option_id = '".$option_id."'";
            $query = $this->db->query($sql1);
        }

        return $option_value_id;
    }


    public function option_value_descriptionLetezesVizsgalat($option_id,$option_value_id) {
        $sql = "SELECT * FROM ".DB_PREFIX."option_value_description WHERE
                `option_value_id` = '" .$option_value_id. "'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {

            for ($i= 1; $i<=3; $i++) {
                $sql = "INSERT INTO " . DB_PREFIX . "option_value_description SET
                      `option_value_id`   = '".$option_value_id."',
                      `option_id`   = '".$option_id."',
                      `name`        = '".$this->csv_sor[42]."',
                      `language_id` = '".$i."'";
                $query = $this->db->query($sql);
            }

        } else {
            $sql1 = "UPDATE " . DB_PREFIX . "option_value_description SET
                                        `name`        = '" .$this->csv_sor[42]. "'
                                  WHERE option_value_id = '".$option_value_id."'";
            $query = $this->db->query($sql1);
        }
    }


    public function product_option_valueLetezesVizsgalat($product_id,$option_id,$option_value_id,$product_option_id,$option_szin_id,$price) {

    /*1	product_option_value_id
	2	product_option_id
	3	product_id
	4	option_id
	5	option_value_id

	6	quantity
	7	subtract
	8	price
	9	price_prefix
	10	points
	11	points_prefix
	12	weight
	13	weight_prefix
	14	option_szin_id
	15	azonosito*/

        $ar = str_replace('Ft','',$this->csv_sor[41]);
        $ar = (int)str_replace(' ','',$ar);

        $ar = $ar - $price;

        $sql = "SELECT * FROM ".DB_PREFIX."product_option_value WHERE
                `product_id`        = '" .$product_id. "'
                AND `product_option_id` = '" .$product_option_id. "'
                AND `option_id`         = '" .$option_id. "'
                AND `option_value_id`   = '" .$option_value_id. "'";

        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET
            `product_id`        = '" .$product_id. "',
            `product_option_id` = '" .$product_option_id. "',
            `option_id`         = '" .$option_id. "',
            `option_value_id`   = '" .$option_value_id. "',
            `option_szin_id`    = '" .$option_szin_id. "',

            `quantity`          = 0,
            `subtract`          = 0,
            `price`             = '" .(float)$ar. "',
            `price_prefix`      = '+',
            `points`            = 0,
            `points_prefix`     = '+',
            `weight`            = 0,
            `weight_prefix`     = '+',
            `azonosito`         = '" .$this->csv_sor[4]. "'";

            $query = $this->db->query($sql);
            $product_option_value_id = $this->db->getLastId();

        } else {
            $product_option_value_id = $query->row['product_option_value_id'];

            $sql = "UPDATE " . DB_PREFIX . "product_option_value SET
                `product_id`        = '" .$product_id. "',
                `product_option_id` = '" .$product_option_id. "',
                `option_id`         = '" .$option_id. "',
                `option_value_id`   = '" .$option_value_id. "',
                `option_szin_id`    = '" .$option_szin_id. "',

                `quantity`          = 0,
                `subtract`          = 0,
                `price`             = '" .(float)$ar. "',
                `price_prefix`      = '+',
                `points`            = 0,
                `points_prefix`     = '+',
                `weight`            = 0,
                `weight_prefix`     = '+',
                `azonosito`         = '" .$this->csv_sor[4]. "'
                WHERE product_option_value_id = '".$product_option_value_id."'";

            $query = $this->db->query($sql);


        }
    }

    /* Kategória */

    public function kategoriaLetezesVizsgalat($fokategoria) {

        $sql = "SELECT * FROM ".DB_PREFIX."category_description cd
                LEFT JOIN ".DB_PREFIX."category_to_store c2s ON (c2s.category_id=cd.category_id)
                WHERE cd.name LIKE '" .$fokategoria. "'
                AND c2s.store_id = '".(substr($this->csv_sor[0],0,3) == "STR" ? 1 : 0)."'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "category SET
                `parent_id` = 0,
                `top` = 0,
                `column` = 1,
                `sort_order` = 0,
                `status` = 1,
                `date_added` = NOW()";
            $query = $this->db->query($sql);
            $category_id = $this->db->getLastId();

            $sql = "INSERT INTO " . DB_PREFIX . "category_to_store SET
                `store_id` = '".(substr($this->csv_sor[0],0,3) == "STR" ? 1 : 0)."',
                `category_id` = '" .$category_id. "'";
            $query = $this->db->query($sql);


            for ($i= 1; $i<=3; $i++) {
                $sql = "INSERT INTO " . DB_PREFIX . "category_description SET
                `category_id` = '" .$category_id. "',
                `language_id` = '" .$i. "',
                `name`      = '" .$fokategoria. "'";
                $query = $this->db->query($sql);
            }
        } else {
            $category_id = $query->row['category_id'];
        }

        return $category_id;
    }

    public function product_to_categoryLetezesVizsgalat($product_id,$category_id) {

        $sql = "SELECT * FROM ".DB_PREFIX."product_to_category WHERE
                `product_id` = '" .$product_id. "'
                AND `category_id` = '" .$category_id. "'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "product_to_category SET
                `product_id` = '" .$product_id. "',
                `category_id` = '" .$category_id. "'";
            $query = $this->db->query($sql);
        }
    }


    public function alkategoriaLetezesVizsgalat($category_id,$alkategoria) {

        $sql = "SELECT * FROM ".DB_PREFIX."category_description cd
                    LEFT JOIN ".DB_PREFIX."category c ON (cd.category_id = c.category_id)
                    LEFT JOIN ".DB_PREFIX."category_to_store c2s ON (c2s.category_id=cd.category_id)

                    WHERE   cd.name     LIKE '" .$alkategoria. "'
                        AND c2s.store_id = '".(substr($this->csv_sor[0],0,3) == "STR" ? 1 : 0)."'
                        AND c.parent_id = '".$category_id."'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "category SET
                `parent_id` = '".$category_id."',
                `top` = 0,
                `column` = 1,
                `sort_order` = 0,
                `status` = 1,
                `date_added` = NOW()";
            $query = $this->db->query($sql);
            $category_id = $this->db->getLastId();

            $sql = "INSERT INTO " . DB_PREFIX . "category_to_store SET
                `store_id` = '".(substr($this->csv_sor[0],0,3) == "STR" ? 1 : 0)."',
                `category_id` = '" .$category_id. "'";
            $query = $this->db->query($sql);

            for ($i= 1; $i<=3; $i++) {
                $sql = "INSERT INTO " . DB_PREFIX . "category_description SET
                `category_id` = '" .$category_id. "',
                `language_id` = '" .$i. "',
                `name`      = '" .$alkategoria. "'";
                $query = $this->db->query($sql);
            }
        } else {
            $category_id = $query->row['category_id'];
        }

        return $category_id;
    }

    public function attribute_groupLetezesVizsgalat() {
        $sql = "SELECT * FROM ".DB_PREFIX."attribute_group_description WHERE `name` LIKE '".TULAJDONSAG_CSOPORT."'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {

            $sql = "INSERT INTO " . DB_PREFIX . "attribute_group  set sort_order = 0";
            $query = $this->db->query($sql);

            $attribute_group_id  = $this->db->getLastId();

            for ($i= 1; $i<=3; $i++) {

                $sql = "INSERT INTO " . DB_PREFIX . "attribute_group_description SET
                      `name`        = '" . TULAJDONSAG_CSOPORT . "',
                      `attribute_group_id`        = '" .$attribute_group_id. "',
                      `language_id` = '".$i."'";
                $query = $this->db->query($sql);
            }

        } else {
            $attribute_group_id = $query->row['attribute_group_id'];
        }

        return $attribute_group_id;
    }

    public function attributeLetezesVizsgalat($attribute_group_id,$product_id) {

        $sql = "SELECT * FROM ".DB_PREFIX."attribute_description WHERE
            `name` LIKE '".$this->csv_sor[48]."'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "attribute SET
                      `attribute_group_id`  = '".$attribute_group_id."'";
            $query = $this->db->query($sql);
            $attribute_id = $this->db->getLastId();

            for ($i= 1; $i<=3; $i++) {
                $sql = "INSERT INTO " . DB_PREFIX . "attribute_description SET
                      `attribute_id`   = '".$attribute_id."',
                      `name`        = '".$this->csv_sor[48]."',
                      `language_id` = '".$i."'";
                $query = $this->db->query($sql);
            }

        } else {
            $attribute_id = $query->row['attribute_id'];
        }



        $sql = "SELECT * FROM ".DB_PREFIX. "product_attribute WHERE product_id='".$product_id."'
                    AND attribute_id='".$attribute_id."'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "product_attribute SET
                      `attribute_id`    = '".$attribute_id."',
                      `product_id`      = '".$product_id."'";
            $query = $this->db->query($sql);
        }

    }



    public function product_to_Torol($product_id) {
        $sql = "DELETE FROM ".DB_PREFIX. "product_to_category WHERE product_id='".$product_id."'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM ".DB_PREFIX. "product_option WHERE product_id='".$product_id."'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM ".DB_PREFIX. "product_option_value WHERE product_id='".$product_id."'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM ".DB_PREFIX. "product_attribute WHERE product_id='".$product_id."'";
        $query = $this->db->query($sql);

    }


}
?>
