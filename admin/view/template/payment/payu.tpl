<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                    <tr>
                        <td><span class="required">*</span> <?php echo $entry_merchant_eur; ?></td>
                        <td><input type="text" name="payu_merchant_eur" value="<?php echo $payu_merchant_eur; ?>" />
                            <?php if ($error_merchant) { ?>
                                <span class="error"><?php echo $error_merchant; ?></span>
                                <?php } ?></td>
                    </tr>

                    <tr>
                        <td><span class="required">*</span> <?php echo $entry_secret_key_eur; ?></td>
                        <td><input type="text" name="payu_secret_key_eur" value="<?php echo $payu_secret_key_eur; ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td><span class="required">*</span> <?php echo $entry_merchant_huf; ?></td>
                        <td><input type="text" name="payu_merchant_huf" value="<?php echo $payu_merchant_huf; ?>" /></td>
                    </tr>

                    <tr>
                        <td><span class="required">*</span> <?php echo $entry_secret_key_huf; ?></td>
                        <td><input type="text" name="payu_secret_key_huf" value="<?php echo $payu_secret_key_huf; ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_testorder; ?></td>
                        <td><select name="payu_testorder">
                            <?php if ($payu_testorder) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                        </select></td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_debug; ?></td>
                        <td><select name="payu_debug">
                            <?php if ($payu_debug) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                        </select></td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_language; ?></td>
                        <td><select name="payu_language" style="width: 100px;">
                            <?php if ($payu_language == 'HU') { ?>
                            <option value="HU" selected="selected"><?php echo $text_language_hu; ?></option>
                            <?php } else { ?>
                            <option value="HU"><?php echo $text_language_hu; ?></option>
                            <?php } ?>
                            <?php if ($payu_language == 'EN') { ?>
                            <option value="EN" selected="selected"><?php echo $text_language_en; ?></option>
                            <?php } else { ?>
                            <option value="EN"><?php echo $text_language_en; ?></option>
                            <?php } ?>
                        </select></td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_order_timeout; ?></td>
                        <td><input type="text" name="payu_order_timeout" value="<?php echo $payu_order_timeout; ?>" />
                            </td>
                    </tr>


                    <tr>
                        <td><?php echo $entry_order_status; ?></td>
                        <td><select name="payu_order_status_id">
                            <?php foreach ($order_statuses as $order_status) { ?>
                            <?php if ($order_status['order_status_id'] == $payu_order_status_id) { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select></td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_status; ?></td>
                        <td><select name="payu_status">
                            <?php if ($payu_status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                        </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_sort_order; ?></td>
                        <td><input type="text" name="payu_sort_order" value="<?php echo $payu_sort_order; ?>" size="1" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>