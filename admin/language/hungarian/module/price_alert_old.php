<?php
// Heading
$_['heading_title']       = 'Árváltozás figyelő';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikerült: az árváltozás figyelő modul módosítása metörtént!';
$_['text_heading_title']  = 'Fejléc';
$_['text_image_manager']  = 'Kép kezelő';
$_['text_only_image']     = 'Csak kép (banner típus)';
$_['text_image_box']      = 'Kép belül (box)';
$_['text_yes']            = 'Igen';
$_['text_no']             = 'Nem';
$_['text_browse']         = 'Fájlok tallózása';
$_['text_clear']          = 'Kép törlése';
$_['text_content_top']    = 'Tartalom felül';
$_['text_content_bottom'] = 'Tartalom alul';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

// Entry
$_['entry_secret_code']   = 'Titkos kód: <span class="help">legalább 5 karakter! ( a-z, A-Z, 0-9 ) <br />Szükséges a Cron Job</span>';
$_['entry_image']         = 'Kép';
$_['entry_dimension']     = 'Kép méretek: (W x H)';
$_['entry_display_type']  = 'Megjelenítés típusa';
$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_status']        = 'Statusz:';
$_['entry_sort_order']    = 'Sorrend:';

// Error
$_['error_permission']    = 'Figyelmeztetés: nincs jogosultsága módosítani az árváltozás figyelő modult!';
$_['error_secret_code']   = 'Titkos kód: legalább 5 karakter! ( a-z, A-Z, 0-9 )';
$_['error_image']         = 'Kép - szükséges';
$_['error_dimension']     = 'Méret - szükséges';
?>