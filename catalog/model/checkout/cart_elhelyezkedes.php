<?php
date_default_timezone_set('Europe/Budapest');
/**
 * @property mixed db
 */
class ModelCheckoutCartElhelyezkedes extends Model {




    public function kellAlcsoport () {

        if ($_REQUEST['elhelyezkedes_id'] == 0) {
            return false;
        }

        $vissza = false;
        $elhelyezkedes = $this->config->get('elhelyezkedes');
        foreach($elhelyezkedes as $value) {
            if ($value['elhelyezkedes_id'] == $_REQUEST['elhelyezkedes_id']) {
                if ($value['alcsoport_status'] == 1) {
                    $vissza = true;
                }
            }
        }

        return $vissza;

    }

}

?>