<?php
class ModelCatalogFizetesElbiralas extends Model {

    public function getFizetesElbiralas() {

        $vissza = array();
        $elhelyezkedesek = $this->config->get("fizetes_elbiralas");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['status'] == 1) {
                    $vissza[] = $value;
                }
            }
        }

        return $vissza;
    }

}
?>