<?php
class ModelPaymentBudapestbank extends Model {

    protected $db_table_transaction = 'transaction';
    protected $db_table_batch       = 'batch';
    protected $db_table_error       = 'error';

    public function getMethod($address, $total) {
        $this->load->language('payment/budapestbank');

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('pp_standard_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

        if ($this->config->get('budapestbank_total') > $total) {
            $status = false;
        } elseif (!$this->config->get('budapestbank_geo_zone_id')) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }

        $currencies = array(
            'AUD',
            'CAD',
            'EUR',
            'GBP',
            'JPY',
            'USD',
            'NZD',
            'CHF',
            'HKD',
            'SGD',
            'SEK',
            'DKK',
            'PLN',
            'NOK',
            'HUF',
            'CZK',
            'ILS',
            'MXN',
            'MYR',
            'BRL',
            'PHP',
            'TWD',
            'THB',
            'TRY'
        );

        if (!in_array(strtoupper($this->currency->getCode()), $currencies)) {
            $status = false;
        }

        $method_data = array();

        if ($status) {
            $method_data = array(
                'code'       => 'budapestbank',
                'title'      => $this->language->get('text_title'),
                'sort_order' => $this->config->get('budapestbank_sort_order')
            );
        }

        return $method_data;
    }

    function checkDB() {

        //$db_table_transaction   =     'transaction';
        //$db_table_batch         =     'batch';
        //$db_table_error         =     'error';

        //$this->db->query("CREATE DATABASE /*!32312 IF NOT EXISTS*/ $this->db_database");

        $this->db->query("CREATE TABLE /*!32312 IF NOT EXISTS*/ $this->db_table_transaction  (
          id INT(10) NOT NULL AUTO_INCREMENT,
          trans_id VARCHAR(50),
          amount INT(10),
          currency INT(10),
          client_ip_addr VARCHAR(50),
          description TEXT,
          language VARCHAR(50),
          dms_ok VARCHAR(50),
          result VARCHAR(50),
          result_code VARCHAR(50),
          result_3dsecure VARCHAR(50),
          card_number VARCHAR(50),
          t_date VARCHAR(20),
          response TEXT,
          reversal_amount INT(10),
          makeDMS_amount INT(10),
          PRIMARY KEY (id)
          )
          ");

        $this->db->query("CREATE TABLE /*!32312 IF NOT EXISTS*/ $this->db_table_batch  (
          id INT(10) NOT NULL AUTO_INCREMENT,
          result TEXT,
          result_code VARCHAR(3),
          count_reversal VARCHAR(10),
          count_transaction VARCHAR(10),
          amount_reversal VARCHAR(16),
          amount_transaction VARCHAR(16),
          close_date VARCHAR(20),
          response TEXT,
          PRIMARY KEY (id)
          )
          ");

        $this->db->query("CREATE TABLE /*!32312 IF NOT EXISTS*/ $this->db_table_error  (
          id INT(10) NOT NULL AUTO_INCREMENT,
          error_time VARCHAR (20),
          action VARCHAR (20),
          response TEXT,
          PRIMARY KEY (id)
          )
          ");
    }

    function saveTransaction($trans_id, $amount, $currency, $ip, $description, $language, $resp) {
        return $this->db->query("INSERT INTO $this->db_table_transaction VALUES ('', '$trans_id', '$amount', '$currency', '$ip', '$description', '$language', '---', '???', '???', '???', '???', now(), '$resp', '', '')");
    }

    function saveBadTransaction($resp) {
        return $this->db->query("INSERT INTO $this->db_table_error VALUES ('', now(), 'startsmstrans', '$resp')");
    }

    function getTransaction($trans_id) {
        return $this->db->query("SELECT client_ip_addr FROM $this->db_table_transaction WHERE `trans_id` = '$trans_id'");
    }

    function updateTransaction($result, $result_code, $result_3dsecure, $card_number, $resp, $trans_id) {
        return $this->db->query("UPDATE $this->db_table_transaction SET result='$result', result_code='$result_code', result_3dsecure='$result_3dsecure',card_number='$card_number',response='$resp' WHERE trans_id='$trans_id'");
    }

    function errorTransaction($resp) {
        return $this->db->query("INSERT INTO $this->db_table_error VALUES ('', now(), 'ReturnOkURL', '$resp')");
    }

    function insertCloseDay($db_table_batch, $result, $result_code, $count_reversal, $count_transaction, $amount_reversal, $amount_transaction, $resp) {
        return $this->db->query("INSERT INTO $db_table_batch  VALUES ('', '$result', '$result_code', '$count_reversal', '$count_transaction', '$amount_reversal', '$amount_transaction', now(), '$resp')");
    }

    //REP
    function getFullTransaction($trans_id) {
        return $this->db->query("SELECT description FROM $this->db_table_transaction WHERE `trans_id` = '$trans_id'");
    }
}
?>