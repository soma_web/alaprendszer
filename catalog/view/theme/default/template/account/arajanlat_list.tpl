<?php echo $header; ?>
<?php if ($error_warning) { ?>
    <script>
        var html = '<div style="display_none" class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>';
        $('#notification').html(html);
        $(".warning").slideDown('slow');
        notaficationBezar();
    </script>

<?php } ?>

<?php if ($success) { ?>
    <script>
        var html = '<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>';
        $('#notification').html(html);
        $(".success").slideDown('slow');

        notaficationBezar();
    </script>

<?php } ?>


    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>


<style>
    #kozepe_hatternek .kozepe #content a:hover {
        color: #010;
    }
</style>

<?php echo $column_left; ?><?php echo $column_right; ?>
    <div id="content"><?php echo $content_top; ?>

        <h1><img style="width: 20px" src="catalog/view/theme/default/image/arajanlat_icon.png" alt="" /> <?php echo $heading_title; ?></h1>



        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">

            <div class="fiok arajanlat">
                <a onclick="$('#form').attr('action', '<?php echo $copy; ?>'); $('#form').submit();" class="button"><?php echo $button_copy; ?></a>
                <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
            </div>

            <table class="list frontend">
                <thead>
                <tr>
                    <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

                    <td class="right"><?php if ($sort == 'arajanlat_id') { ?>
                            <a href="<?php echo $sort_azonosito; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_azonosito; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_azonosito; ?>"><?php echo $column_azonosito; ?></a>
                        <?php } ?>
                    </td>

                    <td class="right"><?php if ($sort == 'name') { ?>
                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                        <?php } ?>
                    </td>


                    <td class="right"><?php if ($sort == 'customer') { ?>
                            <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                        <?php } ?>
                    </td>


                    <td class="right"><?php if ($sort == 'date_added') { ?>
                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                        <?php } ?>
                    </td>
                    <td class="right"><?php if ($sort == 'date_modified') { ?>
                            <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                        <?php } ?>
                    </td>


                    <td class="right" nowrap><?php echo $column_action; ?></td>
                </tr>
                </thead>
                <tbody>
                <tr class="filter">
                    <td></td>
                    <td align="right"><input type="text" name="filter_azonosito_arajanlat"      value="<?php echo $filter_azonosito_arajanlat; ?>"      size="2" style="text-align: right;"/></td>
                    <td align="right"><input type="text" name="filter_name_arajanlat"           value="<?php echo $filter_name_arajanlat; ?>"           style="width: 100%;"/></td>
                    <td align="right"><input type="text" name="filter_customer_arajanlat"       value="<?php echo $filter_customer_arajanlat; ?>"       style="width: 100%"/></td>
                    <td align="right"><input type="text" name="filter_date_added_arajanlat"     value="<?php echo $filter_date_added_arajanlat; ?>"     size="8" class="date"/></td>
                    <td align="right"><input type="text" name="filter_date_modified_arajanlat"  value="<?php echo $filter_date_modified_arajanlat; ?>"  size="8" class="date"/></td>


                    <td align="right" nowrap><a  style="padding: 4px 17px; display: inline-block;" onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
                </tr>

                <?php if ($arajanlatok) { ?>
                    <?php foreach ($arajanlatok as $arajanlat) { ?>
                        <tr>
                            <td style="text-align: center;"><?php if ($arajanlat['selected']) { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $arajanlat['arajanlat_id']; ?>" checked="checked" />
                                <?php } else { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $arajanlat['arajanlat_id']; ?>" />
                                <?php } ?>
                            </td>
                            <td class="right"><?php echo $arajanlat['arajanlat_id']; ?></td>
                            <td class="right"><?php echo $arajanlat['name']; ?></td>
                            <td class="right"><?php echo $arajanlat['customer']; ?></td>
                            <td class="right"><?php echo $arajanlat['date_added']; ?></td>
                            <td class="right"><?php echo $arajanlat['date_modified']; ?></td>
                            <td class="right">
                                <?php foreach ($arajanlat['action'] as $action) { ?>
                                    <?php if(isset($action['image']) && !empty($action['image'])) { ?>
                                        <a href="<?php echo $action['href']; ?>"><img class="pdf_letoltes" src="<?php echo $action['image']; ?>" title="<?php echo $action['text']; ?>" /></a>
                                    <?php } else { ?>
                                        [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                    <?php } ?>
                                <?php } ?>
                            </td>                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td class="center elipsify" colspan="8"><?php echo $text_no_results; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </form>
        <div class="pagination"><?php echo $pagination; ?></div>
        <?php echo $content_bottom; ?></div>





    <script>
    $(document).ready(function(){
    // Confirm Delete

        $('#form').submit(function(){
            debugger;
            if ($(this).attr('action').indexOf('delete',1) != -1) {
                if (!confirm('<?php echo $text_delete?>')) {
                return false;
                }
            }
        });
    });


    </script>

    <script type="text/javascript"><!--
        function filter() {
            url = 'index.php?route=account/arajanlat';

            var filter_azonosito_arajanlat = $('input[name=\'filter_azonosito_arajanlat\']').attr('value');
            if (filter_azonosito_arajanlat) {
                url += '&filter_azonosito_arajanlat=' + encodeURIComponent(filter_azonosito_arajanlat);
            }

            var filter_name_arajanlat = $('input[name=\'filter_name_arajanlat\']').attr('value');
            if (filter_name_arajanlat) {
                url += '&filter_name_arajanlat=' + encodeURIComponent(filter_name_arajanlat);
            }

            var filter_customer_arajanlat = $('input[name=\'filter_customer_arajanlat\']').attr('value');
            if (filter_customer_arajanlat) {
                url += '&filter_customer_arajanlat=' + encodeURIComponent(filter_customer_arajanlat);
            }

            var filter_date_added_arajanlat = $('input[name=\'filter_date_added_arajanlat\']').attr('value');
            if (filter_date_added_arajanlat) {
                url += '&filter_date_added_arajanlat=' + encodeURIComponent(filter_date_added_arajanlat);
            }

            var filter_date_modified_arajanlat = $('input[name=\'filter_date_modified_arajanlat\']').attr('value');
            if (filter_date_modified_arajanlat) {
                url += '&filter_date_modified_arajanlat=' + encodeURIComponent(filter_date_modified_arajanlat);
            }



            location = url;
        }
        //--></script>
    <script type="text/javascript"><!--
        $('#form input').keydown(function(e) {
            if (e.keyCode == 13) {
                filter();
            }
        });

        $(document).ready(function() {
            $('.date').datepicker({dateFormat: 'yy-mm-dd'});
        });

        //--></script>
    <script type="text/javascript"><!--

        $('input[name=\'filter_name_arajanlat\']').autocomplete({
            delay: 500,
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=account/arajanlat/autocomplete&filter_name_arajanlat=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {
                        debugger;

                        response($.map(json, function(item) {
                            return {
                                label: item.name,
                                value: item.arajanlat_id
                            }
                        }));
                    },
                    error: function(e) {
                    debugger;
                }
                });
            },
            select: function(event, ui) {
                $('input[name=\'filter_name_arajanlat\']').val(ui.item.label);

                return false;
            },
            focus: function(event, ui) {
                return false;
            }
        });

        $('input[name=\'filter_customer_arajanlat\']').autocomplete({
            delay: 500,
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=account/arajanlat/autocomplete&filter_customer_arajanlat=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {
                        debugger;
                        response($.map(json, function(item) {
                            return {
                                label: item.customer,
                                value: item.arajanlat_id
                            }
                        }));
                    },
                    error: function(e) {
                        debugger;
                    }
                });
            },
            select: function(event, ui) {
                $('input[name=\'filter_customer_arajanlat\']').val(ui.item.label);

                return false;
            },
            focus: function(event, ui) {
                return false;
            }
        });
        //--></script>
<?php echo $footer; ?>