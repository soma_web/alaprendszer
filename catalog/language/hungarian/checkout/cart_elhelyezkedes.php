<?php
// Heading
$_['heading_title']          = 'Bevásárlókosár';

// Text
$_['text_success']           = 'Sikeresen beletette a(z) <a href="%s">%s</a> a <a href="%s">bevásárlókosarába</a>!';
$_['text_remove']            = 'Sikeresen módosította a bevásárlókosár tartalmát!';
$_['text_coupon']            = 'Sikeresen beváltotta a kupont!';
$_['text_voucher']           = 'Sikeresen beváltotta az ajándékutalványt!';
$_['text_reward']            = 'Sikeresen beváltotta a jutalompontokat!';
$_['text_shipping']          = 'Sikeresen kiszámolta a szállítási költséget!';
$_['text_login']             =  'Figyelem: Az árak megtekintéséhez kérem <a href="%s">lépjen be</a> vagy <a href="%s">regisztráljon </a> új fiókot!';
$_['text_points']            = 'Jutalompontok: %s';
$_['text_items']             = '%s tétel(ek) - %s';
$_['text_next']              = 'Mit szeretne következőnek tenni?';
$_['text_next_choice']       = 'Válasszon egy menüpontot, ha van kuponja, be szeretne váltani jutalompontokat vagy ki szeretné számolni a szállítási költséget.';
$_['text_use_coupon']        = 'Kupon beváltása';
$_['text_use_voucher']       = 'Ajándékutalvány beváltása';
$_['text_use_reward']        = 'Jutalompontok beváltása (elérhető %s)';
$_['text_shipping_estimate'] = 'Becsült szállítási költség';
$_['text_shipping_detail']   = 'Adja meg a szállítási címet a szállítási költség kiszámításához.';
$_['text_shipping_method']   = 'Válassza ki a kívánt szállítási módot.';
$_['text_empty']             = 'A bevásárlókosara üres!';

// Column
$_['column_image']	         = 'Kép';
$_['column_name']	         = 'Név';
$_['column_model']	         = 'Gyártmány';
$_['column_cikkszam']	     = 'Cikkszám';
$_['column_quantity']	     = 'Mennyiség';
$_['column_price']	         = 'Egységár';
$_['column_total']	         = 'Összeg';

// Entry
$_['entry_coupon']           = 'Adja meg a kupon kódját:';
$_['entry_voucher']          = 'Adja meg az ajándékutalvány kódját:';
$_['entry_reward']           = 'Pontok felhasználása (maximum %s):';
$_['entry_country']          = 'Ország:';
$_['entry_zone']             = 'Megye:';
$_['entry_postcode']         = 'Irányítószám:';

// Error
$_['error_stock']	         = 'A *** jellel megjelölt termékekből nem tudjuk biztosítani a kért mennyiséget, vagy nincsenek készleten!';
$_['error_minimum']	         = 'A %s minimális rendelhető mennyisége %s!';
$_['error_required']	     = '%s szükséges!';
$_['error_product']          = 'Figyelem: Nincsenek termékek a kosarában!';
$_['error_coupon']           = 'Figyelem: A kupon vagy érvénytelen, vagy lejárt, vagy elérte a felhasználási korlátot!';
$_['error_voucher']          = 'Figyelem: Az ajándékutalvány vagy érvénytelen, vagy az egyenleg fel lett használva!';
$_['error_reward']           = 'Figyelem: Kérem írja be a felhasználni kívánt jutalompontok számát!';
$_['error_points']           = 'Figyelem: Önnek nincs %s jutalompontja!';
$_['error_maximum']          = 'Figyelem: A felhasználható pontok maximális sáma: %s!';
$_['error_postcode']         = 'Az irányítószám 2-10 karakter között kell, hogy legyen!';
$_['error_country']          = 'Kérem válasszon országot!';
$_['error_zone']             = 'Kérem válasszon megyét/államot!';
$_['error_shipping']         = 'Figyelem: Szállítási mód szükséges!';
$_['error_no_shipping']      = 'Figyelem: Szállítási opciók nem elérhetőek. Kérem, segítségnyújtásért <a href="%s">vegye fel velünk a kapcsolatot</a>!';
$_['text_error_ures_id']     = 'Kérem válasszon elhelyezkedést, vagy kiemelést!';
$_['text_error_ures_idoszak'] = 'Kérem adja meg a mennyiséget!';
$_['ures_datum_kezdo']       = 'Kérem adja meg a kezdő dátumot!';
?>