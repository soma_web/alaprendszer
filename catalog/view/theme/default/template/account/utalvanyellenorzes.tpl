<?php echo $header; ?>
<?php if ($success) { ?>
    <div class="success"><?php echo $success; ?>
        <img src="catalog/view/theme/default/image/close.png" alt="" class="close" />
    </div>
<?php } ?>
<?php if ($error) { ?>
    <div class="warning"><?php echo $error; ?>
        <img src="catalog/view/theme/default/image/close.png" alt="" class="close" />
    </div>
<?php } ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <h1><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h2><?php echo $text_vonalkod; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><span class="required">*</span> <?php echo $entry_vonalkod; ?></td>
          <td><input type="text" name="vonalkod" value="<?php echo  $vonalkod; ?>" />
            <?php if ($error_vonalkod) { ?>
            <span class="error"><?php echo $error_vonalkod; ?></span>
            <?php } ?></td>
        </tr>
        <?php if ($success) { ?>
            <tr class="sikerult">
                <td><?php echo $entry_vonalkod_bevaltasa; ?></td>
                <td>
                    <input type="radio" name="vonalkod_bavaltasa" value="1" checked="checked"/><?php echo $text_yes; ?>
                    <input type="radio" name="vonalkod_bavaltasa" value="0" /><?php echo $text_no; ?>
                </td>
            </tr>
        <?php } ?>

      </table>
    </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
      <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button" /></div>
    </div>
  </form>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>