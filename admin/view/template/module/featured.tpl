<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
            <tr>
                <td><?php echo $entry_fejlec_latszik; ?></td>
                <td colspan=3><?php if ($featured_module_fejlec) { ?>
                        <input type="radio" name="featured_module_fejlec" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="featured_module_fejlec" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="featured_module_fejlec" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="featured_module_fejlec" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

        </table>

        <table class="form keres" id="myForm">
            <tr>
                <td><?php echo $entry_osszes; ?></td>
                <td colspan=3><?php if ($featured_module_osszes) { ?>
                        <input type="radio" name="featured_module_osszes" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="featured_module_osszes" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="featured_module_osszes" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="featured_module_osszes" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <?php $display = isset($featured_module_osszes) && $featured_module_osszes != "1" ? "display: none" : "display: table-row" ?>
            <tr class="osszes_nmb" style="<?php echo $display; ?>">
                <td><?php echo $entry_osszes_nmb; ?></td>
                <td>
                    <input type="text" name="featured_osszes_nmb" value="<?php echo isset($featured_osszes_nmb) ? $featured_osszes_nmb : 0; ?>" size="2" />
                </td>
            </tr>

        </table>

        <table class="form">


        <tr>
          <td><?php echo $entry_product; ?></td>
          <td><input type="text" name="product" value="" /></td>
          <td><input type="hidden" name="featured_module_store" value="0" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div id="featured-product" class="scrollbox">
              <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div id="featured-product<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product['name']; ?> <img src="view/image/delete.png" />
                    <input type="hidden" value="<?php echo $product['product_id']; ?>" />
                  </div>
              <?php } ?>
            </div>
            <input type="hidden" name="featured_product" value="<?php echo $featured_product; ?>" /></td>
        </tr>
      </table>
      <table id="module" class="list">
        <thead>
          <tr>
		    <td class="left"><?php echo $entry_limit; ?></td>
            <td class="left"><?php echo $entry_image; ?></td>
            <td class="left"><?php echo $entry_layout; ?></td>
            <td class="left"><?php echo $entry_position; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
            <td class="right"><?php echo $entry_sort_order; ?></td>
              <?php if ($this->config->get('megjelenit_korhinta') == 1) { ?>
                  <td class="right"><?php echo $entry_show; ?></td>
              <? }?>
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <tbody id="module-row<?php echo $module_row; ?>" class="sorok" >
          <tr>
		    <td class="left"><input type="text" name="featured_module[<?php echo $module_row; ?>][limit]" value="<?php echo $module['limit']; ?>" size="1" /></td>
            <td class="left"><input type="text" name="featured_module[<?php echo $module_row; ?>][image_width]" value="<?php echo $module['image_width']; ?>" size="3" />
              <input type="text" name="featured_module[<?php echo $module_row; ?>][image_height]" value="<?php echo $module['image_height']; ?>" size="3" />
              <?php if (isset($error_image[$module_row])) { ?>
              <span class="error"><?php echo $error_image[$module_row]; ?></span>
              <?php } ?></td>
            <td class="left"><select name="featured_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
            <td class="left"><select name="featured_module[<?php echo $module_row; ?>][position]">
                <?php if ($module['position'] == 'content_top') { ?>
                <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                <?php } else { ?>
                <option value="content_top"><?php echo $text_content_top; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'content_bottom') { ?>
                <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                <?php } else { ?>
                <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_left') { ?>
                <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                <?php } else { ?>
                <option value="column_left"><?php echo $text_column_left; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_right') { ?>
                <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                <?php } else { ?>
                <option value="column_right"><?php echo $text_column_right; ?></option>
                <?php } ?>
              </select></td>
            <td class="left"><select name="featured_module[<?php echo $module_row; ?>][status]">
                <?php if ($module['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
            <td class="right"><input type="text" name="featured_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>

              <?php if ($this->config->get('megjelenit_korhinta') == 1) { $colspan = 7; ?>
                <td class="right">
                  <select id="van_korhinta_<?php echo $module_row; ?>" class="animacio" name="featured_module[<?php echo $module_row; ?>][show]">
                      <?php if ($module['show'] == 0) { ?>
                          <option value="0" selected="selected"><?php echo $text_ures; ?></option>
                          <option value="1"><?php echo $text_korhinta; ?></option>

                      <?php } elseif ($module['show'] == 1) { ?>
                          <option value="0"><?php echo $text_ures; ?></option>
                          <option value="1" selected="selected"><?php echo $text_korhinta; ?></option>
                       <?php } ?>
                  </select>
                  <?php $display = isset($module['show']) && $module['show'] != "1" ? "display: none" : "display: block" ?>
                    <div class="van_korhinta_<?php echo $module_row; ?>_div" style="<?php echo $display; ?>;"><a id="korhinta_set_<?php echo $module_row; ?>" class="korhinta_settings button" >Beállítások</a></div>
                    <div class="korhinta_set_<?php echo $module_row; ?>_abs korhinta_set">
                        <div><?php echo $entry_van_korhinta_title; ?></div>
                        <div>
                            <div><?php echo $text_megjelenit; ?></div>
                            <input type="text"
                                   name="featured_module[<?php echo $module_row; ?>][megjelenit]"
                                   value="<?php echo isset($module['megjelenit']) ? $module['megjelenit'] : 0; ?>"
                                   size="2"/>
                        </div>
                        <div>
                            <div><?php echo $text_korhinta_auto; ?></div>
                            <?php if (isset($module['auto']) && $module['auto'] == "1") { ?>
                                <input type="checkbox"
                                       name="featured_module[<?php echo $module_row; ?>][auto]" value="1"
                                       checked="checked"/>
                            <?php } else { ?>
                                <input type="checkbox"
                                       name="featured_module[<?php echo $module_row; ?>][auto]" value="1"/>
                            <?php } ?>
                        </div>
                        <div>
                            <div><?php echo $text_korhinta_infi; ?></div>
                            <?php if (isset($module['infinity']) && $module['infinity'] == "1") { ?>
                                <input type="checkbox"
                                       name="featured_module[<?php echo $module_row; ?>][infinity]" value="1"
                                       checked="checked"/>
                            <?php } else { ?>
                                <input type="checkbox"
                                       name="featured_module[<?php echo $module_row; ?>][infinity]" value="1"/>
                            <?php } ?>
                        </div>
                        <div>
                            <div><?php echo $text_korhinta_speed; ?></div>
                            <input type="text"
                                   name="featured_module[<?php echo $module_row; ?>][speed]"
                                   value="<?php echo isset($module['speed']) ? $module['speed'] : 0; ?>"
                                   size="5"/>
                        </div>
                    </div>
                </td>
              <? } else { $colspan = 6;?>
                  <input type="hidden" name="featured_module[<?php echo $module_row; ?>][show]" value="0">
              <? } ?>

            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="<? echo $colspan?>"></td>
            <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
</div>
<script>

    $('.animacio').on('change', function() {
            classname = $(this).attr('id');
            arr = classname.split('_');
            if($(this).val() == '1') {
                $("."+classname+"_div").fadeIn(400);
            } else {
                $("."+classname+"_div").fadeOut(400);
                $(".korhinta_set_"+arr[2]+"_abs").fadeOut(400);
            }
    })

    $('.korhinta_settings').on('click', function() {
        classname = $(this).attr('id');
        if($("."+classname+"_abs").css('display') == 'none') {
            $("."+classname+"_abs").fadeIn(800);
        } else {
            $("."+classname+"_abs").fadeOut(800);
        }
    })

    $('#myForm input').on('change', function() {
        if ($('input[name=featured_module_osszes]:checked', '#myForm').val() == "1") {
            $(".osszes_nmb").fadeIn(1000);
        } else {
            $(".osszes_nmb").fadeOut(1000);
        }
    });
</script>

<script>

</script>

<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
		
	}, 
	select: function(event, ui) {
		$('#featured-product' + ui.item.value).remove();
		
		$('#featured-product').append('<div id="featured-product' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" /><input type="hidden" value="' + ui.item.value + '" /></div>');

		$('#featured-product div:odd').attr('class', 'odd');
		$('#featured-product div:even').attr('class', 'even');
		
		data = $.map($('#featured-product input'), function(element){
			return $(element).attr('value');
		});
						
		$('input[name=\'featured_product\']').attr('value', data.join());
					
		return false;
	}
});

$('#featured-product div img').on('click', function() {
	$(this).parent().remove();
	
	$('#featured-product div:odd').attr('class', 'odd');
	$('#featured-product div:even').attr('class', 'even');

	data = $.map($('#featured-product input'), function(element){
		return $(element).attr('value');
	});
					
	$('input[name=\'featured_product\']').attr('value', data.join());	
});
//--></script> 
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <<td class="left"><input type="text" name="featured_module[' + module_row + '][limit]" value="5" size="1" /></td>';
	html += '    <td class="left"><input type="text" name="featured_module[' + module_row + '][image_width]" value="80" size="3" /> <input type="text" name="featured_module[' + module_row + '][image_height]" value="80" size="3" /></td>';	
	html += '    <td class="left"><select name="featured_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="featured_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';
	html += '    <td class="left"><select name="featured_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="featured_module[' + module_row + '][sort_order]" value="" size="3" /></td>';

    html += '   <?php if ($this->config->get('megjelenit_korhinta') == 1) { $colspan = 7; ?>';
    html += '    <td class="right"><select name="featured_module[' + module_row + '][show]">';
    html += '     <option value="0" selected="selected"><?php echo $text_ures; ?></option>';
    html += '    <option value="1"><?php echo $text_mcs5; ?></option>';
    html += '    <option value="2"><?php echo $text_korhinta; ?></option>';
    html += '    </select></td>';
    html += '  <? } else { $colspan = 6;?>';
    html += '    <input type="hidden" name="featured_module[' + module_row + '][show]" value="0"  />';
    html += '    <? } ?> ';

	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script> 
<?php echo $footer; ?>