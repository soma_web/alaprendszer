<?php

// Heading
$_['heading_title']     	= 'Fogalom magyarázat';

// Text
$_['text_success']      	= 'Siker: A fogalom magyarázat frissítése megtörtént!';
$_['text_default']      	= 'Alapértelmezett';

// Column
$_['column_title']      	= 'Fogalom';
$_['column_sort_order'] 	= 'Sorrend';
$_['column_action']     	= 'Művelet';
$_['column_status']     	= 'Státusz';

// Entry
$_['entry_title']       	= 'Fogalom neve:';
$_['entry_description'] 	= 'Leírás:';

$_['entry_status']       	= 'Státusz:';

// Error
$_['error_warning']         = 'Kérjük gondosan ellenőrizze az űrlapon a hibákat!';
$_['error_permission']  	= 'Figyelmeztetés: Az információk módosítása az Ön számára nem engedélyezett!';
$_['error_title']       	= 'Az információ címe legalább 3, és legfeljebb 64 karakterből álljon!';
$_['error_description'] 	= 'A leírás legalább 3 karakterből álljon!';
?>
