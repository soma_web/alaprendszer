<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="location = '<?php echo $futottak; ?>'" class="button"><?php echo $button_futottak; ?></a>
                <a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
                <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
            </div>
        </div>
        <div style="margin-top: 10px; margin-left: 15px"> <?php echo  $text_cron_inditas ?></div>

        <div class="content">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="list">
                    <thead>
                        <tr>
                            <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>


                            <td class="left">
                                <?php if ($sort == 'cron_id') { ?>
                                    <a href="<?php echo $sort_cron_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_crond_id; ?></a>
                                <?php } else { ?>
                                    <a href="<?php echo $sort_cron_id; ?>"><?php echo $column_crond_id; ?></a>
                                <?php } ?>
                            </td>

                            <td class="left">
                                <?php if ($sort == 'program') { ?>
                                    <a href="<?php echo $sort_program; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_program; ?></a>
                                <?php } else { ?>
                                    <a href="<?php echo $sort_program; ?>"><?php echo $column_program; ?></a>
                                <?php } ?>
                            </td>

                            <td class="left">
                                <?php if ($sort == 'rendszeresseg') { ?>
                                    <a href="<?php echo $sort_rendszeresseg; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_rendszeresseg; ?></a>
                                <?php } else { ?>
                                    <a href="<?php echo $sort_rendszeresseg; ?>"><?php echo $column_rendszeresseg; ?></a>
                                <?php } ?>
                            </td>

                            <td class="left">
                                <?php if ($sort == 'status') { ?>
                                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                <?php } else { ?>
                                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                <?php } ?>
                            </td>

                            <td class="right"><?php echo $column_action; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($crons) { ?>
                            <?php foreach ($crons as $cron) { ?>
                                <tr>
                                    <td style="text-align: center;">
                                        <?php if ($cron['selected']) { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $cron['cron_id']; ?>" checked="checked" />
                                        <?php } else { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $cron['cron_id']; ?>" />
                                        <?php } ?>
                                    </td>
                                    <td class="left"><?php echo $cron['cron_id']; ?></td>

                                    <td class="left"><?php echo $cron['program']; ?></td>
                                    <td class="left"><?php echo $cron['rendszeresseg']; ?></td>
                                    <!--<td class="left"><?php echo $cron['status']; ?>-->


                                    <td class="left">
                                        <span class="list_ertek" onclick="listElemValueModify(this)"><?php echo $cron['status']; ?></span>

                                        <span class="list_modify" style="display: none">
                                            <select name="" modositas="<?php echo $cron['status_event']?>" onchange="$(this).blur();">
                                                <?php if (!empty($cron['status_ertek'])) { ?>
                                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                                    <option value="0"><?php echo $text_disabled; ?></option>
                                                <?php } else { ?>
                                                    <option value="1"><?php echo $text_enabled; ?></option>
                                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </td>

                                    </td>
                                    <td class="right">
                                        <?php foreach ($cron['action'] as $action) { ?>
                                            [ <a href="<?php echo $action['href']; ?>" <?php echo !empty($action['target']) ? 'target='.$action['target'] : ''?>><?php echo $action['text']; ?></a> ]
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
    </div>
</div>
<?php require_once('view/javascript/common.php')?>

<?php echo $footer; ?> 