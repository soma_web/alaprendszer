<?php
class ModelCronCrmMegrendelesSzamlazzKiegyenlitve extends Model {

	public function getOrders() {
        $vissza = array();

        $sql = "CREATE TABLE if not exists vtiger_salesorder_kiegyenlitve (
        `salesorderid`	            int(19),
        `sostatus`	        varchar(255) )
         engine=MyISAM default charset=UTF8";
        $query = $this->db_crm->query($sql);


        $sql = "SELECT * FROM vtiger_salesorder_kiegyenlitve";
        $query = $this->db_crm->query($sql);

        $query = $this->db_crm->query($sql);
        $vissza = $query->rows;

        return $vissza;
	}

    public function getOrder($salesorderid) {
        $vissza = array();



        $sql = "SELECT s.*, scf.cf_2496 AS szamlaszam FROM vtiger_salesorder s ";
        $sql .= " LEFT JOIN vtiger_salesordercf scf ON (s.salesorderid=scf.salesorderid) ";
        $sql .= " WHERE s.salesorderid='".$salesorderid."'";
        $query = $this->db_crm->query($sql);

        if ($query->row) {
            $vissza = $query->row;
        } else {
            $sql = "DELETE FROM vtiger_salesorder_kiegyenlitve WHERE salesorderid='".$salesorderid."'";
            $siker = $this->db_crm->query($sql);
            $this->load->model("cron/altalanos");

            $this->model_cron_altalanos->addFuttatasEredmeny('A megrendeles hiányik, a továbbítási listából törölve:'.$salesorderid);
            echo '<br>A megrendeles hiányik, a továbbítási listából törölve:'.$salesorderid;
        }

        return $vissza;
	}



    public function createSzamlaToCrm($data) {

        $this->load->model("cron/altalanos");

        $data['products']               = array();
        $data['sobillads']              = array();
        $data['soshipads']              = array();
        $data['inventoryshippingrel']   = array();


        $sql = "SELECT * FROM vtiger_inventoryproductrel WHERE id='".$data['salesorderid']."'";
        $query = $this->db_crm->query($sql);


        if ($query->rows) {
            $data['products'] = $query->rows;
        }

        $sql = "SELECT * FROM vtiger_sobillads WHERE sobilladdressid = '".$data['salesorderid']."'";
        $query = $this->db_crm->query($sql);
        if ($query->rows) {
            $data['sobillads'] = $query->row;
        }

        $sql = "SELECT * FROM vtiger_soshipads WHERE soshipaddressid = '".$data['salesorderid']."'";
        $query = $this->db_crm->query($sql);
        if ($query->rows) {
            $data['soshipads'] = $query->row;
        }

        $sql = "SELECT * FROM vtiger_inventoryshippingrel WHERE id = '".$data['salesorderid']."'";
        $query = $this->db_crm->query($sql);
        if ($query->rows) {
            $data['inventoryshippingrel'] = $query->row;
        }


        $crm_id = $this->model_cron_altalanos->getCrmId();
        $modtrack_id = $this->model_cron_altalanos->getModtrackerId();
        $invoice_no = $this->model_cron_altalanos->getCrmNo('Invoice');

        $sql = "INSERT INTO vtiger_invoice SET
                    `invoiceid`       = '".$crm_id."',
                    `subject`         = 'Kiegyenlített díjbekérő',
                    `salesorderid`    = '".$data['salesorderid']."',
                    `customerno`      = '".$data['customerno']."',
                    `contactid`       = '".$data['contactid']."',
                    `notes`           = '',
                    `invoicedate`     = CURDATE(),
                    `duedate`         = CURDATE(),
                    `invoiceterms`    = '',
                    `type`            = '',
                    `adjustment`      = '".$data['adjustment']."',
                    `salescommission` = '".$data['salescommission']."',
                    `exciseduty`      = '".$data['exciseduty']."',
                    `subtotal`        = '".$data['subtotal']."',
                    `total`           = '".$data['total']."',
                    `taxtype`         = '".$data['taxtype']."',
                    `discount_percent`= '".$data['discount_percent']."',
                    `discount_amount` = '".$data['discount_amount']."',
                    `s_h_amount`      = '".$data['s_h_amount']."',
                    `shipping`        = '',
                    `accountid`       = '".$data['accountid']."',
                    `terms_conditions`= '".$data['terms_conditions']."',
                    `purchaseorder`   = '".$data['purchaseorder']."',
                    `invoicestatus`   = 'Számla',
                    `invoice_no`      = '".$invoice_no."',
                    `currency_id`     = '".$data['currency_id']."',
                    `conversion_rate` = '".$data['conversion_rate']."',
                    `pre_tax_total`   = '".$data['pre_tax_total']."',
                    `received`        = '0',
                    `balance`         = '".$data['total']."',
                    `s_h_percent`     = '".$data['s_h_percent']."'";
        $siker = $this->db_crm->query($sql);


        $sql = "INSERT INTO vtiger_invoicecf SET
                            invoiceid    = '".$crm_id."',
                            cf_1111         = '0',
                            cf_2433         = 'Továbbításra vár',
                            cf_2439         = CURDATE(),
                            cf_2443         = 'Kiegyenlített',
                            cf_2494         = ''";
        $siker = $this->db_crm->query($sql);

        $sql = "INSERT INTO vtiger_crmentity SET
                            crmid           = '".$crm_id."',
	                        smcreatorid     = '20',
                            smownerid       = '20',
                            modifiedby      = '20',
                            setype          = 'Invoice',
                            description	    = '',
                            createdtime	    = NOW(),
                            modifiedtime    = NOW(),
                            viewedtime      = '',
                            status          = '',
                            version         = '0',
                            presence        = '1',
                            deleted         = '0',
                            label           = 'Díjbekérő - kiegyenlítve',
                            cf_view         = '',
                            cf_viewname	    = ''";
        $siker = $this->db_crm->query($sql);

        $sql = "INSERT INTO vtiger_inventoryshippingrel SET
                          id        = '".$crm_id."',
                          shtax1    = '".(!empty($data['inventoryshippingrel']['shtax1']) ? $data['inventoryshippingrel']['shtax1'] : '')."',
                          shtax2    = '".(!empty($data['inventoryshippingrel']['shtax2']) ? $data['inventoryshippingrel']['shtax2'] : '')."',
                          shtax3    = '".(!empty($data['inventoryshippingrel']['shtax3']) ? $data['inventoryshippingrel']['shtax3'] : '')."'";
        $siker = $this->db_crm->query($sql);

        $sql = "INSERT INTO vtiger_modtracker_basic SET
                            id          = '".$modtrack_id."',
                            crmid       = '".$crm_id."',
                            `module`    = 'Invoice',
                            whodid      = '20',
                            changedon   = NOW(),
                            status      = '2'";
        $siker = $this->db_crm->query($sql);


        $sql = "INSERT INTO vtiger_invoicebillads SET
                          invoicebilladdressid  = '".$crm_id."',
                          bill_city             = '".(!empty($data['sobillads']['bill_city'])   ? $data['sobillads']['bill_city'] : '')."',
                          bill_code             = '".(!empty($data['sobillads']['bill_code'])   ? $data['sobillads']['bill_code'] : '')."',
                          bill_country          = '".(!empty($data['sobillads']['bill_country']) ? $data['sobillads']['bill_country'] : '')."',
                          bill_state            = '".(!empty($data['sobillads']['bill_state'])  ? $data['sobillads']['bill_state'] : '')."',
                          bill_street           = '".(!empty($data['sobillads']['bill_street']) ? $data['sobillads']['bill_street'] : '')."',
                          bill_pobox            = '".(!empty($data['sobillads']['bill_pobox'])  ? $data['sobillads']['bill_pobox'] : '')."'";
        $siker = $this->db_crm->query($sql);


        $sql = "INSERT INTO vtiger_invoiceshipads SET
                          invoiceshipaddressid  = '".$crm_id."',
                          ship_city             = '".(!empty($data['soshipads']['ship_city'])   ? $data['soshipads']['ship_city'] : '')."',
                          ship_code             = '".(!empty($data['soshipads']['ship_code'])   ? $data['soshipads']['ship_code'] : '')."',
                          ship_country          = '".(!empty($data['soshipads']['ship_country']) ? $data['soshipads']['ship_country'] : '')."',
                          ship_state            = '".(!empty($data['soshipads']['ship_state'])  ? $data['soshipads']['ship_state'] : '')."',
                          ship_street           = '".(!empty($data['soshipads']['ship_street']) ? $data['soshipads']['ship_street'] : '')."',
                          ship_pobox            = '".(!empty($data['soshipads']['ship_pobox'])  ? $data['soshipads']['ship_pobox'] : '')."'";
        $siker = $this->db_crm->query($sql);

        if (!empty($data['products'])) {
            foreach($data['products'] as $key=>$product) {
                $lineitem_id = $this->model_cron_altalanos->getLineItemId();
                $sql = "INSERT INTO vtiger_inventoryproductrel SET
                            id                = '".$crm_id."',
                            productid         = '".$product['productid']."',
                            sequence_no       = '".($key+1)."',
                            quantity          = '".$product['quantity']."',
                            listprice         = '".$product['listprice']."',
                            discount_percent  = '".$product['discount_percent']."',
                            discount_amount   = '".$product['discount_amount']."',
                            comment           = '".$product['comment']."',
                            description	      = '".$product['description']."',
                            incrementondel    = '".$product['incrementondel']."',
                            lineitem_id       = '".$lineitem_id."',
                            tax1              = '".$product['tax1']."',
                            tax2              = '".$product['tax2']."',
                            tax3              = '".$product['tax3']."',
                            tax4              = '".$product['tax4']."',
                            tax5              = '".$product['tax5']."'";
                $siker = $this->db_crm->query($sql);
            }
        }
        return $crm_id;

    }

    public function deleteTriggerOrder($data) {
        $sql = "DELETE FROM vtiger_salesorder_kiegyenlitve
          WHERE salesorderid='".$data['salesorderid']."'";
        $query = $this->db_crm->query($sql);
    }

    public function createNewInvoice($invoiceid,$invoicestatus) {
        $sql = "INSERT INTO vtiger_invoice_szamla SET
                                invoiceid       = '".$invoiceid."',
                                invoicestatus   = '".$invoicestatus."'";
        $siker = $this->db_crm->query($sql);
    }




}
?>