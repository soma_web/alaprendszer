<?php echo $header; ?>
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

    <h1><?php echo $text_transaction_title; ?></h1>

    <?php if(isset($date)) { ?> <strong>Tranzakció időpontja:</strong> <?php echo $date; ?><br /> <?php } ?>
    <?php if(isset($payrefno)) { ?> <strong>PayU tranzakció azonosító:</strong> <?php echo $payrefno; ?><br /> <?php } ?>
    <?php if(isset($amount)) { ?> <strong>Tranzakció összege:</strong> <?php echo $amount; ?><br /><br /> <?php } ?>

    <!--<?php if(isset($RT)) { ?> <strong>RT:</strong> <?php //echo $RT; ?><br /><br /> <?php } ?>-->
    <!--<?php if(isset($RC)) { ?> <strong>RC:</strong> <?php //echo $RC; ?><br /><br /> <?php } ?>-->

    <?php if(isset($err)) { ?> <strong>Hiba:</strong> <?php echo $err; ?><br /><br /> <?php } ?>

    <?php echo $text_message; ?>


    <div class="buttons">
        <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
    </div>
    <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>