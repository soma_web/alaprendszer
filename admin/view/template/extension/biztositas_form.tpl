<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error) { ?>
  <div class="warning"><?php echo $error; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="buttons">
            <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
            <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
        </div>

    </div>
    <div class="content">

        <table class="list">
            <thead>
            <tr>
                <td class="left"><?php echo $column_from; ?></td>
                <td class="left"><?php echo $column_to; ?></td>
                <td class="left"><?php echo $column_value; ?></td>
            </tr>
            </thead>
            <tbody>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

            <? if (isset($modosit)) {?>
                <td class="left"><input type="text" name="biztositas_tol" value="<? echo $modosit[0]?>"></td>
                <td class="left"><input type="text" name="biztositas_ig"  value="<? echo $modosit[1]?>"></td>
                <td class="left"><input type="text" name="biztositas_ertek"  value="<? echo $modosit[2]?>"></td>
                <input type="hidden" name="id"  value="<? echo $modosit[3]?>">
            <? } else {?>
                <td class="left"><input type="text" name="biztositas_tol" ></td>
                <td class="left"><input type="text" name="biztositas_ig"  ></td>
                <td class="left"><input type="text" name="biztositas_ertek" ><td>
            <? }?>

            </form>
            </tbody>

        </table>

    </div>
  </div>
</div>
<?php echo $footer; ?>