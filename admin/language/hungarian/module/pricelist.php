<?php
// Heading
$_['heading_title']    = 'Price List';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified module Price List!';

// Entry
$_['entry_customer_group'] = 'Customer Group:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Price List!';
?>