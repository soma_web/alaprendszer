<?php
// Heading
$_['heading_title']       = 'Store selection index';

// Text
$_['text_module']         = 'Modules';
$_['text_slider']         = 'Slider settings';
$_['text_slider_use']     = 'Use Slider';
$_['text_success']        = 'Success: You have modified module store!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_admin']       = 'Admin Users Only:';
$_['entry_mirror']      = 'Mirror Picture:';
$_['entry_opacity']     = 'Opacity of last visible item (0.0 - 1.0):';
$_['entry_speed']       = 'Scrolling speed (0.0 (off) - 1.0):';
$_['entry_around']      = 'Wrap around:';
$_['entry_title']       = 'Image title:';
$_['entry_layout']      = 'Layout:';
$_['entry_position']    = 'Position:';
$_['entry_status']      = 'Status:';
$_['entry_sort_order']  = 'Sort Order:';
$_['entry_width']       = 'Store image width:';
$_['entry_height']      = 'Store image height:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module store!';
?>