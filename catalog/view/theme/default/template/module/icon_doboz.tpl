  <ul>
      <?php foreach ($categories as $category) { ?>
          <li><a class="kategoria" href="<?php echo $category['href']; ?>" style="max-width: <?php echo $icon_width?>px">
              <img class="kategoria-kep" src="<?php echo $category['image_icon']; ?>">
                  <br>
                  <span class="kategoria-nev"><?php echo $category['name']; ?></span>
              </a>
              <?php if ($category['children']) { ?>
                  <div>
                      <?php for ($i = 0; $i < count($category['children']);) { ?>
                          <ul>
                              <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
                              <?php for (; $i < $j; $i++) { ?>
                                  <?php if (isset($category['children'][$i])) { ?>
                                      <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
                                  <?php } ?>
                              <?php } ?>
                          </ul>
                      <?php } ?>
                  </div>
              <?php } ?>
          </li>
      <?php } ?>
  --></ul>