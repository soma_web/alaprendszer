<?php
class ModelModulePriceAlert extends Model {
	public function addAlert($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "product_price_alert
				SET name = '" . $this->db->escape($data['price_alert_name']) . "',
					email = '" . $this->db->escape($data['price_alert_email']) . "',
					product_id = '" . (int)$data['price_alert_product_id'] . "',
					product_price = '" . (float)$data['price_alert_product_price'] . "',
					desired_price = '" . (float)$data['price_alert_desired_price'] . "',
					currency_code = '" . $this->db->escape($data['price_alert_currency_code']) ."',
					customer_group_id = '" . (int)$data['price_alert_customer_group_id'] . "',
					language_id = '" . (int)$this->config->get('config_language_id') . "',
					date_added = NOW()";
				
		$this->db->query($sql);	
		
		$alert_id = $this->db->getLastId();
		
		if ($this->config->get('price_alert_admin_notification')) {
			$this->sendNotificationToAdmin($alert_id);	
		}
	}
	
	public function editAlert($alert_id, $data) {
		$sql = "UPDATE " . DB_PREFIX . "product_price_alert
				SET name = '" . $this->db->escape($data['price_alert_name']) . "',
					product_price = '" . (float)$data['price_alert_product_price'] . "',
					desired_price = '" . (float)$data['price_alert_desired_price'] . "',
					currency_code = '" . $this->db->escape($data['price_alert_currency_code']) ."',
					customer_group_id = '" . (int)$data['price_alert_customer_group_id'] . "'
				WHERE alert_id = '" . (int)$alert_id . "'";
		
		$this->db->query($sql);	
	}
	
	public function getAlertIdByEmailAndProductId($data) {
		$sql = "SELECT alert_id FROM " . DB_PREFIX . "product_price_alert
				WHERE UPPER(email) = '" . $this->db->escape(strtoupper($data['price_alert_email'])) . "'
				AND product_id = '" . (int)$data['price_alert_product_id'] . "'
				LIMIT 0,1";
				
		$query = $this->db->query($sql);

		if ($query->num_rows) {
			return $query->row['alert_id'];
		} else {
			return 0;
		}		
	}
	
	public function getAlertInfo($alert_id) {
		$sql = "SELECT ppa.*, pd.name AS product_name FROM " . DB_PREFIX . "product_price_alert ppa 
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (ppa.product_id = pd.product_id)
				WHERE ppa.alert_id = '" . (int)$alert_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row;
	}
	
	private function sendNotificationToAdmin($alert_id) {
		$alert_info = $this->getAlertInfo($alert_id);
		
		if ($alert_info) {
			$find = array(
				'{customer_name}',
				'{customer_email}',
				'{product_name}',
				'{product_name_with_link}',
				'{product_price}',
				'{desired_price}'
			);
			
			$replace = array(
				'customer_name'  		 => $alert_info['name'],
				'customer_email' 		 => $alert_info['email'],
				'product_name'   		 => $alert_info['product_name'],
				'product_name_with_link' => '<a href="' . $this->url->link('product/product', 'product_id=' . $alert_info['product_id']) . '">' . $alert_info['product_name'] . '</a>',
				'product_price'  		 => $this->currency->format($alert_info['product_price'], $alert_info['currency_code']),
				'desired_price'  		 => $this->currency->format($alert_info['desired_price'], $alert_info['currency_code']),
			);
			
			$admin_mail_templates = $this->config->get('price_alert_admin_mail');
			$admin_mail_template = $admin_mail_templates[$this->config->get('config_language_id')];
			
			$subject = str_replace($find, $replace, html_entity_decode($admin_mail_template['subject'], ENT_QUOTES, 'UTF-8'));
			$message = str_replace($find, $replace, html_entity_decode($admin_mail_template['message'], ENT_QUOTES, 'UTF-8'));
			
			$template = new Template();
			
			$template->data['title'] = $subject;
			$template->data['message'] = $message;
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/price_alert_admin.tpl')) {
				$html = $template->fetch($this->config->get('config_template') . '/template/mail/price_alert_admin.tpl');
			} else {
				$html = $template->fetch('default/template/mail/price_alert_admin.tpl');
			}
			
			$mail = new Mail(); 
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');	
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setTo($this->config->get('config_email'));
			$mail->setSubject($subject);
			$mail->setHtml($html);
			$mail->send();
		}
	}
	
	// cron functions
	
	public function getPriceAlerts() {
		$sql = "SELECT * FROM " . DB_PREFIX . "product_price_alert ppa 
				LEFT JOIN " . DB_PREFIX . "product p ON (ppa.product_id = p.product_id)
				WHERE p.status = 1";
				
		$query = $this->db->query($sql);

		return $query->rows;	
	}
	
	public function addHistory($alert, $html_email) {	
		$sql = "INSERT INTO " . DB_PREFIX . "product_price_alert_history 
				SET name          = '" . $this->db->escape($alert['name']) . "',
					email         = '" . $this->db->escape($alert['email']) . "',
					email_message = '" . $this->db->escape($html_email) . "',
					date_added    = NOW()";
		
		$this->db->query($sql);
	}
	
	public function getHistory($history_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "product_price_alert_history WHERE history_id = '" . (int)$history_id . "' LIMIT 0,1";
		
		$query = $this->db->query($sql);
		
		return $query->row;
	}
	
	public function deleteAlerts($alert_ids) {
		$sql = "DELETE FROM " . DB_PREFIX . "product_price_alert WHERE alert_id IN (" . implode(",", $alert_ids) . ")";
		$this->db->query($sql);
	}
}
?>