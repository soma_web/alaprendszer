<?php
// Heading
$_['heading_title']     = 'Gyakran ismételt kérdések';
 
// Text
$_['text_tabQuestions']      = 'Kérdések';
$_['text_tabCategories']    = 'Kategóriák';
 
$_['text_deleteQuestions']    = 'Kérdés törlése';
$_['text_addQuestion']    = 'Kérdés hozzáadása';
$_['text_search']    = 'Keresés:';
$_['text_resetSearch']    = 'Keresés törlése';
 
$_['text_deleteCategories']    = 'Kategória törlése';
$_['text_addCategory']    = 'Kategória hozzáadása';
 
$_['text_noQuestions']    = 'Nincs kérdés';
$_['text_noCategories']    = 'Nincs kategória';
 
$_['text_save']    = 'Mentés';
$_['text_questions']    = 'Kérdések';
$_['text_categories']    = 'Kategóriák';
$_['text_nameCategory']    = 'Kategória neve';
 
$_['text_label0or1Question']    = ' kérdés';
$_['text_labelmore1Question']    = ' kérdés';
 
$_['text_titleAddQuestion']    = 'Kérdés hozzáadása';
$_['text_titleAddCategory']    = 'Kategória hozzáadása';
 
// Column
$_['column_category']       = 'Kategória';
$_['column_question']     = 'Kérdés';
$_['column_response']     = 'Válasz';
$_['column_action']     = 'Művelet';
 
// Error
$_['error_deleteQuestions']  = 'Nincs törölhető kérdés';
$_['error_deleteCategories']  = 'Nem lehet törölni';
$_['error_allFieldsRequired']  = 'Minden mezőt ki kell tölteni!';
$_['error_permission']  = 'Figyelmeztetés: Nincs jogosultságod módosítani!';
 
//Success
$_['success_deleteQuestions']  = 'Kérdés törölve';
$_['success_deleteCategories']  = 'Kategória törölve';
$_['success_addQuestion']  = 'Kérdés hozzáadva';
$_['success_addCategory']  = 'Kategória hozzáadva';
$_['success_modifyQuestion']  = 'Kérdés módosítva';
$_['success_modifyCategory']  = 'Kategória módosítva';

?>