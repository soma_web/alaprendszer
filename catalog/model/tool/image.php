<?php
class ModelToolImage extends Model {
	public function resize($filename, $width, $height,$watermark=true, $eredeti_meret=false) {
		if (!file_exists(DIR_IMAGE . $filename) || !is_file(DIR_IMAGE . $filename)) {
			return;
		}

        if ($this->config->get('megjelenit_vizjel') == 0){
            $watermark = false;
        }
		$info = pathinfo($filename);
		$extension = $info['extension'];
        if($extension == 'pdf') {
            $extension = 'jpg';
        }

        $megjelenit_altalanos = $this->config->get('megjelenit_altalanos');
		$old_image = $filename;
        if (isset($megjelenit_altalanos['megjelenit_popup_eredeti_meret']) &&  $megjelenit_altalanos['megjelenit_popup_eredeti_meret'] == 1) {
            if ($eredeti_meret && $megjelenit_altalanos['megjelenit_popup_eredeti_meret'] == 1) {
                $meret  = getimagesize(DIR_IMAGE . $old_image);
                $width  = $meret[0];
                $height = $meret[1];
            }
        }

		$new_image = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;


        if ($watermark) {
            $new_water = 'cache/' . 'watermark' . '-' . $width . 'x' . $height . '.' . 'png';
            if ($this->config->get('config_vizjel_icon') && file_exists(DIR_IMAGE . $this->config->get('config_vizjel_icon'))) {
                $vizjel_icon = DIR_IMAGE . $this->config->get('config_vizjel_icon');
                if ($filename == "no_image.jpg") {
                    $watermark = false;
                }
            } else {
                $watermark = false;
            }
        }

		if ((!file_exists(DIR_IMAGE . $new_image) || (filemtime(DIR_IMAGE . $old_image) > filemtime(DIR_IMAGE . $new_image)))
            || ($watermark && (!file_exists(DIR_IMAGE . $new_water) || (filemtime($vizjel_icon) > filemtime(DIR_IMAGE . $new_water)))) ) {
			$path = '';
			
			$directories = explode('/', dirname(str_replace('../', '', $new_image)));
			
			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;
				
				if (!file_exists(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}		
			}


            $image = new Image(DIR_IMAGE . $old_image);

            if($watermark){

                $image->resize($width, $height);
                $image->save(DIR_IMAGE . $new_image);

                $new_water = 'cache/' . 'watermark' . '-' . $width . 'x' . $height . '.' . 'png';

                if (!file_exists(DIR_IMAGE . $new_water) || (filemtime($vizjel_icon) > filemtime(DIR_IMAGE . $new_water))) {

                    $meret  = getimagesize($vizjel_icon);
                    $width_watermark   = $meret[0] == $width  ? $width-1  : $width;
                    $height_watermark  = $meret[1] == $height ? $height-1 : $height;

                    $image_watermark = new Image($vizjel_icon);
                    $image_watermark->resize($width_watermark,$height_watermark);
                    $image_watermark->save(DIR_IMAGE. $new_water);
                }

                $image_watermark_merge = new Image(DIR_IMAGE . $new_image);
                $image_watermark_merge->watermark(DIR_IMAGE . $new_water, 'center');

            } else {

                $image->resize($width, $height);
                $image->save(DIR_IMAGE . $new_image);
            }
        }


        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			return HTTPS_IMAGE . $new_image;
		} else {
			return HTTP_IMAGE . $new_image;
		}	
	}
}
?>