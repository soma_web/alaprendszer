<link rel="stylesheet"  href="catalog/view/javascript/perfect-scrollbar/src/perfect-scrollbar.css" >
<script src="catalog/view/javascript/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight|| e.clientHeight|| g.clientHeight;
        if(x > 960 || y > 960) {
            $('.category-center').perfectScrollbar({
                wheelPropagation: true,
                suppressScrollX: true,
                suppressScrollY: false,
                useBothWheelAxes: false,
                includePadding: true
            });
        }
    });
</script>    <?php if (isset($heading_title) && $description_show) { ?>
        <h1><?php echo $heading_title; ?></h1>
    <?php } ?>
    <div class="box-product">
        <?php foreach($categories as $category) { ?>
            <div class="category-center" style="<?php echo $box_magassag ?>;   position: relative; ">
                <?php if ($category['thumb']) { ?>
                    <a href="<? echo $category['href']?>" class="image"><img style="vertical-align: middle; max-width: <?php echo $category['thumb_image_width']; ?>px" src="<?php echo $category['thumb']; ?>" alt="<?php echo $heading_title; ?>" /></a>
                <?php } ?>
                <?php if ($category['name']) { ?>
                    <div class="name" ><a href="<? echo $category['href']?>"><?php echo $category['name']; ?></a></div>
                <?php } ?>
                <?php if ($category['description'] && $description_show) { ?>
                    <a href="<? echo $category['href']?>" ><?php echo $category['description']; ?></a>
                <?php } ?>
                <?php if (isset($button_bovebben) && $button_bovebben) { ?>
                    <a href="<?php echo $category['href']; ?>">
                        <input  style="margin 0 auto" type="button" value="<?php echo $button_bovebben; ?>" class="button nepszeru_button_cart" />
                    </a>
                <?php } ?>
                <?php foreach($category['children'] as $children) { ?>
                    <div class="category-center-children">
                        <?php if ($children['name']) { ?>
                            <div class="name"><a href="<? echo $children['href']?>"><?php echo $children['name']; ?></a></div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>




