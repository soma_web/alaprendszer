<?php
class ModelAccountProduct extends Model {
	public function addProduct($data) {
         $language = $this->config->get("config_language_id");



         $model                 =  isset($data['model'])            ?  $this->db->escape($data['model'])    : "";
		 $sku                   =  isset($data['sku'])              ?  $this->db->escape($data['sku'])      : "";
		 $upc                   =  isset($data['upc'])              ?  $this->db->escape($data['upc'])      : "";

		 $location              =  isset($data['location'])         ?  $this->db->escape($data['location']) : "";
		 $quantity              =  isset($data['quantity'])         ?  (int)$data['quantity']               : 0;
		 $minimum               =  isset($data['minimum'])          ?  (int)$data['minimum']                : 0;
		 $subtract              =  isset($data['subtract'])         ?  (int)$data['subtract']               : 0;
		 $stock_status_id       =  isset($data['stock_status_id'])  ?  (int)$data['stock_status_id']        : 0;
		 $date_available        =  isset($data['date_available'])   ?  $this->db->escape($data['date_available']) : "";
		 $manufacturer_id       =  isset($data['manufacturer_id'])  ?  (int)$data['manufacturer_id']        : 0;
		 $shipping              =  isset($data['shipping'])         ?  (int)$data['shipping']               : 0;
		 $price                 =  isset($data['price'])            ?  (float)$data['price']                : 0;
		 $eredeti_ar            =  isset($data['eredeti_ar'])       ?  (float)$data['eredeti_ar']           : 0;
		 $szazalek              =  isset($data['szazalek'])         ?  (float)$data['szazalek']             : 0;
		 $utalvany              =  isset($data['utalvany'])         ?  (float)$data['utalvany']             : 1;
		 $weight                =  isset($data['weight'])           ?  (float)$data['weight']               : 0;
		 $weight_class_id       =  isset($data['weight_class_id'])  ?  (int)$data['weight_class_id']        : 0;
		 $length                =  isset($data['length'])           ?  (float)$data['length']               : 0;
		 $width                 =  isset($data['width'])            ?  (float)$data['width']                : 0;
		 $height                =  isset($data['height'])           ?  (float)$data['height']               : 0;
		 $length_class_id       =  isset($data['length_class_id'])  ?  (int)$data['length_class_id']        : 0;
         $points                =  isset($data['points'])           ?  (int)$data['points']                 : 0;
		 $status                =  isset($data['status'])           ?  (int)$data['status']                 : 0;
		 $tax_class_id          =  isset($data['tax_class_id'])     ?  $this->db->escape($data['tax_class_id']) : "";
		 $sort_order            =  isset($data['sort_order'])       ?  (int)$data['sort_order']             : 0;
		 $cikkszam              =  isset($data['cikkszam'])         ?  $this->db->escape($data['cikkszam']) : "";
		 $megyseg               =  isset($data['megyseg'])          ?  $this->db->escape($data['megyseg'])  : "";
		 $csomagolasi_egyseg    =  isset($data['csom_egyseg'])      ?  (int)$data['csom_egyseg']            : 0;
		 $csomagolasi_mennyiseg =  isset($data['csom_mennyiseg'])   ?  (int)$data['csom_mennyiseg']         : 0;
		 $date_ervenyes_ig      =  isset($data['date_ervenyes_ig']) ?  $this->db->escape($data['date_ervenyes_ig']) : "";
         $imagedesabled         =  isset($data['imagedesabled'])    ?  $data['imagedesabled']               : 0;

        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET
		 model                  = '" . $model . "',
		 sku                    = '" . $sku . "',
		 upc                    = '" . $upc . "',
		 location               = '" . $location . "',
		 quantity               = '" . $quantity . "',
		 minimum                = '" . $minimum . "',
		 subtract               = '" . $subtract . "',
		 stock_status_id        = '" . $stock_status_id . "',
		 date_available         = '" . $date_available . "',
		 manufacturer_id        = '" . $manufacturer_id . "',
		 shipping               = '" . $shipping . "',
		 price                  = '" . $price . "',
		 eredeti_ar             = '" . $eredeti_ar . "',
		 szazalek               = '" . $szazalek . "',
		 utalvany               = '" . $utalvany . "',
		 weight                 = '" . $weight . "',
		 weight_class_id        = '" . $weight_class_id . "',
		 length                 = '" . $length . "',
		 width                  = '" . $width . "',
		 height                 = '" . $height . "',
		 length_class_id        = '" . $length_class_id . "',
		 imagedesabled          = '" . $imagedesabled . "',
		 date_added = NOW(),
         points                 = '" . $points . "',
		 status                 = '" . $status . "',
		 tax_class_id           = '" . $tax_class_id . "',
		 sort_order             = '" . $sort_order . "',
		 cikkszam               = '" . $cikkszam . "',
		 megyseg                = '" . $megyseg . "',
		 csomagolasi_egyseg     = '" . $csomagolasi_egyseg . "',
		 csomagolasi_mennyiseg  = '" . $csomagolasi_mennyiseg . "',
		 date_ervenyes_ig       = '" . $date_ervenyes_ig . "'");

		$product_id = $this->db->getLastId();
		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET
			image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE product_id = '" . (int)$product_id . "'");
		}
		if (isset($data['letoltheto'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET
			letoltheto = '" . $this->db->escape(html_entity_decode($data['letoltheto'], ENT_QUOTES, 'UTF-8')) . "' WHERE product_id = '" . (int)$product_id . "'");
		}
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_customer SET
		    product_id = '" . (int)$product_id . "',
		    customer_id = '" . (int)$this->customer->getId() . "'");


        $name                  =  isset($data['product_description'][$language]['name'])              ?  $this->db->escape($data['product_description'][$language]['name']) : "";
        $meta_keyword          =  isset($data['product_description'][$language]['meta_keyword'])      ?  $this->db->escape($data['product_description'][$language]['meta_keyword']) : "";
        $meta_description      =  isset($data['product_description'][$language]['meta_description'])  ?  $this->db->escape($data['product_description'][$language]['meta_description']) : "";
        $description           =  isset($data['product_description'][$language]['description'])       ?  $this->db->escape($data['product_description'][$language]['description']) : "";

        foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET
			    product_id              = '" . (int)$product_id . "',
			    language_id             = '" . (int)$language_id . "',
			    name                    = '" . $name . "',
			    meta_keyword            = '" . $meta_keyword . "',
			    meta_description        = '" . $meta_description . "',
			    description             = '" . $description . "'
			    ");
            /*tag = '" . $this->db->escape($value['tag']) . "'*/

		}
		
		if (isset($data['product_store'])) {
			foreach ($data['product_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

        if (isset($data['product_elhelyezkedes'])) {
            foreach ($data['product_elhelyezkedes'] as $key=>$elhelyezkedes) {
                if ($key !== "NULL") {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_elhelyezes SET
                    `product_id`                            = '" . (int)$product_id . "',
	                `elhelyezkedes_id`	                    = '" . (int)$elhelyezkedes['elhelyezkedes_id'] . "',
	                `elhelyezkedes_neve`	                = '" . $elhelyezkedes['elhelyezkedes_neve'] . "',
	                `elhelyezkedes_netto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_netto_ara'] . "',
	                `elhelyezkedes_brutto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_brutto_ara'] . "',

	                `elhelyezkedes_alcsoport_id`	        = '" . (int)$elhelyezkedes['elhelyezkedes_alcsoport_id'] . "',
	                `elhelyezkedes_alcsoport_neve`	        = '" . $elhelyezkedes['elhelyezkedes_alcsoport_neve'] . "',
	                `elhelyezkedes_alcsoport_netto_ara` 	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_netto_ara'] . "',
	                `elhelyezkedes_alcsoport_brutto_ara`	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'] . "',

	                `kiemelesek_id`                         = '" . (int)$elhelyezkedes['kiemelesek_id'] . "',
	                `kiemelesek_neve`                       = '" . $elhelyezkedes['kiemelesek_neve'] . "',
	                `kiemelesek_netto_ara`                  = '" . $elhelyezkedes['kiemelesek_netto_ara'] . "',
	                `kiemelesek_brutto_ara`                 = '" . $elhelyezkedes['kiemelesek_brutto_ara'] . "',
	                `sor`                                   = '" . $elhelyezkedes['sor'] . "',

	                `netto_ar`                              = '" . $elhelyezkedes['netto_ar'] . "',
	                `brutto_ar`                             = '" . $elhelyezkedes['brutto_ar'] . "',

	                `total_netto_ar`                        = '" . $elhelyezkedes['total_netto_ar'] . "',
	                `total_brutto_ar`                       = '" . $elhelyezkedes['total_brutto_ar'] . "',

	                `idoszak`                   	        = '" . (int)$elhelyezkedes['idoszak'] . "',
	                `idoszak_id`                   	        = '" . (int)$elhelyezkedes['idoszak_id'] . "',
	                `priority`                   	        = '" . (int)$elhelyezkedes['priority'] . "',
	                `datum_tol`                 	        = '" . $elhelyezkedes['datum_tol'] . "',
	                `datum_ig`          	                = '" . $elhelyezkedes['datum_ig'] . "',
	                `fizetes_elbiralas_status_id`           = '" . (int)$elhelyezkedes['fizetes_elbiralas_status_id'] . "',
	                `penzugyi_status_id`                    = '" . (int)$elhelyezkedes['penzugyi_status_id'] . "'");
                }
            }
        }

		if (isset($data['product_attribute'])) {
			foreach ($data['product_attribute'] as $product_attribute) {
				if ($product_attribute['attribute_id']) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");
					
					foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {				
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
					}
				}
			}
		}
	
		if (isset($data['product_option'])) {
			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");
				
					$product_option_id = $this->db->getLastId();
				
					if (isset($product_option['product_option_value']) && count($product_option['product_option_value']) > 0 ) {
						foreach ($product_option['product_option_value'] as $product_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						} 
					}else{
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_option_id = '".$product_option_id."'");
					}
				} else { 
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value = '" . $this->db->escape($product_option['option_value']) . "', required = '" . (int)$product_option['required'] . "'");
				}
			}
		}
		
		if (isset($data['product_discount'])) {
			foreach ($data['product_discount'] as $product_discount) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}

		if (isset($data['product_special'])) {
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}
		
		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape(html_entity_decode($product_image['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
			}
		}
		
		if (isset($data['product_download'])) {
			foreach ($data['product_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
			}
		}
		
		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
			}
		}

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {

                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter WHERE  filter_id = '" . (int)$filter_id . "'");
                $csoport_id = $query->row['filter_group_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET
                    product_id = '" . (int)$product_id . "',
                    filter_group_id = '" . (int)$csoport_id . "',
                    filter_id = '" . (int)$filter_id . "'");
            }
        }
		

		
		if (isset($data['product_related'])) {
			foreach ($data['product_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
			}
		}

		if (isset($data['product_reward'])) {
			foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
			}
		}

		if (isset($data['product_layout'])) {
			foreach ($data['product_layout'] as $store_id => $layout) {
				if ($layout['layout_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
				}
			}
		}


        if (isset($data['keyword']) && $data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		/*if (isset($data['product_profiles'])) {
			foreach ($data['product_profiles'] as $profile) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "product_profile` SET `
				    product_id` = " . (int) $product_id . ",
				    customer_group_id = " . (int) $profile['customer_group_id'] . ",
				    `profile_id` = " . (int) $profile['profile_id']);
			}
		} */
		
		$this->cache->delete('product');

        return $product_id;
	}
	
	public function editProductKiemelesek($product_id, $data) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_elhelyezes WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_elhelyezkedes'])) {
            foreach ($data['product_elhelyezkedes'] as $key=>$elhelyezkedes) {
                if ($key !== "NULL") {


                    $elhelyezkedes_csoport = $this->config->get('elhelyezkedes');
                    $elhelyezkedes_alcsoport = $this->config->get('elhelyezkedes_alcsoport');
                    $kell_alcsoport = 0;
                    $elhelyezkedes_sort_order           = 999;
                    $elhelyezkedes_alcsoport_sort_order = 999;

                    foreach($elhelyezkedes_csoport as $value) {

                        if ($value['elhelyezkedes_id'] == $elhelyezkedes['elhelyezkedes_id']) {
                            $elhelyezkedes_sort_order = $value['sort_order'];
                            if ($value['alcsoport_status'] == 1) {
                                $kell_alcsoport = 1;
                            } else {
                                $kell_alcsoport = 0;
                            }
                            break;
                        }
                    }

                    foreach($elhelyezkedes_alcsoport as $value) {

                        if ($value['elhelyezkedes_alcsoport_id'] == $elhelyezkedes['elhelyezkedes_alcsoport_id']) {
                            $elhelyezkedes_alcsoport_sort_order = $value['sort_order'];
                            break;
                        }
                    }

                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_elhelyezes SET
                     `product_id`                            = '" . (int)$product_id . "',
	                `elhelyezkedes_id`	                    = '" . (int)$elhelyezkedes['elhelyezkedes_id'] . "',
	                `elhelyezkedes_neve`	                = '" . $elhelyezkedes['elhelyezkedes_neve'] . "',
	                `elhelyezkedes_netto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_netto_ara'] . "',
	                `elhelyezkedes_brutto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_brutto_ara'] . "',
	                `elhelyezkedes_sort_order`	            = '" . $elhelyezkedes_sort_order . "',

	                `elhelyezkedes_alcsoport_id`	        = '" . (int)$elhelyezkedes['elhelyezkedes_alcsoport_id'] . "',
	                `elhelyezkedes_alcsoport_neve`	        = '" . $elhelyezkedes['elhelyezkedes_alcsoport_neve'] . "',
	                `elhelyezkedes_alcsoport_netto_ara` 	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_netto_ara'] . "',
	                `elhelyezkedes_alcsoport_brutto_ara`	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'] . "',
	                `elhelyezkedes_alcsoport_sort_order`	= '" . $elhelyezkedes_alcsoport_sort_order . "',
	                `kell_alcsoport`	                    = '" . $kell_alcsoport. "',

	                `kiemelesek_id`                         = '" . (int)$elhelyezkedes['kiemelesek_id'] . "',
	                `kiemelesek_neve`                       = '" . $elhelyezkedes['kiemelesek_neve'] . "',
	                `kiemelesek_netto_ara`                  = '" . $elhelyezkedes['kiemelesek_netto_ara'] . "',
	                `kiemelesek_brutto_ara`                 = '" . $elhelyezkedes['kiemelesek_brutto_ara'] . "',
	                `sor`                                   = '" . $elhelyezkedes['sor'] . "',
	                `netto_ar`                              = '" . $elhelyezkedes['netto_ar'] . "',
	                `brutto_ar`                             = '" . $elhelyezkedes['brutto_ar'] . "',

	                `total_netto_ar`                        = '" . $elhelyezkedes['total_netto_ar'] . "',
	                `total_brutto_ar`                       = '" . $elhelyezkedes['total_brutto_ar'] . "',

	                `idoszak`                   	        = '" . (int)$elhelyezkedes['idoszak'] . "',
	                `idoszak_id`                   	        = '" . (int)$elhelyezkedes['idoszak_id'] . "',
	                `priority`                   	        = '" . (int)$elhelyezkedes['priority'] . "',
	                `datum_tol`                 	        = '" . $elhelyezkedes['datum_tol'] . "',
	                `datum_ig`          	                = '" . $elhelyezkedes['datum_ig'] . "',
	                `fizetes_elbiralas_status_id`           = '" . (int)$elhelyezkedes['fizetes_elbiralas_status_id'] . "',
	                `penzugyi_status_id`                    = '" . (int)$elhelyezkedes['penzugyi_status_id'] . "'");
                }
            }
        }
    }


	public function editProduct($product_id, $data) {

        $query =  $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" .$product_id . "'");

        $model                 =  isset($data['model'])            ?  $this->db->escape($data['model'])    : $query->row['model'];
        $sku                   =  isset($data['sku'])              ?  $this->db->escape($data['sku'])      : $query->row['sku'];
        $upc                   =  isset($data['upc'])              ?  $this->db->escape($data['upc'])      : $query->row['upc'];
        $location              =  isset($data['location'])         ?  $this->db->escape($data['location']) : $query->row['location'];
        $quantity              =  isset($data['quantity'])         ?  (int)$data['quantity']               : $query->row['quantity'];
        $minimum               =  isset($data['minimum'])          ?  (int)$data['minimum']                : $query->row['minimum'];
        $subtract              =  isset($data['subtract'])         ?  (int)$data['subtract']               : $query->row['subtract'];
        $stock_status_id       =  isset($data['stock_status_id'])  ?  (int)$data['stock_status_id']        : $query->row['stock_status_id'];
        $date_available        =  isset($data['date_available'])   ?  $this->db->escape($data['date_available']) : $query->row['date_available'];
        $manufacturer_id       =  isset($data['manufacturer_id'])  ?  (int)$data['manufacturer_id']        : $query->row['manufacturer_id'];
        $shipping              =  isset($data['shipping'])         ?  (int)$data['shipping']               : $query->row['shipping'];
        $price                 =  isset($data['price'])            ?  (float)$data['price']                : $query->row['price'];
        $szazalek              =  isset($data['szazalek'])         ?  (float)$data['szazalek']             : $query->row['szazalek'];
        $utalvany              =  isset($data['utalvany'])         ?  (float)$data['utalvany']             : $query->row['utalvany'];
        $eredeti_ar            =  isset($data['eredeti_ar'])       ?  (float)$data['eredeti_ar']           : $query->row['eredeti_ar'];
        $weight                =  isset($data['weight'])           ?  (float)$data['weight']               : $query->row['weight'];
        $weight_class_id       =  isset($data['weight_class_id'])  ?  (int)$data['weight_class_id']        : $query->row['weight_class_id'];
        $length                =  isset($data['length'])           ?  (float)$data['length']               : $query->row['length'];
        $width                 =  isset($data['width'])            ?  (float)$data['width']                : $query->row['width'];
        $height                =  isset($data['height'])           ?  (float)$data['height']               : $query->row['height'];
        $length_class_id       =  isset($data['length_class_id'])  ?  (int)$data['length_class_id']        : $query->row['length_class_id'];
        $points                =  isset($data['points'])           ?  (int)$data['points']                 : $query->row['points'];
        $status                =  isset($data['status'])           ?  (int)$data['status']                 : $query->row['status'];
        $tax_class_id          =  isset($data['tax_class_id'])     ?  $this->db->escape($data['tax_class_id']) : $query->row['tax_class_id'];
        $sort_order            =  isset($data['sort_order'])       ?  (int)$data['sort_order']             : $query->row['sort_order'];
        $cikkszam              =  isset($data['cikkszam'])         ?  $this->db->escape($data['cikkszam']) : $query->row['cikkszam'];
        $megyseg               =  isset($data['megyseg'])          ?  $this->db->escape($data['megyseg'])  : $query->row['megyseg'];
        $csomagolasi_egyseg    =  isset($data['csomagolasi_egyseg'])    ? (int)$data['csomagolasi_egyseg	'] : $query->row['csomagolasi_egyseg'];
        $csomagolasi_mennyiseg =  isset($data['csomagolasi_mennyiseg']) ? (int)$data['csomagolasi_mennyiseg	'] : $query->row['csomagolasi_mennyiseg'];
        $date_ervenyes_ig      =  isset($data['date_ervenyes_ig']) ?  $this->db->escape($data['date_ervenyes_ig']) : $query->row['date_ervenyes_ig'];
        $imagedesabled         =  isset($data['imagedesabled'])    ?  $data['imagedesabled']               : $query->row['imagedesabled'];

        $a = array();
        $a['model'] = '1';
        $a['sku'] = '2';

        $sql = 'UPDATE " . DB_PREFIX . "product SET';


		$this->db->query("UPDATE " . DB_PREFIX . "product SET

		 model                  = '" . $model . "',
		 sku                    = '" . $sku . "',
		 upc                    = '" . $upc . "',
		 location               = '" . $location . "',
		 quantity               = '" . $quantity . "',
		 minimum                = '" . $minimum . "',
		 subtract               = '" . $subtract . "',
		 stock_status_id        = '" . $stock_status_id . "',
		 date_available         = '" . $date_available . "',
		 manufacturer_id        = '" . $manufacturer_id . "',
		 shipping               = '" . $shipping . "',
		 price                  = '" . $price . "',
		 szazalek               = '" . $szazalek . "',
		 utalvany               = '" . $utalvany . "',
		 eredeti_ar             = '" . $eredeti_ar . "',
		 weight                 = '" . $weight . "',
		 weight_class_id        = '" . $weight_class_id . "',
		 length                 = '" . $length . "',
		 width                  = '" . $width . "',
		 height                 = '" . $height . "',
		 length_class_id        = '" . $length_class_id . "',
		 date_added = NOW(),
         points                 = '" . $points . "',
		 status                 = '" . 0 . "',
		 tax_class_id           = '" . $tax_class_id . "',
		 sort_order             = '" . $sort_order . "',
		 cikkszam               = '" . $cikkszam . "',
		 megyseg                = '" . $megyseg . "',
		 csomagolasi_egyseg     = '" . $csomagolasi_egyseg . "',
		 csomagolasi_mennyiseg  = '" . $csomagolasi_mennyiseg . "',
		 imagedesabled          = '" . $imagedesabled . "',
		 date_ervenyes_ig       = '" . $date_ervenyes_ig . "'

				    		        WHERE product_id = '" . (int)$product_id . "'");


		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE product_id = '" . (int)$product_id . "'");
		}
		if (isset($data['letoltheto'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET letoltheto = '" . $this->db->escape(html_entity_decode($data['letoltheto'], ENT_QUOTES, 'UTF-8')) . "' WHERE product_id = '" . (int)$product_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($data['product_description'] as $language_id => $value) {
            $name              =  isset($value['name'])              ?  $this->db->escape($value['name'])    : "";
            $meta_keyword      =  isset($value['meta_keyword'])      ?  $this->db->escape($value['meta_keyword'])    : "";
            $meta_description  =  isset($value['meta_description'])  ?  $this->db->escape($value['meta_description'])    : "";
            $description       =  isset($value['description'])       ?  $this->db->escape($value['description'])    : "";


            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET
			    product_id = '" . (int)$product_id . "',
			    language_id = '" . (int)$language_id . "',
			    name = '" . $name . "',
			    meta_keyword = '" . $meta_keyword . "',
			    meta_description = '" . $meta_description . "',
			    description = '" . $description . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "'");

		if (isset($data['product_store'])) {
			foreach ($data['product_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "'");
			}
		}

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_elhelyezes WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_elhelyezkedes'])) {
            foreach ($data['product_elhelyezkedes'] as $key=>$elhelyezkedes) {
                if ($key !== "NULL") {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_elhelyezes SET
                    `product_id`                            = '" . (int)$product_id . "',
	                `elhelyezkedes_id`	                    = '" . (int)$elhelyezkedes['elhelyezkedes_id'] . "',
	                `elhelyezkedes_neve`	                = '" . $elhelyezkedes['elhelyezkedes_neve'] . "',
	                `elhelyezkedes_netto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_netto_ara'] . "',
	                `elhelyezkedes_brutto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_brutto_ara'] . "',

	                `elhelyezkedes_alcsoport_id`	        = '" . (int)$elhelyezkedes['elhelyezkedes_alcsoport_id'] . "',
	                `elhelyezkedes_alcsoport_neve`	        = '" . $elhelyezkedes['elhelyezkedes_alcsoport_neve'] . "',
	                `elhelyezkedes_alcsoport_netto_ara` 	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_netto_ara'] . "',
	                `elhelyezkedes_alcsoport_brutto_ara`	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'] . "',

	                `kiemelesek_id`                         = '" . (int)$elhelyezkedes['kiemelesek_id'] . "',
	                `kiemelesek_neve`                       = '" . $elhelyezkedes['kiemelesek_neve'] . "',
	                `kiemelesek_netto_ara`                  = '" . $elhelyezkedes['kiemelesek_netto_ara'] . "',
	                `kiemelesek_brutto_ara`                 = '" . $elhelyezkedes['kiemelesek_brutto_ara'] . "',

	                `netto_ar`                              = '" . $elhelyezkedes['netto_ar'] . "',
	                `brutto_ar`                             = '" . $elhelyezkedes['brutto_ar'] . "',

	                `total_netto_ar`                        = '" . $elhelyezkedes['total_netto_ar'] . "',
	                `total_brutto_ar`                       = '" . $elhelyezkedes['total_brutto_ar'] . "',

	                `idoszak`                   	        = '" . (int)$elhelyezkedes['idoszak'] . "',
	                `idoszak_id`                   	        = '" . (int)$elhelyezkedes['idoszak_id'] . "',
	                `priority`                   	        = '" . (int)$elhelyezkedes['priority'] . "',
	                `datum_tol`                 	        = '" . $elhelyezkedes['datum_tol'] . "',
	                `datum_ig`          	                = '" . $elhelyezkedes['datum_ig'] . "',
	                `fizetes_elbiralas_status_id`           = '" . (int)$elhelyezkedes['fizetes_elbiralas_status_id'] . "',
	                `penzugyi_status_id`                    = '" . (int)$elhelyezkedes['penzugyi_status_id'] . "'");
                }
            }
        }
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");

		if (!empty($data['product_attribute'])) {
			foreach ($data['product_attribute'] as $product_attribute) {
				if ($product_attribute['attribute_id']) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");
					
					foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {				
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
					}
				}
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
		
		if (isset($data['product_option'])) {
			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");
				
					$product_option_id = $this->db->getLastId();
				
					if (isset($product_option['product_option_value'])  && count($product_option['product_option_value']) > 0 ) {
						foreach ($product_option['product_option_value'] as $product_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "', product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						}
					}else{
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_option_id = '".$product_option_id."'");
					}
				} else { 
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value = '" . $this->db->escape($product_option['option_value']) . "', required = '" . (int)$product_option['required'] . "'");
				}					
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
 
		if (isset($data['product_discount'])) {
			foreach ($data['product_discount'] as $product_discount) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		
		if (isset($data['product_special'])) {
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
		
		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape(html_entity_decode($product_image['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
		
		if (isset($data['product_download'])) {
			foreach ($data['product_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		
		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
			}		
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $sql = "SELECT * FROM " . DB_PREFIX . "filter WHERE  filter_id = '" . (int)$filter_id . "'";

                $query = $this->db->query($sql);
                $fi_s_id = $query->row['filter_select_id'];

                $csoport_id = $query->row['filter_group_id'];

                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET
                    product_id = '" . (int)$product_id . "',
                    filter_group_id = '" . (int)$csoport_id . "',
                    filter_select_id = '" . (int)$fi_s_id . "',
                    filter_id = '" . (int)$filter_id . "'");
            }
        }


		if (isset($data['product_related'])) {
			foreach ($data['product_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_reward'])) {
			foreach ($data['product_reward'] as $customer_group_id => $value) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$value['points'] . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_layout'])) {
			foreach ($data['product_layout'] as $store_id => $layout) {
				if ($layout['layout_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
				}
			}
		}
						
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id. "'");
		
		if (isset($data['keyword']) && $data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
						
		//$this->db->query("DELETE FROM `" . DB_PREFIX . "product_profile` WHERE product_id = " . (int) $product_id);		if (isset($data['product_profiles'])) {			foreach ($data['product_profiles'] as $profile) {				$this->db->query("INSERT INTO `" . DB_PREFIX . "product_profile` SET `product_id` = " . (int) $product_id . ", customer_group_id = " . (int) $profile['customer_group_id'] . ", `profile_id` = " . (int) $profile['profile_id']);			}		}		$this->cache->delete('product');
	}
	
	public function copyProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		if ($query->num_rows) {
			$data = array();
			
			$data = $query->row;
			
			$data['sku'] = '';
			$data['upc'] = '';
			$data['viewed'] = '0';
			$data['keyword'] = '';
			$data['status'] = '0';
						
			$data = array_merge($data, array('product_attribute' => $this->getProductAttributes($product_id)));
			$data = array_merge($data, array('product_description' => $this->getProductDescriptions($product_id)));			
			$data = array_merge($data, array('product_discount' => $this->getProductDiscounts($product_id)));
			$data = array_merge($data, array('product_filter' => $this->getProductFilters($product_id)));
			$data = array_merge($data, array('product_image' => $this->getProductImages($product_id)));		
			$data = array_merge($data, array('product_option' => $this->getProductOptions($product_id)));
			$data = array_merge($data, array('product_related' => $this->getProductRelated($product_id)));
			$data = array_merge($data, array('product_reward' => $this->getProductRewards($product_id)));
			$data = array_merge($data, array('product_special' => $this->getProductSpecials($product_id)));
			$data = array_merge($data, array('product_category' => $this->getProductCategories($product_id)));
			$data = array_merge($data, array('product_download' => $this->getProductDownloads($product_id)));
			$data = array_merge($data, array('product_layout' => $this->getProductLayouts($product_id)));
			$data = array_merge($data, array('product_store' => $this->getProductStores($product_id)));
			//$data = array_merge($data, array('product_profiles' => $this->getProfiles($product_id)));
			$this->addProduct($data);
		}
	}
	
	public function deleteProduct($product_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "'");
		//$this->db->query("DELETE FROM `" . DB_PREFIX . "product_profile` WHERE `product_id` = " . (int) $product_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id. "'");
		
		$this->cache->delete('product');
	}
	
	public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "') AS keyword
		    FROM " . DB_PREFIX . "product p
		    LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
		    WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
				
		return $query->row;
	}
	
	public function getProducts($data = array()) {
	
	    $cproArry = array();
	    $cusProId = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product_customer` WHERE customer_id = " .(int)$this->customer->getId())->rows;
		if($cusProId){
		 for($i=0;$i<count($cusProId);$i++){
		   $cproArry[] = $cusProId[$i]['product_id'];
		 }
		 $custProID = implode(',',$cproArry);
		}else{
		 $custProID = 0;
		}
		
		$sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
		
		if (!empty($data['filter_category_id'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";			
		}
				
		$sql .= " WHERE p.product_id IN (".$custProID.") AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'"; 
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}
		
		if (!empty($data['filter_price'])) {
			$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}
		
		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}
		
		$sql .= " GROUP BY p.product_id";
					
		$sort_data = array(
			'pd.name',
			'p.model',
			'p.price',
			'p.quantity',
			'p.status',
			'p.sort_order'
		);	
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY pd.name";	
		}
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
	
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
	
		return $query->rows;
	}
	
	public function getProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");
								  
		return $query->rows;
	} 
	
	public function getProductDescriptions($product_id) {
		$product_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description'],
				'meta_keyword'     => $result['meta_keyword'],
				'meta_description' => $result['meta_description']
				//'tag'              => $result['tag']
			);
		}
		
		return $product_description_data;
	}
		
	public function getProductCategories($product_id) {
		$product_category_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}
	
	public function getProductFilters($product_id) {
		$product_filter_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_filter_data[] = $result['filter_id'];
		}
				
		return $product_filter_data;
	}
	
	public function getProductAttributes($product_id) {
		$product_attribute_data = array();
		
		$product_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' GROUP BY attribute_id");
		
		foreach ($product_attribute_query->rows as $product_attribute) {
			$product_attribute_description_data = array();
			
			$product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");
			
			foreach ($product_attribute_description_query->rows as $product_attribute_description) {
				$product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $product_attribute_description['text']);
			}
			
			$product_attribute_data[] = array(
				'attribute_id'                  => $product_attribute['attribute_id'],
				'product_attribute_description' => $product_attribute_description_data
			);
		}
		
		return $product_attribute_data;
	}
	
	public function getProductOptions($product_id) {
		$product_option_data = array();
		
		$product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();	
				
			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . (int)$product_option['product_option_id'] . "'");
				
			foreach ($product_option_value_query->rows as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'points'                  => $product_option_value['points'],
					'points_prefix'           => $product_option_value['points_prefix'],						
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix']					
				);
			}
				
			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],			
				'product_option_value' => $product_option_value_data,
				'option_value'         => $product_option['option_value'],
				'required'             => $product_option['required']				
			);
		}
		
		return $product_option_data;
	}
			
	public function getProductImages($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
		
		return $query->rows;
	}
	
	public function getProductDiscounts($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, priority, price");
		
		return $query->rows;
	}
	
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");
		
		return $query->rows;
	}
	
	public function getProductRewards($product_id) {
		$product_reward_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
		}
		
		return $product_reward_data;
	}
		
	public function getProductDownloads($product_id) {
		$product_download_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_download_data[] = $result['download_id'];
		}
		
		return $product_download_data;
	}

	public function getProductStores($product_id) {
		$product_store_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_store_data[] = $result['store_id'];
		}
		
		return $product_store_data;
	}

	public function getProductLayouts($product_id) {
		$product_layout_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_layout_data[$result['store_id']] = $result['layout_id'];
		}
		
		return $product_layout_data;
	}

	public function getProductRelated($product_id) {
		$product_related_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_related_data[] = $result['related_id'];
		}
		
		return $product_related_data;
	}

	public function getProfiles($product_id) {
		//return $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_profile` WHERE product_id = " . (int) $product_id)->rows;
	}

	public function getTotalProducts($data = array()) {
	    $cproArry = array();
	    $cusProId = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product_customer` WHERE customer_id = " .(int)$this->customer->getId())->rows;
		if($cusProId){
		 for($i=0;$i<count($cusProId);$i++){
		   $cproArry[] = $cusProId[$i]['product_id'];
		 }
		 $custProID = implode(',',$cproArry);
		}else{
		 $custProID = 0;
		}
			
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

		if (!empty($data['filter_category_id'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";			
		}
		 
		$sql .= " WHERE p.product_id IN(".$custProID.") AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		 			
		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}
		
		if (!empty($data['filter_price'])) {
			$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}
		
		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}	
	
	public function getTotalProductsByTaxClassId($tax_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		return $query->row['total'];
	}
		
	public function getTotalProductsByStockStatusId($stock_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByWeightClassId($weight_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByLengthClassId($length_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int)$length_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByDownloadId($download_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");
		
		return $query->row['total'];
	}
	
	public function getTotalProductsByManufacturerId($manufacturer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int) $manufacturer_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByAttributeId($attribute_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

		return $query->row['total'];
	}	
	
	public function getTotalProductsByOptionId($option_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$option_id . "'");

		return $query->row['total'];
	}	
	
	public function getTotalProductsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
}
?>
