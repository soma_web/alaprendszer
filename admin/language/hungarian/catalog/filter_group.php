<?php

// Heading
$_['heading_title']     = 'Szűrő csoportok';

// Text
$_['text_success']      = 'Sikeresen módosította a szűrő csoportokat!';

// Column
$_['column_name']       = 'Szűrő csoport neve';
$_['column_sort_order'] = 'Sorrend';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_name']        = 'Szűrő csoport neve:';
$_['entry_sort_order']  = 'Sorrend:';

// Error
$_['error_permission']  = 'Figyelem: Önnek nincs joga módodsítani a szűrő csoportokat!';
$_['error_name']        = 'A szűrő csoport hosszának 3 és 64 karakter közt kell lennie!';
$_['error_attribute']   = 'Figyelem: Ez a szűrőcsoport nem törölhető, amíg hozzá van rendelve %s szűrőhoz!';
$_['error_product']     = 'Figyelem: Ez a szűrőcsoport nem törölhető, amíg hozzá van rendelve %s termékhez!';
?>
