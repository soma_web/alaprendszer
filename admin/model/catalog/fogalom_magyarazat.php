<?php
class ModelCatalogFogalomMagyarazat extends Model {
	public function addFogalomMagyarazat($data) {


		$this->db->query("INSERT INTO " . DB_PREFIX . "fogalom_magyarazat SET
			status = '" . (int)$data['status'] . "'");

		$fogalom_magyarazat_id = $this->db->getLastId();
			
		foreach ($data['fogalom_magyarazat_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "fogalom_magyarazat_description SET
				fogalom_magyarazat_id 	= '" . (int)$fogalom_magyarazat_id . "',
				language_id 			= '" . (int)$language_id . "',
				`name` 					= '" . $this->db->escape($value['name']) . "',
				description = '" . $this->db->escape($value['description']) . "'");
		}

	}
	
	public function editFogalomMagyarazat($fogalom_magyarazat_id, $data) {

		$this->db->query("UPDATE " . DB_PREFIX . "fogalom_magyarazat SET
			status = '" . (int)$data['status'] . "'
			WHERE fogalom_magyarazat_id='".$fogalom_magyarazat_id."'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "fogalom_magyarazat_description WHERE fogalom_magyarazat_id = '" . (int)$fogalom_magyarazat_id . "'");
					
		foreach ($data['fogalom_magyarazat_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "fogalom_magyarazat_description SET
				fogalom_magyarazat_id = '" . (int)$fogalom_magyarazat_id . "',
				language_id = '" . (int)$language_id . "',
				`name` = '" . $this->db->escape($value['name']) . "',
				description = '" . $this->db->escape($value['description']) . "'");
		}
	}
	
	public function deleteFogalomMagyarazat($fogalom_magyarazat_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "fogalom_magyarazat WHERE fogalom_magyarazat_id = '" . (int)$fogalom_magyarazat_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "fogalom_magyarazat_description WHERE fogalom_magyarazat_id = '" . (int)$fogalom_magyarazat_id . "'");

	}


	public function getFogalomMagyarazat($fogalom_magyarazat_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "fogalom_magyarazat WHERE fogalom_magyarazat_id = '" . (int)$fogalom_magyarazat_id . "'");

		return $query->row;
	}

	public function getFogalomMagyarazatok($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "fogalom_magyarazat fm
			 	LEFT JOIN " . DB_PREFIX . "fogalom_magyarazat_description fmd ON (fm.fogalom_magyarazat_id = fmd.fogalom_magyarazat_id)
			 	WHERE fmd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

            $sort_data = array(
                'fmd.name',
                'fm.status'
            );


            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                if ($data['sort'] == "fm.status") {
                    $sql .= " ORDER BY " . $data['sort'];
                    if (isset($data['order']) && ($data['order'] == 'DESC')) {
                        $sql .= " DESC";
                    } else {
                        $sql .= " ASC";
                    }
                    $sql .= " ,fmd.name ";

                } else {
                    $sql .= " ORDER BY " . $data['sort'];
                    if (isset($data['order']) && ($data['order'] == 'DESC')) {
                        $sql .= " DESC";
                    } else {
                        $sql .= " ASC";
                    }
                }
            } else {
                $sql .= " ORDER BY fmd.name";
                if (isset($data['order']) && ($data['order'] == 'DESC')) {
                    $sql .= " DESC";
                } else {
                    $sql .= " ASC";
                }
            }



		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}		

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
			
			$query = $this->db->query($sql);
			
			return $query->rows;
		} else {

			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "fogalom_magyarazat fm
				LEFT JOIN " . DB_PREFIX . "fogalom_magyarazat_description fmd ON (fm.fogalom_magyarazat_id = fmd.fogalom_magyarazat_id)
					WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY fmd.name");

			$fogalom_magyarazat_data = $query->rows;


			return $fogalom_magyarazat_data;
		}
	}

	public function getTotalFogalomMagyarazat() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "fogalom_magyarazat");

		return $query->row['total'];
	}


	public function getFogalomMagyarazatDescriptions($fogalom_magyarazat_id) {
		$fogalom_magyarazat_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "fogalom_magyarazat_description WHERE fogalom_magyarazat_id = '" . (int)$fogalom_magyarazat_id . "'");

		foreach ($query->rows as $result) {
			$fogalom_magyarazat_description_data[$result['language_id']] = array(
				'name'       => $result['name'],
				'description' => $result['description']
			);
		}

		return $fogalom_magyarazat_description_data;
	}


}
?>