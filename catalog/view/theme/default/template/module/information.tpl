<div class="box">
    <?php if ($heading_latszik){ ?>
        <div class="box-heading information-box-heading"><?php echo $heading_title; ?></div>
    <?php }?>
  <div class="box-content-information">
    <ul>
      <?php foreach ($informations as $information) { ?>
      <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
      <?php } ?>
      <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
      <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
    </ul>
  </div>
</div>
