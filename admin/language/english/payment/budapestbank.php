<?php
// Heading
$_['heading_title']					 = 'Budapest Bank';

// Text
$_['text_payment']					 = 'Payment';
$_['text_success']					 = 'Success: You have modified Budapest Bank account details!';
$_['text_pp_standard']				 = 'Budapest Bank';
$_['text_authorization']			 = 'Authorization';
$_['text_sale']						 = 'Sale';

$_['text_sms']			             = 'SMS';
$_['text_dms']						 = 'DMS';

// Entry
$_['entry_email']					 = 'E-Mail:';
$_['entry_test']					 = 'Test Mode:';
$_['entry_transaction']				 = 'Transaction Method:<br/><span class="help">Default: SMS</span>';
$_['entry_debug']					 = 'Debug Mode:<br/><span class="help">Logs additional information to the system log.</span>';
$_['entry_total']                    = 'Total:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_canceled_reversal_status'] = 'Canceled Reversal Status:';
$_['entry_completed_status']         = 'Completed Status:';
$_['entry_denied_status']			 = 'Denied Status:';
$_['entry_expired_status']			 = 'Expired Status:';
$_['entry_failed_status']			 = 'Failed Status:';
$_['entry_pending_status']			 = 'Pending Status:';
$_['entry_processed_status']		 = 'Processed Status:';
$_['entry_refunded_status']			 = 'Refunded Status:';
$_['entry_reversed_status']			 = 'Reversed Status:';
$_['entry_voided_status']		     = 'Voided Status:';
$_['entry_geo_zone']				 = 'Geo Zone:';
$_['entry_status']					 = 'Status:';
$_['entry_sort_order']				 = 'Sort Order:';

//NEW
$_['entry_process_or_msg']			 = 'Process or message:'; //Megindított folyamat vagy eredmény

$_['entry_deviza']					 = 'Deviza:';
$_['entry_cert_url']	    		 = 'Certification file path:<br/><span class="help">Base path: SYSTEM DIRECTORY</span>';
$_['entry_cert_pass']				 = 'Certification password:';
$_['entry_client_ip']				 = 'Client IP:';
$_['entry_client_language'] 		 = 'Client language:<br/><span class="help">e.g. hu,en</span>';
//NEW


// Error
$_['error_permission']				 = 'Warning: You do not have permission to modify payment PayPal!';
$_['error_email']					 = 'E-Mail required!';



?>