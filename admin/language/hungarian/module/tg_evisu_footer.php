<?php
// Heading
$_['heading_title']       = 'NMS Net Lábléc';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Sikeres adatmódosítás!';
$_['text_left']        = 'Bal';
$_['text_right']       = 'Jobb';
$_['text_footer']       = 'Lábléc';
$_['text_image_manager'] = 'Képkezelő';
$_['text_description'] = 'Leírás';
$_['text_info_title'] = 'Cím:';



// Tab
$_['tab_gen']         = 'Alapvető beállítások';
$_['tab_info']       = 'Információ';
$_['tab_twitter']    = 'Twitter';
$_['tab_facebook']       = 'Facebook';
$_['tab_contact']        = 'Kapcsolat';
$_['tab_payment']        = 'Fizetési képek';
$_['tab_default']        = 'Alapért. Oszlop';



// Entry
$_['entry_limit']      = 'Határ:';
$_['entry_code']       = 'Szöveg';
$_['entry_position']   = 'Pozició:';
$_['entry_status']     = 'Állapot:';
$_['entry_sort_order'] = 'Sorrend:';
$_['entry_url'] = 'URL:';
$_['entry_storetitle'] = 'Bolt címe:';
$_['entry_unsubscribe']  = 'Leíratkozás:';
$_['entry_mail']   		 = 'Email küldése';
$_['entry_registered']   = 'Regisztrált felhasználók:';
$_['entry_informationurl'] = 'URL:';
$_['entry_informationtitle'] = 'Információs Cím:';
$_['entry_categoryurl'] = 'URL:';
$_['entry_categorytitle'] = 'Kategóriacím:';
$_['entry_supporttitle'] = 'Támogatási Cím:';
$_['entry_image']        = 'Kép: <br /><span class="help">Kattints rá a cseréhez.</span>';
$_['entry_url']        = 'Link: ';

// Buttons
$_['button_addslide']  = 'Új képsor hozzáadása';
$_['button_remove']    = 'Eltávolítás';

// Error
$_['error_permission'] = 'Nincs engedélye a módosításhoz!';

?>