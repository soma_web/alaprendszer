<?php

// Heading
$_['heading_title'] = 'Tranzakció';

// Text
$_['text_message']  = '<p>The transaction is successful.</p><p>Thank you!</p>';
$_['text_error_message']  = '<p>Tehnical error occurred! Please contact merchant!</p>';

$_['text_account']  = 'Account';
$_['text_logout']   = 'Logout';

$_['text_budapestbank'] = 'Budapest Bank';

// Text
$_['text_title']    = 'Budapest Bank';
$_['text_reason'] 	= 'REASON';
$_['text_testmode']	= 'Warning: The Budapest Bank Gateway is in \'Sandbox Mode\'. Your account will not be charged.';
$_['text_total']	= 'Shipping, Handling, Discounts & Taxes';


$_['text_invoice']	= 'Order ID: '; //Rendelési azonosító
$_['text_products']	= 'Products: '; //Termékek
$_['text_name']	    = 'Name: '; //Termékek
?>