<?php
class ControllerAccountProduct extends Controller {
	private $error = array(); 
    private $nyelv = true;

  	public function index() {
	
	   if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');
	  
	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	} 
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS ".DB_PREFIX."product_customer (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  customer_id int(11) NOT NULL,
		  product_id int(11) NOT NULL,
		  PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");




		$this->document->setTitle($this->language->get('heading_title')); 
		
		$this->load->model('account/product');
		
		$this->getList();
  	}
  
  	public function insert() {
	if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	}
    	$this->language->load('account/product');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/product');

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$product_id = $this->model_account_product->addProduct($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
            $url .= '&product_id='.$product_id;
            $url .= '&new=1';
            $mezok = $this->config->get("megjelenit_admin_product");

            if(isset($mezok['biztonsagi_kod']) && $mezok['biztonsagi_kod'] == 1) {
                $this->requestsecuritycode($product_id);
                $this->redirect($this->url->link('account/product/checksecuritycodepage', $url, 'SSL'));
            } else {
                if (isset($this->request->get['filter_name'])) {
                    $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
                }

                if (isset($this->request->get['filter_model'])) {
                    $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
                }

                if (isset($this->request->get['filter_price'])) {
                    $url .= '&filter_price=' . $this->request->get['filter_price'];
                }

                if (isset($this->request->get['filter_quantity'])) {
                    $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
                }

                if (isset($this->request->get['filter_status'])) {
                    $url .= '&filter_status=' . $this->request->get['filter_status'];
                }

                if (isset($this->request->get['sort'])) {
                    $url .= '&sort=' . $this->request->get['sort'];
                }

                if (isset($this->request->get['order'])) {
                    $url .= '&order=' . $this->request->get['order'];
                }

                if (isset($this->request->get['page'])) {
                    $url .= '&page=' . $this->request->get['page'];
                }

			    $this->redirect($this->url->link('account/product', $url, 'SSL'));
            }
    	}

    	$this->getForm();
  	}

  	public function kiemeles() {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

            $this->redirect($this->url->link('account/login', '', 'SSL'));
        }
        $this->language->load('account/product');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('account/product');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_account_product->editProductKiemelesek($this->request->get['product_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('account/product', $url, 'SSL'));
        }

        $this->getFormKiemeles();

    }

  	public function update() {
	if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	}
    	$this->language->load('account/product');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/product');

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_account_product->editProduct($this->request->get['product_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('account/product', $url, 'SSL'));
		}

    	$this->getForm();
  	}

  	public function delete() {
	if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	}
    	$this->language->load('account/product');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/product');

		if ( isset($this->request->post['selected']) && $this->validateDelete() ) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_account_product->deleteProduct($product_id);
               // $this->openbay->deleteProduct($product_id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('account/product', $url, 'SSL'));
		}

    	$this->getList();
  	}

  	public function copy() {
	if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	}
    	$this->language->load('account/product');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/product');

		if (isset($this->request->post['selected']) && $this->validateCopy()) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_account_product->copyProduct($product_id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('account/product', $url, 'SSL'));
		}

    	$this->getList();
  	}

  	protected function getList() {
        $this->load->model('setting/extension');
        $no_module= $this->model_setting_extension->getNoModule();
        if (in_array("language",$no_module)){
            $this->nyelv = false;
        }

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

        $this->data['mezok'] = $this->config->get("megjelenit_admin_product");

        $url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home','SSL'),
      		'separator' => false
   		);
        $this->language->load('account/account');
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );
        $this->language->load('account/product');
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/product', $url, 'SSL'),
      		'separator' => $this->language->get('text_separator')
   		);

		$this->data['insert'] = $this->url->link('account/product/insert', $url, 'SSL');
		$this->data['copy'] = $this->url->link('account/product/copy', $url, 'SSL');
		$this->data['delete'] = $this->url->link('account/product/delete', $url, 'SSL');

		$this->data['products'] = array();

		$data = array(
			'filter_name'	  => $filter_name,
			'filter_model'	  => $filter_model,
			'filter_price'	  => $filter_price,
			'filter_quantity' => $filter_quantity,
			'filter_status'   => $filter_status,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);

		$this->load->model('tool/image');

		$product_total = $this->model_account_product->getTotalProducts($data);

		$results = $this->model_account_product->getProducts($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text'          => $this->language->get('text_edit'),
				'href'          => $this->url->link('account/product/update', '&product_id=' . $result['product_id'] . $url, 'SSL'),
			);

            if ( $this->data['mezok']['elhelyezkedes'] == 1) {
                $action[] = array(
                    'text'          => $this->language->get('kiemel_text'),
                    'href'          => $this->url->link('account/product/kiemeles', '&product_id=' . $result['product_id'] . $url, 'SSL')
                );
            }

            if( isset($this->data['mezok']['biztonsagi_kod']) && $this->data['mezok']['biztonsagi_kod'] == 1 && $result['correctsecuritycode'] != '1') {
                $action[] = array(
                    'text'          => $this->language->get('heading_securitycode'),
                    'href'          => $this->url->link('account/product/checksecuritycodepage', '&product_id=' . $result['product_id'] . $url, 'SSL')
                );
            }

			if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
			}

			$special = false;

			$product_specials = $this->model_account_product->getProductSpecials($result['product_id']);

			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || $product_special['date_start'] < date('Y-m-d')) && ($product_special['date_end'] == '0000-00-00' || $product_special['date_end'] > date('Y-m-d'))) {
					$special = $product_special['price'];

					break;
				}
			}


            if ($result['szazalek'] == 1) {
                $penznem = "%";
            } else {
                $penznem = $this->currency->getFizetoeszkoz();
            }

      		$this->data['products'][] = array(
				'product_id' => $result['product_id'],
				'name'       => $result['name'],
				'model'      => $result['model'],
				'price'      => number_format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')),0,"",".").$penznem,
				//'price'      => $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')),4),
				'special'    => $special,
				'image'      => $image,
				'quantity'   => $result['quantity'],
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'   => isset($this->request->post['selected']) && in_array($result['product_id'], $this->request->post['selected']),
				'action'     => $action
			);
    	}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');

		$this->data['column_image'] = $this->language->get('column_image');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_model'] = $this->language->get('column_model');
		$this->data['column_price'] = $this->language->get('column_price');
		$this->data['column_quantity'] = $this->language->get('column_quantity');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_copy'] = $this->language->get('button_copy');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');

        //$this->data['token'] = $this->session->data['token'];

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_name'] = $this->url->link('account/product', '&sort=pd.name' . $url, 'SSL');
		$this->data['sort_model'] = $this->url->link('account/product', '&sort=p.model' . $url, 'SSL');
		$this->data['sort_price'] = $this->url->link('account/product', '&sort=p.price' . $url, 'SSL');
		$this->data['sort_quantity'] = $this->url->link('account/product', '&sort=p.quantity' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('account/product', '&sort=p.status' . $url, 'SSL');
		$this->data['sort_order'] = $this->url->link('account/product', '&sort=p.sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('account/product', $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_model'] = $filter_model;
		$this->data['filter_price'] = $filter_price;
		$this->data['filter_quantity'] = $filter_quantity;
		$this->data['filter_status'] = $filter_status;

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

        $this->data['kosarban_van'] = count($this->cart_elhelyezkedes->getProducts());

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/product_list.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/account/product_list.tpl';
			} else {
				$this->template = 'default/template/account/product_list.tpl';
			}


		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
            'module/cart_elhelyezkedes',
			'common/header'
		);

		$this->response->setOutput($this->render());
  	}
    protected function getFormKiemeles() {

        $this->data['heading_title']     = $this->language->get('heading_title');

        $this->data['text_enabled']     = $this->language->get('text_enabled');
        $this->data['text_disabled']    = $this->language->get('text_disabled');
        $this->data['text_none']        = $this->language->get('text_none');
        $this->data['text_select']      = $this->language->get('text_select');
        $this->data['text_none']        = $this->language->get('text_none');
        $this->data['text_fizetendo']   = $this->language->get('text_fizetendo');
        $this->data['text_osszesen']    = $this->language->get('text_osszesen');

        $this->data['entry_name']       = $this->language->get('entry_name');
        $this->data['entry_date_kaphato_ig'] = $this->language->get('entry_date_kaphato_ig');

        $this->data['entry_price']      = $this->language->get('entry_price');
        $this->data['entry_kaphato']    = $this->language->get('entry_kaphato');
        $this->data['entry_image_letoltheto'] = $this->language->get('entry_image_letoltheto');

        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $this->data['entry_status']     = $this->language->get('entry_status');

        $this->data['entry_date_start'] = $this->language->get('entry_date_start');
        $this->data['entry_date_end']   = $this->language->get('entry_date_end');
        $this->data['entry_priority']   = $this->language->get('entry_priority');
        $this->data['entry_tag']        = $this->language->get('entry_tag');

        $this->data['button_cart']      = $this->language->get('button_cart');
        $this->data['button_set_valos_ar']      = $this->language->get('button_set_valos_ar');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');

        $this->data['text_prioritas']   = $this->language->get('text_prioritas');

        $this->data['button_save']      = $this->language->get('button_save');
        $this->data['button_cancel']    = $this->language->get('button_cancel');
        $this->data['button_remove']    = $this->language->get('button_remove');
        $this->data['text_select_all']  = $this->language->get('text_select_all');
        $this->data['text_unselect_all']= $this->language->get('text_unselect_all');

        $this->data['entry_elhelyezkedes'] = $this->language->get('entry_elhelyezkedes');
        $this->data['entry_elhelyezkedes_alcsoport'] = $this->language->get('entry_elhelyezkedes_alcsoport');
        $this->data['entry_kiemelt_keret'] = $this->language->get('entry_kiemelt_keret');
        $this->data['entry_idoszak_darab'] = $this->language->get('entry_idoszak_darab');
        $this->data['entry_idoszak']    = $this->language->get('entry_idoszak');
        $this->data['entry_date_start'] = $this->language->get('entry_date_start');
        $this->data['entry_date_end']   = $this->language->get('entry_date_end');
        $this->data['entry_status']     = $this->language->get('entry_status');
        $this->data['entry_priority']   = $this->language->get('entry_priority');
        $this->data['button_add_elhelyezkedes'] = $this->language->get('button_add_elhelyezkedes');

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_price'])) {
            $url .= '&filter_price=' . $this->request->get['filter_price'];
        }

        if (isset($this->request->get['filter_quantity'])) {
            $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'SSL'),
            'separator' => false
        );
        $this->language->load('account/account');

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('account/product', $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );


        $this->data['action'] = $this->url->link('account/product/kiemeles', '&product_id=' . $this->request->get['product_id'] . $url, 'SSL');

        $this->data['cancel'] = $this->url->link('account/product', $url, 'SSL');

        if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $product_info = $this->model_account_product->getProduct($this->request->get['product_id']);
        }

        //$this->data['token'] = $this->session->data['token'];
        $this->data['nyelv'] = $this->nyelv;
        $this->load->model('localisation/language');

        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        $this->data['name'] = $product_info['name'];

        $this->load->model('account/elhelyezkedes');
        $this->data['elhelyezkedesek_selects'] = $this->model_account_elhelyezkedes->getElhelyezkedesek();

        $this->load->model('account/elhelyezkedes_alcsoport');
        $this->data['elhelyezkedesek_alcsoport_selects'] = $this->model_account_elhelyezkedes_alcsoport->getAlcsoportElhelyezkedesek();

        $this->load->model('account/kiemelesek');
        $this->data['kiemelesek_selects'] = $this->model_account_kiemelesek->getKiemelesek(isset($product_info['utalvany']) ? $product_info['utalvany'] : 0);

        $this->data['penznem'] =  " ". $this->currency->getFizetoeszkoz();


        $idoszakok = array();

        $idoszakok[] = array(
            'id'    => '1',
            'name'  => $this->language->get('text_nap')
        );
        $idoszakok[] = array(
            'id'    => '2',
            'name'  => $this->language->get('text_het')
        );
        $idoszakok[] = array(
            'id'    => '3',
            'name'  => $this->language->get('text_honap')
        );
        $idoszakok[] = array(
            'id'    => '4',
            'name'  => $this->language->get('text_ev')
        );

        $this->data['idoszakok'] = $idoszakok;


        if (isset($this->request->post['product_elhelyezkedes'])) {
            $this->data['product_elhelyezkedess'] = $this->request->post['product_elhelyezkedes'];
            if ( isset($this->data['product_elhelyezkedess']['NULL']) ) {
                unset($this->data['product_elhelyezkedess']['NULL']);
            }
        } elseif (isset($this->request->get['product_id'])) {
            $this->load->model('catalog/product');

            $this->data['product_elhelyezkedess'] = $this->model_catalog_product->getProductElhelyezkedes($this->request->get['product_id']);
        } else {
            $this->data['product_elhelyezkedess'] = array();
        }

        $sor = 0;


        if ($this->data['product_elhelyezkedess'] ) {
            $sort_order = array();
            foreach ($this->data['product_elhelyezkedess'] as $key => $value) {
                $sort_order[$key] = $value['priority'];
                if ($value['sor'] > $sor) {
                    $sor = $value['sor'];
                }
            }
            array_multisort($sort_order, SORT_ASC, $this->data['product_elhelyezkedess']);
        }
        $this->data['elhelyezkedes_row_max'] = $sor+1;


        if (isset($this->request->post['date_available'])) {
            $this->data['date_available'] = $this->request->post['date_available'];
        } elseif (!empty($product_info)) {
            $this->data['date_available'] = date('Y-m-d', strtotime($product_info['date_available']));
        } else {
            $this->data['date_available'] = date('Y-m-d', time() - 86400);
        }

        if (isset($this->request->post['date_ervenyes_ig'])) {
            $this->data['date_ervenyes_ig'] = $this->request->post['date_ervenyes_ig'];
        } elseif (!empty($product_info)) {
            if ($product_info['date_ervenyes_ig'] == "0000-00-00")  {
                $this->data['date_ervenyes_ig'] = "";
            } else {
                $this->data['date_ervenyes_ig'] = $product_info['date_ervenyes_ig'];
            }
        } else {
            $this->data['date_ervenyes_ig'] = date('Y-m-d');
        }



        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/product_form_kiemeles.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/product_form_kiemeles.tpl';
        } else {
            $this->template = 'default/template/account/product_form_kiemeles.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'module/cart_elhelyezkedes',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }


    protected function getForm() {
        $this->load->model('setting/extension');
        $no_module= $this->model_setting_extension->getNoModule();
        if (in_array("language",$no_module)){
            $this->nyelv = false;
        }

    	$this->data['heading_title'] = $this->language->get('heading_title');

    	$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
    	$this->data['text_none'] = $this->language->get('text_none');

    	$this->data['text_yes_kep'] = $this->language->get('text_yes_kep');
    	$this->data['text_no_kep'] = $this->language->get('text_no_kep');

    	$this->data['text_yes'] = $this->language->get('text_yes');
    	$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_plus'] = $this->language->get('text_plus');
		$this->data['text_minus'] = $this->language->get('text_minus');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_option_value'] = $this->language->get('text_option_value');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_percent'] = $this->language->get('text_percent');
		$this->data['text_amount'] = $this->language->get('text_amount');

        $this->data['button_sending'] = $this->language->get('button_sending');
        $this->data['text_biztonsagi_kod_igenyles'] = $this->language->get('text_biztonsagi_kod_igenyles');
        $this->data['text_biztonsagi_kod'] = $this->language->get('text_biztonsagi_kod');

        $this->data['text_biztonsagi_kod_elkuldve'] = $this->language->get('text_biztonsagi_kod_elkuldve');
        $this->data['text_sikeres_biztonsagi_kod'] = $this->language->get('text_sikeres_biztonsagi_kod');
        $this->data['text_sikertelen_biztonsagi_kod'] = $this->language->get('text_sikertelen_biztonsagi_kod');
        $this->data['text_biztonsagi_kod_nincs_elkuldve'] = $this->language->get('text_biztonsagi_kod_nincs_elkuldve');
        $this->data['text_biztonsagi_kod_kuldese'] = $this->language->get('text_biztonsagi_kod_kuldese');


        $this->data['entry_filter'] = $this->language->get('entry_filter');
        $this->data['button_bovebben'] = $this->language->get('button_bovebben');

        $this->data['entry_kep_tiltas']     = $this->language->get('entry_kep_tiltas');
		$this->data['entry_name']           = $this->language->get('entry_name');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_meta_keyword']   = $this->language->get('entry_meta_keyword');
		$this->data['entry_description']    = $this->language->get('entry_description');
		$this->data['entry_store']          = $this->language->get('entry_store');
		$this->data['entry_keyword']        = $this->language->get('entry_keyword');
    	$this->data['entry_model']          = $this->language->get('entry_model');
		$this->data['entry_sku']            = $this->language->get('entry_sku');
		$this->data['entry_upc']            = $this->language->get('entry_upc');
		$this->data['entry_ean']            = $this->language->get('entry_ean');
		$this->data['entry_jan']            = $this->language->get('entry_jan');
		$this->data['entry_isbn']           = $this->language->get('entry_isbn');
		$this->data['entry_mpn']            = $this->language->get('entry_mpn');
		$this->data['entry_location']       = $this->language->get('entry_location');
		$this->data['entry_minimum'] = $this->language->get('entry_minimum');
		$this->data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
    	$this->data['entry_shipping'] = $this->language->get('entry_shipping');
    	$this->data['entry_date_available'] = $this->language->get('entry_date_available');
    	$this->data['entry_date_kaphato_ig'] = $this->language->get('entry_date_kaphato_ig');
    	$this->data['entry_quantity'] = $this->language->get('entry_quantity');
		$this->data['entry_stock_status'] = $this->language->get('entry_stock_status');
    	$this->data['entry_price'] = $this->language->get('entry_price');
    	$this->data['entry_kaphato'] = $this->language->get('entry_kaphato');
        $this->data['entry_image_letoltheto'] = $this->language->get('entry_image_letoltheto');

		$this->data['entry_points'] = $this->language->get('entry_points');
		$this->data['entry_option_points'] = $this->language->get('entry_option_points');
		$this->data['entry_subtract'] = $this->language->get('entry_subtract');
    	$this->data['entry_weight_class'] = $this->language->get('entry_weight_class');
    	$this->data['entry_weight'] = $this->language->get('entry_weight');
		$this->data['entry_dimension'] = $this->language->get('entry_dimension');
		$this->data['entry_length'] = $this->language->get('entry_length');
    	$this->data['entry_image'] = $this->language->get('entry_image');
    	$this->data['entry_download'] = $this->language->get('entry_download');
    	$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['entry_filter'] = $this->language->get('entry_filter');
		$this->data['entry_related'] = $this->language->get('entry_related');
		$this->data['entry_attribute'] = $this->language->get('entry_attribute');
		$this->data['entry_text'] = $this->language->get('entry_text');
		$this->data['entry_option'] = $this->language->get('entry_option');
		$this->data['entry_option_value'] = $this->language->get('entry_option_value');
		$this->data['entry_required'] = $this->language->get('entry_required');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');

		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		$this->data['entry_priority'] = $this->language->get('entry_priority');
		$this->data['entry_tag'] = $this->language->get('entry_tag');

		$this->data['entry_reward'] = $this->language->get('entry_reward');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		//$this->data['entry_profile'] = $this->language->get('entry_profile');

		$this->data['text_recurring_help'] = $this->language->get('text_recurring_help');
		$this->data['text_recurring_title'] = $this->language->get('text_recurring_title');
		$this->data['text_recurring_trial'] = $this->language->get('text_recurring_trial');
		$this->data['entry_recurring'] = $this->language->get('entry_recurring');
		$this->data['entry_recurring_price'] = $this->language->get('entry_recurring_price');
		$this->data['entry_recurring_freq'] = $this->language->get('entry_recurring_freq');
		$this->data['entry_recurring_cycle'] = $this->language->get('entry_recurring_cycle');
		$this->data['entry_recurring_length'] = $this->language->get('entry_recurring_length');
		$this->data['entry_trial'] = $this->language->get('entry_trial');
		$this->data['entry_trial_price'] = $this->language->get('entry_trial_price');
		$this->data['entry_trial_freq'] = $this->language->get('entry_trial_freq');
		$this->data['entry_trial_length'] = $this->language->get('entry_trial_length');
		$this->data['entry_trial_cycle'] = $this->language->get('entry_trial_cycle');
		$this->data['entry_szazalek'] = $this->language->get('entry_szazalek');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');

		$this->data['text_length_day'] = $this->language->get('text_length_day');
		$this->data['text_length_week'] = $this->language->get('text_length_week');
		$this->data['text_length_month'] = $this->language->get('text_length_month');
		$this->data['text_length_month_semi'] = $this->language->get('text_length_month_semi');
		$this->data['text_length_year'] = $this->language->get('text_length_year');
		$this->data['text_kupon_keszit'] = $this->language->get('text_kupon_keszit');
		$this->data['text_prioritas'] = $this->language->get('text_prioritas');

    	$this->data['button_save'] = $this->language->get('button_save');
    	$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_attribute'] = $this->language->get('button_add_attribute');
		$this->data['button_add_option'] = $this->language->get('button_add_option');
		$this->data['button_add_option_value'] = $this->language->get('button_add_option_value');
		$this->data['button_add_discount'] = $this->language->get('button_add_discount');
		$this->data['button_add_special'] = $this->language->get('button_add_special');
		$this->data['button_add_image'] = $this->language->get('button_add_image');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
		//$this->data['button_add_profile'] = $this->language->get('button_add_profile');
        $this->data['kaphato'] = $this->language->get('kaphato');
        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');

        $this->data['tab_general'] = $this->language->get('tab_general');
    	$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_attribute'] = $this->language->get('tab_attribute');
		$this->data['tab_option'] = $this->language->get('tab_option');
		//$this->data['tab_profile'] = $this->language->get('tab_profile');
		$this->data['tab_discount'] = $this->language->get('tab_discount');
		$this->data['tab_special'] = $this->language->get('tab_special');
    	$this->data['tab_image'] = $this->language->get('tab_image');
		$this->data['tab_links'] = $this->language->get('tab_links');
		$this->data['tab_reward'] = $this->language->get('tab_reward');
		$this->data['tab_design'] = $this->language->get('tab_design');
		$this->data['tab_marketplace_links'] = $this->language->get('tab_marketplace_links');

        $this->data['entry_elhelyezkedes'] = $this->language->get('entry_elhelyezkedes');
        $this->data['entry_elhelyezkedes_alcsoport'] = $this->language->get('entry_elhelyezkedes_alcsoport');
        $this->data['entry_kiemelt_keret'] = $this->language->get('entry_kiemelt_keret');
        $this->data['entry_idoszak_darab'] = $this->language->get('entry_idoszak_darab');
        $this->data['entry_idoszak'] = $this->language->get('entry_idoszak');
        $this->data['entry_date_start'] = $this->language->get('entry_date_start');
        $this->data['entry_date_end'] = $this->language->get('entry_date_end');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_priority'] = $this->language->get('entry_priority');
        $this->data['button_add_elhelyezkedes'] = $this->language->get('button_add_elhelyezkedes');

        $this->data['megjelenit_termekful'] = $this->config->get('megjelenit_admin_termekful');


        $this->load->model('tool/image');
        $this->data['no_image_product'] = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));

        if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}

        if (isset($this->error['letoltheto'])) {
			$this->data['error_letoltheto'] = $this->error['letoltheto'];
		} else {
			$this->data['error_letoltheto'] = array();
		}



        if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = "";
		}
        if (isset($this->error['product_filter'])) {
			$this->data['error_product_filter'] = $this->error['product_filter'];
		} else {
			$this->data['error_product_filter'] = "";
		}
        if (isset($this->error['date_ervenyes_ig'])) {
			$this->data['error_date_ervenyes_ig'] = $this->error['date_ervenyes_ig'];
		} else {
			$this->data['error_date_ervenyes_ig'] = "";
		}

        if(isset($this->error['category'])) {
            $this->data['error_category'] = $this->error['category'];
        } else {
            $this->data['error_category'] = '';
        }




 		if (isset($this->error['meta_description'])) {
			$this->data['error_meta_description'] = $this->error['meta_description'];
		} else {
			$this->data['error_meta_description'] = array();
		}

   		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = array();
		}

   		if (isset($this->error['model'])) {
			$this->data['error_model'] = $this->error['model'];
		} else {
			$this->data['error_model'] = '';
		}

		if (isset($this->error['date_available'])) {
			$this->data['error_date_available'] = $this->error['date_available'];
		} else {
			$this->data['error_date_available'] = '';
		}

        if (isset($this->error['date_ervenyes_ig'])) {
            $this->data['date_ervenyes_ig'] = $this->error['date_ervenyes_ig'];
        } else {
            $this->data['date_ervenyes_ig'] = '';
        }

        $url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'SSL'),
			'separator' => false
   		);
        $this->language->load('account/account');

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/product', $url, 'SSL'),
      		'separator' => $this->language->get('text_separator')
   		);

		if (!isset($this->request->get['product_id'])) {
			$this->data['action'] = $this->url->link('account/product/insert', $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('account/product/update', '&product_id=' . $this->request->get['product_id'] . $url, 'SSL');
            $this->data['productid'] = $this->request->get['product_id'];
		}

		$this->data['cancel'] = $this->url->link('account/product', $url, 'SSL');

		if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$product_info = $this->model_account_product->getProduct($this->request->get['product_id']);
            $this->data['product_info'] = $product_info;
    	}

		//$this->data['token'] = $this->session->data['token'];
		$this->data['nyelv'] = $this->nyelv;
		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['product_description'])) {
			$this->data['product_description'] = $this->request->post['product_description'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_description'] = $this->model_account_product->getProductDescriptions($this->request->get['product_id']);
		} else {
			$this->data['product_description'] = array();
		}

		if (isset($this->request->post['model'])) {
      		$this->data['model'] = $this->request->post['model'];
    	} elseif (!empty($product_info)) {
			$this->data['model'] = $product_info['model'];
		} else {
      		$this->data['model'] = '';
    	}

		if (isset($this->request->post['sku'])) {
      		$this->data['sku'] = $this->request->post['sku'];
    	} elseif (!empty($product_info)) {
			$this->data['sku'] = $product_info['sku'];
		} else {
      		$this->data['sku'] = '';
    	}

		if (isset($this->request->post['upc'])) {
      		$this->data['upc'] = $this->request->post['upc'];
    	} elseif (!empty($product_info)) {
			$this->data['upc'] = $product_info['upc'];
		} else {
      		$this->data['upc'] = '';
    	}

		if (isset($this->request->post['location'])) {
      		$this->data['location'] = $this->request->post['location'];
    	} elseif (!empty($product_info)) {
			$this->data['location'] = $product_info['location'];
		} else {
      		$this->data['location'] = '';
    	}

		$this->load->model('setting/store');

		$this->data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['product_store'])) {
			$this->data['product_store'] = $this->request->post['product_store'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_store'] = $this->model_account_product->getProductStores($this->request->get['product_id']);
		} else {
			$this->data['product_store'] = array(0);
		}

		 $data = array(
				'start'       => 0,
				'limit'       => 100
			);

		$this->load->model('account/category');
		$this->data['procategorys'] = $this->model_account_category->getCategories($data);

        // Filters
        $this->load->model('account/filter');

        if (isset($this->request->post['product_filter'])) {
            $filters = $this->request->post['product_filter'];

        } elseif (isset($this->request->post['product_store'])) {
            $filters = array();

        } elseif (isset($this->request->get['product_id'])) {
            $filters = $this->model_account_filter->getProductFilters($this->request->get['product_id']);
        } else {
            $filters = array();
        }

        // szűrők kigyüjtése

        $this->data['product_filters'] = array();
        $this->data['product_filters_group'] = array();



        foreach ($filters as $filter_id) {
            $filter_info = $this->model_account_filter->getFilter($filter_id);

            if ($filter_info) {
                $this->data['product_filters'][] = array(
                    'filter_id' => $filter_info['filter_id'],
                    'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name'],
                    'filter_group_id' => $filter_info['filter_group_id'],
                    'filter_select_id' => $filter_info['filter_select_id']
                );
                $this->data['product_filters_group'][] = array(
                    'filter_id' => $filter_info['filter_id'],
                    'name'      => $filter_info['group'],
                    'filter_group_id' => $filter_info['filter_group_id'],
                    'filter_select_id' => $filter_info['filter_select_id']
                );
            }
        }

        $data = array(
            'filter_name' => '',
            'start'       => 0,
            'limit'       => 20
        );


        $filters = $this->model_account_filter->getFilters();
        $szurok = array();
        foreach ($filters as $filter) {
            $szurok[] = array(
                'filter_id' => $filter['filter_id'],
                'name'      => $filter['group'] . ' &gt; ' . $filter['name'],
                'filter_group_id' => $filter['filter_group_id'],
                'filter_select_id' => $filter['filter_select_id']
            );
        }
        $sort_order = array();

        foreach ($szurok as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $szurok);
        $this->data['szurok'] = $szurok;
//szűrők kigyüjtés vége

// szűrő csoport kigyüjtése, ahol van üres név, szűrő alcsoport





        $filters = $this->model_account_filter->getFilters();
        $szuro_csoport = array();
        foreach ($filters as $filter) {
            if (empty($filter['name']) ) {
                $szuro_csoport[] = array(
                    'filter_id' => $filter['filter_id'],
                    'name'      => $filter['group'],
                    'filter_group_id' => $filter['filter_group_id'],
                    'filter_select_id' => $filter['filter_select_id']
                );
            }
        }
        $sort_order = array();

        foreach ($szuro_csoport as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $szuro_csoport);
        $this->data['szuro_csoport'] = $szuro_csoport;


// kigyüjtés vége

		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($product_info)) {
			$this->data['keyword'] = $product_info['keyword'];
		} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['image'])) {
			$this->data['image'] = $this->request->post['image'];
		} elseif (!empty($product_info)) {
			$this->data['image'] = $product_info['image'];
		} else {
			$this->data['image'] = '';
		}


        if (isset($this->request->post['imagedesabled'])) {
            $this->data['imagedesabled'] = $this->request->post['imagedesabled'];
        } elseif (!empty($product_info)) {
            $this->data['imagedesabled'] = $product_info['imagedesabled'];
        } else {
            $this->data['imagedesabled'] = 0;
        }


        if (isset($this->request->post['letoltheto'])) {
            $this->data['letoltheto'] = $this->request->post['letoltheto'];
        } elseif (!empty($product_info)) {
            $this->data['letoltheto'] = $product_info['letoltheto'];
        } else {
            $this->data['letoltheto'] = '';
        }

		$this->load->model('tool/image');

        if (isset($this->request->post['image']) && !empty($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
            $this->data['popup'] = $this->model_tool_image->resize($this->request->post['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
        } elseif (!empty ($product_info['image']) ) {
            $this->data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
        } else {
            $this->data['popup'] = '';
        }

        if (isset($this->request->post['letoltheto']) && !empty($this->request->post['letoltheto']) && file_exists(DIR_IMAGE . $this->request->post['letoltheto'])) {
            $this->data['letoltheto_popup'] = $this->model_tool_image->resize($this->request->post['letoltheto'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
        } elseif (!empty ($product_info['image']) ) {
            $this->data['letoltheto_popup'] = $this->model_tool_image->resize($product_info['letoltheto'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
        } else {
            $this->data['letoltheto'] = '';
        }

        if (isset($this->request->post['image']) && !empty($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
            $this->data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
		} elseif (!empty($product_info) && $product_info['image'] && file_exists(DIR_IMAGE . $product_info['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
		} else {
			$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
		}

        if (isset($this->request->post['letoltheto']) && !empty($this->request->post['letoltheto']) && file_exists(DIR_IMAGE . $this->request->post['letoltheto'])) {
            $this->data['letoltheto_thumb'] = $this->model_tool_image->resize($this->request->post['letoltheto'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
        } elseif (!empty($product_info) && $product_info['letoltheto'] && file_exists(DIR_IMAGE . $product_info['letoltheto'])) {
            $this->data['letoltheto_thumb'] = $this->model_tool_image->resize($product_info['letoltheto'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
        } else {
            $this->data['letoltheto_thumb'] = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
        }


    	if (isset($this->request->post['shipping'])) {
      		$this->data['shipping'] = $this->request->post['shipping'];
    	} elseif (!empty($product_info)) {
      		$this->data['shipping'] = $product_info['shipping'];
    	} else {
			$this->data['shipping'] = 0;
		}

    	if (isset($this->request->post['price'])) {
      		$this->data['price'] = $this->request->post['price'];
    	} elseif (!empty($product_info)) {
			//$this->data['price'] = $this->currency->format( $this->tax->calculate((int)$product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ) );
			$this->data['price'] = $this->tax->calculate((int)$product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ) ;
		} else {
      		$this->data['price'] = '';
    	}

        if (isset($this->request->post['eredeti_ar'])) {
            $this->data['eredeti_ar'] = $this->request->post['eredeti_ar'];
        } elseif (!empty($product_info)) {
            //$this->data['kedvezmeny'] = $this->currency->format( $this->tax->calculate((int)$product_info['kedvezmeny'], $product_info['tax_class_id'], $this->config->get('config_tax') ) );
            $this->data['eredeti_ar'] = $this->tax->calculate((int)$product_info['eredeti_ar'], $product_info['tax_class_id'], $this->config->get('config_tax') ) ;
        } else {
            $this->data['eredeti_ar'] = '';
        }

        if (isset($this->request->post['szazalek'])) {
            $this->data['szazalek'] = $this->request->post['szazalek'];
        } elseif (!empty($product_info)) {
            $this->data['szazalek'] = $product_info['szazalek'];
        } else {
            $this->data['szazalek'] = 0;
        }

        //$this->load->model('account/profile');

        //$this->data['profiles'] = $this->model_account_profile->getProfiles();

        /* if (isset($this->request->post['product_profiles'])) {
      		$this->data['product_profiles'] = $this->request->post['product_profiles'];
    	} elseif (!empty($product_info)) {
			$this->data['product_profiles'] = $this->model_account_product->getProfiles($product_info['product_id']);
		} else {
      		$this->data['product_profiles'] = array();
    	}*/

        $this->load->model('account/elhelyezkedes');
        $this->data['elhelyezkedesek_selects'] = $this->model_account_elhelyezkedes->getElhelyezkedesek();

        $this->load->model('account/elhelyezkedes_alcsoport');
        $this->data['elhelyezkedesek_alcsoport_selects'] = $this->model_account_elhelyezkedes_alcsoport->getAlcsoportElhelyezkedesek();

        $this->load->model('account/kiemelesek');
        $this->data['kiemelesek_selects'] = $this->model_account_kiemelesek->getKiemelesek(isset($product_info['utalvany']) ? $product_info['utalvany'] : 0);

        $this->data['penznem'] =  " ". $this->currency->getFizetoeszkoz();


        $idoszakok = array();

        $idoszakok[] = array(
            'id'    => '1',
            'name'  => $this->language->get('text_nap')
        );
        $idoszakok[] = array(
            'id'    => '2',
            'name'  => $this->language->get('text_het')
        );
        $idoszakok[] = array(
            'id'    => '3',
            'name'  => $this->language->get('text_honap')
        );
        $idoszakok[] = array(
            'id'    => '4',
            'name'  => $this->language->get('text_ev')
        );

        $this->data['idoszakok'] = $idoszakok;


        if (isset($this->request->post['product_elhelyezkedes'])) {
            $this->data['product_elhelyezkedess'] = $this->request->post['product_elhelyezkedes'];
            if ( isset($this->data['product_elhelyezkedess']['NULL']) ) {
                unset($this->data['product_elhelyezkedess']['NULL']);
            }
        } elseif (isset($this->request->get['product_id'])) {
            $this->load->model('catalog/product');

            $this->data['product_elhelyezkedess'] = $this->model_catalog_product->getProductElhelyezkedes($this->request->get['product_id']);
        } else {
            $this->data['product_elhelyezkedess'] = array();
        }

        $sort_order = array();

        foreach ($this->data['product_elhelyezkedess'] as $key => $value) {
            $sort_order[$key] = $value['priority'];
        }

        array_multisort($sort_order, SORT_ASC, $this->data['product_elhelyezkedess']);
        $this->data['tax_class_id'] = 0;

		if (isset($this->request->post['date_available'])) {
       		$this->data['date_available'] = $this->request->post['date_available'];
		} elseif (!empty($product_info)) {
			$this->data['date_available'] = date('Y-m-d', strtotime($product_info['date_available']));
		} else {
			$this->data['date_available'] = date('Y-m-d', time() - 86400);
		}

        if (isset($this->request->post['date_ervenyes_ig'])) {
       		$this->data['date_ervenyes_ig'] = $this->request->post['date_ervenyes_ig'];
		} elseif (!empty($product_info)) {
            if ($product_info['date_ervenyes_ig'] == "0000-00-00")  {
			    $this->data['date_ervenyes_ig'] = "";
            } else {
                $this->data['date_ervenyes_ig'] = $product_info['date_ervenyes_ig'];
            }
		} else {
			$this->data['date_ervenyes_ig'] = date('Y-m-d');
		}
        $config_megjelenit_admin_product = $this->config->get("megjelenit_admin_product");
        $this->data['config_lejarat_nelkuli'] = isset($config_megjelenit_admin_product['lejarat_nelkuli']) ? $config_megjelenit_admin_product['lejarat_nelkuli'] : 1;
        $this->data['config_lejarat_nelkuli_neve'] = isset($config_megjelenit_admin_product['lejarat_nelkuli_neve']) ? $config_megjelenit_admin_product['lejarat_nelkuli_neve'] : '';
        $this->data['config_lejarat_nelkuli_default'] = isset($config_megjelenit_admin_product['lejarat_nelkuli_default']) ? $config_megjelenit_admin_product['lejarat_nelkuli_default'] : 1;

    	if (isset($this->request->post['quantity'])) {
      		$this->data['quantity'] = $this->request->post['quantity'];
    	} elseif (!empty($product_info)) {
      		$this->data['quantity'] = $product_info['quantity'];
    	} else {
			$this->data['quantity'] = 1;
		}

		if (isset($this->request->post['minimum'])) {
      		$this->data['minimum'] = $this->request->post['minimum'];
    	} elseif (!empty($product_info)) {
      		$this->data['minimum'] = $product_info['minimum'];
    	} else {
			$this->data['minimum'] = 1;
		}

		if (isset($this->request->post['subtract'])) {
      		$this->data['subtract'] = $this->request->post['subtract'];
    	} elseif (!empty($product_info)) {
      		$this->data['subtract'] = $product_info['subtract'];
    	} else {
			$this->data['subtract'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
      		$this->data['sort_order'] = $this->request->post['sort_order'];
    	} elseif (!empty($product_info)) {
      		$this->data['sort_order'] = $product_info['sort_order'];
    	} else {
			$this->data['sort_order'] = 1;
		}

		$this->load->model('localisation/stock_status');

		$this->data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

		if (isset($this->request->post['stock_status_id'])) {
      		$this->data['stock_status_id'] = $this->request->post['stock_status_id'];
    	} elseif (!empty($product_info)) {
      		$this->data['stock_status_id'] = $product_info['stock_status_id'];
    	} else {
			$this->data['stock_status_id'] = $this->config->get('config_stock_status_id');
		}

    	if (isset($this->request->post['status'])) {
      		$this->data['status'] = $this->request->post['status'];
    	} elseif (!empty($product_info)) {
			$this->data['status'] = $product_info['status'];
		} else {
      		$this->data['status'] = 1;
    	}

    	if (isset($this->request->post['weight'])) {
      		$this->data['weight'] = $this->request->post['weight'];
		} elseif (!empty($product_info)) {
			$this->data['weight'] = $product_info['weight'];
    	} else {
      		$this->data['weight'] = '';
    	}

		$this->load->model('localisation/weight_class');

		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

		if (isset($this->request->post['weight_class_id'])) {
      		$this->data['weight_class_id'] = $this->request->post['weight_class_id'];
    	} elseif (!empty($product_info)) {
      		$this->data['weight_class_id'] = $product_info['weight_class_id'];
		} else {
      		$this->data['weight_class_id'] = $this->config->get('config_weight_class_id');
    	}

		if (isset($this->request->post['length'])) {
      		$this->data['length'] = $this->request->post['length'];
    	} elseif (!empty($product_info)) {
			$this->data['length'] = $product_info['length'];
		} else {
      		$this->data['length'] = '';
    	}

		if (isset($this->request->post['width'])) {
      		$this->data['width'] = $this->request->post['width'];
		} elseif (!empty($product_info)) {
			$this->data['width'] = $product_info['width'];
    	} else {
      		$this->data['width'] = '';
    	}

		if (isset($this->request->post['height'])) {
      		$this->data['height'] = $this->request->post['height'];
		} elseif (!empty($product_info)) {
			$this->data['height'] = $product_info['height'];
    	} else {
      		$this->data['height'] = '';
    	}

		$this->load->model('localisation/length_class');

		$this->data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();

		if (isset($this->request->post['length_class_id'])) {
      		$this->data['length_class_id'] = $this->request->post['length_class_id'];
    	} elseif (!empty($product_info)) {
      		$this->data['length_class_id'] = $product_info['length_class_id'];
    	} else {
      		$this->data['length_class_id'] = $this->config->get('config_length_class_id');
		}

		$this->load->model('account/manufacturer');

    	if (isset($this->request->post['manufacturer_id'])) {
      		$this->data['manufacturer_id'] = $this->request->post['manufacturer_id'];
		} elseif (!empty($product_info)) {
			$this->data['manufacturer_id'] = $product_info['manufacturer_id'];
		} else {
      		$this->data['manufacturer_id'] = 0;
    	}

    	if (isset($this->request->post['manufacturer'])) {
      		$this->data['manufacturer'] = $this->request->post['manufacturer'];
		} elseif (!empty($product_info)) {
			$manufacturer_info = $this->model_account_manufacturer->getManufacturer($product_info['manufacturer_id']);

			if ($manufacturer_info) {
				$this->data['manufacturer'] = $manufacturer_info['name'];
			} else {
				$this->data['manufacturer'] = '';
			}
		} else {
      		$this->data['manufacturer'] = '';
    	}

		// Categories
		$this->load->model('account/category');

		if (isset($this->request->post['product_category'])) {
			$categories = $this->request->post['product_category'];
		} elseif (isset($this->request->get['product_id'])) {
			$categories = $this->model_account_product->getProductCategories($this->request->get['product_id']);
		} else {
			$categories = array();
		}

		$this->data['product_categories'] = array();

		foreach ($categories as $category_id) {
			$category_info = $this->model_account_category->getCategory($category_id);

			if ($category_info) {
				$this->data['product_categories'][] = array(
					'category_id' => $category_info['category_id'],
					'name'        => ($category_info['path'] ? $category_info['path'] . ' &gt; ' : '') . $category_info['name']
				);
			}
		}

		// Filters
		$this->load->model('account/filter');

		if (isset($this->request->post['product_filter'])) {
			$filters = $this->request->post['product_filter'];
		} elseif (isset($this->request->get['product_id'])) {
			$filters = $this->model_account_product->getProductFilters($this->request->get['product_id']);
		} else {
			$filters = array();
		}



		// Attributes
		$this->load->model('account/attribute');

		if (isset($this->request->post['product_attribute'])) {
			$product_attributes = $this->request->post['product_attribute'];
		} elseif (isset($this->request->get['product_id'])) {
			$product_attributes = $this->model_account_product->getProductAttributes($this->request->get['product_id']);
		} else {
			$product_attributes = array();
		}

		$this->data['product_attributes'] = array();

		foreach ($product_attributes as $product_attribute) {
			$attribute_info = $this->model_account_attribute->getAttribute($product_attribute['attribute_id']);

			if ($attribute_info) {
				$this->data['product_attributes'][] = array(
					'attribute_id'                  => $product_attribute['attribute_id'],
					'name'                          => $attribute_info['name'],
					'product_attribute_description' => $product_attribute['product_attribute_description']
				);
			}
		}

		// Options
		$this->load->model('account/option');

		if (isset($this->request->post['product_option'])) {
			$product_options = $this->request->post['product_option'];
		} elseif (isset($this->request->get['product_id'])) {
			$product_options = $this->model_account_product->getProductOptions($this->request->get['product_id']);
		} else {
			$product_options = array();
		}

		$this->data['product_options'] = array();

		foreach ($product_options as $product_option) {
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
				$product_option_value_data = array();

				foreach ($product_option['product_option_value'] as $product_option_value) {
					$product_option_value_data[] = array(
						'product_option_value_id' => $product_option_value['product_option_value_id'],
						'option_value_id'         => $product_option_value['option_value_id'],
						'quantity'                => $product_option_value['quantity'],
						'subtract'                => $product_option_value['subtract'],
						'price'                   => $product_option_value['price'],
						'price_prefix'            => $product_option_value['price_prefix'],
						'points'                  => $product_option_value['points'],
						'points_prefix'           => $product_option_value['points_prefix'],
						'weight'                  => $product_option_value['weight'],
						'weight_prefix'           => $product_option_value['weight_prefix']
					);
				}

				$this->data['product_options'][] = array(
					'product_option_id'    => $product_option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $product_option['option_id'],
					'name'                 => $product_option['name'],
					'type'                 => $product_option['type'],
					'required'             => $product_option['required']
				);
			} else {
				$this->data['product_options'][] = array(
					'product_option_id' => $product_option['product_option_id'],
					'option_id'         => $product_option['option_id'],
					'name'              => $product_option['name'],
					'type'              => $product_option['type'],
					'option_value'      => $product_option['option_value'],
					'required'          => $product_option['required']
				);
			}
		}

		$this->data['option_values'] = array();

		foreach ($this->data['product_options'] as $product_option) {
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
				if (!isset($this->data['option_values'][$product_option['option_id']])) {
					$this->data['option_values'][$product_option['option_id']] = $this->model_account_option->getOptionValues($product_option['option_id']);
				}
			}
		}





		if (isset($this->request->post['product_discount'])) {
			$this->data['product_discounts'] = $this->request->post['product_discount'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_discounts'] = $this->model_account_product->getProductDiscounts($this->request->get['product_id']);
		} else {
			$this->data['product_discounts'] = array();
		}

		if (isset($this->request->post['product_special'])) {
			$this->data['product_specials'] = $this->request->post['product_special'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_specials'] = $this->model_account_product->getProductSpecials($this->request->get['product_id']);
		} else {
			$this->data['product_specials'] = array();
		}

		// Images
		if (isset($this->request->post['product_image'])) {
			$product_images = $this->request->post['product_image'];
		} elseif (isset($this->request->get['product_id'])) {
			$product_images = $this->model_account_product->getProductImages($this->request->get['product_id']);
		} else {
			$product_images = array();
		}

		$this->data['product_images'] = array();

		foreach ($product_images as $product_image) {
			if ($product_image['image'] && file_exists(DIR_IMAGE . $product_image['image'])) {
				$image = $product_image['image'];
			} else {
				$image = 'no_image.jpg';
			}

			$this->data['product_images'][] = array(
				'image'      => $image,
				'thumb'      => $this->model_tool_image->resize($image, 100, 100),
				'sort_order' => $product_image['sort_order']
			);
		}

		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

		// Downloads
		$this->load->model('account/download');

		if (isset($this->request->post['product_download'])) {
			$product_downloads = $this->request->post['product_download'];
		} elseif (isset($this->request->get['product_id'])) {
			$product_downloads = $this->model_account_product->getProductDownloads($this->request->get['product_id']);
		} else {
			$product_downloads = array();
		}

		$this->data['product_downloads'] = array();

		foreach ($product_downloads as $download_id) {
			$download_info = $this->model_account_download->getDownload($download_id);

			if ($download_info) {
				$this->data['product_downloads'][] = array(
					'download_id' => $download_info['download_id'],
					'name'        => $download_info['name']
				);
			}
		}

		if (isset($this->request->post['product_related'])) {
			$products = $this->request->post['product_related'];
		} elseif (isset($this->request->get['product_id'])) {
			$products = $this->model_account_product->getProductRelated($this->request->get['product_id']);
		} else {
			$products = array();
		}

		$this->data['product_related'] = array();

		foreach ($products as $product_id) {
			$related_info = $this->model_account_product->getProduct($product_id);

			if ($related_info) {
				$this->data['product_related'][] = array(
					'product_id' => $related_info['product_id'],
					'name'       => $related_info['name']
				);
			}
		}

    	if (isset($this->request->post['points'])) {
      		$this->data['points'] = $this->request->post['points'];
    	} elseif (!empty($product_info)) {
			$this->data['points'] = $product_info['points'];
		} else {
      		$this->data['points'] = '';
    	}

		if (isset($this->request->post['product_reward'])) {
			$this->data['product_reward'] = $this->request->post['product_reward'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_reward'] = $this->model_account_product->getProductRewards($this->request->get['product_id']);
		} else {
			$this->data['product_reward'] = array();
		}

		if (isset($this->request->post['product_layout'])) {
			$this->data['product_layout'] = $this->request->post['product_layout'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_layout'] = $this->model_account_product->getProductLayouts($this->request->get['product_id']);
		} else {
			$this->data['product_layout'] = array();
		}

        $this->data['mezok']                    = $this->config->get("megjelenit_admin_product");
        $this->data['megjelenit_termekadat']    = $this->config->get('megjelenit_admin_termekadatok');
        $this->data['megjelenit_altalanos']    = $this->config->get('megjelenit_altalanos');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/product_form.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/account/product_form.tpl';
			} else {
				$this->template = 'default/template/account/product_form.tpl';
			}

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
  	}

  	protected function validateForm() {


        if ($this->config->get("megjelenit_admin_nyelvi_ellenorzes") == 1){
        	foreach ($this->request->post['product_description'] as $language_id => $value) {
          		if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 255)) {
            		$this->error['name'][$language_id] = $this->language->get('error_name');
      		    }
    	    }
    	} else {
            if ( utf8_strlen( $this->request->post['product_description'][$this->config->get("config_language_id")]['name'] ) < 3
                ||  utf8_strlen($this->request->post['product_description'][$this->config->get("config_language_id")]['name'] ) > 255 ) {
                $this->error['name'][$this->config->get("config_language_id")] = $this->language->get('error_name');
            }
        }
        $mezok = $this->config->get("megjelenit_admin_product");
        if (array_key_exists('category_ellenorzes', $mezok) && $mezok['category_ellenorzes'] == 1 ) {
            if( !array_key_exists('product_category', $this->request->request)) {
                $this->error['category'] = $this->language->get('error_category_mandatory');
            } elseif(count($this->request->request['product_category']) == 0) {
                $this->error['category'] = $this->language->get('error_category_mandatory');
            }
        }

        if (array_key_exists('description_ellenorzes', $mezok) && $mezok['description_ellenorzes'] == 1 ) {
            if(array_key_exists('product_description', $this->request->request)) {
                $keys = array_keys($this->request->request['product_description']);
                $key = $keys[0];

                if(array_key_exists('description', $this->request->request['product_description'][$key])) {
                    if($this->request->request['product_description'][$key]['description'] == '') {
                        $this->error['description'] = $this->language->get('error_description_mandatory');
                    }
                } else {
                    $this->error['description'] = $this->language->get('error_description_mandatory');
                }
            } else {
                $this->error['description'] = $this->language->get('error_description_mandatory');
            }
        }

        $ellenoriz = $this->config->get('megjelenit_admin_product');

        if ($ellenoriz['price_ellenorzes'] == 1 && $ellenoriz['price'] == 1){
            if ( $this->request->post['price'] >= 0) {
                $this->error['price'] = $this->language->get('error_price');
            }
        }

        if ($ellenoriz['filter_csoport_ellenorzes'] == 1 && $ellenoriz['filter_csoport'] == 1){
            if (!isset($this->request->post['product_filter'])) {
                $this->error['product_filter'] = $this->language->get('error_product_filter');
            }
        }
        if ($ellenoriz['date_ervenyes_ig_ellenorzes'] == 1 && $ellenoriz['date_ervenyes_ig'] == 1){
            if ( $this->request->post['date_ervenyes_ig'] < date('Y-m-d') && $_REQUEST['meddig_kaphato'] == 0 ) {
                $this->error['date_ervenyes_ig'] = $this->language->get('error_date_ervenyes_ig');
            }
        }

        if ($ellenoriz['letoltheto_ellenorzes'] == 1 && $ellenoriz['letoltheto'] == 1){
            if ( !isset($this->request->post['letoltheto']) || empty($this->request->post['letoltheto'])  ) {
                $this->error['letoltheto'] = $this->language->get('error_letoltheto');
            } elseif(strpos($this->request->post['letoltheto'],"no_image")){
                $this->error['letoltheto'] = $this->language->get('error_letoltheto');
            }
        }

    	/*if ((utf8_strlen($this->request->post['model']) < 1) || (utf8_strlen($this->request->post['model']) > 64)) {
      		 $this->error['model'] = $this->language->get('error_model');
    	}*/

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}

  	protected function validateDelete() {

        /*$this->load->library('user');

        $this->user = new User($this->registry);

    	if (!$this->user->hasPermission('modify', 'account/product')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}*/

		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}

  	protected function validateCopy() {
        /*$this->load->library('user');

        $this->user = new User($this->registry);

    	if (!$this->user->hasPermission('modify', 'account/product')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}*/

		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model']) || isset($this->request->get['filter_category_id'])) {
			$this->load->model('account/product');
			$this->load->model('account/option');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			} else {
				$filter_model = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 20;
			}

			$data = array(
				'filter_name'  => $filter_name,
				'filter_model' => $filter_model,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_account_product->getProducts($data);

			foreach ($results as $result) {
				$option_data = array();

				$product_options = $this->model_account_product->getProductOptions($result['product_id']);

				foreach ($product_options as $product_option) {
					$option_info = $this->model_account_option->getOption($product_option['option_id']);

					if ($option_info) {
						if ($option_info['type'] == 'select' || $option_info['type'] == 'radio' || $option_info['type'] == 'checkbox' || $option_info['type'] == 'image') {
							$option_value_data = array();

							foreach ($product_option['product_option_value'] as $product_option_value) {
								$option_value_info = $this->model_account_option->getOptionValue($product_option_value['option_value_id']);

								if ($option_value_info) {
									$option_value_data[] = array(
										'product_option_value_id' => $product_option_value['product_option_value_id'],
										'option_value_id'         => $product_option_value['option_value_id'],
										'name'                    => $option_value_info['name'],
										'price'                   => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
										'price_prefix'            => $product_option_value['price_prefix']
									);
								}
							}

							$option_data[] = array(
								'product_option_id' => $product_option['product_option_id'],
								'option_id'         => $product_option['option_id'],
								'name'              => $option_info['name'],
								'type'              => $option_info['type'],
								'option_value'      => $option_value_data,
								'required'          => $product_option['required']
							);
						} else {
							$option_data[] = array(
								'product_option_id' => $product_option['product_option_id'],
								'option_id'         => $product_option['option_id'],
								'name'              => $option_info['name'],
								'type'              => $option_info['type'],
								'option_value'      => $product_option['option_value'],
								'required'          => $product_option['required']
							);
						}
					}
				}

				$json[] = array(
					'product_id' => $result['product_id'],
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'model'      => $result['model'],
					'option'     => $option_data,
					'price'      => $result['price']
				);
			}
		}

		$this->response->setOutput(json_encode($json));
	}

    public function elhelyezkedesSzabadHely() {
        $json = array();

        $this->load->language('account/product');
        $this->load->model('account/elhelyezkedes');
        $eredmeny = $this->model_account_elhelyezkedes->getSzabadHelyek();

        if ($eredmeny !== true) {
            if ($eredmeny['elhelyezkedesek_kulonbseg'] < 1 ) {
                $json['error']['elhelyezkedes'] = $this->language->get('elhelyezkedesek_kulonbseg_error');
            }

            if ($eredmeny['elhelyezkedes_alcsoport_kulonbseg']  < 1 ) {
                $json['error']['elhelyezkedes_alcsoport'] = $this->language->get('elhelyezkedes_alcsoport_kulonbseg_error');
            }

            if ($eredmeny['kiemelesek_kulonbseg']  < 1 ) {
                $json['error']['kiemelesek'] = $this->language->get('kiemelesek_kulonbseg_error');
            }

            if (!isset($json['error'])) {
                $json['succsess'] = $this->language->get('elhelyezkedesek_succsess');
            }
        }
        $json['sor'] = $_REQUEST['sor'];

        $this->response->setOutput(json_encode($json));
    }

    public function requestsecuritycode($productid=null) {
        if($productid == null) {
            if(!isset($this->request->request['productid'])) {
                return false;
            }

            if(!is_numeric($this->request->request['productid'])) {
                return false;
            }
            $productid = $this->request->request['productid'];
        }



        /**
         * @var Customer $customer
         */
        $customer = $this->registry->get('customer');
        if(!$customer instanceof Customer) {
            return false;
        }

        $email = $customer->getEmail();

        if(empty($email)) {
            return false;
        }

        $securitycode = rand(100000, 999999);

        $this->load->language('account/product');
        $subject = $this->language->get('email_biztonsagi_kod_subject');
        $message = sprintf($this->language->get('email_biztonsagi_kod_message'), $securitycode);

        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->hostname = $this->config->get('config_smtp_host');
        $mail->username = $this->config->get('config_smtp_username');
        $mail->password = $this->config->get('config_smtp_password');
        $mail->port = $this->config->get('config_smtp_port');
        $mail->timeout = $this->config->get('config_smtp_timeout');
        $mail->setTo($email);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender($this->config->get('config_name'));
        $mail->setSubject($subject);

        $mail->setText($message);
        //$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
        $mail->send();

        $sql = "UPDATE ".DB_PREFIX."product SET securitycode='".$securitycode."' WHERE product_id=".$productid;
        $this->db->query($sql);

        $json = array(
            'success'           => 1
        );

        $this->response->setOutput(json_encode($json));
    }

    public function checksecuritycode() {
        if(!isset($this->request->request['productid'])) {
            return false;
        }

        if(!is_numeric($this->request->request['productid'])) {
            return false;
        }

        $productid = $this->request->request['productid'];

        if(!isset($this->request->request['securitycode'])) {
            return false;
        }

        if(!is_numeric($this->request->request['securitycode'])) {
            return false;
        }

        $userSecurityCode = $this->request->request['securitycode'];

        $sql = "SELECT securitycode FROM ".DB_PREFIX."product WHERE product_id=".$productid;
        $query = $this->db->query($sql);

        $securityCode = null;
        if(property_exists($query, 'row') && array_key_exists('securitycode', $query->row)) {
            $securityCode = $query->row['securitycode'];
        }

        $json = array();
        if($securityCode == $userSecurityCode) {
            $json = array(
                'success'           => 1
            );

            $sql = "UPDATE ".DB_PREFIX."product SET correctsecuritycode=1 WHERE product_id=".$productid;
            $this->db->query($sql);
        } else {
            $json = array(
                'success'           => 0
            );
        }

        $this->response->setOutput(json_encode($json));
    }

    public function checkSecurityCodePage() {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

            $this->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->language->load('account/product');
        $this->document->setTitle($this->language->get('heading_title'));


        $this->getFormSecurityCode();
    }

    public function getFormSecurityCode() {
        $this->load->model('setting/extension');
        $no_module= $this->model_setting_extension->getNoModule();
        if (in_array("language",$no_module)){
            $this->nyelv = false;
        }

        $product_id = $this->request->request['product_id'];
        $this->data['productid'] = $product_id;

        $this->data['button_sending'] = $this->language->get('button_sending');
        $this->data['text_biztonsagi_kod_igenyles'] = $this->language->get('text_biztonsagi_kod_igenyles');
        $this->data['text_biztonsagi_kod'] = $this->language->get('text_biztonsagi_kod');

        $this->data['text_biztonsagi_kod_elkuldve'] = $this->language->get('text_biztonsagi_kod_elkuldve');
        $this->data['text_sikeres_biztonsagi_kod'] = $this->language->get('text_sikeres_biztonsagi_kod');
        $this->data['text_sikertelen_biztonsagi_kod'] = $this->language->get('text_sikertelen_biztonsagi_kod');
        $this->data['text_biztonsagi_kod_nincs_elkuldve'] = $this->language->get('text_biztonsagi_kod_nincs_elkuldve');
        $this->data['text_biztonsagi_kod_kuldese'] = $this->language->get('text_biztonsagi_kod_kuldese');
        $this->data['button_back'] = $this->language->get('button_back');
        $this->data['heading_title'] = $this->language->get('heading_securitycode');

        $this->data['back'] = $this->url->link('account/product', 'SSL');

        if(isset($this->request->request['new']) && $this->request->request['new'] == 1) {
            $this->data['new'] = 1;
        } else {
            $this->data['new'] = 0;
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/product_checksecuritycode.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/product_checksecuritycode.tpl';
        } else {
            $this->template = 'default/template/account/product_checksecuritycode.tpl';
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'SSL'),
            'separator' => false
        );

        $this->language->load('account/account');

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('account/product', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }
}
?>