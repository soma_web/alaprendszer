<?php
class ControllerExtensionFaq extends Controller { 
	private $error = array();
	
	public function index() {
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');
		$this->document->setTitle($this->language->get('heading_title')); 
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tabQuestions'] = $this->language->get('text_tabQuestions');
		$this->data['text_tabCategories'] = $this->language->get('text_tabCategories');
		$this->data['text_deleteQuestions'] = $this->language->get('text_deleteQuestions');
		$this->data['text_addQuestion'] = $this->language->get('text_addQuestion');
		$this->data['text_search'] = $this->language->get('text_search');
		$this->data['text_resetSearch'] = $this->language->get('text_resetSearch');
		$this->data['text_deleteCategories'] = $this->language->get('text_deleteCategories');
		$this->data['text_addCategory'] = $this->language->get('text_addCategory');
		$this->data['text_noQuestions'] = $this->language->get('text_noQuestions');
		$this->data['text_noCategories'] = $this->language->get('text_noCategories');
		$this->data['text_save'] = $this->language->get('text_save');
		$this->data['text_questions'] = $this->language->get('text_questions');
		$this->data['text_categories'] = $this->language->get('text_categories');
		$this->data['text_nameCategory'] = $this->language->get('text_nameCategory');
		
		$this->data['column_category'] = $this->language->get('column_category');
		$this->data['column_question'] = $this->language->get('column_question');
		$this->data['column_response'] = $this->language->get('column_response');
		$this->data['column_action'] = $this->language->get('column_action');
		
		$this->data['text_titleAddQuestion'] = $this->language->get('text_titleAddQuestion');
		$this->data['text_titleAddCategory'] = $this->language->get('text_titleAddCategory');
		
		
		
		$url = "";

		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/faq', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);

		$this->model_extension_faq->creationTables();	

		$this->data['faq_categories'] = array();
		$i=0;
		
		$faqCategories_Cat = $this->model_extension_faq->getFaqCategories();		
		foreach($faqCategories_Cat as $cat){
			$this->data['faq_categories'][$i]['idCategory'] = $cat['idCategory'];			
			$this->data['faq_categories'][$i]['category'] = $cat['category'];
			$i++;
		}

		$this->data['token'] = $this->session->data['token'];
		$this->data['linkListQuestions'] = html_entity_decode ($this->url->link('extension/faq/getListQuestions', 'token=' . $this->session->data['token'], 'SSL'));
		$this->data['linkListCategories'] = html_entity_decode ($this->url->link('extension/faq/getListCategories', 'token=' . $this->session->data['token'], 'SSL'));
		$this->data['linkGetSelectCategories'] = html_entity_decode ($this->url->link('extension/faq/getSelectCategories', 'token=' . $this->session->data['token'], 'SSL'));	
		$this->data['pagination'] = html_entity_decode ($this->url->link('extension/faq/pagination', 'token=' . $this->session->data['token'], 'SSL'));
		
		$this->data['addQuestion'] = $this->url->link('extension/faq/insert');
		$this->data['addCategory'] = $this->url->link('extension/faq/insertCategory', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('extension/faq/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['deleteCategory'] = $this->url->link('extension/faq/deleteCategory', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['actionAddQuestion'] = $this->url->link('extension/faq/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['actionAddCategory'] = $this->url->link('extension/faq/insertCategory', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['actionEditQuestion'] = $this->url->link('extension/faq/update', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['actionEditCategory'] = $this->url->link('extension/faq/updateCategory', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		//$this->getList();
		
		$this->template = 'extension/faq.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
				
		$this->response->setOutput($this->render());
		
	}

	public function getSelectCategories(){
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');		
		$faq_categories = array();
		$i=0;
		
		$faqCategories_Cat = $this->model_extension_faq->getFaqCategories();		
		foreach($faqCategories_Cat as $cat){
			$faq_categories[$i]['idCategory'] = $cat['idCategory'];			
			$faq_categories[$i]['category'] = $cat['category'];
			$i++;
		}
		echo'<select id="selectCategory" name="category">';
		foreach ($faq_categories as $cat) { 
			echo '<option value="'.$cat['idCategory'].'">'.urldecode(rawurldecode($cat['category'])).'</option>';
		}
	   	echo '</select>';
	}


	public function getListQuestions(){
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');		
		$url = '';
		$faqs = array();
		if(!isset($_POST['sort'])){
			$sort = 'ASC';
			$column = 'category';
			$_POST['sort'] = 'ASC';
		}
		else {
			$sort = $_POST['sort'];
		}
		
		if(isset($_POST['query']) && $_POST['query'] != ""){
			$results = $this->model_extension_faq->getFaqsQuery($_POST['query'], $sort, $_POST['column']);
		}
		else{
			$results = $this->model_extension_faq->getFaqs($_POST['limitMin'], $_POST['limitMax'], $sort, $_POST['column']);
		}

 
    	foreach ($results as $result) {
			$action = array();
						
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('extension/faq/update', 'token=' . $this->session->data['token'] . '&idFaq=' . $result['idFaq'] . $url, 'SSL')
			);
						
			$faqs[] = array(
				'idFaq' => $result['idFaq'],
				'category'     => $result['category'],
				'idCategory'     => $result['idCategory'],
				'question'     => $result['question'],
				'response'     => $result['response'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['idFaq'], $this->request->post['selected']),
				'action'         => $action
			);
		}
		
		$sort_question = $this->url->link('extension/faq', 'token=' . $this->session->data['token']. $url, 'SSL');
	
		$sort_category = $this->url->link('extension/faq', 'token=' . $this->session->data['token'] .  $url, 'SSL');
		
		echo "
			<script type=text/javascript>	
				$('.editQuestion').click(function(){	
					$('#titleFormQuestion').text(\"Edit A Question\");
					$('#faq_idFaq').val($(this).attr('id'));
					$('#selectCategory').val($(this).attr('category'));
					$('#faq_question').val($(this).attr('question'));
					CKEDITOR.instances.response1.destroy();
					$('#textAreaTd').html('<textarea name=\"response\" id=\"response1\">'+$(this).attr('response')+'</textarea>');
					CKEDITOR.replace('response1', {
						filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=".$this->session->data['token']."',
						filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=".$this->session->data['token']."',
						filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=".$this->session->data['token']."',
						filebrowserUploadUrl: 'index.php?route=common/filemanager&token=".$this->session->data['token']."',
						filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=".$this->session->data['token']."',
						filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=".$this->session->data['token']."',
					});			
					$('#addQuestionForm').attr('action',$('#linkEditQuestion').val());
					$('#listQuestions').css('display', 'none');
					$('#tabFormQuestion').css('display', 'block');
					return false;
				});
				
				$('.sortCategory').click(function(){	
					$('#searchQuestion').val('');
					var limit = 15;
					limitMin = ($('#currentPage').val()-1)*limit;
					refreshListQuestions(limitMin, limit, $('#sortCategory').val(), 'category');
					if($('#sortCategory').val() == 'DESC'){
						$('#sortCategory').val('ASC');
					}
					else{
						$('#sortCategory').val('DESC');		
					}
					return false;
				});
				
				$('.sortQuestion').click(function(){	
					$('#searchQuestion').val('');
					var limit = 15;
					limitMin = ($('#currentPage').val()-1)*limit;
					refreshListQuestions(limitMin, limit, $('#sortQuestion').val(), 'question');
					if($('#sortQuestion').val() == 'DESC'){
						$('#sortQuestion').val('ASC');
					}
					else{
						$('#sortQuestion').val('DESC');		
					}
					return false;
				});
			</script>";
		echo'<table class="list">
				<thead>
					<tr>
						<td width="1" style="text-align: center;">
							<input type="checkbox" onclick="$(\'input[name*=\\\'selected\\\']\').attr(\'checked\', this.checked);" />
						</td>
						<td class="left" width="200" >';
						if(isset($_POST['sort']) && isset($_POST['column']) && $_POST['column'] == "category"){
							if($_POST['sort'] == "ASC"){
								echo '<a class="sortCategory" style="padding-right:15px;background:url(view/image/asc.png) no-repeat right;">'.$this->language->get('column_category').'</a>';
							}
							else{
								echo '<a class="sortCategory" style="padding-right:15px;background:url(view/image/desc.png) no-repeat right;">'.$this->language->get('column_category').'</a>';	
							}
						}
						else{
							echo '<a class="sortCategory">'.$this->language->get('column_category').'</a>';
						}
					echo'</td>
						<td class="left">';
							if(isset($_POST['sort']) && isset($_POST['column']) && $_POST['column'] == "question"){
								if($_POST['sort'] == "ASC"){
									echo '<a class="sortQuestion" style="padding-right:15px;background:url(view/image/asc.png) no-repeat right;">'.$this->language->get('column_question').'</a>';
								}
								else{
									echo '<a class="sortQuestion" style="padding-right:15px;background:url(view/image/desc.png) no-repeat right;">'.$this->language->get('column_question').'</a>';	
								}
							}
							else{
										echo '<a class="sortQuestion">'.$this->language->get('column_question').'</a>';	
							}
					echo'</td>
					<td class="right" width="50">'.$this->language->get('column_action').'</td>
					</tr>
				</thead>';
				echo '<tbody>';
					if ($faqs) { 
						$i=0;
						foreach ($faqs as $faq) {
							echo '<tr>';
							if($i % 2 == 0){
								$color = 'background-color:#FFFFFF;';
							}
							else{
								$color = 'background-color:#EFEFEF;';
							}
							echo '<td style="text-align: center;'.$color.'">';
							//if ($faq['selected']) {
							//	echo '<input type="checkbox" name="selected[]" value="'.$faq['idFaq'].'" checked="checked" />';
							//} 
							//else {
								echo '<input type="checkbox" name="selected[]" value="'.$faq['idFaq'].'" />';
							//}
							echo'</td>
								<td class="left" style="'.$color.'">'.urldecode(rawurldecode($faq['category'])).'</td>
								<td class="left" style="'.$color.'">'.urldecode(rawurldecode($faq['question'])).'</td>					
								<td class="right" style="'.$color.'">
									[ <a class="editQuestion" id="'.$faq['idFaq'].'" question="'.urldecode(rawurldecode($faq['question'])).'" response="'. htmlspecialchars(urldecode(rawurldecode($faq['response']))).'" category="'.$faq['idCategory'].'">Edit</a> ]
								</td>';
							echo '</tr>';
							$i++;
						}
					}
					else {
						echo '<tr>
								<td class="center" colspan="4">'.$this->language->get('text_noQuestions').'</td>
							</tr>';
					}
				echo'</tbody>';
				echo '</table>';
	}
	
	public function getListCategories(){
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');		
		$url = '';
		$faqs = array();
 
    	$faqCategories = array();
		
		$faqCategories_total = $this->model_extension_faq->getTotalFaqCategories();
	
		$results = $this->model_extension_faq->getFaqCategories(array());
 
		foreach ($results as $result) {
			$action = array();
						
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('extension/faq/updateCategory', 'token=' . $this->session->data['token'] . '&idCategory=' . $result['idCategory'] . $url, 'SSL')
			);
						
			$faqCategories[] = array(
				'idCategory' => $result['idCategory'],
				'category'          => $result['category'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['idCategory'], $this->request->post['selected']),
				'action'         => $action
			);
		}
		
		echo "
			<script type=text/javascript>	
				$('.editCategory').click(function(){	
					$('#faqCategories_id').val($(this).attr('id'));
					$('#faqCategories_question').val($(this).attr('name'));
					$('#addCategoryForm').attr('action',$('#linkEditCategory').val());
					$('#titleFormCat').text(\"Edit A Category\");
					$('#listCategories').css('display', 'none');
					$('#tabFormCategory').css('display', 'block');
					return false;
				});
			</script>";
		
		echo'<table class="list">
				  <thead>
					<tr>
					  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$(\'input[name*=\\\'selected\\\']\').attr(\'checked\', this.checked);" /></td>
					  <td class="left">'.$this->language->get('column_category').'</td>
						<td class="left" width="70"></td>
					  <td class="right" width="50">'.$this->language->get('column_action').'</td>
					</tr>
				  </thead>
				  <tbody>';
					if ($faqCategories) { 
						$i=0;
						foreach ($faqCategories as $faqCategories) {
							$nbQuesions = $this->model_extension_faq->getTotalFaqsByCategory($faqCategories['idCategory']);
							echo '<tr>';
							if($i % 2 == 0){
								$color = 'background-color:#FFFFFF;';
							}
							else{
								$color = 'background-color:#EFEFEF;';
							}
							echo'<td style="text-align: center;'.$color.'">';
								if ($faqCategories['selected']) {
									echo'<input type="checkbox" name="selected[]" value="'.$faqCategories['idCategory'].'" checked="checked" />';
								} 
								else {
									echo'<input type="checkbox" name="selected[]" value="'.$faqCategories['idCategory'].'" />';
								}
							echo'</td>
								<td class="left" style="'.$color.'">'.urldecode(rawurldecode($faqCategories['category'])).'</td>';
							if($nbQuesions[0]>1){
								echo'<td class="left" style="'.$color.';text-align:center;">'.$nbQuesions[0].$this->language->get('text_labelmore1Question').'</td>';
							}
							else if($nbQuesions[0]==1){
								echo'<td class="left" style="'.$color.';text-align:center;">'.$nbQuesions[0].$this->language->get('text_label0or1Question').'</td>';
							}	
							else{
								echo'<td class="left" style="'.$color.';text-align:center;color:red;">'.$nbQuesions[0].$this->language->get('text_label0or1Question').'</td>';
							}	
							echo'<td class="right" style="'. $color.'">
									[ <a class="editCategory" id="'.$faqCategories['idCategory'].'" name="'.html_entity_decode(urldecode(rawurldecode($faqCategories['category']))).'">Edit</a> ]
								</td>
							</tr>';
							$i++;
						}
					} 
					else{
					echo '<tr>
					  <td class="center" colspan="2">'.$this->language->get('text_noCategories').'</td>
					</tr>';
					}
				  echo'</tbody>
				</table>';
	}
	
	public function insert() {	
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		echo "<result>\n";
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');
		if($_POST['question'] == "" || $_POST['response'] == ""){
			echo "<status>warning</status>\n";
			echo "<message>".$this->language->get('error_allFieldsRequired')."</message>\n";
		}
		else if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_extension_faq->addFaq($this->request->post);
			echo "<status>success</status>\n";
			echo "<message>".$this->language->get('success_addQuestion')."</message>\n";
		}
		
		echo "</result>\n";
		exit();
	}
	
	public function insertCategory() {
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		echo "<result>\n";

		$this->load->model('extension/faq');
		$this->load->language('extension/faq');		
		if($_POST['category'] == ""){
			echo "<status>warning</status>\n";
			echo "<message>".$this->language->get('error_allFieldsRequired')."</message>\n";
		}
		else if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormCategory()) {
			$this->model_extension_faq->addFaqCategories($this->request->post);
			echo "<status>success</status>\n";
			echo "<message>".$this->language->get('success_addCategory')."</message>\n";
		}
		
		echo "</result>\n";
		exit();
	}

	public function update() {
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		echo "<result>\n";
		
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');
		if($_POST['question'] == "" || $_POST['response'] == ""){
			echo "<status>warning</status>\n";
			echo "<message>".$this->language->get('error_allFieldsRequired')."</message>\n";
		}
		else if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_extension_faq->editFaq($this->request->post['idFaq'], $this->request->post);			
			echo "<status>success</status>\n";
			echo "<message>".$this->language->get('success_modifyQuestions')."</message>\n";
		}

		echo "</result>\n";
		exit();
	}
 
 	public function updateCategory() {
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		echo "<result>\n";
		
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');
		if($_POST['category'] == ""){
			echo "<status>warning</status>\n";
			echo "<message>".$this->language->get('error_allFieldsRequired')."</message>\n";
		}
		else if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormCategory()) {
			$this->model_extension_faq->editFaqCategories($this->request->post['idCategory'], $this->request->post);
			echo "<status>success</status>\n";
			echo "<message>".$this->language->get('success_modifyCategory')."</message>\n";
		}
		echo "</result>\n";
		exit();
	}
 
	public function delete() {
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		echo "<result>\n";
		
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $faq_id) {
				$this->model_extension_faq->deleteFaq($faq_id);
			}
			echo "<status>success</status>\n";
			echo "<message>".$this->language->get('success_deleteQuestions')."</message>\n";
		}
		else{
			echo "<status>fail</status>\n";
			echo "<message>".$this->language->get('error_deleteQuestions')."</message>\n";
		}
		echo "</result>\n";
		exit();
	}
	
	public function deleteCategory() {
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		echo "<result>\n";
		
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');
		if (isset($this->request->post['selected']) && $this->validateDeleteCategory()) {
			foreach ($this->request->post['selected'] as $faqCategories_id) {
				$this->model_extension_faq->deleteFaqCategories($faqCategories_id);
			}
			echo "<status>success</status>\n";
			echo "<message>".$this->language->get('success_deleteCategories')."</message>\n";
		}
		else{
			echo "<status>fail</status>\n";
			echo "<message>".$this->language->get('error_deleteCategories')."</message>\n";
		}
		
		echo "</result>\n";
		exit();
	}


	public function pagination(){
		$this->load->model('extension/faq');
		$this->load->language('extension/faq');		
		$limit = 15;
			
		if(isset($this->request->get['category'])){
			$nbQuestions = $this->model_extension_faq->getTotalFaqsByCategory($this->request->get['category']);
		}
		else{
			$nbQuestions = $this->model_extension_faq->getTotalFaqs();
		}
		
		$nbPages = ceil($nbQuestions / $limit);
		
		echo '<div class="links">';
		for($i=1;$i<=$nbPages;$i++){
			if(isset($_POST['currentPage']) && $i == $_POST['currentPage']){
				echo '<b style="margin-left:3px;">'.$i.'</b>';
			}
			else{
				echo '<a class="page" id="'.$i.'" style="margin-left:3px;">'.$i.'</a>';
			}
		}
		echo '</div>';
		echo "<script type=text/javascript>	
			$('.page').click(function(){
				var limit = 15;
				limitMin = (this.id-1)*limit;
				$('#currentPage').val(this.id);
				pagination(this.id);
				refreshListQuestions(limitMin, limit, $('#sortCategory').val(), 'category');
			});";
		echo "</script>";
		exit();
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'extension/faq')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	private function validateFormCategory() {
		if (!$this->user->hasPermission('modify', 'extension/faq')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	private function validateDelete() {
		if (!$this->user->hasPermission('modify', 'extension/faq')) {
			$this->error['warning'] = $this->language->get('error_permission');
			return false;
		}
		return true;
	}
	
	private function validateDeleteCategory() {
		if (!$this->user->hasPermission('modify', 'extension/faq')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		foreach ($this->request->post['selected'] as $idCategory) {
			if($this->model_extension_faq->testCategory($idCategory)){
				$this->error['warning'] = $this->language->get('error_relation');
			}
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>