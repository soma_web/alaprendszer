<?php echo $header; ?>
    <div class="breadcrumb">
        <?php $elemszam = count($breadcrumbs);
        $i = 0;
        foreach ($breadcrumbs as $breadcrumb) {
            $i++;
            if ($i == $elemszam) {
                ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread"
                                                       href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?
            } else {
                echo $breadcrumb['separator']; ?><a
                href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?
            }
        } ?>
    </div>



<?php echo $column_left; ?><?php echo $column_right; ?>
    <div id="content"><?php echo $content_top; ?>

        <h1><?php echo $heading_title; ?></h1>

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">


            <div class="contact-info">
                <div class="content">
                    <div class="left">
                        <h2><?php echo $text_location; ?></h2>
                        <b><?php echo $text_address; ?></b><br/>
                        <?php echo $store; ?><br/>
                        <?php echo $ceg_neve; ?><br/>
                        <?php echo $address; ?>

                        <?php if ($telephone) { ?>
                            <br/><b><?php echo $text_telephone; ?></b><br/>
                            <?php echo $telephone; ?><br/>
                            <br/>
                        <?php } ?>
                        <?php if ($fax) { ?>
                            <br/><b><?php echo $text_fax; ?></b><br/>
                            <?php echo $fax; ?>
                        <?php } ?>
                        <div class="content kapcsolat">
                            <h2><?php echo $text_contact; ?></h2>

                            <b><?php echo $entry_name; ?></b><br/>
                            <input type="text" name="name" value="<?php echo $name; ?>"/>
                            <br/>
                            <?php if ($error_name) { ?>
                                <span class="error"><?php echo $error_name; ?></span>
                            <?php } ?>
                            <br/>
                            <b><?php echo $entry_email; ?></b><br/>
                            <input type="text" name="email" value="<?php echo $email; ?>"/>
                            <br/>
                            <?php if ($error_email) { ?>
                                <span class="error"><?php echo $error_email; ?></span>
                            <?php } ?>
                            <br/>
                            <b><?php echo $entry_enquiry; ?></b><br/>
                            <textarea name="enquiry" cols="40" rows="10" style="width: 95%;"><?php echo $enquiry; ?></textarea>
                            <br/>
                            <?php if ($error_enquiry) { ?>
                                <span class="error"><?php echo $error_enquiry; ?></span>
                            <?php } ?>
                            <br/>
                            <b><?php echo $entry_captcha; ?></b><br/>
                            <input type="text" name="captcha" value="<?php echo $captcha; ?>"/>
                            <br/>
                            <img src="index.php?route=information/contact/captcha" alt=""/>
                            <?php if ($error_captcha) { ?>
                                <span class="error"><?php echo $error_captcha; ?></span>
                            <?php } ?>
                            <div class="buttons">
                                <!-- Küldés gomb -->
                                <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button"/></div>
                            </div>
                        </div>
                    </div>
                    <div class="right">
                        <h2><?php echo $text_map; ?></h2>
                        <?php echo $terkep ?>
                    </div>
                </div>
            </div>


        </form>
        <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>