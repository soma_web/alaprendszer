<?php

// Heading
$_['heading_title']   = 'Letölthető utalványok';

// Text
$_['text_account']    = 'Fiók';
$_['text_downloads']  = 'Utalvány letöltés';
$_['text_order']      = 'Rendelési szám:';
$_['text_date_added'] = 'Vásárlás dátuma:';
$_['text_name']       = 'Letöltés neve:';
$_['text_remaining']  = 'Maradék letöltések:';
$_['text_size']       = 'Méret:';
$_['text_download']   = 'Utalvány letöltés';
$_['text_empty']      = 'Ön nem vásárolt még letölthető terméket!';
?>
