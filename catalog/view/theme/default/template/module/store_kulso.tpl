<?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/felnott.css')) { ?>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/felnott.css" />
<?php } else { ?>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE?>/default/stylesheet/felnott.css" />
<?php } ?>

<?php if (isset($slider) && !empty($slider) && $slider['slider'] == "1") { ?>
<script type="text/javascript" src="catalog/view/javascript/contentflow/contentflow.js" media="screen"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/contentflow/contentflow.css" />
<?php } ?>

<div id="aruhaz_valaszt">
    <div class="felnott" >
        <?php if(isset($store_text_status) && !$store_text_status) { ?>
            <div class="box-heading" style="text-align: center;"><?php echo $heading_title; ?></div>
            <div class="box-content" style="text-align: center;">
                <p><?php echo $text_store; ?></p>
        <?php } ?>
        <?php if (isset($slider) && !empty($slider) && $slider['slider'] == "1") { ?>
            <div class="ContentFlow" id="myFlow">

                <div class="flow">
                    <?php foreach ($stores as $store) { ?>
                        <div class="item" href="<?php echo $store['url']?>"><img class="content" src="<?php echo $store['image']?>" alt='<?php echo $store['name']?>' title='<?php echo $store['name']?>'>
                            <?php if(isset($slider['title']) && $slider['title']) { ?>
                                <div class="title"><span><?php echo $store['name']?></span></div>
                            <?php } ?>
                        </div>
                    <?php } ?>

                        <?php foreach ($stores as $store) { ?>
                            <div class="item shadow-effect" href="<?php echo $store['url']?>"><img class="content" src="<?php echo $store['image']?>" alt='<?php echo $store['name']?>' title='<?php echo $store['name']?>'>
                                <?php if(isset($slider['title']) && $slider['title']) { ?>
                                    <div class="title"><span><?php echo $store['name']?></span></div>
                                <?php } ?>
                            </div>
                        <?php } ?>

                </div>

           <div style="height: 100px;"></div>

                  <!--  <div class="preButton"><img  src="<?php echo DIR_TEMPLATE?>/default/image/arrow-left-24.png" title="Balra"/></div>
                    <div class="nextButton"><img  src="<?php echo DIR_TEMPLATE?>/default/image/arrow-24.png" title="Jobbra"/></div>-->


            </div>
        <?php } else { ?>
            <?php foreach ($stores as $store) { ?>
                <div class="stooore">
                <div class="store-outer" style="height: <?php echo (isset($image_height) && $image_height) ? $image_height : 400 ?>px; width: <?php echo (isset($image_width) && $image_width) ? $image_width : 280 ?>px" >
                    <a class="store" href="<?php echo $store['url']?>">

                        <div class="belepes"><?php echo $text_enter; ?></div>
                        <img class="content" src="<?php echo $store['image']?>" alt='<?php echo $store['name']?>' title='<?php echo $store['name']?>'>

                    </a>

                </div>
                <?php if(isset($slider['title']) && $slider['title']) { ?>
                    <a class="title" href="<?php echo $store['url']?>"><span><?php echo $store['name']?></span></a>
                <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
        </div>
    </div>
</div>
<script>
    /* Kategória menü eltűntetése, az összehasonlítás és a hármas menü eltűntetése */
    $(document).ready(function(){
        $('#menu-kategoriak').css("display","none");
        $('.product-compare').css("display","none");
        $('.menu-three').css("display","none");
        $('.price-calculator').css("display","none");
        $('.kozepe > * > * > :not("#aruhaz_valaszt")').css("display","none");
        $('.kozepe > *:not(".contenttop")').css("display","none");
    });

</script>

<?php if (isset($slider) && !empty($slider) && $slider['slider'] == "1") { ?>
<script>
    $(document).ready(function(){

        $('.kozepe > * > * > :not("#aruhaz_valaszt")').css("display","none");
        $('.kozepe > *:not(".contenttop")').css("display","none");
        myFlow2 = new ContentFlow('myFlow', {

            maxItemHeight: <?php echo (isset($image_height) && $image_height) ? $image_height : 0 ?>,
            reflectionHeight: <?php echo (isset($slider['mirror']) && $slider['mirror']) ? $slider['mirror'] : (float)0.0 ?>,
            circularFlow: <?php echo (isset($slider['around']) && $slider['around']) ? 'true' : 'false' ?>,
            scrollWheelSpeed: 0,
            flowSpeedFactor: <?php echo isset($slider['speed']) ? $slider['speed'] : 0.0?>,
            endOpacity: <?php echo isset($slider['opacity']) ? $slider['opacity'] : 0.0?>
        } ) ;
    });

</script>
<?php } ?>
