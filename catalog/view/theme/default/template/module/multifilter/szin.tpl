<?php if ( isset($szinek) && $szinek && count($szinek) > 1 ){ ?>

    <style>
        .szintablazat {
            display: inline-table;
            height: 14px;
            width: 14px;
            padding: 0;
        }



        .box-filter-szin {
            width: 45px;
            height: 10px;


            -webkit-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.1);
            -moz-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.1);
            box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.1);
            position: relative;
            display: inline-block;
            margin: 0 4px 20px 0;

        }

        .box-filter-szin .szin_input {
            opacity: 0;
        }

        .box-filter-szin .szin_label {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            cursor: pointer;
            position: absolute;
            box-shadow: 1px 1px 5px #ddd;
            -webkit-box-shadow: 1px 1px 5px #ddd;
            -moz-box-shadow: 1px 1px 5px #ddd;
            width: 100%;
            height: 100%;
            left: 0px;
            top: 0px;
        }

        .box-filter-szin .szin_label * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }

        .box-filter-szin .szin_label:after, .szincsoport:after {
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            filter: alpha(opacity=0);
            opacity: 0;
            content: '';
            position: absolute;
            width: 44px;
            height: 3px;
            background: red;
            top: 14px;
            left: 0;
        }

        .box-filter-szin .szin_label:hover::after, .szincsoport:hover::after {
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=10";
            filter: alpha(opacity=10);
            opacity: 0.1;
        }

        .box-filter-szin input[type=checkbox]:checked + .szin_label:after, .szincsoport.aktiv_csoport:after  {
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
            filter: alpha(opacity=100);
            opacity: 1;
        }

        .szincsoportok {
            border-bottom: 1px solid #ddd;
            margin: 0 0 20px 0;
        }
        .szincsoport_neve {
            position: absolute;
            left: 53px;
            top: -4px;
        }
        .box .box-filter-szin.nev_kiiatas {
            width: auto;
            height: auto;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            display: block;
            margin: 0;
        }
        .szinlatszik {
            display: none;
        }
        .szincsoport_egyedul {
            display: inline-block;
            margin: 0;
            border: none;
        }


    </style>

    <div class="box">
        <div class="box-heading reszek harmadik-filter"><?php echo $heading_title_szin; ?><img style="display: none"  src="catalog/view/theme/default/image/responsive/pos_r_opened.png"></div>
            <div class="box-content">

                <?php if ( isset($beallitasok['szin']['szinek']) && $beallitasok['szin']['szinek'] == 0  ) { ?>
                    <?php foreach ($szinek as $szin) { ?>
                        <?php $szinek_db = str_replace(' ','',$szin['szinkod']);?>
                        <?php $szinek_db = str_replace('#','',$szinek_db);?>
                        <?php $szinek_db = explode('/',$szinek_db); ?>

                        <?php if (!empty($szin['name'])) { ?>
                            <?php $check = ''?>
                            <?php if (in_array($szin['option_szin_id'], $filter_szin)) { ?>
                                <?php $check = 'checked="checked"'?>
                            <?php } ?>

                            <?php $box_filter_class = ''; ?>

                            <?php if ( isset($beallitasok['szin']['szinnev']) && $beallitasok['szin']['szinnev'] == 1 ) { ?>
                                <?php $box_filter_class = 'nev_kiiatas'; ?>
                            <?php } ?>

                            <div class="box-filter box-filter-szin <?php echo $box_filter_class;?>" title="<?php echo $szin['name']?>">

                                <?php if ( isset($beallitasok['szin']['szinnev']) && $beallitasok['szin']['szinnev'] == 1 ) { ?>
                                    <input type="checkbox" value="<?php echo $szin['option_szin_id']; ?>" id="szin<?php echo $szin['option_szin_id']; ?>" name="check" <?php echo $check?>/>
                                    <label style=""  class="" for="szin<?php echo $szin['option_szin_id']; ?>"><?php echo $szin['name']?></label>
                                <?php } else { ?>

                                    <input type="checkbox" value="<?php echo $szin['option_szin_id']; ?>" id="szin<?php echo $szin['option_szin_id']; ?>" name="check" <?php echo $check?> class="szin_input"/>
                                    <label style="display: block"  class="szin_label" for="szin<?php echo $szin['option_szin_id']; ?>">
                                        <?php foreach($szinek_db as $szinkod) { ?>
                                            <div style="background: #<?php echo $szinkod; ?>; width:<?php echo round(45/count($szinek_db),0)?>px; height: 10px; display: table-cell;"></div>
                                        <?php } ?>
                                    </label>
                                <?php } ?>

                            </div>
                        <?php } ?>
                    <?php } ?>


                <?php } elseif ( isset($beallitasok['szin']['szinek']) && ($beallitasok['szin']['szinek'] == 1 ||  $beallitasok['szin']['szinek'] == 2 || $beallitasok['szin']['szinek'] == 3) ) { ?>

                    <?php foreach ($szin_groups as $szin_group) { ?>
                        <?php $szinek_db = str_replace(' ','',$szin_group['szinkod']);?>
                        <?php $szinek_db = str_replace('#','',$szinek_db);?>
                        <?php $szinek_db = explode('/',$szinek_db); ?>

                        <?php if (!empty($szin_group['name'])) { ?>
                            <?php $check = ''?>
                            <?php if (in_array($szin_group['option_szin_group_id'], $filter_szin)) { ?>
                                <?php $check = 'checked="checked"'?>
                            <?php } ?>

                            <?php $class = ''; ?>
                            <?php if ($beallitasok['szin']['szinek'] == 3) { ?>
                                <?php $class = 'szincsoport_egyedul';?>
                            <?php } ?>
                            <div style="" class="szincsoportok <?php echo $class;?>">
                                <input type="hidden" name="szin_csoport<?php echo $szin_group['option_szin_group_id']?>" class="szincsoportok_valasztva" szincsoport_id="<?php echo $szin_group['option_szin_group_id']?>" value="">

                                <div class="box-filter box-filter-szin szincsoport" title="<?php echo $szin_group['name']?>" ertek="<?php echo $szin_group['option_szin_group_id']?>"
                                     style="cursor: pointer;"
                                     onclick="csoportosKijeloles('szin_csoport<?php echo $szin_group['option_szin_group_id']?>',this)">

                                    <?php foreach($szinek_db as $szinkod) { ?>
                                        <div class="szinkod-teglalap" style="background: #<?php echo $szinkod; ?>; width:<?php echo round(45/count($szinek_db),0)?>px; height: 10px; display: table-cell;"></div>
                                    <?php } ?>
                                    <?php if ($beallitasok['szin']['szinek'] == 2) { ?>
                                        <span class="szincsoport_neve"> <?php echo $szin_group['name']?></span>
                                    <?php } ?>
                                </div>
                                <br>
                                <?php foreach($szinek as $szin) { ?>
                                    <?php $szinek_db = str_replace(' ','',$szin['szinkod']);?>
                                    <?php $szinek_db = str_replace('#','',$szinek_db);?>
                                    <?php $szinek_db = explode('/',$szinek_db); ?>

                                    <?php $check = ''?>
                                    <?php if (in_array($szin['option_szin_id'], $filter_szin)) { ?>
                                        <?php $check = 'checked="checked"'?>
                                    <?php } ?>

                                    <?php if ( isset($szin['groups']) && $szin['groups'] ) { ?>
                                        <?php $szinlatszik = ''; ?>

                                        <?php if($beallitasok['szin']['szinek'] == 3 ) { ?>
                                            <?php $szinlatszik = 'szinlatszik'; ?>
                                        <?php } ?>
                                        <?php foreach($szin['groups'] as $group) { ?>
                                            <?php if($group['option_szin_group_id'] == $szin_group['option_szin_group_id']) {?>
                                                <div class="box-filter box-filter-szin <?php echo $szinlatszik;?>" title="<?php echo $szin['name']?>">

                                                    <input type="checkbox"  class="szin_csoport<?php echo $szin_group['option_szin_group_id']?> csoportban_<?php echo $szin['option_szin_id']?> szin_input"
                                                            szin="csoportban_<?php echo $szin['option_szin_id']?>"
                                                            value="<?php echo $szin['option_szin_id']; ?>"
                                                            id="szin_<?php echo $szin_group['option_szin_group_id'].'_'. $szin['option_szin_id']; ?>" name="check" <?php echo $check?>
                                                            onclick="szinekClick('csoportban_<?php echo $szin['option_szin_id']?>',this)"/>

                                                    <label style="display: block"  class="szin_label" for="szin_<?php echo $szin_group['option_szin_group_id'].'_'.$szin['option_szin_id']; ?>">
                                                        <?php foreach($szinek_db as $szinkod) { ?>
                                                            <div style="background: #<?php echo $szinkod; ?>; width:<?php echo round(45/count($szinek_db),0)?>px; height: 10px; display: table-cell;"></div>
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>

                                    <?php } ?>
                                <?php } ?>
                            </div>

                        <?php } ?>
                    <?php } ?>
                <?php } ?>

            </div>

    </div>

    <script>


        $(document).ready(function(){

            $('.szincsoportok').each(function(para,para1){
                valasztva = true;
                if ($(this).children().find('input').length > 0) {
                    $(this).children().find('input').each(function(){
                        if ( $(this).prop('checked') == false) {
                            valasztva = false;
                        }
                    });
                } else {
                    valasztva = false;
                }
                var ertek = $(para1).find('.szincsoport').attr("ertek");

                if (valasztva) {
                    $(para1).find('.szincsoport').addClass('aktiv_csoport');
                    $('input[name="szin_csoport'+ertek+'"]').val("1");
                } else {
                    $('input[name="szin_csoport'+ertek+'"]').val("0");
                }
            });

            <?php if ($filter_szin_group) { ?>
                <?php foreach($filter_szin_group as $ertek) { ?>
                    if ($('.szincsoport').filter("[ertek='<?php echo $ertek ?>']").length > 0) {

                        $('.szin_csoport<?php echo $ertek?>').attr('checked',"checked");
                        $('.szincsoport').filter("[ertek='<?php echo $ertek ?>']").addClass('aktiv_csoport');
                        $('input[name="szin_csoport<?php echo $ertek?>"]').val("1");
                        $('.szin_csoport<?php echo $ertek?>').attr('checked',"checked");

                        $('.szin_csoport<?php echo $ertek?>').each(function(){
                            var szinkod=this.attributes.szin.value;
                            if ($('.aktiv_csoport').parent().find('input[szin='+szinkod+']').length < 1) {
                                szinekClick(szinkod,this);
                            }
                        });
                    }
                <?php } ?>
            <?php } ?>
        });

        function csoportosKijeloles(para,aktualis){
            var minden_kijelolve = true;
            $('.'+para).each(function(){
                if ( $(this).attr('checked') == 'checked' ) {
                } else {
                    minden_kijelolve = false;
                }
            });
            if (minden_kijelolve) {
                $('.'+para).removeAttr('checked');
                $(aktualis).removeClass('aktiv_csoport');
                $('input[name='+para+']').val("0");

            } else {
                $('.'+para).attr('checked',"checked");
                $(aktualis).addClass('aktiv_csoport');
                $('input[name='+para+']').val("1");
            }

            $('.'+para).each(function(){

                var szinkod=this.attributes.szin.value;
                if ($('.aktiv_csoport').parent().find('input[szin='+szinkod+']').length < 1) {
                    szinekClick(szinkod,this);
                }
            });
            szuroIndit('szin_csoport');

        }

        function szinekClick(para,aktualis) {
            if ($(aktualis).prop('checked')) {
                $('.'+para).attr('checked',"checked");
            } else {
                $('.'+para).removeAttr('checked');
            }

        }

    </script>

<?php }?>