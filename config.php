<?php
// HTTP

define('HTTP_SERVER', 'http://localhost/alap/');
define('HTTP_IMAGE', 'http://localhost/alap/image/');
define('HTTP_ADMIN', 'http://localhost/alap/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/alap/');
define('HTTPS_IMAGE', 'http://localhost/alap/image/');
define('HTTPS_ADMIN', 'http://localhost/alap/admin/');

// DIR
define('DIR_APPLICATION', '/var/www/html/alap/catalog/');
define('DIR_ARUHAZ', '/var/www/html/alap/');
define('DIR_SYSTEM', '/var/www/html/alap/system/');
define('DIR_DATABASE', '/var/www/html/alap/system/database/');
define('DIR_LANGUAGE', '/var/www/html/alap/catalog/language/');
define('DIR_TEMPLATE', 'catalog/view/theme/');
define('DIR_CONFIG', '/var/www/html/alap/system/config/');
define('DIR_IMAGE_NYOMTAT', 'image/');
define('DIR_IMAGE', '/var/www/html/alap/image/');
define('DIR_CACHE', '/var/www/html/alap/system/cache/');
define('DIR_DOWNLOAD', '/var/www/html/alap/download/');
define('DIR_LOGS', '/var/www/html/alap/system/logs/');
define('DIR_TEMPLATE_IMAGE', 'catalog/view/theme/');

// DB

define('DB_DRIVER', 'mysqliz');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'dell');
define('DB_PREFIX', 'hungaro_');
define('DB_DATABASE', 'solar_webshop');

?>
