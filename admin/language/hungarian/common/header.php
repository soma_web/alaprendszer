<?php

// Heading
$_['heading_title']                    = 'Fejléc';

// Text
$_['text_pre_order']                   = 'Ajánlatkérések';
$_['text_integration_kapcsolat']       = 'Integrációs kapcsolatok';
$_['text_megjelenito_header']          = 'Header beállítás';
$_['text_affiliate']                   = 'Partnerek';
$_['text_attribute']                   = 'Tulajdonságok';
$_['text_attribute_group']             = 'Tulajdonság csoportok';
$_['text_backup']                      = 'Bizt. Mentés / Visszaállítás';
$_['text_export']                      = 'Excel Export / Import (Csak XLS)';
$_['text_banner']                      = 'Bannerek';
$_['text_catalog']                     = 'Katalógus';
$_['text_category']                    = 'Kategóriák';
$_['text_confirm']		               = 'A törlés nem vonható vissza! Biztos, hogy ezt akarja?';
$_['text_country']                     = 'Országok';
$_['text_coupon']                      = 'Kupon';
$_['text_currency']                    = 'Pénznemek';
$_['text_customer']                    = 'Vevők';
$_['text_address']                     = 'Címek';
$_['text_customer_group']              = 'Vevőcsoportok';
$_['text_customer_blacklist']          = 'Feketelista';
$_['text_dashboard']                   = 'Műszerfal';
$_['text_filter']                      = 'Szűrők';
$_['text_filter_group']                = 'Szűrő csoportok';
$_['text_design']                      = 'Dizájn';
$_['text_download']                    = 'Letöltések';
$_['text_error_log']                   = 'Hibanapló';
$_['text_extension']                   = 'Bővítmények';
$_['text_feed']                        = 'Termékcsatornák';
$_['text_megjelenito']                 = 'Termék megjelenítés';
$_['text_front']                       = 'Áruház előnézet';
$_['text_geo_zone']                    = 'Földrajzi zónák';
$_['text_help']                        = 'Súgó';
$_['text_biztositas']                  = 'Biztosítási díj';
$_['text_information']                 = 'Információk';
$_['text_fogalom_magyarazat']          = 'Fogalom magyarázat';
$_['text_language']                    = 'Nyelvek';
$_['text_layout']                      = 'Elrendezések';
$_['text_localisation']                = 'Területi beállítások';
$_['text_logged']                      = 'Bejelentkezve: <span>%s</span> néven!';
$_['text_logout']                      = 'Kijelentkezés';
$_['text_contact']                     = 'Hírlevél írása';
$_['text_manufacturer']                = 'Gyártók';
$_['text_module']                      = 'Modulok';
$_['text_option']                      = 'Választék';
$_['text_option_value']                = 'Választék érték';
$_['text_option_szin']                 = 'Színtáblázat';
$_['text_option_szin_group']           = 'Színcsoport';
$_['text_option_szin_to_group']        = 'Színcsoport-szín kapcsolat';
$_['text_order']                       = 'Rendelések';
$_['text_order_status']                = 'Rendelések állapotai';
$_['text_opencart']                    = 'Kezdőlap';
$_['text_payment']                     = 'Fizetési módok';
$_['text_product']                     = 'Termékek';
$_['text_product_customer_special']    = 'Vevő egyedi árak';
$_['text_product_special']             = 'Akciós termékek';
$_['text_product_discount']            = 'Mennyiségi kedvezmények';
$_['text_reports']                     = 'Jelentések';
$_['text_report_sale_order']           = 'Rendelések';
$_['text_report_sale_tax']             = 'Adó';
$_['text_report_sale_shipping']        = 'Szállítás';
$_['text_report_sale_return']          = 'Garancia';
$_['text_report_sale_coupon']          = 'Kuponok';
$_['text_report_product_viewed']       = 'Megnézett';
$_['text_report_product_purchased']    = 'Megvásároltak';
$_['text_report_customer_order']       = 'Rendelések';
$_['text_report_customer_reward']      = 'Hűségpontok';
$_['text_report_customer_credit']      = 'Kredit';
$_['text_report_affiliate_commission'] = 'Jutalék';
$_['text_review']                      = 'Vélemények';
$_['text_return']                      = 'Garancia';
$_['text_return_action']               = 'Garanciális műveletek';
$_['text_return_reason']               = 'Garanciális okok';
$_['text_return_status']               = 'Garancia állapotok';
$_['text_sale']                        = 'Eladások';
$_['text_shipping']                    = 'Szállítás';
$_['text_setting']                     = 'Beállítások';
$_['text_stock_status']                = 'Hiánycikk állapotok';
$_['text_support']                     = 'Támogató fórum';
$_['text_system']                      = 'Rendszer';
$_['text_tax']                         = 'Adók';
$_['text_tax_class']                   = 'Adó osztályok';
$_['text_tax_rate']                    = 'Adókulcsok';
$_['text_total']                       = 'Rendelés menete';
$_['text_user']                        = 'Felhasználó';
$_['text_documentation']               = 'Dokumentáció';
$_['text_users']                       = 'Felhasználók';
$_['text_user_group']                  = 'Felhasználó csoport';
$_['text_voucher']                     = 'Ajándékutalványok';
$_['text_voucher_theme']               = 'Utalvány témák';
$_['text_weight_class']                = 'Súlymérték';
$_['text_packaging_class']             = 'Kiszerelés mértékegység';
$_['text_length_class']                = 'Hosszmérték';
$_['text_zone']                        = 'Zónák';
$_['text_nyelv']                       = 'Nyelvi file-ok';
$_['text_newssubscribe']               = 'Hírlevélre feliratkozók';
$_['text_customer_eletkor']            = 'Életkor';
$_['text_customer_vegzettseg']         = 'Iskolai végzettség';
$_['text_elhelyezkedes']               = 'Elhelyezés - kiemelés';
$_['text_elhelyezkedes_group']         = 'Elhelyezés alcsoport';
$_['text_kiemelesek']                  = 'Kiemelések';
$_['text_fizetes_allapot']             = 'Fizetési állapotok';
$_['text_fizetes_elbiralas_statusz']   = 'Elbírálás státuszai';
$_['text_report_customer_wishlist']    = 'Kívánságlista';

$_['text_csv']                         = 'CSV export-import';
$_['text_description']                  = 'Megnevezés - leírás';

$_['text_event']                       = 'Eseménynaptár';
$_['text_faq']                         = 'GY.I.K.';

// Error
$_['error_install']                    = 'Figyelmeztetés: Kérem, törölje az "Install" mappát!';
$_['text_customer_product']            = 'Vevő termékek';

$_['text_report_sale_catgories']       = 'Eladások kategóriánként';
$_['text_report_sale_uploaders']       = 'Feltöltők';
$_['text_disabledproduct']             = 'Letiltott termékek';
$_['text_faq']                         = 'GYIK';

$_['text_pavblog_blog']                     = 'Blog';
$_['text_pavblog_manage_comment']           = 'Hozzászólások';
$_['text_pavblog_manage_cate']              = 'Kategóriák kezelése';
$_['text_pavblog_manage_blog']              = 'Blogok kezelése';
$_['text_pavblog_add_blog']			        = 'Blog hozzáadása';
$_['text_pavblog_general_setting']          = 'Általános beállítások';

$_['text_pavblog_front_mods']               = 'Frontend modulok';
$_['text_pavblog_category']                 = 'Kategóriák kezelése';
$_['text_pavblog_comment']                  = 'Legutóbbi hozzászólások';
$_['text_pavblog_latest']                   = 'Legutóbbi blogok';

$_['text_qa']                          = 'Kérdezz felelek';


$_['text_product_option']             = 'Kapcsolódó választék';
$_['text_product_option_value']       = 'Kapcsolódó választék érték';
$_['text_product_relations']          = 'Általános kapcsolatok';


?>