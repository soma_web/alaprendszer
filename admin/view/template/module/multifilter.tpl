<?php echo $header; ?>
<style>
    .szincsoport_cella {
        display: table-cell;
    }
    #szin_egyeb {
        margin: 10px 0;
    }

</style>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <table class="form">

              <tr>
                  <td><?php echo $entry_fejlec_latszik; ?></td>
                  <td><?php if ($multifilter_module_fejlec) { ?>
                          <input type="radio" name="multifilter_module_fejlec" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_module_fejlec" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_module_fejlec" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_module_fejlec" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $entry_ajaxos; ?></td>
                  <td><?php if ($multifilter_module_ajax) { ?>
                          <input type="radio" name="multifilter_module_ajax" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_module_ajax" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_module_ajax" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_module_ajax" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
              </tr>



          </table>

          <table class="list padding_5">
              <thead>
              <tr>
                  <td class="left"><?php echo $entry_szuro_neve; ?></td>
                  <td class="left"><?php echo $entry_status; ?></td>
                  <td class="left"><?php echo $entry_folyamat; ?></td>
                  <td class="left"><?php echo $entry_teljes_lista; ?></td>
                  <td class="left"><?php echo $entry_sort_order; ?></td>
                  <td class="left"><?php echo $entry_template; ?></td>
                  <td></td>
              </tr>
              </thead>

              <tr>
                  <td><?php echo $entry_kategoria_checkbox; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok['kategoria_checkbox']['value']) && $beallitasok['kategoria_checkbox']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_checkbox][value]" value="1" checked="checked" class="kategoriak_be" id="kategoria_checkbox_be"/>
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_checkbox][value]" value="0"  class="kategoriak_ki" id="kategoria_checkbox_ki" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_checkbox][value]" value="1"   class="kategoriak_be" id="kategoria_checkbox_be"/>
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_checkbox][value]" value="0" checked="checked"  class="kategoriak_ki" id="kategoria_checkbox_ki"/>
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek"></td>

                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok['kategoria_checkbox']['teljes']) &&  $beallitasok['kategoria_checkbox']['teljes'] == 1) { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_checkbox][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_checkbox][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_checkbox][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_checkbox][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td>
                      <input type="text" name="multifilter_beallitasok[kategoria_checkbox][sort_order]" value="<?php echo isset($beallitasok) ? $beallitasok['kategoria_checkbox']['sort_order'] : 0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[kategoria_checkbox][tpl_name]" value="<?php echo isset($beallitasok) ? $beallitasok['kategoria_checkbox']['tpl_name'] : 'category_checkbox.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[kategoria_checkbox][name]" value="kategoria_checkbox" readonly="readonly" size="20" />
                  </td>
              </tr>



              <tr>
                  <td><?php echo $entry_kategoria_normal; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['kategoria_normal']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_normal][value]" value="1" checked="checked"  class="kategoriak_be" id="kategoria_normal_be" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_normal][value]" value="0"  class="kategoriak_ki" id="kategoria_normal_ki" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_normal][value]" value="1"  class="kategoriak_be" id="kategoria_normal_be" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_normal][value]" value="0" checked="checked"  class="kategoriak_ki" id="kategoria_normal_ki" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek"></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['kategoria_normal']['teljes']) { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_normal][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_normal][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_normal][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_normal][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[kategoria_normal][sort_order]" value="<?php echo isset($beallitasok) ? $beallitasok['kategoria_normal']['sort_order'] : 0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[kategoria_normal][tpl_name]" value="<?php echo isset($beallitasok) ? $beallitasok['kategoria_normal']['tpl_name']:'categor.tpl'; ?>"   size="20" />
                      <input  class="input_multi_readonly" type="text" name="multifilter_beallitasok[kategoria_normal][name]" value="kategoria_normal"  readonly="readonly" size="20" />
                  </td>
              </tr>
              <tr>
                  <td><?php echo $entry_kategoria_sub; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['kategoria_sub']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_sub][value]" value="1" checked="checked"   class="kategoriak_be" id="kategoria_sub_be"/>
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_sub][value]" value="0"  class="kategoriak_ki" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_sub][value]" value="1"   class="kategoriak_be" id="kategoria_sub_be"/>
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_sub][value]" value="0" checked="checked"  class="kategoriak_ki" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek"></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['kategoria_sub']['teljes']) { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_sub][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_sub][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_sub][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[kategoria_sub][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[kategoria_sub][sort_order]" value="<?php echo isset($beallitasok) ? $beallitasok['kategoria_sub']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[kategoria_sub][tpl_name]" value="<?php echo isset($beallitasok) ? $beallitasok['kategoria_sub']['tpl_name']:'sub_category.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[kategoria_sub][name]" value="kategoria_sub" readonly="readonly" size="20" />
                  </td>
              </tr>




              <tr>
                  <td><?php echo $entry_szuro_csoport; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['szuro_csoport']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['szuro_csoport']['szukit']) { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][szukit]" value="1" checked="checked" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][szukit]" value="0" />
                          <?php echo $text_bovit; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][szukit]" value="1" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][szukit]" value="0" checked="checked" />
                          <?php echo $text_bovit; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['szuro_csoport']['teljes']) { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro_csoport][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td>
                      <input type="text" name="multifilter_beallitasok[szuro_csoport][sort_order]" value="<?php echo isset($beallitasok) ? $beallitasok['szuro_csoport']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[szuro_csoport][tpl_name]" value="<?php echo isset($beallitasok) ? $beallitasok['szuro_csoport']['tpl_name']:'filter_group.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[szuro_csoport][name]" value="szuro_csoport" readonly="readonly" size="20" />
                  </td>
              </tr>




              <tr>
                  <td><?php echo $entry_szuro; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['szuro']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['szuro']['szukit']) { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][szukit]" value="1" checked="checked" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][szukit]" value="0" />
                          <?php echo $text_bovit; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][szukit]" value="1" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][szukit]" value="0" checked="checked" />
                          <?php echo $text_bovit; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['szuro']['teljes']) { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szuro][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[szuro][sort_order]" value="<?php echo isset($beallitasok) ? $beallitasok['szuro']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[szuro][tpl_name]" value="<?php echo isset($beallitasok) ? $beallitasok['szuro']['tpl_name']:'filter.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[szuro][name]" value="szuro" readonly="readonly" size="20" />
                  </td>
              </tr>





              <tr>
                  <td><?php echo $entry_gyarto; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['gyarto']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['gyarto']['szukit']) { ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][szukit]" value="1" checked="checked" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][szukit]" value="0"/>
                          <?php echo $text_bovit; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][szukit]" value="1"/>
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][szukit]" value="0" checked="checked" />
                          <?php echo $text_bovit; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['gyarto']['teljes']) { ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[gyarto][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[gyarto][sort_order]" value="<?php echo isset($beallitasok) ? $beallitasok['gyarto']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[gyarto][tpl_name]" value="<?php echo isset($beallitasok) ? $beallitasok['gyarto']['tpl_name']:'manufacturer.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[gyarto][name]" value="gyarto" readonly="readonly" size="20" />
                  </td>
              </tr>

              <tr>
                  <td><?php echo $entry_meret; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok['meret']['value']) && $beallitasok['meret']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[meret][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[meret][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[meret][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[meret][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok['meret']['szukit']) && $beallitasok['meret']['szukit']) { ?>
                          <input type="radio" name="multifilter_beallitasok[meret][szukit]" value="1" checked="checked" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[meret][szukit]" value="0"/>
                          <?php echo $text_bovit; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[meret][szukit]" value="1"/>
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[meret][szukit]" value="0" checked="checked" />
                          <?php echo $text_bovit; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok['meret']['teljes']) &&  $beallitasok['meret']['teljes']) { ?>
                          <input type="radio" name="multifilter_beallitasok[meret][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[meret][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[meret][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[meret][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[meret][sort_order]" value="<?php echo isset($beallitasok['meret']['sort_order']) ? $beallitasok['meret']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[meret][tpl_name]" value="<?php echo isset($beallitasok['meret']['tpl_name']) ? $beallitasok['meret']['tpl_name']:'meret.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[meret][name]" value="meret" readonly="readonly" size="20" />
                  </td>
              </tr>

              <tr>
                  <td><?php echo $entry_szin; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok['szin']['value']) && $beallitasok['szin']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[szin][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szin][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szin][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szin][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>

                      <?php if (isset($beallitasok['szin']['value']) && $beallitasok['szin']['value']) { ?>
                          <div style="display: table" id="szin_egyeb">


                              <div style="display: table-row">
                                  <div class="szincsoport_cella">
                                      <b style="margin: 0 0 0 5px;"><?php echo $text_megjelenites.':'?></b>
                                  </div>
                              </div>

                              <div style="display: table-row">
                                  <div class="szincsoport_cella">
                                      <input type="radio" name="multifilter_beallitasok[szin][szinek]" value="0"  <?php echo isset($beallitasok['szin']['szinek']) && $beallitasok['szin']['szinek'] == 0 ? 'checked="checked"' : ''?> />
                                      Szinek
                                  </div>
                              </div>

                              <div style="display: table-row">
                                  <div class="szincsoport_cella">
                                      <input type="radio" name="multifilter_beallitasok[szin][szinek]" value="1" <?php echo isset($beallitasok['szin']['szinek']) && $beallitasok['szin']['szinek'] == 1 ? 'checked="checked"' : ''?> />
                                      Csoport + szinek
                                  </div>
                              </div>


                              <div style="display: table-row">
                                  <div class="szincsoport_cella">
                                      <input type="radio" name="multifilter_beallitasok[szin][szinek]" value="2" <?php echo isset($beallitasok['szin']['szinek']) && $beallitasok['szin']['szinek'] == 2 ? 'checked="checked"' : ''?> />
                                      Csoport névvel + szinek
                                  </div>
                              </div>


                              <div style="display: table-row">
                                  <div class="szincsoport_cella">
                                      <input type="radio" name="multifilter_beallitasok[szin][szinek]" value="3" <?php echo isset($beallitasok['szin']['szinek']) && $beallitasok['szin']['szinek'] == 3 ? 'checked="checked"' : ''?> />
                                      Csoport
                                  </div>
                              </div>


                          </div>
                      <?php } ?>


                  </td>

                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok['szin']['szukit']) && $beallitasok['szin']['szukit']) { ?>
                          <input type="radio" name="multifilter_beallitasok[szin][szukit]" value="1" checked="checked" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[szin][szukit]" value="0"/>
                          <?php echo $text_bovit; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szin][szukit]" value="1"/>
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[szin][szukit]" value="0" checked="checked" />
                          <?php echo $text_bovit; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">

                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[szin][sort_order]" value="<?php echo isset($beallitasok['szin']['sort_order']) ? $beallitasok['szin']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <select style="width: 155px;" name="multifilter_beallitasok[szin][tpl_name]">
                          <option value="" <?php echo empty($beallitasok['szin']['tpl_name'])  ? 'selected' : ''; ?>><?php echo $text_select?></option>
                          <option value="szin.tpl"              <?php echo isset($beallitasok['szin']['tpl_name']) && $beallitasok['szin']['tpl_name'] == "szin.tpl"             ? 'selected' : ''; ?>><?php echo $color_tpl?></option>
                          <option value="szin_checkbox.tpl"     <?php echo isset($beallitasok['szin']['tpl_name']) && $beallitasok['szin']['tpl_name'] == "szin_checkbox.tpl"    ? 'selected' : ''; ?>><?php echo $checkox_tpl?></option>
                          <option value="szin_negyzetracs.tpl"  <?php echo isset($beallitasok['szin']['tpl_name']) && $beallitasok['szin']['tpl_name'] == "szin_negyzetracs.tpl" ? 'selected' : ''; ?>><?php echo $negyzetes_tpl?></option>
                      </select>
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[szin][name]" value="szin" readonly="readonly" size="20" />
                  </td>
              </tr>

              <tr>
                  <td><?php echo $entry_tulajdonsagok; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['tulajdonsagok']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['tulajdonsagok']['szukit']) { ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][szukit]" value="1" checked="checked" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][szukit]" value="0" />
                          <?php echo $text_bovit; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][szukit]" value="1" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][szukit]" value="0" checked="checked" />
                          <?php echo $text_bovit; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['tulajdonsagok']['teljes']) { ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[tulajdonsagok][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[tulajdonsagok][sort_order]" value="<?php echo  isset($beallitasok) ? $beallitasok['tulajdonsagok']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[tulajdonsagok][tpl_name]" value="<?php echo  isset($beallitasok) ? $beallitasok['tulajdonsagok']['tpl_name']:'attribute.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[tulajdonsagok][name]" value="tulajdonsagok" readonly="readonly"  size="20" />
                  </td>
              </tr>



              <tr>
                  <td><?php echo $entry_valasztek; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['valasztek']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['valasztek']['szukit']) { ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][szukit]" value="1" checked="checked" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][szukit]" value="0" />
                          <?php echo $text_bovit; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][szukit]" value="1" />
                          <?php echo $text_szukit; ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][szukit]" value="0" checked="checked" />
                          <?php echo $text_bovit; ?>
                      <?php } ?>
                  </td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['valasztek']['teljes']) { ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[valasztek][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[valasztek][sort_order]" value="<?php echo  isset($beallitasok) ? $beallitasok['valasztek']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[valasztek][tpl_name]" value="<?php echo  isset($beallitasok) ? $beallitasok['valasztek']['tpl_name']:'selection.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[valasztek][name]" value="valasztek" readonly="readonly" size="20" />
                  </td>
              </tr>
              <tr>
                  <td><?php echo $entry_raktaron; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['raktaron']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[raktaron][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[raktaron][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[raktaron][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[raktaron][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek"></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) &&  $beallitasok['raktaron']['teljes']) { ?>
                          <input type="radio" name="multifilter_beallitasok[raktaron][teljes]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[raktaron][teljes]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[raktaron][teljes]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[raktaron][teljes]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[raktaron][sort_order]" value="<?php echo  isset($beallitasok) ? $beallitasok['raktaron']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[raktaron][tpl_name]" value="<?php echo  isset($beallitasok) ? $beallitasok['raktaron']['tpl_name']:'stock.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[raktaron][name]" value="raktaron" readonly="readonly" size="20" />
                  </td>
              </tr>
              
              <tr>
                  <td><?php echo $entry_ar_szures; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['ar_szures']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek">
                      <input type="text" name="multifilter_beallitasok[ar_szures][tol]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures']['tol']:0; ?>"  size="10" /> -
                      <input type="text" name="multifilter_beallitasok[ar_szures][ig]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures']['ig']:10000; ?>"  size="10" />
                  </td>
                  <td></td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[ar_szures][sort_order]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[ar_szures][tpl_name]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures']['tpl_name']:'price.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[ar_szures][name]" value="ar_szures" readonly="readonly" size="20" />
                  </td>
              </tr>
              <tr>
                  <td><?php echo $entry_ar_szures_szazalek; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['ar_szures_szazalek']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_szazalek][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_szazalek][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_szazalek][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_szazalek][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek">
                      <input type="text" name="multifilter_beallitasok[ar_szures_szazalek][tol]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures_szazalek']['tol']:0; ?>"  size="4" /> -
                      <input type="text" name="multifilter_beallitasok[ar_szures_szazalek][ig]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures_szazalek']['ig']:100; ?>"  size="4" />

                  </td>
                  <td></td>

                  <td>
                      <input type="text" name="multifilter_beallitasok[ar_szures_szazalek][sort_order]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures_szazalek']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[ar_szures_szazalek][tpl_name]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures_szazalek']['tpl_name']:'percent_price.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[ar_szures_szazalek][name]" value="ar_szures_szazalek" readonly="readonly" size="20" />
                  </td>
              </tr>

              <tr>
                  <td><?php echo $entry_ar_szures_tol_ig; ?></td>
                  <td colspan=1 class="width_szazalek">
                      <?php if (isset($beallitasok) && $beallitasok['ar_szures_tol_ig']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_tol_ig][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_tol_ig][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_tol_ig][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_tol_ig][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td colspan=1 class="width_szazalek">
                      <input type="text" name="multifilter_beallitasok[ar_szures_tol_ig][tol]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures_tol_ig']['tol']:0; ?>"  size="10" /> -
                      <input type="text" name="multifilter_beallitasok[ar_szures_tol_ig][ig]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures_tol_ig']['ig']:100000; ?>"  size="10" />
                      <br />
                      <?php echo $text_netto_ar; ?>
                      <?php if (isset($beallitasok) && isset($beallitasok['ar_szures_tol_ig']['netto']) && $beallitasok['ar_szures_tol_ig']['netto']) { ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_tol_ig][netto]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_tol_ig][netto]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_tol_ig][netto]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[ar_szures_tol_ig][netto]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>


                  </td>
                  <td></td>

                  <td>
                      <input type="text" name="multifilter_beallitasok[ar_szures_tol_ig][sort_order]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures_tol_ig']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[ar_szures_tol_ig][tpl_name]" value="<?php echo  isset($beallitasok) ? $beallitasok['ar_szures_tol_ig']['tpl_name']:'price_tol_ig.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[ar_szures_tol_ig][name]" value="ar_szures_tol_ig" readonly="readonly" size="20" />
                  </td>
              </tr>


              <tr>
                  <td><?php echo $entry_szuresgomb_latszik; ?></td>
                  <td colspan=3>
                  <?php if (isset($beallitasok) && $beallitasok['szures_gomb']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[szures_gomb][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szures_gomb][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szures_gomb][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szures_gomb][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td>
                      <input type="text" name="multifilter_beallitasok[szures_gomb][sort_order]" value="<?php echo  isset($beallitasok) ? $beallitasok['szures_gomb']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[szures_gomb][tpl_name]" value="<?php echo  isset($beallitasok) ? $beallitasok['szures_gomb']['tpl_name']:'szures_gomb.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[szures_gomb][name]" value="szures_gomb" readonly="readonly" size="20" />
                  </td>
              </tr>


              <tr>
                  <td><?php echo $entry_kereses_latszik; ?></td>
                  <td colspan=3>
                      <?php if (isset($beallitasok) && $beallitasok['altalanos_kereso']['value']) { ?>
                          <input type="radio" name="multifilter_beallitasok[altalanos_kereso][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[altalanos_kereso][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[altalanos_kereso][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[altalanos_kereso][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td>
                      <input type="text" name="multifilter_beallitasok[altalanos_kereso][sort_order]" value="<?php echo  isset($beallitasok) ? $beallitasok['altalanos_kereso']['sort_order']:0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[altalanos_kereso][tpl_name]" value="<?php echo  isset($beallitasok) ? $beallitasok['altalanos_kereso']['tpl_name']:'altalanos_kereso.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[altalanos_kereso][name]" value="altalanos_kereso" readonly="readonly" size="20" />
                  </td>
              </tr>

              <tr>
                  <td><?php echo $entry_szurok_torlese; ?></td>
                  <td colspan=3>
                      <?php if (isset($beallitasok) && !empty($beallitasok['szurok_torlese']['value'])) { ?>
                          <input type="radio" name="multifilter_beallitasok[szurok_torlese][value]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szurok_torlese][value]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="multifilter_beallitasok[szurok_torlese][value]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="multifilter_beallitasok[szurok_torlese][value]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>

                  <td>
                      <input type="text" name="multifilter_beallitasok[szurok_torlese][sort_order]" value="<?php echo  !empty($beallitasok['szurok_torlese']['sort_order']) ? $beallitasok['szurok_torlese']['sort_order'] : 0; ?>"  size="3" />
                  </td>
                  <td>
                      <input type="text" name="multifilter_beallitasok[szurok_torlese][tpl_name]" value="<?php echo  !empty($beallitasok['szurok_torlese']['tpl_name']) ? $beallitasok['szurok_torlese']['tpl_name']:'szurok_torlese.tpl'; ?>"  size="20" />
                      <input class="input_multi_readonly" type="text" name="multifilter_beallitasok[szurok_torlese][name]" value="szurok_torlese" readonly="readonly" size="20" />
                  </td>
              </tr>
          </table>
        <table id="module" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_layout; ?></td>
              <td class="left"><?php echo $entry_position; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
              <td class="right"><?php echo $entry_sort_order; ?></td>
              <td></td>
            </tr>
          </thead>
          <?php $module_row = 0; ?>
          <?php foreach ($modules as $key=>$module) { ?>
                <tbody id="module-row<?php echo $module_row; ?>">
                <tr>
                  <td class="left"><select name="multifilter_module[<?php echo $module_row; ?>][layout_id]">
                      <?php foreach ($layouts as $layout) { ?>
                      <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                      <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select></td>
                  <td class="left"><select name="multifilter_module[<?php echo $module_row; ?>][position]">
                      <?php if ($module['position'] == 'content_top') { ?>
                      <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                      <?php } else { ?>
                      <option value="content_top"><?php echo $text_content_top; ?></option>
                      <?php } ?>
                      <?php if ($module['position'] == 'content_bottom') { ?>
                      <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                      <?php } else { ?>
                      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                      <?php } ?>
                      <?php if ($module['position'] == 'column_left') { ?>
                      <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                      <?php } else { ?>
                      <option value="column_left"><?php echo $text_column_left; ?></option>
                      <?php } ?>
                      <?php if ($module['position'] == 'column_right') { ?>
                      <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                      <?php } else { ?>
                      <option value="column_right"><?php echo $text_column_right; ?></option>
                      <?php } ?>
                    </select></td>
                  <td class="left"><select name="multifilter_module[<?php echo $module_row; ?>][status]">
                      <?php if ($module['status']) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select></td>
                  <td class="right"><input type="text" name="multifilter_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
                  <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
                </tr>
              </tbody>
              <?php $module_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="4"></td>
              <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--

$(document).ready(function(){


    $(".kategoriak_be, .kategoriak_ki").bind("click",function(){
        $(".kategoriak_be").prop("checked",false);
        $(".kategoriak_ki").prop("checked",true);
    });

    $('#kategoria_checkbox_be').bind("click",function() {
        $("#kategoria_checkbox_be").prop("checked",true);
    });

    $('#kategoria_normal_be').bind("click",function() {
        $("#kategoria_normal_be").prop("checked",true);
    });
     $('#kategoria_sub_be').bind("click",function() {
        $("#kategoria_sub_be").prop("checked",true);
    });

    $("input[name='multifilter_beallitasok[szin][value]']").bind("click",function(){
        if ( $("input[name='multifilter_beallitasok[szin][value]']").prop('checked')) {
            $('#szin_egyeb').show();
        } else {
            $('#szin_egyeb').hide();
        }
    });


});



    var module_row = <?php echo $module_row; ?>;

    function addModule() {
        html  = '<tbody id="module-row' + module_row + '">';
        html += '  <tr>';
        html += '    <td class="left"><select name="multifilter_module[' + module_row + '][layout_id]">';
        <?php foreach ($layouts as $layout) { ?>
        html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
        <?php } ?>
        html += '    </select></td>';
        html += '    <td class="left"><select name="multifilter_module[' + module_row + '][position]">';
        html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
        html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
        html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
        html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
        html += '    </select></td>';
        html += '    <td class="left"><select name="multifilter_module[' + module_row + '][status]">';
        html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
        html += '      <option value="0"><?php echo $text_disabled; ?></option>';
        html += '    </select></td>';
        html += '    <td class="right"><input type="text" name="multifilter_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
        html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
        html += '  </tr>';
        html += '</tbody>';

        $('#module tfoot').before(html);

        module_row++;
    }




//--></script> 
<?php echo $footer; ?>