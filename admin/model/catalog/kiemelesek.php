<?php
class ModelCatalogKiemelesek extends Model {

    public function getKiemelesek() {

        $vissza = array();
        $elhelyezkedesek = $this->config->get("kiemelesek");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['status'] == 1) {
                    $vissza[] = $value;
                }
            }
        }

        return $vissza;

    }

}
?>