<?php
// Heading
$_['heading_title']     = 'Pagini de informatii';

// Text
$_['text_success']      = 'Succes: Ai modificat informația!';
$_['text_default']      = 'Implicit';

// Column
$_['column_title']      = 'Titlu';
$_['column_sort_order']	= 'Ordinea';
$_['column_action']     = 'Acțiune';

// Entry
$_['entry_title']       = 'Titlul Paginii de Informații:';
$_['entry_description'] = 'Descriere:';
$_['entry_store']       = 'Magazine:';
$_['entry_keyword']     = 'Cuvinte SEO URL:<br/><span class="help">Acesta trebuie să fie unic global.Se refera la cuvantul(cuvintele) care prescrie adresa url cand SEO URL este pornit.Ex: in loc de situl.ro/index.php?route=information/information&information_id=x ar fii situl.ro/cuvantul-seo-url</span>';
$_['entry_status']      = 'Status:';
$_['entry_sort_order']  = 'Ordinea:<br/><span class="help">Setează la -1 ca să îl ascunzi din listare</span>';
$_['entry_layout']      = 'Trecere Peste Aspect:<br/><span class="help">Implicit, o pagina de informatii afiseaza in pagina modulele care sunt asociate aspectului Informatii.Aici puteti seta ca aceasta pagina de informatii sa afiseze modulele asociate altui aspect la alegere.</span>';

// Error 
$_['error_warning']     = 'Atenție: Vă rugăm, verificați cu atenție forumularul pentru erori';
$_['error_permission']  = 'Atenție: Nu ai permisiunea să modifici informația!';
$_['error_title']       = 'Titlul Informației trebuie sa fie cuprins intre 3 si 64 de caractere!';
$_['error_description'] = 'Descripția trebuie sa fie intre 3 caractere!';
$_['error_account']     = 'Atenție: Această pagină cu informații nu poate fi ștearsă deoarece este folosită ca și termenii contului de magazin!';
$_['error_checkout']    = 'Atenție: Această pagină cu informații nu poate fi ștearsă deoarece este folosită ca și termenii de finalizare comandă a magazinului!';
$_['error_affiliate']   = 'Atenție: Această pagină cu informații nu poate fi ștearsă deoarece este folosită ca si termenii de afiliat al magazinului!';
$_['error_store']       = 'Atenție: Această pagină cu informații nu poate fi ștearsă deoarece este folosită de %s magazine!';
?>