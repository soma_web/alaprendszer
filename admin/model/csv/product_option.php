<?php
class ModelCsvProductOption extends Model {

    private $ertek=array();
    private $id;
    private $tabla;
    private $tablak;
    private $nyelvek=array();
    private $store;

    public function getFields($table) {

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."product_option_value ");
        $product = array();

        foreach($oszlop->rows as $curoszlop) {
            $product[] = DB_PREFIX."product_option_value".'.'.$curoszlop['Field'];
            if ($curoszlop['Field'] == "product_id") {
                $product[] = DB_PREFIX."product.model";
                $product[] = DB_PREFIX."product_description.product_name";
            }
            if ($curoszlop['Field'] == "option_id") {
                $product[] = DB_PREFIX."option_description.option_name";
                $product[] = DB_PREFIX."option.option_type";
            }
            if ($curoszlop['Field'] == "option_value_id") {
                $product[] = DB_PREFIX."option_value_description.option_value_name";
            }
        }
        $product[] = DB_PREFIX."product_option".'.'."required";
        $product[] = "Delete";

        return $product;
    }

    public function backup($tabla) {
        date_default_timezone_set('Europe/Budapest');
        $filename = $tabla.'_'.date('Y-m-d_H:i:s', time()).'.csv';

        $sql = "SELECT pov.product_option_value_id,
                       pov.product_option_id,
                       pov.product_id,
                       p.model,
                       pd.name AS product_name,
                       pov. option_id,
                       od.name AS option_name,
                       o.type AS option_type,
                       pov. option_value_id,
                       ovd.name AS option_value_name,
                       pov.quantity,
                       pov.subtract,
                       pov.price,
                       pov.price_prefix,
                       pov.points,
                       pov.points_prefix,
                       pov.weight,
                       pov.weight_prefix,
                       pov.option_szin_id,
                       pov.azonosito,
                       po.required,
                       'N'";
        $sql .= " FROM ".DB_PREFIX."product_option_value pov";
        $sql .= " LEFT JOIN ".DB_PREFIX."product_description pd ON (pd.product_id=pov.product_id AND pd.language_id=".$this->config->get('config_language_id').")";
        $sql .= " LEFT JOIN ".DB_PREFIX."option_description od ON (od.option_id=pov.option_id AND od.language_id=".$this->config->get('config_language_id').")";
        $sql .= " LEFT JOIN ".DB_PREFIX."option o ON (o.option_id=pov.option_id)";
        $sql .= " LEFT JOIN ".DB_PREFIX."option_value_description ovd ON (ovd.option_value_id=pov.option_value_id)";
        $sql .= " LEFT JOIN ".DB_PREFIX."product_option po ON (po.product_option_id=pov.product_option_id)";
        $sql .= " LEFT JOIN ".DB_PREFIX."product p ON (p.product_id=pov.product_id)";
        $sql .= " WHERE 1 ";


        $products   = array();
        $fejlec     = array();
        foreach($this->request->post['backup'] as $value) {
            if ($value == "Delete") {
                $fejlec[] = "Delete";
            } else {
                $fejlec[]   = substr(strstr($value,".",0),1);
            }
        }


        if (!empty($this->request->post['filter'])) {
            foreach($this->request->post['filter'] as $filter_key=>$value) {
                $mezo = explode('-',$filter_key);
                $present = substr(strstr($mezo[0],".",0),1);
                $present = $present == "product_name" ? "pd.name" : $present;
                $present = $present == "option_name" ? "od.name" : $present;
                $present = $present == "option_type" ? "o.name" : $present;
                $present = $present == "option_value_name" ? "ovd.name" : $present;
                $present = $present == "required" ? "po.required" : $present;
                $present = $present == "product_id" ? "pov.product_id" : $present;
                $present = $present == "model" ? "p.model" : $present;

                if ($mezo[1] == "tol" && $value){

                    $sql .=" AND substring(lcase(".$present."),1,".strlen($value).") >= '".strtolower($value)."'";
                } elseif ($mezo[1] == "ig" && $value ){
                    $sql .=" AND substring(lcase(".$present."),1,".strlen($value).") <= '".strtolower($value)."'";

                } elseif ($mezo[1] == "select" && $value != "no" ){
                    $sql .=" AND ".$present." = ".$value;
                }
            }
        }
        $sql .= " GROUP BY product_option_value_id ";

        $order = isset($this->request->post['sorrend']) ? substr(strstr($this->request->post['sorrend'],".",0),1) : 'product_option_value_id';
        $sql .= " ORDER BY ".$order;

        $sql .= " LIMIT ".(int)$this->request->post['limit_tol'].",";
        $sql .= $this->request->post['limit_db'] ? $this->request->post['limit_db'] : 1844674407370955161;
        $query = $this->db->query($sql);



        if ($query->num_rows > 0) {
            $handle = fopen('../csv/'.$filename, "w");
            //fputcsv ( $handle, $fejlec, ";", '"' ,"\\" );
            fputcsv ( $handle, $fejlec, ";", '"');

            foreach($query->rows as $fields) {
                foreach($fields as $key=>$value) {
                    $fields[$key] = str_ireplace(array("\r","\n","\t",'"','','','','\\"'),'', $value);
                }
                $fields['product_name'] = strlen($fields['product_name']) > 20 ? substr(($fields['product_name']),0,20)." ..." : $fields['product_name'];
                //$fields['option_name'] = strlen($fields['option_name']) > 30 ? substr(($fields['option_name']),0,30)." ..." : $fields['option_name'];
                //$fields['option_value_name'] = strlen($fields['option_value_name']) > 20 ? substr(($fields['option_value_name']),0,20)." ..." : $fields['option_value_name'];
                fputcsv ( $handle, $fields, ";", '"');
            }
            fclose($handle);

        }
        return '../csv/'.$filename;
    }

    public function restore($csv) {

        require_once(DIR_SYSTEM."library/Encoding.php");
        $encode = new Encoding();
        $csv = $encode->toUTF8($csv);

        $this->load->model("localisation/language");
        $this->nyelvek = $this->model_localisation_language->getLanguages();

        $sorok = str_getcsv($csv, "\n");
        $fejlec = str_getcsv($sorok[0], ";",'"','\\');

        $this->tabla = strstr($fejlec[0],".",1);

        $sql = "SHOW TABLES";
        $query = $this->db->query($sql);
        $this->store = false;
        foreach($query->rows as $value) {
            if (in_array(DB_PREFIX.$this->tabla."_to_store",$value)) {
                $this->store = true;
                break;
            }
        }


        if (!$this->formatumEllenorzes($fejlec)) return false;

        $van_torles = false;
        if ($torol_elem = array_keys($fejlec,"Delete")) {
            $torol_elem = $torol_elem[0];
            $van_torles = true;
        }
        $this->load->model("catalog/product");

        $i = 0;
        $sorok_szama = 0;
        foreach($sorok as $sor) {
            if ($i == 0) {
                $i++;
                continue;
            }
            if (!empty($sor)) {
                $this->ertek = str_getcsv($sor, ";",'"','\\');
                if ($van_torles &&  !empty($this->ertek[$torol_elem]) && (strtoupper($this->ertek[$torol_elem]) == "I" || strtoupper($this->ertek[$torol_elem]) == "Y" || $this->ertek[$torol_elem] == 1))  {
                    $this->sorTorles($fejlec);
                    continue;
                }
                $this->letezesVizsgalat($fejlec);
            }
            $sorok_szama++;
        }

        return $sorok_szama;
    }

    public function letezesVizsgalat($fejlec) {

        // product_option_value
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();



        if (empty($this->ertek[2])) {
            if (empty($this->ertek[3])) {
                return false;
            } else {
                $sql = "SELECT product_id FROM ".DB_PREFIX."product WHERE model LIKE '".$this->ertek[3]."'";
                $query = $this->db->query($sql);
                if ($query->num_rows) {
                    $this->ertek[2] = $query->row['product_id'];
                } else {
                    return false;
                }
            }
        }
        if (empty($this->ertek[1])) {
            $sql = "SELECT product_option_id FROM ".DB_PREFIX."product_option WHERE option_id = '".$this->ertek[5]."' AND product_id = '".$this->ertek[2]."'";
            $query = $this->db->query($sql);
            if ($query->num_rows) {
                $this->ertek[1] = $query->row['product_option_id'];
            } else {
                $sql = "INSERT INTO ".DB_PREFIX."product_option SET
                        product_id = '".$this->ertek[2]."',
                        option_id  = '".$this->ertek[5]."'";
                $query = $this->db->query($sql);
                $this->ertek[1] = $this->db->getLastId();

            }
        }
        if (empty($this->ertek[7])) {
            $this->ertek[7] = "checkbox_qty";
        }



        if (empty($this->ertek[9])) {
            $sql = "SELECT name FROM ".DB_PREFIX."option_szin_description WHERE option_szin_id = '".$this->ertek[18]."' AND language_id = '".$this->config->get('config_language_id')."'";
            $query = $this->db->query($sql);
            if ($query->num_rows) {
                $this->ertek[9] = $query->row['name'];
            }
        }

        if (empty($this->ertek[8]) && !empty($this->ertek[18])) {
            $sql = "SELECT option_value_id FROM ".DB_PREFIX."option_value WHERE option_id = '".$this->ertek[5]."' AND option_szin_id = '".$this->ertek[18]."'";
            $query = $this->db->query($sql);
            if ($query->num_rows) {
                $this->ertek[8] = $query->row['option_value_id'];
            } else {
                $sql = "SELECT szinkod FROM ".DB_PREFIX."option_szin WHERE option_szin_id = '".$this->ertek[18]."'";
                $query = $this->db->query($sql);

                $sql = "INSERT INTO ".DB_PREFIX."option_value SET
                            option_id       ='".$this->ertek[5]."',
                            heading         ='".$query->row['szinkod']."',
                            option_szin_id  ='".$this->ertek[18]."'";
                $query = $this->db->query($sql);
                $this->ertek[8] = $this->db->getLastId();
                foreach($languages as $value) {
                    $sql = "INSERT INTO ".DB_PREFIX."option_value_description SET
                                option_value_id  ='".$this->ertek[8]."',
                                option_id       ='".$this->ertek[5]."',
                                name            ='".$this->ertek[9]."',
                                language_id     ='".$value['language_id']."'";
                    $query = $this->db->query($sql);
                }


            }
        }

        if (empty($this->ertek[0])) {
            $sql = "SELECT product_option_value_id FROM ".DB_PREFIX."product_option_value WHERE
                        product_option_id   = '".$this->ertek[1]."' AND
                        product_id          = '".$this->ertek[2]."' AND
                        option_id           = '".$this->ertek[5]."' AND
                        option_value_id     = '".$this->ertek[8]."'";
            $query = $this->db->query($sql);
            if ($query->num_rows) {
                $this->ertek[0] = $query->row['product_option_value_id'];
            } else {
                $sql = "INSERT INTO ".DB_PREFIX."product_option_value set product_option_value_id=''";
                $query = $this->db->query($sql);
                $this->ertek[0] = $this->db->getLastId();
            }
        }

        $sql_set  = "product_option_value_id='".$this->ertek[0]."',";
        $sql_set .= "product_option_id='".$this->ertek[1]."',";
        $sql_set .= "product_id='".$this->ertek[2]."',";
        //$sql_set .= "model='".$this->ertek[3]."',";
        $sql_set .= "option_id='".$this->ertek[5]."',";
        $sql_set .= "option_value_id='".$this->ertek[8]."',";
        $sql_set .= "quantity='".$this->ertek[10]."',";
        $sql_set .= "subtract='".$this->ertek[11]."',";
        $sql_set .= "price='".$this->ertek[12]."',";
        $sql_set .= "price_prefix='".$this->ertek[13]."',";
        $sql_set .= "points='".$this->ertek[14]."',";
        $sql_set .= "points_prefix='".$this->ertek[15]."',";
        $sql_set .= "weight='".$this->ertek[16]."',";
        $sql_set .= "weight_prefix='".$this->ertek[17]."',";
        $sql_set .= "option_szin_id='".$this->ertek[18]."',";
        $sql_set .= "azonosito='".$this->ertek[19]."'";

        $sql = "SELECT product_option_value_id FROM ".DB_PREFIX."product_option_value WHERE product_option_value_id=".$this->ertek[0];
        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $sql = "INSERT INTO ".DB_PREFIX."product_option_value SET ";
            $query = $this->db->query($sql.$sql_set);
        } else {
            $sql = "UPDATE ".DB_PREFIX."product_option_value SET ";
            $sql .= $sql_set;
            $sql .= " WHERE product_option_value_id=".$this->ertek[0];
            $query = $this->db->query($sql);
        }

        // product_option
        $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_option SET product_option_id='".$this->ertek[1]."'";
        $query = $this->db->query($sql);
        $sql = "UPDATE ".DB_PREFIX."product_option SET product_id='".$this->ertek[2]."', option_id='".$this->ertek[5]."'";
        $sql .= " WHERE product_option_id='".$this->ertek[1]."'";
        $query = $this->db->query($sql);

        // product
        $sql = "INSERT IGNORE INTO ".DB_PREFIX."product SET product_id='".$this->ertek[2]."'";
        $query = $this->db->query($sql);

        //option
        $sql = "INSERT IGNORE INTO ".DB_PREFIX."option SET option_id='".$this->ertek[5]."'";
        $query = $this->db->query($sql);
        $sql = "UPDATE ".DB_PREFIX."option SET type='".$this->ertek[7]."'";
        $sql .= " WHERE option_id='".$this->ertek[5]."'";
        $query = $this->db->query($sql);


        return true;

        $query = $this->db->query($sql);
    }

    public function sorTorles() {
        $sql = "DELETE FROM ".DB_PREFIX."product_option_value WHERE product_option_value_id='".$this->ertek[0]."'";
        $query = $this->db->query($sql);
    }

    public function formatumEllenorzes($fejlec) {
        if ($fejlec[0] != 'product_option_value_id') return false;
        if ($fejlec[1] != 'product_option_id') return false;
        if ($fejlec[2] != 'product_id')  return false;
        if ($fejlec[3] != 'model')  return false;
        if ($fejlec[4] != 'product_name')  return false;
        if ($fejlec[5] != 'option_id')  return false;
        if ($fejlec[6] != 'option_name')  return false;
        if ($fejlec[7] != 'option_type')  return false;
        if ($fejlec[8] != 'option_value_id')  return false;
        if ($fejlec[9] != 'option_value_name')  return false;
        if ($fejlec[10] != 'quantity')  return false;
        if ($fejlec[11] != 'subtract')  return false;
        if ($fejlec[12] != 'price')  return false;
        if ($fejlec[13] != 'price_prefix')  return false;
        if ($fejlec[14] != 'points')  return false;
        if ($fejlec[15] != 'points_prefix')  return false;
        if ($fejlec[16] != 'weight')  return false;
        if ($fejlec[17] != 'weight_prefix')  return false;
        if ($fejlec[18] != 'option_szin_id')  return false;
        if ($fejlec[19] != 'azonosito')  return false;
        if ($fejlec[20] != 'checked')  return false;
        if ($fejlec[21] != 'required')  return false;
        if (isset($fejlec[22]) && $fejlec[22] != 'Delete')  return false;
        if (isset($fejlec[23]))  return false;

        return true;
    }

}