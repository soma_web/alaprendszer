<?php
// HTTP

define('HTTP_SERVER', 'http://optiweb.nmsnet.hu/');
define('HTTP_IMAGE', 'http://optiweb.nmsnet.hu/image/');
define('HTTP_ADMIN', 'http://optiweb.nmsnet.hu/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://optiweb.nmsnet.hu/');
define('HTTPS_IMAGE', 'http://optiweb.nmsnet.hu/image/');

// DIR
define('DIR_APPLICATION', '/home/optiweb/public_html/catalog/');
define('DIR_SYSTEM', '/home/optiweb/public_html/system/');
define('DIR_DATABASE', '/home/optiweb/public_html/system/database/');
define('DIR_LANGUAGE', '/home/optiweb/public_html/catalog/language/');
define('DIR_TEMPLATE', 'catalog/view/theme/');
define('DIR_CONFIG', '/home/optiweb/public_html/system/config/');
define('DIR_IMAGE_NYOMTAT', 'image/');
define('DIR_IMAGE', '/home/optiweb/public_html/image/');
define('DIR_CACHE', '/home/optiweb/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/optiweb/public_html/download/');
define('DIR_LOGS', '/home/optiweb/public_html/system/logs/');
define('DIR_TEMPLATE_IMAGE', '/catalog/view/theme/');

// DB

define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'optiweb');
define('DB_PASSWORD', 'opt_119');
define('DB_DATABASE', 'optiweb_optiweb');
define('DB_PREFIX', 'hungaro_');

?>
