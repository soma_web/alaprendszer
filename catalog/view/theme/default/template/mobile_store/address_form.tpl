<?php echo $header; ?>
<div id="content" class="round"><?php echo $content_top; ?>
 
  <h1><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="address">
    <div class="content">
		<span class="required">*</span> <?php echo $entry_firstname; ?><br />
        <input class="maximize" type="text" name="firstname" value="<?php echo $firstname; ?>" />
		<?php if ($error_firstname) { ?>
		<span class="error"><?php echo $error_firstname; ?></span>
		<?php } ?>
       
		<br /><br />
	   
        <span class="required">*</span> <?php echo $entry_lastname; ?><br />
        <input class="maximize" type="text" name="lastname" value="<?php echo $lastname; ?>" />
		<?php if ($error_lastname) { ?>
		<span class="error"><?php echo $error_lastname; ?></span>
		<?php } ?>
		
		<br /><br />
		
		<?php echo $entry_company; ?><br />
        <input class="maximize" type="text" name="company" value="<?php echo $company; ?>" />
		
		<br /><br />

        <?php echo $entry_adoszam; ?><br />
        <input class="maximize" type="text" name="adoszam" value="<?php echo $adoszam; ?>" />

        <br /><br />
		
        <span class="required">*</span> <?php echo $entry_address_1; ?><br />
          <input type="text" name="address_1" value="<?php echo $address_1; ?>" />
            <?php if ($error_address_1) { ?>
            <span class="error"><?php echo $error_address_1; ?></span>
            <?php } ?>
        
		<br /><br />
        
          <?php echo $entry_address_2; ?><br />
          <input class="maximize" type="text" name="address_2" value="<?php echo $address_2; ?>" />
        
        <br /><br />
		
          <span class="required">*</span> <?php echo $entry_city; ?><br />
          <input class="maximize" type="text" name="city" value="<?php echo $city; ?>" />
            <?php if ($error_city) { ?>
            <span class="error"><?php echo $error_city; ?></span>
            <?php } ?>
        
        <br /><br />
		
          <span class="required">*</span> <?php echo $entry_postcode; ?><br />
          <input class="maximize" type="text" name="postcode" value="<?php echo $postcode; ?>" />
            <?php if ($error_postcode) { ?>
            <span class="error"><?php echo $error_postcode; ?></span>
            <?php } ?>
        
        <br /><br />
		
          <span class="required">*</span> <?php echo $entry_country; ?><br />
          <select name="country_id" onchange="$('select[name=\'zone_id\']').load('index.php?route=account/address/zone&country_id=' + this.value + '&zone_id=<?php echo $zone_id; ?>');">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($countries as $country) { ?>
              <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
            <?php if ($error_country) { ?>
            <span class="error"><?php echo $error_country; ?></span>
            <?php } ?>
        
        <br /><br />
		
          <span class="required">*</span> <?php echo $entry_zone; ?><br />
          <select name="zone_id">
            </select>
            <?php if ($error_zone) { ?>
            <span class="error"><?php echo $error_zone; ?></span>
            <?php } ?>
        
		<br /><br />
        
          <?php echo $entry_default; ?><br />
          <?php if ($default) { ?>
            <input type="radio" name="default" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="default" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="default" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="default" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?>

    </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="button"><span><?php echo $button_back; ?></span></a></div>
      <div class="right"><a onclick="$('#address').submit();" class="button"><span><?php echo $button_continue; ?></span></a></div>
    </div>
  </form>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('select[name=\'zone_id\']').load('index.php?route=account/address/zone&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
//--></script> 
<?php echo $footer; ?>