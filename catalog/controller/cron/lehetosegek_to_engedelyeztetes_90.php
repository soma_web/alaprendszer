<?php   
class ControllerCronLehetosegekToEngedelyeztetes90 extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;


        $this->load->model("cron/altalanos");
        $this->load->model('cron/lehetosegek_to_engedelyeztetes_90');

        if ($cron_id) {
            $input = $this->model_cron_altalanos->getInputElements($cron_id);
        }
        echo 'Elindítva<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $potentials = $this->model_cron_lehetosegek_to_engedelyeztetes_90->getPotentials();

        echo '<br><br>Ajánlatkülédés összesen: '.count($potentials).' db';
        $this->model_cron_altalanos->addFuttatasEredmeny('Ajánlatkülédés összesen: '.count($potentials).' db');


        if ($potentials) {

            foreach($potentials as $key=>$value) {
                $siker = true;

                $potential = $this->model_cron_lehetosegek_to_engedelyeztetes_90->getPotential($value['potentialid']);
                if ($potential && ($potential['cf']['cf_2518'] || $potential['cf']['cf_2524'] || $potential['cf']['cf_2526'])  ) {

                    $product_price = $this->model_cron_lehetosegek_to_engedelyeztetes_90->getProductPrice($potential['cf'],(!empty($input['eloleg_afa']) ? $input['eloleg_afa'] : 27));

                    $potential['product'] = $this->model_cron_lehetosegek_to_engedelyeztetes_90->getProduct((!empty($input['eloleg_termek']) ? $input['eloleg_termek'] : 394507));
                    if ($potential['product']) {
                        $potential['product']['unit_price'] = round($product_price*0.9);
                        $siker = $this->model_cron_lehetosegek_to_engedelyeztetes_90->addPotential($potential);
                    } else {
                        $this->model_cron_altalanos->addFuttatasEredmeny('Van feldolgozható engedélyeztetés állapot, <br>van kijelölt ajánlat, <br>de az adminban megadott termék (szolgáltatás) ID nem található!');

                    }
                } else {
                    $this->model_cron_altalanos->addFuttatasEredmeny('Van feldolgozható engedélyeztetés állapot, <br> de nincs kijelölt ajánlat!');
                }
                if ($siker) {
                    $this->model_cron_lehetosegek_to_engedelyeztetes_90->deleteTriggerPotential($value);
                } else {
                    $this->model_cron_altalanos->addFuttatasEredmeny('Van feldolgozható engedélyeztetés állapot, <br> de az adatátvitel hibájából az adat nem törlődött a táblából!');

                }
            }
        } else {
            $this->model_cron_altalanos->addFuttatasEredmeny('Nincs feldolgozható engedélyeztetés állapot!');

        }


        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}

