<?php
// Heading
$_['heading_title']         = 'Árlista';
$_['common_title']		    = 'Árlista';

// Text
$_['text_module']           = 'Modulok';
$_['text_success']          = 'Sikeresen módosította az  árlista modult!';

//Entry
$_['entry_login']           = 'Belépés szükséges:<br /><span class="help">Árlista modul megtekintéséhez bejelentkezés szükséges</span>';
$_['entry_column_picture']  = 'Kép megjelenítése:';
$_['entry_column_model']    = 'Model megjelenítése:';
$_['entry_column_rate']     = 'Értékelés megjelenítése:';
$_['entry_column_buy']      = 'Vásárlás megjelenítése:';

// Error
$_['error_permission']    = 'Figyelmeztetés: Az árlista modul módosítása az Ön számára nem engedélyezett!';
?>