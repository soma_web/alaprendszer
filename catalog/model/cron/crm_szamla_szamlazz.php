<?php
class ModelCronCrmSzamlaSzamlazz extends Model {

	public function getOrders() {
        $vissza = array();

        $sql = "CREATE TABLE if not exists vtiger_invoice_szamla (
        `invoiceid`	            int(19),
        `invoicestatus`	        varchar(200) )
         engine=MyISAM default charset=UTF8";
        $query = $this->db_crm->query($sql);


        $sql = "SELECT * FROM vtiger_invoice_szamla";
        $query = $this->db_crm->query($sql);

        $query = $this->db_crm->query($sql);
        $vissza = $query->rows;

        return $vissza;
	}

    public function getOrder($invoiceid) {
        $vissza = array();

        $sql = "SELECT i.*, icf.cf_2439 AS teljesites, icf.cf_2443 AS fizetve FROM vtiger_invoice i ";
        $sql .= " LEFT JOIN vtiger_invoicecf icf ON (i.invoiceid=icf.invoiceid) ";
        $sql .= " WHERE i.invoiceid='".$invoiceid."'";
        $query = $this->db_crm->query($sql);

        if ($query->row) {
            $sql = "SELECT salesorder_no FROM vtiger_salesorder WHERE salesorderid = '".$query->row['salesorderid']."'";
            $query_sale = $this->db_crm->query($sql);
            $query->row['rendeles_szam'] = $query_sale->row['salesorder_no'];
            $vissza = $query->row;
        } else {
            $sql = "DELETE FROM vtiger_invoice_szamla WHERE invoiceid='".$invoiceid."'";
            $siker = $this->db_crm->query($sql);
            echo '<br>A számla hiányik, a továbbítási listából törölve:'.$invoiceid;
        }



        return $vissza;
	}

    public function getOrderPayment($invoiceid) {
        $vissza = array();

        $sql = "SELECT bill_city, bill_code, bill_country, bill_street  FROM vtiger_invoicebillads WHERE invoicebilladdressid   = '" . $invoiceid . "'";
        $query = $this->db_crm->query($sql);
        $vissza = $query->row;

        return $vissza;
    }

    public function getOrderShipping($invoiceid) {
        $vissza = array();

        $sql = "SELECT ship_city, ship_code,ship_country,ship_street FROM vtiger_invoiceshipads WHERE invoiceshipaddressid   = '".$invoiceid."'";
        $query = $this->db_crm->query($sql);
        $vissza = $query->row;

        return $vissza;
    }

    public function getOrderAccount($accountid) {

        if (!empty($accountid)) {

            $sql = "SELECT a.accountname, acf.cf_843 AS adoszam FROM vtiger_account a ";
            $sql .= " LEFT JOIN vtiger_accountscf acf ON (acf.accountid=a.accountid) ";
            $sql .= " WHERE a.accountid   = '" . $accountid . "'";
            $query = $this->db_crm->query($sql);
            if ($query->row) {
                $vissza = $query->row;
            } else {
                $vissza['accountname'] = '';
                $vissza['adoszam'] = '';
            }
        } else {
            $vissza['accountname'] = '';
            $vissza['adoszam'] = '';
        }
        return $vissza;
    }

    public function getOrderProducts($invoiceid) {
        $vissza = array();

        $sql = "SELECT * FROM vtiger_inventoryproductrel WHERE id = '".$invoiceid."'";
        $query = $this->db_crm->query($sql);

        if ($query->rows) {
            foreach($query->rows as $value){
                $sql = "SELECT productname FROM vtiger_products WHERE productid='".$value['productid']."'";
                $query_product = $this->db_crm->query($sql);
                if (empty($query_product->row)) {
                    $sql = "SELECT servicename FROM vtiger_service WHERE serviceid='".$value['productid']."'";
                    $query_product = $this->db_crm->query($sql);
                    if (empty($query_product->row)) {
                        return array();
                    }
                    $value['name'] = $query_product->row['servicename'];
                } else {
                    $value['name'] = $query_product->row['productname'];
                }
                if (!empty($query_product->row)) {
                    $vissza[] = $value;
                }
            }

        } else {
            $vissza = array();
        }

        return $vissza;
    }

    public function getOrderContact($contactid) {
        $vissza = array();
        if (count($this->db_crm)) {
            $sql = "SELECT * FROM vtiger_contactdetails WHERE contactid = '" . $contactid . "'";
            $query = $this->db_crm->query($sql);
            $vissza = $query->row;

        }

        return $vissza;

    }
    public function deleteTriggerOrder($data) {
        $sql = "DELETE FROM vtiger_invoice_szamla
          WHERE invoiceid='".$data['invoiceid']."'
            AND invoicestatus LIKE '".$data['invoicestatus']."'";
        $query = $this->db_crm->query($sql);
    }

    public function updateAllapotStatus($data) {
        if (!empty($data['siker'])) {
            $sql = "UPDATE vtiger_invoicecf SET
                        cf_2433 = 'Továbbítva',
                        cf_2494 = '".$data['szamlaszam']."'
                      WHERE invoiceid = '".$data['invoiceid']."'";
            $query = $this->db_crm->query($sql);

        } else {
            $sql = "UPDATE vtiger_invoicecf SET cf_2433 = 'Továbbítás sikertelen' WHERE invoiceid = '".$data['invoiceid']."'";
            $query = $this->db_crm->query($sql);
        }
    }


}
?>