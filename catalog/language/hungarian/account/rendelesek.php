<?php
// Heading
$_['heading_title']   = 'Letöltések jelentés';


// Text
$_['text_account']    = 'Fiók';
$_['text_home']       = 'Webáruház';

$_['entry_date_start']  = 'Kezdete:';
$_['entry_date_end']    = 'Vége:';
$_['entry_korosztaly']  = 'Korosztály:';
$_['entry_nem']         = 'Nemek:';
$_['entry_group']       = 'Csoportosítás: ';
$_['entry_status']      = 'Rendelés állapota:';
$_['entry_iskolai_vegzettseg'] = 'Iskolai végzettség';

$_['text_all_status']   = 'Minden állapot';
$_['text_week']         = 'Csoportosítás:';

$_['select_ferfi']      = 'Férfi';
$_['select_holgy']      = 'Nő';

$_['text_year']         = 'Év';
$_['text_month']        = 'Hónap';
$_['text_week']         = 'Hét';
$_['text_day']          = 'Nap';
$_['button_filter']     = 'Szűrés';

$_['column_date_start'] = 'Kezdete';
$_['column_date_end']   = 'Vége';
$_['column_orders']     = 'Letöltések száma';
$_['column_products']   = 'Kuponok száma';
$_['column_total']      = 'Összesen';
$_['column_iskolai_vegzettseg'] = 'Iskolai végzettség';

$_['text_no_results']  = 'A megadott feltételek szerint nincs letöltött termék';
?>