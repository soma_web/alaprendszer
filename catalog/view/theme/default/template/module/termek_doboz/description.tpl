<?php if(isset($beallitas['lenyilo']) && $beallitas['lenyilo'] && isset($product['description']) && ($product['description']) != "") { ?>

    <script type="text/javascript">
        /* .:Lenyíló div:. - pl.: Optiweb - termékdobozoknál
         *
         *   Működés: Mouseenter esetén meghatározza az adminban megadott
         *   div (class alapján) pozícióját (bottom, left) és a másik
         *   megadott divet a kiszámolt pozícióhoz igazítja.
         *
         * */
        /* CSS a stylesheet.css-ben, itt csak a működés zajlik */
        $('.<?php echo $beallitas['lenyilo']; ?>').mouseenter(function(){
            if($("div.sdesc").length == 0){
                var short = $(this).find('.<?php echo $beallitas['lenyilo_mit']; ?>').clone().html();
                var left = $(this).offset().left;
                var top = $(this).offset().top;
                var bottom = top+$(this).outerHeight();
                var width = $(this).outerWidth();

                if (!isEmpty($(this).find('.description'))) {
                    $('body').append('<div class="sdesc" id="sdecs_div" style="width: '+width+'px; display: none;">'+short+'</div>');
                    $('.sdesc').offset({top:bottom-2, left:left});
                    $('.sdesc').stop().slideDown(300);
                    $(this).css('border-bottom-left-radius', 0);
                    $(this).css('border-bottom-right-radius', 0);
                    $(this).css('border-bottom', 0);
                }

            }
        });

        function isEmpty( el ){
            return !$.trim(el.html());
        }


        /*
         * .:Lenyíló div:. - eltűntetés
         *
         *   Mouseleave esetén (kimegyünk az kurzorral a megadott divből)
         *   Eltűntetjük az előző pontban megjelenített divet.
         *
         * */

        $('.<?php echo $beallitas['lenyilo']; ?>').mouseleave(function(){
            /*$(this).removeAttr('style');*/
            $(this).css('border','2px solid #b3258b');
            $(this).css('border-radius','10px');
            $('div.sdesc').remove();
        });




    </script>
<?php } ?>

<div class="description">
    <?php echo mb_substr($product['description'], 0, $beallitas['karakter'], "UTF-8");
    if(!empty($product['description']) && strlen($product['description']) > $beallitas['karakter'] ){
        echo "...";
    } ?>
</div>