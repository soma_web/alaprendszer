<?php
// Heading 
$_['heading_title'] 	 = 'Hírlevél feliratkozás';

//Fields
$_['entry_email'] 		 = 'E-mail';
$_['entry_name'] 		 = 'Név';

//Buttons
$_['entry_button'] 		 = 'Feliratkozás';
$_['entry_unbutton'] 	 = 'Leiratkozás';

//text
$_['text_subscribe'] 	 = 'Feliratkozás itt';

$_['mail_subject']   	 = 'Hírlevél feliratkozás';

//Error
$_['error_invalid'] 	 = 'Érvénytelen Email';
$_['error_invalid_name'] = 'Kérem, adja meg a nevét! (min.3 betű)';

$_['subscribe']	    	 = 'Sikeresen feliratkozott';
$_['unsubscribe'] 	     = 'Sikeresen leiratkozott';
$_['alreadyexist'] 	     = 'Már létezik';
$_['notexist'] 	    	 = 'Email azonosító nem létezik';

?>