<?php
// HTTP

define('HTTP_SERVER', 'http://ptp.nmsnet.hu/');
define('HTTP_IMAGE', 'http://ptp.nmsnet.hu/image/');
define('HTTP_ADMIN', 'http://ptp.nmsnet.hu/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://ptp.nmsnet.hu/');
define('HTTPS_IMAGE', 'http://ptp.nmsnet.hu/image/');

// DIR
define('DIR_ARUHAZ', '/home/ptpnmsnet/public_html/');
define('DIR_APPLICATION', '/home/ptpnmsnet/public_html/catalog/');
define('DIR_SYSTEM', '/home/ptpnmsnet/public_html/system/');
define('DIR_DATABASE', '/home/ptpnmsnet/public_html/system/database/');
define('DIR_LANGUAGE', '/home/ptpnmsnet/public_html/catalog/language/');
define('DIR_TEMPLATE', 'catalog/view/theme/');
define('DIR_CONFIG', '/home/ptpnmsnet/public_html/system/config/');
define('DIR_IMAGE', '/home/ptpnmsnet/public_html/image/');
define('DIR_CACHE', '/home/ptpnmsnet/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/ptpnmsnet/public_html/download/');
define('DIR_LOGS', '/home/ptpnmsnet/public_html/system/logs/');
define('DIR_TEMPLATE_IMAGE', '/catalog/view/theme/');

// DB

define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'ptpnmsnet');
define('DB_PASSWORD', 'ptp_119');
define('DB_DATABASE', 'ptpnmsne_ptpnmsne');
define('DB_PREFIX', 'hungaro_');
?>