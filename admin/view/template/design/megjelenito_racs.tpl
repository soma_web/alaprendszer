<?php if (empty($sablonvalaszto)) { ?>

    <?php  echo $header; ?>

    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <?php if ($success) { ?>
            <div class="success"><?php echo $success; ?></div>
        <?php } ?>
        <div class="box">
        <div class="heading">
            <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
        <div class="content">
            <input id="megmutat" type="button" class="button szinezett" value="Előnézet" ><br>

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

                <table>
                    <tr>
                        <td title="racs_beallitas_racs[modularis]" style="width: 244px;"><?php echo "Moduláris megjelenítés:"; ?></td>
                        <td style="width: 144px;">
                            <?php if ($racs_beallitas_racs['modularis']) { ?>
                                <input type="radio" name="racs_beallitas_racs[modularis]" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="racs_beallitas_racs[modularis]" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="racs_beallitas_racs[modularis]" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="racs_beallitas_racs[modularis]" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                    </tr>
                </table>

                <div class="csoportok_nyito"  id="sablon" onclick="csikiCsuki(this)" style="width: 98%; padding: 10px; margin: 20px 0; font-weight: bold; color:rgb(146, 0, 0);">Sablonok engedélyezés, tiltása</div>
                <table class="csoport_beallitasok" style="margin: -20px 0 20px 0;">
                    <tr>
                        <td>
                            <div class="scrollbox">
                                <?php $class = 'odd'; ?>
                                <?php foreach ($racs_beallitas_alap as $fileskey=>$beallitdoboz) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div class="<?php echo $class; ?>">
                                        <?php if (!empty($racs_beallitas_alap[$fileskey]['status'])) { ?>
                                            <?php $disabled = ""?>
                                            <input class="valaszto"  type="checkbox" name="racs_beallitas_alap[<?php echo $fileskey?>][status]" value="1" checked="checked"  onclick="sablonValaszt()"/>
                                            <span class='szoveg'><?php echo $fileskey; ?></span>
                                        <?php } else { ?>
                                            <input class="valaszto"   type="checkbox" name="racs_beallitas_alap[<?php echo $fileskey?>][status]" value="1"  onclick="sablonValaszt()"/>
                                            <span class='szoveg'><?php echo $fileskey; ?></span>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </td>
                        <td style="vertical-align: bottom">
                            <input type="button" class="button" onclick="$('#form').submit()" value="Változások átvezetése">
                        </td>
                    </tr>
                </table>
<?php } ?>
            <div id="racs_altalanos">
                <a onclick="addCsoport();" class="button">Csoport hozzáadása</a>

                <?php if (isset($racs_beallitas_racs_csoportok) && $racs_beallitas_racs_csoportok) {?>
                    <?php $elemszam = 0?>
                    <?php for($i=0; count($racs_beallitas_racs_csoportok_kiegeszito) > $i; $i++) {?>
                        <?php $key_aktualis = $racs_beallitas_racs_csoportok_kiegeszito[$i];?>
                        <?php $key_kovetkezo = isset($racs_beallitas_racs_csoportok_kiegeszito[$i+1]) ? $racs_beallitas_racs_csoportok_kiegeszito[$i+1] : $racs_beallitas_racs_csoportok_kiegeszito[$i];?>
                        <?php $csoportok = $racs_beallitas_racs_csoportok[$key_aktualis]?>

                        <?php $tomb_mostani = explode("_",$key_aktualis);?>
                        <?php $tomb_kovetkezo = explode("_",$key_kovetkezo);?>
                        <?php $kovetkezo_elemszam = count($tomb_kovetkezo);?>

                        <?php if ($elemszam < count($tomb_mostani) || true) {?>
                            <?php $elemszam = count($tomb_mostani);?>
                        <?php } ?>

                        <div class="csoportok sortitem" id="csoport_<?php echo $key_aktualis?>" style="padding-left: <?php echo (strlen($key_aktualis)+8)?>px" >
                            <span class="sorthandle"><img src="view/image/icon-drag-drop.png"></span>
                            <div class="csoportok_nyito"  onclick="csikiCsuki(this)">
                                <?php $sor = true?>
                                <?php foreach($tomb_mostani as $value) {?>
                                    <?php if ($sor){?>
                                        <span>Sor: <b><?php echo $value?></b>  </span>
                                        <?php $sor = false;?>
                                    <?php } else {?>
                                        <span>Oszl.: <b><?php echo $value?></b>  </span>
                                        <?php $sor = true;?>
                                    <?php }?>
                                <?php } ?>

                                <?php
                                $inttomb = array();
                                foreach($tomb_mostani as $value){
                                    $inttomb[] = (int)$value;
                                }
                                ?>
                                <a onclick="$('#csoport<?php echo "_".$key_aktualis;?>').remove();" class="button"><?php echo $button_remove; ?></a>
                                <a onclick="addCsoport(<?php echo json_encode($inttomb)?>);" class="button">Új csoport</a>
                                <a onclick="addPaste('<?php echo $key_aktualis;?>');" class="button beillesztes">Beillesztés</a>
                                <a style="text-decoration: none; margin-right: 40px;">
                                    <?php echo $entry_status?>
                                    <?php if (isset($racs_beallitas_racs_csoportok[$key_aktualis]['default']['status']) && $racs_beallitas_racs_csoportok[$key_aktualis]['default']['status'] == 1) {?>
                                        <input type="radio" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][default][status]" value="1" checked="checked" />
                                        <input type="radio" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][default][status]" value="0"  />
                                    <?php } else {?>
                                        <input type="radio" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][default][status]" value="1"  />
                                        <input type="radio" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][default][status]" value="0" checked="checked" />
                                    <?php } ?>
                                </a>
                                <a class="input_tol_ig">Felbontás (tól -ig):
                                    <input type="text" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][default][width_tol]" size="3"
                                           value="<?php echo isset($racs_beallitas_racs_csoportok[$key_aktualis]['default']['width_tol']) ? $racs_beallitas_racs_csoportok[$key_aktualis]['default']['width_tol'] : 0; ?>"/>-
                                    <input type="text" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][default][width_ig]" size="3"
                                           value="<?php echo isset($racs_beallitas_racs_csoportok[$key_aktualis]['default']['width_ig']) ? $racs_beallitas_racs_csoportok[$key_aktualis]['default']['width_ig'] : 3000; ?>" />
                                </a>
                                <a class="tartalom" style="margin-right: 30px;">
                                    <?php if(isset($racs_beallitas_racs_csoportok[$key_aktualis]['templates'])) { ?>
                                        <img src="view/image/tartalom1.png">
                                    <?php } ?>
                                </a>
                            </div>

                            <div class="csoport_beallitasok">
                                <div>
                                    <div class="scrollbox">
                                        <?php $class = 'odd'; ?>
                                        <?php foreach ($racs_beallitas_racs_csoportok_files as $fileskey=>$beallitdoboz) { ?>
                                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                            <div class="<?php echo $class; ?>">
                                                <?php if (isset($racs_beallitas_racs_csoportok[$key_aktualis]['templates'][$fileskey]['status']) && $racs_beallitas_racs_csoportok[$key_aktualis]['templates'][$fileskey]['status'] == 1) { ?>
                                                    <?php $disabled = ""?>
                                                    <input class="valaszto" onclick="elrejt(this);" type="checkbox" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][status]" value="1" checked="checked" />
                                                    <span class='szoveg'><?php echo $fileskey; ?></span>
                                                <?php } else { ?>
                                                    <input class="valaszto" onclick="elrejt(this);" type="checkbox" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][status]" value="1" />
                                                    <span class='szoveg'><?php echo $fileskey; ?></span>
                                                    <?php $disabled = "disabled"?>

                                                <?php } ?>

                                                <?php if (count($beallitdoboz) > 0) {?>
                                                    <?php
                                                        $key = $fileskey;
                                                        $melyik_doboz = "racs_beallitas_racs_csoportok[$key_aktualis][templates]"
                                                    ?>
                                                    <span>
                                                    <?php $doboz = isset($racs_beallitas_racs_csoportok[$key_aktualis]['templates'][$fileskey]) ? $racs_beallitas_racs_csoportok[$key_aktualis]['templates'][$fileskey] : ""; ?>
                                                    <?php foreach ($beallitdoboz as $keydoboz=>$tomb) { ?>
                                                        <?php if(count($tomb) > 1) { ?>
                                                                <?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    </span>
                                                <?php } ?>

                                                <?php if (isset($racs_beallitas_racs_csoportok[$key_aktualis]['templates'][$fileskey]['sort_order'])) { ?>
                                                    <input <?php echo $disabled;?> class="sorrend" type="text" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][sort_order]" value="<?php echo $racs_beallitas_racs_csoportok[$key_aktualis]['templates'][$fileskey]['sort_order']?>" size="2"/>
                                                <?php } else { ?>
                                                <input <?php echo $disabled;?> class="sorrend" type="text" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][sort_order]" value="0" size="2"/>
                                            <?php } ?>

                                        </div>
                                    <?php } ?>
                                </div>
                                <br>
                                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);
                                            $(this).parent().find('input, select').attr('disabled',false);"><?php echo $text_select_all; ?>
                                </a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);
                                                   $(this).parent().find('input, select').attr('disabled',true);
                                                   $('.valaszto').attr('disabled',false)"><?php echo $text_unselect_all; ?>
                                </a>
                                <br>
                            </div>
                            <div class="kijeloles">
                                <a onclick="addCopy('<?php echo $key_aktualis;?>');" class="button masolas">Kijelölés másolásra</a>
                                <a onclick="deleteCopy();" style="display: none; background: #B60024;" class="button kijeloles_torles">Kijelölés törlése</a>

                                <span> <b>CSS display érték:</b> (pl.: inline-block: )
                                    <input type="text" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][default][display]" size="8"
                                           value="<?php echo isset($racs_beallitas_racs_csoportok[$key_aktualis]['default']['display']) ? $racs_beallitas_racs_csoportok[$key_aktualis]['default']['display'] : "block"; ?>" />
                                </span>
                                <span> <b>CSS class neve:</b>
                                    <input type="text" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][default][class]" size="18"
                                           value="<?php echo isset($racs_beallitas_racs_csoportok[$key_aktualis]['default']['class']) ? $racs_beallitas_racs_csoportok[$key_aktualis]['default']['class'] : ""; ?>" />
                                </span>
                                <span> <b style="vertical-align: top; width: 93px; display: inline-block">Inline style:</b>
                                    <textarea style="width: 300px; height: 100px;" name="racs_beallitas_racs_csoportok[<?php echo $key_aktualis?>][default][inline]"><?php echo isset($racs_beallitas_racs_csoportok[$key_aktualis]['default']['inline']) ? $racs_beallitas_racs_csoportok[$key_aktualis]['default']['inline'] : ""; ?></textarea>
                                </span>
                            </div>
                        </div>
                        <div class="div_csoport" style="position: relative">

                        <?php if ($kovetkezo_elemszam <= $elemszam ) {
                            for($j=$kovetkezo_elemszam; $j<= $elemszam; $j++){
                                echo "</div>";
                            }
                        } ?>
                        <?php if ($kovetkezo_elemszam <= $elemszam ) {
                            for($j=$kovetkezo_elemszam; $j<= $elemszam; $j++){
                                echo "</div>";
                            }
                        } ?>
                <?php } ?>
                <?php for($j=1; $j< $elemszam; $j++){
                     echo "</div>";
                }?>
            <?php } ?>
        </div>

<?php if (empty($sablonvalaszto)) { ?>

        </form>
    </div>


    <?php $modul_name = "racs_beallitas_racs_csoportok"?>
    <?php $csoportok_files = &$racs_beallitas_racs_csoportok_files?>
    <?php $hozzafuz = "racs_altalanos"?>
    <?php $megjelenito = "megjelenito_racs"?>
    <?php $nezet = "racsnezet"?>
    <script>
        root = 'category';
    </script>
    <?php include_once(DIR_TEMPLATE. "design/modularis_js.php")?>
    </div>
    </div>
    <?php echo $footer; ?>
<?php } ?>