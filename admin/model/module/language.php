<?php
class ModelModuleLanguage extends Model {

    private $language  = array();
    private $directory  = '';

    public function getLanguages() {

        $config_language_id = $this->config->get('config_language_id');
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        foreach($languages as $key=>$language) {
            if ($language['language_id'] == $config_language_id) {
                $dir = DIR_CATALOG.'language/'.$language['directory'];

                $this->language = $language_tree[$language['directory']] = $this->getDir($dir);
                $this->directory = $language['directory'];

                $language_files[$language['directory']] = $this->getFiles($dir);
                if ($language_files[$language['directory']]) {
                    foreach($language_files[$language['directory']] as $keyfile=>$value) {
                        $language_tree[$language['directory']][$keyfile] = $value;
                    }
                }
                unset ($languages[$key]);
            }
        }

        foreach($languages as $key=>$language) {
            $dir = DIR_CATALOG.'language/'.$language['directory'];

            $language_tree[$language['directory']]  = $this->getDir($dir);
            $language_tree[$language['directory']]  = $this->merger($language_tree[$language['directory']]);

            $language_files[$language['directory']] = $this->getFiles($dir);
            if ($language_files[$language['directory']]) {
                foreach($language_files[$language['directory']] as $keyfile=>$value) {
                    $language_tree[$language['directory']][$keyfile] = $value;
                }
            } else {
                $language_tree[$language['directory']][$language['directory'].'.php'] = array();
            }
        }

        foreach($language_tree as $key=>$language) {
            if ($key != $this->directory) {
                $language_tree[$key][$key.'.php'] = array_merge($language_tree[$this->directory][$this->directory.'.php'], $language_tree[$key][$key.'.php']);

            }
        }

        echo '';
    }

    public function getDir($directory) {
        $vissza = array();
        $dirs = array_filter(glob($directory.'/*'), 'is_dir');

        foreach($dirs as $dir_key=>$dir) {
            $files = glob($dir.'/*.php');
            if ($files) {

                foreach ($files as $file) {
                    $_ = array();
                    require_once($file);
                    $vissza[basename($dir)][basename($file)] = $_;

                }
            }
        }

        return $vissza;
    }

    public function getFiles($directory) {
        $vissza = array();
        $files = glob($directory.'/*.php');
        if ($files) {
            foreach ($files as $file) {
                $_ = array();
                require_once($file);
                $vissza[basename($file)] = $_;
            }
        }
        return $vissza;
    }

    public function merger($alap) {

        $vissza = array_merge($this->language,$alap);
        foreach($this->language as $key=>$subdir) {
            if (is_array($this->language[$key])) {
                $vissza[$key] = array_merge($this->language[$key], $vissza[$key]);
                foreach ($subdir as $subkey => $value) {
                    $vissza[$key][$subkey] = array_merge($this->language[$key][$subkey], $vissza[$key][$subkey]);
                }
            }
        }
        return $vissza;
    }
}
?>