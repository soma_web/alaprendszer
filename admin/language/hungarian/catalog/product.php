<?php

// Heading
$_['heading_title']          = 'Termékek';

// Text
$_['text_success']           = 'Siker: A termék módosítása megtörtént!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Alapértelmezett';
$_['text_image_manager']     = 'Képszerkesztő';
$_['text_browse']            = 'Fájlok tallózása';
$_['text_clear']             = 'Kép törlése';
$_['text_option']            = 'Választék';
$_['text_option_value']      = 'Választék értéke';
$_['text_percent']           = 'Százalék';
$_['text_amount']            = 'Fix érték';
$_['text_kupon_keszit']     = 'Kupon készítése';

$_['text_szuro_csoport']    = 'Beváltás helye:';
$_['text_szuro']            = 'Beváltási pont:';
$_['text_kaphato']          = 'Beváltható:';
$_['text_utalvany_erteke']  = 'Az utalvány értéke:';

// Column
$_['column_garancia_ertek']  = 'Garancia';
$_['column_date_modify']     = 'Módosítás dátuma';
$_['column_date_added']      = 'Létrehozás dátuma';
$_['column_category']        = 'Kategóriák';
$_['column_name']            = 'Terméknév';
$_['column_model']           = 'Modell';
$_['column_cikkszam']        = 'Cikkszám';
$_['column_cikkszam2']       = 'Termékkód';
$_['column_kifuto']          = 'Kifutó';
$_['column_ujdonsag']        = 'Újdonság';
$_['column_elorendeles']     = 'Ajánlatkérések';
$_['column_megrendelem']     = 'Megrendelem';
$_['column_ar_tiltasa']      = 'Ár letiltva';
$_['column_product_id']      = 'Termék ID';
$_['column_image']           = 'Kép';
$_['column_quantity']        = 'Mennyiség';
$_['column_status']          = 'Állapot';
$_['column_action']          = 'Művelet';
$_['column_price']           = 'Ár';

// Entry
$_['entry_helyettesito']     = 'Helyettesítő termékek:';
$_['entry_stock_date']       = 'Várható beérkezés:';
$_['entry_lizing']           = 'Lízing:';
$_['entry_kiszereles']       = 'Kiszerelés:';
$_['entry_kiszereles_mertekegyseg'] = 'Kiszerelés mértékegysége:';
$_['entry_checked']          = 'Bepipált:';
$_['entry_kifuto']           = 'Kifutó termék:';
$_['entry_ujdonsag']         = 'Újdonság:';
$_['entry_elorendeles']      = 'Csak rendelhető:';
$_['entry_megrendelem']      = 'Megrendelem gomb:';
$_['entry_ar_tiltasa']       = 'Ár letiltva:';
$_['entry_video']            = 'Videó:';
$_['entry_ajandek']          = 'Ajándék:';
$_['entry_general_accessory']= 'Általános tartozékok:';
$_['entry_garancia']         = 'Garancia:';
$_['entry_garancia_honap']   = 'Hónap';
$_['entry_garancia_ev']      = 'Év';
$_['entry_image_letoltheto']  = 'Kapcsolódó letölthető kép:';
$_['entry_name']             = 'Terméknév:';
$_['entry_szallitas']        = 'Egyedi szállítási díj:';
$_['entry_meta_keyword']     = 'Meta Tag Kulcsszó:';
$_['entry_meta_description'] = 'Meta leírás:';
$_['entry_description']      = 'Ismertető:';
$_['entry_store']            = 'Áruházak:';
$_['entry_keyword']          = 'URL-ben megjelenő SEO kulcsszó:';
$_['entry_model']            = 'Modell:';
$_['entry_cikkszam']         = 'Cikkszám:';
$_['entry_cikkszam2']        = 'Termékkód:';
$_['entry_filter']           = 'Szűrő:';
$_['entry_sku']              = 'Egyedi azonosító:';
$_['entry_upc']              = 'Vonalkód:';
$_['entry_location']         = 'Egyéb:';
$_['entry_manufacturer']     = 'Gyártó:';
$_['entry_shipping']         = 'Szállítandó termék:';
$_['entry_date_available']   = 'Mikortól kapható:';
$_['entry_date_kaphato_ig']  = 'Meddig kapható:';
$_['entry_quantity']         = 'Mennyiség:';
$_['entry_fizetes_status']   = 'Fizetés státusza:';
$_['entry_fizetendo']        = 'Ára:';
$_['entry_kep_tiltas']       = 'Megjelenés letiltva:';
$_['mennyisegi_egyseg']      = 'Mennyiségi egység:';
$_['csomagolasi_egyseg']     = 'Csomagolási egység:';
$_['csomagolasi_mennyiseg']  = 'Csomagolási Mennyiség:';
$_['entry_kiemelt']          = 'Kiemelt:';
$_['entry_short_description']= 'Rövid leírás:';
$_['entry_test_description'] = 'Tesztek:';
$_['entry_accessory']        = 'Ajánlott tartozékok:';
$_['entry_garancia_description']= 'Garancia leírás:';
$_['entry_termek_vevo']      = 'Kapcsolódó partnerek:<br/><span class="help">Front-end oldalon ellenőrizhetik a voucher tartozó vonalkóddal a vonalkód érvényességét.</span>';
$_['entry_szazalek_kedvezmeny']= 'Maximum adható kedvezmény mértéke:';
$_['entry_size_color_required']= 'Méret - szín kötelező:';

$_['entry_minimum']          = 'Minimális mennyiség :<br/><span class="help">Minimális vásárlási mennyiség</span>';
$_['entry_stock_status']     = 'Hiánycikk állapot: <br/><span class="help">Az az állaptot, amit megjelenik, ha egy termék nincs raktáron</span>';
$_['entry_price']            = 'Ár:';
$_['entry_package']          = 'Csomag ajánlat:';
$_['entry_eredeti_ar']       = 'Eredeti ár:';
$_['entry_utalvany']         = 'Ingyenes termék:';
$_['entry_szazalek']         = 'Ár százalékban értendő:';
$_['entry_tax_class']        = 'Áfakulcs:';
$_['entry_points']           = 'Pontok:<br/><span class="help">Ennyi pontra van szükséged a termék megvásárlásához. Amennyiben nincs rás szüksége 0-val kiléphet.</span>';
$_['entry_option_points']    = 'Pontok:';
$_['entry_subtract']         = 'Készlet kivonása:';
$_['entry_weight_class']     = 'Súlymérték:';
$_['entry_weight']           = 'Súly:';
$_['entry_length']           = 'Hosszmérték:';
$_['entry_dimension']        = 'Méret (Hosszúság × Szélesség × Magasság):';
$_['entry_image']            = 'Kép:<br /><span class="help">Kattints a módosításhoz!</span>';
$_['entry_customer_group']   = 'Ügyfél csoport:';
$_['vevo_kedvezmeny']        = 'Ügyfél:';
$_['entry_date_start']       = 'Kezdő dátum:';
$_['entry_letoltes']         = 'Letölthető kép (kupon):';
$_['entry_date_end']         = 'Záró dátum:';
$_['entry_priority']         = 'Prioritás:';
$_['entry_attribute']        = 'Jellemző:';
$_['entry_attribute_group']  = 'Jellemző csoport:';
$_['entry_text']             = 'Szöveg:';
$_['entry_option']           = 'Választék:';
$_['entry_option_value']     = 'Választék érték:';
$_['entry_required']         = 'Szükséges:';
$_['entry_status']           = 'Állapot:';
$_['entry_sort_order']       = 'Sorrend:';
$_['entry_category']         = 'Kategóriák:';
$_['entry_download']         = 'Letöltések:';
$_['entry_related']          = 'Kapcsolódó termékek:<br /><span class="help">(Autocomplete)</span>';
$_['entry_tag']   	         = 'Termék címkék:<br /><span class="help">Vesszővel elválasztva</span>';
$_['entry_reward']           = 'Jutalékpont:';
$_['entry_layout']           = 'Elrendezés felülbírálása:';
$_['entry_fizetes_status']   = 'Fizetés státusza:';
$_['entry_egyszer_kosarba']  = 'Csak egyszer tehető kosárba:';
$_['entry_package_alapar']   = 'Alapár';
$_['entry_package_csomagar'] = 'Csomagár';
$_['entry_package_csomagar_ossz']  = 'Csomagár össz.';

$_['entry_matchdate']        = 'Meccs dátuma:';
$_['entry_seat']             = 'Ülőhely:';

$_['entry_elhelyezkedes']           = 'Elhelyezkedés I.:';
$_['entry_elhelyezkedes_alcsoport'] = 'Elhelyezkedés II.:';
$_['entry_kiemelt_keret']           = 'Keret:';
$_['entry_idoszak']          = 'Időszak';
$_['entry_idoszak_darab']    = 'Mennyiség';

$_['entry_uj']              = 'Új:';
$_['entry_hasznalt']        = 'Használt:';
$_['entry_berelheto']       = 'Bérelhető:';
$_['entry_allapot']         = 'Tulajdonság:';
$_['entry_csomagar_ossz']   = 'Csomag értéke össz.:';
$_['entry_csomagar_akutalizal'] = 'Csomag árának aktualizásása';


$_['text_nap']               = 'Nap';
$_['text_het']               = 'Hét';
$_['text_honap']             = 'Hónap';
$_['text_ev']                = 'Év';

$_['text_fizetes_alatt']     = 'Fizetés alatt';
$_['text_kifizetve']         = 'Fizetve';

// Error
$_['error_warning']          = 'Figyelem: Kérjük gondosan nézze át a hibákat az űrlapon!';
$_['error_permission']       = 'Figyelmeztetés: A termékek módosítása az Ön számára nem engedélyezett';
$_['error_name']             = 'A terméknév legalább 3, és legfeljebb 255 karakterből álljon!';
$_['error_model']            = 'A termék gyártmányának megnevezése legalább 3, és legfeljebb 64 karakterből álljon!';
$_['error_required_data']    = 'A szükséges adatok hiányoznak. Ellenőrizze a mezőket!';

$_['error_price']            = 'Kérem adja meg az árat!';
$_['error_negativ_ar']       = 'Nem lehet 0 vagy negativ árat megadni nem ingyenes termék esetén!';
$_['error_product_filter']   = 'Kérem válasszon legalább egy szektort!';
$_['error_date_ervenyes_ig'] = 'Kérem adja meg az érvényesség dátumát!';
$_['error_letoltheto_kep']   = 'Kérem töltsön fel kupont!';

$_['tab_kepek_helyszinenkent']      = 'Képek helyszínenként';
$_['entry_helyszin']         = 'Beváltási helyek';
$_['entry_generalt_nev']            = 'Név generálása';

$_['button_csv_export']            = 'CSV Export';
$_['button_csv_import']            = 'CSV Import';



$_['text_valasszon']                = '-- Válasszon --';
$_['text_import_valasszon']                = '-- Válasszon (Importálás) --';
$_['text_termekjellemzok']          = 'Termék tulajdonságok';
$_['text_kapcsolodotermekek']       = 'Kapcsolódó termékek';
$_['text_ajanlott_tartozekok']      = 'Ajánlott tartozékok';
$_['text_altalanos_tartozekok']     = 'Általános tartozékok';
$_['text_engedelyezett']            = 'Engedélyezett';
$_['text_letiltott']                = 'Letiltott';
$_['text_file_feltoltes']           = 'File feltöltés';
$_['text_file_importalas']          = 'File importálás';
$_['text_kerem_toltsefel']          = 'Kérem töltse fel a file-t!';
$_['text_crm_tipus']                = 'CRM-ben tárolt <b>tipus:</b>';
$_['text_crm_category']             = 'CRM-ben tárolt <b>kategória:</b>';
$_['text_crm_product']              = 'CRM-ben tárolt <b>termék:</b>';



$_['elhelyezkedesek_kulonbseg_error']         = 'Figyelmeztetés! Az adott időszakra már nincs szabad hely.';
$_['elhelyezkedes_alcsoport_kulonbseg_error'] = 'Figyelmeztetés! Az adott időszakra már nincs szabad hely.';
$_['kiemelesek_kulonbseg_error']              = 'Figyelmeztetés! Az adott időszakban már nincs lehetőség a kiemelésre.';

//Video fül
$_['button_add_video']            = 'Videó hozzáadása';
$_['button_add_pdf']              = 'Pdf hozzáadása';
$_['entry_video']                 = 'Videó:';
$_['entry_pdf']                   = 'Pdf:';
$_['tab_videok']                  = 'Videók';
$_['tab_pdf']                     = 'Pdf-ek feltöltése';
$_['tab_crm_kapcsolat']           = 'CRM beállítások';
$_['entry_video_link']            = 'Videó elérhetősége';
$_['entry_pdf_link']              = 'Pdf elérhetősége';
$_['entry_video_title']           = 'Videó címe';
$_['entry_pdf_title']             = 'Pdf címe';

$_['success_modositas']  = 'Sikerült! A módosítás megtörtént.';
$_['error_sql']  = 'Figyelem: Hiba történt az adatbázis módosítása során!';
$_['error_post']  = 'Figyelem: Hiba történt az adatátvitel során!';
$_['not_crmtipus']  = 'Figyelem: CRM típus hiányzik!';

?>
