<?php

// Heading
$_['heading_title']      = 'Authorize.Net (SIM)';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Siker: A Authorize.Net SIM fiók részleteinek módosítása megtörtént!';

// Entry
$_['entry_merchant']     = 'Kereskedő ID:';
$_['entry_key']          = 'Tranzakció kulcs:';
$_['entry_callback']     = 'Közvetítő válasz URL:<br /><span class="help">Kérem jelentkezzen be és állítsa be itt: <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.</span>';
$_['entry_test']         = 'Teszt mód:';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';

// Error
$_['error_permission']   = 'Figyelmeztetés: Az Authorize.Net (SIM) történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_merchant']     = 'Kereskedő ID szükséges!';
$_['error_key']          = 'Tranzakció kulcs szükséges!';
?>
