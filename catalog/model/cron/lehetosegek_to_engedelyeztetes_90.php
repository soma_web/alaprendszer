<?php
class ModelCronLehetosegekToEngedelyeztetes90 extends Model {

    private $crm_id;
    private $contact_no;
    private $account_crm_id = 0;
    private $account_no     = '';
    private $potential_id = 0;
    private $potential_no = '';
    private $lead_id;
    private $quoteid = 0;
    private $quote_no = 0;

	public function getPotentials() {
        $sql = "CREATE TABLE if not exists vtiger_salesorder90 (
        `potentialid`	            int(19),
        `sales_stage`	        varchar(200) )
         engine=MyISAM default charset=UTF8";
        $query = $this->db_crm->query($sql);


        $sql = "SELECT * FROM vtiger_salesorder90";
        $query = $this->db_crm->query($sql);

        $vissza = $query->rows;

        return $vissza;
	}




    public function getPotential($potentialid) {
        $sql = "SELECT * FROM vtiger_potential p WHERE potentialid='".$potentialid."'";
        $query = $this->db_crm->query($sql);
        if ($query->row) {
            $sql = "SELECT * FROM vtiger_potentialscf WHERE potentialid='".$potentialid."'";
            $query_cf = $this->db_crm->query($sql);
            $query->row['cf'] = $query_cf->row;

            $sql = "SELECT * FROM vtiger_crmentity WHERE crmid='".$potentialid."'";
            $query_entity = $this->db_crm->query($sql);
            $query->row['entity'] = $query_entity->row;
        }

        return $query->row;
    }

    public function getProduct($productid) {

        $sql = "SELECT * FROM vtiger_service WHERE serviceid='".$productid."'";
        $query = $this->db_crm->query($sql);
        return $query->row;

    }

    public function addPotential($data) {

        $siker = true;
        $this->load->model("cron/altalanos");

        $crm_id         = $this->model_cron_altalanos->getCrmId();
        $modtrack_id    = $this->model_cron_altalanos->getModtrackerId();
        $salesno        = $this->model_cron_altalanos->getCrmNo('SalesOrder');
        $address        = $this->getContact($data['contact_id']);

        $sql = "INSERT INTO vtiger_salesorder SET
                            salesorderid    = '".$crm_id."',
                            subject         = 'Lehetőségekből - teljes rendszer',
                            potentialid     = '".$data['potentialid']."',
                            customerno      = '',
                            salesorder_no   = '".$salesno."',
                            quoteid         = '0',
                            duedate         = CURDATE() + INTERVAL 4 DAY,
                            vendorterms     = '',
                            contactid       = '".$data['contact_id']."',
                            vendorid        = '',
                            carrier         = '',
                            pending         = '',
                            `type`          = '',
                            adjustment      = '',
                            salescommission = '',
                            exciseduty      = '',
                            taxtype         = 'group',
                            discount_percent= '0',
                            discount_amount = '0',
                            s_h_amount      = '0',
                            accountid       = '".$address['accountid']."',
                            terms_conditions= '',
                            purchaseorder   = '',
                            sostatus        = 'Díjbekérő',
                            currency_id     = '1',
                            conversion_rate = '1',
                            enable_recurring= '0',

                            total           = '".((int)$data['product']['unit_price']+round($data['product']['unit_price']*0.27))."',
                            subtotal        = '".((int)$data['product']['unit_price'])."',
                            pre_tax_total   = '".(round($data['product']['unit_price']*0.27))."',
                            s_h_percent     = '27'";

        $siker = $this->db_crm->query($sql);
        echo '<br>vtiger_salesorder: ' .$siker;

        $sql = "INSERT INTO vtiger_salesordercf SET
                            salesorderid    = '".$crm_id."',
                            cf_2435         = 'Továbbításra vár',
                            cf_2437         = 'Átutalás 4 nap'";
        $siker = $this->db_crm->query($sql);
        echo '<br>vtiger_salesordercf: ' .$siker;

        $sql = "INSERT INTO vtiger_crmentity SET
                            crmid           = '".$crm_id."',
	                        smcreatorid     = '".(!empty($data['entity']['smcreatorid']) ? $data['entity']['smcreatorid'] : 20)."',
                            smownerid       = '".(!empty($data['entity']['smownerid']) ? $data['entity']['smownerid'] : 20)."',
                            modifiedby      = '".(!empty($data['entity']['modifiedby']) ? $data['entity']['modifiedby'] : 20)."',
                            setype          = 'SalesOrder',
                            description	    = '',
                            createdtime	    = NOW(),
                            modifiedtime    = NOW(),
                            viewedtime      = '',
                            status          = '',
                            version         = '0',
                            presence        = '1',
                            deleted         = '0',
                            label           = 'Lehetőségekből - teljes rendszer',
                            cf_view         = '',
                            cf_viewname	    = ''";
        $siker = $this->db_crm->query($sql);
        echo '<br>vtiger_crmentity: ' .$siker;

        $sql = "INSERT INTO vtiger_modtracker_basic SET
                            id          = '".$modtrack_id."',
                            crmid       = '".$crm_id."',
                            `module`    = 'SalesOrder',
                            whodid      = '20',
                            changedon   = NOW(),
                            status      = '2'";
        $siker = $this->db_crm->query($sql);
        echo '<br>vtiger_modtracker_basic: ' .$siker;

        $sql = "INSERT INTO vtiger_inventoryshippingrel SET
                          id        = '".$crm_id."',
                          shtax1    = '',
                          shtax2    = '',
                          shtax3    = ''";
        $siker = $this->db_crm->query($sql);
        echo '<br>vtiger_inventoryshippingrel: ' .$siker;


        $sql = "INSERT INTO vtiger_sobillads SET
                          sobilladdressid   = '".$crm_id."',
                          bill_city         = '".(!empty($address['bill_address']) ? $address['bill_address']['bill_city'] : $address['contact_adress']['mailingcity'])."',
                          bill_code         = '".(!empty($address['bill_address']) ? $address['bill_address']['bill_code'] : $address['contact_adress']['mailingzip'])."',
                          bill_country      = '".(!empty($address['bill_address']) ? $address['bill_address']['bill_country'] : $address['contact_adress']['mailingcountry'])."',
                          bill_state        = '',
                          bill_street       = '".(!empty($address['bill_address']) ? $address['bill_address']['bill_street'] : $address['contact_adress']['mailingstreet'])."',
                          bill_pobox        = ''";
        $siker = $this->db_crm->query($sql);
        echo '<br>vtiger_sobillads: ' .$siker;


        $sql = "INSERT INTO vtiger_soshipads SET
                          soshipaddressid   = '".$crm_id."',
                          ship_city         = '".(!empty($address['shipp_address']) ? $address['shipp_address']['ship_city'] : '')."',
                          ship_code         = '".(!empty($address['shipp_address']) ? $address['shipp_address']['ship_code'] : '')."',
                          ship_country      = '".(!empty($address['shipp_address']) ? $address['shipp_address']['ship_country'] : '')."',
                          ship_state        = '',
                          ship_street       = '".(!empty($address['shipp_address']) ? $address['shipp_address']['ship_street'] : '')."',
                          ship_pobox        = ''";
        $siker = $this->db_crm->query($sql);
        echo '<br>vtiger_sobillads: ' .$siker;


        if (!empty($data['product'])) {
                $lineitem_id = $this->model_cron_altalanos->getLineItemId();
                $sql = "INSERT INTO vtiger_inventoryproductrel SET
                            id                = '".$crm_id."',
                            productid         = '".$data['product']['serviceid']."',
                            sequence_no       = '1',
                            quantity          = '1',
                            listprice         = '".$data['product']['unit_price']."',
                            discount_percent  = '',
                            discount_amount   = '',
                            comment           = '',
                            description	      = '',
                            incrementondel    = '0',
                            lineitem_id       = '".$lineitem_id."',
                            tax1              = '',
                            tax2              = '',
                            tax3              = '',
                            tax4              = '',
                            tax5              = '27'";
                $siker = $this->db_crm->query($sql);
                echo '<br>vtiger_inventoryproductrel: ' .$siker;
        }

        $sql = "INSERT INTO vtiger_salesorder_dijbekero SET
                            salesorderid    = '".$crm_id."',
                            sostatus        = 'Díjbekérő'";
        $siker = $this->db_crm->query($sql);
        echo '<br>vtiger_salesorder_dijbekero: ' .$siker;




        return true;
    }



    public function getContact($contact_id) {
        $sql = "SELECT * FROM vtiger_contactaddress WHERE contactaddressid='".$contact_id."'";
        $query = $this->db_crm->query($sql);
        $contact_adress = $query->row;

        $sql = "SELECT accountid FROM vtiger_contactdetails WHERE contactid='".$contact_id."'";
        $query = $this->db_crm->query($sql);
        $bill_address   = array();
        $shipp_address  = array();
        $accountid = 0;
        if (!empty($query->row['accountid'])) {
            $accountid = $query->row['accountid'];
            $sql = "SELECT * FROM vtiger_accountbillads WHERE accountaddressid='".$query->row['accountid']."'";
            $query_bill = $this->db_crm->query($sql);
            $bill_address = $query_bill->row;

            $sql = "SELECT * FROM vtiger_accountshipads WHERE accountaddressid='".$query->row['accountid']."'";
            $query_ship = $this->db_crm->query($sql);
            $shipp_address = $query_ship->row;
        }
        $vissza = array(
            'contact_adress'    => $contact_adress,
            'bill_address'      => $bill_address,
            'shipp_address'     => $shipp_address,
            'accountid'         => $accountid
        );

        return $vissza;
    }

    public function getProductPrice($cf,$afa) {

        $vissza = false;
        if ($cf['cf_2518']) {
            $vissza = $cf["cf_2516"];

        } elseif ($cf['cf_2524']) {
            $vissza = $cf["cf_2520"];

        } elseif ($cf['cf_2526']) {
            $vissza = $cf["cf_2522"];
        }
        if ($vissza) {
            $vissza = $vissza/(1+$afa/100);
        }

        return $vissza;

    }

    public function deleteTriggerPotential($data) {
        $sql = "DELETE FROM vtiger_salesorder90
            WHERE
                potentialid      = '".$data['potentialid']."'";
        $siker = $this->db_crm->query($sql);
        if ($siker) {
            $this->model_cron_altalanos->addFuttatasEredmeny('A '.$data['potentialid'].' ID bejegyzés a vtiger_salesorder90 táblából törölve!');
        } else {
            $this->model_cron_altalanos->addFuttatasEredmeny('A '.$data['potentialid'].' ID bejegyzést a vtiger_salesorder90 táblából nem sikerült törölni!');

        }

    }



}
?>