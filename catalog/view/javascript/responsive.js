/* Responsive-hoz tartozik */
/* Start */
$(document).ready(function(){
    var kepernyoszeles = $(window).width();
    var kosar = $("#header_kosar");
    if($("#menu").css("display") != "none"){
        $("#responsive_kiemeltmenusor").css("display","none");
    }
    $(window).resize(function(){
        if($("#menu").css("display") != "none"){
            $("#responsive_kiemeltmenusor").css("display","none");
        }
    });
    $("#responsive_cart").bind('click', function() {
        var becsukni = "#cart .content";
        var display = "block";
        mindentbecsuk(becsukni,display);
    });
    $("#information").bind('click', function() {
        if($(window).width() >= 626){
            var becsukni = "#responsive_informations_small";
        }
        else{
            var becsukni = "#responsive_informations";
        }
        var display = "block";
        mindentbecsuk(becsukni,display);
    });
    $("#menuheader").click(function(){
        responsiveToggle($("#menu_items"), 'block');
    });
  /*  $('.box .elso-filter').bind('click', function() {


    /*$('.box .elso-filter').bind('click', function() {
        var contentObj = $(this).parents('.box').find('.box-content');
        if(contentObj != undefined && contentObj[0] != undefined) {
            var status = responsiveToggle(contentObj, 'block');
            $(this).find('img')[0].setAttribute('divstatus', status);
        }
    });
    $('.box .masodik-filter').bind('click', function() {
        var contentObj = $(this).parents('.box').find('.box-content');
        if(contentObj != undefined && contentObj[0] != undefined) {
            var status = responsiveToggle(contentObj, 'block');
            $(this).find('img')[0].setAttribute('divstatus', status);
        }
    });*/
    /*$('.box .harmadik-filter').bind('click', function() {
        var fparent = $(this).parents('.box');
        var contentObj = fparent.find('.box-content');
        var contentObj2 = fparent.nextAll().find('.box-content');
        if(contentObj != undefined && contentObj[0] != undefined) {
            var status = responsiveToggle(contentObj, 'block');
            var status = responsiveToggle(contentObj2, 'block');
            $(this).find('img')[0].setAttribute('divstatus', status);
        }
    });



    });*/
    $('#header_kategoriagomb').bind('click', function() {
        var becsukni = "#responsive_kiemeltmenusor";
        var display = "block";
        mindentbecsuk(becsukni,display);
    });
    $('#header_menugomb').bind('click', function() {
        var becsukni = ".multicategory";
        var display = "block";
        mindentbecsuk(becsukni,display);
    });
    $('#smallest_header_kategoriagomb').bind('click', function() {

        if($('.smallest_responsive_menu').css('display') != 'none'){
            $('.smallest_responsive_menu').css('display','none');
        }
        else{
            $('.smallest_responsive_menu').css('display','block');
        }
    });
    $("#information_smallest").bind('click', function() {
        var becsukni = "#responsive_informations";
        var display = "block";
        mindentbecsuk(becsukni,display);
    });
    $('#header_menugomb_smallest').bind('click', function() {
        var becsukni = ".multicategory";
        var display = "block";
        mindentbecsuk(becsukni,display);
    });
    $('#responsive_cart_smallest').bind('click', function() {
        var becsukni = "#cart .content";
        var display = "block";
        mindentbecsuk(becsukni,display);
    });
    var szam = 0;
    if(szam == 0){
        if($("#registry").length > 0){
            $(".responsive_icons").append("<style>@media screen and (max-width:639px) {#header .responsive_small{width: calc(12.5% - 1px) !important;width: -moz-calc(12.5% - 1px) !important;width: -webkit-calc(12.5% - 1px) !important;}}@media screen and (max-width: 1236px) and (min-width: 640px) { #header .responsive_small {width: calc(16.6% - 1px);width: -moz-calc(16.6% - 1px);width: -webkit-calc(16.6% - 1px);}}</style>");
        }
        else{
            $(".responsive_icons").append("<style>@media screen and (max-width:639px) {#header .responsive_small{width: calc(14.5% - 1px) !important;width: -moz-calc(14.5% - 1px) !important;width: -webkit-calc(14.5% - 1px) !important;}}@media screen and (max-width: 1236px) and (min-width: 640px) { #header .responsive_small {width: -moz-calc(20% - 1px);width: -webkit-calc(20% - 1px);width: calc(20% - 1px);}}</style>");
        }
    }
    szam++;
    $(window).resize(function(){
        if(szam == 0){
            if($("#registry").length > 0){
                $(".responsive_icons").append("<style>@media screen and (max-width:639px) {#header .responsive_small{width: calc(12.5% - 1px) !important;width: -moz-calc(12.5% - 1px) !important;width: -webkit-calc(12.5% - 1px) !important;}}@media screen and (max-width: 1236px) and (min-width: 640px) { #header .responsive_small {width: calc(16.6% - 1px);width: -moz-calc(16.6% - 1px);width: -webkit-calc(16.6% - 1px);}}</style>");
            }
            else{
                $(".responsive_icons").append("<style>@media screen and (max-width:639px) {#header .responsive_small{width: calc(14.5% - 1px) !important;width: -moz-calc(14.5% - 1px) !important;width: -webkit-calc(14.5% - 1px) !important;}}@media screen and (max-width: 1236px) and (min-width: 640px) { #header .responsive_small {width: -moz-calc(20% - 1px);width: -webkit-calc(20% - 1px);width: calc(20% - 1px);}}</style>");
            }
        }
        szam++;
    });
    if($(window).width() <= 798){
        $(".multicategory .togglebutton img").attr("src", "catalog/view/theme/kupon/image/responsive/pos_r_opened.png");
    }
    $(window).resize(function(){
        if($(window).width() <= 798){
            $(".multicategory .togglebutton img").attr("src", "catalog/view/theme/kupon/image/responsive/pos_r_opened.png");
        }
    });
});
/* Vége */