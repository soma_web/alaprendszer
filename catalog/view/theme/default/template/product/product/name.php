<?php
$_['model']          = array(
    'type'              => 'checkbox',
    'name'              => 'model',
    'default'           => "1",
    'heading'           => 'Modell megjelenítés'
);

$_['manufacturer']          = array(
    'type'              => 'checkbox',
    'name'              => 'manufacturer',
    'default'           => "1",
    'heading'           => 'Gyártó megjelenítés'
);

$_['cikkszam']          = array(
    'type'              => 'checkbox',
    'name'              => 'cikkszam',
    'default'           => "1",
    'heading'           => 'Cikkszám megjelenítés'
);

?>