<?php
class ControllerModuleLastSeen extends Controller {

        public function index() {

            $this->language->load('module/lastseen',$this->config->get('config_template'));

            if (!isset($setting['limit'])) {
            $setting['limit'] = 20;
        }

            $this->data['breadcrumbs'] = array();
            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_home'),
                'href'      => $this->url->link('common/home'),
                'separator' => false
            );

            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('heading_title'),
                'href'      => $this->url->link('module/lastseen'),
                'separator' => $this->language->get('text_separator')
            );


      	$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
		$this->data['kaphato']          = $this->language->get('kaphato');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
		$this->data['button_cart']      = $this->language->get('button_cart');
		$this->data['button_set_valos_ar']      = $this->language->get('button_set_valos_ar');
        $this->data['button_bovebben']  = $this->language->get('button_bovebben');
        $this->data['text_tax']         = $this->language->get('text_tax');
        $this->data['text_netto']       = $this->language->get('text_netto');
        $this->data['module_name']      = "lastseen";
        $this->data['button_wishlist']  = $this->language->get('button_wishlist');
        $this->data['button_compare']   = $this->language->get('button_compare');
        $this->data['heading_title']    = $this->language->get('heading_title');

        $this->data['text_akcio_title']         = $this->language->get('text_akcio_title');
        $this->data['text_pardarab_title']      = $this->language->get('text_pardarab_title');
        $this->data['text_uj_title']            = $this->language->get('text_uj_title');

        if ($this->config->get('lastseen_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

        if ($this->config->get('megjelenit_korhinta') == 1) {
            if (isset($setting['show']) ) {
                $this->data['show'] = $setting['show'];
            } else {
                $this->data['show'] = 0;
            }
        } else {
            $this->data['show'] = 0;
        }

        $this->load->model('catalog/product');
		
		$this->load->model('tool/image');

		$this->data['products'] = array();

		$results = $this->model_catalog_product->getLastSeenProducts();

        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );

        $results1 = $this->model_catalog_product->getProductsNew($data1);

        if ($results) {
            foreach ($results as $result) {
                $result = $this->model_catalog_product->getProduct($result);

                $this->data['products'][] = $this->product->productPreparation($result,$setting,$results1);




            }
        }



            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/lastseen.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/module/lastseen.tpl';
            } else {
                $this->template = 'default/template/module/lastseen.tpl';
            }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
	}
}
?>