<?php

require_once(DIR_CATALOG . 'controller/common/seo_url.php');

class ControllerCatalogQA extends Controller {
    protected $error = array();
    protected $alert = array(
        'error'     => array(),
        'warning'   => array(),
        'success'   => array(),
        'info'      => array()
    );

    private $columns = array(
        'selector'              => array('display' => 1, 'index' =>  0, 'align' => 'text-center', 'sort' => '',                         'class'=>          '', ),
        'id'                    => array('display' => 0, 'index' =>  1, 'align' =>   'text-left', 'sort' => 'q.qa_id',                  'class'=>          '', ),
        'product'               => array('display' => 1, 'index' =>  5, 'align' =>   'text-left', 'sort' => 'pd.name',                  'class'=>          '', ),
        'question_author_name'  => array('display' => 1, 'index' => 10, 'align' =>   'text-left', 'sort' => 'question_author_name',     'class'=>'visible-sm visible-md visible-lg', ),
        'question_author_email' => array('display' => 0, 'index' => 10, 'align' =>   'text-left', 'sort' => 'question_author_name',     'class'=>'visible-sm visible-md visible-lg', ),
        'question_author_phone' => array('display' => 0, 'index' => 10, 'align' =>   'text-left', 'sort' => 'question_author_name',     'class'=>'visible-sm visible-md visible-lg', ),
        'question_author_custom'=> array('display' => 0, 'index' => 10, 'align' =>   'text-left', 'sort' => 'question_author_name',     'class'=>'visible-sm visible-md visible-lg', ),
        'question'              => array('display' => 1, 'index' => 15, 'align' =>   'text-left', 'sort' => '',                         'class'=>           '', ),
        'answer'                => array('display' => 1, 'index' => 20, 'align' =>   'text-left', 'sort' => '',                         'class'=>'visible-md visible-lg', ),
        'answer_author_name'    => array('display' => 1, 'index' => 25, 'align' =>   'text-left', 'sort' => 'answer_author_name',       'class'=>'visible-xl', ),
        'language'              => array('display' => 0, 'index' => 30, 'align' =>   'text-left', 'sort' => 'l.name',                   'class'=>          '', ),
        'date_asked'            => array('display' => 1, 'index' => 35, 'align' =>   'text-left', 'sort' => 'date_asked',               'class'=>'visible-lg', ),
        'date_answered'         => array('display' => 1, 'index' => 40, 'align' =>   'text-left', 'sort' => 'date_answered',            'class'=>'visible-xl', ),
        'date_modified'         => array('display' => 0, 'index' => 45, 'align' =>   'text-left', 'sort' => 'date_modified',            'class'=>          '', ),
        'store'                 => array('display' => 0, 'index' => 50, 'align' =>   'text-left', 'sort' => '',                         'class'=>'visible-md visible-lg', ),
        'status'                => array('display' => 1, 'index' => 55, 'align' => 'text-center', 'sort' => 'status',                   'class'=>          '', ),
        'action'                => array('display' => 1, 'index' => 60, 'align' =>  'text-right', 'sort' => '',                         'class'=>          '', ),
    );

    public function __construct($registry) {
        parent::__construct($registry);
        $this->load->helper('qa');

        $this->language->load('catalog/qa');
        $this->load->model('catalog/qa');
    }

    public function index() {
        $this->getList();
    }

    public function delete() {
        $this->action('delete');
    }

    public function copy() {
        $this->action('copy');
    }

    public function insert() {
        $this->action('insert');
    }

    public function update() {
        $this->action('update');
    }

    public function autocomplete() {
        if ($this->request->server['REQUEST_METHOD'] == 'GET' && isset($this->request->get['type'])) {
            $resp = array();
            switch ($this->request->get['type']) {
                case 'product':
                    $this->load->model('catalog/product');

                    $results = array();

                    if (isset($this->request->get['query'])) {
                        $data = array(
                            'filter_name'   => $this->request->get['query'],
                            'sort'          => 'pd.name',
                            'start'         => 0,
                            'limit'         => 20,
                        );

                        $results = $this->model_catalog_product->getProducts($data);
                    }

                    foreach ($results as $result) {
                        $result['name'] = html_entity_decode($result['name']);
                        $resp[] = array(
                            'value'     => $result['name'],
                            'tokens'    => explode(' ', $result['name']),
                            'id'        => $result['product_id'],
                            'model'     => $result['model']
                        );
                    }
                    break;
                case 'customer':
                    $this->load->model('sale/customer');

                    $results = array();

                    if (isset($this->request->get['query'])) {
                        $data = array(
                            'filter_name'   => $this->request->get['query'],
                            'sort'          => 'name',
                            'start'         => 0,
                            'limit'         => 20,
                        );

                        $results = $this->model_sale_customer->getCustomers($data);
                    }

                    foreach ($results as $result) {
                        $result['name'] = html_entity_decode($result['name']);
                        $resp[] = array(
                            'value'     => $result['name'],
                            'tokens'    => explode(' ', $result['name']),
                            'id'        => $result['customer_id'],
                            'email'     => $result['email']
                        );
                    }
                    break;
                default:
                    break;
            }
        }

        $this->response->setOutput(json_enc($resp, JSON_UNESCAPED_SLASHES));
    }

    private function action($action) {
        $ajax_request = isset($this->request->server['HTTP_X_REQUESTED_WITH']) && !empty($this->request->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

        $response = array('success' => false);

        switch ($action) {
            case 'copy':
            case 'delete':
                if (isset($this->request->post['selected']) && $this->validateAction($action)) {
                    foreach ((array)$this->request->post['selected'] as $qa_id) {
                        switch ($action) {
                            case 'copy':
                                $this->model_catalog_qa->copyQuestion($qa_id);
                                break;
                            case 'delete':
                                $this->model_catalog_qa->deleteQuestion($qa_id);
                                break;
                        }
                    }

                    if ($ajax_request) {
                        $response['success'] = true;
                        $this->session->data['success'] = sprintf($this->language->get('text_success_' . $action), count((array)$this->request->post['selected']));
                        $response['msg'] = sprintf($this->language->get('text_success_' . $action), count((array)$this->request->post['selected']));
                    } else {
                        $this->session->data['success'] = sprintf($this->language->get('text_success_' . $action), count((array)$this->request->post['selected']));
                    }
                } else {
                    if ($ajax_request) {
                        $response = array_merge(array("error" => true), array("errors" => $this->error), array("alerts" => $this->alert));
                    } else {
                        $this->session->data['errors'] = $this->error;
                        $this->session->data['alerts'] = $this->alert;
                    }
                }

                if ($ajax_request) {
                    $this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
                    return;
                } else {
                    $url = $this->urlParams();

                    $this->redirect($this->url->link('catalog/qa', $url, 'SSL'));
                }
                break;
            case 'insert':
            case 'update':
                if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($this->request->post)) {
                    $question_id = isset($this->request->get['qa_id']) ? $this->request->get['qa_id'] : '';
                    switch ($action) {
                        case 'insert':
                            $this->request->post['store_id'] = ((!isset($this->request->post['store_id']) || $this->request->post['store_id'] == '') && isset($this->request->post['question_stores'][0])) ? $this->request->post['question_stores'][0] : 0;

                            $question_id = $this->model_catalog_qa->addQuestion($this->request->post);
                            break;
                        case 'update':
                            if ($question_id) {
                                $this->model_catalog_qa->editQuestion($question_id, $this->request->post);
                            }
                            break;
                    }

                    $this->notify($question_id, $this->request->post);

                    if ($ajax_request) {
                        $response['success'] = true;

                        if ($action == 'insert') {
                            $response['reload'] = true;
                            $this->session->data['success'] = $this->language->get('text_success_' . $action);
                        }

                        $response['msg'] = $this->language->get('text_success_' . $action);

                        $response = array_merge($response, array("errors" => $this->error), array("alerts" => $this->alert));

                        $this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
                        return;
                    } else {
                        $this->session->data['success'] = $this->language->get('text_success_' . $action);
                        $this->session->data['errors'] = $this->error;
                        $this->session->data['alerts'] = $this->alert;

                        $url = $this->urlParams();

                        $this->redirect($this->url->link('catalog/qa', $url, 'SSL'));
                    }
                }

                if ($ajax_request) {
                    $response = array_merge(array("error" => true), array("errors" => $this->error), array("alerts" => $this->alert));

                    $this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
                    return;
                } else {
                    $this->getForm();
                }
                break;
        }
    }

    protected function notify($question_id, $data) {
        if (isset($data['notify_customer']) && (int)$data['notify_customer'] && $data['question'] != "" && $data['answer'] != "" && $data['question_author_email'] != "") {
            $l_query = $this->db->query("SELECT language_id, filename, directory FROM " . DB_PREFIX . "language WHERE language_id = '" . $data['language_id'] . "'");
            $language = new Language($l_query->row['directory'], $this->config->get('config_template'));
            $language->load($l_query->row['filename']);
            $language->load('mail/question_reply');

            // Get product info
            $p_query = $this->db->query("SELECT p.model AS model, pd.name AS name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$data['product_id'] . "' AND pd.language_id = '" . $l_query->row['language_id'] . "'");

            $config = new Config();

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$data['store_id'] . "'");

            foreach ($query->rows as $setting) {
                if (!$setting['serialized']) {
                    $config->set($setting['key'], $setting['value']);
                } else {
                    $config->set($setting['key'], unserialize($setting['value']));
                }
            }

            if ((int)$data['store_id'] == 0) {
                $config->set('config_url', HTTP_CATALOG);
                $config->set('config_ssl', HTTPS_CATALOG);
            }

            $url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));
            $product_link = $url->link('product/product', 'product_id=' . $data['product_id']);
            if ($this->config->get('config_seo_url')) {
                $seo_url = new ControllerCommonSeoUrl($this->registry);
                $product_link = $seo_url->rewrite($product_link);
            }

            $subject = sprintf($language->get('text_subject'), $config->get('config_name'));

            // HTML Mail
            $template = new Template();

            $template->data['title'] = sprintf($language->get('text_subject'), html_entity_decode($config->get('config_name'), ENT_QUOTES, 'UTF-8'));

            $template->data['text_answered'] = sprintf($language->get('text_answered'), $product_link, $p_query->row['name']);
            $template->data['text_view'] = ((int)$data['status']) ? sprintf($language->get('text_view'), $product_link, $product_link) : '';
            $template->data['text_question_detail'] = $language->get('text_question_detail');
            $template->data['text_answer'] = $language->get('text_answer');
            $template->data['text_asked'] = sprintf($language->get('text_asked'), date($language->get('date_format_short'), strtotime($data['date_asked'])));
            //$template->data['text_powered_by'] = $language->get('text_powered_by');
            $template->data['text_closing'] = $language->get('text_closing');
            $template->data['text_sender'] = sprintf($language->get('text_sender'), $config->get('config_name'));

            $template->data['store_name'] = $config->get('config_name');
            $template->data['store_url'] = $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url');;
            $template->data['logo'] = $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url') . 'image/' . $config->get('config_logo');
            $template->data['question'] = str_replace(array("\r\n", "\r", "\n"), '<br />', strip_tags($data['question']));
            $template->data['answer'] = str_replace(array("\r\n", "\r", "\n"), '<br />', html_entity_decode($data['answer']));

            $html = $template->fetch('mail/question_reply.tpl');

            // Text Mail
            $text  = sprintf(strip_tags($language->get('text_answered')), $p_query->row['name']) . "\n";
            if ((int)$data['status']) {
                $text .= sprintf(strip_tags($language->get('text_view')), $product_link) . "\n\n";
            }
            $text .= sprintf($language->get('text_asked'), date($language->get('date_format_short'), strtotime($data['date_asked']))) . "\n";
            $text .= $data['question'] . "\n\n";
            $text .= $language->get('text_answer') . "\n" . strip_tags($data['answer']) . "\n\n";
            $text .= $language->get('text_closing') . "\n";
            $text .= sprintf($language->get('text_sender'), $config->get('config_name')) . "\n";

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->hostname = $this->config->get('config_smtp_host');
            $mail->username = $this->config->get('config_smtp_username');
            $mail->password = $this->config->get('config_smtp_password');
            $mail->port = $this->config->get('config_smtp_port');
            $mail->timeout = $this->config->get('config_smtp_timeout');
            $mail->setFrom($config->get('config_email'));
            $mail->setSender($config->get('config_name'));
            $mail->setSubject($subject);
            $mail->setHtml($html);
            $mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));

            $mail->setTo($data['question_author_email']);
            $mail->send();

            $this->model_catalog_qa->updateNotificationSent($question_id, 1);
        }
    }

    protected function getList() {
        if (isset($this->session->data['errors'])) {
            $this->error = array_merge($this->error, (array)$this->session->data['errors']);

            unset($this->session->data['errors']);
        }

        if (isset($this->session->data['alerts'])) {
            $this->alert = array_merge($this->alert, (array)$this->session->data['alerts']);

            unset($this->session->data['alerts']);
        }

        $this->document->addStyle('view/stylesheet/qa/css/custom.min.css');

        $this->document->addScript('view/javascript/qa/custom.min.js');
        $this->document->addScript('view/javascript/jquery/superfish/js/superfish.js');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_search'] = $this->language->get('text_search');
        $this->data['text_edit'] = $this->language->get('text_edit');
        $this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_toggle_navigation'] = $this->language->get('text_toggle_navigation');
        $this->data['text_confirm_delete'] = $this->language->get('text_confirm_delete');
        $this->data['text_are_you_sure'] = $this->language->get('text_are_you_sure');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_filter'] = $this->language->get('text_filter');
        $this->data['text_clear_filter'] = $this->language->get('text_clear_filter');
        $this->data['text_autocomplete'] = $this->language->get('text_autocomplete');
        $this->data['text_anonymous'] = $this->language->get('text_anonymous');
        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_all_languages'] = $this->language->get('text_all_languages');
        $this->data['text_display_language'] = $this->language->get('text_display_language');
        $this->data['error_ajax_request'] = $this->language->get('error_ajax_request');

        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_copy'] = $this->language->get('button_copy');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['button_save'] = $this->language->get('button_save');

        $url = $this->urlParams();

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'active'    => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/qa', 'token=' . $this->session->data['token'], 'SSL'),
            'active'    => true
        );

        $this->data['insert'] = $this->url->link('catalog/qa/insert', $url, 'SSL');
        $this->data['copy'] = $this->url->link('catalog/qa/copy', $url, 'SSL');
        $this->data['delete'] = $this->url->link('catalog/qa/delete', $url, 'SSL');
        $this->data['filter'] = html_entity_decode($this->url->link('catalog/qa', 'token=' . $this->session->data['token'], 'SSL'), ENT_QUOTES, 'UTF-8');
        $this->data['autocomplete'] = html_entity_decode($this->url->link('catalog/qa/autocomplete', 'token=' . $this->session->data['token'], 'SSL'), ENT_QUOTES, 'UTF-8');
        $this->data['customer'] = $this->url->link('sale/customer', 'token=' . $this->session->data['token'] . '&filter_name=', 'SSL');
        $this->data['product'] = $this->url->link('catalog/product/update', 'token=' . $this->session->data['token'] . '&product_id=', 'SSL');

        $this->load->model('setting/store');

        $multistore = $this->model_setting_store->getTotalStores();
        $this->data['multistore'] = $multistore;

        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();
        $multilingual = count($languages);
        $this->data['languages'] = $languages;
        $this->data['multilingual'] = $multilingual;

        $columns = $this->columns;
        $filters = array();

        foreach ($columns as $column => $attr) {
            $columns[$column]['name'] = $this->language->get('column_' . $column);

            if (isset($this->request->get['filter_' . $column])) {
                $filters[$column] = $this->request->get['filter_' . $column];
            }

            if ($column == 'store' && !$multistore) {
                unset($columns[$column]);
            }
        }

        uasort($columns, 'column_sort');

        if ($multistore) {
            $columns['store']['display'] = 1;
        }

        $columns = array_filter($columns, 'column_display');

        $displayed_columns = array_keys($columns);

        $this->data['columns'] = $columns;
        $this->data['typeahead'] = array();

        foreach (array('product') as $column) {
            if (in_array($column, $displayed_columns)) {
                $url = $this->urlParams(0, 0, 0, 0, 0);
                $this->data['typeahead'][$column] = array(
                    'remote' => html_entity_decode($this->url->link('catalog/qa/autocomplete', 'type=' . $column . '&query=%QUERY' . $url, 'SSL'))
                );
            }
        }

        if (in_array('store', $displayed_columns)) {
            $stores = $this->cache->get('store.all');

            if (is_null($stores)) {
                $_stores = $this->model_setting_store->getStores(array());

                $stores = array(
                    '0' => array(
                        'store_id'  => 0,
                        'name'      => $this->config->get('config_name'),
                        'url'       => HTTP_CATALOG
                    )
                );

                foreach ($_stores as $store) {
                    $stores[$store['store_id']] = array(
                        'store_id'  => $store['store_id'],
                        'name'      => $store['name'],
                        'url'       => $store['url']
                    );
                }

                $this->cache->set('store.all', $stores);
            }

            $this->data['stores'] = $stores;
        }

        if (isset($this->request->get['search'])) {
            $search = html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8');
        } else {
            $search = '';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'date_asked';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $this->data['questions'] = array();

        $data = array(
            'columns'   => $displayed_columns,
            'search'    => $search,
            'filter'    => $filters,
            'sort'      => $sort,
            'order'     => $order,
            'start'     => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'     => $this->config->get('config_admin_limit')
        );

        $results = $this->model_catalog_qa->getQuestions($data);

        $filtered_total = $this->model_catalog_qa->getFilteredTotalQuestions();
        $total = $this->model_catalog_qa->getTotalQuestions();

        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'name'      => 'edit',
                'title'     => $this->language->get('text_edit'),
                'text'      => $this->language->get('text_edit'),
                'icon'      => 'pencil',
                'url'       => $this->url->link('catalog/qa/update', 'qa_id=' . $result['qa_id'] . $this->urlParams(), 'SSL')
            );

            $question = array(
                'qa_id'                 => $result['qa_id'],
                'question_author_email' => $result['question_author_email'],
                'question_author_phone' => $result['question_author_phone'],
                'question_author_custom'=> $result['question_author_custom'],
                'customer_id'           => $result['customer_id'],
                'language_id'           => $result['language_id'],
                'selected'              => isset($this->request->post['selected']) && in_array($result['qa_id'], $this->request->post['selected']),
            );

            foreach ($displayed_columns as $column) {
                switch ($column) {
                    case 'action':
                        $value = $action;
                        break;
                    case 'product':
                        $value = $result['product_name'];
                        $question['product_id'] = $result['product_id'];
                        break;
                    case 'date_asked':
                    case 'date_answered':
                        if ($result[$column] != '0000-00-00 00:00:00') {
                            $date = new DateTime($result[$column]);
                            $value = $date->format('Y-m-d');
                        } else {
                            $value = '';
                        }
                        break;
                    case 'date_modified':
                        if ($result[$column] != '0000-00-00 00:00:00') {
                            $date = new DateTime($result[$column]);
                            $value = $date->format('Y-m-d H:i:s');
                        } else {
                            $value = '';
                        }
                        break;
                    case 'store':
                        $value = $result['stores'];
                        break;
                    case 'language':
                        $value = $result['language_name'];
                        break;
                    case 'question':
                    case 'answer':
                        $value = (utf8_strlen(strip_tags(html_entity_decode($result[$column]))) > 50) ? mb_substr(strip_tags(html_entity_decode($result[$column])), 0, 50) . ' ...' : strip_tags(html_entity_decode($result[$column]));
                        $question[$column . '_full'] = str_replace('"', '&quot;', html_entity_decode($result[$column]));
                        break;
                    case 'question_author_name':
                        $value = $result[$column];
                        $details = '';
                        if ($result['question_author_email']) {
                            $details .= "<tr><td>" . $this->language->get('entry_email') . "</td><td>" . htmlentities($result['question_author_email']) . "</td></tr>";
                        }
                        if ($result['question_author_phone']) {
                            $details .= "<tr><td>" . $this->language->get('entry_phone') . "</td><td>" . htmlentities($result['question_author_phone']) . "</td></tr>";
                        }
                        if ($result['question_author_custom']) {
                            $field_names = $this->config->get('qa_form_custom_field_name');
                            $field_name = isset($field_names[$this->config->get('config_language_id')]) ? $field_names[$this->config->get('config_language_id')] : $this->language->get('entry_custom');
                            $details .= "<tr><td>" . $field_name  . "</td><td>" . htmlentities($result['question_author_custom']) . "</td></tr>";
                        }
                        if ($details) {
                            $details = "<table class='table-condensed details'>" . $details . '</table>';
                        }
                        $question['author_details'] = $details;
                        $question['customer_name'] = $result['customer_name'];
                        break;
                    case 'status':
                        $value = $result['status_text'];
                        $question['status_class'] = (int)$result['status'] ? 'success' : 'danger';
                        break;
                    default:
                        $value = isset($result[$column]) ? $result[$column] : '';
                        break;
                }

                $question[$column] = $value;
            }

            $this->data['questions'][] = $question;
        }

        if (isset($this->error['warning'])) {
            $this->alert['warning']['warning'] = $this->error['warning'];
        }

        if (isset($this->error['error'])) {
            $this->alert['error']['error'] = $this->error['error'];
        }

        if (isset($this->session->data['success'])) {
            $this->alert['success']['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        }

        $this->data['token'] = $this->session->data['token'];

        $this->data['alerts'] = $this->alert;

        $this->data['sorts'] = array();

        foreach ($columns as $column => $attr) {
            if ($attr['sort']) {
                $this->data['sorts'][$column] = $this->url->link('catalog/qa', $this->urlParams(1, 1, $attr['sort'], $order == 'ASC' ? 'DESC' : 'ASC', '1'), 'SSL');
            } else {
                $this->data['sorts'][$column] = null;
            }
        }

        $pagination = new Pagination();
        $pagination->total = $filtered_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = ($total != $filtered_total) ? $this->language->get('text_pagination') . ' ' . sprintf($this->language->get('text_filtered_from'), $total) : $this->language->get('text_pagination');
        $pagination->text_first = '&larr; ' . $this->language->get('text_first_page');
        $pagination->text_last = $this->language->get('text_last_page') . ' &rarr;';
        $pagination->text_next = $this->language->get('text_next_page');
        $pagination->text_prev = $this->language->get('text_previous_page');
        $pagination->style_results = 'text-muted';
        $pagination->style_links = 'qa pagination pull-right';
        $pagination->url = $this->url->link('catalog/qa', $this->urlParams(1, 1, 1, 1, '{page}'), 'SSL');

        $this->data['pagination'] = $pagination->render_bs();

        $this->data['search'] = $search;
        $this->data['filters'] = $filters;
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->data['page'] = $page;

        $this->template = 'catalog/qa_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function getForm() {
        $qa_id = isset($this->request->get['qa_id']) ? $this->request->get['qa_id'] : null;

        if (isset($this->session->data['errors'])) {
            $this->error = array_merge($this->error, (array)$this->session->data['errors']);

            unset($this->session->data['errors']);
        }

        if (isset($this->session->data['alerts'])) {
            $this->alert = array_merge($this->alert, (array)$this->session->data['alerts']);

            unset($this->session->data['alerts']);
        }

        $this->document->addStyle('view/stylesheet/qa/css/custom.min.css');

        $this->document->addScript('view/javascript/qa/custom.min.js');
        $this->document->addScript('view/javascript/jquery/superfish/js/superfish.js');
        $this->document->addScript('view/javascript/ckeditor/ckeditor.js');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_toggle_navigation'] = $this->language->get('text_toggle_navigation');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_question'] = $this->language->get('text_question');
        $this->data['text_answer'] = $this->language->get('text_answer');
        $this->data['text_autocomplete'] = $this->language->get('text_autocomplete');

        $this->data['help_notify'] = $this->language->get('help_notify');

        $this->data['entry_id'] = $this->language->get('entry_id');
        $this->data['entry_product'] = $this->language->get('entry_product');
        $this->data['entry_language'] = $this->language->get('entry_language');
        $this->data['entry_author_name'] = $this->language->get('entry_author_name');
        $this->data['entry_answer_name'] = $this->language->get('entry_answer_name');
        $this->data['entry_customer'] = $this->language->get('entry_customer');
        $this->data['entry_email'] = $this->language->get('entry_email');
        $this->data['entry_phone'] = $this->language->get('entry_phone');
        $this->data['entry_custom'] = $this->language->get('entry_custom');
        $this->data['entry_question'] = $this->language->get('entry_question');
        $this->data['entry_answer'] = $this->language->get('entry_answer');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_date_asked'] = $this->language->get('entry_date_asked');
        $this->data['entry_date_answered'] = $this->language->get('entry_date_answered');
        $this->data['entry_date_modified'] = $this->language->get('entry_date_modified');
        $this->data['entry_stores'] = $this->language->get('entry_stores');
        $this->data['entry_notify'] = $this->language->get('entry_notify');
        $this->data['entry_update_date_answered'] = $this->language->get('entry_update_date_answered');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_apply'] = $this->language->get('button_apply');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['error_ajax_request'] = $this->language->get('error_ajax_request');

        $url = $this->urlParams();

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'active'    => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/qa', $url, 'SSL'),
            'active'    => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $qa_id ? $this->language->get('text_update') : $this->language->get('text_insert'),
            'href'      => $this->url->link('catalog/qa/' . ($qa_id ? 'update' : 'insert'), ($qa_id ? 'qa_id=' . $qa_id : '') . $url, 'SSL'),
            'active'    => true
        );

        $this->data['save'] = $this->url->link('catalog/qa/' . ($qa_id ? 'update' : 'insert'), ($qa_id ? 'qa_id=' . $qa_id : '') . $url, 'SSL');
        $this->data['cancel'] = $this->url->link('catalog/qa', $url, 'SSL');

        $this->data['customer_link'] = $this->url->link('sale/customer', 'token=' . $this->session->data['token'] . '&filter_name=', 'SSL');

        $this->data['typeahead'] = array();

        foreach (array('product', 'customer') as $type) {
            $url = $this->urlParams(0, 0, 0, 0, 0);
            $this->data['typeahead'][$type] = array(
                'remote' => html_entity_decode($this->url->link('catalog/qa/autocomplete', 'type=' . $type . '&query=%QUERY' . $url, 'SSL'))
            );
        }

        $this->load->model('localisation/language');

        $this->data['languages'] = $this->model_localisation_language->getLanguages();
        $this->data['languages'] = arrayRemapKeysToIds('language_id', $this->data['languages']);

        $stores = $this->cache->get('store.all');

        if (is_null($stores)) {
            $this->load->model('setting/store');

            $_stores = $this->model_setting_store->getStores(array());

            $stores = array(
                '0' => array(
                    'store_id'  => 0,
                    'name'      => $this->config->get('config_name'),
                    'url'       => HTTP_CATALOG
                )
            );

            foreach ($_stores as $store) {
                $stores[$store['store_id']] = array(
                    'store_id'  => $store['store_id'],
                    'name'      => $store['name'],
                    'url'       => $store['url']
                );
            }

            $this->cache->set('store.all', $stores);
        }

        $this->data['stores'] = $stores;
        $this->data['multistore'] = count($stores) > 1;

        if (isset($this->session->data['error'])) {
            $this->error = $this->session->data['error'];

            unset($this->session->data['error']);
        }

        if (isset($this->error['warning'])) {
            $this->alert['warning']['warning'] = $this->error['warning'];
        }

        if (isset($this->error['error'])) {
            $this->alert['error']['error'] = $this->error['error'];
        }

        if (isset($this->session->data['success'])) {
            $this->alert['success']['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        }

        if ($qa_id && $this->request->server['REQUEST_METHOD'] != 'POST') {
            $qa_info = $this->model_catalog_qa->getQuestion($qa_id);
        }

        $form = array(
            'qa_id'                 => '',
            'product_id'            => '',
            'customer_id'           => '',
            'customer_name'         => '',
            'language_id'           => '',
            'store_id'              => '',
            'product_name'          => '',
            'question_author_name'  => '',
            'question_author_phone' => '',
            'question_author_email' => '',
            'question_author_custom'=> '',
            'question'              => '',
            'answer_author_name'    => '',
            'answer'                => '',
            'status'                => '0',
            'notified'              => '0',
            'date_asked'            => '',
            'date_answered'         => '',
            'date_modified'         => '',
            'update_date_answered'  => 0,
            'question_stores'       => $qa_id ? $this->model_catalog_qa->getQuestionStores($qa_id) : array(0),
        );

        foreach ($form as $key => $v) {
            if (isset($this->request->post[$key])) {
                $this->data[$key] = $this->request->post[$key];
            } else if (isset($qa_info[$key])) {
                $this->data[$key] = $qa_info[$key];
            } else {
                $this->data[$key] = $v;
            }

            if (in_array($key, array('date_asked', 'date_answered', 'date_modified'))) {
                if ($this->data[$key] != '0000-00-00 00:00:00') {
                    $date = new DateTime($this->data[$key]);
                    $formatted = $date->format('Y-m-d');
                } else {
                    $formatted = "";
                }
                $this->data[$key . '_formatted'] = $formatted;
            }
        }

        $this->data['notify_customer'] = (int)$this->config->get("qa_question_reply_notification") && !(int)$this->data['notified'];

        if (!isset($this->request->post['update_date_answered'])) {
            $this->data['update_date_answered'] = $this->data['answer'] == '' || $this->data['date_answered'] == '0000-00-00 00:00:00';
        }

        $this->data['errors'] = $this->error;

        $this->data['token'] = $this->session->data['token'];

        $this->data['alerts'] = $this->alert;

        $this->template = 'catalog/qa_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validateForm(&$data) {
        $errors = false;

        if (!$this->user->hasPermission('modify', 'catalog/qa')) {
            $errors = true;
            $this->alert['error']['permission'] = $this->language->get('error_permission');
        }

        if ($data['product_id'] == '') {
            $errors = true;
            $this->error['product_id'] = $this->language->get('error_product');
        } else {
            $this->load->model('catalog/product');

            $product = $this->model_catalog_product->getProduct($data['product_id']);

            if (!isset($product['product_id'])) {
                $errors = true;
                $data['product_id'] = '';
                $data['product_name'] = '';
                $this->error['product_id'] = $this->language->get('error_product');
            }
        }

        if (utf8_strlen($data['question_author_email']) > 0 && !validate_email(utf8_decode($data['question_author_email']))) {
            $errors = true;
            $this->error['question_author_email'] = $this->language->get('error_email');
        }

        if (utf8_strlen($data['question']) < 1) {
            $errors = true;
            $this->error['question'] = $this->language->get('error_question');
        }

        if ($errors) {
            $errors = true;
            $this->alert['warning']['warning'] = $this->language->get('error_warning');
        }

        return !$errors;
    }

    protected function validateAction($action) {
        $errors = false;

        if (!$this->user->hasPermission('modify', 'catalog/qa')) {
            $errors = true;
            $this->alert['error']['permission'] = $this->language->get('error_permission');
        }

        return !$errors;
    }

    protected function urlParams($search = true, $filters = true, $sort = true, $order = true, $page = true) {
        $url = '';

        if ($search) {
            if (is_string($search)) {
                $url .= '&search=' . urlencode($search);
            } else if (isset($this->request->get['search'])) {
                $url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
            }
        }

        if ($filters) {
            foreach($this->columns as $column => $attr) {
                if (isset($this->request->get['filter_' . $column])) {
                    $url .= '&filter_' . $column . '=' . urlencode(html_entity_decode($this->request->get['filter_' . $column], ENT_QUOTES, 'UTF-8'));
                }
            }
        }

        if ($sort) {
            if (is_string($sort)) {
                $url .= '&sort=' . $sort;
            } else if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
        }

        if ($order) {
            if (is_string($order)) {
                $url .= '&order=' . $order;
            } else if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
        }

        if ($page) {
            if (is_string($page)) {
                $url .= '&page=' . $page;
            } else if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
        }

        $url .= '&token=' . $this->session->data['token'];

        return $url;
    }
}
?>
