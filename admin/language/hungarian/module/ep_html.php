<?php
/*
Developer: Ekrem KAYA
Web Page: www.e-piksel.com
Free Page: free.e-piksel.com
Lisence: GNU General Public License version 3 (GPLv3)
*/

// Heading
$_['heading_title_normal']= 'Korlátlan HTML Module';
$_['heading_title']       = '<font color="green"><b>Korlátlan HTML Module</b></font>';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikerült: A HTML modul módosítása sikerült!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_content_header'] = 'Fejléc';

$_['text_or']			  = 'vagy';
$_['text_create_paypal']  = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=YS4L83BNSEC6A\');">Create your PayPal account</a>';
$_['text_developer']	  = 'Fejlesztő:';
$_['text_html_version']	  = 'E-Piksel Html Module 1.1';

// Entry
$_['entry_heading']		  = 'Fejléc:';
$_['entry_description']   = 'HTML Code / Message:';
$_['entry_layout']        = 'Megjelenés helye:';
$_['entry_position']      = 'Pozíció:';
$_['entry_box_status']    = 'Keret engedélyetés:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_minden_oldal']  = 'Fejléc kikapcsolva:';

// Error
$_['error_permission']    = 'Figyelmeztetés: Önnek nincs jogosultsága a módosításra!';
?>