<?php

// Heading
$_['heading_title']  = 'Utalvány ellenőrzés';

// Text
$_['text_account']   = 'Fiók';
$_['text_vonalkod']  = 'Vonalkód';
$_['text_yes']  = 'Igen';
$_['text_no']  = 'Nem';

$_['text_success']   = 'Sikerült: Az utalvány érvényes.';
$_['text_success_bevaltva']   = 'Sikerült: Az utalvány beváltva.';

// Entry
$_['entry_vonalkod']             = 'Vonalkód:';
$_['entry_vonalkod_bevaltasa']    = 'Vonalkód beváltása:';


// Error
$_['error_vonalkod'] = 'A vonalkód 13 karakterből állhat!';
$_['error_felhasznalva'] = 'Érvénytelen, már felhasznált vonalkód !';
$_['error_nemletezo'] = 'Nem létező vonalkód !';
$_['error_nem_engedelyezett'] = 'Az utalvány még nincs engedélyezve !';
$_['error_nincs_bevaltva'] = 'Az utalvány nem lett beváltva !';
$_['error_nemjogosult'] = 'Létező vonalkód! Az ellenőrzéséhez nincs jogosultsága!';
?>
