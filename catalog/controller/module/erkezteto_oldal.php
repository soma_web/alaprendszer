<?php
class ControllerModuleErkeztetoOldal extends Controller {
    public function index() {
		$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/erkezteto_oldal.css');
        $this->data['styles'] = $this->document->getStyles();

        $settings = $this->config->get('erkezteto_oldal_module');

        $this->data['erkeztetes'] = '';
        $this->data['product_id'] = '';

        if (isset($_REQUEST['oldal'])) {
            $setting = $settings[trim($_REQUEST['oldal'])];

            $this->data['erkeztetes'] = $setting['hova'];
            $this->data['product_id'] = $setting['product_id'];
        }

        if (  !empty($setting['description'][$this->config->get('config_language_id')] )  ) {
    	    $this->data['message'] = html_entity_decode($setting['description'][$this->config->get('config_language_id')], ENT_COMPAT, 'UTF-8');
        } else {
            $this->data['message']='';
        }

        if (!empty($_REQUEST['bisnode_id']) && count($this->db_kalkulator)) {
            $this->load->model('module/erkezteto_oldal');
            $bisnode_data = $this->model_module_erkezteto_oldal->getBisnodeData($_REQUEST['bisnode_id']);
            if ($bisnode_data) {
                $this->model_module_erkezteto_oldal->addBisnodeData($bisnode_data);
            }
        }




        if (isset($setting['status']) && $setting['status'] == 1) {

            $this->data['url'] = $this->adatokToSession();

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/erkezteto_oldal.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/module/erkezteto_oldal.tpl';
            } else {
                $this->template = 'default/template/module/erkezteto_oldal.tpl';
            }
        } else {
            $this->redirect($this->url->link('error/not_found'));
        }

        $this->response->setOutput($this->render());
    }

    public function adatokToSession() {
        $url = '';
        if (!empty($_REQUEST['firstname'])) {
            $this->session->data['guest']['firstname'] = trim($_REQUEST['firstname']);
            $url .= 'firstname='.trim($_REQUEST['firstname']);
        }
        if (!empty($_REQUEST['lastname'])) {
            $this->session->data['guest']['lastname'] = trim($_REQUEST['lastname']);
            $url .= 'lastname='.trim($_REQUEST['lastname']);
        }
        if (!empty($_REQUEST['email'])) {
            $this->session->data['guest']['email'] = trim($_REQUEST['email']);
            $url .= 'email='.trim($_REQUEST['email']);
        }
        if (!empty($_REQUEST['telephone'])) {
            $this->session->data['guest']['telephone'] = trim($_REQUEST['telephone']);
            $url .= 'telephone='.trim($_REQUEST['telephone']);
        }
        if (!empty($_REQUEST['address_1'])) {
            $this->session->data['guest']['payment']['address_1'] = trim($_REQUEST['address_1']);
            $url .= 'address_1='.trim($_REQUEST['address_1']);
        }
        if (!empty($_REQUEST['city'])) {
            $this->session->data['guest']['payment']['city'] = trim($_REQUEST['city']);
            $url .= 'city='.trim($_REQUEST['city']);
        }
        if (!empty($_REQUEST['postcode'])) {
            $this->session->data['guest']['payment']['postcode'] = trim($_REQUEST['postcode']);
            $url .= 'postcode='.trim($_REQUEST['postcode']);
        }
        if (!empty($_REQUEST['adoszam'])) {
            $this->session->data['guest']['payment']['adoszam'] = trim($_REQUEST['adoszam']);
            $url .= 'city='.trim($_REQUEST['adoszam']);
        }
        if (!empty($_REQUEST['company'])) {
            $this->session->data['guest']['payment']['company'] = trim($_REQUEST['company']);
            $url .= 'city='.trim($_REQUEST['company']);
        }
        if (!empty($_REQUEST['country_id'])) {
            $this->session->data['guest']['payment']['country_id'] = trim($_REQUEST['country_id']);
            $url .= 'country_id='.trim($_REQUEST['country_id']);
        }
        if (!empty($_REQUEST['formertek_id'])) {
            $this->session->data['guest']['formertek_id'] = trim($_REQUEST['formertek_id']);
            $url .= 'formertek_id='.trim($_REQUEST['formertek_id']);
        }
        if (!empty($_REQUEST['teszt'])) {
            $this->session->data['teszt'] = trim($_REQUEST['teszt']);
        }

        return $url;

    }


}
?>