BEGIN
DECLARE customer_group_azonosito INT(11) default 0;
DECLARE product_azonosito INT(11) default 0;
DECLARE product_special_azonosito INT(11) default 0;
declare i int(2) default 1;

call termek_felvitel(uj_TermekKod);
SELECT product_id into product_azonosito FROM trodimp_aruhaz.product WHERE cikkszam LIKE uj_TermekKod limit 1;

WHILE i <= 10 DO
set @csoportnev=concat("Ar",i);
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE @csoportnev limit 1;
if customer_group_azonosito = 0 then insert into trodimp_aruhaz.customer_group set `name`= @csoportnev; end if;
SET i=i+1;
END WHILE;

if uj_ListaAr > 0 then
update trodimp_aruhaz.product set price = uj_ListaAr where product_id = product_azonosito;
end if;

if uj_Ar1 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar1" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar1;
else
update trodimp_aruhaz.product_special set price = uj_Ar1 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

if uj_Ar2 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar2" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar2;
else
update trodimp_aruhaz.product_special set price = uj_Ar2 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

if uj_Ar3 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar3" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar3;
else
update trodimp_aruhaz.product_special set price = uj_Ar3 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

if uj_Ar4 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar4" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar4;
else
update trodimp_aruhaz.product_special set price = uj_Ar4 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

if uj_Ar5 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar5" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar5;
else
update trodimp_aruhaz.product_special set price = uj_Ar5 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

if uj_Ar6 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar6" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar6;
else
update trodimp_aruhaz.product_special set price = uj_Ar6 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

if uj_Ar7 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar7" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar7;
else
update trodimp_aruhaz.product_special set price = uj_Ar7 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

if uj_Ar8 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar8" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar8;
else
update trodimp_aruhaz.product_special set price = uj_Ar8 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

if uj_Ar9 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar9" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar9;
else
update trodimp_aruhaz.product_special set price = uj_Ar9 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

if uj_Ar10 > 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` LIKE "Ar10" limit 1;
select product_special_id into product_special_azonosito from trodimp_aruhaz.product_special where product_id=product_azonosito and customer_group_id=customer_group_azonosito limit 1;
if product_special_azonosito=0 then
insert into trodimp_aruhaz.product_special set  product_id        = product_azonosito,
customer_group_id = customer_group_azonosito,
price             = uj_Ar10;
else
update trodimp_aruhaz.product_special set price = uj_Ar10 where product_id = product_azonosito and customer_group_id=customer_group_azonosito;
end if;
end if;

END