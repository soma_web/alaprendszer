<?php

// Heading
$_['heading_title']     = 'Értékesítési jelentés';

// Text
$_['text_year']         = 'Év';
$_['text_month']        = 'Hónap';
$_['text_week']         = 'Hét';
$_['text_day']          = 'Nap';
$_['text_all_status']   = 'Minden állapot';

$_['select_ferfi']          = 'Férfi';
$_['select_holgy']          = 'Nő';
$_['select_kupon']          = 'Kupon';
$_['select_utalvany']       = 'Utalvany';

// Column
$_['column_date_start'] = 'Kezdete';
$_['column_date_end']   = 'Vége';
$_['column_orders']     = 'Rendelések száma';
$_['column_products']   = 'Termékek száma';
$_['column_tax']        = 'Adó';
$_['column_total']      = 'Összesen';

// Entry
$_['entry_date_start']  = 'Kezdete:';
$_['entry_date_end']    = 'Vége:';
$_['entry_group']       = 'Csoportosítás:';
$_['entry_status']      = 'Rendelés állapot:';
$_['entry_korosztaly']      = 'Korosztály:';
$_['entry_nem']             = 'Nemek:';
$_['entry_ingyenes']        = 'Termék:';

$_['entry_iskolai_vegzettseg'] = 'Iskolai végzettség';
$_['column_iskolai_vegzettseg'] = 'Iskolai végzettség';

?>