<?php
class ModelReportWishlist extends Model {
	public function getWishlists($data = array()) {

        $sql = "SELECT c.*, cg.name AS customer_group FROM " . DB_PREFIX . "customer c
                LEFT JOIN " . DB_PREFIX . "customer_group cg ON (c.customer_group_id = cg.customer_group_id)";
        $sql .= " WHERE wishlist > ''
                    AND c.status = 1";

        $sql .= " GROUP BY c.customer_id ";

        /*if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }*/

        $query= $this->db->query($sql);

        $wishlist = array();
        if ($query->num_rows > 0) {
            foreach ($query->rows as $key=>$value) {
                $query->rows[$key]['wishlist'] = unserialize($value['wishlist']);
                $query->rows[$key]['customer'] = $value['firstname'].' '.$value['lastname'];

                foreach($query->rows[$key]['wishlist'] as $product) {
                    $wishlist[$product][] = array(
                        'customer'          =>  $query->rows[$key]['customer'],
                        'email'             =>  $query->rows[$key]['email'],
                        'customer_group'    =>  $query->rows[$key]['customer_group']
                    );
                }
            }
        }


        return $wishlist;
	}

    public function getTotalWishlists($data = array()) {

        $sql = "SELECT COUNT(DISTINCT c.customer_id) AS total FROM " . DB_PREFIX . "customer c
                LEFT JOIN " . DB_PREFIX . "customer_group cg ON (c.customer_group_id = cg.customer_group_id) WHERE wishlist > '' ";

        $query= $this->db->query($sql);

        return $query->row['total'];
    }



}
?>