<?php if (isset($termekadatok['megjelenit_product_cikkszam']) && !empty($termekadatok['megjelenit_product_cikkszam'])) { ?>
    <div class="termekadatok termekadatok-cikkszam" >
        <span class="termekadatok-cikkszam-szoveg"><?php echo $termekadatok['megjelenit_product_cikkszam']['text_name']; ?></span>
        <span class="termekadatok-cikkszam-ertek"><?php echo $termekadatok['megjelenit_product_cikkszam']['value']; ?></span>
    </div>
<?php } ?>