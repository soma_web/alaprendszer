<?php 
class ControllerInformationArajanlatotKerek extends Controller {
	private $error = array(); 
	    
  	public function index() {
		$this->language->load('information/arajanlatot_kerek');

    	$this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("catalog/product");

        $product = $this->model_catalog_product->getProduct($this->request->request['product_id']);
        $this->data['product_id']           = $this->request->request['product_id'];
        $this->data['product_name']         = $product['name'];
        $this->data['product_model']        = $product['model'];
        $this->data['product_cikkszam']     = $product['cikkszam'];
        $this->data['product_gyarto']       = $product['manufacturer'];

        $this->data['text_product_id']           = $this->language->get('text_product_id');
        $this->data['text_product_name']         = $this->language->get('text_product_name');
        $this->data['text_product_model']        = $this->language->get('text_product_model');
        $this->data['text_product_cikkszam']     = $this->language->get('text_product_cikkszam');
        $this->data['text_product_gyarto']       = $this->language->get('text_product_gyarto');
        $this->data['text_success']       = $this->language->get('text_success');

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_location'] = $this->language->get('text_location');
        $this->data['text_contact'] = $this->language->get('text_contact');
        $this->data['text_map'] = $this->language->get('text_map');
        $this->data['text_address'] = $this->language->get('text_address');
        $this->data['text_telephone'] = $this->language->get('text_telephone');
        $this->data['text_fax'] = $this->language->get('text_fax');
        $this->data['text_close'] = $this->language->get('text_close');

        $this->data['entry_name']       = $this->language->get('entry_name');
        $this->data['entry_email']      = $this->language->get('entry_email');
        $this->data['entry_enquiry']    = $this->language->get('entry_enquiry');
        $this->data['entry_captcha']    = $this->language->get('entry_captcha');
        $this->data['entry_telefon']    = $this->language->get('entry_telefon');
        $this->data['entry_varos']       = $this->language->get('entry_varos');
        $this->data['entry_iranyitoszam']= $this->language->get('entry_iranyitoszam');
        $this->data['entry_ceg']         = $this->language->get('entry_ceg');
        $this->data['entry_adoszam']     = $this->language->get('entry_adoszam');
        $this->data['entry_utca']        = $this->language->get('entry_utca');

        $this->data['kilep'] = false;

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $html = 'Termék:'."\r\n";
            $html .= $this->data['text_product_id'].": ".$this->data['product_id']."\r\n";

            if ($this->data['product_name']) {
                $html .= $this->data['text_product_name'].": ".$this->data['product_name']."\r\n";
            }
            if ($this->data['product_model']) {
                $html .= $this->data['text_product_model'].": ".$this->data['product_model']."\r\n";
            }
            if ($this->data['product_cikkszam']) {
                $html .= $this->data['text_product_cikkszam'].": ".$this->data['product_cikkszam']."\r\n";
            }
            if ($this->data['product_gyarto']) {
                $html .= $this->data['text_product_gyarto'].": ".$this->data['product_gyarto']."\r\n";
            }

            $html.= "\r\n";
            $html.= "\r\n"."Adatok:"."\r\n";
            $html .= !empty($this->request->post['name'])           ? $this->data['entry_name'].": ".           $this->request->post['name']."\r\n" : '';
            $html .= !empty($this->request->post['email'])          ? $this->data['entry_email'].": ".          $this->request->post['email']."\r\n" : '';
            $html .= !empty($this->request->post['telefon'])        ? $this->data['entry_telefon'].": ".        $this->request->post['telefon']."\r\n" : '';
            $html .= !empty($this->request->post['iranyitoszam'])   ? $this->data['entry_iranyitoszam'].": ".   $this->request->post['iranyitoszam']."\r\n" : '';
            $html .= !empty($this->request->post['varos'])          ? $this->data['entry_varos'].": ".          $this->request->post['varos']."\r\n" : '';
            $html .= !empty($this->request->post['utca'])           ? $this->data['entry_utca'].": ".           $this->request->post['utca']."\r\n" : '';
            $html .= !empty($this->request->post['ceg'])            ? $this->data['entry_ceg'].": ".            $this->request->post['ceg']."\r\n" : '';
            $html .= !empty($this->request->post['adoszam'])        ? $this->data['entry_adoszam'].": ".        $this->request->post['adoszam']."\r\n" : '';


            $this->request->post['enquiry'] =  $html."\r\n".$this->request->post['enquiry'];

            $mail = new Mail();
            $mail->protocol     = $this->config->get('config_mail_protocol');
            $mail->parameter    = $this->config->get('config_mail_parameter');
            $mail->hostname     = $this->config->get('config_smtp_host');
            $mail->username     = $this->config->get('config_smtp_username');
            $mail->password     = $this->config->get('config_smtp_password');
            $mail->port         = $this->config->get('config_smtp_port');
            $mail->timeout      = $this->config->get('config_smtp_timeout');
            $mail->setTo($this->config->get('config_email'));
            $mail->setFrom($_POST['email']);
            $mail->setSender($_POST['email']);

            $mail->setSubject('Árajánlatot kérek');
            $mail->setText(html_entity_decode($this->request->post['enquiry'], ENT_QUOTES, 'UTF-8'));
            $mail->send();

            $emails = explode(',', $this->config->get('config_alert_emails'));
            foreach ($emails as $email) {
                if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
                    $mail->setTo($email);
                    $mail->send();
                }
            }


            $mail = new Mail();
            $mail->protocol     = $this->config->get('config_mail_protocol');
            $mail->parameter    = $this->config->get('config_mail_parameter');
            $mail->hostname     = $this->config->get('config_smtp_host');
            $mail->username     = $this->config->get('config_smtp_username');
            $mail->password     = $this->config->get('config_smtp_password');
            $mail->port         = $this->config->get('config_smtp_port');
            $mail->timeout  = $this->config->get('config_smtp_timeout');
            $mail->setTo($_POST['email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender($_POST['email']);

            $mail->setSubject('Árajánlatot kérek visszaigazolás');
            $this->request->post['enquiry'] = 'Az ajánlatkérés az áruház felé az alábbi tartalommal elküldve:'."\r\n"."\r\n".$this->request->post['enquiry'];
            $mail->setText(html_entity_decode($this->request->post['enquiry'], ENT_NOQUOTES, 'UTF-8'));
            $mail->send();

            $this->konyvel();
            $this->data['kilep'] = true;
            //exit;
        }




        if (isset($this->error['name'])) {
            $this->data['error_name'] = $this->error['name'];
        } else {
            $this->data['error_name'] = '';
        }
        if (isset($this->error['telefon'])) {
            $this->data['error_telefon'] = $this->error['telefon'];
        } else {
            $this->data['error_telefon'] = '';
        }
		
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}
        if (isset($this->error['iranyitoszam'])) {
            $this->data['error_iranyitoszam'] = $this->error['iranyitoszam'];
        } else {
            $this->data['error_iranyitoszam'] = '';
        }
        if (isset($this->error['varos'])) {
            $this->data['error_varos'] = $this->error['varos'];
        } else {
            $this->data['error_varos'] = '';
        }
        if (isset($this->error['utca'])) {
            $this->data['error_utca'] = $this->error['utca'];
        } else {
            $this->data['error_utca'] = '';
        }
        if (isset($this->error['ceg'])) {
            $this->data['error_ceg'] = $this->error['ceg'];
        } else {
            $this->data['error_ceg'] = '';
        }
        if (isset($this->error['adoszam'])) {
            $this->data['error_adoszam'] = $this->error['adoszam'];
        } else {
            $this->data['error_adoszam'] = '';
        }

        if (isset($this->error['enquiry'])) {
			$this->data['error_enquiry'] = $this->error['enquiry'];
		} else {
			$this->data['error_enquiry'] = '';
		}		
		
 		if (isset($this->error['captcha'])) {
			$this->data['error_captcha'] = $this->error['captcha'];
		} else {
			$this->data['error_captcha'] = '';
		}

    	$this->data['button_continue'] = $this->language->get('text_submit');
    
		$this->data['action']       = $this->url->link('information/arajanlatot_kerek');
		$this->data['store']        = $this->config->get('config_name');
		$this->data['ceg_neve']     = $this->config->get('config_owner');
    	$this->data['address']      = nl2br($this->config->get('config_address'));
    	$this->data['telephone']    = $this->config->get('config_telephone');
    	$this->data['fax']          = $this->config->get('config_fax');

        $address = $this->customer->getAddress();

        if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];

		} elseif (isset($this->request->get['name'])) {
            $this->data['name'] = $this->request->get['name'];
        } else {
			$this->data['name'] = $this->customer->getFirstName();
		}

        if (isset($this->request->post['telefon'])) {
            $this->data['telefon'] = $this->request->post['telefon'];

        } elseif (isset($this->request->get['telefon'])) {
            $this->data['telefon'] = $this->request->get['telefon'];
        } else {
            $this->data['telefon'] = $this->customer->getTelephone();
        }

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
        } elseif (isset($this->request->get['email'])) {
            $this->data['email'] = $this->request->get['email'];
            $this->data['error_email'] = $this->language->get('error_email');
        } else {
			$this->data['email'] = $this->customer->getEmail();
		}

        if (isset($this->request->post['iranyitoszam'])) {
            $this->data['iranyitoszam'] = $this->request->post['iranyitoszam'];
        } elseif (isset($this->request->get['iranyitoszam'])) {
            $this->data['iranyitoszam'] = $this->request->get['iranyitoszam'];
            $this->data['error_iranyitoszam'] = $this->language->get('error_iranyitoszam');
        } else {
            $this->data['iranyitoszam'] = $address['iranyitoszam'];
        }
        if (isset($this->request->post['varos'])) {
            $this->data['varos'] = $this->request->post['varos'];
        } elseif (isset($this->request->get['varos'])) {
            $this->data['varos'] = $this->request->get['varos'];
            $this->data['error_varos'] = $this->language->get('error_varos');
        } else {
            $this->data['varos'] = $address['varos'];
        }
        if (isset($this->request->post['utca'])) {
            $this->data['utca'] = $this->request->post['utca'];
        } elseif (isset($this->request->get['utca'])) {
            $this->data['utca'] = $this->request->get['utca'];
            $this->data['error_utca'] = $this->language->get('error_utca');
        } else {
            $this->data['utca'] = $address['utca'];
        }
        if (isset($this->request->post['ceg'])) {
            $this->data['ceg'] = $this->request->post['ceg'];
        } elseif (isset($this->request->get['ceg'])) {
            $this->data['ceg'] = $this->request->get['ceg'];
            $this->data['error_ceg'] = $this->language->get('error_ceg');
        } else {
            $this->data['ceg'] = $this->customer->getCompany();
        }
        if (isset($this->request->post['adoszam'])) {
            $this->data['adoszam'] = $this->request->post['adoszam'];
        } elseif (isset($this->request->get['adoszam'])) {
            $this->data['adoszam'] = $this->request->get['adoszam'];
            $this->data['error_adoszam'] = $this->language->get('error_adoszam');
        } else {
            $this->data['adoszam'] = $this->customer->getAdoszam();
        }



        $this->data['megjelenit_telefonszam']           = $this->config->get('megjelenit_arajanlat_telefon');
        $this->data['megjelenit_telefonszam_kotelezo']  = $this->config->get('megjelenit_arajanlat_telefon_kotelezo');
        $this->data['megjelenit_iranyitoszam']          = $this->config->get('megjelenit_arajanlat_iranyitoszam');
        $this->data['megjelenit_iranyitoszam_kotelezo'] = $this->config->get('megjelenit_arajanlat_iranyitoszam_kotelezo');
        $this->data['megjelenit_varos']                 = $this->config->get('megjelenit_arajanlat_varos');
        $this->data['megjelenit_varos_kotelezo']        = $this->config->get('megjelenit_arajanlat_varos_kotelezo');
        $this->data['megjelenit_utca']                  = $this->config->get('megjelenit_arajanlat_utca');
        $this->data['megjelenit_utca_kotelezo']         = $this->config->get('megjelenit_arajanlat_utca_kotelezo');
        $this->data['megjelenit_ceg']                   = $this->config->get('megjelenit_arajanlat_vallalkozas');
        $this->data['megjelenit_ceg_kotelezo']          = $this->config->get('megjelenit_arajanlat_vallalkozas_kotelezo');
        $this->data['megjelenit_adoszam_kotelezo']      = $this->config->get('megjelenit_arajanlat_adoszam');
        $this->data['megjelenit_adoszam']               = $this->config->get('megjelenit_arajanlat_adoszam_kotelezo');



        if (isset($_POST['enquiry'])) {
			$this->data['enquiry'] = $_POST['enquiry'];
        } elseif (isset($this->request->get['enquiry'])) {
            $this->data['enquiry'] = $this->request->get['enquiry'];
        } else {
            $this->data['enquiry'] = $this->language->get("text_enquiry");
        }
		
		if (isset($this->request->post['captcha'])) {
			$this->data['captcha'] = $this->request->post['captcha'];
		} else {
			$this->data['captcha'] = '';
		}		

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/arajanlatot_kerek.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/information/arajanlatot_kerek.tpl';
		} else {
			$this->template = 'default/template/information/arajanlatot_kerek.tpl';
		}


        $this->response->setOutput($this->render());
    }

  	private function validate($para=0) {
        //parameter

        if (!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if ($this->config->get('megjelenit_arajanlat_telefon') && $this->config->get('megjelenit_arajanlat_telefon_kotelezo')) {
            if (utf8_strlen($this->request->post['telefon']) < 7 || utf8_strlen($this->request->post['telefon']) > 32) {
                $this->error['telefon'] = $this->language->get('error_telefon');
            }
        }

        if ($this->config->get('megjelenit_arajanlat_iranyitoszam') && $this->config->get('megjelenit_arajanlat_iranyitoszam_kotelezo')) {
            if (utf8_strlen($this->request->post['iranyitoszam']) != 4) {
                $this->error['iranyitoszam'] = $this->language->get('error_iranyitoszam');
            }
        }
        if ($this->config->get('megjelenit_arajanlat_varos') && $this->config->get('megjelenit_arajanlat_varos_kotelezo')) {
            if (utf8_strlen($this->request->post['varos']) < 2 || utf8_strlen($this->request->post['varos']) > 32) {
                $this->error['varos'] = $this->language->get('error_varos');
            }
        }
        if ($this->config->get('megjelenit_arajanlat_utca') && $this->config->get('megjelenit_arajanlat_utca_kotelezo')) {
            if (utf8_strlen($this->request->post['utca']) < 2 || utf8_strlen($this->request->post['utca']) > 64) {
                $this->error['utca'] = $this->language->get('error_utca');
            }
        }
        if ($this->config->get('megjelenit_arajanlat_vallalkozas') && $this->config->get('megjelenit_arajanlat_vallalkozas_kotelezo')) {
            if (utf8_strlen($this->request->post['ceg']) < 2 || utf8_strlen($this->request->post['ceg']) > 64) {
                $this->error['ceg'] = $this->language->get('error_ceg');
            }
        }
        if ($this->config->get('megjelenit_arajanlat_adoszam') && $this->config->get('megjelenit_arajanlat_adoszam_kotelezo')) {
            if (utf8_strlen($this->request->post['adoszam']) < 7 || utf8_strlen($this->request->post['adoszam']) > 32) {
                $this->error['adoszam'] = $this->language->get('error_adoszam');
            }
        }

    	/*if ((utf8_strlen($this->request->post['enquiry']) < 10) || (utf8_strlen($this->request->post['enquiry']) > 3000)) {
      		$this->error['enquiry'] = $this->language->get('error_enquiry');
    	}*/

    	if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
      		$this->error['captcha'] = $this->language->get('error_captcha');
    	}
		
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
    }

	public function captcha() {
		$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
    }

    public function konyvel() {

        $data = array(
            'product_id'        =>   $this->data['product_id'],
            'product_name'      =>   $this->data['product_name'],
            'product_model'     =>   $this->data['product_model'],
            'product_cikkszam'  =>   $this->data['product_cikkszam'],
            'product_gyarto'    =>   $this->data['product_gyarto'],
            'name'              =>   $_POST['name'],
            'email'             =>   $_POST['email'],
            'telefon'           =>   isset($_POST['telefon'])       ? $_POST['telefon']     : '',
            'iranyitoszam'      =>   isset($_POST['iranyitoszam'])  ? $_POST['iranyitoszam']: '',
            'varos'             =>   isset($_POST['varos'])         ? $_POST['varos']       : '',
            'utca'              =>   isset($_POST['utca'])          ? $_POST['utca']        : '',
            'ceg'               =>   isset($_POST['ceg'])           ? $_POST['ceg']         : '',
            'adoszam'           =>   isset($_POST['adoszam'])       ? $_POST['adoszam']     : '',
            'enquiry'           =>   $_POST['enquiry']
        );
        $this->load->model("catalog/arajanlatot_kerek");

        $this->model_catalog_arajanlatot_kerek->addElorendeles($data);
    }
}
?>