<?php  
class ControllerModuleSpecialOfferReminder extends Controller {
	protected function index($setting) {
		static $module = 0;
		
		$this->language->load('module/special_offer_reminder');
		
		$this->load->model('tool/image');
		$this->load->model('module/special_offer_reminder');
		
		$this->document->addStyle('catalog/view/theme/' .  $this->config->get('config_template') . '/stylesheet/special_offer_reminder.css');
		
		$this->data['heading_title'] = $this->config->get('special_offer_reminder_heading_title_' . $this->config->get('config_language_id') );
		
		$this->data['modal_title'] = $this->language->get('modal_title');
		$this->data['text_info'] = $this->language->get('text_info');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_days_before'] = $this->language->get('entry_days_before');
		$this->data['text_set_alert'] = $this->language->get('text_set_alert');
		
		if ($this->customer->isLogged()){
			$this->data['sor_fullname'] = $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
			$this->data['sor_email'] = $this->customer->getEmail();
			$this->data['fieldset_class'] = 'hidden';
		} else {
			$this->data['sor_fullname'] = '';
			$this->data['sor_email'] = '';
			$this->data['fieldset_class'] = '';
		}
		
		$show_module = false;
		
		if (isset($this->request->get['product_id'])){
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		$special_info = $this->model_module_special_offer_reminder->getProductSpecialInfo($product_id);
		
		if ($special_info){
			$show_module = true;
			$date_expire = date($this->language->get('date_format_short'), strtotime($special_info['date_end']));
			$this->data['text_expire_date'] = sprintf($this->language->get('text_expire_date'), $date_expire);
			$this->data['sor_expire_date'] = $special_info['date_end'];
		}
		
		$this->data['customer_logged'] = $this->customer->isLogged();
		$this->data['sor_product_id'] = $this->request->get['product_id'];
		$this->data['theme_name'] = $this->config->get('config_template');
		$this->data['image'] = $this->model_tool_image->resize($this->config->get('special_offer_reminder_image'), $this->config->get('special_offer_reminder_width'), $this->config->get('special_offer_reminder_height'));
		$this->data['display_type'] = $this->config->get('special_offer_reminder_display_type');
		$this->data['show_module'] = $show_module;
		
		$this->data['module'] = $module++;
				
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/special_offer_reminder.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/special_offer_reminder.tpl';
		} else {
			$this->template = 'default/template/module/special_offer_reminder.tpl';
		}
		
		if ($show_module){ 
			$this->render();
		}	
	}
	
	public function save_reminder() {
		$this->language->load('module/special_offer_reminder');
		
		$this->load->model('module/special_offer_reminder');
		
		$json = array();
		
		if (!is_numeric($this->request->post['sor_days_before']) || (int)$this->request->post['sor_days_before'] == 0 ) {
			$json['error'] = $this->language->get('error_days_before');
		}	
		
		if (!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['sor_email'])) {
      		$json['error'] = $this->language->get('error_email');
    	}
		
		if (utf8_strlen($this->request->post['sor_fullname']) < 3) {
			$json['error'] = $this->language->get('error_name');
		}
		
		
		if (is_numeric($this->request->post['sor_days_before'])) {
			$days_before = (int)$this->request->post['sor_days_before'];
			$expire_split = preg_split('/\-/', $this->request->post['sor_expire_date']);		
				
			$now = mktime(0,0,0, date('m'), date('d'), date('Y'));
			$target = mktime(0,0,0, $expire_split[1], $expire_split[2], $expire_split[0]);
			$days_left = (int)(($target - $now)/60/60/24);
			
			if ($days_left < $days_before){
				$json['error'] = sprintf($this->language->get('error_only_days_remain'), $days_left);
			}
			
			if ($days_left == $days_before){
				$json['error'] = $this->language->get('error_this_is_day');
			}
		}		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && !isset($json['error'])) {
			$reminder_id = $this->model_module_special_offer_reminder->existReminder($this->request->post);
			if (!$reminder_id) {
				$this->model_module_special_offer_reminder->addReminder($this->request->post);
			} else {
				$this->model_module_special_offer_reminder->editReminder($reminder_id, $this->request->post);
			}	
			
			$json['success'] = $this->language->get('text_success');
		}
		
		$this->response->setOutput(json_encode($json));
	}
}
?>