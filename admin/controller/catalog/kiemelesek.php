<?php
class ControllerCatalogkiemelesek extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/kiemelesek');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/kiemelesek');

        $this->data['heading_title']    = $this->language->get('heading_title');
        $this->data['button_insert']    = $this->language->get('button_insert');
        $this->data['button_delete']    = $this->language->get('button_delete');

        $this->data['column_megnevezes']   = $this->language->get('column_megnevezes');
        $this->data['column_class']   = $this->language->get('column_class');
        $this->data['column_ara']   = $this->language->get('column_ara');
        $this->data['column_status']   = $this->language->get('column_status');
        $this->data['column_action']    = $this->language->get('column_action');

        $this->data['text_no_results']    = $this->language->get('text_no_results');
        $this->data['error_warning']    = '';
        $this->data['success'] = '';
        $this->data['text_engedelyezett']  = $this->language->get('text_engedelyezett');
        $this->data['text_letiltott']  = $this->language->get('text_letiltott');
        $this->data['column_sort_order']= $this->language->get('column_sort_order');
        $this->data['column_maximum']   = $this->language->get('column_maximum');


        $this->getList();
    }

    public function insert() {
        $this->load->language('catalog/kiemelesek');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/kiemelesek');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $kiemelesek = $this->config->get('kiemelesek');
            $last_id = 0;

            if ($kiemelesek) {
                if (is_array($kiemelesek) ) {
                    foreach ($kiemelesek as $value) {
                        if ($value['kiemelesek_id'] > $last_id) {
                            $last_id = $value['kiemelesek_id'];
                        }
                    }
                }
            }
            $last_id++;
            if ( !empty($_REQUEST['megnevezes']) ) {
                $kiir["kiemelesek"] = $this->config->get("kiemelesek");
                $kiir["kiemelesek"][] = array(
                    'megnevezes'        => $_REQUEST['megnevezes'],
                    'kiemelesek_id'     => $last_id,
                    'ara'               => $_REQUEST['ara'],
                    'status'            => $_REQUEST['status'],
                    'tax_class_id'      => $_REQUEST['tax_class_id'],
                    'maximum'           => $_REQUEST['maximum'],
                    'class'             => $_REQUEST['class'],
                    'sort_order'        => $_REQUEST['sort_order'],
                    'beallitas'         => $_REQUEST['beallitas']

                );
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('kiemelesek', $kiir);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('catalog/kiemelesek', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function update() {
        $this->load->language('catalog/kiemelesek');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/kiemelesek');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $kiir["kiemelesek"] = $this->config->get("kiemelesek");
            foreach ($kiir["kiemelesek"] as $value) {
                if ($value['kiemelesek_id'] ==  $_REQUEST['kiemelesek_id']) {
                    $modositott["kiemelesek"][] = array(
                        'megnevezes'         => $_REQUEST['megnevezes'],
                        'kiemelesek_id'      => $_REQUEST['kiemelesek_id'],
                        'ara'                => $_REQUEST['ara'],
                        'status'             => $_REQUEST['status'],
                        'tax_class_id'       => $_REQUEST['tax_class_id'],
                        'class'              => $_REQUEST['class'],
                        'maximum'            => $_REQUEST['maximum'],
                        'sort_order'         => $_REQUEST['sort_order'],
                        'beallitas'          => $_REQUEST['beallitas']

                    );
                } else {
                    $modositott["kiemelesek"][] = array(
                        'megnevezes'        => $value['megnevezes'],
                        'kiemelesek_id'  => $value['kiemelesek_id'],
                        'ara'               => $value['ara'],
                        'status'            => $value['status'],
                        'tax_class_id'      => $value['tax_class_id'],
                        'maximum'           => $value['maximum'],
                        'class'             => $value['class'],
                        'sort_order'        => $value['sort_order'],
                        'beallitas'         => $value['beallitas']

                    );
                }
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('kiemelesek', $modositott);
            $this->session->data['success'] = $this->language->get('text_success');


            $this->redirect($this->url->link('catalog/kiemelesek', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/kiemelesek');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/kiemelesek');

        if (isset($this->request->post['selected']) ) {

            $kiir["kiemelesek"] = $this->config->get("kiemelesek");

            foreach ($kiir["kiemelesek"] as $value) {

                if ( !in_array($value['kiemelesek_id'],$_REQUEST['selected']) ) {
                    $modositott["kiemelesek"][] = array(
                        'megnevezes'    => $value['megnevezes'],
                        'kiemelesek_id' => $value['kiemelesek_id'],
                        'ara'           => $value['ara'],
                        'status'        => $value['status'],
                        'tax_class_id'  => $value['tax_class_id'],
                        'class'         => $value['class'],
                        'maximum'       => $value['maximum'],
                        'sort_order'    => $value['sort_order'],
                        'beallitas'     => $value['beallitas']
                    );
                }
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('kiemelesek', $modositott);
            $this->session->data['success'] = $this->language->get('text_success');


            $this->redirect($this->url->link('catalog/kiemelesek', 'token=' . $this->session->data['token'], 'SSL'));

        }

        $this->getList();
    }

    private function getList() {

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/kiemelesek', 'token=' . $this->session->data['token'] , 'SSL'),
            'separator' => ' :: '
        );

        $this->data['insert'] = $this->url->link('catalog/kiemelesek/insert', 'token=' . $this->session->data['token'] , 'SSL');
        $this->data['delete'] = $this->url->link('catalog/kiemelesek/delete', 'token=' . $this->session->data['token'] , 'SSL');



        $this->data['kiemeleseks'] = array();
        $kiemelesek = $this->config->get("kiemelesek");

        if ($kiemelesek) {
            foreach ($kiemelesek as $value) {
                $action = array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->url->link('catalog/kiemelesek/update', 'token=' . $this->session->data['token'].'&kiemelesek_id='.$value['kiemelesek_id'], 'SSL')
                );

                if ($value['status'] == 0) {
                    $statusza = $this->data['text_letiltott'];
                } else {
                    $statusza = $this->data['text_engedelyezett'];
                }




                $this->data['kiemeleseks'][] = array(
                    'megnevezes'     => $value['megnevezes'],
                    'kiemelesek_id'  => $value['kiemelesek_id'],
                    'ara'            => $value['ara'],
                    'action'         => $action,
                    'status'         => $value['status'],
                    'tax_class_id'   => $value['tax_class_id'],
                    'class'          => $value['class'],
                    'maximum'        => $value['maximum'],
                    'sort_order'     => $value['sort_order'],
                    'statusza'       => $statusza,
                    'selected'       => isset($this->request->post['selected']) && in_array($value['kiemelesek_id'], $this->request->post['selected']),

                );
            }
        }


        $sortingSettings = array(
            0       =>  array(
                'orderby' =>  'sort_order',
                'sortorder'     => 'ASC'
            )
        );

        $rendezo = new ArrayOfArrays($this->data['kiemeleseks']);
        $rendezo->multiSorting($sortingSettings,true);
        $this->data['kiemeleseks'] = $rendezo->getArrayCopy();


        $this->template = 'catalog/kiemelesek_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function getForm() {
        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled']     = $this->language->get('text_enabled');
        $this->data['text_disabled']    = $this->language->get('text_disabled');
        $this->data['text_yes']         = $this->language->get('text_yes');
        $this->data['text_no']          = $this->language->get('text_no');
        $this->data['text_percent']     = $this->language->get('text_percent');
        $this->data['text_amount']      = $this->language->get('text_amount');
        $this->data['text_select']      = $this->language->get('text_select');

        $this->data['entry_name']       = $this->language->get('entry_name');
        $this->data['entry_date_start'] = $this->language->get('entry_date_start');
        $this->data['entry_date_end']   = $this->language->get('entry_date_end');
        $this->data['entry_status']     = $this->language->get('entry_status');
        $this->data['entry_ara']        = $this->language->get('entry_ara');
        $this->data['entry_class']      = $this->language->get('entry_class');
        $this->data['entry_sorrend']    = $this->language->get('entry_sorrend');
        $this->data['entry_tax_class']  = $this->language->get('entry_tax_class');
        $this->data['entry_darabszam']  = $this->language->get('entry_darabszam');
        $this->data['entry_beallitas']  = $this->language->get('entry_beallitas');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_kiemelesek_history'] = $this->language->get('tab_kiemelesek_history');

        $this->data['token'] = $this->session->data['token'];

        $this->data['text_engedelyezett']  = $this->language->get('text_engedelyezett');
        $this->data['text_letiltott']  = $this->language->get('text_letiltott');
        $this->data['text_altalanos']  = $this->language->get('text_altalanos');
        $this->data['text_ingyenes']  = $this->language->get('text_ingyenes');
        $this->data['text_egyeb']  = $this->language->get('text_egyeb');

        $this->load->model('localisation/tax_class');
        $this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

        $files = glob(DIR_CATALOG . 'view/theme/default/template/kiemelesek/*.tpl');

        $tpls = array();
        if ($files) {
            foreach ($files as $file) {
                $tpls[] = basename($file, '.tpl');
            }
        }
        $this->data['tpls'] = $tpls;



        if (isset($this->request->get['kiemelesek_id'])) {
            $kiemelesek = $this->config->get("kiemelesek");
            foreach ($kiemelesek as $value) {
                if ($value['kiemelesek_id'] == $this->request->get['kiemelesek_id']) {
                    $this->data['kiemelesek_id'] = $value['kiemelesek_id'];
                    $this->data['megnevezes']    = $value['megnevezes'];
                    $this->data['ara']           = $value['ara'];
                    $this->data['status']        = $value['status'];
                    $this->data['tax_class_id']  = $value['tax_class_id'];
                    $this->data['class']         = $value['class'];
                    $this->data['sort_order']    = $value['sort_order'];
                    $this->data['maximum']       = $value['maximum'];
                    $this->data['beallitas']     = $value['beallitas'];
                }
            }

        } else {
            $this->data['kiemelesek_id'] = 0;
            $this->data['megnevezes']    = '';
            $this->data['ara']           = 0;
            $this->data['status']        = 0;
            $this->data['tax_class_id']  = 0;
            $this->data['class']         = '';
            $this->data['sort_order']    = 0;
            $this->data['maximum']       = 0;
            $this->data['beallitas']     = 0;
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }



        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/kiemelesek', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        if (!isset($this->request->get['kiemelesek_id'])) {
            $this->data['action'] = $this->url->link('catalog/kiemelesek/insert', 'token=' . $this->session->data['token'], 'SSL');
        } else {
            $this->data['action'] = $this->url->link('catalog/kiemelesek/update', 'token=' . $this->session->data['token'] . '&kiemelesek_id=' . $this->request->get['kiemelesek_id'], 'SSL');
        }

        $this->data['cancel'] = $this->url->link('catalog/kiemelesek', 'token=' . $this->session->data['token'], 'SSL');


        $this->template = 'catalog/kiemelesek_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

}
?>