<?php
class ControllerCatalogOptionSzin extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/option_szin');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/option_szin');

        $this->getList();
    }

    public function insert() {
        $this->load->language('catalog/option_szin');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/option_szin');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_option_szin->addOptionSzin($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function update() {
        $this->load->language('catalog/option_szin');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/option_szin');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_option_szin->editOptionSzin($this->request->get['option_szin_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/option_szin');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/option_szin');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $optino_szin_id) {
                $this->model_catalog_option_szin->deleteOptionSzin($optino_szin_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    private function getList() {
        $this->data['button_filter'] = $this->language->get('button_filter');

        $filter_szin        = isset($this->request->get['filter_szin'])         ? $this->request->get['filter_szin'] : null;
        $filter_szinkod     = isset($this->request->get['filter_szinkod'])      ? $this->request->get['filter_szinkod'] : null;
        $sort               = isset($this->request->get['sort'])                ? $this->request->get['sort'] : 'name';
        $order              = isset($this->request->get['order'])               ? $this->request->get['order'] : 'ASC';
        $page               = isset($this->request->get['page'])                ? $this->request->get['page'] : '1';


        $url = '';
        $url .=  isset($this->request->get['filter_szin'])          ? '&filter_szin=' . $this->request->get['filter_szin'] : '';
        $url .=  isset($this->request->get['filter_szinkod'])       ? '&filter_szinkod=' . $this->request->get['filter_szinkod'] : '';
        $url .=  isset($this->request->get['sort'])                 ? '&sort=' . $this->request->get['sort'] : '';
        $url .=  isset($this->request->get['order'])                ? '&order=' . $this->request->get['order'] : '';
        $url .=  isset($this->request->get['page'])                 ? '&page=' . $this->request->get['page'] : '';




        /*if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }*/

        /*$url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }*/

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['insert'] = $this->url->link('catalog/option_szin/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['delete'] = $this->url->link('catalog/option_szin/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['option_szins'] = array();

        $data = array(
            'filter_szin'       => $filter_szin,
            'filter_szinkod'    => $filter_szinkod,
            'sort'              => $sort,
            'order'             => $order,
            'start'             => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'             => $this->config->get('config_admin_limit')
        );

        $option_szin_total = $this->model_catalog_option_szin->getTotalOptionSzinek($data);
        $results = $this->model_catalog_option_szin->getOptionSzinek($data);

        $this->data['option_szinek'] = array();
        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => $this->url->link('catalog/option_szin/update', 'token=' . $this->session->data['token'] . '&option_szin_id=' . $result['option_szin_id'] . $url, 'SSL')
            );

            $this->data['option_szinek'][] = array(
                'option_szin_id'    => $result['option_szin_id'],
                'name'              => $result['name'],
                'szinkod'           => $result['szinkod'],
                'sort_order'        => $result['sort_order'],
                'selected'          => isset($this->request->post['selected']) && in_array($result['option_szin_id'], $this->request->post['selected']),
                'action'            => $action
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['column_name'] = $this->language->get('column_name');
        $this->data['column_szinkod'] = $this->language->get('column_szinkod');
        $this->data['column_sort_order'] = $this->language->get('column_sort_order');
        $this->data['column_action'] = $this->language->get('column_action');

        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

         $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }



        $this->data['sort_name'] = $this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
        $this->data['sort_szinkod'] = $this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . '&sort=szinkod' . $url, 'SSL');
        $this->data['sort_sort_order'] = $this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }


        $url = '';
        $url .=  isset($this->request->get['filter_szin'])          ? '&filter_szin=' . $this->request->get['filter_szin'] : '';
        $url .=  isset($this->request->get['filter_szinkod'])       ? '&filter_szinkod=' . $this->request->get['filter_szinkod'] : '';
        $url .=  isset($this->request->get['sort'])                 ? '&sort=' . $this->request->get['sort'] : '';
        $url .=  isset($this->request->get['order'])                ? '&order=' . $this->request->get['order'] : '';
        //$url .=  isset($this->request->get['page'])                 ? '&page=' . $this->request->get['page'] : '';


        $pagination = new Pagination();
        $pagination->total = $option_szin_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['filter_szin']      = $filter_szin;
        $this->data['filter_szinkod']   = $filter_szinkod;
        $this->data['sort']             = $sort;
        $this->data['order']            = $order;
        $this->data['token']            = $this->session->data['token'];

        $this->template = 'catalog/option_szin_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function getForm() {
        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $this->data['entry_szinkod'] = $this->language->get('entry_szinkod');
        $this->data['entry_azonosito'] = $this->language->get('entry_azonosito');
        $this->data['entry_szin_group'] = $this->language->get('entry_szin_group');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $this->data['error_name'] = $this->error['name'];
        } else {
            $this->data['error_name'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        if (!isset($this->request->get['option_szin_id'])) {
            $this->data['action'] = $this->url->link('catalog/option_szin/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $this->data['action'] = $this->url->link('catalog/option_szin/update', 'token=' . $this->session->data['token'] . '&option_szin_id=' . $this->request->get['option_szin_id'] . $url, 'SSL');
        }

        $this->data['cancel'] = $this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['option_szin_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $option_szin_info = $this->model_catalog_option_szin->getOptionSzin($this->request->get['option_szin_id']);
        }

        $this->load->model('localisation/language');

        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['option_szin_description'])) {
            $this->data['option_szin_description'] = $this->request->post['option_szin_description'];
        } elseif (isset($this->request->get['option_szin_id'])) {
            $this->data['option_szin_description'] = $this->model_catalog_option_szin->getOptionSzinDescriptions($this->request->get['option_szin_id']);
        } else {
            $this->data['option_szin_description'] = array();
        }

        if (isset($this->request->post['sort_order'])) {
            $this->data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($option_szin_info)) {
            $this->data['sort_order'] = $option_szin_info['sort_order'];
        } else {
            $this->data['sort_order'] = '';
        }

        if (isset($this->request->post['szinkod'])) {
            $this->data['szinkod'] = $this->request->post['szinkod'];
        } elseif (!empty($option_szin_info)) {
            $this->data['szinkod'] = $option_szin_info['szinkod'];
        } else {
            $this->data['szinkod'] = '';
        }

        $this->load->model('catalog/option_szin_group');

        $this->data['groups'] = $this->model_catalog_option_szin_group->getOptionSzinGroups();

        if (isset($this->request->post['szin_group'])) {
            $this->data['szin_group'] = $this->request->post['szin_group'];
        } elseif (isset($this->request->get['option_szin_id'])) {
            $this->data['szin_group'] = $this->model_catalog_option_szin->getSzinGroups($this->request->get['option_szin_id']);
        } else {
            $this->data['szin_group'] = array(0);
        }



        if (isset($this->request->post['azonosito'])) {
            $this->data['azonosito'] = $this->request->post['azonosito'];
        } elseif (!empty($option_szin_info)) {
            $this->data['azonosito'] = $option_szin_info['azonosito'];
        } else {
            $this->data['azonosito'] = '';
        }

        $this->template = 'catalog/option_szin_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/option_szin')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if ($this->config->get("megjelenit_admin_nyelvi_ellenorzes") == 1){

            foreach ($this->request->post['option_szin_description'] as $language_id => $value) {
                if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 64)  && $language_id == $this->config->get('config_language_id')) {
                    $this->error['name'][$language_id] = $this->language->get('error_name');
                }
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    private function validateDelete() {

        return true;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_szin']) || isset($this->request->get['filter_szinkod'])  ) {
            $this->load->model('catalog/option_szin');



            if (isset($this->request->get['filter_szin'])) {
                $filter_szin = $this->request->get['filter_szin'];
            } else {
                $filter_szin = '';
            }

            if (isset($this->request->get['filter_szinkod'])) {
                $filter_szinkod = $this->request->get['filter_szinkod'];
            } else {
                $filter_szinkod = '';
            }



            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 20;
            }

            $data = array(
                'filter_szin'         => $filter_szin,
                'filter_szinkod'        => $filter_szinkod,
                'start'               => 0,
                'limit'               => $limit
            );




            $results = $this->model_catalog_option_szin->getOptionSzinek($data);

            foreach ($results as $key=>$result) {

                $name = strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'));
                $name = str_replace("&nbsp;","",$name);
                $name = str_replace("\r","",$name);
                $name = str_replace("\t","",$name);
                $name = str_replace("\n","",$name);
                $name = trim($name);
                $name =  strlen($name) > 100 ? mb_substr($name, 0, 100, "UTF-8").'...' : $name;


                $json[] = array(
                    'option_szin_id' => $result['option_szin_id'],
                    'name'       => $name,
                    'szinkod'      => $result['szinkod']
                );
            }
        }

        $this->response->setOutput(json_encode($json));
    }
}
?>