<?php
class ModelAccountSale extends Model {

	public function getOrders($data = array()) {
	    $cOrderID = $this->getcustormOrderID();
		$sql = "SELECT  o.order_id,
		                CONCAT(o.firstname, ' ', o.lastname) AS customer,
		                (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status,
		                o.total,
		                o.currency_code,
		                o.currency_value,
		                o.date_added,
		                o.date_modified
		       FROM `" . DB_PREFIX . "order` o";



        if (isset($data['filter_order_status_id']) && !is_null($data['filter_order_status_id'])) {
            $sql .= " WHERE o.order_id IN (".$cOrderID.") AND o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
        } else {
            $sql .= " WHERE o.order_id IN (".$cOrderID.") AND o.order_status_id > '0'";
        }

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}
		
		if (!empty($data['filter_total'])) {
			$sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
		}

		$sort_data = array(
			'o.order_id',
			'customer',
			'status',
			'o.date_added',
			'o.date_modified',
			'o.total'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY o.order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getProductName($id,$cProduct){
	
	 $query = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "order_product` WHERE order_id =".$id. " AND product_id IN (".$cProduct.")") ;
	 $proId =  $query->row['product_id'];
     $language_id = $this->config->get('config_language_id');
	 
	 $query = $this->db->query("SELECT name FROM `" . DB_PREFIX . "product_description` WHERE product_id ='".(int)$proId."' AND language_id = '" .$language_id. "'");
     if ($query->row) {
	    return $query->row['name'];
     } else  {
         return '';
     }
	
	}
	public function getTotalOrders($data = array()) {
        $cOrderID = $this->getcustormOrderID();
      //	$sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order`";
        $sql = "SELECT COUNT(*) AS total  FROM `" . DB_PREFIX . "order` o";

        if (isset($data['filter_order_status_id']) && !is_null($data['filter_order_status_id'])) {
            $sql .= " WHERE o.order_id IN (".$cOrderID.") AND o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
        } else {
            $sql .= " WHERE o.order_id IN (".$cOrderID.") AND o.order_status_id > '0'";
        }

        if (!empty($data['filter_order_id'])) {
            $sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
        }

        if (!empty($data['filter_customer'])) {
            $sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (!empty($data['filter_date_modified'])) {
            $sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
        }

        if (!empty($data['filter_total'])) {
            $sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
        }
		/*if (isset($data['filter_order_status_id']) && !is_null($data['filter_order_status_id'])) {
			$sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE order_status_id > '0'";
		}
        $sql .= " AND order_id = '" . (int)$cOrderID . "'";


		if (!empty($data['filter_order_id'])) {
			$sql .= " AND order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(firstname, ' ', lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}
		
		if (!empty($data['filter_total'])) {
			$sql .= " AND total = '" . (float)$data['filter_total'] . "'";
		}*/

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

   function getcustormProduct(){
       $proId = array();

       $query = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product_customer` WHERE customer_id =".(int)$this->customer->getId()) ;
       if ($query->num_rows > 0){
           foreach($query->rows as $value) {
               $proId[]= $value['product_id'];
           }
           $proids = implode(',',$proId);
       } else {
           $proids = 0;
       }

       return $proids;
   }

   function getcustormOrderID(){
    $proId = array();
	$ordID = array();
	$query = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product_customer` WHERE customer_id =".(int)$this->customer->getId()) ;
	 
	if($query->rows){
         for($i=0;$i<count($query->rows);$i++){
            $proId[]= $query->rows[$i]['product_id'];
         }
         $proids = implode(',',$proId);
	}else{
	    $proids = 0;
	}
	$query = $this->db->query("SELECT order_id FROM `" . DB_PREFIX . "order_product` WHERE product_id IN(".$proids.")");
	if($query->rows){
        for($i=0;$i<count($query->rows);$i++){
         $ordID[]= $query->rows[$i]['order_id'];
	    }
	    $ordIDs = implode(',',$ordID);
	} else {
	    $ordIDs = 0;
	}

	return $ordIDs;
   }
	
}
?>