<?php
class Cart {
    private $data = array();
    private $megrendelem = array();

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->customer = $registry->get('customer');
        $this->session = $registry->get('session');
        $this->db = $registry->get('db');
        $this->tax = $registry->get('tax');
        $this->weight = $registry->get('weight');
        $this->currency = $registry->get('currency');

        if (!isset($this->session->data['cart']) || !is_array($this->session->data['cart'])) {
            $this->session->data['cart'] = array();
        }
    }

    public function getProducts($letoltes=false, $termekek = false, $artol_arig_szuro = false, $kosar='cart') {
        if ( ($kosar=='cart' && !$this->data) || ($kosar=='megrendelem' && !$this->megrendelem) || $kosar='arajanlat') {

            if ($termekek) {
                $product_leker = $termekek;
                if ($artol_arig_szuro) {
                    $termek_vissza['also'] = array(
                        'product_id'            => false,
                        'price'                 => 0
                    );
                    $termek_vissza['felso'] = array(
                        'product_id'            => false,
                        'price'                 => 0
                    );
                }

            } else {
                $product_leker = !empty($this->session->data[$kosar]) ? $this->session->data[$kosar] : array();
            }

            foreach ($product_leker as $key => $quantity) {
                $mehet = true;
                if ($letoltes) {
                    $query_letolt = $this->db->query("SELECT utalvany FROM " . DB_PREFIX . "product WHERE product_id = '" .$key. "'");
                    if ($query_letolt->row['utalvany'] == 0) {
                        $mehet = false;
                    }
                }
                if ($mehet) {
                    unset($special_price);
                    $product = explode(':', $key);
                    $product_id = $product[0];
                    $stock = true;

                    // Options
                    if (isset($product[1])) {
                        $options = unserialize(base64_decode($product[1]));
                    } else {
                        $options = array();
                    }

                    $product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p
                        LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
                        WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");

                    if ($product_query->num_rows) {
                        $option_price = 0;
                        $option_points = 0;
                        $option_weight = 0;

                        $option_data = array();

                        if (isset($mennyiseg) && $mennyiseg > 0) {
                        } else {
                            $mennyiseg = 0;
                            foreach ($this->session->data[$kosar] as $key_2 => $quantity_2) {
                                $product_2 = explode(':', $key_2);

                                if ($product_2[0] == $product_id) {
                                    $mennyiseg += $quantity_2;
                                }
                            }
                        }
                        //$price = $this->productPrice($product_id, $mennyiseg);
                        $price = $this->productPrice($product_id, $quantity);

                        $option_price_with_quantity = 0;
                        foreach ($options as $product_option_id => $option_value) {
                            $option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type, po.required FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                            if ($option_query->num_rows) {
                                if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
                                    $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$option_value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                                    if ($option_value_query->num_rows) {
                                        if ($option_value_query->row['price_prefix'] == '+') {
                                            $option_price += $option_value_query->row['price'];
                                        } elseif ($option_value_query->row['price_prefix'] == '-') {
                                            $option_price -= $option_value_query->row['price'];
                                        }

                                        if ($option_value_query->row['points_prefix'] == '+') {
                                            $option_points += $option_value_query->row['points'];
                                        } elseif ($option_value_query->row['points_prefix'] == '-') {
                                            $option_points -= $option_value_query->row['points'];
                                        }

                                        if ($option_value_query->row['weight_prefix'] == '+') {
                                            $option_weight += $option_value_query->row['weight'];
                                        } elseif ($option_value_query->row['weight_prefix'] == '-') {
                                            $option_weight -= $option_value_query->row['weight'];
                                        }

                                        if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
                                            $stock = false;
                                        }

                                        $option_data[] = array(
                                            'product_option_id'       => $product_option_id,
                                            'product_option_value_id' => $option_value,
                                            'option_id'               => $option_query->row['option_id'],
                                            'option_value_id'         => $option_value_query->row['option_value_id'],
                                            'name'                    => $option_query->row['name'],
                                            'option_value'            => $option_value_query->row['name'],
                                            'type'                    => $option_query->row['type'],
                                            'quantity'                => $option_value_query->row['quantity'],
                                            'subtract'                => $option_value_query->row['subtract'],
                                            'price'                   => $option_value_query->row['price'],
                                            'price_prefix'            => $option_value_query->row['price_prefix'],
                                            'points'                  => $option_value_query->row['points'],
                                            'points_prefix'           => $option_value_query->row['points_prefix'],
                                            'weight'                  => $option_value_query->row['weight'],
                                            'weight_prefix'           => $option_value_query->row['weight_prefix'],
                                            'required'                => $option_query->row['required']
                                        );
                                    }
                                } elseif ($option_query->row['type'] == 'checkbox' && is_array($option_value)) {
                                    foreach ($option_value as $product_option_value_id) {
                                        $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight,
                                            pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov
                                            LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id)
                                            LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id)
                                            WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                                        if ($option_value_query->num_rows) {
                                            if ($option_value_query->row['price_prefix'] == '+') {
                                                $option_price += $option_value_query->row['price'];
                                            } elseif ($option_value_query->row['price_prefix'] == '-') {
                                                $option_price -= $option_value_query->row['price'];
                                            }

                                            if ($option_value_query->row['points_prefix'] == '+') {
                                                $option_points += $option_value_query->row['points'];
                                            } elseif ($option_value_query->row['points_prefix'] == '-') {
                                                $option_points -= $option_value_query->row['points'];
                                            }

                                            if ($option_value_query->row['weight_prefix'] == '+') {
                                                $option_weight += $option_value_query->row['weight'];
                                            } elseif ($option_value_query->row['weight_prefix'] == '-') {
                                                $option_weight -= $option_value_query->row['weight'];
                                            }

                                            if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
                                                $stock = false;
                                            }

                                            $option_data[] = array(
                                                'product_option_id'       => $product_option_id,
                                                'product_option_value_id' => $product_option_value_id,
                                                'option_id'               => $option_query->row['option_id'],
                                                'option_value_id'         => $option_value_query->row['option_value_id'],
                                                'name'                    => $option_query->row['name'],
                                                'option_value'            => $option_value_query->row['name'],
                                                'type'                    => $option_query->row['type'],
                                                'quantity'                => $option_value_query->row['quantity'],
                                                'subtract'                => $option_value_query->row['subtract'],
                                                'price'                   => $option_value_query->row['price'],
                                                'price_prefix'            => $option_value_query->row['price_prefix'],
                                                'points'                  => $option_value_query->row['points'],
                                                'points_prefix'           => $option_value_query->row['points_prefix'],
                                                'weight'                  => $option_value_query->row['weight'],
                                                'weight_prefix'           => $option_value_query->row['weight_prefix'],
                                                'required'                => $option_query->row['required']

                                            );
                                        }
                                    }
                                } elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
                                    $option_data[] = array(
                                        'product_option_id'       => $product_option_id,
                                        'product_option_value_id' => '',
                                        'option_id'               => $option_query->row['option_id'],
                                        'option_value_id'         => '',
                                        'name'                    => $option_query->row['name'],
                                        'option_value'            => $option_value,
                                        'type'                    => $option_query->row['type'],
                                        'quantity'                => '',
                                        'subtract'                => '',
                                        'price'                   => '',
                                        'price_prefix'            => '',
                                        'points'                  => '',
                                        'points_prefix'           => '',
                                        'weight'                  => '',
                                        'weight_prefix'           => '',
                                        'required'                => $option_query->row['required']

                                    );


                                } elseif ($option_query->row['type'] == 'checkbox_qty') {


                                    foreach ($option_value as $product_option_value_id) {
                                        if ( empty($product_option_value_id) )
                                            continue;

                                        $option_value_arr = explode('|', $product_option_value_id);
                                        $product_option_value_id = $option_value_arr[0];
                                        $option_value_quantity = $option_value_arr[1];

                                        $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix,
                                            pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov
                                                LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id)
                                                LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id)
                                                WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "'
                                                AND pov.product_option_id = '" . (int)$product_option_id . "'
                                                AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                                        if ( ($option_value_query->num_rows) && (!empty($option_value_quantity)) ) {

                                            $option_price_with_quantity = isset($option_price_with_quantity) ? $option_price_with_quantity : 0;


                                            if ($option_value_query->row['price_prefix'] == '-') {
                                                $kedvezmenyes_ar = $option_value_query->row['price'];
                                            } else {
                                                $kedvezmenyes_ar = $this->optionKedvezmeny($option_value_query->row['price'],$product_id,$price);
                                            }

                                            if ($option_value_query->row['price_prefix'] == '-') {
                                                $kedvezmenyes_ar = (-1)*$kedvezmenyes_ar;
                                            }

                                            //if ($option_value_query->row['price_prefix'] == '+') {
                                              //  $option_price_with_quantity += ($kedvezmenyes_ar+$price) * $option_value_quantity;
                                            //} else if ($option_value_query->row['price_prefix'] == '-') {
                                                $option_price_with_quantity += ($kedvezmenyes_ar+$price) * $option_value_quantity;
                                            //}

                                            $subtract = $option_value_query->row['subtract'];
                                            if ($subtract > 0) {
                                                $subtract = $option_value_quantity;
                                            }

                                            if ($subtract && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity*$option_value_quantity))) {
                                                $stock = false;
                                            }


                                            $option_data[] = array(
                                                'product_option_id'       => $product_option_id,
                                                'product_option_value_id' => $product_option_value_id,
                                                'option_id'               => $option_query->row['option_id'],
                                                'option_value_id'         => $option_value_query->row['option_value_id'],
                                                'name'                    => $option_query->row['name'],
                                                'option_value'            => $option_value_query->row['name']." x ".$option_value_quantity." - ".$this->currency->format(($kedvezmenyes_ar+$price) * $option_value_quantity),
                                                'type'                    => $option_query->row['type'],
                                                'quantity'                => $option_value_query->row['quantity'],
                                                'subtract'                => $subtract,
                                                'price'                   => $option_value_query->row['price'],
                                                'price_prefix'            => $option_value_query->row['price_prefix'],
                                                'points'                  => $option_value_query->row['points'],
                                                'points_prefix'           => $option_value_query->row['points_prefix'],
                                                'weight'                  => $option_value_query->row['weight'],
                                                'weight_prefix'           => $option_value_query->row['weight_prefix'],
                                                'required'                => $option_query->row['required']

                                            );
                                        }
                                    }
                                }
                            }
                        }

                        if ($this->customer->isLogged()) {
                            $customer_group_id = $this->customer->getCustomerGroupId();
                        } else {
                            $customer_group_id = $this->config->get('config_customer_group_id');
                        }


                        // Reward Points
                        $product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "'");

                        if ($product_reward_query->num_rows) {
                            $reward = $product_reward_query->row['points'];
                        } else {
                            $reward = 0;
                        }

                        // Downloads

                        $download_data = array();

                        $download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$product_id . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                        foreach ($download_query->rows as $download) {
                            $download_data[] = array(
                                'download_id' => $download['download_id'],
                                'name'        => $download['name'],
                                'filename'    => $download['filename'],
                                'mask'        => $download['mask'],
                                'remaining'   => $download['remaining']
                            );
                        }

                        // Stock
                        if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $quantity)) {
                            $stock = false;
                        }

                        if ($termekek) {
                            if ($artol_arig_szuro) {

                                if ( !$termek_vissza['also']['product_id']) {
                                    $termek_vissza['also'] = array(
                                        'product_id'            => $product_query->row['product_id'],
                                        'price'                 => ($price + $option_price),
                                        'tax_class_id'          => $product_query->row['tax_class_id'],

                                    );
                                } elseif (($price + $option_price) <  $termek_vissza['also']['price']) {
                                    $termek_vissza['also'] = array(
                                        'product_id'            => $product_query->row['product_id'],
                                        'price'                 => ($price + $option_price),
                                        'tax_class_id'          => $product_query->row['tax_class_id'],

                                    );
                                }

                                if ( !$termek_vissza['felso']['product_id']) {
                                    $termek_vissza['felso'] = array(
                                        'product_id'            => $product_query->row['product_id'],
                                        'price'                 => ($price + $option_price),
                                        'tax_class_id'          => $product_query->row['tax_class_id'],

                                    );
                                } elseif (($price + $option_price) >  $termek_vissza['felso']['price']) {
                                    $termek_vissza['felso'] = array(
                                        'product_id'            => $product_query->row['product_id'],
                                        'price'                 => ($price + $option_price),
                                        'tax_class_id'          => $product_query->row['tax_class_id'],

                                    );
                                }


                            }
                        } else {
                            if ($kosar == "megrendelem") {
                                $this->megrendelem[$key] = array(
                                    'key'                   => $key,
                                    'product_id'            => $product_query->row['product_id'],
                                    'name'                  => $product_query->row['name'],
                                    'model'                 => $product_query->row['model'],
                                    'cikkszam'              => $product_query->row['cikkszam'],
                                    'cikkszam2'             => $product_query->row['cikkszam2'],
                                    'manufacturer_id'       => $product_query->row['manufacturer_id'],
                                    'egyedi_szallitas'      => $product_query->row['egyedi_szallitas'],
                                    'egyedi_szallitasi_dij' => $product_query->row['egyedi_szallitasi_dij'],
                                    'shipping'              => $product_query->row['shipping'],
                                    'utalvany'              => $product_query->row['utalvany'],
                                    'image'                 => $product_query->row['image'],
                                    'letoltheto'            => $product_query->row['letoltheto'],
                                    'option'                => $option_data,
                                    'download'              => $download_data,
                                    'quantity'              => $quantity,
                                    'minimum'               => $product_query->row['minimum'],
                                    'subtract'              => $product_query->row['subtract'],
                                    'stock'                 => $stock,
                                    'price'                 => ( (isset($option_price_with_quantity) && $option_price_with_quantity ? $option_price_with_quantity+ $option_price : $price+ $option_price) ),
                                    'total'                 => ( (isset($option_price_with_quantity) && $option_price_with_quantity ? $option_price_with_quantity+$option_price : ($price+$option_price)*$quantity) ),
                                    'reward'                => $reward * $quantity,
                                    'points'                => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $quantity : 0),
                                    'tax_class_id'          => $product_query->row['tax_class_id'],
                                    'weight'                => ($product_query->row['weight'] + $option_weight) * $quantity,
                                    'weight_class_id'       => $product_query->row['weight_class_id'],
                                    'length'                => $product_query->row['length'],
                                    'width'                 => $product_query->row['width'],
                                    'height'                => $product_query->row['height'],
                                    'length_class_id'       => $product_query->row['length_class_id'],
                                    'szin_meret_szukseges'  => $product_query->row['szin_meret_szukseges']
                                );

                            } else {
                                $this->data[$key] = array(
                                    'key'                   => $key,
                                    'product_id'            => $product_query->row['product_id'],
                                    'name'                  => $product_query->row['name'],
                                    'model'                 => $product_query->row['model'],
                                    'cikkszam'              => $product_query->row['cikkszam'],
                                    'cikkszam2'             => $product_query->row['cikkszam2'],
                                    'manufacturer_id'       => $product_query->row['manufacturer_id'],
                                    'egyedi_szallitas'      => $product_query->row['egyedi_szallitas'],
                                    'egyedi_szallitasi_dij' => $product_query->row['egyedi_szallitasi_dij'],
                                    'shipping'              => $product_query->row['shipping'],
                                    'utalvany'              => $product_query->row['utalvany'],
                                    'image'                 => $product_query->row['image'],
                                    'letoltheto'            => $product_query->row['letoltheto'],
                                    'option'                => $option_data,
                                    'download'              => $download_data,
                                    'quantity'              => $quantity,
                                    'minimum'               => $product_query->row['minimum'],
                                    'subtract'              => $product_query->row['subtract'],
                                    'stock'                 => $stock,
                                    'price'                 => ( (isset($option_price_with_quantity) && $option_price_with_quantity ? $option_price_with_quantity+ $option_price : $price+ $option_price) ),
                                    'total'                 => ( (isset($option_price_with_quantity) && $option_price_with_quantity ? $option_price_with_quantity+$option_price : ($price+$option_price)*$quantity) ),
                                    'reward'                => $reward * $quantity,
                                    'points'                => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $quantity : 0),
                                    'tax_class_id'          => $product_query->row['tax_class_id'],
                                    'weight'                => ($product_query->row['weight'] + $option_weight) * $quantity,
                                    'weight_class_id'       => $product_query->row['weight_class_id'],
                                    'length'                => $product_query->row['length'],
                                    'width'                 => $product_query->row['width'],
                                    'height'                => $product_query->row['height'],
                                    'length_class_id'       => $product_query->row['length_class_id'],
                                    'szin_meret_szukseges'  => $product_query->row['szin_meret_szukseges']
                                );
                            }
                        }
                    } else {
                        $this->remove($key);
                    }
                }
            }
        }

        if ($termekek) {
            if ($artol_arig_szuro) {
                $vissza[0]    =  round($this->tax->calculate( $termek_vissza['also']['price'],  $termek_vissza['also']['tax_class_id'], $this->config->get('config_tax') ),0);
                $vissza[1]    =  round($this->tax->calculate( $termek_vissza['felso']['price'], $termek_vissza['also']['tax_class_id'], $this->config->get('config_tax') ),0);
            }
            return $vissza;
        } else {
            if ($kosar == "megrendelem") {
                return $this->megrendelem;
            } else {
                return $this->data;
            }
        }
    }

    public function add($product_id, $qty = 1, $option = array()) {
        if (!$option) {
            $key = (int)$product_id;
        } else {
            $key = (int)$product_id . ':' . base64_encode(serialize($option));
        }

        if ((float)$qty && ((float)$qty > 0)) {
            if (!isset($this->session->data['cart'][$key])) {
                $this->session->data['cart'][$key] = (float)$qty;
            } else {
                $this->session->data['cart'][$key] += (float)$qty;
            }
        }

        $this->data = array();
    }

    public function addMegrendelem($product_id, $qty = 1, $option = array()) {
        if (!$option) {
            $key = (int)$product_id;
        } else {
            $key = (int)$product_id . ':' . base64_encode(serialize($option));
        }

        if ((int)$qty && ((int)$qty > 0)) {
            if (!isset($this->session->data['megrendelem'][$key])) {
                $this->session->data['megrendelem'][$key] = (int)$qty;
            } else {
                $this->session->data['megrendelem'][$key] += (int)$qty;
            }
        }
        $this->data = array();
    }



    public function update($key, $qty) {
        if ((int)$qty && ((int)$qty > 0)) {
            $this->session->data['cart'][$key] = (int)$qty;
        } else {
            $this->remove($key);
        }

        $this->data = array();
    }

    public function remove($key) {
        if (isset($this->session->data['cart'][$key])) {
            unset($this->session->data['cart'][$key]);
        }

        $this->data = array();
    }



    public function clear() {
        $this->session->data['cart'] = array();
        $this->data = array();
    }

    public function clearMegrendelem() {
        $this->session->data['megrendelem'] = array();
        $this->megrendelem = array();
    }

    public function getWeight() {
        $weight = 0;

        foreach ($this->getProducts() as $product) {
            if ($product['shipping']) {
                $weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
            }
        }

        return $weight;
    }

    public function getSubTotal($kosar="cart") {
        $total = 0;

        foreach ($this->getProducts(false,false,false,$kosar) as $product) {
            if ($product['utalvany'] != 1) {
                $total += $product['total'];
            }
        }

        return $total;
    }

    public function getSubTotalProductShipping($kosar="cart") {
        $total = 0;

        foreach ($this->getProducts(false,false,false,$kosar) as $product) {
            if ($product['utalvany'] != 1) {
                $total += $product['total'];
            }
        }
        if (isset($this->session->data['shipping_method']['cost'])) {

            $total += $this->session->data['shipping_method']['cost'];
        }

        return $total;
    }

    public function getTaxes($kosar="cart") {
        $tax_data = array();

        foreach ($this->getProducts(false,false,false,$kosar) as $product) {
            if($product['utalvany'] == "1") {
                continue;
            }
            if ($product['tax_class_id']) {
                $tax_rates = $this->tax->getRates($product['total'], $product['tax_class_id']);

                foreach ($tax_rates as $tax_rate) {
                    $amount = 0;

                    if ($tax_rate['type'] == 'F') {
                        $amount = ($tax_rate['amount'] * $product['quantity']);
                    } elseif ($tax_rate['type'] == 'P') {
                        $amount = $tax_rate['amount'];
                    }

                    if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
                        $tax_data[$tax_rate['tax_rate_id']] = $amount;
                    } else {
                        $tax_data[$tax_rate['tax_rate_id']] += $amount;
                    }
                }
            }
        }

        return $tax_data;
    }


    public function getTotal() {
        $total = 0;

        foreach ($this->getProducts() as $product) {
            $total += $this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'));
        }

        return $total;
    }

    public function countProducts() {
        $product_total = 0;

        $products = $this->getProducts();

        foreach ($products as $product) {
            $product_total += $product['quantity'];
        }

        return $product_total;
    }

    public function countProduct($product_id) {

        $quantity = 0;
        foreach($this->session->data['cart'] as $key=>$cart) {
            $tartalom = explode(':',$key);
            if ($tartalom[0] == $product_id) {
                $quantity += $cart;
            }
        }
        return $quantity;
    }


    public function getProductShipping() {
        $products = $this->getProducts();
        $egyedi_szallitasi_dij = false;
        foreach ($products as $product) {
            if ($product['egyedi_szallitas'] == 1 ) {
                if (!$egyedi_szallitasi_dij) {
                    $egyedi_szallitasi_dij = $product['egyedi_szallitasi_dij'];
                } else {
                    if ($this->config->get('item_product_magas_alacsony') == 1 || $this->config->get('item_product_gls_magas_alacsony') == 1) {
                        if ( $product['egyedi_szallitasi_dij'] > $egyedi_szallitasi_dij) {
                            $egyedi_szallitasi_dij = $product['egyedi_szallitasi_dij'];
                        }
                    } else {
                        if ( $product['egyedi_szallitasi_dij'] < $egyedi_szallitasi_dij) {
                            $egyedi_szallitasi_dij = $product['egyedi_szallitasi_dij'];
                        }
                    }
                }
            }
        }
        return $egyedi_szallitasi_dij;
    }

    public function hasProducts($kosar="cart") {
        return count($this->session->data[$kosar]);
    }

    public function hasStock() {
        $stock = true;

        foreach ($this->getProducts() as $product) {
            if (!$product['stock']) {
                $stock = false;
            }
        }

        return $stock;
    }

    public function hasShipping($kosar="cart") {
        $shipping = false;

        foreach ($this->getProducts(false,false,false,$kosar) as $product) {
            if ($product['shipping']) {
                $shipping = true;

                break;
            }
        }

        return $shipping;
    }

    public function hasPayment() {
        $payment = false;

        foreach ($this->getProducts() as $product) {
            if ($product['utalvany'] == 0 ) {
                $payment = true;

                break;
            }
        }

        return $payment;
    }

    public function hasDownload() {
        $download = false;

        foreach ($this->getProducts() as $product) {
            if ($product['download']) {
                $download = true;

                break;
            }
        }

        return $download;
    }

    public function productPrice($product_id, $quantity = 1, $szamolhat = true) {

        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        // price
        $price = 0;
        $query = $this->db->query("SELECT price, max_szazalek_kedvezmeny FROM " . DB_PREFIX . "product WHERE product_id = '" .(int)$product_id."'");
        if ($query->num_rows ) {
            $price = $query->row['price'];

            $megjelenit_admin = $this->config->get('megjelenit_admin_termekadatok');
            if ($megjelenit_admin['max_szazalek_kedvezmeny'] == 1) {
                if ($query->row['max_szazalek_kedvezmeny'] > 0) {
                    $min_price = $query->row['price'] * (100 - $query->row['max_szazalek_kedvezmeny']) / 100;
                } else {
                    $min_price = $query->row['price'];
                }
            } else {
                $min_price = 0;
            }
        }

        // Mennyiségi kedvezmény
        $discount_price = $price;
        $query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE
                                                                        product_id = '" . (int)$product_id . "'
                                                                        AND customer_group_id = '" . (int)$customer_group_id . "'
                                                                        AND quantity <= '" . (int)$quantity . "'
                                                                        AND ((date_start = '0000-00-00' OR date_start < NOW())
                                                                        AND (date_end = '0000-00-00' OR date_end > NOW()))

                                                                    ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");
        if ($query->num_rows) {
            $discount_price = $query->row['price'];
        }

        //Személyes kedvezmény
        $szemelyes_ar = $price;
        if($this->customer->isLogged()){
            $sql="SELECT price FROM " . DB_PREFIX . "product_customer_special WHERE
                    customer_id=".$this->customer->isLogged(). "
                    AND product_id='".(int)$product_id."'
                    AND (date_start <= NOW() OR date_start='0000-00-00')
                    AND (date_end >= NOW() OR date_end='0000-00-00')
                    AND quantity <=" .$quantity. "
                    ORDER BY price ASC LIMIT 1 ";

            $query=$this->db->query($sql);
            if($query->num_rows>0){
                $szemelyes_ar = $query->row['price'];
            }
        }


        // Product Specials
        $special_price = $price;
        $sql = "SELECT price FROM " . DB_PREFIX . "product_special
                WHERE product_id = '" . (int)$product_id . "'
                AND customer_group_id = '" . (int)$customer_group_id . "'
                AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW()))
                ORDER BY priority ASC, price ASC LIMIT 1";
        $query = $this->db->query($sql);

        if ($query->num_rows) {
            $special_price = $query->row['price'];
        }



        //Ha a partnernek van általános kedvezménye
        $partner_engedmenyes_ar = $price;
        if($customer=$this->customer->isLogged()) {
            $sql="SELECT szazalek FROM " . DB_PREFIX . "customer WHERE customer_id=".$customer;
            $query=$this->db->query($sql);
            if ($query->num_rows > 0) {
                $partner_engedmenyes_ar = ( (100-$query->row['szazalek'])/100 ) * $price;
                $partner_engedmenyes_ar = $partner_engedmenyes_ar < $min_price ? $min_price: $partner_engedmenyes_ar;

            }
        }

        if ( (isset( $this->session->data['ar_atszamol']) && $this->session->data['ar_atszamol'] > 0) && $szamolhat) {
            return min($discount_price, $special_price, ($price*(100-$this->session->data['ar_atszamol'])/100) );
        }

        return min($partner_engedmenyes_ar, $special_price, $szemelyes_ar, $discount_price, $price);
    }

    public function optionKedvezmeny($price,$product_id) {
        $partner_engedmenyes_ar = $price;

        if (isset( $this->session->data['ar_atszamol']) && $this->session->data['ar_atszamol'] > 0) {
            $query = $this->db->query("SELECT price, max_szazalek_kedvezmeny FROM " . DB_PREFIX . "product WHERE product_id = '" .(int)$product_id."'");
            $alap_ar    = $query->row['price'];
            $special_ar = $this->productPrice($product_id,1,false);

            if ($special_ar+$price < ($alap_ar + $price)*(100-$this->session->data['ar_atszamol'])/100) {
                return $price;

            } else {
                return $price*(100-$this->session->data['ar_atszamol'])/100;
            }
        }

        if($customer=$this->customer->isLogged()) {
            $sql="SELECT szazalek FROM " . DB_PREFIX . "customer WHERE customer_id=".$customer;
            $query=$this->db->query($sql);
            if ($query->num_rows > 0) {
                $partner_szazalek = $query->row['szazalek'];

                $query = $this->db->query("SELECT price, max_szazalek_kedvezmeny FROM " . DB_PREFIX . "product WHERE product_id = '" .(int)$product_id."'");
                if ($query->num_rows ) {
                    $megjelenit_admin = $this->config->get('megjelenit_admin_termekadatok');
                    if ($megjelenit_admin['max_szazalek_kedvezmeny'] == 1) {
                        if ($query->row['max_szazalek_kedvezmeny'] > 0) {
                            //$min_price = $query->row['price'] * (100 - $query->row['max_szazalek_kedvezmeny']) / 100;
                            $min_price = $price * (100 - $query->row['max_szazalek_kedvezmeny']) / 100;
                        } else {
                            //$min_price = $query->row['price'];
                            $min_price = $price;
                        }
                    } else {
                        $min_price = 0;
                    }
                }
                $partner_engedmenyes_ar = ( (100-$partner_szazalek)/100 ) * $price;
                $partner_engedmenyes_ar = $partner_engedmenyes_ar < $min_price ? $min_price: $partner_engedmenyes_ar;
            }
        }
        return $partner_engedmenyes_ar;
    }

    public function getProductsCartForUpsell() {
        $products = $this->session->data['cart'];
        $vissza = array();
        foreach($products as $key=>$product) {
            $vissza[$key] = $key;
        }

        if (isset($_REQUEST['product_id']) && $_REQUEST['product_id']) {
            $vissza[$_REQUEST['product_id']] = $_REQUEST['product_id'];
        }



        return $vissza ? " AND p.product_id NOT IN (".implode(",",$vissza).") " : '';

    }

    public function getSqlMinimumPriceRow() {

        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $sql = ", p.price,
            if (p.max_szazalek_kedvezmeny > 0, round( (100-p.max_szazalek_kedvezmeny)/100*p.price),0) AS min_ar, LEAST(p.price,

            IFNULL((SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id
                    AND ps.customer_group_id = '" . (int)$customer_group_id . "'
                    AND ((ps.date_start = '0000-00-00' OR ps.date_start <= curdate()) AND (ps.date_end = '0000-00-00' OR ps.date_end >= curdate()))
                ORDER BY ps.priority ASC, ps.price ASC LIMIT 1),p.price),

            IFNULL((SELECT price FROM " . DB_PREFIX . "product_discount pd WHERE pd.product_id = p.product_id
                    AND pd.customer_group_id = '" . (int)$customer_group_id . "'
                    AND ((pd.date_start = '0000-00-00' OR pd.date_start <= curdate()) AND (pd.date_end = '0000-00-00' OR pd.date_end >= curdate()))
                    AND pd.quantity <= 1
                ORDER BY pd.priority DESC, pd.price ASC LIMIT 1),p.price)";

            if ( $this->customer->isLogged()) {

                $sql .= ", IFNULL((SELECT price FROM " . DB_PREFIX . "product_customer_special pcs WHERE pcs.product_id = p.product_id
                            AND pcs.customer_id = '" . $this->customer->isLogged() . "'
                            AND ((pcs.date_start = '0000-00-00' OR pcs.date_start <= curdate()) AND (pcs.date_end = '0000-00-00' OR pcs.date_end >= curdate()))
                            AND pcs.quantity <= 1
                        ORDER BY  pcs.price ASC LIMIT 1),p.price),

                        (SELECT if (round((100-szazalek)/100 * p.price) < min_ar,min_ar,round((100-szazalek)/100 * p.price)) FROM " . DB_PREFIX . "customer WHERE customer_id=".$this->customer->isLogged() .")) AS minimum_price ";
            } else {
                $sql .= ") AS minimum_price";
            }

        return $sql;

    }
}
?>