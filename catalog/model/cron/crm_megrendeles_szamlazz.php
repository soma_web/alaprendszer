<?php
class ModelCronCrmMegrendelesSzamlazz extends Model {

	public function getOrders() {
        $vissza = array();
        $sql = "CREATE TABLE if not exists vtiger_salesorder_dijbekero (
                `salesorderid`	            int(19),
                `sostatus`	            varchar(200) )
                engine=MyISAM default charset=UTF8";
        $query = $this->db_crm->query($sql);


        $sql = "SELECT * FROM vtiger_salesorder_dijbekero";
        $query = $this->db_crm->query($sql);

        $vissza = $query->rows;

        return $vissza;
	}

    public function getOrder($salesorderid) {

        $sql = "SELECT * FROM vtiger_salesorder WHERE salesorderid='".$salesorderid."'";
        $query = $this->db_crm->query($sql);
        $vissza = $query->row;

        return $vissza;
	}

    public function getOrderPayment($salesorderid) {

        $sql = "SELECT bill_city, bill_code, bill_country, bill_street  FROM vtiger_sobillads WHERE sobilladdressid   = '" . $salesorderid . "'";
        $query = $this->db_crm->query($sql);
        $vissza = $query->row;

        return $vissza;
    }

    public function getOrderShipping($salesorderid) {

        $sql = "SELECT ship_city, ship_code,ship_country,ship_street FROM vtiger_soshipads WHERE soshipaddressid   = '".$salesorderid."'";
        $query = $this->db_crm->query($sql);
        $vissza = $query->row;
        return $vissza;
    }

    public function getOrderAccount($accountid) {

        if (!empty($accountid)) {

            $sql = "SELECT a.accountname, acf.cf_843 AS adoszam FROM vtiger_account a ";
            $sql .= " LEFT JOIN vtiger_accountscf acf ON (acf.accountid=a.accountid) ";
            $sql .= " WHERE a.accountid   = '" . $accountid . "'";
            $query = $this->db_crm->query($sql);
            if ($query->row) {
                $vissza = $query->row;
            } else {
                $vissza['accountname'] = '';
                $vissza['adoszam'] = '';
            }
        } else {
            $vissza['accountname'] = '';
            $vissza['adoszam'] = '';
        }
        return $vissza;
    }

    public function getOrderProducts($salesorderid) {
        $vissza = array();

        $sql = "SELECT * FROM vtiger_inventoryproductrel WHERE id = '".$salesorderid."'";
        $query = $this->db_crm->query($sql);

        if ($query->rows) {
            foreach($query->rows as $value){
                $sql = "SELECT productname FROM vtiger_products WHERE productid='".$value['productid']."'";
                $query_product = $this->db_crm->query($sql);
                if (empty($query_product->row)) {
                    $sql = "SELECT servicename FROM vtiger_service WHERE serviceid='".$value['productid']."'";
                    $query_product = $this->db_crm->query($sql);
                    if (empty($query_product->row)) {
                        return array();
                    }
                    $value['name'] = $query_product->row['servicename'];
                } else {
                    $value['name'] = $query_product->row['productname'];
                }
                if (!empty($query_product->row)) {
                    $vissza[] = $value;
                }
            }

        } else {
            $vissza = array();
        }

        return $vissza;
    }

    public function getOrderContact($contactid) {
        $vissza = array();
        if (count($this->db_crm)) {
            $sql = "SELECT * FROM vtiger_contactdetails WHERE contactid = '" . $contactid . "'";
            $query = $this->db_crm->query($sql);
            $vissza = $query->row;

        }

        return $vissza;

    }
    public function deleteTriggerOrder($data) {
        $sql = "DELETE FROM vtiger_salesorder_dijbekero
          WHERE salesorderid='".$data['salesorderid']."'
            AND sostatus LIKE '".$data['sostatus']."'";
        $query = $this->db_crm->query($sql);
    }

    public function updateAllapotStatus($data) {
        if (!empty($data['siker'])) {
            $sql = "UPDATE vtiger_salesordercf SET
                  cf_2435 = 'Továbbítva',
                  cf_2496 = '".$data['szamlaszam']."'
                WHERE salesorderid = '".$data['salesorderid']."'";
            $query = $this->db_crm->query($sql);

        } else {
            $sql = "UPDATE vtiger_salesordercf SET cf_2435 = 'Továbbítás sikertelen' WHERE salesorderid = '".$data['salesorderid']."'";
            $query = $this->db_crm->query($sql);
        }
    }


}
?>