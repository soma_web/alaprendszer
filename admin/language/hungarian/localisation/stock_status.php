<?php

// Heading
$_['heading_title']    = 'Hiánycikk állapota';

// Text
$_['text_success']     = 'Sikeresen módosította a hiánycikk állapotát!';

// Column
$_['column_name']      = 'Hiánycikk állapot neve';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_name']       = 'Hiánycikk állapot neve:';

// Error
$_['error_permission'] = 'Figyelmeztetés: A Hiánycikk állapot módosítása az Ön számára nem engedélyezett!';
$_['error_name']       = 'A készletállapot nevének hosszabbnak kell lennie, mint 3 és rövidebbnek, mint 32 karakter!';
$_['error_product']    = 'Figyelmeztetés: Ez a készletállapot nem törölhető, mivel korábban hozzárendelésre került a következő termékekhez: %s!';
?>
