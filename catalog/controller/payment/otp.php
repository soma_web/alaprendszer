<?php

class ControllerPaymentOtp extends Controller {
    protected function index() {
        $this->data['button_confirm'] = $this->language->get('button_confirm');

        $this->data['continue'] = $_GET['route'] == "checkout/confirm_megrendelem" ? $this->url->link('checkout/success&megrendelem=1') : $this->url->link('checkout/success');

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $this->data['action'] = dirname($_SERVER['SCRIPT_NAME']).'/otp/web_demo/fiz3.php';

        $total = explode(".", $order_info['total']);
        $this->data['amt'] = $total[0];

        $this->load->language('payment/otp');

        $this->data['otp_description'] = $this->language->get('otp_description');

        $this->data['utvonal'] = HTTP_SERVER."otp/web_demo/fiz3.php?func=fiz3";

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/otp.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/payment/otp.tpl';
        } else {
            $this->template = 'default/template/payment/otp.tpl';
        }

        $this->render();
    }

    public function confirm() {
        $this->load->model('checkout/order');

        $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('otp_order_status_id'));
    }
}
?>
