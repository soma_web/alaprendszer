<?php
class ControllerModuleMultiFilter extends Controller {
	private $error = array(); 

	public function index() {   
		$this->language->load('module/multifilter');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('multifilter', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['entry_fejlec_latszik'] = $this->language->get('entry_fejlec_latszik');
        $this->data['entry_ajaxos'] 		= $this->language->get('entry_ajaxos');
		$this->data['text_enabled']         = $this->language->get('text_enabled');
		$this->data['text_disabled']        = $this->language->get('text_disabled');
		$this->data['text_content_top']     = $this->language->get('text_content_top');
		$this->data['text_content_bottom']  = $this->language->get('text_content_bottom');
		$this->data['text_column_left']     = $this->language->get('text_column_left');
		$this->data['text_column_right']    = $this->language->get('text_column_right');
		$this->data['text_name']    = $this->language->get('text_name');
		$this->data['text_szin']    = $this->language->get('text_szin');
		$this->data['text_megjelenites']    = $this->language->get('text_megjelenites');
		$this->data['text_megjelenites_szincsoport']    = $this->language->get('text_megjelenites_szincsoport');
		$this->data['text_megjelenites_szinek']    = $this->language->get('text_megjelenites_szinek');

		$this->data['text_select']    	= $this->language->get('text_select');
		$this->data['checkox_tpl']    	= $this->language->get('checkox_tpl');
		$this->data['color_tpl']    	= $this->language->get('color_tpl');
		$this->data['negyzetes_tpl']    = $this->language->get('negyzetes_tpl');

		$this->data['entry_layout']     = $this->language->get('entry_layout');
		$this->data['entry_position']   = $this->language->get('entry_position');
		$this->data['entry_status']     = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_template']   = $this->language->get('entry_template');
		$this->data['entry_teljes_lista']   = $this->language->get('entry_teljes_lista');

		//$this->data['entry_kategoria_vezerel']  = $this->language->get('entry_kategoria_vezerel');
		$this->data['entry_kategoria_checkbox'] = $this->language->get('entry_kategoria_checkbox');
		$this->data['entry_szuro_csoport']      = $this->language->get('entry_szuro_csoport');
		$this->data['entry_szuro']              = $this->language->get('entry_szuro');
		$this->data['entry_gyarto']             = $this->language->get('entry_gyarto');
		$this->data['entry_meret']              = $this->language->get('entry_meret');
		$this->data['entry_szin']              = $this->language->get('entry_szin');
		$this->data['entry_tulajdonsagok']      = $this->language->get('entry_tulajdonsagok');
		$this->data['entry_valasztek']          = $this->language->get('entry_valasztek');
		$this->data['entry_raktaron']           = $this->language->get('entry_raktaron');
		$this->data['entry_ar_szures']          = $this->language->get('entry_ar_szures');
		$this->data['entry_ar_szures_szazalek'] = $this->language->get('entry_ar_szures_szazalek');
		$this->data['entry_kategoria_normal']   = $this->language->get('entry_kategoria_normal');
		$this->data['entry_kategoria_sub']      = $this->language->get('entry_kategoria_sub');
		$this->data['entry_ar_szures_tol_ig']   = $this->language->get('entry_ar_szures_tol_ig');
		$this->data['entry_szuresgomb_latszik'] = $this->language->get('entry_szuresgomb_latszik');
		$this->data['entry_kereses_latszik']    = $this->language->get('entry_kereses_latszik');
		$this->data['entry_szurok_torlese']     = $this->language->get('entry_szurok_torlese');

		$this->data['entry_szuro_neve']         = $this->language->get('entry_szuro_neve');
		$this->data['text_szukit']              = $this->language->get('text_szukit');
		$this->data['text_bovit']               = $this->language->get('text_bovit');
		$this->data['text_netto_ar']            = $this->language->get('text_netto_ar');
		$this->data['entry_folyamat']           = $this->language->get('entry_folyamat');

		$this->data['button_save']          = $this->language->get('button_save');
		$this->data['button_cancel']        = $this->language->get('button_cancel');
		$this->data['button_add_module']    = $this->language->get('button_add_module');
		$this->data['button_remove']        = $this->language->get('button_remove');
        $this->data['text_yes']             = $this->language->get('text_yes');
        $this->data['text_no']              = $this->language->get('text_no');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/multifilter', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('module/multifilter', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['modules'] = array();

		if (isset($this->request->post['multifilter_module'])) {
			$this->data['modules'] = $this->request->post['multifilter_module'];
		} elseif ($this->config->get('multifilter_module')) {
			$this->data['modules'] = $this->config->get('multifilter_module');
		}

        if (isset($this->request->post['multifilter_beallitasok'])) {
			$this->data['beallitasok'] = $this->request->post['multifilter_beallitasok'];
		} elseif ($this->config->get('multifilter_beallitasok')) {
			$this->data['beallitasok'] = $this->config->get('multifilter_beallitasok');
		}

        if (isset($this->request->post['multifilter_module_fejlec'])) {
            $this->data['multifilter_module_fejlec'] = $this->request->post['multifilter_module_fejlec'];
        } else {
            $this->data['multifilter_module_fejlec'] = $this->config->get('multifilter_module_fejlec');
        }

		if (isset($this->request->post['multifilter_module_ajax'])) {
			$this->data['multifilter_module_ajax'] = $this->request->post['multifilter_module_ajax'];
		} else {
			$this->data['multifilter_module_ajax'] = $this->config->get('multifilter_module_ajax');
		}


		$this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/multifilter.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/multifilter')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>