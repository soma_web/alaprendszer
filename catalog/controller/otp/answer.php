<?php
/**
 *
 */

Class ControllerOtpAnswer extends Controller
{
    private $error = array();

    public function index()
    {
        $filename = DIR_ARUHAZ."otp/web_demo/request.txt";

        if (file_exists($filename)) {
            $handle = fopen($filename, "rb");
            $nms_contents = fread($handle, filesize($filename));
            fclose($handle);
        }

        $this->language->load('otp/answer');

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_thank'] = $this->language->get('text_thank');
        $this->data['text_details'] = $this->language->get('text_details');
        $this->data['text_transaction_id'] = $this->language->get('text_transaction_id');
        $this->data['text_see_you_soon'] = $this->language->get('text_see_you_soon');
        $this->data['text_cancelled'] = $this->language->get('text_cancelled');
        $this->data['text_no_purchase'] = $this->language->get('text_no_purchase');
        $this->data['text_store_error'] = $this->language->get('text_store_error');
        $this->data['text_bank_error'] = $this->language->get('text_bank_error');
        $this->data['text_error'] = $this->language->get('text_error');
        $this->data['text_dear_customer'] = $this->language->get('text_dear_customer');

        //print_r(unserialize($nms_contents));

        if(!empty($nms_contents)){
            $this->data['nms_tartalom']         = unserialize($nms_contents);
            $this->data['answer_page']          = $this->data['nms_tartalom']['eredmeny'];
            $this->data['tranzakcioAzonosito']  = $this->data['nms_tartalom']['tranzakcioAzonosito'];
            //$this->data['posId']                = $this->data['nms_tartalom']['posId'];
            if(!empty($this->data['nms_tartalom']['error_msg'])){
                $this->data['error_msg']        = $this->data['nms_tartalom']['error_msg'];
            }
        }

        $this->data['filename'] = $filename;
        $this->load->model('checkout/order');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/otp/answer.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/otp/answer.tpl';
        } else {
            $this->template = 'default/template/otp/answer.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );
        //$this->data['tranzakcioAzonosito']
        if(empty($this->data['answer_page'])) $this->data['answer_page'] = "default";

        if($this->data['answer_page'] == "success"){
            $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('otp_order_status_id'),"Tranzakció azonosító: ".$this->data['tranzakcioAzonosito'],"true");
            if (isset($this->session->data['order_id'])) {
                $this->cart->clear();

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);
                unset($this->session->data['guest']);
                unset($this->session->data['comment']);
                unset($this->session->data['order_id']);
                unset($this->session->data['coupon']);
                unset($this->session->data['reward']);
                unset($this->session->data['voucher']);
                unset($this->session->data['vouchers']);

                if (!empty($_SESSION['checkout_click_history_id'])) {
                    $this->load->model("checkout/guest_history");
                    $this->model_checkout_guest_history->addHistory_To(array('payment_otp' => 'success'));
                    unset ($_SESSION['checkout_click_history_id']);
                }
            }
        }else if($this->data['answer_page'] == "cancel"){
            if (isset($this->session->data['order_id'])) {
                $this->cart->clear();

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);
                unset($this->session->data['guest']);
                unset($this->session->data['comment']);
                unset($this->session->data['order_id']);
                unset($this->session->data['coupon']);
                unset($this->session->data['reward']);
                unset($this->session->data['voucher']);
                unset($this->session->data['vouchers']);

                if (!empty($_SESSION['checkout_click_history_id'])) {
                    $this->load->model("checkout/guest_history");
                    $this->model_checkout_guest_history->addHistory_To(array('payment_otp' => 'cancel'));
                    unset ($_SESSION['checkout_click_history_id']);
                }
            }
        }

        if (file_exists($filename)) {
            unlink($filename);
        }

        $this->response->setOutput($this->render());
    }

}