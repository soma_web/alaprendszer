<?php  
class ControllerCheckoutCheckoutElhelyezkedes extends Controller {
	public function index() {
		// Validate cart has products and has stock.
		if ((!$this->cart_elhelyezkedes->hasProducts() ) ) {
	  		$this->redirect($this->url->link('account/product'));
    	}	
		
		$products = $this->cart_elhelyezkedes->getProducts();


        foreach ($products as $product) {
			$product_total = 0;
		}
				
		$this->language->load('checkout/checkout_elhelyezkedes');
		
		$this->document->setTitle($this->language->get('heading_title')); 
		
		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_termekeim'),
			'href'      => $this->url->link('account/product'),
        	'separator' => $this->language->get('text_separator')
      	);
		
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('checkout/checkout_elhelyezkedes', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
					
	    $this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_checkout_account'] = $this->language->get('text_checkout_account');
		$this->data['text_checkout_payment_address'] = $this->language->get('text_checkout_payment_address');
		$this->data['text_checkout_payment_method'] = $this->language->get('text_checkout_payment_method');

        $this->data['text_checkout_confirm'] = $this->language->get('text_checkout_confirm');


		$this->data['text_modify'] = $this->language->get('text_modify');
		
		$this->data['logged'] = $this->customer->isLogged();

        if ($this->data['logged'] != null && isset($_SESSION['customer_id']) && $_SESSION['customer_id'] > 0  )  {
            $this->load->model('account/customer');
            $customer_info = $this->model_account_customer->getCustomer($_SESSION['customer_id']);
        }
        $this->data['kosarban_van'] = count($this->cart_elhelyezkedes->getProducts());
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/checkout_elhelyezkedes.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/checkout/checkout_elhelyezkedes.tpl';
		} else {
			$this->template = 'default/template/checkout/checkout_elhelyezkedes.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
            'module/cart_elhelyezkedes',
            'common/header'
		);
				
		$this->response->setOutput($this->render());
  	}
}
?>