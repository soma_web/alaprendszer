<?php   
class ControllerCronCrmMegrendelesSzamlazz extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;

        $this->load->model("cron/altalanos");
        $this->load->model('cron/crm_megrendeles_szamlazz');

        echo 'Elindítva<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $orders = $this->model_cron_crm_megrendeles_szamlazz->getOrders();

        echo '<br><br>Rendelések összesen: '.count($orders).' db';
        $this->model_cron_altalanos->addFuttatasEredmeny('Rendelések összesen: '.count($orders).' db');


        if ($orders) {
            $data['fizmod']         = "Banki átutalás";
            $data['szamlaNyelve']   = "hu";
            $data['arfolyam']       = 1;

            foreach($orders as $key=>$value) {
                $szamlazz[$key] = $this->model_cron_crm_megrendeles_szamlazz->getOrder($value["salesorderid"]);
                $this->model_cron_altalanos->addFuttatasEredmeny('Rendelések orderId: '.$value["salesorderid"]);

                if ($szamlazz[$key]) {
                    $szamlazz[$key]['payment']  = $this->model_cron_crm_megrendeles_szamlazz->getOrderPayment($value["salesorderid"]);
                    $szamlazz[$key]['account']  = $this->model_cron_crm_megrendeles_szamlazz->getOrderAccount($szamlazz[$key]['accountid']);
                    $szamlazz[$key]['shipping'] = $this->model_cron_crm_megrendeles_szamlazz->getOrderShipping($value["salesorderid"]);
                    $szamlazz[$key]['products'] = $this->model_cron_crm_megrendeles_szamlazz->getOrderProducts($value["salesorderid"]);
                    $szamlazz[$key]['contact']  = $this->model_cron_crm_megrendeles_szamlazz->getOrderContact($szamlazz[$key]["contactid"]);
                    if ($szamlazz[$key]['products']) {
                        $szla_tipus = $value['sostatus'] == "Díjbekérő" ? 2 : ($value['sostatus'] == "Delivered" ? 5 : 0);
                        if ($szla_tipus) {
                            if ($value['szamlaszam'] = $this->model_cron_altalanos->sendCrmToSzamlazz($szamlazz[$key],$szla_tipus,$data)) {
                                $this->model_cron_crm_megrendeles_szamlazz->deleteTriggerOrder($value);
                                $value['siker'] = 1;
                                $this->model_cron_crm_megrendeles_szamlazz->updateAllapotStatus($value);
                            } else {
                                $value['siker'] = '';
                                $this->model_cron_crm_megrendeles_szamlazz->updateAllapotStatus($value);
                            }
                        }
                    }
                }
            }
        }

        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}

