<?php
// Heading
$_['heading_title']         = 'Customer';

// Text
$_['text_success']          = 'Success: You have modified customers!';
$_['text_default']          = 'Default';
$_['text_approved']         = 'You have approved %s accounts!';
$_['text_wait']             = 'Please Wait!';
$_['text_balance']          = 'Balance:';
$_['text_add_blacklist']    = 'Add Blacklist';
$_['text_remove_blacklist'] = 'Remove Blacklist';
$_['text_nem']                = 'Sex:';
$_['text_eletkor']            = 'Age:';
$_['text_ferfi']              = 'Men:';
$_['text_no']                 = 'Women:';


$_['error_eletkor']           = 'Please select the age of';
$_['error_nem']                = 'Please select the gender of';

// Column
$_['column_name']           = 'Customer Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Customer Group';
$_['column_status']         = 'Status'; 
$_['column_login']          = 'Login into Store';
$_['column_approved']       = 'Approved';
$_['column_date_added']     = 'Date Added';
$_['column_description']    = 'Description';
$_['column_amount']         = 'Amount';
$_['column_points']         = 'Points';
$_['column_ip']             = 'IP';
$_['column_total']          = 'Total Accounts';
$_['column_action']         = 'Action';
$_['column_atkuldve']       = 'Sent to SAP ';

$_['atkuldesre_var']        ='Átküldésre vár';
$_['importalt']             ='SAP-ból integrált';
$_['atkuldve']              ='Átküldve';
$_['ellenorzott']           ='Leellenőrizve';
$_['column_payemail']       = 'Paypal Email';


// Entry
$_['entry_adoszam']           = 'Tax number';
$_['entry_sap_kod']           = 'SAP customer number';
$_['entry_fizetes_engedely']  = 'Enabled';
$_['entry_fizetes_modositas']  = 'Modify';
$_['entry_fizetes_torles']  = 'Delete';
$_['button_uj_fizetes']     = 'New payment';
$_['entry_fizetes']           = 'Unique payment:';
$_['entry_firstname']       = 'First Name:';
$_['entry_lastname']        = 'Last Name:';
$_['entry_email']           = 'E-Mail:';
$_['entry_telephone']       = 'Telephone:';
$_['entry_fax']             = 'Fax:';
$_['entry_newsletter']      = 'Newsletter:';
$_['entry_nem']            = 'Sex';
$_['entry_eletkor']        = 'Age';
$_['entry_customer_group']  = 'Customer Group:';
$_['entry_status']          = 'Status:';
$_['entry_password']        = 'Password:';
$_['entry_confirm']         = 'Confirm:';
$_['entry_company']         = 'Company:';
$_['entry_adoszam']         = 'No tax:';
$_['entry_address_1']       = 'Address 1:';
$_['entry_address_2']       = 'Address 2:';
$_['entry_city']            = 'City:';
$_['entry_postcode']        = 'Postcode:';
$_['entry_country']         = 'Country:';
$_['entry_zone']            = 'Region / State:';
$_['entry_default']         = 'Default Address:';
$_['entry_amount']          = 'Amount:';
$_['entry_points']          = 'Points:<br /><span class="help">Use minus to remove points</span>';
$_['entry_description']     = 'Description:';
$_['entry_szazalek']        = 'Percent discount';
$_['entry_iskolai_vegzettseg']        = 'Educational attainment';
$_['entry_vallalkozasi_forma']      = 'Business form:';
$_['entry_szekhely']                = 'Head office:';
$_['entry_ugyvezeto_neve']          = 'Manager name:';
$_['entry_ugyvezeto_telefonszama']  = 'Manager phone:';


// Error
$_['error_warning']         = 'Warning: Please check the form carefully for errors!';
$_['error_permission']      = 'Warning: You do not have permission to modify customers!';
$_['error_exists']          = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']       = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']        = 'Last Name must be between 1 and 32 characters!';
$_['error_email']           = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']       = 'Telephone must be between 3 and 32 characters!';
$_['error_password']        = 'Password must be between 4 and 20 characters!';
$_['error_confirm']         = 'Password and password confirmation do not match!';
$_['error_address_1']       = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']            = 'City must be between 2 and 128 characters!';
$_['error_postcode']        = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']         = 'Please select a country!';
$_['error_zone']            = 'Please select a region / state!';

$_['select_ferfi']           = 'Men';
$_['select_no']             = 'Women';

$_['entry_feltolto']          = 'Customer type';
$_['select_feltolto']         = 'Partner';
$_['select_altalanos']        = 'Individuals';

$_['entry_newsletter_categories'] = 'Newsletter categories';
?>