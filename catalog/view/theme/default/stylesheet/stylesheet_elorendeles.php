<?php
?>
<style>
    .box-product > div input.button.arajanlat, .ajanlott_termekek input.button.arajanlat {
        padding-left: 20px;
        padding-right: 20px;
        margin: 0;
        width: 100%;
    }


    #arajanlatot_kerek_mail {
        position: absolute;
        z-index: 10000000;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.3);
    }

    #arajanlatot_kerek_mail > div {
        background: #FFFFFF;
        border: 1px solid #efefef;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0px 0px 30px rgba(50, 50, 50, 0.75);
        -moz-box-shadow: 0px 0px 30px rgba(50, 50, 50, 0.75);
        box-shadow: 0px 0px 30px rgba(50, 50, 50, 0.75);
        margin: 50px auto 0 auto;
        max-width: 800px;
        padding: 20px;
        width: 100%;
        position: relative;
    }
    #arajanlatot_kerek_mail .close_arajanlat_icon{
        position: absolute;
        right: 0;
        top: 2px;
        cursor: pointer;
    }

    #arajanlatot_kerek_mail textarea {
        height: 51px;
        width: 90%;
    }

    #arajanlatot_kerek_mail .buttons {
        width: 90%;
        border: none;

    }
    #arajanlatot_kerek_mail .email_adatok{
        display: inline-block;
        width: 50%;
        white-space: normal;
    }

    #arajanlatot_kerek_mail .valasztott_termek{
        display: inline-block;
        vertical-align: top;
        width: 48%;
    }

    #arajanlatot_kerek_mail .cimke{
        font-weight: bold;
        width: 20%;
    }
    #arajanlatot_kerek_mail .ertek{
    }


    #arajanlatot_kerek_mail textarea {
        height: 51px;
        width: 90%;
    }


    #arajanlatot_kerek_mail h2 {
        margin-top: 25px;
    }
    #arajanlatot_kerek_mail .buttons {
        border: none;
        display: table;
        margin: auto;
        width: auto;
        margin-bottom: 30px;
    }

    #arajanlatot_kerek_mail input.button {
        height: auto;
        padding: 15px 28px;
        font-size: 18px;
    }

    #arajanlatot_kerek_mail .email_adatok{
        display: inline-block;
        width: 50%;
    }

    #arajanlatot_kerek_mail .valasztott_termek{
        display: inline-block;
        vertical-align: top;
        white-space: normal;
    }

    #arajanlatot_kerek_mail .cimke{
        font-weight: bold;
        width: 20%;
    }
    #arajanlatot_kerek_mail {
        white-space: nowrap;

    }
    #arajanlatot_kerek_mail .kapcsolat{
        display: inline-block;
        width: 50%;
        vertical-align: top;
        box-sizing: border-box;
        white-space: normal;
    }

    #arajanlatot_kerek_mail input[type='text'], #arajanlatot_kerek_mail input[type='password'], #arajanlatot_kerek_mail textarea {
        box-sizing: border-box;

    }

</style>

