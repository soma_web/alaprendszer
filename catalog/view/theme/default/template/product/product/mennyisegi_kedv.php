<?php

$_['netto_ar']         = array(
    'type'              => 'checkbox',
    'name'              => 'netto_ar',
    'default'           => "1",
    'heading'           => 'Nettó ár megj.'
);

$_['netto_szoveg']     = array(
    'type'              => 'checkbox',
    'name'              => 'netto_szoveg',
    'default'           => "1",
    'heading'           => 'Nettó szöveg megj.'
);

$_['brutto_ar']         = array(
    'type'              => 'checkbox',
    'name'              => 'brutto_ar',
    'default'           => "1",
    'heading'           => 'Bruttó ár megj.'
);

$_['brutto_szoveg']     = array(
    'type'              => 'checkbox',
    'name'              => 'brutto_szoveg',
    'default'           => "1",
    'heading'           => 'Bruttó szöveg megj.'
);

$_['net_bru']     = array(
    'type'              => 'checkbox',
    'name'              => 'net_bru',
    'default'           => "1",
    'heading'           => 'Nettó -> Bruttó sorrend'
);

?>