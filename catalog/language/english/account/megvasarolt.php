<?php
// Heading
$_['heading_title']     = 'Purchased products';
$_['heading_title_katagoria']    = 'Sector Coupons';

// Text
$_['text_account']      = 'Account';
$_['text_home']         = 'Shop';
$_['column_name']       = 'product Name';
$_['column_quantity']   = 'Quantity';
$_['column_total']       = 'All';
$_['button_filter']     = 'Filter';
$_['entry_date_start']  = 'Begin:';
$_['entry_date_end']    = 'Over:';
$_['entry_status']     = 'Order status';
$_['text_all_status']  = '-- Each state --';
$_['text_no_results']  = 'No results found!';

$_['column_category_name']     = 'Category';
$_['column_hirdetok']          = 'Advertisers';
$_['column_vasaroltak']        = 'Downloaded';
$_['column_vasaroltak_nalam']  = 'Downloaded<br>My coupon';
$_['column_atlag']             = 'Average';
$_['column_atlag_szazalek']    = 'Average %';
$_['column_lattak']            = 'Seen';
?>