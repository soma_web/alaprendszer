<?php if ($addresses) { ?>
    <input type="radio" name="shipping_address" value="existing" id="shipping-address-existing" checked="checked" />
    <label for="shipping-address-existing"><?php echo $text_address_existing; ?></label>
    <div id="shipping-existing">
        <select name="address_id" style="width: 100%; margin-bottom: 15px;" size="5">
            <?php $i=0; foreach ($addresses as $address) { ?>
                <?php if ($address['address_id'] == $address_id) { ?>
                    <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['company']; ?> <?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php //echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                <?php } elseif (count($addresses) == 1) { ?>
                    <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['company']; ?> <?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php //echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $address['address_id']; ?>"><?php echo $address['company']; ?> <?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php //echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                <?php } ?>
                <?php $i=0; } ?>
        </select>
    </div>
<?php } ?>
<p>
    <input type="radio" name="shipping_address" value="new" id="shipping-address-new" />
    <label for="shipping-address-new"><?php echo $text_address_new; ?></label>
</p>
<div id="shipping-new" style="display: none;">
    <table class="form">
        <?php if (($this->config->get('megjelenit_regisztracio_vezeteknev') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_vezeteknev') == 1  )) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
                <td><input type="text" name="firstname" value="" class="large-field" /></td>
            </tr>
        <?php } ?>
        <?php if (($this->config->get('megjelenit_regisztracio_keresztnev') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_keresztnev') == 1  )) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
                <td><input type="text" name="lastname" value="" class="large-field" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_cegnev') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_cegnev') == 1 ) ) {?>
            <tr>
                <td><?php echo $entry_company; ?></td>
                <td><input type="text" name="company" value="" class="large-field" /></td>
            </tr>
        <?php } else { ?>
            <input type="hidden" name="company" value=""  />
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_adoszam') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_adoszam') == 1 ) ) {?>
            <tr>
                <td><?php echo $entry_adoszam; ?></td>
                <td><input type="text" name="adoszam" value="" class="large-field" /></td>
            </tr>
        <?php } else { ?>
            <input type="hidden" name="adoszam" value=""  />
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_vallalkozasi_forma') == 1 )  || ($this->config->get('megjelenit_regisztracio_ceges_vallalkozasi_forma') == 1 ) ) {?>
            <tr>
                <td><?php echo $entry_vallalkozasi_forma; ?></td>
                <td><input type="text" name="vallalkozasi_forma" class="large-field" value="" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_szekhely') == 1  ) || ($this->config->get('megjelenit_regisztracio_ceges_szekhely') == 1  ) ) {?>
            <tr>
                <td><?php echo $entry_szekhely; ?></td>
                <td><input type="text" name="szekhely" class="large-field" value="" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_ugyvezeto_neve') == 1  ) || ($this->config->get('megjelenit_regisztracio_ceges_ugyvezeto_neve') == 1  ) ) {?>
            <tr>
                <td><?php echo $entry_ugyvezeto_neve; ?></td>
                <td><input type="text" name="ugyvezeto_neve" class="large-field" value="" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_ugyvezeto_telefonszama') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_ugyvezeto_telefonszama') == 1 ) ) {?>
            <tr>
                <td><?php echo $entry_ugyvezeto_telefonszama; ?></td>
                <td><input type="text" name="ugyvezeto_telefonszama" class="large-field" value="" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_utca') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_utca') == 1 ) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
                <td><input type="text" name="address_1" value="" class="large-field" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_cimadat_kiegeszito') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_cimadat_kiegeszito') == 1 ) ) {?>
            <tr>
                <td><?php echo $entry_address_2; ?></td>
                <td><input type="text" name="address_2" value="" class="large-field" /></td>
            </tr>
        <?php } else { ?>
            <input type="hidden" name="address_2" value=""  />
        <?php } ?>

        <?php if ( ($this->config->get('megjelenit_regisztracio_varos') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_varos') == 1 ) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_city; ?></td>
                <td><input type="text" name="city" value="" class="large-field" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_iranyitoszam') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_iranyitoszam') == 1 ) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
                <td><input type="text" name="postcode" value="" class="large-field" /></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_orszag') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_orszag') == 1 ) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_country; ?></td>
                <td><select name="country_id" class="large-field" onchange="$('#shipping-address select[name=\'zone_id\']').load('index.php?route=checkout/shipping_address/zone&country_id=' + this.value);">
                        <option value=""><?php echo $text_select; ?></option>
                        <?php foreach ($countries as $country) { ?>
                            <?php if ($country['country_id'] == $country_id) { ?>
                                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select></td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_megye') || $this->config->get('megjelenit_regisztracio_ceges_megye')) && !empty($this->request->post['zone_id'])) { ?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
                <td><select name="zone_id" class="large-field">
                    </select></td>
            </tr>
        <?php } ?>
    </table>
</div>
<br />
<div class="buttons">
    <div class="right"><input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-address" class="button" /></div>
</div>
<script type="text/javascript"><!--
    $('#shipping-address select[name=\'zone_id\']').load('index.php?route=checkout/shipping_address/zone&country_id=<?php echo $country_id; ?>');

    $('#shipping-address input[name=\'shipping_address\']').on('change', function() {
        if (this.value == 'new') {
            $('#shipping-existing').hide();
            $('#shipping-new').show();
        } else {
            $('#shipping-existing').show();
            $('#shipping-new').hide();
        }
    });
    //--></script>