<div class="text_letoltes"><?php echo $entry_letoltesek;?></div>
<div class="text_letoltes_kepmeret"><?php echo $entry_letoltesek_kep;?></div>

<?php foreach($letoltesek as $value) { ?>
    <?php $productID = $value['key'] ; $productImageInPlaces = $productImageInPlacesALL[$productID]; $productData = $productDatas[$productID]; ?>
    <div class="kupon_nevek">
        <b><?php echo $productData['name']; ?></b>
    </div>
    <br />

    <table>
        <?php if(isset($productData['thumb'])) { ?>
            <tr>
                <td>
                    <img src="<?php echo $productData['thumb'] ?>" /><br />
                    <?php if(count($productImageInPlaces) > 0) { ?>
                        <input type='checkbox' isSend=1 name='defaultImage' productID="<?php echo $productID ?>" value='1' /> <?php echo $entry_alapertelmezett ?>
                    <?php } else { ?>
                        <input type='checkbox' isSend=1 name='defaultImage' productID="<?php echo $productID ?>" checked="checked" value='1' /> <?php echo $entry_alapertelmezett ?>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <?php if(count($productImageInPlaces) > 0) { ?>
            <tr>
                <td>
                    <?php echo $entry_bevaltasi_helyek; ?>
                </td>
                <?php foreach($productImageInPlaces as $productImageInPlace) { ?>
                    <td>
                        <img src="<?php echo $productImageInPlace['image'] ?>" /><br />
                        <input type='checkbox' isSend=1 checked='checked' name='productImageInPlaces' productID="<?php echo $productID ?>" placeImageID="<?php echo $productImageInPlace['id'] ?>" value='1' />
                        <?php echo $productImageInPlace['name'] ?>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
<?php } ?>
<br />

<div class="kupon_nevek">
        <input type="button" value="<?php echo $button_letoltes; ?>" id="button-termek-letoltes" class="button_letoltes" />

</div>
<div class="kupon_nevek">
    <input type="button" value="<?php echo $button_nyomtatas; ?>" id="button-termek-nyomtatas" class="button_letoltes" />
</div>

<div class="buttons"  style="display: none">
  <div class="right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-nyomtat-method" class="button" />
  </div>
</div>

<script>
    $("#button-termek-letoltes").bind("click",function(){
        var sending = $('[isSend="1"]');
        var prefix='';
        var sendingDatas = { };
        for(var n=0;n<sending.length;n++) {
            var obj = sending[n];
            if(obj.checked != true) {
                continue;
            }
            var name = obj.getAttribute('name');
            var productID = obj.getAttribute('productID');
            if(sendingDatas[productID] == undefined) {
                sendingDatas[productID] = { };
            }
            if(name == 'defaultImage') {
                sendingDatas[productID]['defaultImage'] = 1;
            } else if(name == 'productImageInPlaces') {
                var placeImageID = obj.getAttribute('placeImageID');
                if(sendingDatas[productID]['placeImageID'] == undefined) {
                    sendingDatas[productID]['placeImageID'] = { };
                }
                sendingDatas[productID]['placeImageID'][placeImageID] = 1;
            }
        }
        if (n != 0) {
            prefix = '&jsondata=' + JSON.stringify(sendingDatas);
            location='index.php?route=checkout/ingyen_method/letoltes'+prefix;
            $('#nyomtatas .buttons').slideDown('slow');
        }
    });

    $("#button-termek-nyomtatas").bind("click",function(){
        var szelesseg = $(window).width();
        var magassag = $(window).height();

        var postObject = { };
        var sending = $('[isSend="1"]');
        var prefix='';
        var sendingDatas = { };
        for(var n=0;n<sending.length;n++) {
            var obj = sending[n];
            if(obj.checked != true) {
                continue;
            }
            var name = obj.getAttribute('name');
            var productID = obj.getAttribute('productID');
            if(sendingDatas[productID] == undefined) {
                sendingDatas[productID] = { };
            }

            if(name == 'defaultImage') {
                sendingDatas[productID]['defaultImage'] = 1;
            } else if(name == 'productImageInPlaces') {
                var placeImageID = obj.getAttribute('placeImageID');
                if(sendingDatas[productID]['placeImageID'] == undefined) {
                    sendingDatas[productID]['placeImageID'] = { };
                }
                sendingDatas[productID]['placeImageID'][placeImageID] = 1;
            }
        }

        postObject['jsondata'] = JSON.stringify(sendingDatas);

        var newWin = window.open("","","width=400,height=500,top=300,left=300");
        $.ajax({
            url: 'index.php?route=checkout/ingyen_method/nyomtatas',
            dataType: 'html',
            type: 'POST',
            data: postObject,
            success: function() {
                newWin.location.replace("kupon.pdf"),
                $('#nyomtatas .buttons').slideDown('slow');
                //newWin.print();

            },
            complete: function() {
            },

            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });


</script>