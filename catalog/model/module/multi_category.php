<?php
class ModelModuleMultiCategory extends Model {

    private $bulid = 0;

    public function getCategoriesAll() {
         $sql = "SELECT * FROM " . DB_PREFIX . "category c
            LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
            LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)";

		 $sql .= "WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
		        AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1'
		        ORDER BY c.category_id, LCASE(cd.name)";

        $query = $this->db->query($sql);

        if ($query->rows) {
            foreach($query->rows as $key=>$value) {
                $sql  = "SELECT COUNT(DISTINCT p2c.product_id) AS total FROM ".DB_PREFIX."product_to_category p2c ";
                $sql .= " INNER JOIN ".DB_PREFIX."product p ON (p2c.product_id=p.product_id AND p.status=1)";
                $sql .= " WHERE p2c.category_id='".$value['category_id']."'";
                $query_product = $this->db->query($sql);
                if ($query_product->row) {
                    $query->rows[$key]['product_total'] = $query_product->row['total'];
                } else {
                    $query->rows[$key]['product_total'] = 0;

                }
            }
        }
        return $query->rows;
    }

    public function buildTree(array $elements, $parentId = 0, $path) {

        $this->load->model('tool/image');

        $branch = array();
        foreach ($elements as $element) {

            if ($element['parent_id'] == $parentId) {
                $ha_piacter="";
                if ($element['piacter'] == 1){
                    $ha_piacter.="&piacter=piacter";
                }

                $element['href'] =  $this->url->link('product/category', 'path=' . $path.$element['category_id'].$ha_piacter );
                $children = $this->buildTree($elements, $element['category_id'], $path.$element['category_id']."_");
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    public function categorySort($category=array()){
        $width  = $this->config->get('multi_category_image_width') ? $this->config->get('multi_category_image_width') : $this->config->get('config_image_thumb_width');
        $height = $this->config->get('multi_category_image_height') ? $this->config->get('multi_category_image_height') : $this->config->get('config_image_thumb_height');

        $width_full  = $this->config->get('multi_category_full_image_width') ? $this->config->get('multi_category_full_image_width') : $this->config->get('config_image_thumb_width');
        $height_full = $this->config->get('multi_category_full_image_height') ? $this->config->get('multi_category_full_image_height') : $this->config->get('config_image_thumb_height');

        $elements = $this->config->rendezes($category,"sort_order");

        $vissza = array();
        foreach($elements as $element) {
            if ($element['image_icon']) {
                $element['image_icon'] = $this->model_tool_image->resize($element['image_icon'],$width, $height);
            } else {
                $element['image_icon'] =  $this->model_tool_image->resize('no_image.jpg', $width, $height,false);
            }

            if ($element['image']) {
                $element['image'] = $this->model_tool_image->resize($element['image'],$width_full, $height_full);
            } else {
                $element['image'] =  $this->model_tool_image->resize('no_image.jpg', $width_full, $height_full,false);
            }

            if (isset($element['children'])) {
                $element['children'] = $this->categorySort($element['children']);
            }
            $vissza[] = $element;


        }

        return $vissza;

    }

    public function onlyFindProduct($categoryes) {

        $tomb = array();
        foreach($categoryes as $key=>$value) {
            if (empty($value['product_total']) && empty($value['product_id'])) {
                $tomb[$key] = 1;
            }

            if (!empty($value['children'])) {
                foreach($value['children'] as $key1=>$value1) {
                    if (empty($value1['product_total']) && empty($value1['product_id'])) {
                        $tomb[$key.'_'.$key1] = 1;

                    } else {
                        unset ($tomb[$key]);
                    }

                    if (!empty($value1['children'])) {
                        foreach($value1['children'] as $key2=>$value2) {
                            if (empty($value2['product_total']) && empty($value2['product_id'])) {
                                $tomb[$key.'_'.$key1.'_'.$key2] = 1;

                            } else {
                                unset ($tomb[$key]);
                                unset ($tomb[$key.'_'.$key1]);
                            }

                            if (!empty($value2['children'])) {
                                foreach($value2['children'] as $key3=>$value3) {
                                    if (empty($value3['product_total']) && empty($value3['product_id'])) {
                                        $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3] = 1;

                                    } else {
                                        unset ($tomb[$key]);
                                        unset ($tomb[$key.'_'.$key1]);
                                        unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                    }

                                    if (!empty($value3['children'])) {
                                        foreach($value3['children'] as $key4=>$value4) {
                                            if (empty($value4['product_total']) && empty($value4['product_id'])) {
                                                $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4] = 1;

                                            } else {
                                                unset ($tomb[$key]);
                                                unset ($tomb[$key.'_'.$key1]);
                                                unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                            }

                                            if (!empty($value4['children'])) {
                                                foreach($value4['children'] as $key5=>$value5) {
                                                    if (empty($value5['product_total']) && empty($value5['product_id'])) {
                                                        $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5] = 1;

                                                    } else {
                                                        unset ($tomb[$key]);
                                                        unset ($tomb[$key.'_'.$key1]);
                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4]);
                                                    }

                                                    if (!empty($value5['children'])) {
                                                        foreach($value5['children'] as $key6=>$value6) {
                                                            if (empty($value6['product_total']) && empty($value6['product_id'])) {
                                                                $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6] = 1;

                                                            } else {
                                                                unset ($tomb[$key]);
                                                                unset ($tomb[$key.'_'.$key1]);
                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4]);
                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5]);
                                                            }

                                                            if (!empty($value6['children'])) {
                                                                foreach($value6['children'] as $key7=>$value7) {
                                                                    if (empty($value7['product_total']) && empty($value7['product_id'])) {
                                                                        $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6.'_'.$key7] = 1;

                                                                    } else {
                                                                        unset ($tomb[$key]);
                                                                        unset ($tomb[$key.'_'.$key1]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6]);
                                                                    }

                                                                    if (!empty($value7['children'])) {
                                                                        foreach($value7['children'] as $key8=>$value8) {
                                                                            if (empty($value8['product_total']) && empty($value8['product_id'])) {
                                                                                $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6.'_'.$key7.'_'.$key8] = 1;

                                                                            } else {
                                                                                unset ($tomb[$key]);
                                                                                unset ($tomb[$key.'_'.$key1]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6.'_'.$key7]);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        foreach($tomb as $key=>$value) {
            $kulcsok = explode('_', $key);
            $kulcs_ossz = count($kulcsok);
            if ($kulcs_ossz == 1) {
                if (isset($categoryes[$kulcsok[0]])) {
                    unset ($categoryes[$kulcsok[0]]);
                }

            } elseif ($kulcs_ossz == 2) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]);
                }

            } elseif ($kulcs_ossz == 3) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]);
                }

            } elseif ($kulcs_ossz == 4) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]);
                }

            } elseif ($kulcs_ossz == 5) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]);
                }

            } elseif ($kulcs_ossz == 6) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]);
                }

            } elseif ($kulcs_ossz == 7) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]);
                }

            } elseif ($kulcs_ossz == 8) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]['children'][$kulcsok[7]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]['children'][$kulcsok[7]]);
                }

            } elseif ($kulcs_ossz == 9) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]['children'][$kulcsok[7]]['children'][$kulcsok[8]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]['children'][$kulcsok[7]]['children'][$kulcsok[8]]);
                }
            }
        }

        return $categoryes;
    }

}
?>