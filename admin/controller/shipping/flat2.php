<?php
class ControllerShippingflat2 extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('shipping/flat2');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!isset($this->request->post['gls_csomagpont_fl2'])){
                $this->request->post['gls_csomagpont_fl2'] = 0;
            } else {
                $this->request->post['gls_csomagpont_fl2'] = 1;
            }

			$this->model_setting_setting->editSetting('flat2', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}

        $this->data['megjelenit_gls'] = $this->config->get('megjelenit_gls');

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_gls_csomagpont']  = $this->language->get('text_gls_csomagpont');

        $this->data['entry_title_heading'] = $this->language->get('entry_title_heading');
        $this->data['entry_title_text'] = $this->language->get('entry_title_text');
		
		$this->data['entry_cost'] = $this->language->get('entry_cost');
		$this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_mail'] = $this->language->get('entry_mail');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/flat2', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('shipping/flat2', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['flat2_cost'])) {
			$this->data['flat2_cost'] = $this->request->post['flat2_cost'];
		} else {
			$this->data['flat2_cost'] = $this->config->get('flat2_cost');
		}


		if (isset($this->request->post['flat2_mail'])) {
			$this->data['flat2_mail'] = $this->request->post['flat2_mail'];
		} else {
			$this->data['flat2_mail'] = $this->config->get('flat2_mail');
		}

        if (isset($this->request->post['gls_csomagpont_fl2'])) {
            $this->data['gls_csomagpont_fl2'] = $this->request->post['gls_csomagpont_fl2'];
        } else {
            $this->data['gls_csomagpont_fl2'] = $this->config->get('gls_csomagpont_fl2');
        }

		if (isset($this->request->post['flat2_tax_class_id'])) {
			$this->data['flat2_tax_class_id'] = $this->request->post['flat2_tax_class_id'];
		} else {
			$this->data['flat2_tax_class_id'] = $this->config->get('flat2_tax_class_id');
		}

		if (isset($this->request->post['flat2_geo_zone_id'])) {
			$this->data['flat2_geo_zone_id'] = $this->request->post['flat2_geo_zone_id'];
		} else {
			$this->data['flat2_geo_zone_id'] = $this->config->get('flat2_geo_zone_id');
		}
		
		if (isset($this->request->post['flat2_status'])) {
			$this->data['flat2_status'] = $this->request->post['flat2_status'];
		} else {
			$this->data['flat2_status'] = $this->config->get('flat2_status');
		}
		
		if (isset($this->request->post['flat2_sort_order'])) {
			$this->data['flat2_sort_order'] = $this->request->post['flat2_sort_order'];
		} else {
			$this->data['flat2_sort_order'] = $this->config->get('flat2_sort_order');
		}

        if (isset($this->request->post['flat2_header'])) {
            $this->data['flat2_header'] = $this->request->post['flat2_header'];
        } else {
            $this->data['flat2_header'] = $this->config->get('flat2_header');
        }
        if (isset($this->request->post['flat2_text'])) {
            $this->data['flat2_text'] = $this->request->post['flat2_text'];
        } else {
            $this->data['flat2_text'] = $this->config->get('flat2_text');
        }

		$this->load->model('localisation/tax_class');
		
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		$this->load->model('localisation/geo_zone');
		
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();


        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getInstalled('payment');
        $no_module = $this->model_setting_extension->getNoModule();


        foreach ($extensions as $key => $value) {
            if (!file_exists(DIR_APPLICATION . 'controller/payment/' . $value . '.php')) {
                $this->model_setting_extension->uninstall('payment', $value);

                unset($extensions[$key]);
            } elseif (in_array($value, $no_module)){
                unset($extensions[$key]);
            } else {
                $this->load->language('payment/' . $value);

                if (isset($this->request->post['flat2_payment_'.$value])) {
                    $this->data['flat2_payment_'.$value] = $this->request->post['flat2_payment_'.$value];
                } elseif ( $this->config->get('flat2_payment_'.$value)) {
                    $this->data['flat2_payment_'.$value] = $this->config->get('flat2_payment_'.$value);
                } else {
                    $this->data['flat2_payment_'.$value] = 0;
                }

                $this->data['payment']['payment_' . $value] = array(
                    'name'      => $this->language->get('heading_title'),
                    'valasztva' => $this->data['flat2_payment_'.$value]
                );


            }
        }

		$this->template = 'shipping/flat2.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/flat2')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>