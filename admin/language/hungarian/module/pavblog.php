<?php
/******************************************************
 * @package Pav Category module for Opencart 1.5.x
 * @version 1.0
 * @author http://www.pavothemes.com
 * @copyright	Copyright (C) Feb 2013 PavoThemes.com <@emai:pavothemes@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
*******************************************************/

// Heading
$_['heading_title']       = '<b style="color:#666;font-size:110%;font-weight: normal">Blogok kezelése</b>';

// Text
$_['text_treemenu']         = 'Tree Category Management';
$_['text_menu_assignment']  = 'Modul kiosztás';
$_['text_module']           = 'Modulok';
$_['text_success']        = 'Sikeresen módosította a Kategória modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Taratalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_mainmenu']    = 'Főmenü';
$_['text_slideshow']   = 'Slideshow';
$_['text_promotion']   = 'Promóció';
$_['text_mass_bottom'] = 'Mass Bottom';
$_['text_bottom1']   = 'Alsó 1';
$_['text_bottom2']   = 'Alsó 2';
$_['text_bottom3']   = 'Alsó 3';
$_['text_footer_top']       = 'Footer teteje';
$_['text_footer_center']    = 'Footer Közepe';
$_['text_footer_bottom']    = 'Footer alja';
$_['text_create_new']       = 'Új Kategória Hozzáadása';
$_['text_edit_menu']        = 'Szerkesztés: %s  (ID:%s)';
$_['all_page']              = 'Minden Oldal';
$_['text_seo_data']         = 'SEO Adat';
$_['entry_keyword']         = '<b>SEO Kulcsszó</b><p><i>*Ne használjon szóközt! Helyette használja a következő karaktert: _  ne használja kétszer ugyanazt a kulcsszót.</i></p>';
$_['text_keyword_listing_blogs_page'] = 'Kulcs Szó - Listázó Blog Oldal<p><i>Blog oldalak kilistázására használt Kulcsszó (tag, user) </i>:<br> </p>';

$_['text_image_manager']    = 'Képek Kezelése';
$_['text_front_page']       = 'Fő Oldal';
$_['text_category_page']    = 'Kategória Oldal';
$_['text_item_page']        = 'Bejegyzés Oldal';
$_['text_filter_blog']      = 'Szűrő Blogok';
$_['text_normal']           = 'Normal';
$_['text_seo']              = 'SEO';
$_['text_guide_urls']       = 'Útmutatók';
$_['text_guide_seo']        = 'Segítség SEO engedélyezéséhez';
$_['text_guide_category']   = 'Kategória Kezelése';
$_['text_guide_blog']       = 'Blog Kezelése';
$_['text_guide_module_setting'] = 'Modul Beállítás';
$_['text_cim'] = 'Cím';

$_['text_yes']                = 'Igen';
$_['text_no']                 = 'Nem';

$_['text_kategoria'] = 'Kategória';
$_['text_ujkategoria'] = 'Új kategória';
$_['text_blogok'] = 'Blogok';
$_['text_ujblog'] = 'Új Blog';
$_['text_hozzaszolas'] = 'Hozzászólás';
$_['text_modulbeallitas'] = 'Modul Beállítás';
$_['text_frontendmodulok'] = 'Front End Modulok';



$_['text_legutobbiblog'] = 'Legutóbbi Blog';
$_['text_legolvasottabb'] = 'Legolvasottabb';
$_['text_legutobbihozzaszolas'] = 'Legutóbbi hozzászólás';

// Entry
$_['entry_banner']          = 'Banner:';
$_['entry_dimension']       = 'Méret (Sz x M) és Újraméretezés típusa:';
$_['entry_layout']          = 'Elrendezés:';
$_['entry_position']        = 'Pozíció:';
$_['entry_status']          = 'Állapot:';
$_['entry_sort_order']      = 'Sorrend:';
$_['entry_description']     = 'Előnézet:';
$_['entry_type']            = 'Típus:';
$_['entry_url']             = 'URL:';
$_['entry_category']        = 'Kategória:';
$_['entry_product']         = 'Termék:';
$_['entry_manufacturer']    = 'Gyártó:';
$_['entry_information']     = 'Információ:';
$_['entry_html']            = 'HTML:';
$_['entry_parent_id']       = 'Szülő ID:';
$_['entry_menuclass']       = 'Menü Osztály:';
$_['entry_showtitle']       = 'Cím mutatása:';
$_['entry_isgroup']         = 'Csoport:';
$_['entry_iscontent']       = 'Tartalom:';
$_['entry_default_column_width']    = 'Alapértelmezett Oszlop Szélesség (SpanX): ';
$_['entry_columns']         = 'Oszlopok';
$_['entry_detail_columns']  = 'Részlet Oszlopok Szélessége';
$_['entry_sub_menutype']    = 'Almenü Típus';
$_['entry_submenu_content'] = 'Almenü Tartalom';
$_['text_none']             = 'Nincs';
$_['text_explain_input_html'] = 'Ha a menü tartalma HTML, akkor ennek a menünek az almenüi el lesznek rejtve';
$_['text_explain_input_auto'] = 'Írjon be karaktert vagy szót a kereséshez';
$_['text_explain_submenu_cols'] = 'Minden eggyes részletek aloszlop szélessége 1->12.<br>Pl.: col1=3 col3=5';
$_['text_explain_submenu_type'] = 'Ha a típus Menü, akkor almenük megjelenítése';
$_['text_browse'] = 'Fájlok tallózása';
$_['text_clear'] = 'Kép törlése';
$_['entry_publish'] = 'Közzététel';


$_['button_save_edit']    = "Mentés és Szerkesztés";
$_['button_save_new']   = 'Mentés és Új Hozzáadása';


//
$_['message_delete'] = "Biztosan törölni szeretné?";
// Error
$_['error_permission']    = 'Figyelmeztetés: Önnek nincs jogosultsága módosítani a pavblog modult!';
$_['error_dimension']     = 'Szélesség &amp; Mgasság értékek megadása szükséges!';
$_['error_missing_title'] = 'Kérem írja be a menü címét minden nyelvi fülhöz';
$_['text_explain_drapanddrop'] = 'A sorrend változtatásához kattintson a menüre, majd húzza a kívánt helyre. Utána kattintson a frissités gombra.';
/***/
$_['menu_dashboard']            = "Műszerfal";
$_['menu_manage_categories']    = "Kategóriák kezelése";
$_['menu_manage_blogs']         = "Blogok kezelése";
$_['menu_addblog']              = "Blog hozzáadása";
$_['menu_comment']              = "Hozzászólások";
$_['menu_module_setting']       = "Általános beállítások";
$_['menu_information']          = "Információ";
/* DASHBOARD PAGE **/
$_['panel_page_heading_title']  = 'Műszerfal';
$_['text_quickicons']           = 'Gyors Ikonok';
$_['module_latest']             = 'Blog Legutóbbi';
$_['module_comment']            = 'Blog Hozzászólás';
$_['module_category']           = 'Blog Kategória Menü';
$_['text_modules_urls']         = 'URLs Format';
/** CATEGORY PAGE **/
$_['category_page_heading_title']   = "Kategóriák Kezelése";
$_['text_tree_category_menu']       = "Kategória Fa Menü";
$_['text_category_information']     = "Információ";
$_['text_filter_title']             = 'Szürő Cím';
$_['text_reset']                    = 'Visszaállítás';
$_['text_position']                 = 'Pozíció';
$_['text_mentes']                   = 'Mentés';
/** BLOGS PAGE **/
$_['blogs_page_heading_title']      = 'Blogok Kezelése';
$_['entry_title']                   = "Cím:";
$_['button_create_blog']            = 'Blog Hozzáadása';
$_['text_created']                  = 'Létrehozva';
$_['text_hits']                     = 'Megtekintések';
$_['entry_tags']                    = 'Címkék';
$_['text_explain_tags']             = 'példa: divat, tank,vicces';
$_['text_facebook_id_explain']      = 'Register A Comment Box, Then Get Application ID in Script Or Register Facebook Application ID to moderate comments';
/** BLOG PAGE **/
$_['blog_page_heading_title']       = "Blog Szerkesztés/Új Hozzáadása";
$_['text_general']                  = "Általános";
$_['text_gallery']                  = "Képek - Galéria";
$_['text_meta']                     = "Meta - SEO";
$_['entry_category_id']             = 'Kategória';
$_['entry_featured']                = 'Kiemelt';
$_['entry_hits']                    = 'Megtekintések';
$_['entry_created']                 = 'Létrehozva';
$_['entry_creator']                 = 'Szerző';
$_['entry_content']                 = 'Tartalom:';
$_['entry_image']                   = 'Kép';
$_['entry_video_code']              = 'Videó Kód (Youtube Code, Vimeo Code)';
$_['entry_meta_title']              = 'Meta Cím';
$_['entry_meta_keyword']            = 'Meta Kulcsszó';
$_['entry_meta_description']        = 'Meta Leírás';
/**COMMENTS PAGE **/
$_['comments_page_heading_title']   = 'Hozzászólások Kezelése';
$_['text_comment']                  = 'Hozzászólás';
$_['text_blog_title']               = 'Blog Cím';
$_['text_username']                 = 'Felhasználó';
$_['text_email']                    = 'Email';
$_['button_publish']                = 'Közzététel';
$_['button_unpublish']              = 'Közzététel visszavonása';
$_['text_status']                   = 'Állapot';
$_['text_confirm_delete']           = 'Biztosan ki szeretné törölni?';
$_['text_disable']                  = 'Tiltás';
$_['text_enable']                   = 'Engedélyezés';


/** GENERAL SETTING **/
$_['entry_children_columns']            = 'Gyerek Oszlopok';
$_['entry_category_image_demension']    = 'Kategória Kép Méret (Szélesség x Magasság) ';
$_['modules_page_heading_title']        = 'Általános & Modul Beállítások';
$_['text_general_setting']              = 'Általános Beállítások';
$_['entry_large_image_demension']       = 'Blog - Nagy Kép Méret (Szélesség x Magasság)';
$_['entry_small_image_demension']       = 'Blog - Kis Kép Méret (Szélesség x Magasság)';
$_['entry_xsmall_image_demension']      = 'Blog - Apró Kép Méret (Szélesség x Magasság)';
$_['entry_rss_limit']                   = 'Blogo Limit RSS-ben';
$_['text_category_setting']             = 'Kategória Oldal Beállítás';
$_['entry_limit_leading_blogs']         = 'Vezető Blog Limit';
$_['entry_limit_secondary_blogs']       = 'Másodlagos Blog Limit';
$_['entry_secondary_image_types']       = 'Másodlagos Kép Típusok';
$_['entry_leading_image_types']         = 'Kép Típusok';
$_['text_large_image']                  = 'Nagy Kép';
$_['text_small_image']                  = 'Kis Kép';
$_['entry_secondary_image_types']       = 'Másodlagos Kép Típusok';
$_['entry_show_title']                  = 'Cím megjelenítése';
$_['entry_show_image']                  = 'Kép megjelenítése';
$_['entry_show_author']                 = 'Szerző megjelenítése';
$_['entry_show_category']               = 'Kategória megjelenítése';
$_['entry_show_created']                = 'Létrehozva megjelenítése (Létrehozás dátuma)';
$_['entry_show_comment_counter']        = 'Hozzászólás Számláló megjelenítése';
$_['entry_show_description']            = 'Leírás megjelenítése' ;
$_['entry_show_readmore']               = 'Bővebben megjelenítése' ;
$_['entry_show_hits']                   = 'Megtekintések megjelenítése' ;
$_['text_blog_setting']                 = 'Blog Beállítások';
$_['entry_blog_image_types']            = 'Blog Kép Típusok';
$_['blog_secondary_image_types']        = 'Másodlagos Kép Típusok';
$_['blog_show_title']                   = 'Cím megjelenítése';
$_['blog_show_image']                   = 'Kép megjelenítése';
$_['blog_show_author']                  = 'Szerző megjelenítése';
$_['blog_show_category']                = 'Kategória megjelenítése';
$_['blog_show_created']                 = 'Létrehozva megjelenítése';
$_['blog_show_comment_counter']         = 'Hozzászólás Számláló megjelenítése';
$_['entry_show_comment_form']           = 'Hozzászólás Űrlap megjelenítése';

$_['text_modules_setting'] = 'Modulok Beállítása';

$_['text_categories_module'] = 'Kategória Menü Modulok';
$_['text_latest_blog_module'] = 'Legutóbbi Blog Modulok';
$_['text_latest_comment_module'] = 'Legutóbbi Hozzászólás Modulok';

$_['entry_columns_secondary_blogs'] = 'Oszlopok Másodlagos Blogok';
$_['entry_columns_leading_blogs'] = 'Oszlopok Vezető Blogs';


$_['entry_comment_engine'] = 'Hozzászólás Motor';
$_['entry_diquis_account'] = 'Diquis Képernyő Név';
$_['text_signup_diquis'] = 'Disquis Bejelentkezés';
$_['entry_facebook_appid'] = 'Facebook Application ID';
$_['entry_comment_limit'] = 'Hozzászólás Limit';
$_['entry_facebook_width'] = 'Facebook Szélesség';
$_['entry_auto_publish_comment'] = 'Hozzászólások automatikus közzététele';

$_['entry_enable_recaptcha'] = 'ReCaptCha Engedélyezése';
$_['entry_recaptcha_public_key'] = 'ReCaptCha közzététele';
$_['entry_recaptcha_private_key'] = 'Privát Kulcs';
$_['text_register_recaptcha'] = 'Recaptcha Regisztráció';
$_['tab_pavblogcategory'] = 'Kategória modul beállítások';
$_['tab_pavblogcomment'] = 'Legutóbbi hozzászólás modul beállítások';
$_['tab_pavbloglatest'] = 'Legutóbbi modul beállítások';
$_['text_showcase']	= 'Kirakat';
$_['menu_frontend_module_setting'] = 'Frontend modulok';


?>
