<?php

$_['heading_title']		        ='PostaPont';


$_['text_shipping']		        ='Szállítás';
$_['text_success']		        ='Sikeres Módosítás';

$_['button_save']			    ='Mentés';
$_['button_cancel']			    ='Kilépés';
$_['other_setting']			    ='További Beállítások';


$_['entry_cost']		        ='Költség:';
$_['entry_free_total']          ='Ingyenes szállítás:<br /><span class="help">Részösszeg szükséges, mielőtt az ingyenes szállítás elérhető.</span>';
$_['entry_tax']			        ='Adóosztály:';
$_['entry_tax_class']	        ='Adóosztály:';
$_['entry_geo_zone']	        ='Földrajzi zóna:';
$_['entry_status']		        ='Állapot:';
$_['entry_status_postapont']    ='PostaPontok megjelenítése:';
$_['entry_status_mol']		    ='MOL pontok megjelenítése:';
$_['entry_status_coop']		    ='Coop pontok megjelenítése:';
$_['entry_status_automata']	    ='Csomagautomaták megjelenítése:';
$_['entry_sort_order']	        ='Sorrend:';
$_['entry_google_maps_api_key']	='Google Maps API key (opcionális):';
$_['google_maps_api_key_help'] ='Nagy forgalmú webáruházak esetén ajánlott';


// Error
$_['error_permission']          = 'HIBA TÖRTÉNT!';
?>