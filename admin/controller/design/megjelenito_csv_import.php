<?php
class ControllerDesignMegjelenitoCsvImport extends Controller {
    private $error = array();

    public function index() {

        $this->load->language('design/megjelenito_csv');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            foreach($this->request->post as $key=>$value) {
                $beallit[$key] = $value;
            }

            if (isset($beallit['csv_beallitasok_import'])) {
              /*  foreach($beallit['csv_beallitasok_import'] as $key=>$value) {
                    if (isset($beallit['csv_beallitasok_import'][$key]['mezok'])) {
                        $beallit['csv_beallitasok_import'][$key]['mezok'] = $this->config->rendezes($beallit['csv_beallitasok_import'][$key]['mezok'],"sort_order");
                    }
                }*/

               // $beallit['csv_mezo_sort'] = $this->mezoRendez($beallit['csv_beallitasok_import']);
                $this->document->setTitle($this->language->get('heading_title'));
                $this->load->model('setting/setting');

                $this->model_setting_setting->editSetting('csv_beallitasok_import', $beallit);

                $this->load->model('setting/extension');

                $this->session->data['success'] = $this->language->get('text_success');
            }


            $this->redirect($this->url->link('design/megjelenito_csv_import', 'nyitott_sor='.$this->request->post['nyitott_sor'].'&token=' . $this->session->data['token'], 'SSL'));
        }

        $this->load->model('catalog/information');
        $this->data['information_pages'] 	= $this->model_catalog_information->getInformations();


        $this->data['text_yes']         = $this->language->get('text_yes');
        $this->data['text_no']          = $this->language->get('text_no');
        $this->data['button_save']      = $this->language->get('button_save');
        $this->data['button_cancel']    = $this->language->get('button_cancel');
        $this->data['button_remove']    = $this->language->get('button_remove');
        $this->data['text_select_all']  = $this->language->get('text_select_all');
        $this->data['text_unselect_all']= $this->language->get('text_unselect_all');
        $this->data['entry_status']     = $this->language->get('entry_status');

        if (isset($this->session->data['success']))
            $this->data['success'] = $this->language->get('text_success');
        else
            $this->data['success'] = false;

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('design/megjelenito_csv_import', 'token=' . $this->session->data['token'], 'SSL'),

            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('design/megjelenito_csv_import', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['token'] = $this->session->data['token'];

        $this->data['nyitott_sor'] = isset($this->request->request['nyitott_sor']) ? $this->request->request['nyitott_sor'] : "";


        $query= $this->db->query("show tables");
        foreach($query->rows as $tablak) {
            foreach($tablak as $tabla) {
                $mezok = $this->db->query("SHOW columns from ".$tabla);
                $szerkezet[$tabla] = $mezok->rows;
            }
        }

        $this->data['szerkezet'] = $szerkezet;


        /* header tpl szerkesztő */
        $csv_beallitasok_import = $this->config->get('csv_beallitasok_import');
        if (is_null($csv_beallitasok_import)) {
            $csv_beallitasok_import = array();
        }
        $this->data['csv_beallitasok_import'] = $csv_beallitasok_import;

        $this->template = 'design/megjelenito_csv_import.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function mezoRendez($tomb) {

        $vissza = array();
        foreach($tomb as $csv_sor) {
            $gyujto = array();
            if (isset($csv_sor['tabla'])) {
                foreach($csv_sor['tabla'] as $tablak) {
                    if (isset($tablak['mezo_name'])) {
                        foreach($tablak['mezo_name'] as $mezo) {
                            $gyujto[] = array(
                                'mezo'       => $tablak['name'].".".$mezo['neve'],
                                'sort_order' => $mezo['sort_order']
                            );
                        }
                    }
                }
                $vissza[$csv_sor['export_name']] = $this->config->rendezes($gyujto,"sort_order");
            }
        }
        return $vissza;
    }


}
?>