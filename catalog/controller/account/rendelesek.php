<?php
class ControllerAccountRendelesek extends Controller {
    public function index() {
        $this->load->language('account/rendelesek');

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->request->get['filter_date_start'])) {
            $filter_date_start = $this->request->get['filter_date_start'];
        } else {
            $filter_date_start = '';
        }

        if (isset($this->request->get['filter_date_end'])) {
            $filter_date_end = $this->request->get['filter_date_end'];
        } else {
            $filter_date_end = '';
        }

        if (isset($this->request->get['filter_korosztaly'])) {
            $filter_korosztaly = $this->request->get['filter_korosztaly'];
        } else {
            $filter_korosztaly = '';
        }

        if (isset($this->request->get['filter_nem'])) {
            $filter_nem = $this->request->get['filter_nem'];
        } else {
            $filter_nem = '';
        }

        if (isset($this->request->get['filter_iskolai_vegzettseg'])) {
            $filter_iskolai_vegzettseg = $this->request->get['filter_iskolai_vegzettseg'];
        } else {
            $filter_iskolai_vegzettseg = '';
        }

        if (isset($this->request->get['filter_group'])) {
            $filter_group = $this->request->get['filter_group'];
        } else {
            $filter_group = 'week';
        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $filter_order_status_id = $this->request->get['filter_order_status_id'];
        } else {
            $filter_order_status_id = 0;
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['filter_korosztaly'])) {
            $url .= '&filter_korosztaly=' . $this->request->get['filter_korosztaly'];
        }

        if (isset($this->request->get['filter_nem'])) {
            $url .= '&filter_nem=' . $this->request->get['filter_nem'];
        }

        if (isset($this->request->get['filter_group'])) {
            $url .= '&filter_group=' . $this->request->get['filter_group'];
        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', '', 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('account/rendelesek', $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->load->model('account/rendelesek');

        $this->data['orders'] = array();

        $data = array(
            'filter_date_start'	                => $filter_date_start,
            'filter_date_end'	                => $filter_date_end,
            'filter_group'                      => $filter_group,
            'filter_korosztaly'                 => $filter_korosztaly,
            'filter_nem'                        => $filter_nem,
            'filter_iskolai_vegzettseg'         => $filter_iskolai_vegzettseg,
            'filter_order_status_id'            => $filter_order_status_id,
            'start'                             => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'                             => $this->config->get('config_admin_limit')
        );


        $cOrderID = $this->model_account_rendelesek->getcustormProduct();

        $order_total = $this->model_account_rendelesek->getTotalOrders($data,$cOrderID);

        $results = $this->model_account_rendelesek->getOrders($data,$cOrderID);

        foreach ($results as $result) {
            $this->data['orders'][] = array(
                'date_start' => date($this->language->get('date_format_short'), strtotime($result['date_start'])),
                'date_end'   => date($this->language->get('date_format_short'), strtotime($result['date_end'])),
                'orders'     => $result['orders'],
                'products'   => $result['products'],
                'tax'        => $this->currency->format($result['tax'], $this->config->get('config_currency')),
                'total'      => $this->currency->format($result['total'], $this->config->get('config_currency'))
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_all_status'] = $this->language->get('text_all_status');

        $this->data['column_date_start'] = $this->language->get('column_date_start');
        $this->data['column_date_end'] = $this->language->get('column_date_end');
        $this->data['column_orders'] = $this->language->get('column_orders');
        $this->data['column_products'] = $this->language->get('column_products');
        $this->data['column_tax'] = $this->language->get('column_tax');
        $this->data['column_total'] = $this->language->get('column_total');
        $this->data['column_iskolai_vegzettseg'] = $this->language->get('column_iskolai_vegzettseg');

        $this->data['entry_date_start'] = $this->language->get('entry_date_start');
        $this->data['entry_date_end'] = $this->language->get('entry_date_end');
        $this->data['entry_group'] = $this->language->get('entry_group');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_korosztaly'] = $this->language->get('entry_korosztaly');
        $this->data['entry_nem'] = $this->language->get('entry_nem');
        $this->data['entry_ingyenes'] = $this->language->get('entry_ingyenes');
        $this->data['entry_iskolai_vegzettseg'] = $this->language->get('entry_iskolai_vegzettseg');
        $this->data['button_filter'] = $this->language->get('button_filter');
        $this->data['button_nyomtatas'] = $this->language->get('button_nyomtatas');
        $this->data['button_exportalas'] = $this->language->get('button_exportalas');

        $this->load->model('localisation/order_status');

        $this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $this->data['groups'] = array();

        $this->data['groups'][] = array(
            'text'  => $this->language->get('text_year'),
            'value' => 'year',
        );

        $this->data['groups'][] = array(
            'text'  => $this->language->get('text_month'),
            'value' => 'month',
        );

        $this->data['groups'][] = array(
            'text'  => $this->language->get('text_week'),
            'value' => 'week',
        );

        $this->data['groups'][] = array(
            'text'  => $this->language->get('text_day'),
            'value' => 'day',
        );


        $this->load->model('localisation/country');
        $this->data['eletkors'] = $this->model_localisation_country->getEletkor();


        $this->data['nems'][] = array(
            'ertek' => 1,
            'nev'   => $this->language->get('select_ferfi')
        );
        $this->data['nems'][] = array(
            'ertek' => 2,
            'nev'   => $this->language->get('select_holgy')
        );

        $this->load->model('account/iskolaivegzettseg');

        $this->data['iskolai_vegzettsegek'] = $this->model_account_iskolaivegzettseg->getAll();

        $this->data['ingyenesek'][] = array(
            'ertek' => 0,
            'nev'   => $this->language->get('select_utalvany')
        );
        $this->data['ingyenesek'][] = array(
            'ertek' => 1,
            'nev'   => $this->language->get('select_kupon')
        );

        $url = '';

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['filter_group'])) {
            $url .= '&filter_group=' . $this->request->get['filter_group'];
        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
        }

        if (isset($this->request->get['filter_korosztaly'])) {
            $url .= '&filter_korosztaly=' . $this->request->get['filter_korosztaly'];
        }
        if (isset($this->request->get['filter_nem'])) {
            $url .= '&filter_nem=' . $this->request->get['filter_nem'];
        }

        if (isset($this->request->get['filter_iskolai_vegzettseg'])) {
            $url .= '&filter_iskolai_vegzettseg=' . $this->request->get['filter_iskolai_vegzettseg'];
        }

        $this->data['ceges'] = $this->customer->getFeltolto();

        $this->data['csv_be']  = $this->url->link('account/rendelesek_csvbe', $url, 'SSL');

        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('account/rendelesek', $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['filter_date_start'] = $filter_date_start;
        $this->data['filter_date_end'] = $filter_date_end;
        $this->data['filter_group'] = $filter_group;
        $this->data['filter_order_status_id'] = $filter_order_status_id;
        $this->data['filter_korosztaly']        = $filter_korosztaly;
        $this->data['filter_nem']               = $filter_nem;
        $this->data['filter_iskolai_vegzettseg']               = $filter_iskolai_vegzettseg;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/rendelesek.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/rendelesek.tpl';
        } else {
            $this->template = 'default/template/account/rendelesek.tpl';
        }


        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }

}
?>
