<?php   
class ControllerCronCrmMegrendelesSzamlazzKiegyenlitve extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;

        $this->load->model("cron/altalanos");
        $this->load->model('cron/crm_megrendeles_szamlazz_kiegyenlitve');

        echo 'Elindítva<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $orders = $this->model_cron_crm_megrendeles_szamlazz_kiegyenlitve->getOrders();

        echo '<br><br>Rendelések összesen: '.count($orders).' db';
        $this->model_cron_altalanos->addFuttatasEredmeny('Számlák összesen: '.count($orders).' db');


        if ($orders) {

            foreach($orders as $key=>$value) {
                $szamlazz = $this->model_cron_crm_megrendeles_szamlazz_kiegyenlitve->getOrder($value["salesorderid"]);


                if ($szamlazz) {
                    $szamlazz['invoiceid'] = $szamlazz['salesorderid'];
                    $crmid = $this->model_cron_crm_megrendeles_szamlazz_kiegyenlitve->createSzamlaToCrm($szamlazz);
                    if (!empty($szamlazz['szamlaszam'])) {
                        if ($this->model_cron_altalanos->sendCrmToSzamlazzKiegyenlitve($szamlazz)) {
                            $this->model_cron_crm_megrendeles_szamlazz_kiegyenlitve->deleteTriggerOrder($value);
                        } else {
                            $this->model_cron_altalanos->addFuttatasEredmeny('Sikertelen továbbítás.<br>Számlaszám: '.$szamlazz['szamlaszam'].'<br>salesorderid:'.$szamlazz['salesorderid']);
                            echo '<br>Sikertelen továbbítás.<br>Számlaszám: '.$szamlazz['szamlaszam'].'<br>salesorderid:'.$szamlazz['salesorderid'];
                        }
                    } else {
                        $this->model_cron_crm_megrendeles_szamlazz_kiegyenlitve->createNewInvoice($crmid,"Számla");
                        $this->model_cron_crm_megrendeles_szamlazz_kiegyenlitve->deleteTriggerOrder($value);
                    }
                }
            }
        }


        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}

