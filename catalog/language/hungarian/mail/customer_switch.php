<?php
// Text
$_['text_subject']  = '%s - Vásárlói csoportja le lett cserélve.';
$_['text_welcome']  = 'Nagyon köszönjük hogy nálunk vásárolt: %s!';
$_['text_login']    = 'A fiókja megváltozott, át lett helyezve másik vásárlói csoportba: %s, az alábbi linken be tud jelentkezni az email címével és jelszavával:';
$_['text_services'] = 'Upon switched to new group, you will be able to access different services special offers, discounts.';
$_['text_services'] = 'Amint átkerült az új vásárlói csoportba, hozzáférst kap a különleges ajánlatokról, leárazásokról.';
$_['text_thanks']   = 'Köszönjük,';

//admin
$_['text_admin_subject']   = '%s - A vásárló átkerült az új csoportba: %s.';
$_['text_admin_content']   = 'A vásárló ezzel az email címmel: %s átkerült ebbe a csoportba: %s.';
?>