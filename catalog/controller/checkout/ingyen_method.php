<?php

class ControllerCheckoutIngyenMethod extends Controller {
    private $convertPDFWithImagick = true;

    public function index() {
        $this->language->load('checkout/checkout');

        $this->load->model('account/address');


        $this->data['text_modify'] = $this->language->get('text_modify');
        $this->data['text_comments'] = $this->language->get('text_comments');
        $this->data['entry_letoltesek'] = $this->language->get('entry_letoltesek');
        $this->data['entry_letoltesek_kep'] = $this->language->get('entry_letoltesek_kep');
        $this->data['entry_bevaltasi_helyek'] = $this->language->get('entry_bevaltasi_helyek');
        $this->data['entry_alapertelmezett'] = $this->language->get('entry_alapertelmezett');
        $this->data['button_letoltes'] = $this->language->get('button_letoltes');
        $this->data['button_nyomtatas'] = $this->language->get('button_nyomtatas');

        $this->data['button_continue'] = $this->language->get('button_continue');

        if (isset($this->session->data['comment'])) {
            $this->data['comment'] = $this->session->data['comment'];
        } else {
            $this->data['comment'] = '';
        }

        $products = $this->cart->getProducts();

        $this->load->model('catalog/productimageplaces');
        $this->load->model('tool/image');
        $productIDRaw = $this->session->data['cart'];
        $productImageInPlaces = array();
        $productDatas = array();
        foreach($productIDRaw as $productID=>$foo) {
            $productImageInPlacesRaws = $this->model_catalog_productimageplaces->getImagesByProductID($productID);

            $productImageInPlacesData = array();
            foreach($productImageInPlacesRaws as $productImageInPlacesRaw) {
                if(isset($productImageInPlacesRaw['image'])) {
                    $imageName = $productImageInPlacesRaw['image'];
                } else {
                    $imageName = false;
                }
                $productImageInPlacesData[] = array(
                    'id'            => $productImageInPlacesRaw['imagebylocations_id'],
                    'image'         => ($imageName !== false) ? $this->model_tool_image->resize($imageName, 100, 100) : '',
                    'name'          => $productImageInPlacesRaw['location_name']
                );
            }
            $productImageInPlaces[$productID] = $productImageInPlacesData;

            if(array_key_exists($productID, $products)) {
                $product = $products[$productID];
                $productDatas[$productID] = array(
                    'id'            => $product['key'],
                    'name'          => $product['name'],
                    'thumb'         => isset($product['letoltheto']) ? $this->model_tool_image->resize($product['letoltheto'], 100, 100) : ''
                );
            }

        }
        $this->data['productImageInPlacesALL'] = $productImageInPlaces;
        $this->data['productDatas'] = $productDatas;

        $this->data['shipping_required'] = $_REQUEST['shipping_required'];
        $this->data['fizetos'] = $_REQUEST['fizetos'];

        $this->data['letoltesek'] = array();
        foreach ($products as $product) {
            if ($product['utalvany'] == 1) {
                $this->data['letoltesek'][] = $product;
            }

        }


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/ingyen_method.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/checkout/ingyen_method.tpl';
        } else {
            $this->template = 'default/template/checkout/ingyen_method.tpl';
        }

        $this->response->setOutput($this->render());
    }


    private function generatePDF($newDir, $image) {
        if($this->convertPDFWithImagick) {
            $this->generateImagesWithImagick($newDir, $image);
        } else {
            $this->generateImagesWithConvert($newDir, $image);
        }
    }

    private function generateImagesWithImagick($newDir, $image) {
        if(!file_exists($newDir)) {
            mkdir($newDir);
            $filenameArray = array();
            if(preg_match('/\/([^\/]+)\.pdf$/', $image, $filenameArray)) {
                $imagick = new Imagick();
                $imagick->setresolution(800, 800);
                $imagick->readImage(DIR_IMAGE.$image);
                //$imagick->setimageresolution(600, 600);
                $imagick->scaleimage(800, 0);
                $imagick->setImageCompression(imagick::COMPRESSION_LOSSLESSJPEG);
                $imagick->setImageCompressionQuality(100);
                $imagick->flattenimages();
                $pages = $imagick->getnumberimages();
                for($n =0 ; $n < $pages; $n++) {
                    $imagick->setiteratorindex($n);
                    $imagick->setimageformat('jpeg');
                    $imagick->writeimage($newDir."/".$filenameArray[1]."-".$n.".jpg");
                }

            }
        }
    }

    private function generateImagesWithConvert($newDir, $image) {
        if(!file_exists($newDir)) {
            mkdir($newDir);
            $filenameArray = array();
            if(preg_match('/\/([^\/]+)\.pdf$/', $image, $filenameArray)) {
                $newFile = $newDir."/".$filenameArray[1].".jpg";
                exec('convert -density 300 "'.DIR_IMAGE.$image.'" "'.$newFile.'"');
            }
        }
    }

    private function addImage($concatPDF, $imagePath) {
        $concatPDF->AddPage();
        list($width, $height) = getimagesize($imagePath);

        $widthScale = 200 / $width;
        $heightScale = 200 / $height;

        $scale = min($widthScale, $heightScale);

        $sizeX = $scale * $width;
        $sizeY = $scale * $height;

        $createdImage = $concatPDF->Image($imagePath, 0, 0, $sizeX, $sizeY);
        $concatPDF->Cell(0, 0, $createdImage, 0, 1, 'L');
    }

    /**
     * @param ConcatPDF $concatPDF
     * @return bool
     */
    private function generateOutputPDF($concatPDF) {
        if(file_exists('kupon.pdf')) {
            unlink('kupon.pdf');
        }

        if(!isset($this->request->request['jsondata'])) {
            return false;
        }
        $data = json_decode(html_entity_decode($this->request->request['jsondata']), true);

        $this->load->model('catalog/product');
        $this->load->model('catalog/productimageplaces');

        $defaults = array();
        $images = array();
        foreach($data as $productID=>$needArray) {
            $product = $this->model_catalog_product->getProduct($productID);
            if(array_key_exists('defaultImage', $needArray) && array_key_exists('letoltheto', $product)) {
                if(file_exists(DIR_IMAGE.$product['letoltheto'])) {
                    $defaults[$productID] = $product['letoltheto'];
                }
            }
            if(array_key_exists('placeImageID', $needArray)) {
                $productImagesRaw = $this->model_catalog_productimageplaces->getImagesByProductID($productID);
                $productImages = array();
                foreach($productImagesRaw as $productImageRaw) {
                    $productImages[$productImageRaw['imagebylocations_id']] = $productImageRaw;
                }
                foreach($needArray['placeImageID'] as $placeImageID=>$foo) {
                    if(array_key_exists($placeImageID, $productImages) && array_key_exists('image', $productImages[$placeImageID]) && $productImages[$placeImageID]['image'] != '') {
                        if($productImages[$placeImageID]['image'] == null) {
                            continue;
                        }
                        if(!file_exists(DIR_IMAGE.$productImages[$placeImageID]['image'])) {
                            continue;
                        }
                        $images[$placeImageID] = $productImages[$placeImageID]['image'];
                    }
                }
            }
        }

        foreach($images as $imageID=>$image) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_file($finfo, DIR_IMAGE.$image);
            finfo_close($finfo);
            if($mimeType == 'application/pdf') {
                if(!file_exists(DIR_CACHE."PDF")) {
                    mkdir(DIR_CACHE."PDF");
                }
                if(!file_exists(DIR_CACHE."PDF/placeImages")) {
                    mkdir(DIR_CACHE."PDF/placeImages");
                }
                $newDir = DIR_CACHE."PDF/placeImages/".$imageID;
                $this->generatePDF($newDir, $image);
                $it = new RegexIterator(new DirectoryIterator($newDir), "/\\.jpg\$/i");

                unset($images[$imageID]);
                /**
                 * @var DirectoryIterator $filename
                 */
                foreach ($it as $filename) {
                    $imagePath = $newDir."/".$filename->getFilename();

                    $this->addImage($concatPDF, $imagePath);

                }
            } else {
                $this->addImage($concatPDF, DIR_IMAGE.$image);
            }
        }

        foreach($defaults as $imageID=>$image) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_file($finfo, DIR_IMAGE.$image);
            finfo_close($finfo);
            if($mimeType == 'application/pdf') {
                if(!file_exists(DIR_CACHE."PDF")) {
                    mkdir(DIR_CACHE."PDF");
                }
                if(!file_exists(DIR_CACHE."PDF/defaultImages")) {
                    mkdir(DIR_CACHE."PDF/defaultImages");
                }
                $newDir = DIR_CACHE."PDF/defaultImages/".$imageID;
                $this->generatePDF($newDir, $image);
                $it = new RegexIterator(new DirectoryIterator($newDir), "/\\.jpg\$/i");

                unset($images[$imageID]);
                foreach ($it as $filename) {
                    $imagePath = $newDir."/".$filename->getFilename();

                    $this->addImage($concatPDF, $imagePath);

                }
            } else {
                $this->addImage($concatPDF, DIR_IMAGE.$image);
            }
        }
        return true;
    }

    public function letoltes() {
        require_once('catalog/model/tool/ConcatPDF.php');
        $concatPDF = new ConcatPdf();
        $concatPDF->SetFont('Arial', 'B', 16);

        if($this->generateOutputPDF($concatPDF)) {
            $concatPDF->concat();
            $concatPDF->Output('kupon.pdf', 'D');
            $concatPDF->Close();
        }

        return true;
    }

    public function nyomtatas() {
        require_once('catalog/model/tool/ConcatPDF.php');
        $concatPDF = new ConcatPdf();
        $concatPDF->SetFont('Arial', 'B', 16);

        if($this->generateOutputPDF($concatPDF)) {
            $concatPDF->concat();
            $concatPDF->Output('kupon.pdf', 'F');
            $concatPDF->Close();
        }
    }

    public function nyomtatas1() {

        if (isset($_REQUEST) && count($_REQUEST) > 1) {
        if (file_exists("kupon.zip")) {
            unlink("kupon.zip");
            $this->nyomtatas();
        }
        else if(!file_exists("kupon.zip")){

                $files_to_zip = array();
                foreach ($_REQUEST['tombom'] as $value) {
                    $files_to_zip[] = DIR_IMAGE.$value;
                }

            $this->create_zip($files_to_zip,"kupon.zip");

            $mask ="kupon.zip";
            header('Content-Description: File Transfer');
            header('Content-Type: application/zip');
            header('Content-Disposition: attachment; filename="' . $mask . '"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($mask));
            readfile($mask, 'rb');
            }
        }
    }

    public function tovabb() {

        if (!isset($this->session->data['order_id']) ) {

            $products = $this->cart->getProducts(true);

            foreach ($products as $product) {
                if ($product['utalvany'] != 1) {
                    $product_total = 0;

                    foreach ($products as $product_2) {
                        if ($product_2['product_id'] == $product['product_id']) {
                            $product_total += $product_2['quantity'];
                        }
                    }

                    if ($product['minimum'] > $product_total) {
                        $redirect = $this->url->link('checkout/cart');

                        break;
                    }
                }
            }

            $total_data = array();
            $total = 0;
            $taxes = $this->cart->getTaxes();

            $this->load->model('setting/extension');

            $sort_order = array();

            $results = $this->model_setting_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                }
            }

            $sort_order = array();

            foreach ($total_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $total_data);

            $this->language->load('checkout/checkout');

            $data = array();

            $data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
            $data['store_id'] = $this->config->get('config_store_id');
            $data['store_name'] = $this->config->get('config_name');

            if ($data['store_id']) {
                $data['store_url'] = $this->config->get('config_url');
            } else {
                $data['store_url'] = HTTP_SERVER;
            }

            if ($this->customer->isLogged()) {
                $data['customer_id'] = $this->customer->getId();
                $data['customer_group_id'] = $this->customer->getCustomerGroupId();
                $data['firstname'] = $this->customer->getFirstName();
                $data['lastname'] = $this->customer->getLastName();
                $data['email'] = $this->customer->getEmail();
                $data['telephone'] = $this->customer->getTelephone();
                $data['fax'] = $this->customer->getFax();
                $data['nem'] = $this->customer->getNem();
                $data['eletkor'] = $this->customer->getEletkor();

                $this->load->model('account/address');

                $payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
            } elseif (isset($this->session->data['guest'])) {
                $data['customer_id'] = 0;
                $data['customer_group_id'] = $this->config->get('config_customer_group_id');
                $data['firstname'] = $this->session->data['guest']['firstname'];
                $data['lastname'] = $this->session->data['guest']['lastname'];
                $data['email'] = $this->session->data['guest']['email'];
                $data['telephone'] = $this->session->data['guest']['telephone'];
                $data['fax'] = $this->session->data['guest']['fax'];
                $data['nem'] = $this->session->data['guest']['nem'];
                $data['eletkor'] = $this->session->data['guest']['eletkor'];

                $payment_address = $this->session->data['guest']['payment'];
            }

            $data['payment_firstname'] = $payment_address['firstname'];
            $data['payment_lastname'] = $payment_address['lastname'];
            $data['payment_company'] = $payment_address['company'];
            $data['payment_adoszam'] = $payment_address['adoszam'];
            $data['payment_address_1'] = $payment_address['address_1'];
            $data['payment_address_2'] = $payment_address['address_2'];
            $data['payment_city'] = $payment_address['city'];
            $data['payment_postcode'] = $payment_address['postcode'];
            $data['payment_zone'] = $payment_address['zone'];
            $data['payment_zone_id'] = $payment_address['zone_id'];
            $data['payment_country'] = $payment_address['country'];
            $data['payment_country_id'] = $payment_address['country_id'];
            $data['payment_address_format'] = $payment_address['address_format'];

            if (isset($this->session->data['payment_method']['title'])) {
                $data['payment_method'] = $this->session->data['payment_method']['title'];
            } else {
                $data['payment_method'] = '';
            }

            if (isset($this->session->data['payment_method']['code'])) {
                $data['payment_code'] = $this->session->data['payment_method']['code'];
            } else {
                $data['payment_code'] = '';
            }

            if ($this->cart->hasShipping()) {
                if ($this->customer->isLogged()) {
                    $this->load->model('account/address');

                    $shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);
                } elseif (isset($this->session->data['guest'])) {
                    $shipping_address = $this->session->data['guest']['shipping'];
                }

                $data['shipping_firstname'] = $shipping_address['firstname'];
                $data['shipping_lastname'] = $shipping_address['lastname'];
                $data['shipping_company'] = $shipping_address['company'];
                $data['shipping_adoszam'] = $shipping_address['adoszam'];
                $data['shipping_address_1'] = $shipping_address['address_1'];
                $data['shipping_address_2'] = $shipping_address['address_2'];
                $data['shipping_city'] = $shipping_address['city'];
                $data['shipping_postcode'] = $shipping_address['postcode'];
                $data['shipping_zone'] = $shipping_address['zone'];
                $data['shipping_zone_id'] = $shipping_address['zone_id'];
                $data['shipping_country'] = $shipping_address['country'];
                $data['shipping_country_id'] = $shipping_address['country_id'];
                $data['shipping_address_format'] = $shipping_address['address_format'];

                if (isset($this->session->data['shipping_method']['title'])) {
                    $data['shipping_method'] = $this->session->data['shipping_method']['title'];
                } else {
                    $data['shipping_method'] = '';
                }

                if (isset($this->session->data['shipping_method']['code'])) {
                    $data['shipping_code'] = $this->session->data['shipping_method']['code'];
                } else {
                    $data['shipping_code'] = '';
                }
            } else {
                $data['shipping_firstname'] = '';
                $data['shipping_lastname'] = '';
                $data['shipping_company'] = '';
                $data['shipping_adoszam'] = '';
                $data['shipping_address_1'] = '';
                $data['shipping_address_2'] = '';
                $data['shipping_city'] = '';
                $data['shipping_postcode'] = '';
                $data['shipping_zone'] = '';
                $data['shipping_zone_id'] = '';
                $data['shipping_country'] = '';
                $data['shipping_country_id'] = '';
                $data['shipping_address_format'] = '';
                $data['shipping_method'] = '';
                $data['shipping_code'] = '';
            }

            $product_data = array();

            foreach ( $this->cart->getProducts() as $product) {
                if ($product['utalvany'] != 1 || true) {
                    $option_data = array();

                    foreach ($product['option'] as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['option_value'];
                        } else {
                            $value = $this->encryption->decrypt($option['option_value']);
                        }

                        $option_data[] = array(
                            'product_option_id'       => $option['product_option_id'],
                            'product_option_value_id' => $option['product_option_value_id'],
                            'option_id'               => $option['option_id'],
                            'option_value_id'         => $option['option_value_id'],
                            'name'                    => $option['name'],
                            'value'                   => $value,
                            'type'                    => $option['type'],
                            'price'                   => $option['price']
                        );
                    }

                    $product_data[] = array(
                        'product_id' => $product['product_id'],
                        'name'       => $product['name'],
                        'model'      => $product['model'],
                        'option'     => $option_data,
                        'download'   => $product['download'],
                        'quantity'   => $product['quantity'],
                        'subtract'   => $product['subtract'],
                        'price'      => $product['price'],
                        'total'      => $product['total'],
                        'tax'        => $this->tax->getTax($product['total'], $product['tax_class_id']),
                        'reward'     => $product['reward'],
                        'ingyenes'   => $product['utalvany']

                    );
                }
            }

            // Gift Voucher
            $voucher_data = array();

            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $voucher) {
                    $voucher_data[] = array(
                        'description'      => $voucher['description'],
                        'code'             => substr(md5(rand()), 0, 7),
                        'to_name'          => $voucher['to_name'],
                        'to_email'         => $voucher['to_email'],
                        'from_name'        => $voucher['from_name'],
                        'from_email'       => $voucher['from_email'],
                        'voucher_theme_id' => $voucher['voucher_theme_id'],
                        'message'          => $voucher['message'],
                        'amount'           => $voucher['amount']
                    );
                }
            }

            $data['products'] = $product_data;
            $data['vouchers'] = $voucher_data;
            $data['totals'] = $total_data;
            $data['comment'] = isset($this->session->data['comment']) ? $this->session->data['comment'] : "";
            $data['total'] = $total;

            if (isset($this->request->cookie['tracking'])) {
                $this->load->model('affiliate/affiliate');

                $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

                if ($affiliate_info) {
                    $data['affiliate_id'] = $affiliate_info['affiliate_id'];
                    $data['commission'] = ($total / 100) * $affiliate_info['commission'];
                } else {
                    $data['affiliate_id'] = 0;
                    $data['commission'] = 0;
                }
            } else {
                $data['affiliate_id'] = 0;
                $data['commission'] = 0;
            }

            $data['language_id'] = $this->config->get('config_language_id');
            $data['currency_id'] = $this->currency->getId();
            $data['currency_code'] = $this->currency->getCode();
            $data['currency_value'] = $this->currency->getValue($this->currency->getCode());
            $data['ip'] = $this->request->server['REMOTE_ADDR'];

            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                $data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif(!empty($this->request->server['HTTP_CLIENT_IP'])) {
                $data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                $data['forwarded_ip'] = '';
            }

            if (isset($this->request->server['HTTP_USER_AGENT'])) {
                $data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
            } else {
                $data['user_agent'] = '';
            }

            if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                $data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
            } else {
                $data['accept_language'] = '';
            }

            $this->load->model('checkout/order');

            $this->session->data['order_id'] = $this->model_checkout_order->addOrder($data);
        }
        $this->load->model('checkout/order');
        $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('cod_order_status_id'),"",false,true);

        unset ($this->session->data['order_id']);

        if ($_REQUEST['vanmeg'] == 0) {
            $this->redirect($this->url->link('common/home'));
        } else {
            $this->redirect($this->url->link('checkout/checkout'));
        }
    }
        public function pdfVersion($filename){
            $fp = @fopen($filename, 'rb');

            if (!$fp) {
                return 0;
            }

            /* Reset file pointer to the start */
            fseek($fp, 0);

            /* Read 20 bytes from the start of the PDF */
            preg_match('/\d\.\d/',fread($fp,20),$match);

            fclose($fp);

            if (isset($match[0])) {
                return $match[0];
            } else {
                return 0;
            }
        }
        /* creates a compressed zip file */
        public function create_zip($files = array(),$destination = '',$overwrite = false) {
            //if the zip file already exists and overwrite is false, return false

                if(file_exists($destination) && !$overwrite) { return false; }
                //vars
                $valid_files = array();
                //if files were passed in...
                if(is_array($files)) {
                    //cycle through each file
                    foreach($files as $file) {
                        //make sure the file exists
                        if(file_exists($file)) {
                            $valid_files[] = $file;
                        }
                    }
                }
                //if we have good files...
                if(count($valid_files)) {
                    //create the archive
                    $zip = new ZipArchive();
                    if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                        return false;
                    }
                    //add the files
                    foreach($valid_files as $file) {
                        $zip->addFile($file,$file);
                    }
                    //debug
                    //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

                    //close the zip -- done!
                    $zip->close();

                    //check to make sure the file exists
                    return file_exists($destination);
                }
                else
                {
                    return false;
                }

        }

    }
?>