<div class="left">
    <h2><?php echo $text_your_details; ?></h2>
    <?php if (($this->config->get('megjelenit_regisztracio_vezeteknev') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_vezeteknev') == 1  )) {?>
        <span class="required">*</span> <?php echo $entry_firstname; ?><br />
        <input type="text" name="firstname" value="<?php echo $firstname; ?>" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_keresztnev') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_keresztnev') == 1  )) {?>
        <span class="required">*</span> <?php echo $entry_lastname; ?><br />
        <input type="text" name="lastname" value="<?php echo $lastname; ?>" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_email') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_email') == 1  )) {?>
        <span class="required">*</span> <?php echo $entry_email; ?><br />
        <input type="text" name="email" value="<?php echo $email; ?>" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_telefon') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_telefon') == 1  )) {?>
        <span class="required">*</span> <?php echo $entry_telephone; ?><br />
        <input type="text" name="telephone" value="<?php echo $telephone; ?>" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_fax') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_fax') == 1  )) {?>
        <?php echo $entry_fax; ?><br />
        <input type="text" name="fax" value="<?php echo $fax; ?>" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_eletkor_regisztracio') == 1 ) || ($this->config->get('megjelenit_eletkor_regisztracio_ceges') == 1 ) ) {?>

        <span class="required">*</span> <?php echo $entry_eletkor; ?><br />
        <select name="eletkor" class="large-field">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($eletkors as $eletkora) { ?>
                <?php if ($eletkora['megnevezes'] == $eletkor) { ?>
                    <option value="<?php echo $eletkora['megnevezes']; ?>" selected="selected"><?php echo $eletkora['megnevezes']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $eletkora['megnevezes']; ?>"><?php echo $eletkora['megnevezes']; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_nem_regisztracio') == 1  ) || ($this->config->get('megjelenit_nem_regisztracio_ceges') == 1  ) ) {?>
        <span class="required">*</span> <?php echo $entry_nem; ?><br />
        <select name="nem"  class="large-field">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($nems as $value) { ?>
                <?php if ($value['ertek'] == $nem) { ?>
                    <option value="<?php echo $value['ertek']; ?>" selected="selected"><?php echo $value['nev']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $value['ertek']; ?>"><?php echo $value['nev']; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        <br />
        <br />
    <?php } ?>

    <?php if (($this->config->get('megjelenit_regisztracio_iskolai_vegzettseg') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_iskolai_vegzettseg') == 1  )) {?>

        <?php echo $entry_iskolai_vegzettseg; ?><br />
        <select name="iskolai_vegzettseg" class="large-field">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($iskolai_vegzettsegek as $value) { ?>
                <option value="<?php echo $value['megnevezes']; ?>"><?php echo $value['megnevezes']; ?></option>
            <?php } ?>
        </select>
        <br />
        <br />
    <?php } ?>

</div>
<div class="right">
    <?php if ( ($this->config->get('megjelenit_regisztracio_cegnev') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_cegnev') == 1 ) ) {?>
        <h2><?php echo $text_your_address; ?></h2>
        <?php echo $entry_company; ?><br />
        <input type="text" name="company" value="<?php echo $company; ?>" class="large-field" />
        <br />
        <br />
    <?php } else { ?>
        <input type="hidden" name="company" value=""  />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_adoszam') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_adoszam') == 1 ) ) {?>
        <?php echo $entry_adoszam; ?><br />
        <input type="text" name="adoszam" value="<?php echo $adoszam; ?>" class="large-field" />
        <br />
        <br />
    <?php } else { ?>
        <input type="hidden" name="adoszam" value=""  />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_utca') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_utca') == 1 ) ) {?>
        <span class="required">*</span> <?php echo $entry_address_1; ?><br />
        <input type="text" name="address_1" value="<?php echo $address_1; ?>" class="large-field" />
        <br />
        <br />
    <?php } else { ?>
        <input type="hidden" name="address_1" value=""  />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_cimadat_kiegeszito') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_cimadat_kiegeszito') == 1 ) ) {?>
        <?php echo $entry_address_2; ?><br />
        <input type="text" name="address_2" value="<?php echo $address_2; ?>" class="large-field" />
        <br />
        <br />
    <?php } else { ?>
        <input type="hidden" name="address_2" value=""  />
    <?php } ?>


    <?php if ( ($this->config->get('megjelenit_regisztracio_varos') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_varos') == 1 ) ) {?>
        <span class="required">*</span> <?php echo $entry_city; ?><br />
        <input type="text" name="city" value="<?php echo $city; ?>" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_iranyitoszam') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_iranyitoszam') == 1 ) ) {?>
        <span class="required">*</span> <?php echo $entry_postcode; ?><br />
        <input type="text" name="postcode" value="<?php echo $postcode; ?>" class="large-field" />
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_orszag') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_orszag') == 1 ) ) {?>
        <span class="required">*</span> <?php echo $entry_country; ?><br />
        <select name="country_id" class="large-field" onchange="$('#payment-address select[name=\'zone_id\']').load('index.php?route=checkout/guest/zone&country_id=' + this.value);">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $country_id) { ?>
                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        <br />
        <br />
    <?php } ?>

    <?php if ( ($this->config->get('megjelenit_regisztracio_megye') == 1) || ($this->config->get('megjelenit_regisztracio_ceges_megye') == 1 ) ) {?>
        <span class="required">*</span> <?php echo $entry_zone; ?><br />
        <select name="zone_id" class="large-field">
        </select>
        <br />
        <br />
    <?php } ?>
    <br />
</div>
<input type="hidden" name="ceges" value="<?php echo $ceges ?>">

<script type="text/javascript"><!--
    $('#payment-address select[name=\'zone_id\']').load('index.php?route=checkout/guest/zone&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
    //--></script>

<script type="text/javascript">
    $("[name=postcode]").autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: 'index.php?route=account/address/getpostcode&part=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.text,
                            value: item.id
                        }
                    }));
                }
            });
        },
        search: function(event) {
        },
        focus: function() {
        },
        select: function( event, ui ) {
            $.ajax({
                url: 'index.php?route=account/address/getcity&postcodeid=' +  encodeURIComponent(ui.item.value),
                dataType: 'json',
                success: function(json) {
                    debugger;
                    if(! json instanceof Object) {
                        return false;
                    }
                    var cityInput = $('[name=city]');
                    var postcode = $("[name=postcode]")
                    //var zoneInput = $('[name=zone_id]');
                    //var countryInput = $('[name=country_id]');
                    if(cityInput != undefined && cityInput.length != 0) {
                        cityInput.val(json.city);
                    }
                    if(postcode != undefined && postcode.length != 0) {
                        postcode.val(json.postcode);
                    }
                    /*
                     if(zoneInput != undefined && zoneInput.length != 0) {
                     var zoneSelect = zoneInput.find('option:contains("'+json['country']+'")');
                     if(zoneSelect != undefined && zoneSelect.length != 0) {
                     zoneSelect[0].selected = 'selected'
                     }
                     }
                     if(countryInput != undefined) {
                     countryInput.find('option:contains("Hungary")')[0].selected = 'selected';
                     }
                     */
                }
            });
            ui.item.value = $('[name=postcode]').val();
        }
    });
</script>