<?php

// Heading
$_['heading_title']  = 'Insurance';

// Text
$_['text_install']   = 'Install';
$_['text_uninstall'] = 'Remove';

// Column
$_['column_from']    = 'Purchase value from';
$_['column_to']      = 'Purchase value up to';
$_['column_value']   = 'Insurance premium';
$_['button_insert']  = 'Insert';
$_['button_modify']  = 'Modify';
$_['button_delete']  = 'Delete';
$_['button_save']    = 'Save';
$_['button_cancel']  = 'Cancel';
$_['column_modosit'] = 'Modify';
$_['text_success']  = 'The modify executed';
$_['column_torol'] = 'Delete';

// Error
$_['error_permission']  = 'Warning: Changing Insurance is not allowed for you!';
?>
