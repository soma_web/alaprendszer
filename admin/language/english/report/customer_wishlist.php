<?php
// Heading
$_['heading_title']         = 'Customer Wishlist Report';

// Column

$_['column_product_name']      = 'Product name';
$_['column_product_model']     = 'Medel';
$_['column_product_price']     = 'Price';
$_['column_product_quantity']  = 'In stock';
$_['column_total_customer']    = 'Customer';

$_['column_customer']       = 'Customer Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Customer Group';
$_['column_status']         = 'Status';
$_['column_action']         = 'Action';
$_['text_edit']             = 'Customers';

// Entry
$_['entry_date_start']      = 'Date Start:';
$_['entry_date_end']        = 'Date End:';
?>