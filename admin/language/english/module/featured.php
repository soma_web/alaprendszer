<?php
// Heading
$_['heading_title']       = 'Featured';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module featured!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_ures']          = 'There is no animation';
$_['text_mcs5']          = 'Scroll animation';
$_['text_korhinta']      = 'Carousel animation';
$_['text_megjelenit']           = 'Show:';
$_['text_korhinta_auto']        = 'Auto:';
$_['text_korhinta_infi']        = 'Infinity:';
$_['text_korhinta_speed']       = 'Speed:';

// Entry
$_['entry_osszes']          = 'Enable "All featured products button"';
$_['entry_osszes_nmb']      = 'Number of products at first';
$_['entry_product']         = 'Products:<br /><span class="help">(Autocomplete)</span>';
$_['entry_limit']           = 'Limit:';
$_['entry_image']           = 'Image (W x H):';
$_['entry_layout']          = 'Layout:';
$_['entry_position']        = 'Position:';
$_['entry_status']          = 'Status:';
$_['entry_sort_order']      = 'Sort Order:';
$_['entry_show']            = 'Show animation:';
$_['entry_menusorban']      = 'Menu bar appears:';
$_['entry_van_korhinta_title']  = 'Carousel settings:';



// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify module featured!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>