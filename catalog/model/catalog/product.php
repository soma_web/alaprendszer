<?php
class ModelCatalogProduct extends Model {
    private $special_price=0;
    public function updateViewed($product_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET viewed = (viewed + 1) WHERE product_id = '" . (int)$product_id . "'");
    }

    public function getProduct($product_id) {
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }


        $sql="SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer,
                (SELECT price       FROM " . DB_PREFIX . "product_discount pd2          WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$customer_group_id . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount,
                (SELECT points      FROM " . DB_PREFIX . "product_reward pr             WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$customer_group_id . "') AS reward,
                (SELECT ss.name     FROM " . DB_PREFIX . "stock_status ss               WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status,

                (SELECT wcd.unit    FROM " . DB_PREFIX . "weight_class_description wcd  WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class,
                (SELECT lcd.unit    FROM " . DB_PREFIX . "length_class_description lcd  WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class,
                (SELECT pcd.unit    FROM " . DB_PREFIX . "packaging_class_description pcd  WHERE p.packaging_class_id = pcd.packaging_class_id AND pcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS packaging_class,
                (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1            WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating,
                (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2               WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order
                FROM      " . DB_PREFIX . "product p
                LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
                LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
                LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)

                WHERE p.product_id = '" . (int)$product_id . "'
                    AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                    AND p.date_available <= NOW() AND
                    p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

                    //AND p.status = '1'

        $query = $this->db->query($sql);




        if ($query->num_rows) {
            $query->row['price'] = ($query->row['discount'] ? $query->row['discount'] : $query->row['price']);
            $query->row['rating'] = (int)$query->row['rating'];

            $special = $this->cart->productPrice($product_id);
            $query->row['special'] = $special < $query->row['price'] ? $special : 0;

            $videos = $this->getProductVideos($product_id);
            $query->row['videos'] = (isset($videos) && !empty($videos)) ? $videos : '';

            // Downloads
            $download_data = array();

            $download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$product_id . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

            foreach ($download_query->rows as $download) {
                $download_data[] = array(
                    'download_id' => $download['download_id'],
                    'name'        => $download['name'],
                    'filename'    => $download['filename'],
                    'mask'        => $download['mask'],
                    'remaining'   => $download['remaining']
                );
            }

            // Csomag


            $vissza = $query->row;
            $vissza['letoltesek']=$download_data;

            /*$arajanlat_keres = $this->config->get('arajanlat_keres_module');
            $arajanlat = false;
            if (isset($arajanlat_keres['status']) && $arajanlat_keres['status'] == 1) {
                if (isset($arajanlat_keres['altalanos'])) {
                    if (in_array($product_id,$arajanlat_keres['altalanos'])) {
                        $arajanlat = true;
                    }
                } elseif (isset($arajanlat_keres['hozzaadott'])) {
                    if (in_array($product_id,$arajanlat_keres['hozzaadott'])) {
                        $arajanlat = true;
                    }
                }
            }*/
            //$vissza['arajanlat']=$arajanlat;

            return $vissza;
        } else {
            return false;
        }
    }

    public function getProductPackage($product_id) {
        $package_data = array();

        $package_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_package WHERE product_id = '" . (int)$product_id .  "'");

        if ($package_query->num_rows > 0) {
            foreach ($package_query->rows as $package) {
                $package_adat = $this->getProduct($package['package_id']);
                if ($package_adat) {
                    $package_data[] = $this->getProduct($package['package_id']);
                }
            }
        } else $package_data = false;

        return $package_data;

    }

    public function getProductAjandek($product_id) {
        $ajandek_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_ajandek WHERE product_id = '" . (int)$product_id .  "'");

        if ($query->num_rows > 0) {
            foreach ($query->rows as $ajandek) {
                $ajandek_termek = $this->getProduct($ajandek['ajandek_id']);
                if ($ajandek_termek) {
                    $ajandek_data[] = $ajandek_termek;
                }
            }
        }

        return $ajandek_data;

    }


    public function getProducts($data = array(),$total=false) {
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $cache = md5(http_build_query($data));

        $product_data = $this->cache->get('product.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache);

        if (!$product_data) {
            $sql = "SELECT p.product_id,
            (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating ";

            if (isset($data['sort']) && $data['sort'] == "p.price") {
                $sql .= $this->cart->getSqlMinimumPriceRow();
            }

            $sql .= " FROM " . DB_PREFIX . "product p
            LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
            LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)";

            if (!empty($data['filter_tag'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_tag pt ON (p.product_id = pt.product_id)";
            }

            if (!empty($data['filter_category_id'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
            }



            if ( isset($data['keres_manufacturer']) &&  $data['keres_manufacturer']) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)";
            }
            $sql .= !empty($data['filter_tulajdonsagok']) ? " LEFT JOIN " . DB_PREFIX . "product_attribute pa ON (p.product_id = pa.product_id)" : "";

            $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
            if (empty($data['products'])) {
                $sql .= " AND p.status = '1'";
                $sql .= " AND p.date_available <= NOW()";
                $sql .= " AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

            } else {
                $products = implode(',',$data['products']);
                $sql .= " AND p.product_id in(".$products.")";
            }

            if (isset($_REQUEST['route']) && $_REQUEST['route'] == "module/upsell" ) {
                $sql .= $this->cart->getProductsCartForUpsell();
            }

            $szuresek = $this->config->get('megjelenit_altalanos');

            if ($szuresek['mennyisegre_szur'] == 1) {
                $sql .= " AND p.quantity > 0 ";
            }
            if ($szuresek['ervenesseg_datumra_szur'] == 1) {
                $sql .= " AND ( p.date_ervenyes_ig = '' OR p.date_ervenyes_ig = '0000-00-00' OR p.date_ervenyes_ig >= NOW() ) ";
            }

            if ($this->config->get('megjelenit_keres_bovit') == 1) {
                if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
                    $sql .= " AND (";

                    if (!empty($data['filter_name'])) {
                        $implode = array();

                        $words = explode(' ', $data['filter_name']);

                        foreach ($words as $word) {
                            if (!empty($data['filter_description'])) {
                                $implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' OR LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
                            } else {
                                $implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
                            }
                        }

                        if ($implode) {
                            $sql .= " " . implode(" OR ", $implode) . "";
                        }
                    }

                    if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                        $sql .= " OR ";
                    }

                    if (!empty($data['filter_tag'])) {
                        $implode = array();

                        $words = explode(' ', $data['filter_tag']);

                        foreach ($words as $word) {
                            $implode[] = "LCASE(pt.tag) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' AND pt.language_id = '" . (int)$this->config->get('config_language_id') . "'";
                        }

                        if ($implode) {
                            $sql .= " " . implode(" OR ", $implode) . "";
                        }
                    }

                    $sql .= ")";
                }
            } else {
                if (!empty($data['filter_name'])) {
                    $sql .= "
                    AND ";
                    $sql .= " ( ";

                    $implode = array();
                    $words = str_replace('-',' ',$data['filter_name']);
                    $words = explode(' ', $words);
                    foreach($words as $key=>$word) {
                        if (!$word) {
                            unset ($words[$key]);
                        }
                    }

                    foreach ($words as $word) {
                        $sql_sor = " ( ";
                        $sql_sor .= "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' collate utf8_general_ci ";

                        if (isset($data['keres_model']) &&  $data['keres_model']) {
                            $sql_sor .= "   OR   LCASE(p.model) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_cikkszam']) &&  $data['keres_cikkszam']) {
                            $sql_sor .= "   OR   LCASE(p.cikkszam) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_cikkszam2']) &&  $data['keres_cikkszam2']) {
                            $sql_sor .= "   OR   LCASE(p.cikkszam2) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_rovid_leiras']) &&  $data['keres_rovid_leiras']) {
                            $sql_sor .= "   OR   LCASE(pd.short_description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_hosszu_leiras']) &&  $data['keres_hosszu_leiras']) {
                            $sql_sor .= "   OR   LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_manufacturer']) &&  $data['keres_manufacturer']) {
                            $sql_sor .= "   OR   LCASE(m.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['filter_description']) &&  $data['filter_description']) {
                            $sql_sor .= "   OR   LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        $sql_sor .= " ) ";
                        $implode[] = $sql_sor;
                    }
                    if ($implode) {
                        $sql .= " " . implode(" AND ", $implode) . "";
                    }
                    $sql .= " ) ";
                }
            }

            if (!empty($data['filter_category_id'])) {
                if (!empty($data['filter_sub_category'])) {
                    $implode_data = array();

                    $implode_data[] = "p2c.category_id = '" . (int)$data['filter_category_id'] . "'";

                    $this->load->model('catalog/category');

                    $categories = $this->model_catalog_category->getCategoriesByParentId($data['filter_category_id']);

                    foreach ($categories as $category_id) {
                        $implode_data[] = "p2c.category_id = '" . (int)$category_id . "'";
                    }
                    $sql .= " AND (" . implode(' OR ', $implode_data) . ")";
                } else {
                    $sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
                }
            }

            if (!empty($data['filter_manufacturer_id'])) {
                $sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
            }
            if (!empty($data['filter_tulajdonsagok'])) {
                $sql .= " AND pa.attribute_id = '" . (int)$data['filter_tulajdonsagok'] . "'";
            }
            if (!empty($data['filter_model'])) {
                $sql .= " AND LCASE(p.model) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%' collate utf8_general_ci ";
            }
            if (!empty($data['filter_cikkszam'])) {
                $sql .= " AND LCASE(p.cikkszam) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_cikkszam'])) . "%' collate utf8_general_ci ";
            }
            if (!empty($data['filter_cikkszam2'])) {
                $sql .= " AND LCASE(p.cikkszam2) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_cikkszam2'])) . "%' collate utf8_general_ci ";
            }

            $sql .= "   GROUP BY p.product_id";

            $sort_data = array(
                'pd.name',
                'p.model',
                'p.quantity',
                'p.price',
                'p.cikkszam',
                'rating',
                'p.sort_order',
                'p.date_added'
            );

            $utalvany_sorrend="";
            if ($this->config->get('megjelenit_utalvany_szerint_rendez') == 1) {
                if ($this->config->get('megjelenit_utalvany_szerint_rendez_ask') == 1) {
                    $utalvany_sorrend = " p.utalvany DESC, ";
                } else {
                    $utalvany_sorrend = " p.utalvany ASC, ";
                }
            }

           /* if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                    $sql .= " ORDER BY " . $utalvany_sorrend . " LCASE(" . $data['sort'] . ")";
                } else {
                    $sql .= " ORDER BY " . $utalvany_sorrend . $data['sort'];

                }
            } else {
                $sql .= " ORDER BY " . $utalvany_sorrend. " p.sort_order";
            }*/

            $megjelenit_product = $this->config->get('megjelenit_product');

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                    if ($data['sort'] == 'p.model') {

                        if (isset($megjelenit_product['product_cikkszam_termeknevben']) && $megjelenit_product['product_cikkszam_termeknevben']) {
                            $sql .= " ORDER BY $utalvany_sorrend LCASE(p.cikkszam)";
                        } else {
                            $sql .= " ORDER BY $utalvany_sorrend LCASE(" . $data['sort'] . ")";
                        }

                    } else {
                        $sql .= " ORDER BY $utalvany_sorrend LCASE(" . $data['sort'] . ")";
                    }
                } elseif (isset($data['sort']) && $data['sort'] == "p.price") {
                    $sql .= " ORDER BY " . $utalvany_sorrend . 'minimum_price';

                } else {
                    $sql .= " ORDER BY " . $utalvany_sorrend . $data['sort'];
                }
            } else {
                $sql .= " ORDER BY p.sort_order";
            }
            
            /* Ezt ne vedd figyelembe */




            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC, LCASE(pd.name) DESC";
            } else {
                $sql .= " ASC, LCASE(pd.name) ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }
            $product_data = array();

            $query = $this->db->query($sql);

            foreach ($query->rows as $result) {
                $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
            }

            $this->cache->set('product.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache, $product_data);
        }

        return $product_data;
    }


    public function getProductSpecials($data = array()) {
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $sql = "
		SELECT DISTINCT ps.product_id,
		    (SELECT AVG(rating) FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = ps.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating
		    FROM " . DB_PREFIX . "product_special ps
		        LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id)
		        LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
		        LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
		    WHERE p.status = '1'
		        AND p.date_available <= curdate()
		        AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
		        AND ps.customer_group_id = '" . (int)$customer_group_id . "'
		        AND p.price > ps.price
		        AND ((ps.date_start = '0000-00-00' OR ps.date_start < curdate()) AND (ps.date_end = '0000-00-00' OR ps.date_end > curdate()))";
        if (isset($data['kiemelt']) && $data['kiemelt'] == 1 ) {
            $sql .= " AND ps.kiemelt = 1 ";
        }

		$sql .= " GROUP BY ps.product_id";

        $sort_data = array(
            'pd.name',
            'p.model',
            'ps.price',
            'rating',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
            } else {
                $sql .= " ORDER BY " . $data['sort'];
            }
        } else {
            $sql .= " ORDER BY p.sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $product_data = array();

        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
        }

        return $product_data;
    }

    public function getLatestProducts($limit) {
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $product_data = $this->cache->get('product.latest.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $customer_group_id . '.' . (int)$limit);

        if (!$product_data) {
            $query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.date_added DESC LIMIT " . (int)$limit);

            foreach ($query->rows as $result) {
                $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
            }

            $this->cache->set('product.latest.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'). '.' . $customer_group_id . '.' . (int)$limit, $product_data);
        }

        return $product_data;
    }

    public function getPopularProducts($limit) {
        $product_data = array();

        $query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.viewed, p.date_added DESC LIMIT " . (int)$limit);

        foreach ($query->rows as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
        }

        return $product_data;
    }

    public function getBestSellerProducts($limit) {
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $product_data = $this->cache->get('product.bestseller.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'). '.' . $customer_group_id . '.' . (int)$limit);

        if (!$product_data) {
            $product_data = array();

            $sql = "SELECT op.product_id, COUNT(*) AS total FROM " . DB_PREFIX . "order_product op
                LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)
                LEFT JOIN `" . DB_PREFIX . "product` p ON (op.product_id = p.product_id)
                LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
                 WHERE o.order_status_id > '0'
                 AND p.status = '1'
                 AND p.date_available <= NOW()
                 AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

            if (isset($_REQUEST['route']) && $_REQUEST['route'] == "module/upsell" ) {
                $sql .= $this->cart->getProductsCartForUpsell();
            }

            $szuresek = $this->config->get('megjelenit_altalanos');

            if ($szuresek['mennyisegre_szur'] == 1) {
                $sql .= " AND p.quantity > 0 ";
            }
            if ($szuresek['ervenesseg_datumra_szur'] == 1) {
                $sql .= " AND ( p.date_ervenyes_ig = '' OR p.date_ervenyes_ig = '0000-00-00' OR p.date_ervenyes_ig >= NOW() ) ";
            }

            $sql .= ' GROUP BY op.product_id ';

            $utalvany_sorrend="";
            if ($this->config->get('megjelenit_utalvany_szerint_rendez') == 1) {
                if ($this->config->get('megjelenit_utalvany_szerint_rendez_ask') == 1) {
                    $utalvany_sorrend = " p.utalvany DESC, ";
                } else {
                    $utalvany_sorrend = " p.utalvany ASC, ";
                }
            }


                $sql .= " ORDER BY " . $utalvany_sorrend. "  total DESC LIMIT " . (int)$limit;


            $query = $this->db->query($sql);

            foreach ($query->rows as $result) {
                $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
            }

            $this->cache->set('product.bestseller.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'). '.' . $customer_group_id . '.' . (int)$limit, $product_data);
        }

        return $product_data;
    }

    public function getProductAttributes($product_id) {
        $product_attribute_group_data = array();

        $product_attribute_group_query = $this->db->query("SELECT ag.attribute_group_id, agd.name FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_group ag ON (a.attribute_group_id = ag.attribute_group_id) LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE pa.product_id = '" . (int)$product_id . "' AND agd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, agd.name");

        foreach ($product_attribute_group_query->rows as $product_attribute_group) {
            $product_attribute_data = array();

            $product_attribute_query = $this->db->query("SELECT a.attribute_id, ad.name, pa.text FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE pa.product_id = '" . (int)$product_id . "' AND a.attribute_group_id = '" . (int)$product_attribute_group['attribute_group_id'] . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pa.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY a.sort_order, ad.name");

            foreach ($product_attribute_query->rows as $product_attribute) {
                $product_attribute_data[] = array(
                    'attribute_id' => $product_attribute['attribute_id'],
                    'name'         => $product_attribute['name'],
                    'text'         => $product_attribute['text']
                );
            }

            $product_attribute_group_data[] = array(
                'attribute_group_id' => $product_attribute_group['attribute_group_id'],
                'name'               => $product_attribute_group['name'],
                'attribute'          => $product_attribute_data
            );
        }

        return $product_attribute_group_data;
    }

    public function getProductOptions($product_id) {
        $product_option_data = array();

        $product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");

        foreach ($product_option_query->rows as $product_option) {
            if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox'
                        || $product_option['type'] == 'image' || $product_option['type'] == 'checkbox_qty') {
                $product_option_value_data = array();

              $sql = "SELECT * FROM " . DB_PREFIX . "product_option_value pov
                    LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id)
                    LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id)
                    WHERE pov.product_id = '" . (int)$product_id . "'
                        AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "'
                        AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                        ORDER BY ov.sort_order";
                $product_option_value_query = $this->db->query($sql);

                foreach ($product_option_value_query->rows as $product_option_value) {
                    $product_option_value_data[] = array(
                        'product_option_value_id' => $product_option_value['product_option_value_id'],
                        'option_value_id'         => $product_option_value['option_value_id'],
                        'name'                    => $product_option_value['name'],
                        'heading'                 => (isset($product_option_value['heading']) ? $product_option_value['heading'] : ''),
                        'image'                   => $product_option_value['image'],
                        'quantity'                => $product_option_value['quantity'],
                        'subtract'                => $product_option_value['subtract'],
                        'checked'                 => $product_option_value['checked'],
                        'price'                   => $product_option_value['price'],
                        'price_prefix'            => $product_option_value['price_prefix'],
                        'points'                  => $product_option_value['points'],
                        'points_prefix'           => $product_option_value['points_prefix'],
                        'weight'                  => $product_option_value['weight'],
                        'weight_prefix'           => $product_option_value['weight_prefix']
                    );
                }

                $product_option_data[] = array(
                    'product_option_id' => $product_option['product_option_id'],
                    'option_id'         => $product_option['option_id'],
                    'name'              => $product_option['name'],
                    'description'       => $product_option['description'],
                    'type'              => $product_option['type'],
                    'option_value'      => $product_option_value_data,
                    'required'          => $product_option['required']
                );
            } else {
                $product_option_data[] = array(
                    'product_option_id' => $product_option['product_option_id'],
                    'option_id'         => $product_option['option_id'],
                    'name'              => $product_option['name'],
                    'description'       => $product_option['description'],
                    'type'              => $product_option['type'],
                    'option_value'      => $product_option['option_value'],
                    'required'          => $product_option['required']
                );
            }
        }

        return $product_option_data;
    }

    public function getProductOptionsName($option_value_id){
        $vissza = '';
        $sql = "SELECT name FROM " . DB_PREFIX . "option_value_description WHERE  option_value_id = '".$option_value_id."'";
        $query = $this->db->query($sql);
        if ($query->rows) {
            foreach($query->rows as $value) {
                if (!empty($value['name'])) {
                    $vissza = $value['name'];
                    break;
                }
            }
        }
        return $vissza;

    }


    public function getProductMeretek($data=array()){
        $meret = array();
        foreach($data as $adatok) {
            if ($adatok['type'] == 'checkbox_qty') {
                $tomb = array();
                foreach($adatok['option_value'] as $key =>$value) {

                    $szinek = str_replace(' ','',$value['heading']);
                    $szinek = str_replace('#','',$szinek);
                    $value['heading'] = explode('/',$szinek);

                    $tomb[$key] = $value;
                    $tomb[$key]['product_option_id'] = $adatok['product_option_id'];
                    $tomb[$key]['option_id'] = $adatok['option_id'];
                }


                $meret[$adatok['name'].'|||'.$adatok['description']] =  $tomb;
            }
        }

        if ($meret) {
            foreach($meret as $key=>$value) {
                if (!$value) {
                    unset ($meret[$key]);
                }
            }
        }
        return $meret;
    }

    public function getProductDiscounts($product_id) {

        $query_customer = false;

        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
            $sql = "SELECT * FROM " . DB_PREFIX . "product_customer_special WHERE
                        product_id = '" . (int)$product_id . "'
                        AND customer_id=".$this->customer->isLogged(). "
                        AND quantity > 0
                        AND ((date_start = '0000-00-00' OR date_start < NOW())
                        AND (date_end = '0000-00-00' OR date_end > NOW()))
                        ORDER BY quantity ASC, price ASC";

            $query_customer = $this->db->query($sql);

        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE
                product_id = '" . (int)$product_id . "'
            AND customer_group_id = '" . (int)$customer_group_id . "'
            AND quantity > 1
            AND ((date_start = '0000-00-00' OR date_start < NOW())
            AND (date_end = '0000-00-00' OR date_end > NOW()))
            ORDER BY quantity ASC, priority ASC, price ASC");

        if ($query_customer && $query_customer->num_rows > 0) {
            foreach($query_customer->rows as $value) {
                $quantity   = $value['quantity'];
                $price      = $value['price'];
                $talalt     = false;
                foreach($query->rows as $key=>$ertek) {
                    if ($ertek['quantity'] == $quantity && $ertek['price'] > $price) {
                        $query->rows[$key]['price'] = $price;
                        $talalt = true;
                    }
                }
                if (!$talalt) {
                    $query->rows[] = array(
                        'product_discount_id'   => 0,
                        'product_id'            => $value['product_id'],
                        'customer_group_id'     => $customer_group_id,
                        'quantity'              => $quantity,
                        'priority'              => 0,
                        'price'                 => $price,
                        'date_start'            => $value['date_start'],
                        'date_end'              => $value['date_end']
                    );
                    $query->num_rows ++;
                }
            }
        }
        if ($query->rows) {
            $query->rows= $this->config->rendezes($query->rows,'quantity','ASC','price','ASC');

            $this->special_price  = $this->cart->productPrice($product_id);

            foreach($query->rows as $key=>$value) {
                if (($value['price'] >= $this->special_price)) {
                    unset($query->rows[$key]);
                }
            }

            $legkisebb = $this->special_price;
            foreach($query->rows as $key=>$value) {
                if ($value['price'] >= $legkisebb) {
                    unset($query->rows[$key]);
                } else {
                    $legkisebb = $value['price'];
                }
            }
        }

        return $query->rows;
    }

    public function getProductImages($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getProductRelated($product_id) {
        $product_data = array();

        $sql="SELECT * FROM " . DB_PREFIX . "product_related pr
            LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id)
            LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
                WHERE    pr.product_id = '" . (int)$product_id . "'
                    AND  p.status = '1'
                    AND  p.date_available <= NOW()
                    AND  p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";
        if (isset($_REQUEST['route']) && $_REQUEST['route'] == "module/upsell" ) {
            $sql .= $this->cart->getProductsCartForUpsell();
        }


        $utalvany_sorrend="";
        if ($this->config->get('megjelenit_utalvany_szerint_rendez') == 1) {
            if ($this->config->get('megjelenit_utalvany_szerint_rendez_ask') == 1) {
                $utalvany_sorrend = " p.utalvany DESC, ";
            } else {
                $utalvany_sorrend = " p.utalvany ASC, ";
            }
        }


        $sql .= " ORDER BY " . $utalvany_sorrend. " p.sort_order";


        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[$result['related_id']] = $this->getProduct($result['related_id']);
        }

        return $product_data;
    }

    public function getProductAccessory($product_id) {
        $product_data = array();

        $sql="SELECT * FROM " . DB_PREFIX . "product_accessory pr
            INNER JOIN " . DB_PREFIX . "product p ON (pr.accessory_id = p.product_id)
            INNER JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
                WHERE    pr.product_id = '" . (int)$product_id . "'
                    AND  p.status = '1'
                    AND p.date_available <= NOW()
                    AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

        if (isset($_REQUEST['route']) && $_REQUEST['route'] == "module/upsell" ) {
            $sql .= $this->cart->getProductsCartForUpsell();
        }

        $utalvany_sorrend="";
        if ($this->config->get('megjelenit_utalvany_szerint_rendez') == 1) {
            if ($this->config->get('megjelenit_utalvany_szerint_rendez_ask') == 1) {
                $utalvany_sorrend = " p.utalvany DESC, ";
            } else {
                $utalvany_sorrend = " p.utalvany ASC, ";
            }
        }


        $sql .= " ORDER BY " . $utalvany_sorrend. " p.sort_order";


        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[$result['accessory_id']] = $this->getProduct($result['accessory_id']);
        }

        return $product_data;
    }

    public function getProductGeneralAccessory($product_id) {
        $product_data = array();

        $sql="SELECT * FROM " . DB_PREFIX . "product_general_accessory pr
            INNER JOIN " . DB_PREFIX . "product p ON (pr.general_accessory_id = p.product_id)
            INNER JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
                WHERE    pr.product_id = '" . (int)$product_id . "'
                    AND  p.status = '1'
                    AND p.date_available <= NOW()
                    AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";



        $utalvany_sorrend="";
        if ($this->config->get('megjelenit_utalvany_szerint_rendez') == 1) {
            if ($this->config->get('megjelenit_utalvany_szerint_rendez_ask') == 1) {
                $utalvany_sorrend = " p.utalvany DESC, ";
            } else {
                $utalvany_sorrend = " p.utalvany ASC, ";
            }
        }


        $sql .= " ORDER BY " . $utalvany_sorrend. " p.sort_order";


        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[$result['general_accessory_id']] = $this->getProduct($result['general_accessory_id']);
        }

        return $product_data;
    }

    public function getProductHelyettesito($product_id) {
        $product_data = array();

        $sql="SELECT * FROM " . DB_PREFIX . "product_helyettesito pr
            INNER JOIN " . DB_PREFIX . "product p ON (pr.helyettesito_id = p.product_id)
            INNER JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
                WHERE    pr.product_id = '" . (int)$product_id . "'
                    AND  p.status = '1'
                    AND p.date_available <= NOW()
                    AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

        if (isset($_REQUEST['route']) && $_REQUEST['route'] == "module/upsell" ) {
            $sql .= $this->cart->getProductsCartForUpsell();
        }

        $utalvany_sorrend="";
        if ($this->config->get('megjelenit_utalvany_szerint_rendez') == 1) {
            if ($this->config->get('megjelenit_utalvany_szerint_rendez_ask') == 1) {
                $utalvany_sorrend = " p.utalvany DESC, ";
            } else {
                $utalvany_sorrend = " p.utalvany ASC, ";
            }
        }


        $sql .= " ORDER BY " . $utalvany_sorrend. " p.sort_order";


        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[$result['helyettesito_id']] = $this->getProduct($result['helyettesito_id']);
        }

        return $product_data;
    }

    public function getProductTags($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_tag WHERE product_id = '" . (int)$product_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->rows;
    }

    public function getProductLayoutId($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return  $this->config->get('config_layout_product');
        }
    }

    public function getCategories($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

        return $query->rows;
    }

    public function getTotalProducts($data = array()) {

        $cache = md5(http_build_query($data));

        $product_data = $this->cache->get('product_total.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache);


        if (!$product_data) {

            $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p
            LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
            LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)";

            $sql .= !empty($data['filter_category_id'])   ? " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)" : "";
            $sql .= !empty($data['filter_tag'])           ? " LEFT JOIN " . DB_PREFIX . "product_tag pt ON (p.product_id = pt.product_id)" : "";
            $sql .= !empty($data['keres_manufacturer'])   ? " LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)" : "";
            $sql .= !empty($data['filter_tulajdonsagok']) ? " LEFT JOIN " . DB_PREFIX . "product_attribute pa ON (p.product_id = pa.product_id)" : "";

            $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                AND p.status = '1'
                AND p.date_available <= NOW()
                AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

            $szuresek = $this->config->get('megjelenit_altalanos');

            $sql .= $szuresek['mennyisegre_szur'] == 1          ? " AND p.quantity > 0 " : "";
            $sql .=$szuresek['ervenesseg_datumra_szur'] == 1    ? " AND ( p.date_ervenyes_ig = '' OR p.date_ervenyes_ig = '0000-00-00' OR p.date_ervenyes_ig >= NOW() ) " : "";

            if ($this->config->get('megjelenit_keres_bovit') == 1) {

                if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
                    $sql .= " AND (";

                    if (!empty($data['filter_name'])) {
                        $implode = array();

                        $words = explode(' ', $data['filter_name']);

                        foreach ($words as $word) {
                            if (!empty($data['filter_description'])) {
                                $implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' OR LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
                            } else {
                                $implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
                            }
                        }

                        if ($implode) {
                            $sql .= " " . implode(" OR ", $implode) . "";
                        }
                    }

                    if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                        $sql .= " OR ";
                    }

                    if (!empty($data['filter_tag'])) {
                        $implode = array();

                        $words = explode(' ', $data['filter_tag']);

                        foreach ($words as $word) {
                            $implode[] = "LCASE(pt.tag) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' AND pt.language_id = '" . (int)$this->config->get('config_language_id') . "'";
                        }

                        if ($implode) {
                            $sql .= " " . implode(" OR ", $implode) . "";
                        }
                    }

                    $sql .= ")";
                }
            } else {
                if (!empty($data['filter_name'])) {
                    $sql .= "
                        AND ";
                    $sql .= " ( ";

                    $implode = array();
                    $words = str_replace('-',' ',$data['filter_name']);
                    $words = explode(' ', $words);
                    foreach($words as $key=>$word) {
                        if (!$word) {
                            unset ($words[$key]);
                        }
                    }

                    foreach ($words as $word) {
                        $sql_sor = " ( ";
                        $sql_sor .= "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' collate utf8_general_ci ";

                        if (isset($data['keres_model']) &&  $data['keres_model']) {
                            $sql_sor .= "   OR   LCASE(p.model) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_cikkszam']) &&  $data['keres_cikkszam']) {
                            $sql_sor .= "   OR   LCASE(p.cikkszam) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_cikkszam2']) &&  $data['keres_cikkszam2']) {
                            $sql_sor .= "   OR   LCASE(p.cikkszam2) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_rovid_leiras']) &&  $data['keres_rovid_leiras']) {
                            $sql_sor .= "   OR   LCASE(pd.short_description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_hosszu_leiras']) &&  $data['keres_hosszu_leiras']) {
                            $sql_sor .= "   OR   LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['keres_manufacturer']) &&  $data['keres_manufacturer']) {
                            $sql_sor .= "   OR   LCASE(m.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        if (isset($data['filter_description']) &&  $data['filter_description']) {
                            $sql_sor .= "   OR   LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) ."%' collate utf8_general_ci ";
                        }
                        $sql_sor .= " ) ";
                        $implode[] = $sql_sor;
                    }
                    if ($implode) {
                        $sql .= " " . implode(" AND ", $implode) . "";
                    }
                    $sql .= " ) ";

                }

            }

            if (!empty($data['filter_category_id'])) {
                if (!empty($data['filter_sub_category'])) {
                    $implode_data = array();

                    $implode_data[] = "p2c.category_id = '" . (int)$data['filter_category_id'] . "'";

                    $this->load->model('catalog/category');

                    $categories = $this->model_catalog_category->getCategoriesByParentId($data['filter_category_id']);

                    foreach ($categories as $category_id) {
                        $implode_data[] = "p2c.category_id = '" . (int)$category_id . "'";
                    }

                    $sql .= " AND (" . implode(' OR ', $implode_data) . ")";
                } else {
                    $sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
                }
            }

            if (!empty($data['filter_manufacturer_id'])) {
                $sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
            }

            if (!empty($data['filter_tulajdonsagok'])) {
                $sql .= " AND pa.attribute_id = '" . (int)$data['filter_tulajdonsagok'] . "'";
            }

            if (!empty($data['filter_model'])) {
                $sql .= " AND LCASE(p.model) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%' collate utf8_general_ci ";
            }
            if (!empty($data['filter_cikkszam'])) {
                $sql .= " AND LCASE(p.cikkszam) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_cikkszam'])) . "%' collate utf8_general_ci ";
            }
            if (!empty($data['filter_cikkszam2'])) {
                $sql .= " AND LCASE(p.cikkszam2) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_cikkszam2'])) . "%' collate utf8_general_ci ";
            }

            $query = $this->db->query($sql);

            $this->cache->set('product_total.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache, $query->row['total']);
            return $query->row['total'];

        } else {
            return $product_data;
        }


    }

    public function getTotalProductSpecials() {
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $cache = md5("123");

        $special_total = $this->cache->get('product_special_total.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache);

        if (!$special_total) {
            $query = $this->db->query("SELECT COUNT(DISTINCT ps.product_id) AS total
              FROM " . DB_PREFIX . "product_special ps
                LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id)
                LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
                WHERE p.status = '1' AND p.date_available <= NOW()
                AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
                AND ps.customer_group_id = '" . (int)$customer_group_id . "'
                AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW())
                AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))");

            $special_total = $query->row['total'];
            $this->cache->set('product_special_total.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache, $special_total);
        }

        if (isset($special_total)) {
            return $special_total;
        } else {
            return 0;
        }
    }
    public function getInformationDisplay($data) {
        $kiiras = array();

        if ($this->config->get('megjelenit_product_elerhetoseg') == 1 && !empty($data['stock_status'])){

            if ($data['quantity'] <= 0) {
                $stock = $data['stock_status'];
            } elseif ($this->config->get('config_stock_display')) {
                $stock = $data['quantity'];
            } else {
                $stock = $this->language->get('text_instock');
            }

            $kiiras['megjelenit_product_elerhetoseg'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_elerhetoseg_sorrend'),
                'text_name'   => $this->language->get('text_stock'),
                'group_filter'  => false,
                'value'       => $stock

            );
        }
        if ($this->config->get('megjelenit_product_kaphato') == 1 && $data['date_ervenyes_ig'] != null){
            $kiiras['megjelenit_product_kaphato'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_kaphato_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_kaphato'),
                'value'       => $data['date_ervenyes_ig']."-ig"


            );
        }
        if ($this->config->get('megjelenit_product_filter_gropup') == 1){
            $results = $this->getProductFilterGroup($data['product_id']);
            if ($results->num_rows > 0) {
                $csoportnev = array();
                foreach ($results->rows as $result) {
                    if (!in_array($result['name'], $csoportnev)) {
                        $csoportnev[] = $result['name'];
                    }
                }
                $kiiras['megjelenit_product_filter_gropup'] = array(
                    'sorrend'    => $this->config->get('megjelenit_product_filter_gropup_sorrend'),
                    'group_filter'  => false,
                    'text_name'       => $this->language->get('text_szuro_csoport'),
                    'value'       => $csoportnev
                );
            }
        }
        if ($this->config->get('megjelenit_product_filter') == 1){
            $results = $this->getProductFilter($data['product_id']);
            if ($results->num_rows > 0) {
                $csoportnev = array();
                foreach ($results->rows as $result) {
                    if (!in_array($result['name'], $csoportnev) && !empty($result['name']) ) {
                        $csoportnev[] = $result['name'];
                    }
                }
                if (count($csoportnev) > 0) {
                    $kiiras['megjelenit_product_filter'] = array(
                        'sorrend'    => $this->config->get('megjelenit_product_filter_sorrend'),
                        'group_filter'  => false,
                        'text_name'       => $this->language->get('text_szuro'),
                        'value'       => $csoportnev
                    );
                }
            }
        }
        if ($this->config->get('megjelenit_product_gropup_filter') == 1){
            $results = $this->getProductGroupFilter($data['product_id']);
            if ($results->num_rows > 0) {

                $sortingSettings = array(
                    0       =>  array(
                        'orderby' =>  'filter_group_id',
                        'sortorder'     => 'ASC'
                    ),
                    1       =>  array(
                        'orderby' =>   'group_order',
                        'sortorder'     => 'ASC'
                    ),
                    2       =>  array(
                        'orderby'  => 'filter_order',
                        'sortorder'     => 'ASC'
                    )
                );

                $sortedBody = new ArrayOfArrays($results->rows);
                $sortedBody->multiSorting($sortingSettings);
                $results = $sortedBody->getArrayCopy();

                $csoportnev = array();

                $letezik = false;

                foreach ($results as $result) {
                    if ($csoportnev != null) {
                        $letezik = false;
                        foreach ($csoportnev as $csname) {
                            if (in_array($result['name'], $csname) ) {
                                $letezik = true;
                                break;
                            }
                        }
                    }
                    if (!$letezik) {
                        $csoportnev[] = array(
                            'name' => $result['name'],
                           'filter_group_id' => $result['filter_group_id']
                        );
                    }
                }
                $mehet = array();

                foreach ($csoportnev as $csoport) {
                    $alcsoport = $this->getProductGroupToFilter($csoport['filter_group_id'],$results);
                    $mehet[] = array(
                        'group' => $csoport['name'],
                        'filter' => $alcsoport
                    );

                }
                $kiiras['megjelenit_product_gropup_filter'] = array(
                    'sorrend'    => $this->config->get('megjelenit_product_filter_gropup_sorrend'),
                    'text_name'       => $this->language->get('text_szuro_csoport'),
                    'group_filter'  => true,
                    'value'       => $mehet
                );
            }
        }
        if ($this->config->get('megjelenit_product_suly') == 1 && $data['weight'] > 0){
            $kiiras['megjelenit_product_suly'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_suly_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_suly'),
                'value'       => number_format($data['weight'],4,"."," ") .$data['weight_class']." / ".$data['megyseg']


            );
        }
        if ($this->config->get('megjelenit_product_mennyisegi_egyseg') == 1 && !empty($data['megyseg']) ){
            $kiiras['megjelenit_product_mennyisegi_egyseg'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_mennyisegi_egyseg_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_megyseg'),
                'value'       => $data['megyseg']

            );
        }
        if ($this->config->get('megjelenit_product_cikkszam') == 1  && !empty($data['cikkszam']) ){
            $kiiras['megjelenit_product_cikkszam'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_cikkszam_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_cikkszam'),
                'value'       => $data['cikkszam']

            );
        }
        if ($this->config->get('megjelenit_product_cikkszam2') == 1  && !empty($data['cikkszam2']) ){
            $kiiras['megjelenit_product_cikkszam2'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_cikkszam2_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_cikkszam2'),
                'value'       => $data['cikkszam2']

            );
        }
        if ($this->config->get('megjelenit_product_model') == 1  && !empty($data['model']) ){
            $kiiras['megjelenit_product_model'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_model_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_model'),
                'value'       => $data['model']

            );
        }
        if ($this->config->get('megjelenit_product_gyarto') == 1  && !empty($data['manufacturer'])  ){
            $kiiras['megjelenit_product_gyarto'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_gyarto_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_manufacturer'),
                'value'       => $data['manufacturer']

            );
        }
        if ($this->config->get('megjelenit_product_reward') == 1  && $data['reward'] > 0 ){
            $kiiras['megjelenit_product_reward'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_reward_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_reward'),
                'value'       => $data['reward']
            );
        }

        if ($this->config->get('megjelenit_product_sku') == 1 && !empty($data['sku'])) {
            $kiiras['megjelenit_product_sku'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_sku_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_sku'),
                'value'       => $data['sku']
            );
        }

        if ($this->config->get('megjelenit_product_meret') == 1){
            $kiiras['megjelenit_product_meret'] = array(
                'sorrend'    => $this->config->get('megjelenit_product_meret_sorrend'),
                'group_filter'  => false,
                'text_name'       => $this->language->get('text_meret'),
                'value'       => number_format($data['length'])." × ".number_format($data['width'])." × ".number_format($data['height'])." ".$data['length_class']
            );
        }
        asort($kiiras);

        return $kiiras;
    }

    public function getInformationDisplays($data) {
        $kiiras = array();

        if (!empty($data['stock_status'])){

            if ($data['quantity'] <= 0) {
                $stock = $data['stock_status'];
            } elseif ($this->config->get('config_stock_display')) {
                $stock = $data['quantity'];
            } else {
                $stock = $this->language->get('text_instock');
            }

            $kiiras['megjelenit_product_elerhetoseg'] = array(
                'text_name'   => $this->language->get('text_stock'),
                'value'       => $stock

            );
        }
        if ($data['date_ervenyes_ig'] != null || $data['date_ervenyes_ig'] != "0000-00-00"){
            $kiiras['megjelenit_product_kaphato'] = array(
                'text_name'       => $this->language->get('text_kaphato'),
                'value'       => $data['date_ervenyes_ig']."-ig"


            );
        }

        if ($data['weight'] > 0){
            $kiiras['megjelenit_product_suly'] = array(
                'text_name'       => $this->language->get('text_suly'),
                'value'       => number_format($data['weight'],4,"."," ") .$data['weight_class']." / ".$data['megyseg']


            );
        }
        if (!empty($data['megyseg']) ){
            $kiiras['megjelenit_product_mennyisegi_egyseg'] = array(
                'text_name'       => $this->language->get('text_megyseg'),
                'value'       => $data['megyseg']

            );
        }
        if (!empty($data['cikkszam']) ){
            $kiiras['megjelenit_product_cikkszam'] = array(
                'text_name'       => $this->language->get('text_cikkszam'),
                'value'       => $data['cikkszam']

            );
        }
        if (!empty($data['cikkszam2']) ){
            $kiiras['megjelenit_product_cikkszam2'] = array(
                'text_name'       => $this->language->get('text_cikkszam2'),
                'value'       => $data['cikkszam2']

            );
        }
        if (!empty($data['model']) ){
            $kiiras['megjelenit_product_model'] = array(
                'text_name'       => $this->language->get('text_model'),
                'value'       => $data['model']

            );
        }
        if (!empty($data['manufacturer'])  ){
            $kiiras['megjelenit_product_gyarto'] = array(
                'text_name'       => $this->language->get('text_manufacturer'),
                'value'       => $data['manufacturer']

            );
        }
        if ($data['reward'] > 0 ){
            $kiiras['megjelenit_product_reward'] = array(
                'text_name'       => $this->language->get('text_reward'),
                'value'       => $data['reward']
            );
        }

        if (!empty($data['sku'])) {
            $kiiras['megjelenit_product_sku'] = array(
                'text_name'       => $this->language->get('text_sku'),
                'value'       => $data['sku']
            );
        }

        if (!empty($data['length']) && $data['length'] != 0 && !empty($data['width']) && $data['width'] != 0 && !empty($data['height']) && $data['height'] != 0 && !empty($data['length_class'])){
            $kiiras['megjelenit_product_meret'] = array(
                'text_name'       => $this->language->get('text_meret'),
                'value'       => number_format($data['length'])." × ".number_format($data['width'])." × ".number_format($data['height'])." ".$data['length_class']
            );
        }

        return $kiiras;
    }


    public function getProductFilterGroup($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter pf
                        LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (pf.filter_group_id = fgd.filter_group_id)
                        WHERE   fgd.language_id = '" . $this->config->get('config_language_id') . "'
                         AND pf.product_id = '" . (int)$product_id . "'  ");

        return $query;
    }

    public function getProductFilter($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter pf
                        LEFT JOIN " . DB_PREFIX . "filter_description fd ON (pf.filter_id = fd.filter_id)
                        WHERE   fd.language_id = '" . $this->config->get('config_language_id') . "'
                         AND pf.product_id = '" . (int)$product_id . "'  ");

        return $query;
    }

    public function getProductGroupFilter($product_id) {
        $sql = "SELECT DISTINCT *, fd.name AS filter_name, fg.sort_order AS group_order, f.sort_order AS filter_order FROM " . DB_PREFIX . "product_filter pf
                        LEFT JOIN " . DB_PREFIX . "filter_description fd ON (pf.filter_id = fd.filter_id)
                        LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (pf.filter_group_id = fgd.filter_group_id)

                        LEFT JOIN " . DB_PREFIX . "filter_group fg ON (pf.filter_group_id = fg.filter_group_id)
                        LEFT JOIN " . DB_PREFIX . "filter f ON (pf.filter_id = f.filter_id)

                        WHERE   fd.language_id = '" . $this->config->get('config_language_id') . "'
                         AND fgd.language_id = '" . $this->config->get('config_language_id') . "'
                         AND pf.product_id = '" . (int)$product_id . "'  ";
        $query = $this->db->query($sql);

        return $query;
    }


    public function getProductGroupToFilter ($csoportkod, $tombs) {

        $vissza = array();
        foreach ($tombs as $tomb) {
            if ( $tomb['filter_group_id'] == $csoportkod) {
                $vissza[] = array(
                    'filter_name' => $tomb['filter_name']
                );
            }
        }
        return $vissza;

    }

    public function getProductElhelyezkedes($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_elhelyezes WHERE product_id = '" . (int)$product_id . "' ORDER BY datum_tol");

        $vissza = array();
        if ($query->num_rows > 0) {
            foreach ($query->rows as $value) {
                $value['penzugyi_status'] = $this->getPentugyiStatus( $value['penzugyi_status_id'] );
                $value['fizetesi_status'] = $this->getFizetesiStatus( $value['fizetes_elbiralas_status_id'] );
                $value['kosarban_van']    = $this->getKosarbanVan($product_id, $value['sor']);
                $vissza[] = $value;

            }
        }

        return $vissza;
    }

    public function getKosarbanVan($product_id,$sor) {

        $vissza = 0;

        if (isset($this->session->data['cart_elhelyezkedes'][$product_id][$sor])) {
            $vissza = 1;
        }

        return $vissza;
    }

    public function getPentugyiStatus($id) {

        $elhelyezkedesek = $this->config->get("penzugyi_status");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['penzugyi_status_id'] == $id) {
                    return $value;
                }
            }
        }

        return false;
    }

    public function getFizetesiStatus($id) {

        $elhelyezkedesek = $this->config->get("fizetes_elbiralas");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['fizetes_elbiralas_status_id'] == $id) {
                    return $value;
                }
            }
        }

        return false;
    }

    public function getLastSeenProducts() {
        $latta = false;

        if (isset($this->session->data['latta'])) {
            $sees = $this->session->data['latta'];
            if (count($sees) > 0) {
                foreach(array_reverse($sees) as $key=>$see) {
                    $latta[$key]=$see;
                }
            }
        }
        return $latta;
    }

    public function setLastSeenProducts($product_id) {

        if (isset($this->session->data['latta']) ) {
            $sees = $this->session->data['latta'];
            foreach($sees as $key=>$see) {
                $latta[$key]=$see;
            }
            if (isset($latta[$product_id])) {
                unset ($latta[$product_id]);
            }
            $latta[$product_id]=$product_id;
            $this->session->data['latta']  = $latta;
        } else {
            $this->session->data['latta'][$product_id] = $product_id;
        }
    }

    public function isLastSeenProducts() {

        $vissza = false;
        if (isset($this->session->data['latta']) ) {
            $sees = $this->session->data['latta'];
            if (count($sees) > 0 ){
                $vissza = true;
            }
        }

        return $vissza;
    }
    public function getProductShipping($weight) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");

        foreach ($query->rows as $result) {
            if ($this->config->get('weight_' . $result['geo_zone_id'] . '_status')) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND
                country_id = '" . (int)$this->config->get('config_country_id') . "'
                AND (zone_id = '" . (int)$this->config->get('config_zone_id') . "' OR zone_id = '0')");

                $rates = explode(',', $this->config->get('weight_' . $result['geo_zone_id'] . '_rate'));
                break;
            }
        }

        if ( isset($rates) && !empty($rates)) {
            foreach ($rates as $rate) {
                $data = explode(':', $rate);

                if ($data[0] >= $weight) {
                    if (isset($data[1])) {
                        $cost = $data[1];
                    }

                    break;
                }
            }
            return $this->currency->format($this->tax->calculate($cost, $this->config->get('weight_tax_class_id'), $this->config->get('config_tax')));

        } else {
            return "";
        }

    }

    public function getPackages($product_id=0,$data=array() ) {


        $sql = "SELECT pp.*, p.status FROM " . DB_PREFIX . "product_package pp
        LEFT JOIN  ".DB_PREFIX."product p ON (pp.product_id=p.product_id)  WHERE pp.package_id = '" . (int)$product_id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows == 0 || $query->row['status'] == 0) return false;

        //Ha a csomagban van olyan termék, aminek a státusza nulla, akkor a termékhez nem jön elő a csomag.
        /*$sql = "SELECT pp.*, p.status FROM " . DB_PREFIX . "product_package pp
         LEFT JOIN  ".DB_PREFIX."product p ON (pp.package_id=p.product_id)
        WHERE pp.product_id = '" . (int)$query->row['product_id'] . "'";
        $query = $this->db->query($sql);
        foreach($query->rows as $value) {
            if ($value['status'] == 0) return false;
        }*/


        $product_package_data = array();
        $sql = "SELECT * FROM " . DB_PREFIX . "product_package ";
        if ($product_id > 0) {
            $sql .= " WHERE package_id = '" . (int)$product_id . "'";
        }

        $sql .= " GROUP BY product_id ";
        if (isset($data['limit']) && $data['limit'] > 0) {
            $sql .= " LIMIT 0,".$data['limit'];
        }


        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {

            foreach ($query->rows as $result) {
                $product_package_data[] = array(
                    'product_id'    =>  $result['product_id']
                );
            }
            return $product_package_data;

        } else {
            return false;
        }

    }

    public function getProductDownloadablePdfs($product_id) {


        $sql = "SELECT * FROM " . DB_PREFIX . "product_pdf WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order";
        $query = $this->db->query($sql);

        $product_pdfs = array();

        if ($query->num_rows > 0) {

            foreach ($query->rows as $result) {
                $product_pdfs[] = array(
                    'product_id'    =>  $result['product_id'],
                    'pdf_title'     =>  $result['pdf_title'],
                    'filename'          =>  $this->getLastOfExplode('/',$result['path']),
                    'href'          =>  $this->url->link('product/product/downloadPdf', 'pdf_download_id=' . $result['pdf_id'], 'SSL')
                );
            }
            return $product_pdfs;

        } else {
            return false;
        }

    }

    public function getProductDownloadablePdf($pdf_id) {

        $sql = "SELECT * FROM " . DB_PREFIX . "product_pdf WHERE pdf_id = '" . (int)$pdf_id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {

            return $query->row;

        } else {
            return false;
        }

    }

    public function getProductVideos($product_id) {

        $sql = "SELECT * FROM " . DB_PREFIX . "product_video WHERE product_id = '" . (int)$product_id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {

            return $query->row;

        } else {
            return false;
        }

    }

    public function getProductsPackage($product_id) {
        $product_package_data = array();

        $sql = "SELECT * FROM " . DB_PREFIX . "product_package  WHERE product_id = '" . (int)$product_id . "'";


        $query = $this->db->query($sql);

        return $query->num_rows > 0 ? $query->rows : false;
    }

    public function getLastOfExplode($delimeter, $string) {
        $array_of_exploded = explode($delimeter, $string);
        return end($array_of_exploded);
    }

    public function getProductsNew($data = array()) {

        $cache = md5(http_build_query($data));

        $product_data = $this->cache->get('product_new.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache);

        if (!$product_data) {
            $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE status=1 ";



            $sql .= " ORDER BY date_added DESC ";

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            } else {
                $sql .= " LIMIT 0,10 ";

            }


            $query = $this->db->query($sql);
            $product_data = array();

            foreach ($query->rows as $result) {
                $product_data[$result['product_id']] = $result['product_id'];
            }

            $this->cache->set('product_new.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache, $product_data);
        }

        return $product_data;
    }

    }

?>