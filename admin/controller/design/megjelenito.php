<?php
class ControllerDesignMegjelenito extends Controller {
	private $error = array(); 
     
  	public function index() {
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            if (isset($this->request->post['megjelenit_termek_doboz']) ) {
                $this->request->post['megjelenit_termek_doboz'] = $this->config->rendezes($this->request->post['megjelenit_termek_doboz'], 'status','DESC','sort_order_sor', 'ASC', 'sort_order_oszlop', 'ASC');
            }
            if (isset($this->request->post['megjelenit_termek_fektetett']) ) {
                $this->request->post['megjelenit_termek_fektetett'] = $this->config->rendezes($this->request->post['megjelenit_termek_fektetett'],  'status','DESC', 'sort_order_oszlop', 'ASC', 'sort_order_sor', 'ASC');
            }
            if (isset($this->request->post['megjelenit_termek_tabok']) ) {
                $this->request->post['megjelenit_termek_tabok'] = $this->config->rendezes($this->request->post['megjelenit_termek_tabok'],  'status','DESC', 'sort_order_sor', 'ASC', 'sort_order_oszlop', 'ASC');
            }

            if (isset($this->request->post['megjelenit_lista_termek_doboz']) ) {
                $this->request->post['megjelenit_lista_termek_doboz'] = $this->config->rendezes($this->request->post['megjelenit_lista_termek_doboz'],  'status','DESC', 'sort_order_oszlop', 'ASC', 'sort_order_sor', 'ASC');
            }
            foreach($this->request->post as $key=>$value) {
                if ( $key == 'megjelenit_elolvastam_informaciok') {
                    foreach($value as $key_elolvastam=>$elolvastam) {
                        if (!isset($elolvastam['information_id'])) {
                            unset ($value[$key_elolvastam]);
                        }
                    }
                }
                if ( $key == 'megjelenit_elolvastam_informaciok_kosar') {
                    foreach($value as $key_elolvastam=>$elolvastam) {
                        if (!isset($elolvastam['information_id'])) {
                            unset ($value[$key_elolvastam]);
                        }
                    }
                }

                if (substr($key,0,11) == "megjelenit_" || substr($key,0,11) == "engedelyez_"  ){
                    $beallit[$key] = $value;
                } elseif (  substr($key,0,8) == "nomodul_" && $value == 0 ){
                    $nomodul[substr($key,8)] = $value;
                } elseif (  substr($key,0,8) == "nomodul_" && $value == 1 ){
                    $yesmodul[substr($key,8)] = $value;
                }
            }
            $this->document->setTitle($this->language->get('heading_title'));
            $this->load->model('setting/setting');

            $this->model_setting_setting->editSetting('megjelenito', $beallit);

            $this->load->model('setting/extension');
            $nomodul = isset($nomodul) ? $nomodul : "";

            $this->model_setting_extension->insertNoModule($nomodul);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('design/megjelenito', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->load->language('catalog/product');

        $this->data['text_default']           = $this->language->get('text_default');
        $this->data['text_image_manager']     = $this->language->get('text_image_manager');
        $this->data['text_browse']            = $this->language->get('text_browse');
        $this->data['text_option']            = $this->language->get('text_option');
        $this->data['text_option_value']      = $this->language->get('text_option_value');
        $this->data['text_percent']           = $this->language->get('text_percent');
        $this->data['text_amount']            = $this->language->get('text_amount');

// Column
        $this->data['column_category']        = $this->language->get('column_category');
        $this->data['column_name']            = $this->language->get('column_name');
        $this->data['column_model']           = $this->language->get('column_model');
        $this->data['column_image']           = $this->language->get('column_image');
        $this->data['column_quantity']        = $this->language->get('column_quantity');
        $this->data['column_status']          = $this->language->get('column_status');
        $this->data['column_action']          = $this->language->get('column_action');
        $this->data['column_price']           = $this->language->get('column_price');

// Entry



        $this->data['entry_name']             = $this->language->get('entry_name');
        $this->data['entry_meta_keyword']     = $this->language->get('entry_meta_keyword');
        $this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $this->data['entry_description']      = $this->language->get('entry_description');
        $this->data['entry_store']            = $this->language->get('entry_store');
        $this->data['entry_keyword']          = $this->language->get('entry_keyword');
        $this->data['entry_model']            = $this->language->get('entry_model');
        $this->data['entry_cikkszam']         = $this->language->get('entry_cikkszam');
        $this->data['entry_cikkszam2']        = $this->language->get('entry_cikkszam2');
        $this->data['entry_filter']           = $this->language->get('entry_filter');
        $this->data['entry_termek_filter_csoport']     = $this->language->get('entry_termek_filter_csoport');
        $this->data['entry_sku']              = $this->language->get('entry_sku');
        $this->data['entry_upc']              = $this->language->get('entry_upc');
        $this->data['entry_location']         = $this->language->get('entry_location');
        $this->data['entry_manufacturer']     = $this->language->get('entry_manufacturer');
        $this->data['entry_shipping']         = $this->language->get('entry_shipping');
        $this->data['entry_date_available']   = $this->language->get('entry_date_available');
        $this->data['entry_date_kaphato_ig']  = $this->language->get('entry_date_kaphato_ig');
        $this->data['entry_quantity']         = $this->language->get('entry_quantity');
        $this->data['mennyisegi_egyseg']      = $this->language->get('mennyisegi_egyseg');
        $this->data['csomagolasi_egyseg']     = $this->language->get('csomagolasi_egyseg');
        $this->data['csomagolasi_mennyiseg']  = $this->language->get('csomagolasi_mennyiseg');



        $this->data['entry_minimum']          = $this->language->get('entry_minimum');
        $this->data['entry_stock_status']     = $this->language->get('entry_stock_status');
        $this->data['entry_price']            = $this->language->get('entry_price');
        $this->data['entry_tax_class']        = $this->language->get('entry_tax_class');
        $this->data['entry_points']           = $this->language->get('entry_points');
        $this->data['entry_option_points']    = $this->language->get('entry_option_points');
        $this->data['entry_subtract']         = $this->language->get('entry_subtract');
        $this->data['entry_weight_class']     = $this->language->get('entry_weight_class');
        $this->data['entry_weight']           = $this->language->get('entry_weight');
        $this->data['entry_length']           = $this->language->get('entry_length');
        $this->data['entry_dimension']        = $this->language->get('entry_dimension');
        $this->data['entry_image']            = $this->language->get('entry_image');
        $this->data['entry_customer_group']   = $this->language->get('entry_customer_group');
        $this->data['vevo_kedvezmeny']        = $this->language->get('vevo_kedvezmeny');
        $this->data['entry_date_start']       = $this->language->get('entry_date_start');
        $this->data['entry_date_end']         = $this->language->get('entry_date_end');
        $this->data['entry_priority']         = $this->language->get('entry_priority');
        $this->data['entry_attribute']        = $this->language->get('entry_attribute');
        $this->data['entry_attribute_group']  = $this->language->get('entry_attribute_group');
        $this->data['entry_text']             = $this->language->get('entry_text');
        $this->data['entry_option']           = $this->language->get('entry_option');
        $this->data['entry_option_value']     = $this->language->get('entry_option_value');
        $this->data['entry_required']         = $this->language->get('entry_required');
        $this->data['entry_status']           = $this->language->get('entry_status');
        $this->data['entry_sort_order']       = $this->language->get('entry_sort_order');
        $this->data['entry_category']         = $this->language->get('entry_category');
        $this->data['entry_download']         = $this->language->get('entry_download');
        $this->data['entry_related']          = $this->language->get('entry_related');
        $this->data['entry_tag']   	          = $this->language->get('entry_tag');
        $this->data['entry_reward']           = $this->language->get('entry_reward');
        $this->data['entry_layout']           = $this->language->get('entry_layout felülbírálása');


        $this->data['entry_matchdate']        = $this->language->get('entry_matchdate dátuma');
        $this->data['entry_seat']             = $this->language->get('entry_seat');


        $this->load->language('design/megjelenito');

        $this->data['text_ujdonsag']             = $this->language->get('text_ujdonsag');
        $this->data['entry_ellenorzes']          = $this->language->get('entry_ellenorzes');
        $this->data['entry_megnevezes']          = $this->language->get('entry_megnevezes');
        $this->data['entry_keyword']             = $this->language->get('entry_keyword');
        $this->data['entry_meta_description']    = $this->language->get('entry_meta_description');
        $this->data['entry_description']         = $this->language->get('entry_description');
        $this->data['entry_tag']                 = $this->language->get('entry_tag');
        $this->data['entry_filter']              = $this->language->get('entry_filter');
        $this->data['entry_images']              = $this->language->get('entry_images');
        $this->data['entry_elolnezet']           = $this->language->get('entry_elolnezet');
        $this->data['entry_admin_elhelyezkedes'] = $this->language->get('entry_admin_elhelyezkedes');
        $this->data['entry_ellenorzokod']        = $this->language->get('entry_ellenorzokod');
        $this->data['tab_regisztracio_elolvastam']  = $this->language->get('tab_regisztracio_elolvastam');
        $this->data['entry_stock_date']       = $this->language->get('entry_stock_date');

        $this->data['entry_tulajdonsag_ful']    = $this->language->get('entry_tulajdonsag_ful');
        $this->data['entry_valasztek_ful']      = $this->language->get('entry_valasztek_ful');
        $this->data['entry_mennyisegi_ful']     = $this->language->get('entry_mennyisegi_ful');
        $this->data['entry_vevo_ful']           = $this->language->get('entry_vevo_ful');
        $this->data['entry_akcios_ful']         = $this->language->get('entry_akcios_ful');
        $this->data['entry_kep_ful']            = $this->language->get('entry_kep_ful');
        $this->data['entry_jutalom_ful']        = $this->language->get('entry_jutalom_ful');
        $this->data['entry_elhelyezkedes']      = $this->language->get('entry_elhelyezkedes');

        $this->data['entry_form_min_mennyiseg']    = $this->language->get('entry_form_min_mennyiseg');
        $this->data['entry_form_szallitando']      = $this->language->get('entry_form_szallitando');
        $this->data['entry_form_meret']            = $this->language->get('entry_form_meret');
        $this->data['entry_form_hosszmertek']      = $this->language->get('entry_form_hosszmertek');
        $this->data['entry_form_suly']             = $this->language->get('entry_form_suly');
        $this->data['entry_form_sulymertek']       = $this->language->get('entry_form_sulymertek');
        $this->data['entry_kiszereles']            = $this->language->get('entry_kiszereles');
        $this->data['entry_kiszerelesmertek']      = $this->language->get('entry_kiszerelesmertek');
        $this->data['entry_form_ketszintu_szuro']  = $this->language->get('entry_form_ketszintu_szuro');

        $this->data['entry_form_kapcsolodo_partnerek']   = $this->language->get('entry_form_kapcsolodo_partnerek');
        $this->data['entry_ajandekutalvanyok_ful']       = $this->language->get('entry_ajandekutalvanyok_ful');
        $this->data['entry_kupon']                       = $this->language->get('entry_kupon');
        $this->data['entry_ajandekutalvany']             = $this->language->get('entry_ajandekutalvany');
        $this->data['entry_jutalom']                     = $this->language->get('entry_jutalom');

        $this->data['entry_stock_status_id']             = $this->language->get('entry_stock_status_id');
        $this->data['entry_manufacturer_id']             = $this->language->get('entry_manufacturer_id');
        $this->data['entry_tax_class_id']                = $this->language->get('entry_tax_class_id');
        $this->data['entry_weight_class_id']             = $this->language->get('entry_weight_class_id');
        $this->data['entry_width']                       = $this->language->get('entry_width');
        $this->data['entry_height']                      = $this->language->get('entry_height');
        $this->data['entry_length_class_id']             = $this->language->get('entry_length_class_id');
        $this->data['entry_date_added']                  = $this->language->get('entry_date_added');
        $this->data['entry_date_modified']               = $this->language->get('entry_date_modified');
        $this->data['entry_viewed']                      = $this->language->get('entry_viewed');
        $this->data['entry_megyseg']                     = $this->language->get('entry_megyseg');
        $this->data['entry_csomagolasi_egyseg']          = $this->language->get('entry_csomagolasi_egyseg');
        $this->data['entry_ar_latszik']                  = $this->language->get('entry_ar_latszik');
        $this->data['entry_date_ervenyes_ig']            = $this->language->get('entry_date_ervenyes_ig');
        $this->data['entry_darabaru_meter']              = $this->language->get('entry_darabaru_meter');
        $this->data['entry_frontend_input_neve']         = $this->language->get('entry_frontend_input_neve');
        $this->data['entry_form_termek_elhelyezkedes']   = $this->language->get('entry_form_termek_elhelyezkedes');
        $this->data['entry_termek_kiemeles_modositasa']  = $this->language->get('entry_termek_kiemeles_modositasa');
        $this->data['entry_kepek_helyszinenkent']        = $this->language->get('entry_kepek_helyszinenkent');
        $this->data['entry_tab_videok']                  = $this->language->get('entry_tab_videok');
        $this->data['entry_tab_pdf']                     = $this->language->get('entry_tab_pdf');
        $this->data['entry_emailreg_jelszo']             = $this->language->get('entry_emailreg_jelszo');
        $this->data['entry_admin_rovid_leiras']          = $this->language->get('entry_admin_rovid_leiras');
        $this->data['entry_admin_tesztek']               = $this->language->get('entry_admin_tesztek');
        $this->data['entry_admin_tartozekok']            = $this->language->get('entry_admin_tartozekok');
        $this->data['entry_admin_altalanos_tartozekok']  = $this->language->get('entry_admin_altalanos_tartozekok');
        $this->data['entry_product_gyarto_termeknevben'] = $this->language->get('entry_product_gyarto_termeknevben');
        $this->data['entry_video']                       = $this->language->get('entry_video');
        $this->data['entry_lizing']                      = $this->language->get('entry_lizing');

        $this->data['entry_emailreg']                    = $this->language->get('entry_emailreg');

        $this->data['entry_register_hirlevel_kategoriak']  = $this->language->get('entry_register_hirlevel_kategoriak');
        $this->data['entry_cimmodositas_a_szerkeztesben']  = $this->language->get('entry_cimmodositas_a_szerkeztesben');
        $this->data['entry_admin_seourl_generalas']  = $this->language->get('entry_admin_seourl_generalas');
        $this->data['entry_admin_termek_cimkek']  = $this->language->get('entry_admin_termek_cimkek');
        $this->data['entry_admin_letoltheto_kep']  = $this->language->get('entry_admin_letoltheto_kep');
        $this->data['entry_admin_csomag_ajanlat']  = $this->language->get('entry_admin_csomag_ajanlat');
        $this->data['entry_lejarat_nelkuli']            = $this->language->get('entry_lejarat_nelkuli');
        $this->data['entry_default']            = $this->language->get('entry_default');
        $this->data['button_remove']        = $this->language->get('button_remove');



        if (isset($this->session->data['success']))
            $this->data['success'] = $this->language->get('text_success');
        else
            $this->data['success'] = false;

    	$this->data['heading_title'] = $this->language->get('heading_title');
 
    	$this->data['text_enabled'] =$this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
    	$this->data['text_none'] = $this->language->get('text_none');
    	$this->data['text_yes'] = $this->language->get('text_yes');
    	$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_altalanos'] = $this->language->get('text_altalanos');
		$this->data['text_modulok'] = $this->language->get('text_modulok');


        $this->data['entry_termek_kapcsolod_group_filter'] = $this->language->get('entry_termek_kapcsolod_group_filter');
        $this->data['entry_termek_kapcsolod_allitott'] = $this->language->get('entry_termek_kapcsolod_allitott');

        $this->data['entry_product_velemeny_megosztas_doboz'] = $this->language->get('entry_product_velemeny_megosztas_doboz');

        $this->data['entry_product_leiras'] = $this->language->get('entry_product_leiras');

        $this->data['entry_product_koztes'] = $this->language->get('entry_product_koztes');
        $this->data['entry_product_garancia'] = $this->language->get('entry_product_garancia');
        $this->data['entry_product_szallitas'] = $this->language->get('entry_product_szallitas');
        $this->data['entry_product_velemeny'] = $this->language->get('entry_product_velemeny');
        $this->data['entry_product_osszehasonlit_kivansag'] = $this->language->get('entry_product_osszehasonlit_kivansag');
        $this->data['entry_product_addthis'] = $this->language->get('entry_product_addthis');


        $this->data['entry_product_uj_ikon'] = $this->language->get('entry_product_uj_ikon');
        $this->data['entry_product_par_ikon'] = $this->language->get('entry_product_par_ikon');
        $this->data['entry_product_szazalek_ikon'] = $this->language->get('entry_product_szazalek_ikon');

        $this->data['entry_product_mennyisegi_ikon'] = $this->language->get('entry_product_mennyisegi_ikon');

        $this->data['entry_tabs_settings'] = $this->language->get('entry_tabs_settings');
        $this->data['entry_product_tab_description'] = $this->language->get('entry_product_tab_description');
        $this->data['entry_product_tab_meret'] = $this->language->get('entry_product_tab_meret');
        $this->data['entry_product_tab_attribute_groups'] = $this->language->get('entry_product_tab_attribute_groups');
        $this->data['entry_product_tab_review_status'] = $this->language->get('entry_product_tab_review_status');
        $this->data['entry_product_tab_products'] = $this->language->get('entry_product_tab_products');
        $this->data['entry_product_tab_test_description'] = $this->language->get('entry_product_tab_test_description');
        $this->data['entry_product_tab_image'] = $this->language->get('entry_product_tab_image');

        $this->data['entry_termek_quantity'] = $this->language->get('entry_termek_quantity');
        $this->data['entry_termek_social'] = $this->language->get('entry_termek_social');
        $this->data['entry_termek_leiras'] = $this->language->get('entry_termek_leiras');
        $this->data['entry_form_eredeti_ar'] = $this->language->get('entry_form_eredeti_ar');
        $this->data['entry_utalvany_sorrendben'] = $this->language->get('entry_utalvany_sorrendben');
        $this->data['entry_utalvany_sorrendben_asc'] = $this->language->get('entry_utalvany_sorrendben_asc');
        $this->data['entry_fixmenu'] = $this->language->get('entry_fixmenu');
        $this->data['entry_fixfilter'] = $this->language->get('entry_fixfilter');

        $this->data['entry_mennyisegre_szur'] = $this->language->get('entry_mennyisegre_szur');
        $this->data['entry_ervenesseg_datumra_szur'] = $this->language->get('entry_ervenesseg_datumra_szur');

        $this->data['entry_meddig_kaphato'] = $this->language->get('entry_meddig_kaphato');
        $this->data['entry_mikortol_kaphato'] = $this->language->get('entry_mikortol_kaphato');
        $this->data['entry_szukitett_regisztracio'] = $this->language->get('entry_szukitett_regisztracio');
        $this->data['entry_nem_regisztracio'] = $this->language->get('entry_nem_regisztracio');
        $this->data['entry_eletkor_regisztracio'] = $this->language->get('entry_eletkor_regisztracio');
        $this->data['entry_vonalkod_kuldes'] = $this->language->get('entry_vonalkod_kuldes');
        $this->data['entry_netto_ujsor'] = $this->language->get('entry_netto_ujsor');

        $this->data['entry_termek_neve_fejlec'] = $this->language->get('entry_termek_neve_fejlec');
        $this->data['entry_product_model_termeknevben'] = $this->language->get('entry_product_model_termeknevben');
        $this->data['entry_termek_neve_dobozban'] = $this->language->get('entry_termek_neve_dobozban');
        $this->data['entry_termek_kaphato'] = $this->language->get('entry_termek_kaphato');
        $this->data['entry_termek_filter_csoport'] = $this->language->get('entry_termek_filter_csoport');
        $this->data['entry_termek_filter'] = $this->language->get('entry_termek_filter');

        $this->data['entry_termek_jobb_jobb_elso'] = $this->language->get('entry_termek_jobb_jobb_elso');
        $this->data['entry_megjelenit_kosar_kivansag'] = $this->language->get('entry_megjelenit_kosar_kivansag');
        $this->data['entry_termek_jobb_jobb_masodik'] = $this->language->get('entry_termek_jobb_jobb_masodik');
        $this->data['entry_termek_jobb_jobb_harmadik'] = $this->language->get('entry_termek_jobb_jobb_harmadik');

        $this->data['entry_admin_letoltheto'] = $this->language->get('entry_admin_letoltheto');
        $this->data['entry_admin_model'] = $this->language->get('entry_admin_model');
        $this->data['entry_admin_cikkszam'] = $this->language->get('entry_admin_cikkszam');
        $this->data['entry_admin_cikkszam2'] = $this->language->get('entry_admin_cikkszam2');
        $this->data['entry_admin_szazalek'] = $this->language->get('entry_admin_szazalek');
        $this->data['entry_admin_eredeti_ar'] = $this->language->get('entry_admin_eredeti_ar');
        $this->data['entry_form_utalvany'] = $this->language->get('entry_form_utalvany');
        $this->data['entry_nokep_megjelenites_tiltasa'] = $this->language->get('entry_nokep_megjelenites_tiltasa');
        $this->data['entry_kep_megjelenites_tiltasa'] = $this->language->get('entry_kep_megjelenites_tiltasa');

        $this->data['entry_form_model'] = $this->language->get('entry_form_model');
        $this->data['entry_form_cikkszam'] = $this->language->get('entry_form_cikkszam');
        $this->data['entry_form_cikkszam2'] = $this->language->get('entry_form_cikkszam2');

        $this->data['entry_admin_keres_model'] = $this->language->get('entry_admin_keres_model');
        $this->data['entry_admin_keres_cikkszam'] = $this->language->get('entry_admin_keres_cikkszam');
        $this->data['entry_admin_keres_cikkszam2'] = $this->language->get('entry_admin_keres_cikkszam2');

        $this->data['entry_frontend_feltoltes'] = $this->language->get('entry_frontend_feltoltes');

        $this->data['entry_termek_jobb_jobb'] = $this->language->get('entry_termek_jobb_jobb');
        $this->data['entry_pagination'] = $this->language->get('entry_pagination');
        $this->data['entry_allitott'] = $this->language->get('entry_allitott');
        $this->data['entry_ikonok'] = $this->language->get('entry_ikonok');
        $this->data['entry_kosarba'] = $this->language->get('entry_kosarba');
        $this->data['entry_brutto_ar'] = $this->language->get('entry_brutto_ar');
        $this->data['entry_netto_ar'] = $this->language->get('entry_netto_ar');
        $this->data['entry_negativ_ar'] = $this->language->get('entry_negativ_ar');
        $this->data['entry_brutto_netto'] = $this->language->get('entry_brutto_netto');
        $this->data['entry_description'] = $this->language->get('entry_description');
        $this->data['entry_uj'] = $this->language->get('entry_uj');
        $this->data['entry_pardarab'] = $this->language->get('entry_pardarab');
        $this->data['entry_szazalek'] = $this->language->get('entry_szazalek');
        $this->data['entry_mennyiseg'] = $this->language->get('entry_mennyiseg');
        $this->data['entry_csomagolasi_mennyiseg'] = $this->language->get('entry_csomagolasi_mennyiseg');
        $this->data['entry_masolhato'] = $this->language->get('entry_masolhato');
        $this->data['entry_jobb_egerbolt'] = $this->language->get('entry_jobb_egerbolt');
        $this->data['entry_kivansaglista'] = $this->language->get('entry_kivansaglista');
        $this->data['entry_osszehasonlitas'] = $this->language->get('entry_osszehasonlitas');

        $this->data['entry_ikonok_fk'] = $this->language->get('entry_ikonok_fk');
        $this->data['entry_kosarba_fk'] = $this->language->get('entry_kosarba_fk');
        $this->data['entry_brutto_ar_fk'] = $this->language->get('entry_brutto_ar_fk');
        $this->data['entry_netto_ar_fk'] = $this->language->get('entry_netto_ar_fk');
        $this->data['entry_brutto_netto_fk'] = $this->language->get('entry_brutto_netto_fk');
        $this->data['entry_description_fk'] = $this->language->get('entry_description_fk');
        $this->data['entry_uj_fk'] = $this->language->get('entry_uj_fk');
        $this->data['entry_pardarab_fk'] = $this->language->get('entry_pardarab_fk');
        $this->data['entry_szazalek_fk'] = $this->language->get('entry_szazalek_fk');
        $this->data['entry_mennyiseg_fk'] = $this->language->get('entry_mennyiseg_fk');
        $this->data['entry_csomagolasi_mennyiseg_fk'] = $this->language->get('entry_csomagolasi_mennyiseg_fk');
        $this->data['entry_masolhato_fk'] = $this->language->get('entry_masolhato_fk');
        $this->data['entry_jobb_egerbolt_fk'] = $this->language->get('entry_jobb_egerbolt_fk');
        $this->data['entry_kivansaglista_fk'] = $this->language->get('entry_kivansaglista_fk');
        $this->data['entry_osszehasonlitas_fk'] = $this->language->get('entry_osszehasonlitas_fk');
        $this->data['entry_facebook_fk'] = $this->language->get('entry_facebook_fk');
        $this->data['entry_email_fk'] = $this->language->get('entry_email_fk');

        $this->data['entry_lista_kosarba'] = $this->language->get('entry_lista_kosarba');
        $this->data['entry_lista_brutto_ar'] = $this->language->get('entry_lista_brutto_ar');
        $this->data['entry_lista_netto_ar'] = $this->language->get('entry_lista_netto_ar');
        $this->data['entry_lista_brutto_netto'] = $this->language->get('entry_lista_brutto_netto');
        $this->data['entry_lista_description'] = $this->language->get('entry_lista_description');
        $this->data['entry_lista_uj'] = $this->language->get('entry_lista_uj');
        $this->data['entry_lista_pardarab'] = $this->language->get('entry_lista_pardarab');
        $this->data['entry_lista_szazalek'] = $this->language->get('entry_lista_szazalek');
        $this->data['entry_lista_mennyiseg'] = $this->language->get('entry_lista_mennyiseg');
        $this->data['entry_lista_csomagolasi_mennyiseg'] = $this->language->get('entry_lista_csomagolasi_mennyiseg');
        $this->data['entry_lista_kivansaglista'] = $this->language->get('entry_lista_kivansaglista');
        $this->data['entry_lista_osszehasonlitas'] = $this->language->get('entry_lista_osszehasonlitas');
        $this->data['entry_lista_mennyisegi'] = $this->language->get('entry_lista_mennyisegi');

        $this->data['ikon_fent_vizszintes'] = $this->language->get('ikon_fent_vizszintes');
        $this->data['ikon_fent_fuggoleges'] = $this->language->get('ikon_fent_fuggoleges');
        $this->data['ikon_kep_felett'] = $this->language->get('ikon_kep_felett');
        $this->data['ikon_kep_mellett'] = $this->language->get('ikon_kep_mellett');

        $this->data['entry_ikon_helye'] = $this->language->get('entry_ikon_helye');
        $this->data['entry_ikon_helye_fk'] = $this->language->get('entry_ikon_helye_fk');



        $this->data['entry_kiskepek_megjelenitese'] = $this->language->get('entry_kiskepek_megjelenitese');
        $this->data['entry_kiskepek_slider'] = $this->language->get('entry_kiskepek_slider');
        $this->data['entry_kep_szoveg'] = $this->language->get('entry_kep_szoveg');
        $this->data['entry_kep_szoveg_fk'] = $this->language->get('entry_kep_szoveg_fk');
        $this->data['entry_minicart_fk'] = $this->language->get('entry_minicart_fk');
        $this->data['entry_megnezemgomb_fk'] = $this->language->get('entry_megnezemgomb_fk');

        $this->data['entry_slideshow'] = $this->language->get('entry_slideshow');
        $this->data['entry_csomagolas_admin'] = $this->language->get('entry_csomagolas_admin');

        $this->data['entry_product_jelentkezzen'] = $this->language->get('entry_product_jelentkezzen');
        $this->data['entry_product_elerhetoseg'] = $this->language->get('entry_product_elerhetoseg');
        $this->data['entry_product_suly'] = $this->language->get('entry_product_suly');
        $this->data['entry_product_mennyisegi_egyseg'] = $this->language->get('entry_product_mennyisegi_egyseg');
        $this->data['entry_product_cikkszam'] = $this->language->get('entry_product_cikkszam');
        $this->data['entry_product_cikkszam2'] = $this->language->get('entry_product_cikkszam2');
        $this->data['entry_product_model'] = $this->language->get('entry_product_model');
        $this->data['entry_product_gyarto'] = $this->language->get('entry_product_gyarto');
        $this->data['entry_product_reward'] = $this->language->get('entry_product_reward');
        $this->data['entry_product_sku'] = $this->language->get('entry_product_sku');
        $this->data['entry_product_meret'] = $this->language->get('entry_product_meret');
        $this->data['entry_footer_kategoria'] = $this->language->get('entry_footer_kategoria');

        $this->data['entry_product_sorrend'] = $this->language->get('entry_product_sorrend');

        $this->data['entry_megjelenit_minicart_kosar'] = $this->language->get('entry_megjelenit_minicart_kosar');

        $this->data['kep_szoveg'] = $this->language->get('kep_szoveg');
        $this->data['kep_szam'] = $this->language->get('kep_szam');
        $this->data['kep_szoveg_szam'] = $this->language->get('kep_szoveg_szam');
        $this->data['kep_szam_szoveg'] = $this->language->get('kep_szam_szoveg');
        $this->data['szam_kep'] = $this->language->get('szam_kep');
        $this->data['szam_szoveg'] = $this->language->get('szam_szoveg');
        $this->data['szam_kep_szoveg'] = $this->language->get('szam_kep_szoveg');
        $this->data['szam_szoveg_kep'] = $this->language->get('szam_szoveg_kep');
        $this->data['szoveg_szam'] = $this->language->get('szoveg_szam');
        $this->data['szoveg_kep'] = $this->language->get('szoveg_kep');
        $this->data['szoveg_szam_kep'] = $this->language->get('szoveg_szam_kep');
        $this->data['szoveg_kep_szam'] = $this->language->get('szoveg_kep_szam');



        $this->data['entry_admin_sorrend'] = $this->language->get('entry_admin_sorrend');


        $this->data['entry_kosar_kupon']                = $this->language->get('entry_kosar_kupon');
        $this->data['entry_kosar_suly']                 = $this->language->get('entry_kosar_suly');
        $this->data['entry_penztar_fizetesi_mod']       = $this->language->get('entry_penztar_fizetesi_mod');
        $this->data['entry_model_adat']                 = $this->language->get('entry_model_adat');
        $this->data['entry_netto_adat']                 = $this->language->get('entry_netto_adat');
        $this->data['entry_admin_cart_shipping_region'] = $this->language->get('entry_admin_cart_shipping_region');
        $this->data['entry_admin_cart_shipping_postcode'] = $this->language->get('entry_admin_cart_shipping_postcode');
        $this->data['entry_penznem_kiiras']             = $this->language->get('entry_penznem_kiiras');
        $this->data['entry_footer_popup']               = $this->language->get('entry_footer_popup');
        $this->data['entry_vizjel']                     = $this->language->get('entry_vizjel');
        $this->data['entry_admin_nyelv_ellenorzes']     = $this->language->get('entry_admin_nyelv_ellenorzes');
        $this->data['entry_admin_ar_ellenorzes']        = $this->language->get('entry_admin_ar_ellenorzes');
        $this->data['entry_admin_ketszintu_szuro_ellenorzes'] = $this->language->get('entry_admin_ketszintu_szuro_ellenorzes');
        $this->data['entry_admin_ervenyes_ig_ellenorzes'] = $this->language->get('entry_admin_ervenyes_ig_ellenorzes');
        $this->data['entry_category_leiras']            = $this->language->get('entry_category_leiras');
        $this->data['entry_category_kep_fejlecben']     = $this->language->get('entry_category_kep_fejlecben');
        $this->data['entry_category_kep_fejlec_felett'] = $this->language->get('entry_category_kep_fejlec_felett');
        $this->data['entry_category_null_fejlecben']    = $this->language->get('entry_category_null_fejlecben');
        $this->data['entry_piacter']                    = $this->language->get('entry_piacter');
        $this->data['entry_mini_cart']                  = $this->language->get('entry_mini_cart');
        $this->data['entry_vatera']                     = $this->language->get('entry_vatera');
        $this->data['entry_description_karakter_szam']  = $this->language->get('entry_description_karakter_szam');
        $this->data['entry_description_karakter_szam_fk'] = $this->language->get('entry_description_karakter_szam_fk');
        $this->data['entry_gorgetosav']                 = $this->language->get('entry_gorgetosav');
        $this->data['entry_sap']                        = $this->language->get('entry_sap');
        $this->data['entry_gls']                        = $this->language->get('entry_gls');

        $this->data['entry_register_iskolai_vegzettseg'] = $this->language->get('entry_register_iskolai_vegzettseg');
        $this->data['entry_register_address_street'] = $this->language->get('entry_register_address_street');
        $this->data['entry_register_address_city'] = $this->language->get('entry_register_address_city');
        $this->data['entry_register_address_postcode'] = $this->language->get('entry_register_address_postcode');
        $this->data['entry_register_address_country'] = $this->language->get('entry_register_address_country');
        $this->data['entry_register_address_region'] = $this->language->get('entry_register_address_region');

        $this->data['entry_registerblock_szemelyes'] = $this->language->get('entry_registerblock_szemelyes');
        $this->data['entry_registration_default'] = $this->language->get('entry_registration_default');
        $this->data['entry_registration_company'] = $this->language->get('entry_registration_company');
        $this->data['entry_registerblock_cim'] = $this->language->get('entry_registerblock_cim');
        $this->data['entry_registerblock_ceges_cim'] = $this->language->get('entry_registerblock_ceges_cim');
        $this->data['entry_registerblock_paypal'] = $this->language->get('entry_registerblock_paypal');
        $this->data['entry_registerblock_hirlevel'] = $this->language->get('entry_registerblock_hirlevel');

        $this->data['entry_register_vezeteknev'] = $this->language->get('entry_register_vezeteknev');
        $this->data['entry_register_keresztnev'] = $this->language->get('entry_register_keresztnev');
        $this->data['entry_register_cegnev'] = $this->language->get('entry_register_cegnev');
        $this->data['entry_register_adoszam'] = $this->language->get('entry_register_adoszam');
        $this->data['entry_register_vallalkozasi_forma'] = $this->language->get('entry_register_vallalkozasi_forma');
        $this->data['entry_register_szekhely'] = $this->language->get('entry_register_szekhely');
        $this->data['entry_register_ugyvezeto_neve'] = $this->language->get('entry_register_ugyvezeto_neve');
        $this->data['entry_register_ugyvezeto_telefonszama'] = $this->language->get('entry_register_ugyvezeto_telefonszama');

        $this->data['entry_register_address_cegnev'] = $this->language->get('entry_register_address_cegnev');
        $this->data['entry_register_address_adoszam'] = $this->language->get('entry_register_address_adoszam');
        $this->data['entry_register_address_vallalkozasi_forma'] = $this->language->get('entry_register_address_vallalkozasi_forma');
        $this->data['entry_register_address_szekhely'] = $this->language->get('entry_register_address_szekhely');
        $this->data['entry_register_address_ugyvezeto_neve'] = $this->language->get('entry_register_address_ugyvezeto_neve');
        $this->data['entry_register_address_ugyvezeto_telefonszama'] = $this->language->get('entry_register_address_ugyvezeto_telefonszama');
        $this->data['entry_register_address_kiegeszito'] = $this->language->get('entry_register_address_kiegeszito');

        $this->data['entry_register_email'] = $this->language->get('entry_register_email');
        $this->data['entry_register_email_megerosites'] = $this->language->get('entry_register_email_megerosites');
        $this->data['entry_register_telefon'] = $this->language->get('entry_register_telefon');
        $this->data['entry_register_fax'] = $this->language->get('entry_register_fax');
        $this->data['entry_register_paypal'] = $this->language->get('entry_register_paypal');

        $this->data['entry_admin_biztonsagi_kod'] = $this->language->get('entry_admin_biztonsagi_kod');
        $this->data['entry_admin_nincs_negativ_ar'] = $this->language->get('entry_admin_nincs_negativ_ar');


        $this->data['tab_frontend_adat_bekeres'] = $this->language->get('tab_frontend_adat_bekeres');
        $this->data['tab_frontend_lista_megjeleni'] = $this->language->get('tab_frontend_lista_megjeleni');


    	$this->data['button_save'] = $this->language->get('button_save');
    	$this->data['button_cancel'] = $this->language->get('button_cancel');

    	$this->data['tab_data']     = $this->language->get('tab_data');
    	$this->data['tab_fektetett']     = $this->language->get('tab_fektetett');
    	$this->data['tab_allitott']     = $this->language->get('tab_allitott');

    	$this->data['tab_lista']    = $this->language->get('tab_lista');
    	$this->data['tab_view']    = $this->language->get('tab_view');
    	$this->data['tab_kosar']    = $this->language->get('tab_kosar');
    	$this->data['tab_header']    = $this->language->get('tab_header');
    	$this->data['tab_frontend_top']    = $this->language->get('tab_frontend_top');
    	$this->data['tab_backend_top']    = $this->language->get('tab_backend_top');
    	$this->data['tab_altalanos']    = $this->language->get('tab_altalanos');
    	$this->data['tab_altalanosadmin']    = $this->language->get('tab_altalanosadmin');
    	$this->data['tab_admin']    = $this->language->get('tab_admin');
    	$this->data['tab_frontend']    = $this->language->get('tab_frontend');
    	$this->data['tab_product']  = $this->language->get('tab_product');
    	$this->data['tab_nomodul']  = $this->language->get('tab_nomodul');
    	$this->data['tab_termekful']  = $this->language->get('tab_termekful');
    	$this->data['tab_termektulajdonsag']  = $this->language->get('tab_termektulajdonsag');
    	$this->data['tab_rendeles']  = $this->language->get('tab_rendeles');
    	$this->data['tab_vevok']  = $this->language->get('tab_vevok');
    	$this->data['tab_export']  = $this->language->get('tab_export');

    	$this->data['text_csv_vevok_title']    = $this->language->get('text_csv_vevok_title');
    	$this->data['text_csv_vevok_setting']  = $this->language->get('text_csv_vevok_setting');
    	$this->data['text_partnerkod_vevok_setting']  = $this->language->get('text_partnerkod_vevok_setting');
    	$this->data['text_group_name']         = $this->language->get('text_group_name');

        $this->data['tab_alapmodulok'] = $this->language->get('tab_alapmodulok');
        $this->data['tab_fizetes'] = $this->language->get('tab_fizetes');
        $this->data['tab_szallitas'] = $this->language->get('tab_szallitas');
        $this->data['tab_regisztracio'] = $this->language->get('tab_regisztracio');
        $this->data['tab_regisztracio_ceges'] = $this->language->get('tab_regisztracio_ceges');
        $this->data['tab_email']           = $this->language->get('tab_email');
        $this->data['entry_regisztracios_email_jelszo']           = $this->language->get('entry_regisztracios_email_jelszo');
        $this->data['entry_emlekezz_ram']           = $this->language->get('entry_emlekezz_ram');
        $this->data['entry_facebook_login_fiok']           = $this->language->get('entry_facebook_login_fiok');
        $this->data['entry_facebook_login_penztar']           = $this->language->get('entry_facebook_login_penztar');
        $this->data['entry_cookie_figyelmeztetes']           = $this->language->get('entry_cookie_figyelmeztetes');
        $this->data['entry_megjelenit_popup_eredeti_meret']           = $this->language->get('entry_megjelenit_popup_eredeti_meret');


  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('design/megjelenito', 'token=' . $this->session->data['token'], 'SSL'),

            'separator' => ' :: '
   		);


        $this->data['action'] = $this->url->link('design/megjelenito', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];



        // termék doboz templetes állított

        $termek_dobozok = $this->config->get('megjelenit_termek_doboz');
        if (is_null($termek_dobozok)) {
            $termek_dobozok = array();
        }
        $termek_dobozok_beallitas = array();
        if (is_dir(DIR_CATALOG . $this->config->get('config_template') . '/template/module/termek_doboz')) {
            $files = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/module/termek_doboz/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/module/termek_doboz/*.php');
        } else {
            $files = glob(DIR_CATALOG . 'view/theme/default/template/module/termek_doboz/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/default/template/module/termek_doboz/*.php');
        }

        foreach($termek_dobozok as $key=>$value){
            $talalt = false;
            foreach($files as $file) {
                $extension = basename($file, '.tpl');
                if ($key == $extension) {
                    $talalt = true;
                    break;
                }
            }
            if (!$talalt) {
                unset ($termek_dobozok[$key]);
            }
        }

        foreach ($files as $file) {
            $datas=array();

            $extension = basename($file, '.tpl');
            if (!array_key_exists($extension,$termek_dobozok) ) {
                $termek_dobozok[$extension] = array(
                    'status' => 0,
                    'sort_order_sor' => 0,
                    'sort_order_oszlop' => 0
                );
            }
            $beallitas = str_replace(".tpl",".php",$file);
            if (in_array($beallitas,$files_beallitasok)) {
                $_=array();
                require($beallitas);

                $datas = array_merge($datas, $_);
                foreach($datas as $keydata=>$data) {
                    $termek_dobozok_beallitas[$extension][$keydata] = $data;
                }
            }
        }
        $this->data['megjelenit_termek_doboz']= $termek_dobozok;
        $this->data['megjelenit_termek_doboz_beallitas']= $termek_dobozok_beallitas;
        $this->data['megjelenit_modularis']= $this->config->get('megjelenit_modularis');



        //termék doboz templetes, fektetett
        $termek_fektetett = $this->config->get('megjelenit_termek_fektetett');
        if (is_null($termek_fektetett)) {
            $termek_fektetett = array();
        }
        $termek_fektetett_beallitas = array();
        if (is_dir(DIR_CATALOG . $this->config->get('config_template') . '/template/module/termek_doboz')) {
            $files = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/module/termek_doboz/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/module/termek_doboz/*.php');
        } else {
            $files = glob(DIR_CATALOG . 'view/theme/default/template/module/termek_doboz/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/default/template/module/termek_doboz/*.php');
        }

        foreach($termek_fektetett as $key=>$value){
            $talalt = false;
            foreach($files as $file) {
                $extension = basename($file, '.tpl');
                if ($key == $extension) {
                    $talalt = true;
                    break;
                }
            }
            if (!$talalt) {
                unset ($termek_fektetett[$key]);
            }
        }

        foreach ($files as $file) {
            $datas=array();

            $extension = basename($file, '.tpl');
            if (!array_key_exists($extension,$termek_fektetett) ) {
                $termek_fektetett[$extension] = array(
                    'status' => 0,
                    'sort_order_sor' => 0,
                    'sort_order_oszlop' => 0
                );
            }
            $beallitas = str_replace(".tpl",".php",$file);
            if (in_array($beallitas,$files_beallitasok)) {
                $_=array();
                require($beallitas);

                $datas = array_merge($datas, $_);
                foreach($datas as $keydata=>$data) {
                    $termek_fektetett_beallitas[$extension][$keydata] = $data;
                }
            }
        }
        $this->data['megjelenit_termek_fektetett']= $termek_fektetett;
        $this->data['megjelenit_termek_fektetett_beallitas']= $termek_fektetett_beallitas;
        //termék templetes fektetett vége


        //termék lista templetes
        $termek_lista = $this->config->get('megjelenit_lista_termek_doboz');
        if (is_null($termek_lista)) {
            $termek_lista = array();
        }
        $termek_fektetett_beallitas = array();
        if (is_dir(DIR_CATALOG . $this->config->get('config_template') . '/template/module/termek_doboz')) {
            $files = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/module/termek_doboz/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/module/termek_doboz/*.php');
        } else {
            $files = glob(DIR_CATALOG . 'view/theme/default/template/module/termek_doboz/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/default/template/module/termek_doboz/*.php');
        }

        foreach($termek_lista as $key=>$value){
            $talalt = false;
            foreach($files as $file) {
                $extension = basename($file, '.tpl');
                if ($key == $extension) {
                    $talalt = true;
                    break;
                }
            }
            if (!$talalt) {
                unset ($termek_lista[$key]);
            }
        }

        foreach ($files as $file) {
            $datas=array();

            $extension = basename($file, '.tpl');
            if (!array_key_exists($extension,$termek_lista) ) {
                $termek_lista[$extension] = array(
                    'status' => 0,
                    'sort_order_sor' => 0,
                    'sort_order_oszlop' => 0
                );
            }
            $beallitas = str_replace(".tpl",".php",$file);
            if (in_array($beallitas,$files_beallitasok)) {
                $_=array();
                require($beallitas);

                $datas = array_merge($datas, $_);
                foreach($datas as $keydata=>$data) {
                    $termek_lista_beallitas[$extension][$keydata] = $data;
                }
            }
        }
        $this->data['megjelenit_lista_termek_doboz']= $termek_lista;
        $this->data['megjelenit_termek_lista_beallitas']= $termek_lista_beallitas;
        //termék lista templates vége



        /* termék aloldal tab templetek*/

        $termek_tabok = $this->config->get('megjelenit_termek_tabok');
        if (is_null($termek_tabok)) {
            $termek_tabok = array();
        }
        $termek_tabok_beallitas = array();
        if (is_dir(DIR_CATALOG . $this->config->get('config_template') . '/template/product/product')) {
            $files = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/product/product/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/product/product/*.php');
        } else {
            $files = glob(DIR_CATALOG . 'view/theme/default/template/product/product/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/default/template/product/product/*.php');
        }
        foreach ($files as $file) {
            $datas=array();

            $extension = basename($file, '.tpl');
            if (!array_key_exists($extension,$termek_tabok) ) {
                $termek_tabok[$extension] = array(
                    'status' => 0,
                    'sort_order_sor' => 0,
                    'sort_order_oszlop' => 0
                );
            }
            $beallitas = str_replace(".tpl",".php",$file);
            if (in_array($beallitas,$files_beallitasok)) {
                $_=array();
                require($beallitas);

                $datas = array_merge($datas, $_);
                foreach($datas as $keydata=>$data) {
                    $termek_tabok_beallitas[$extension][$keydata] = $data;
                }
            }
        }
        $this->data['megjelenit_termek_tabok']= $termek_tabok;
        $this->data['megjelenit_termek_tabok_beallitas']= $termek_tabok_beallitas;
        /* termék aloldal tab templetek vége*/



        /*megrendelm gombra checkout template */
        $this->data['megjelenit_checkout_template'] = $this->config->get('megjelenit_checkout_template') ? $this->config->get('megjelenit_checkout_template') : "checkout_megrendelem";

        $this->data['megjelenit_checkout_templates'] = array();
        if (is_dir(DIR_CATALOG . $this->config->get('config_template') . '/template/checkout')) {
            $files = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/checkout/checkout_megrendelem*.tpl');
        } else {
            $files = glob(DIR_CATALOG . 'view/theme/default/template/checkout/checkout_megrendelem*.tpl');
        }
        $this->data['megjelenit_checkout_templates'] = array();

        if (!empty($files)) {
            foreach($files as $file) {
                $extension = basename($file, '.tpl');
                $this->data['megjelenit_checkout_templates'][] = $extension;
            }
        }


        /*megrendelm gombra checkout template */



        $this->load->model('catalog/information');

        $this->data['information_pages'] 	= $this->model_catalog_information->getInformations();



        $megjelenit_elolvastam_informaciok =  $this->config->get('megjelenit_elolvastam_informaciok');
        if (is_null($megjelenit_elolvastam_informaciok)) {
            $this->data['megjelenit_elolvastam_informaciok'][11]['information_id'] = "11";
            $this->data['megjelenit_elolvastam_informaciok'][11]['sort_order'] = "1";
        } else {
            $this->data['megjelenit_elolvastam_informaciok'] = $megjelenit_elolvastam_informaciok;
        }
        $megjelenit_elolvastam =  $this->config->get('megjelenit_elolvastam');
        if ( is_null($megjelenit_elolvastam) ) {
            $this->data['megjelenit_elolvastam'][$this->config->get('config_language_id')] = $this->language->get('text_agree');
        } else {
            $this->data['megjelenit_elolvastam'] =  $megjelenit_elolvastam;

        }






        $megjelenit_elolvastam_informaciok_kosar =  $this->config->get('megjelenit_elolvastam_informaciok_kosar');
        if (is_null($megjelenit_elolvastam_informaciok_kosar)) {
            $this->data['megjelenit_elolvastam_informaciok_kosar'][11]['information_id'] = "11";
            $this->data['megjelenit_elolvastam_informaciok_kosar'][11]['sort_order'] = "1";
        } else {
            $this->data['megjelenit_elolvastam_informaciok_kosar'] = $megjelenit_elolvastam_informaciok_kosar;
        }
        $megjelenit_elolvastam_kosar =  $this->config->get('megjelenit_elolvastam_kosar');
        if ( is_null($megjelenit_elolvastam_kosar) ) {
            $this->data['megjelenit_elolvastam_kosar'][$this->config->get('config_language_id')] = $this->language->get('text_agree');
        } else {
            $this->data['megjelenit_elolvastam_kosar'] =  $megjelenit_elolvastam_kosar;

        }


        $this->load->model('localisation/language');
        $this->data['languages'] = $this->model_localisation_language->getLanguages();



        if (isset($this->request->post['megjelenit_allitott'])) {
            $this->data['megjelenit_allitott'] = $this->request->post['megjelenit_allitott'];
        } else {
            $this->data['megjelenit_allitott'] = $this->config->get('megjelenit_allitott');
        }

        if (isset($this->request->post['megjelenit_altalanos_cache'])) {
            $this->data['megjelenit_altalanos_cache'] = $this->request->post['megjelenit_altalanos_cache'];
        } else {
            $this->data['megjelenit_altalanos_cache'] = $this->config->get('megjelenit_altalanos_cache');
        }

        if (isset($this->request->post['megjelenit_kosarba'])) {
            $this->data['megjelenit_kosarba'] = $this->request->post['megjelenit_kosarba'];
        } else {
            $this->data['megjelenit_kosarba'] = $this->config->get('megjelenit_kosarba');
        }

        if (isset($this->request->post['megjelenit_footer_kategoria'])) {
            $this->data['megjelenit_footer_kategoria'] = $this->request->post['megjelenit_footer_kategoria'];
        } else {
            $this->data['megjelenit_footer_kategoria'] = $this->config->get('megjelenit_footer_kategoria');
        }

        if (isset($this->request->post['megjelenit_regisztracio_szukitett'])) {
            $this->data['megjelenit_regisztracio_szukitett'] = $this->request->post['megjelenit_regisztracio_szukitett'];
        } else {
            $this->data['megjelenit_regisztracio_szukitett'] = $this->config->get('megjelenit_regisztracio_szukitett');
        }

        if (isset($this->request->post['megjelenit_nem_regisztracio'])) {
            $this->data['megjelenit_nem_regisztracio'] = $this->request->post['megjelenit_nem_regisztracio'];
        } else {
            $this->data['megjelenit_nem_regisztracio'] = $this->config->get('megjelenit_nem_regisztracio');
        }

        if (isset($this->request->post['megjelenit_eletkor_regisztracio'])) {
            $this->data['megjelenit_eletkor_regisztracio'] = $this->request->post['megjelenit_eletkor_regisztracio'];
        } else {
            $this->data['megjelenit_eletkor_regisztracio'] = $this->config->get('megjelenit_eletkor_regisztracio');
        }

        if (isset($this->request->post['megjelenit_vonalkod_kuldes'])) {
            $this->data['megjelenit_vonalkod_kuldes'] = $this->request->post['megjelenit_vonalkod_kuldes'];
        } else {
            $this->data['megjelenit_vonalkod_kuldes'] = $this->config->get('megjelenit_vonalkod_kuldes');
        }

        if (isset($this->request->post['megjelenit_utalvany_szerint_rendez'])) {
            $this->data['megjelenit_utalvany_szerint_rendez'] = $this->request->post['megjelenit_utalvany_szerint_rendez'];
        } else {
            $this->data['megjelenit_utalvany_szerint_rendez'] = $this->config->get('megjelenit_utalvany_szerint_rendez');
        }

        if (isset($this->request->post['megjelenit_utalvany_szerint_rendez_ask'])) {
            $this->data['megjelenit_utalvany_szerint_rendez_ask'] = $this->request->post['megjelenit_utalvany_szerint_rendez_ask'];
        } else {
            $this->data['megjelenit_utalvany_szerint_rendez_ask'] = $this->config->get('megjelenit_utalvany_szerint_rendez_ask');
        }

        if (isset($this->request->post['megjelenit_meddig_kaphato'])) {
            $this->data['megjelenit_meddig_kaphato'] = $this->request->post['megjelenit_meddig_kaphato'];
        } else {
            $this->data['megjelenit_meddig_kaphato'] = $this->config->get('megjelenit_meddig_kaphato');
        }

        if (isset($this->request->post['megjelenit_mikortol_kaphato'])) {
            $this->data['megjelenit_mikortol_kaphato'] = $this->request->post['megjelenit_mikortol_kaphato'];
        } else {
            $this->data['megjelenit_mikortol_kaphato'] = $this->config->get('megjelenit_mikortol_kaphato');
        }

        if (isset($this->request->post['megjelenit_felul_lapszamozas'])) {
            $this->data['megjelenit_felul_lapszamozas'] = $this->request->post['megjelenit_felul_lapszamozas'];
        } else {
            $this->data['megjelenit_felul_lapszamozas'] = $this->config->get('megjelenit_felul_lapszamozas');
        }



        $this->data['megjelenit_keres_bovit']        = isset($this->request->post['megjelenit_keres_bovit'])         ? $this->request->post['megjelenit_keres_bovit']        : $this->config->get('megjelenit_keres_bovit');
        $this->data['megjelenit_keres_autocomplete'] = isset($this->request->post['megjelenit_keres_autocomplete'])  ? $this->request->post['megjelenit_keres_autocomplete'] : $this->config->get('megjelenit_keres_autocomplete');
        $this->data['megjelenit_keres_gyartora']     = isset($this->request->post['megjelenit_keres_gyartora'])      ? $this->request->post['megjelenit_keres_gyartora']     : $this->config->get('megjelenit_keres_gyartora');
        $this->data['megjelenit_keres_modell']       = isset($this->request->post['megjelenit_keres_modell'])        ? $this->request->post['megjelenit_keres_modell']       : $this->config->get('megjelenit_keres_modell');
        $this->data['megjelenit_keres_cikkszam']     = isset($this->request->post['megjelenit_keres_cikkszam'])      ? $this->request->post['megjelenit_keres_cikkszam']     : $this->config->get('megjelenit_keres_cikkszam');
        $this->data['megjelenit_keres_cikkszam2']    = isset($this->request->post['megjelenit_keres_cikkszam2'])     ? $this->request->post['megjelenit_keres_cikkszam2']    : $this->config->get('megjelenit_keres_cikkszam2');
        $this->data['megjelenit_keres_rovid_leiras'] = isset($this->request->post['megjelenit_keres_rovid_leiras'])  ? $this->request->post['megjelenit_keres_rovid_leiras'] : $this->config->get('megjelenit_keres_rovid_leiras');
        $this->data['megjelenit_keres_hosszu_leiras']= isset($this->request->post['megjelenit_keres_hosszu_leiras']) ? $this->request->post['megjelenit_keres_hosszu_leiras']: $this->config->get('megjelenit_keres_hosszu_leiras');

        $this->data['megjelenit_arajanlat_telefon']             = isset($this->request->post['megjelenit_arajanlat_telefon'])               ? $this->request->post['megjelenit_arajanlat_telefon']              : $this->config->get('megjelenit_arajanlat_telefon');
        $this->data['megjelenit_arajanlat_telefon_kotelezo']    = isset($this->request->post['megjelenit_arajanlat_telefon_kotelezo'])      ? $this->request->post['megjelenit_arajanlat_telefon_kotelezo']     : $this->config->get('megjelenit_arajanlat_telefon_kotelezo');
        $this->data['megjelenit_arajanlat_iranyitoszam']        = isset($this->request->post['megjelenit_arajanlat_iranyitoszam'])          ? $this->request->post['megjelenit_arajanlat_iranyitoszam']         : $this->config->get('megjelenit_arajanlat_iranyitoszam');
        $this->data['megjelenit_arajanlat_iranyitoszam_kotelezo']= isset($this->request->post['megjelenit_arajanlat_iranyitoszam_kotelezo'])? $this->request->post['megjelenit_arajanlat_iranyitoszam_kotelezo']: $this->config->get('megjelenit_arajanlat_iranyitoszam_kotelezo');
        $this->data['megjelenit_arajanlat_varos']               = isset($this->request->post['megjelenit_arajanlat_varos'])                 ? $this->request->post['megjelenit_arajanlat_varos']                : $this->config->get('megjelenit_arajanlat_varos');
        $this->data['megjelenit_arajanlat_varos_kotelezo']      = isset($this->request->post['megjelenit_arajanlat_varos_kotelezo'])        ? $this->request->post['megjelenit_arajanlat_varos_kotelezo']       : $this->config->get('megjelenit_arajanlat_varos_kotelezo');
        $this->data['megjelenit_arajanlat_utca']                = isset($this->request->post['megjelenit_arajanlat_utca'])                  ? $this->request->post['megjelenit_arajanlat_utca']                 : $this->config->get('megjelenit_arajanlat_utca');
        $this->data['megjelenit_arajanlat_utca_kotelezo']       = isset($this->request->post['megjelenit_arajanlat_utca_kotelezo'])         ? $this->request->post['megjelenit_arajanlat_utca_kotelezo']        : $this->config->get('megjelenit_arajanlat_utca_kotelezo');
        $this->data['megjelenit_arajanlat_vallalkozas']         = isset($this->request->post['megjelenit_arajanlat_vallalkozas'])           ? $this->request->post['megjelenit_arajanlat_vallalkozas']          : $this->config->get('megjelenit_arajanlat_vallalkozas');
        $this->data['megjelenit_arajanlat_vallalkozas_kotelezo']= isset($this->request->post['megjelenit_arajanlat_vallalkozas_kotelezo'])  ? $this->request->post['megjelenit_arajanlat_vallalkozas_kotelezo'] : $this->config->get('megjelenit_arajanlat_vallalkozas_kotelezo');
        $this->data['megjelenit_arajanlat_adoszam']             = isset($this->request->post['megjelenit_arajanlat_adoszam'])               ? $this->request->post['megjelenit_arajanlat_adoszam']              : $this->config->get('megjelenit_arajanlat_adoszam');
        $this->data['megjelenit_arajanlat_adoszam_kotelezo']    = isset($this->request->post['megjelenit_arajanlat_adoszam_kotelezo'])      ? $this->request->post['megjelenit_arajanlat_adoszam_kotelezo']     : $this->config->get('megjelenit_arajanlat_adoszam_kotelezo');








        if (isset($this->request->post['megjelenit_brutto_ar'])) {
            $this->data['megjelenit_brutto_ar'] = $this->request->post['megjelenit_brutto_ar'];
        } else {
            $this->data['megjelenit_brutto_ar'] = $this->config->get('megjelenit_brutto_ar');
        }

        if (isset($this->request->post['megjelenit_netto_ar'])) {
            $this->data['megjelenit_netto_ar'] = $this->request->post['megjelenit_netto_ar'];
        } else {
            $this->data['megjelenit_netto_ar'] = $this->config->get('megjelenit_netto_ar');
        }

        if (isset($this->request->post['megjelenit_netto_ujsor'])) {
            $this->data['megjelenit_netto_ujsor'] = $this->request->post['megjelenit_netto_ujsor'];
        } else {
            $this->data['megjelenit_netto_ujsor'] = $this->config->get('megjelenit_netto_ujsor');
        }

        if (isset($this->request->post['megjelenit_brutto_netto'])) {
            $this->data['megjelenit_brutto_netto'] = $this->request->post['megjelenit_brutto_netto'];
        } else {
            $this->data['megjelenit_brutto_netto'] = $this->config->get('megjelenit_brutto_netto');
        }

        if (isset($this->request->post['megjelenit_negativ_ar'])) {
            $this->data['megjelenit_negativ_ar'] = $this->request->post['megjelenit_negativ_ar'];
        } else {
            $this->data['megjelenit_negativ_ar'] = $this->config->get('megjelenit_negativ_ar');
        }

        if (isset($this->request->post['megjelenit_kiskepek'])) {
            $this->data['megjelenit_kiskepek'] = $this->request->post['megjelenit_kiskepek'];
        } else {
            $this->data['megjelenit_kiskepek'] = $this->config->get('megjelenit_kiskepek');
        }

        if (isset($this->request->post['megjelenit_kiskepek_slider'])) {
            $this->data['megjelenit_kiskepek_slider'] = $this->request->post['megjelenit_kiskepek_slider'];
        } else {
            $this->data['megjelenit_kiskepek_slider'] = $this->config->get('megjelenit_kiskepek_slider');
        }

        if (isset($this->request->post['megjelenit_description'])) {
            $this->data['megjelenit_description'] = $this->request->post['megjelenit_description'];
        } else {
            $this->data['megjelenit_description'] = $this->config->get('megjelenit_description');
        }


        if (isset($this->request->post['megjelenit_ikon_hely'])) {
            $this->data['megjelenit_ikon_hely'] = $this->request->post['megjelenit_ikon_hely'];
        } else {
            $this->data['megjelenit_ikon_hely'] = $this->config->get('megjelenit_ikon_hely');
        }

        if (isset($this->request->post['megjelenit_uj'])) {
            $this->data['megjelenit_uj'] = $this->request->post['megjelenit_uj'];
        } else {
            $this->data['megjelenit_uj'] = $this->config->get('megjelenit_uj');
        }
        if (isset($this->request->post['megjelenit_pardarab'])) {
            $this->data['megjelenit_pardarab'] = $this->request->post['megjelenit_pardarab'];
        } else {
            $this->data['megjelenit_pardarab'] = $this->config->get('megjelenit_pardarab');
        }
        if (isset($this->request->post['megjelenit_szazalek'])) {
            $this->data['megjelenit_szazalek'] = $this->request->post['megjelenit_szazalek'];
        } else {
            $this->data['megjelenit_szazalek'] = $this->config->get('megjelenit_szazalek');
        }
        if (isset($this->request->post['megjelenit_mennyiseg'])) {
            $this->data['megjelenit_mennyiseg'] = $this->request->post['megjelenit_mennyiseg'];
        } else {
            $this->data['megjelenit_mennyiseg'] = $this->config->get('megjelenit_mennyiseg');
        }
        if (isset($this->request->post['megjelenit_csomagolasi_mennyiseg'])) {
            $this->data['megjelenit_csomagolasi_mennyiseg'] = $this->request->post['megjelenit_csomagolasi_mennyiseg'];
        } else {
            $this->data['megjelenit_csomagolasi_mennyiseg'] = $this->config->get('megjelenit_csomagolasi_mennyiseg');
        }
        if (isset($this->request->post['megjelenit_kep_szoveg'])) {
            $this->data['megjelenit_kep_szoveg'] = $this->request->post['megjelenit_kep_szoveg'];
        } else {
            $this->data['megjelenit_kep_szoveg'] = $this->config->get('megjelenit_kep_szoveg');
        }

        if (isset($this->request->post['megjelenit_kivansaglista'])) {
            $this->data['megjelenit_kivansaglista'] = $this->request->post['megjelenit_kivansaglista'];
        } else {
            $this->data['megjelenit_kivansaglista'] = $this->config->get('megjelenit_kivansaglista');
        }
        if (isset($this->request->post['megjelenit_osszehasonlitas'])) {
            $this->data['megjelenit_osszehasonlitas'] = $this->request->post['megjelenit_osszehasonlitas'];
        } else {
            $this->data['megjelenit_osszehasonlitas'] = $this->config->get('megjelenit_osszehasonlitas');
        }

        if (isset($this->request->post['megjelenit_honlap'])) {
            $this->data['megjelenit_honlap'] = $this->request->post['megjelenit_honlap'];
        } else {
            $this->data['megjelenit_honlap'] = $this->config->get('megjelenit_honlap');
        }


        if (isset($this->request->post['megjelenit_mini_cart'])) {
            $this->data['megjelenit_mini_cart'] = $this->request->post['megjelenit_mini_cart'];
        } else {
            $this->data['megjelenit_mini_cart'] = $this->config->get('megjelenit_mini_cart');
        }

        if (isset($this->request->post['megjelenit_vatera'])) {
            $this->data['megjelenit_vatera'] = $this->request->post['megjelenit_vatera'];
        } else {
            $this->data['megjelenit_vatera'] = $this->config->get('megjelenit_vatera');
        }
        if (isset($this->request->post['megjelenit_description_karakterek'])) {
            $this->data['megjelenit_description_karakterek'] = $this->request->post['megjelenit_description_karakterek'];
        } else {
            $this->data['megjelenit_description_karakterek'] = $this->config->get('megjelenit_description_karakterek');
        }

        if (isset($this->request->post['megjelenit_korhinta'])) {
            $this->data['megjelenit_korhinta'] = $this->request->post['megjelenit_korhinta'];
        } else {
            $this->data['megjelenit_korhinta'] = $this->config->get('megjelenit_korhinta');
        }




        if (isset($this->request->post['megjelenit_kosarba_fk'])) {
            $this->data['megjelenit_kosarba_fk'] = $this->request->post['megjelenit_kosarba_fk'];
        } else {
            $this->data['megjelenit_kosarba_fk'] = $this->config->get('megjelenit_kosarba_fk');
        }

        if (isset($this->request->post['megjelenit_minikosar_fk'])) {
            $this->data['megjelenit_minikosar_fk'] = $this->request->post['megjelenit_minikosar_fk'];
        } else {
            $this->data['megjelenit_minikosar_fk'] = $this->config->get('megjelenit_minikosar_fk');
        }

        if (isset($this->request->post['megjelenit_megnezemgomb_fk'])) {
            $this->data['megjelenit_megnezemgomb_fk'] = $this->request->post['megjelenit_megnezemgomb_fk'];
        } else {
            $this->data['megjelenit_megnezemgomb_fk'] = $this->config->get('megjelenit_megnezemgomb_fk');
        }

        if (isset($this->request->post['megjelenit_brutto_ar_fk'])) {
            $this->data['megjelenit_brutto_ar_fk'] = $this->request->post['megjelenit_brutto_ar_fk'];
        } else {
            $this->data['megjelenit_brutto_ar_fk'] = $this->config->get('megjelenit_brutto_ar_fk');
        }

        if (isset($this->request->post['megjelenit_netto_ar_fk'])) {
            $this->data['megjelenit_netto_ar_fk'] = $this->request->post['megjelenit_netto_ar_fk'];
        } else {
            $this->data['megjelenit_netto_ar_fk'] = $this->config->get('megjelenit_netto_ar_fk');
        }

        if (isset($this->request->post['megjelenit_brutto_netto_fk'])) {
            $this->data['megjelenit_brutto_netto_fk'] = $this->request->post['megjelenit_brutto_netto_fk'];
        } else {
            $this->data['megjelenit_brutto_netto_fk'] = $this->config->get('megjelenit_brutto_netto_fk');
        }

        if (isset($this->request->post['megjelenit_description_fk'])) {
            $this->data['megjelenit_description_fk'] = $this->request->post['megjelenit_description_fk'];
        } else {
            $this->data['megjelenit_description_fk'] = $this->config->get('megjelenit_description_fk');
        }

        if (isset($this->request->post['megjelenit_ikon_hely_fk'])) {
            $this->data['megjelenit_ikon_hely_fk'] = $this->request->post['megjelenit_ikon_hely_fk'];
        } else {
            $this->data['megjelenit_ikon_hely_fk'] = $this->config->get('megjelenit_ikon_hely_fk');
        }

        if (isset($this->request->post['megjelenit_uj_fk'])) {
            $this->data['megjelenit_uj_fk'] = $this->request->post['megjelenit_uj_fk'];
        } else {
            $this->data['megjelenit_uj_fk'] = $this->config->get('megjelenit_uj_fk');
        }
        if (isset($this->request->post['megjelenit_pardarab_fk'])) {
            $this->data['megjelenit_pardarab_fk'] = $this->request->post['megjelenit_pardarab_fk'];
        } else {
            $this->data['megjelenit_pardarab_fk'] = $this->config->get('megjelenit_pardarab_fk');
        }
        if (isset($this->request->post['megjelenit_szazalek_fk'])) {
            $this->data['megjelenit_szazalek_fk'] = $this->request->post['megjelenit_szazalek_fk'];
        } else {
            $this->data['megjelenit_szazalek_fk'] = $this->config->get('megjelenit_szazalek_fk');
        }
        if (isset($this->request->post['megjelenit_mennyiseg_fk'])) {
            $this->data['megjelenit_mennyiseg_fk'] = $this->request->post['megjelenit_mennyiseg_fk'];
        } else {
            $this->data['megjelenit_mennyiseg_fk'] = $this->config->get('megjelenit_mennyiseg_fk');
        }
        if (isset($this->request->post['megjelenit_csomagolasi_mennyiseg_fk'])) {
            $this->data['megjelenit_csomagolasi_mennyiseg_fk'] = $this->request->post['megjelenit_csomagolasi_mennyiseg_fk'];
        } else {
            $this->data['megjelenit_csomagolasi_mennyiseg_fk'] = $this->config->get('megjelenit_csomagolasi_mennyiseg_fk');
        }
        if (isset($this->request->post['megjelenit_kep_szoveg_fk'])) {
            $this->data['megjelenit_kep_szoveg_fk'] = $this->request->post['megjelenit_kep_szoveg_fk'];
        } else {
            $this->data['megjelenit_kep_szoveg_fk'] = $this->config->get('megjelenit_kep_szoveg_fk');
        }
        if (isset($this->request->post['megjelenit_kivansaglista_fk'])) {
            $this->data['megjelenit_kivansaglista_fk'] = $this->request->post['megjelenit_kivansaglista_fk'];
        } else {
            $this->data['megjelenit_kivansaglista_fk'] = $this->config->get('megjelenit_kivansaglista_fk');
        }
        if (isset($this->request->post['megjelenit_osszehasonlitas_fk'])) {
            $this->data['megjelenit_osszehasonlitas_fk'] = $this->request->post['megjelenit_osszehasonlitas_fk'];
        } else {
            $this->data['megjelenit_osszehasonlitas_fk'] = $this->config->get('megjelenit_osszehasonlitas_fk');
        }

        if (isset($this->request->post['megjelenit_facebook_fk'])) {
            $this->data['megjelenit_facebook_fk'] = $this->request->post['megjelenit_facebook_fk'];
        } else {
            $this->data['megjelenit_facebook_fk'] = $this->config->get('megjelenit_facebook_fk');
        }

        if (isset($this->request->post['megjelenit_email_fk'])) {
            $this->data['megjelenit_email_fk'] = $this->request->post['megjelenit_email_fk'];
        } else {
            $this->data['megjelenit_email_fk'] = $this->config->get('megjelenit_email_fk');
        }

        if (isset($this->request->post['megjelenit_honlap_fk'])) {
            $this->data['megjelenit_honlap_fk'] = $this->request->post['megjelenit_honlap_fk'];
        } else {
            $this->data['megjelenit_honlap_fk'] = $this->config->get('megjelenit_honlap_fk');
        }

        if (isset($this->request->post['megjelenit_mini_cart_fk'])) {
            $this->data['megjelenit_mini_cart_fk'] = $this->request->post['megjelenit_mini_cart_fk'];
        } else {
            $this->data['megjelenit_mini_cart_fk'] = $this->config->get('megjelenit_mini_cart_fk');
        }

        if (isset($this->request->post['megjelenit_vatera_fk'])) {
            $this->data['megjelenit_vatera_fk'] = $this->request->post['megjelenit_vatera_fk'];
        } else {
            $this->data['megjelenit_vatera_fk'] = $this->config->get('megjelenit_vatera_fk');
        }
        if (isset($this->request->post['megjelenit_description_karakterek_fk'])) {
            $this->data['megjelenit_description_karakterek_fk'] = $this->request->post['megjelenit_description_karakterek_fk'];
        } else {
            $this->data['megjelenit_description_karakterek_fk'] = $this->config->get('megjelenit_description_karakterek_fk');
        }

        if (isset($this->request->post['megjelenit_korhinta_fk'])) {
            $this->data['megjelenit_korhinta_fk'] = $this->request->post['megjelenit_korhinta_fk'];
        } else {
            $this->data['megjelenit_korhinta_fk'] = $this->config->get('megjelenit_korhinta_fk');
        }





        if (isset($this->request->post['megjelenit_lista_modularis'])) {
            $this->data['megjelenit_lista_modularis'] = $this->request->post['megjelenit_lista_modularis'];
        } else {
            $this->data['megjelenit_lista_modularis'] = $this->config->get('megjelenit_lista_modularis') ;
        }

        if (isset($this->request->post['megjelenit_lista_kosarba'])) {
            $this->data['megjelenit_lista_kosarba'] = $this->request->post['megjelenit_lista_kosarba'];
        } else {
            $this->data['megjelenit_lista_kosarba'] = $this->config->get('megjelenit_lista_kosarba');
        }

        if (isset($this->request->post['megjelenit_lista_brutto_ar'])) {
            $this->data['megjelenit_lista_brutto_ar'] = $this->request->post['megjelenit_lista_brutto_ar'];
        } else {
            $this->data['megjelenit_lista_brutto_ar'] = $this->config->get('megjelenit_lista_brutto_ar');
        }

        if (isset($this->request->post['megjelenit_lista_netto_ar'])) {
            $this->data['megjelenit_lista_netto_ar'] = $this->request->post['megjelenit_lista_netto_ar'];
        } else {
            $this->data['megjelenit_lista_netto_ar'] = $this->config->get('megjelenit_lista_netto_ar');
        }

        if (isset($this->request->post['megjelenit_lista_brutto_netto'])) {
            $this->data['megjelenit_lista_brutto_netto'] = $this->request->post['megjelenit_lista_brutto_netto'];
        } else {
            $this->data['megjelenit_lista_brutto_netto'] = $this->config->get('megjelenit_lista_brutto_netto');
        }

        if (isset($this->request->post['megjelenit_lista_description'])) {
            $this->data['megjelenit_lista_description'] = $this->request->post['megjelenit_lista_description'];
        } else {
            $this->data['megjelenit_lista_description'] = $this->config->get('megjelenit_lista_description');
        }

        if (isset($this->request->post['megjelenit_description_lista_karakterek'])) {
            $this->data['megjelenit_description_lista_karakterek'] = $this->request->post['megjelenit_description_lista_karakterek'];
        } else {
            $this->data['megjelenit_description_lista_karakterek'] = $this->config->get('megjelenit_description_lista_karakterek');
        }

        if (isset($this->request->post['megjelenit_lista_uj'])) {
            $this->data['megjelenit_lista_uj'] = $this->request->post['megjelenit_lista_uj'];
        } else {
            $this->data['megjelenit_lista_uj'] = $this->config->get('megjelenit_lista_uj');
        }
        if (isset($this->request->post['megjelenit_lista_pardarab'])) {
            $this->data['megjelenit_lista_pardarab'] = $this->request->post['megjelenit_lista_pardarab'];
        } else {
            $this->data['megjelenit_lista_pardarab'] = $this->config->get('megjelenit_lista_pardarab');
        }
        if (isset($this->request->post['megjelenit_lista_szazalek'])) {
            $this->data['megjelenit_lista_szazalek'] = $this->request->post['megjelenit_lista_szazalek'];
        } else {
            $this->data['megjelenit_lista_szazalek'] = $this->config->get('megjelenit_lista_szazalek');
        }
        if (isset($this->request->post['megjelenit_lista_mennyiseg'])) {
            $this->data['megjelenit_lista_mennyiseg'] = $this->request->post['megjelenit_lista_mennyiseg'];
        } else {
            $this->data['megjelenit_lista_mennyiseg'] = $this->config->get('megjelenit_lista_mennyiseg');
        }
        if (isset($this->request->post['megjelenit_lista_csomagolasi_mennyiseg'])) {
            $this->data['megjelenit_lista_csomagolasi_mennyiseg'] = $this->request->post['megjelenit_lista_csomagolasi_mennyiseg'];
        } else {
            $this->data['megjelenit_lista_csomagolasi_mennyiseg'] = $this->config->get('megjelenit_lista_csomagolasi_mennyiseg');
        }
        if (isset($this->request->post['megjelenit_lista_kivansaglista'])) {
            $this->data['megjelenit_lista_kivansaglista'] = $this->request->post['megjelenit_lista_kivansaglista'];
        } else {
            $this->data['megjelenit_lista_kivansaglista'] = $this->config->get('megjelenit_lista_kivansaglista');
        }
        if (isset($this->request->post['megjelenit_lista_osszehasonlitas'])) {
            $this->data['megjelenit_lista_osszehasonlitas'] = $this->request->post['megjelenit_lista_osszehasonlitas'];
        } else {
            $this->data['megjelenit_lista_osszehasonlitas'] = $this->config->get('megjelenit_lista_osszehasonlitas');
        }

        if (isset($this->request->post['megjelenit_lista'])) {
            $admin = $this->request->post['megjelenit_lista'];
        } else {
            $admin = $this->config->get("megjelenit_lista");
        }

        $megjelenit_lista = array(
            'mennyisegi_ikon'   => isset($admin['mennyisegi_ikon']) ? $admin['mennyisegi_ikon'] : 0
        );
        $this->data['mennyisegi_ikon'] = $megjelenit_lista;




        if (isset($this->request->post['megjelenit_slideshow'])) {
            $this->data['megjelenit_slideshow'] = $this->request->post['megjelenit_slideshow'];
        } else {
            $this->data['megjelenit_slideshow'] = $this->config->get('megjelenit_slideshow');
        }
        if (isset($this->request->post['megjelenit_csomagolas_admin'])) {
            $this->data['megjelenit_csomagolas_admin'] = $this->request->post['megjelenit_csomagolas_admin'];
        } else {
            $this->data['megjelenit_csomagolas_admin'] = $this->config->get('megjelenit_csomagolas_admin');
        }
        if (isset($this->request->post['megjelenit_stock_date'])) {
            $this->data['megjelenit_stock_date'] = $this->request->post['megjelenit_stock_date'];
        } else {
            $this->data['megjelenit_stock_date'] = $this->config->get('megjelenit_stock_date');
        }

        if (isset($this->request->post['megjelenit_jobb_egerbolt'])) {
            $this->data['megjelenit_jobb_egerbolt'] = $this->request->post['megjelenit_jobb_egerbolt'];
        } else {
            $this->data['megjelenit_jobb_egerbolt'] = $this->config->get('megjelenit_jobb_egerbolt');
        }
        if (isset($this->request->post['megjelenit_masolhato'])) {
            $this->data['megjelenit_masolhato'] = $this->request->post['megjelenit_masolhato'];
        } else {
            $this->data['megjelenit_masolhato'] = $this->config->get('megjelenit_masolhato');
        }

        if (isset($this->request->post['megjelenit_piacter'])) {
            $this->data['megjelenit_piacter'] = $this->request->post['megjelenit_piacter'];
        } else {
            $this->data['megjelenit_piacter'] = $this->config->get('megjelenit_piacter');
        }



        if (isset($this->request->post['megjelenit_product_jelentkezzen'])) {
            $this->data['megjelenit_product_jelentkezzen'] = $this->request->post['megjelenit_product_jelentkezzen'];
        } else {
            $this->data['megjelenit_product_jelentkezzen'] = $this->config->get('megjelenit_product_jelentkezzen');
        }

        if (isset($this->request->post['megjelenit_product_elerhetoseg'])) {
            $this->data['megjelenit_product_elerhetoseg'] = $this->request->post['megjelenit_product_elerhetoseg'];
        } else {
            $this->data['megjelenit_product_elerhetoseg'] = $this->config->get('megjelenit_product_elerhetoseg');
        }
        if (isset($this->request->post['megjelenit_product_suly'])) {
            $this->data['megjelenit_product_suly'] = $this->request->post['megjelenit_product_suly'];
        } else {
            $this->data['megjelenit_product_suly'] = $this->config->get('megjelenit_product_suly');
        }
        if (isset($this->request->post['megjelenit_product_mennyisegi_egyseg'])) {
            $this->data['megjelenit_product_mennyisegi_egyseg'] = $this->request->post['megjelenit_product_mennyisegi_egyseg'];
        } else {
            $this->data['megjelenit_product_mennyisegi_egyseg'] = $this->config->get('megjelenit_product_mennyisegi_egyseg');
        }

        if (isset($this->request->post['megjelenit_product_cikkszam'])) {
            $this->data['megjelenit_product_cikkszam'] = $this->request->post['megjelenit_product_cikkszam'];
        } else {
            $this->data['megjelenit_product_cikkszam'] = $this->config->get('megjelenit_product_cikkszam');
        }
        if (isset($this->request->post['megjelenit_product_cikkszam_sorrend'])) {
            $this->data['megjelenit_product_cikkszam_sorrend'] = $this->request->post['megjelenit_product_cikkszam_sorrend'];
        } else {
            $this->data['megjelenit_product_cikkszam_sorrend'] = $this->config->get('megjelenit_product_cikkszam_sorrend');
        }

        if (isset($this->request->post['megjelenit_product_cikkszam2'])) {
            $this->data['megjelenit_product_cikkszam2'] = $this->request->post['megjelenit_product_cikkszam2'];
        } else {
            $this->data['megjelenit_product_cikkszam2'] = $this->config->get('megjelenit_product_cikkszam2');
        }
        if (isset($this->request->post['megjelenit_product_cikkszam2_sorrend'])) {
            $this->data['megjelenit_product_cikkszam2_sorrend'] = $this->request->post['megjelenit_product_cikkszam2_sorrend'];
        } else {
            $this->data['megjelenit_product_cikkszam2_sorrend'] = $this->config->get('megjelenit_product_cikkszam2_sorrend');
        }

        if (isset($this->request->post['megjelenit_product_model'])) {
            $this->data['megjelenit_product_model'] = $this->request->post['megjelenit_product_model'];
        } else {
            $this->data['megjelenit_product_model'] = $this->config->get('megjelenit_product_model');
        }
        if (isset($this->request->post['megjelenit_product_gyarto'])) {
            $this->data['megjelenit_product_gyarto'] = $this->request->post['megjelenit_product_gyarto'];
        } else {
            $this->data['megjelenit_product_gyarto'] = $this->config->get('megjelenit_product_gyarto');
        }
        if (isset($this->request->post['megjelenit_product_reward'])) {
            $this->data['megjelenit_product_reward'] = $this->request->post['megjelenit_product_reward'];
        } else {
            $this->data['megjelenit_product_reward'] = $this->config->get('megjelenit_product_reward');
        }
        if (isset($this->request->post['megjelenit_product_sku'])) {
            $this->data['megjelenit_product_sku'] = $this->request->post['megjelenit_product_sku'];
        } else {
            $this->data['megjelenit_product_sku'] = $this->config->get('megjelenit_product_sku');
        }
        if (isset($this->request->post['megjelenit_product_meret'])) {
            $this->data['megjelenit_product_meret'] = $this->request->post['megjelenit_product_meret'];
        } else {
            $this->data['megjelenit_product_meret'] = $this->config->get('megjelenit_product_meret');
        }


        if (isset($this->request->post['megjelenit_product_elerhetoseg_sorrend'])) {
            $this->data['megjelenit_product_elerhetoseg_sorrend'] = $this->request->post['megjelenit_product_elerhetoseg_sorrend'];
        } else {
            $this->data['megjelenit_product_elerhetoseg_sorrend'] = $this->config->get('megjelenit_product_elerhetoseg_sorrend');
        }
        if (isset($this->request->post['megjelenit_product_suly_sorrend'])) {
            $this->data['megjelenit_product_suly_sorrend'] = $this->request->post['megjelenit_product_suly_sorrend'];
        } else {
            $this->data['megjelenit_product_suly_sorrend'] = $this->config->get('megjelenit_product_suly_sorrend');
        }
        if (isset($this->request->post['megjelenit_product_mennyisegi_egyseg_sorrend'])) {
            $this->data['megjelenit_product_mennyisegi_egyseg_sorrend'] = $this->request->post['megjelenit_product_mennyisegi_egyseg_sorrend'];
        } else {
            $this->data['megjelenit_product_mennyisegi_egyseg_sorrend'] = $this->config->get('megjelenit_product_mennyisegi_egyseg_sorrend');
        }




        if (isset($this->request->post['megjelenit_list_admin_model'])) {
            $this->data['megjelenit_list_admin_model'] = $this->request->post['megjelenit_list_admin_model'];
        } else {
            $this->data['megjelenit_list_admin_model'] = $this->config->get('megjelenit_list_admin_model');
        }

        if (isset($this->request->post['megjelenit_list_admin_cikkszam'])) {
            $this->data['megjelenit_list_admin_cikkszam'] = $this->request->post['megjelenit_list_admin_cikkszam'];
        } else {
            $this->data['megjelenit_list_admin_cikkszam'] = $this->config->get('megjelenit_list_admin_cikkszam');
        }

        if (isset($this->request->post['megjelenit_list_admin_cikkszam2'])) {
            $this->data['megjelenit_list_admin_cikkszam2'] = $this->request->post['megjelenit_list_admin_cikkszam2'];
        } else {
            $this->data['megjelenit_list_admin_cikkszam2'] = $this->config->get('megjelenit_list_admin_cikkszam2');
        }

        if (isset($this->request->post['megjelenit_list_admin_modositas'])) {
            $this->data['megjelenit_list_admin_modositas'] = $this->request->post['megjelenit_list_admin_modositas'];
        } else {
            $this->data['megjelenit_list_admin_modositas'] = $this->config->get('megjelenit_list_admin_modositas');
        }

        if (isset($this->request->post['megjelenit_list_admin_hozzaadas'])) {
            $this->data['megjelenit_list_admin_hozzaadas'] = $this->request->post['megjelenit_list_admin_hozzaadas'];
        } else {
            $this->data['megjelenit_list_admin_hozzaadas'] = $this->config->get('megjelenit_list_admin_hozzaadas');
        }


        if (isset($this->request->post['megjelenit_list_admin_kifuto'])) {
            $this->data['megjelenit_list_admin_kifuto'] = $this->request->post['megjelenit_list_admin_kifuto'];
        } else {
            $this->data['megjelenit_list_admin_kifuto'] = $this->config->get('megjelenit_list_admin_kifuto');
        }
        if (isset($this->request->post['megjelenit_list_admin_ujdonsag'])) {
            $this->data['megjelenit_list_admin_ujdonsag'] = $this->request->post['megjelenit_list_admin_ujdonsag'];
        } else {
            $this->data['megjelenit_list_admin_ujdonsag'] = $this->config->get('megjelenit_list_admin_ujdonsag');
        }

        if (isset($this->request->post['megjelenit_list_admin_extra_garancia'])) {
            $this->data['megjelenit_list_admin_extra_garancia'] = $this->request->post['megjelenit_list_admin_extra_garancia'];
        } else {
            $this->data['megjelenit_list_admin_extra_garancia'] = $this->config->get('megjelenit_list_admin_extra_garancia');
        }


        if (isset($this->request->post['megjelenit_list_admin_elorendeles'])) {
            $this->data['megjelenit_list_admin_elorendeles'] = $this->request->post['megjelenit_list_admin_elorendeles'];
        } else {
            $this->data['megjelenit_list_admin_elorendeles'] = $this->config->get('megjelenit_list_admin_elorendeles');
        }

        if (isset($this->request->post['megjelenit_list_admin_ar_tiltasa'])) {
            $this->data['megjelenit_list_admin_ar_tiltasa'] = $this->request->post['megjelenit_list_admin_ar_tiltasa'];
        } else {
            $this->data['megjelenit_list_admin_ar_tiltasa'] = $this->config->get('megjelenit_list_admin_ar_tiltasa');
        }

        if (isset($this->request->post['megjelenit_list_admin_megrendelem'])) {
            $this->data['megjelenit_list_admin_megrendelem'] = $this->request->post['megjelenit_list_admin_megrendelem'];
        } else {
            $this->data['megjelenit_list_admin_megrendelem'] = $this->config->get('megjelenit_list_admin_megrendelem');
        }

        if (isset($this->request->post['megjelenit_product_cikkszam_sorrend'])) {
            $this->data['megjelenit_product_cikkszam_sorrend'] = $this->request->post['megjelenit_product_cikkszam_sorrend'];
        } else {
            $this->data['megjelenit_product_cikkszam_sorrend'] = $this->config->get('megjelenit_product_cikkszam_sorrend');
        }

        if (isset($this->request->post['megjelenit_product_cikkszam2_sorrend'])) {
            $this->data['megjelenit_product_cikkszam2_sorrend'] = $this->request->post['megjelenit_product_cikkszam2_sorrend'];
        } else {
            $this->data['megjelenit_product_cikkszam2_sorrend'] = $this->config->get('megjelenit_product_cikkszam2_sorrend');
        }

        if (isset($this->request->post['megjelenit_product_model_sorrend'])) {
            $this->data['megjelenit_product_model_sorrend'] = $this->request->post['megjelenit_product_model_sorrend'];
        } else {
            $this->data['megjelenit_product_model_sorrend'] = $this->config->get('megjelenit_product_model_sorrend');
        }
        if (isset($this->request->post['megjelenit_product_gyarto_sorrend'])) {
            $this->data['megjelenit_product_gyarto_sorrend'] = $this->request->post['megjelenit_product_gyarto_sorrend'];
        } else {
            $this->data['megjelenit_product_gyarto_sorrend'] = $this->config->get('megjelenit_product_gyarto_sorrend');
        }
        if (isset($this->request->post['megjelenit_product_reward_sorrend'])) {
            $this->data['megjelenit_product_reward_sorrend'] = $this->request->post['megjelenit_product_reward_sorrend'];
        } else {
            $this->data['megjelenit_product_reward_sorrend'] = $this->config->get('megjelenit_product_reward_sorrend');
        }

        if (isset($this->request->post['megjelenit_product_sku_sorrend'])) {
            $this->data['megjelenit_product_sku_sorrend'] = $this->request->post['megjelenit_product_sku_sorrend'];
        } else {
            $this->data['megjelenit_product_sku_sorrend'] = $this->config->get('megjelenit_product_sku_sorrend');
        }

        if (isset($this->request->post['megjelenit_product_meret_sorrend'])) {
            $this->data['megjelenit_product_meret_sorrend'] = $this->request->post['megjelenit_product_meret_sorrend'];
        } else {
            $this->data['megjelenit_product_meret_sorrend'] = $this->config->get('megjelenit_product_meret_sorrend');
        }

        if (isset($this->request->post['megjelenit_product_csomagolasi_mennyiseg'])) {
            $this->data['megjelenit_product_csomagolasi_mennyiseg'] = $this->request->post['megjelenit_product_csomagolasi_mennyiseg'];
        } else {
            $this->data['megjelenit_product_csomagolasi_mennyiseg'] = $this->config->get('megjelenit_product_csomagolasi_mennyiseg');
        }

        if (isset($this->request->post['megjelenit_product_brutto_ar'])) {
            $this->data['megjelenit_product_brutto_ar'] = $this->request->post['megjelenit_product_brutto_ar'];
        } else {
            $this->data['megjelenit_product_brutto_ar'] = $this->config->get('megjelenit_product_brutto_ar');
        }

        if (isset($this->request->post['megjelenit_product_netto_ar'])) {
            $this->data['megjelenit_product_netto_ar'] = $this->request->post['megjelenit_product_netto_ar'];
        } else {
            $this->data['megjelenit_product_netto_ar'] = $this->config->get('megjelenit_product_netto_ar');
        }

        if (isset($this->request->post['megjelenit_product_brutto_netto'])) {
            $this->data['megjelenit_product_brutto_netto'] = $this->request->post['megjelenit_product_brutto_netto'];
        } else {
            $this->data['megjelenit_product_brutto_netto'] = $this->config->get('megjelenit_product_brutto_netto');
        }


        if (isset($this->request->post['megjelenit_minicart_kosar'])) {
            $this->data['megjelenit_minicart_kosar'] = $this->request->post['megjelenit_minicart_kosar'];
        } else {
            $this->data['megjelenit_minicart_kosar'] = $this->config->get('megjelenit_minicart_kosar');
        }

        if (isset($this->request->post['megjelenit_kosar_kupon'])) {
            $this->data['megjelenit_kosar_kupon'] = $this->request->post['megjelenit_kosar_kupon'];
        } else {
            $this->data['megjelenit_kosar_kupon'] = $this->config->get('megjelenit_kosar_kupon');
        }
        if (isset($this->request->post['megjelenit_kosar_suly'])) {
            $this->data['megjelenit_kosar_suly'] = $this->request->post['megjelenit_kosar_suly'];
        } else {
            $this->data['megjelenit_kosar_suly'] = $this->config->get('megjelenit_kosar_suly');
        }
        if (isset($this->request->post['megjelenit_penztar_fizetesi_mod'])) {
            $this->data['megjelenit_penztar_fizetesi_mod'] = $this->request->post['megjelenit_penztar_fizetesi_mod'];
        } else {
            $this->data['megjelenit_penztar_fizetesi_mod'] = $this->config->get('megjelenit_penztar_fizetesi_mod');
        }

        if (isset($this->request->post['megjelenit_vizjel'])) {
            $this->data['megjelenit_vizjel'] = $this->request->post['megjelenit_vizjel'];
        } else {
            $this->data['megjelenit_vizjel'] = $this->config->get('megjelenit_vizjel');
        }
        if (isset($this->request->post['megjelenit_admin_nyelvi_ellenorzes'])) {
            $this->data['megjelenit_admin_nyelvi_ellenorzes'] = $this->request->post['megjelenit_admin_nyelvi_ellenorzes'];
        } else {
            $this->data['megjelenit_admin_nyelvi_ellenorzes'] = $this->config->get('megjelenit_admin_nyelvi_ellenorzes');
        }

        if (isset($this->request->post['megjelenit_admin_ar_ellenorzes'])) {
            $this->data['megjelenit_admin_ar_ellenorzes'] = $this->request->post['megjelenit_admin_ar_ellenorzes'];
        } else {
            $this->data['megjelenit_admin_ar_ellenorzes'] = $this->config->get('megjelenit_admin_ar_ellenorzes');
        }

        if (isset($this->request->post['megjelenit_admin_ketszintu_szuro_ellenorzes'])) {
            $this->data['megjelenit_admin_ketszintu_szuro_ellenorzes'] = $this->request->post['megjelenit_admin_ketszintu_szuro_ellenorzes'];
        } else {
            $this->data['megjelenit_admin_ketszintu_szuro_ellenorzes'] = $this->config->get('megjelenit_admin_ketszintu_szuro_ellenorzes');
        }

        if (isset($this->request->post['megjelenit_admin_ervenyes_ig_ellenorzes'])) {
            $this->data['megjelenit_admin_ervenyes_ig_ellenorzes'] = $this->request->post['megjelenit_admin_ervenyes_ig_ellenorzes'];
        } else {
            $this->data['megjelenit_admin_ervenyes_ig_ellenorzes'] = $this->config->get('megjelenit_admin_ervenyes_ig_ellenorzes');
        }

        if (isset($this->request->post['megjelenit_category_description'])) {
            $this->data['megjelenit_category_description'] = $this->request->post['megjelenit_category_description'];
        } else {
            $this->data['megjelenit_category_description'] = $this->config->get('megjelenit_category_description');
        }

        if (isset($this->request->post['megjelenit_category_header_image'])) {
            $this->data['megjelenit_category_header_image'] = $this->request->post['megjelenit_category_header_image'];
        } else {
            $this->data['megjelenit_category_header_image'] = $this->config->get('megjelenit_category_header_image');
        }

        if (isset($this->request->post['megjelenit_category_header_image_above'])) {
            $this->data['megjelenit_category_header_image_above'] = $this->request->post['megjelenit_category_header_image_above'];
        } else {
            $this->data['megjelenit_category_header_image_above'] = $this->config->get('megjelenit_category_header_image_above');
        }

        if (isset($this->request->post['megjelenit_category_header_null'])) {
            $this->data['megjelenit_category_header_null'] = $this->request->post['megjelenit_category_header_null'];
        } else {
            $this->data['megjelenit_category_header_null'] = $this->config->get('megjelenit_category_header_null');
        }

        if (isset($this->request->post['megjelenit_sap'])) {
            $this->data['megjelenit_sap'] = $this->request->post['megjelenit_sap'];
        } else {
            $this->data['megjelenit_sap'] = $this->config->get('megjelenit_sap');
        }

        if (isset($this->request->post['megjelenit_gls'])) {
            $this->data['megjelenit_gls'] = $this->request->post['megjelenit_gls'];
        } else {
            $this->data['megjelenit_gls'] = $this->config->get('megjelenit_gls');
        }



        if (isset($this->request->post['engedelyez_integracio_argep'])) {
            $this->data['engedelyez_integracio_argep'] = $this->request->post['engedelyez_integracio_argep'];
        } else {
            $this->data['engedelyez_integracio_argep'] = $this->config->get('engedelyez_integracio_argep');
        }
        if (isset($this->request->post['engedelyez_integracio_olcso'])) {
            $this->data['engedelyez_integracio_olcso'] = $this->request->post['engedelyez_integracio_olcso'];
        } else {
            $this->data['engedelyez_integracio_olcso'] = $this->config->get('engedelyez_integracio_olcso');
        }
        if (isset($this->request->post['engedelyez_integracio_kirakat'])) {
            $this->data['engedelyez_integracio_kirakat'] = $this->request->post['engedelyez_integracio_kirakat'];
        } else {
            $this->data['engedelyez_integracio_kirakat'] = $this->config->get('engedelyez_integracio_kirakat');
        }
        if (isset($this->request->post['engedelyez_integracio_joaron'])) {
            $this->data['engedelyez_integracio_joaron'] = $this->request->post['engedelyez_integracio_joaron'];
        } else {
            $this->data['engedelyez_integracio_joaron'] = $this->config->get('engedelyez_integracio_joaron');
        }
        if (isset($this->request->post['engedelyez_integracio_arukereso'])) {
            $this->data['engedelyez_integracio_arukereso'] = $this->request->post['engedelyez_integracio_arukereso'];
        } else {
            $this->data['engedelyez_integracio_arukereso'] = $this->config->get('engedelyez_integracio_arukereso');
        }
        if (isset($this->request->post['engedelyez_integracio_arkozpont'])) {
            $this->data['engedelyez_integracio_arkozpont'] = $this->request->post['engedelyez_integracio_arkozpont'];
        } else {
            $this->data['engedelyez_integracio_arkozpont'] = $this->config->get('engedelyez_integracio_arkozpont');
        }
        if (isset($this->request->post['engedelyez_integracio_osszehasonlitom'])) {
            $this->data['engedelyez_integracio_osszehasonlitom'] = $this->request->post['engedelyez_integracio_osszehasonlitom'];
        } else {
            $this->data['engedelyez_integracio_osszehasonlitom'] = $this->config->get('engedelyez_integracio_osszehasonlitom');
        }
        if (isset($this->request->post['engedelyez_integracio_olcsobbat'])) {
            $this->data['engedelyez_integracio_olcsobbat'] = $this->request->post['engedelyez_integracio_olcsobbat'];
        } else {
            $this->data['engedelyez_integracio_olcsobbat'] = $this->config->get('engedelyez_integracio_olcsobbat');
        }




        if (isset($this->request->post['megjelenit_kosar_kivansag'])) {
            $this->data['megjelenit_kosar_kivansag'] = $this->request->post['megjelenit_kosar_kivansag'];
        } else {
            $this->data['megjelenit_kosar_kivansag'] = $this->config->get('megjelenit_kosar_kivansag');
        }
        if (isset($this->request->post['megjelenit_product_jobb_jobb_elso'])) {
            $this->data['megjelenit_product_jobb_jobb_elso'] = $this->request->post['megjelenit_product_jobb_jobb_elso'];
        } else {
            $this->data['megjelenit_product_jobb_jobb_elso'] = $this->config->get('megjelenit_product_jobb_jobb_elso');
        }
        if (isset($this->request->post['megjelenit_product_jobb_jobb_masodik'])) {
            $this->data['megjelenit_product_jobb_jobb_masodik'] = $this->request->post['megjelenit_product_jobb_jobb_masodik'];
        } else {
            $this->data['megjelenit_product_jobb_jobb_masodik'] = $this->config->get('megjelenit_product_jobb_jobb_masodik');
        }
        if (isset($this->request->post['megjelenit_product_jobb_jobb_harmadik'])) {
            $this->data['megjelenit_product_jobb_jobb_harmadik'] = $this->request->post['megjelenit_product_jobb_jobb_harmadik'];
        } else {
            $this->data['megjelenit_product_jobb_jobb_harmadik'] = $this->config->get('megjelenit_product_jobb_jobb_harmadik');
        }

        if (isset($this->request->post['megjelenit_product_nev_fejlec'])) {
            $this->data['megjelenit_product_nev_fejlec'] = $this->request->post['megjelenit_product_nev_fejlec'];
        } else {
            $this->data['megjelenit_product_nev_fejlec'] = $this->config->get('megjelenit_product_nev_fejlec');
        }
        if (isset($this->request->post['megjelenit_product_megnevezes'])) {
            $this->data['megjelenit_product_megnevezes'] = $this->request->post['megjelenit_product_megnevezes'];
        } else {
            $this->data['megjelenit_product_megnevezes'] = $this->config->get('megjelenit_product_megnevezes');
        }

        if (isset($this->request->post['megjelenit_product_kaphato'])) {
            $this->data['megjelenit_product_kaphato'] = $this->request->post['megjelenit_product_kaphato'];
        } else {
            $this->data['megjelenit_product_kaphato'] = $this->config->get('megjelenit_product_kaphato');
        }
        if (isset($this->request->post['megjelenit_product_filter_gropup'])) {
            $this->data['megjelenit_product_filter_gropup'] = $this->request->post['megjelenit_product_filter_gropup'];
        } else {
            $this->data['megjelenit_product_filter_gropup'] = $this->config->get('megjelenit_product_filter_gropup');
        }
        if (isset($this->request->post['megjelenit_product_filter'])) {
            $this->data['megjelenit_product_filter'] = $this->request->post['megjelenit_product_filter'];
        } else {
            $this->data['megjelenit_product_filter'] = $this->config->get('megjelenit_product_filter');
        }

        if (isset($this->request->post['megjelenit_product_kaphato_sorrend'])) {
            $this->data['megjelenit_product_kaphato_sorrend'] = $this->request->post['megjelenit_product_kaphato_sorrend'];
        } else {
            $this->data['megjelenit_product_kaphato_sorrend'] = $this->config->get('megjelenit_product_kaphato_sorrend');
        }
        if (isset($this->request->post['megjelenit_product_filter_gropup_sorrend'])) {
            $this->data['megjelenit_product_filter_gropup_sorrend'] = $this->request->post['megjelenit_product_filter_gropup_sorrend'];
        } else {
            $this->data['megjelenit_product_filter_gropup_sorrend'] = $this->config->get('megjelenit_product_filter_gropup_sorrend');
        }
        if (isset($this->request->post['megjelenit_product_filter_sorrend'])) {
            $this->data['megjelenit_product_filter_sorrend'] = $this->request->post['megjelenit_product_filter_sorrend'];
        } else {
            $this->data['megjelenit_product_filter_sorrend'] = $this->config->get('megjelenit_product_filter_sorrend');
        }



        if (isset($this->request->post['megjelenit_product_gropup_filter'])) {
            $this->data['megjelenit_product_gropup_filter'] = $this->request->post['megjelenit_product_gropup_filter'];
        } else {
            $this->data['megjelenit_product_gropup_filter'] = $this->config->get('megjelenit_product_gropup_filter');
        }
        if (isset($this->request->post['megjelenit_product_gropup_filter_sorrend'])) {
            $this->data['megjelenit_product_gropup_filter_sorrend'] = $this->request->post['megjelenit_product_gropup_filter_sorrend'];
        } else {
            $this->data['megjelenit_product_gropup_filter_sorrend'] = $this->config->get('megjelenit_product_gropup_filter_sorrend');
        }


        if (isset($this->request->post['megjelenit_product_quantity'])) {
            $this->data['megjelenit_product_quantity'] = $this->request->post['megjelenit_product_quantity'];
        } else {
            $this->data['megjelenit_product_quantity'] = $this->config->get('megjelenit_product_quantity');
        }
        if (isset($this->request->post['megjelenit_product_social'])) {
            $this->data['megjelenit_product_social'] = $this->request->post['megjelenit_product_social'];
        } else {
            $this->data['megjelenit_product_social'] = $this->config->get('megjelenit_product_social');
        }
        if (isset($this->request->post['megjelenit_product_leiras'])) {
            $this->data['megjelenit_product_leiras'] = $this->request->post['megjelenit_product_leiras'];
        } else {
            $this->data['megjelenit_product_leiras'] = $this->config->get('megjelenit_product_leiras');
        }

        if (isset($this->request->post['megjelenit_product_kapcsolodo'])) {
            $this->data['megjelenit_product_kapcsolodo'] = $this->request->post['megjelenit_product_kapcsolodo'];
        } else {
            $this->data['megjelenit_product_kapcsolodo'] = $this->config->get('megjelenit_product_kapcsolodo');
        }

        $vezeteknev = $this->config->get('megjelenit_regisztracio_vezeteknev');

        if (isset($this->request->post['megjelenit_regisztracio_vezeteknev'])) {
            $this->data['megjelenit_regisztracio_vezeteknev'] = $this->request->post['megjelenit_regisztracio_vezeteknev'];
        } elseif ( !is_null($vezeteknev  ) ) {
            $this->data['megjelenit_regisztracio_vezeteknev'] = $this->config->get('megjelenit_regisztracio_vezeteknev');
        } else {
            $this->data['megjelenit_regisztracio_vezeteknev'] = 1;
        }

        if (isset($this->request->post['megjelenit_regisztracio_keresztnev'])) {
            $this->data['megjelenit_regisztracio_keresztnev'] = $this->request->post['megjelenit_regisztracio_keresztnev'];
        } else {
            $this->data['megjelenit_regisztracio_keresztnev'] = $this->config->get('megjelenit_regisztracio_keresztnev');
        }
        if (isset($this->request->post['megjelenit_regisztracio_utca'])) {
            $this->data['megjelenit_regisztracio_utca'] = $this->request->post['megjelenit_regisztracio_utca'];
        } else {
            $this->data['megjelenit_regisztracio_utca'] = $this->config->get('megjelenit_regisztracio_utca');
        }
        if (isset($this->request->post['megjelenit_regisztracio_varos'])) {
            $this->data['megjelenit_regisztracio_varos'] = $this->request->post['megjelenit_regisztracio_varos'];
        } else {
            $this->data['megjelenit_regisztracio_varos'] = $this->config->get('megjelenit_regisztracio_varos');
        }
        if (isset($this->request->post['megjelenit_regisztracio_iranyitoszam'])) {
            $this->data['megjelenit_regisztracio_iranyitoszam'] = $this->request->post['megjelenit_regisztracio_iranyitoszam'];
        } else {
            $this->data['megjelenit_regisztracio_iranyitoszam'] = $this->config->get('megjelenit_regisztracio_iranyitoszam');
        }
        if (isset($this->request->post['megjelenit_regisztracio_orszag'])) {
            $this->data['megjelenit_regisztracio_orszag'] = $this->request->post['megjelenit_regisztracio_orszag'];
        } else {
            $this->data['megjelenit_regisztracio_orszag'] = $this->config->get('megjelenit_regisztracio_orszag');
        }
        if (isset($this->request->post['megjelenit_regisztracio_megye'])) {
            $this->data['megjelenit_regisztracio_megye'] = $this->request->post['megjelenit_regisztracio_megye'];
        } else {
            $this->data['megjelenit_regisztracio_megye'] = $this->config->get('megjelenit_regisztracio_megye');
        }
        if (isset($this->request->post['megjelenit_regisztracio_iskolai_vegzettseg'])) {
            $this->data['megjelenit_regisztracio_iskolai_vegzettseg'] = $this->request->post['megjelenit_regisztracio_iskolai_vegzettseg'];
        } else {
            $this->data['megjelenit_regisztracio_iskolai_vegzettseg'] = $this->config->get('megjelenit_regisztracio_iskolai_vegzettseg');
        }
        if (isset($this->request->post['megjelenit_regisztracio_weblap'])) {
            $this->data['megjelenit_regisztracio_weblap'] = $this->request->post['megjelenit_regisztracio_weblap'];
        } else {
            $this->data['megjelenit_regisztracio_weblap'] = $this->config->get('megjelenit_regisztracio_weblap');
        }
        if (isset($this->request->post['megjelenit_regisztracio_cegnev'])) {
            $this->data['megjelenit_regisztracio_cegnev'] = $this->request->post['megjelenit_regisztracio_cegnev'];
        } else {
            $this->data['megjelenit_regisztracio_cegnev'] = $this->config->get('megjelenit_regisztracio_cegnev');
        }
        if (isset($this->request->post['megjelenit_regisztracio_cegnev_kotelezo'])) {
            $this->data['megjelenit_regisztracio_cegnev_kotelezo'] = $this->request->post['megjelenit_regisztracio_cegnev_kotelezo'];
        } else {
            $this->data['megjelenit_regisztracio_cegnev_kotelezo'] = $this->config->get('megjelenit_regisztracio_cegnev_kotelezo');
        }
        if (isset($this->request->post['megjelenit_regisztracio_adoszam'])) {
            $this->data['megjelenit_regisztracio_adoszam'] = $this->request->post['megjelenit_regisztracio_adoszam'];
        } else {
            $this->data['megjelenit_regisztracio_adoszam'] = $this->config->get('megjelenit_regisztracio_adoszam');
        }
        if (isset($this->request->post['megjelenit_regisztracio_adoszam_kotelezo'])) {
            $this->data['megjelenit_regisztracio_adoszam_kotelezo'] = $this->request->post['megjelenit_regisztracio_adoszam_kotelezo'];
        } else {
            $this->data['megjelenit_regisztracio_adoszam_kotelezo'] = $this->config->get('megjelenit_regisztracio_adoszam_kotelezo');
        }
        if (isset($this->request->post['megjelenit_regisztracio_vallalkozasi_forma'])) {
            $this->data['megjelenit_regisztracio_vallalkozasi_forma'] = $this->request->post['megjelenit_regisztracio_vallalkozasi_forma'];
        } else {
            $this->data['megjelenit_regisztracio_vallalkozasi_forma'] = $this->config->get('megjelenit_regisztracio_vallalkozasi_forma');
        }
        if (isset($this->request->post['megjelenit_regisztracio_vallalkozasi_forma_kotelezo'])) {
            $this->data['megjelenit_regisztracio_vallalkozasi_forma_kotelezo'] = $this->request->post['megjelenit_regisztracio_vallalkozasi_forma_kotelezo'];
        } else {
            $this->data['megjelenit_regisztracio_vallalkozasi_forma_kotelezo'] = $this->config->get('megjelenit_regisztracio_vallalkozasi_forma_kotelezo');
        }
        if (isset($this->request->post['megjelenit_regisztracio_szekhely'])) {
            $this->data['megjelenit_regisztracio_szekhely'] = $this->request->post['megjelenit_regisztracio_szekhely'];
        } else {
            $this->data['megjelenit_regisztracio_szekhely'] = $this->config->get('megjelenit_regisztracio_szekhely');
        }
        if (isset($this->request->post['megjelenit_regisztracio_szekhely_kotelezo'])) {
            $this->data['megjelenit_regisztracio_szekhely_kotelezo'] = $this->request->post['megjelenit_regisztracio_szekhely_kotelezo'];
        } else {
            $this->data['megjelenit_regisztracio_szekhely_kotelezo'] = $this->config->get('megjelenit_regisztracio_szekhely_kotelezo');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ugyvezeto_neve'])) {
            $this->data['megjelenit_regisztracio_ugyvezeto_neve'] = $this->request->post['megjelenit_regisztracio_ugyvezeto_neve'];
        } else {
            $this->data['megjelenit_regisztracio_ugyvezeto_neve'] = $this->config->get('megjelenit_regisztracio_ugyvezeto_neve');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ugyvezeto_neve_kotelezo'])) {
            $this->data['megjelenit_regisztracio_ugyvezeto_neve_kotelezo'] = $this->request->post['megjelenit_regisztracio_ugyvezeto_neve_kotelezo'];
        } else {
            $this->data['megjelenit_regisztracio_ugyvezeto_neve_kotelezo'] = $this->config->get('megjelenit_regisztracio_ugyvezeto_neve_kotelezo');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ugyvezeto_telefonszama'])) {
            $this->data['megjelenit_regisztracio_ugyvezeto_telefonszama'] = $this->request->post['megjelenit_regisztracio_ugyvezeto_telefonszama'];
        } else {
            $this->data['megjelenit_regisztracio_ugyvezeto_telefonszama'] = $this->config->get('megjelenit_regisztracio_ugyvezeto_telefonszama');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo'])) {
            $this->data['megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo'] = $this->request->post['megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo'];
        } else {
            $this->data['megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo'] = $this->config->get('megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo');
        }

        if (isset($this->request->post['megjelenit_regisztracio_cimadat_cegnev'])) {
            $this->data['megjelenit_regisztracio_cimadat_cegnev'] = $this->request->post['megjelenit_regisztracio_cimadat_cegnev'];
        } else {
            $this->data['megjelenit_regisztracio_cimadat_cegnev'] = $this->config->get('megjelenit_regisztracio_cimadat_cegnev');
        }
        if (isset($this->request->post['megjelenit_regisztracio_cimadat_adoszam'])) {
            $this->data['megjelenit_regisztracio_cimadat_adoszam'] = $this->request->post['megjelenit_regisztracio_cimadat_adoszam'];
        } else {
            $this->data['megjelenit_regisztracio_cimadat_adoszam'] = $this->config->get('megjelenit_regisztracio_cimadat_adoszam');
        }
        if (isset($this->request->post['megjelenit_regisztracio_cimadat_vallalkozasi_forma'])) {
            $this->data['megjelenit_regisztracio_cimadat_vallalkozasi_forma'] = $this->request->post['megjelenit_regisztracio_cimadat_vallalkozasi_forma'];
        } else {
            $this->data['megjelenit_regisztracio_cimadat_vallalkozasi_forma'] = $this->config->get('megjelenit_regisztracio_cimadat_vallalkozasi_forma');
        }
        if (isset($this->request->post['megjelenit_regisztracio_cimadat_szekhely'])) {
            $this->data['megjelenit_regisztracio_cimadat_szekhely'] = $this->request->post['megjelenit_regisztracio_cimadat_szekhely'];
        } else {
            $this->data['megjelenit_regisztracio_cimadat_szekhely'] = $this->config->get('megjelenit_regisztracio_cimadat_szekhely');
        }
        if (isset($this->request->post['megjelenit_regisztracio_cimadat_ugyvezeto_neve'])) {
            $this->data['megjelenit_regisztracio_cimadat_ugyvezeto_neve'] = $this->request->post['megjelenit_regisztracio_cimadat_ugyvezeto_neve'];
        } else {
            $this->data['megjelenit_regisztracio_cimadat_ugyvezeto_neve'] = $this->config->get('megjelenit_regisztracio_cimadat_ugyvezeto_neve');
        }
        if (isset($this->request->post['megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama'])) {
            $this->data['megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama'] = $this->request->post['megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama'];
        } else {
            $this->data['megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama'] = $this->config->get('megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama');
        }
        if (isset($this->request->post['megjelenit_regisztracio_cimadat_kiegeszito'])) {
            $this->data['megjelenit_regisztracio_cimadat_kiegeszito'] = $this->request->post['megjelenit_regisztracio_cimadat_kiegeszito'];
        } else {
            $this->data['megjelenit_regisztracio_cimadat_kiegeszito'] = $this->config->get('megjelenit_regisztracio_cimadat_kiegeszito');
        }
        if (isset($this->request->post['megjelenit_regisztracio_email'])) {
            $this->data['megjelenit_regisztracio_email'] = $this->request->post['megjelenit_email'];
        } else {
            $this->data['megjelenit_regisztracio_email'] = $this->config->get('megjelenit_regisztracio_email');
        }
        if (isset($this->request->post['megjelenit_regisztracio_email_megerosites'])) {
            $this->data['megjelenit_regisztracio_email_megerosites'] = $this->request->post['megjelenit_regisztracio_email_megerosites'];
        } else {
            $this->data['megjelenit_regisztracio_email_megerosites'] = $this->config->get('megjelenit_regisztracio_email_megerosites');
        }
        if (isset($this->request->post['megjelenit_regisztracio_telefon'])) {
            $this->data['megjelenit_regisztracio_telefon'] = $this->request->post['megjelenit_regisztracio_telefon'];
        } else {
            $this->data['megjelenit_regisztracio_telefon'] = $this->config->get('megjelenit_regisztracio_telefon');
        }
        if (isset($this->request->post['megjelenit_regisztracio_fax'])) {
            $this->data['megjelenit_regisztracio_fax'] = $this->request->post['megjelenit_regisztracio_fax'];
        } else {
            $this->data['megjelenit_regisztracio_fax'] = $this->config->get('megjelenit_regisztracio_fax');
        }
        if (isset($this->request->post['megjelenit_regisztracio_paypal'])) {
            $this->data['megjelenit_regisztracio_paypal'] = $this->request->post['megjelenit_regisztracio_paypal'];
        } else {
            $this->data['megjelenit_regisztracio_paypal'] = $this->config->get('megjelenit_regisztracio_paypal');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_utca'])) {
            $this->data['megjelenit_regisztracio_ceges_utca'] = $this->request->post['megjelenit_regisztracio_ceges_utca'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_utca'] = $this->config->get('megjelenit_regisztracio_ceges_utca');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_varos'])) {
            $this->data['megjelenit_regisztracio_ceges_varos'] = $this->request->post['megjelenit_regisztracio_ceges_varos'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_varos'] = $this->config->get('megjelenit_regisztracio_ceges_varos');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_iranyitoszam'])) {
            $this->data['megjelenit_regisztracio_ceges_iranyitoszam'] = $this->request->post['megjelenit_regisztracio_ceges_iranyitoszam'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_iranyitoszam'] = $this->config->get('megjelenit_regisztracio_ceges_iranyitoszam');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_orszag'])) {
            $this->data['megjelenit_regisztracio_ceges_orszag'] = $this->request->post['megjelenit_regisztracio_ceges_orszag'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_orszag'] = $this->config->get('megjelenit_regisztracio_ceges_orszag');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_megye'])) {
            $this->data['megjelenit_regisztracio_ceges_megye'] = $this->request->post['megjelenit_regisztracio_ceges_megye'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_megye'] = $this->config->get('megjelenit_regisztracio_ceges_megye');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_iskolai_vegzettseg'])) {
            $this->data['megjelenit_regisztracio_ceges_iskolai_vegzettseg'] = $this->request->post['megjelenit_regisztracio_ceges_iskolai_vegzettseg'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_iskolai_vegzettseg'] = $this->config->get('megjelenit_regisztracio_ceges_iskolai_vegzettseg');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_vezeteknev'])) {
            $this->data['megjelenit_regisztracio_ceges_vezeteknev'] = $this->request->post['megjelenit_regisztracio_ceges_vezeteknev'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_vezeteknev'] = $this->config->get('megjelenit_regisztracio_ceges_vezeteknev');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_keresztnev'])) {
            $this->data['megjelenit_regisztracio_ceges_keresztnev'] = $this->request->post['megjelenit_regisztracio_ceges_keresztnev'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_keresztnev'] = $this->config->get('megjelenit_regisztracio_ceges_keresztnev');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_cegnev'])) {
            $this->data['megjelenit_regisztracio_ceges_cegnev'] = $this->request->post['megjelenit_regisztracio_ceges_cegnev'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_cegnev'] = $this->config->get('megjelenit_regisztracio_ceges_cegnev');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_adoszam'])) {
            $this->data['megjelenit_regisztracio_ceges_adoszam'] = $this->request->post['megjelenit_regisztracio_ceges_adoszam'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_adoszam'] = $this->config->get('megjelenit_regisztracio_ceges_adoszam');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_vallalkozasi_forma'])) {
            $this->data['megjelenit_regisztracio_ceges_vallalkozasi_forma'] = $this->request->post['megjelenit_regisztracio_ceges_vallalkozasi_forma'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_vallalkozasi_forma'] = $this->config->get('megjelenit_regisztracio_ceges_vallalkozasi_forma');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_szekhely'])) {
            $this->data['megjelenit_regisztracio_ceges_szekhely'] = $this->request->post['megjelenit_regisztracio_ceges_szekhely'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_szekhely'] = $this->config->get('megjelenit_regisztracio_ceges_szekhely');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_ugyvezeto_neve'])) {
            $this->data['megjelenit_regisztracio_ceges_ugyvezeto_neve'] = $this->request->post['megjelenit_regisztracio_ceges_ugyvezeto_neve'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_ugyvezeto_neve'] = $this->config->get('megjelenit_regisztracio_ceges_ugyvezeto_neve');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_ugyvezeto_telefonszama'])) {
            $this->data['megjelenit_regisztracio_ceges_ugyvezeto_telefonszama'] = $this->request->post['megjelenit_regisztracio_ceges_ugyvezeto_telefonszama'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_ugyvezeto_telefonszama'] = $this->config->get('megjelenit_regisztracio_ceges_ugyvezeto_telefonszama');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_cimadat_cegnev'])) {
            $this->data['megjelenit_regisztracio_ceges_cimadat_cegnev'] = $this->request->post['megjelenit_regisztracio_ceges_cimadat_cegnev'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_cimadat_cegnev'] = $this->config->get('megjelenit_regisztracio_ceges_cimadat_cegnev');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_cimadat_adoszam'])) {
            $this->data['megjelenit_regisztracio_ceges_cimadat_adoszam'] = $this->request->post['megjelenit_regisztracio_ceges_cimadat_adoszam'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_cimadat_adoszam'] = $this->config->get('megjelenit_regisztracio_ceges_cimadat_adoszam');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma'])) {
            $this->data['megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma'] = $this->request->post['megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma'] = $this->config->get('megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_cimadat_szekhely'])) {
            $this->data['megjelenit_regisztracio_ceges_cimadat_szekhely'] = $this->request->post['megjelenit_regisztracio_ceges_cimadat_szekhely'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_cimadat_szekhely'] = $this->config->get('megjelenit_regisztracio_ceges_cimadat_szekhely');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve'])) {
            $this->data['megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve'] = $this->request->post['megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve'] = $this->config->get('megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama'])) {
            $this->data['megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama'] = $this->request->post['megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama'] = $this->config->get('megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_cimadat_kiegeszito'])) {
            $this->data['megjelenit_regisztracio_ceges_cimadat_kiegeszito'] = $this->request->post['megjelenit_regisztracio_ceges_cimadat_kiegeszito'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_cimadat_kiegeszito'] = $this->config->get('megjelenit_regisztracio_ceges_cimadat_kiegeszito');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_email'])) {
            $this->data['megjelenit_regisztracio_ceges_email'] = $this->request->post['megjelenit_email'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_email'] = $this->config->get('megjelenit_regisztracio_ceges_email');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_email_megerosites'])) {
            $this->data['megjelenit_regisztracio_ceges_email_megerosites'] = $this->request->post['megjelenit_regisztracio_ceges_email_megerosites'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_email_megerosites'] = $this->config->get('megjelenit_regisztracio_ceges_email_megerosites');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_telefon'])) {
            $this->data['megjelenit_regisztracio_ceges_telefon'] = $this->request->post['megjelenit_regisztracio_ceges_telefon'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_telefon'] = $this->config->get('megjelenit_regisztracio_ceges_telefon');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_fax'])) {
            $this->data['megjelenit_regisztracio_ceges_fax'] = $this->request->post['megjelenit_regisztracio_ceges_fax'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_fax'] = $this->config->get('megjelenit_regisztracio_ceges_fax');
        }
        if (isset($this->request->post['megjelenit_regisztracio_ceges_paypal'])) {
            $this->data['megjelenit_regisztracio_ceges_paypal'] = $this->request->post['megjelenit_regisztracio_ceges_paypal'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_paypal'] = $this->config->get('megjelenit_regisztracio_ceges_paypal');
        }
        if (isset($this->request->post['megjelenit_nem_regisztracio_ceges'])) {
            $this->data['megjelenit_nem_regisztracio_ceges'] = $this->request->post['megjelenit_nem_regisztracio_ceges'];
        } else {
            $this->data['megjelenit_nem_regisztracio_ceges'] = $this->config->get('megjelenit_nem_regisztracio_ceges');
        }
        if (isset($this->request->post['megjelenit_eletkor_regisztracio_ceges'])) {
            $this->data['megjelenit_eletkor_regisztracio_ceges'] = $this->request->post['megjelenit_eletkor_regisztracio_ceges'];
        } else {
            $this->data['megjelenit_eletkor_regisztracio_ceges'] = $this->config->get('megjelenit_eletkor_regisztracio_ceges');
        }


        if (isset($this->request->post['megjelenit_regisztracioblokk_szemelyes'])) {
            $this->data['megjelenit_regisztracioblokk_szemelyes'] = $this->request->post['megjelenit_regisztracioblokk_szemelyes'];
        } else {
            $this->data['megjelenit_regisztracioblokk_szemelyes'] = $this->config->get('megjelenit_regisztracioblokk_szemelyes');
        }
        if (isset($this->request->post['megjelenit_regisztracioblokk_ceges_szemelyes'])) {
            $this->data['megjelenit_regisztracioblokk_ceges_szemelyes'] = $this->request->post['megjelenit_regisztracioblokk_ceges_szemelyes'];
        } else {
            $this->data['megjelenit_regisztracioblokk_ceges_szemelyes'] = $this->config->get('megjelenit_regisztracioblokk_ceges_szemelyes');
        }
        if (isset($this->request->post['megjelenit_regisztracioblokk_cim'])) {
            $this->data['megjelenit_regisztracioblokk_cim'] = $this->request->post['megjelenit_regisztracioblokk_cim'];
        } else {
            $this->data['megjelenit_regisztracioblokk_cim'] = $this->config->get('megjelenit_regisztracioblokk_cim');
        }
        if (isset($this->request->post['megjelenit_regisztracioblokk_ceges_cim'])) {
            $this->data['megjelenit_regisztracioblokk_ceges_cim'] = $this->request->post['megjelenit_regisztracioblokk_ceges_cim'];
        } else {
            $this->data['megjelenit_regisztracioblokk_ceges_cim'] = $this->config->get('megjelenit_regisztracioblokk_ceges_cim');
        }
        if (isset($this->request->post['megjelenit_regisztracioblokk_paypal'])) {
            $this->data['megjelenit_regisztracioblokk_paypal'] = $this->request->post['megjelenit_regisztracioblokk_paypal'];
        } else {
            $this->data['megjelenit_regisztracioblokk_paypal'] = $this->config->get('megjelenit_regisztracioblokk_paypal');
        }
        if (isset($this->request->post['megjelenit_regisztracioblokk_ceges_paypal'])) {
            $this->data['megjelenit_regisztracioblokk_ceges_paypal'] = $this->request->post['megjelenit_regisztracioblokk_ceges_paypal'];
        } else {
            $this->data['megjelenit_regisztracioblokk_ceges_paypal'] = $this->config->get('megjelenit_regisztracioblokk_ceges_paypal');
        }
        if (isset($this->request->post['megjelenit_regisztracioblokk_hirlevel'])) {
            $this->data['megjelenit_regisztracioblokk_hirlevel'] = $this->request->post['megjelenit_regisztracioblokk_hirlevel'];
        } else {
            $this->data['megjelenit_regisztracioblokk_hirlevel'] = $this->config->get('megjelenit_regisztracioblokk_hirlevel');
        }
        if (isset($this->request->post['megjelenit_regisztracioblokk_ceges_hirlevel'])) {
            $this->data['megjelenit_regisztracioblokk_ceges_hirlevel'] = $this->request->post['megjelenit_regisztracioblokk_ceges_hirlevel'];
        } else {
            $this->data['megjelenit_regisztracioblokk_ceges_hirlevel'] = $this->config->get('megjelenit_regisztracioblokk_ceges_hirlevel');
        }

        if (isset($this->request->post['megjelenit_regisztracio_hirlevel_kategoriak'])) {
            $this->data['megjelenit_regisztracio_hirlevel_kategoriak'] = $this->request->post['megjelenit_regisztracio_hirlevel_kategoriak'];
        } else {
            $this->data['megjelenit_regisztracio_hirlevel_kategoriak'] = $this->config->get('megjelenit_regisztracio_hirlevel_kategoriak');
        }

        if (isset($this->request->post['megjelenit_regisztracio_ceges_hirlevel_kategoriak'])) {
            $this->data['megjelenit_regisztracio_ceges_hirlevel_kategoriak'] = $this->request->post['megjelenit_regisztracio_ceges_hirlevel_kategoriak'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_hirlevel_kategoriak'] = $this->config->get('megjelenit_regisztracio_ceges_hirlevel_kategoriak');
        }

        if (isset($this->request->post['megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben'])) {
            $this->data['megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben'] = $this->request->post['megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben'];
        } else {
            $this->data['megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben'] = $this->config->get('megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben');
        }

        if (isset($this->request->post['megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben'])) {
            $this->data['megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben'] = $this->request->post['megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben'];
        } else {
            $this->data['megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben'] = $this->config->get('megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben');
        }

        if (isset($this->request->post['megjelenit_admin_altalanos'])) {
            $admin = $this->request->post['megjelenit_admin_altalanos'];
        } else {
            $admin = $this->config->get("megjelenit_admin_altalanos");
        }

        $megjelenit_admin_altalanos = array(
            'ketszintu_szuro_admin'   => isset($admin['ketszintu_szuro_admin']) ? $admin['ketszintu_szuro_admin'] : 0
        );
        $this->data['ketszintu_szuro_admin'] = $megjelenit_admin_altalanos;

        $megjelenit_admin_altalanos = array(
            'termek_elhelyezkedes'   => isset($admin['termek_elhelyezkedes']) ? $admin['termek_elhelyezkedes'] : 0
        );
        $this->data['termek_elhelyezkedes'] = $megjelenit_admin_altalanos;

        $megjelenit_admin_altalanos = array(
            'termek_kiemeles_modositasa'   => isset($admin['termek_kiemeles_modositasa']) ? $admin['termek_kiemeles_modositasa'] : 0
        );
        $this->data['termek_kiemeles_modositasa'] = $megjelenit_admin_altalanos;


        $megjelenit_admin_altalanos = array(
            'csv_export_import'   => isset($admin['csv_export_import']) ? $admin['csv_export_import'] : 0
        );
        $this->data['csv_export_import'] = $megjelenit_admin_altalanos;

        $megjelenit_admin_altalanos = array(
            'fogalom_magyarazat'   => isset($admin['fogalom_magyarazat']) ? $admin['fogalom_magyarazat'] : 0
        );
        $this->data['fogalom_magyarazat'] = $megjelenit_admin_altalanos;

        $megjelenit_admin_altalanos = array(
            'felhasznalo_atiranyitas'   => isset($admin['felhasznalo_atiranyitas']) ? $admin['felhasznalo_atiranyitas'] : 0
        );
        $this->data['felhasznalo_atiranyitas'] = $megjelenit_admin_altalanos;

        if (isset($this->request->post['megjelenit_admin_product'])) {
            $admin = $this->request->post['megjelenit_admin_product'];
        } else {
            $admin = $this->config->get("megjelenit_admin_product");
        }

        $megjelenit_admin_product = array(
            'filter_csoport'   => isset($admin['filter_csoport']) ? $admin['filter_csoport'] : 0,
            'filter_csoport_sorrend' => isset($admin['filter_csoport_sorrend']) ? $admin['filter_csoport_sorrend'] : 0,
            'filter_csoport_neve' => isset($admin['filter_csoport_neve']) ? $admin['filter_csoport_neve'] : "",
            'filter_csoport_ellenorzes' => isset($admin['filter_csoport_ellenorzes']) ? $admin['filter_csoport_ellenorzes'] : 0
        );
        $this->data['filter_csoport'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'megnevezes'   => isset($admin['megnevezes']) ? $admin['megnevezes'] : 0,
            'megnevezes_sorrend' => isset($admin['megnevezes_sorrend']) ? $admin['megnevezes_sorrend'] : 0,
            'megnevezes_neve' => isset($admin['megnevezes_neve']) ? $admin['megnevezes_neve'] : "",
            'megnevezes_ellenorzes' => isset($admin['megnevezes_ellenorzes']) ? $admin['megnevezes_ellenorzes'] : 0
        );
        $this->data['megnevezes'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'keyword'   => isset($admin['keyword']) ? $admin['keyword'] : 0,
            'keyword_sorrend' => isset($admin['keyword_sorrend']) ? $admin['keyword_sorrend'] : 0,
            'keyword_neve' => isset($admin['keyword_neve']) ? $admin['keyword_neve'] : "",
            'keyword_ellenorzes' => isset($admin['keyword_ellenorzes']) ? $admin['keyword_ellenorzes'] : 0
        );
        $this->data['keyword'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'meta_description'   => isset($admin['meta_description']) ? $admin['meta_description'] : 0,
            'meta_description_sorrend' => isset($admin['meta_description_sorrend']) ? $admin['meta_description_sorrend'] : 0,
            'meta_description_neve' => isset($admin['meta_description_neve']) ? $admin['meta_description_neve'] : "",
            'meta_description_ellenorzes' => isset($admin['meta_description_ellenorzes']) ? $admin['meta_description_ellenorzes'] : 0
        );
        $this->data['meta_description'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'description'   => isset($admin['description']) ? $admin['description'] : 0,
            'description_sorrend' => isset($admin['description_sorrend']) ? $admin['description_sorrend'] : 0,
            'description_neve' => isset($admin['description_neve']) ? $admin['description_neve'] : "",
            'description_ellenorzes' => isset($admin['description_ellenorzes']) ? $admin['description_ellenorzes'] : 0
        );
        $this->data['description'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'tag'   => isset($admin['tag']) ? $admin['tag'] : 0,
            'tag_sorrend' => isset($admin['tag_sorrend']) ? $admin['tag_sorrend'] : 0,
            'tag_neve' => isset($admin['tag_neve']) ? $admin['tag_neve'] : "",
            'tag_ellenorzes' => isset($admin['tag_ellenorzes']) ? $admin['tag_ellenorzes'] : 0
        );
        $this->data['tag'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'category'   => isset($admin['category']) ? $admin['category'] : 0,
            'category_sorrend' => isset($admin['category_sorrend']) ? $admin['category_sorrend'] : 0,
            'category_neve' => isset($admin['category_neve']) ? $admin['category_neve'] : "",
            'category_ellenorzes' => isset($admin['category_ellenorzes']) ? $admin['category_ellenorzes'] : 0
        );
        $this->data['category'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'filter'   => isset($admin['filter']) ? $admin['filter'] : 0,
            'filter_sorrend' => isset($admin['filter_sorrend']) ? $admin['filter_sorrend'] : 0,
            'filter_neve' => isset($admin['filter_neve']) ? $admin['filter_neve'] : "",
            'filter_ellenorzes' => isset($admin['filter_ellenorzes']) ? $admin['filter_ellenorzes'] : 0
        );
        $this->data['filter'] = $megjelenit_admin_product;


        $megjelenit_admin_product = array(
            'model'   => isset($admin['model']) ? $admin['model'] : 0,
            'model_sorrend' => isset($admin['model_sorrend']) ? $admin['model_sorrend'] : 0,
            'model_neve' => isset($admin['model_neve']) ? $admin['model_neve'] : "",
            'model_ellenorzes' => isset($admin['model_ellenorzes']) ? $admin['model_ellenorzes'] : 0
        );
        $this->data['model'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'sku'   => isset($admin['sku']) ? $admin['sku'] : 0,
            'sku_sorrend' => isset($admin['sku_sorrend']) ? $admin['sku_sorrend'] : 0,
            'sku_neve' => isset($admin['sku_neve']) ? $admin['sku_neve'] : "",
            'sku_ellenorzes' => isset($admin['sku_ellenorzes']) ? $admin['sku_ellenorzes'] : 0
        );
        $this->data['sku'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'meret'   => isset($admin['meret']) ? $admin['meret'] : 0,
            'meret_sorrend' => isset($admin['meret_sorrend']) ? $admin['meret_sorrend'] : 0,
            'meret_neve' => isset($admin['meret_neve']) ? $admin['meret_neve'] : "",
            'meret_ellenorzes' => isset($admin['meret_ellenorzes']) ? $admin['meret_ellenorzes'] : 0
        );
        $this->data['meret'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
                'upc'   => isset($admin['upc']) ? $admin['upc'] : 0,
                'upc_sorrend' => isset($admin['upc_sorrend']) ? $admin['upc_sorrend'] : 0,
                'upc_neve' => isset($admin['upc_neve']) ? $admin['upc_neve'] : "",
                'upc_ellenorzes' => isset($admin['upc_ellenorzes']) ? $admin['upc_ellenorzes'] : 0
        );
        $this->data['upc'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'location'   => isset($admin['location']) ? $admin['location'] : 0,
            'location_sorrend' => isset($admin['location_sorrend']) ? $admin['location_sorrend'] : 0,
            'location_neve' => isset($admin['location_neve']) ? $admin['location_neve'] : "",
            'location_ellenorzes' => isset($admin['location_ellenorzes']) ? $admin['location_ellenorzes'] : 0
        );
        $this->data['location'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'quantity'   => isset($admin['quantity']) ? $admin['quantity'] : 0,
            'quantity_sorrend' => isset($admin['quantity_sorrend']) ? $admin['quantity_sorrend'] : 0,
            'quantity_neve' => isset($admin['quantity_neve']) ? $admin['quantity_neve'] : "",
            'quantity_ellenorzes' => isset($admin['quantity_ellenorzes']) ? $admin['quantity_ellenorzes'] : 0
        );
        $this->data['quantity'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'stock_status_id'   => isset($admin['stock_status_id']) ? $admin['stock_status_id'] : 0,
            'stock_status_id_sorrend' => isset($admin['stock_status_id_sorrend']) ? $admin['stock_status_id_sorrend'] : 0,
            'stock_status_id_neve' => isset($admin['stock_status_id_neve']) ? $admin['stock_status_id_neve'] : "",
            'stock_status_id_ellenorzes' => isset($admin['stock_status_id_ellenorzes']) ? $admin['stock_status_id_ellenorzes'] : 0
        );
        $this->data['stock_status_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'image'   => isset($admin['image']) ? $admin['image'] : 0,
            'image_sorrend' => isset($admin['image_sorrend']) ? $admin['image_sorrend'] : 0,
            'image_neve' => isset($admin['image_neve']) ? $admin['image_neve'] : "",
            'image_ellenorzes' => isset($admin['image_ellenorzes']) ? $admin['image_ellenorzes'] : 0
        );
        $this->data['image'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'images'   => isset($admin['images']) ? $admin['images'] : 0,
            'images_sorrend' => isset($admin['images_sorrend']) ? $admin['images_sorrend'] : 0,
            'images_neve' => isset($admin['images_neve']) ? $admin['images_neve'] : "",
            'images_ellenorzes' => isset($admin['images_ellenorzes']) ? $admin['images_ellenorzes'] : 0
        );
        $this->data['images'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'elolnezet'   => isset($admin['elolnezet']) ? $admin['elolnezet'] : 0,
            'elolnezet_sorrend' => isset($admin['elolnezet_sorrend']) ? $admin['elolnezet_sorrend'] : 0,
            'elolnezet_neve' => isset($admin['elolnezet_neve']) ? $admin['elolnezet_neve'] : "",
            'elolnezet_ellenorzes' => isset($admin['elolnezet_ellenorzes']) ? $admin['elolnezet_ellenorzes'] : 0
        );
        $this->data['elolnezet'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'manufacturer_id'   => isset($admin['manufacturer_id']) ? $admin['manufacturer_id'] : 0,
            'manufacturer_id_sorrend' => isset($admin['manufacturer_id_sorrend']) ? $admin['manufacturer_id_sorrend'] : 0,
            'manufacturer_id_neve' => isset($admin['manufacturer_id_neve']) ? $admin['manufacturer_id_neve'] : "",
            'manufacturer_id_ellenorzes' => isset($admin['manufacturer_id_ellenorzes']) ? $admin['manufacturer_id_ellenorzes'] : 0
        );
        $this->data['manufacturer_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'shipping'   => isset($admin['shipping']) ? $admin['shipping'] : 0,
            'shipping_sorrend' => isset($admin['shipping_sorrend']) ? $admin['shipping_sorrend'] : 0,
            'shipping_neve' => isset($admin['shipping_neve']) ? $admin['shipping_neve'] : "",
            'shipping_ellenorzes' => isset($admin['shipping_ellenorzes']) ? $admin['shipping_ellenorzes'] : 0
        );
        $this->data['shipping'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'price'   => isset($admin['price']) ? $admin['price'] : 0,
            'price_sorrend' => isset($admin['price_sorrend']) ? $admin['price_sorrend'] : 0,
            'price_neve' => isset($admin['price_neve']) ? $admin['price_neve'] : "",
            'price_ellenorzes' => isset($admin['price_ellenorzes']) ? $admin['price_ellenorzes'] : 0
        );
        $this->data['price'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'szazalek'   => isset($admin['price']) ? $admin['szazalek'] : 0,
            'szazalek_sorrend' => isset($admin['szazalek_sorrend']) ? $admin['szazalek_sorrend'] : 0,
            'szazalek_neve' => isset($admin['szazalek_neve']) ? $admin['szazalek_neve'] : "",
            'szazalek_ellenorzes' => isset($admin['szazalek_ellenorzes']) ? $admin['szazalek_ellenorzes'] : 0
        );
        $this->data['szazalek'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'points'   => isset($admin['price']) ? $admin['price'] : 0,
            'points_sorrend' => isset($admin['points_sorrend']) ? $admin['points_sorrend'] : 0,
            'points_neve' => isset($admin['points_neve']) ? $admin['points_neve'] : "",
            'points_ellenorzes' => isset($admin['points_ellenorzes']) ? $admin['points_ellenorzes'] : 0
        );
        $this->data['points'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'tax_class_id'   => isset($admin['tax_class_id']) ? $admin['tax_class_id'] : 0,
            'tax_class_id_sorrend' => isset($admin['tax_class_id_sorrend']) ? $admin['tax_class_id_sorrend'] : 0,
            'tax_class_id_neve' => isset($admin['tax_class_id_neve']) ? $admin['tax_class_id_neve'] : "",
            'tax_class_id_ellenorzes' => isset($admin['tax_class_id_ellenorzes']) ? $admin['tax_class_id_ellenorzes'] : 0
        );
        $this->data['tax_class_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'date_available'   => isset($admin['date_available']) ? $admin['date_available'] : 0,
            'date_available_sorrend' => isset($admin['date_available_sorrend']) ? $admin['date_available_sorrend'] : 0,
            'date_available_neve' => isset($admin['date_available_neve']) ? $admin['date_available_neve'] : "",
            'date_available_ellenorzes' => isset($admin['date_available_ellenorzes']) ? $admin['date_available_ellenorzes'] : 0
        );
        $this->data['date_available'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'weight'   => isset($admin['weight']) ? $admin['weight'] : 0,
            'weight_sorrend' => isset($admin['weight_sorrend']) ? $admin['weight_sorrend'] : 0,
            'weight_neve' => isset($admin['weight_neve']) ? $admin['weight_neve'] : "",
            'weight_ellenorzes' => isset($admin['weight_ellenorzes']) ? $admin['weight_ellenorzes'] : 0
        );
        $this->data['weight'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'weight_class_id'   => isset($admin['weight_class_id']) ? $admin['weight_class_id'] : 0,
            'weight_class_id_sorrend' => isset($admin['weight_class_id_sorrend']) ? $admin['weight_class_id_sorrend'] : 0,
            'weight_class_id_neve' => isset($admin['weight_class_id_neve']) ? $admin['weight_class_id_neve'] : "",
            'weight_class_id_ellenorzes' => isset($admin['weight_class_id_ellenorzes']) ? $admin['weight_class_id_ellenorzes'] : 0
        );
        $this->data['weight_class_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'length'   => isset($admin['length']) ? $admin['length'] : 0,
            'length_sorrend' => isset($admin['length_sorrend']) ? $admin['length_sorrend'] : 0,
            'length_neve' => isset($admin['length_neve']) ? $admin['length_neve'] : "",
            'length_ellenorzes' => isset($admin['length_ellenorzes']) ? $admin['length_ellenorzes'] : 0
        );
        $this->data['length'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'width'   => isset($admin['width']) ? $admin['width'] : 0,
            'width_sorrend' => isset($admin['width_sorrend']) ? $admin['width_sorrend'] : 0,
            'width_neve' => isset($admin['width_neve']) ? $admin['width_neve'] : "",
            'width_ellenorzes' => isset($admin['width_ellenorzes']) ? $admin['width_ellenorzes'] : 0
        );
        $this->data['width'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'height'   => isset($admin['height']) ? $admin['height'] : 0,
            'height_sorrend' => isset($admin['height_sorrend']) ? $admin['height_sorrend'] : 0,
            'height_neve' => isset($admin['height_neve']) ? $admin['height_neve'] : "",
            'height_ellenorzes' => isset($admin['height_ellenorzes']) ? $admin['height_ellenorzes'] : 0
        );
        $this->data['height'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'length_class_id'   => isset($admin['length_class_id']) ? $admin['length_class_id'] : 0,
            'length_class_id_sorrend' => isset($admin['length_class_id_sorrend']) ? $admin['length_class_id_sorrend'] : 0,
            'length_class_id_neve' => isset($admin['length_class_id_neve']) ? $admin['length_class_id_neve'] : "",
            'length_class_id_ellenorzes' => isset($admin['length_class_id_ellenorzes']) ? $admin['length_class_id_ellenorzes'] : 0
        );
        $this->data['length_class_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'subtract'   => isset($admin['subtract']) ? $admin['subtract'] : 0,
            'subtract_sorrend' => isset($admin['subtract_sorrend']) ? $admin['subtract_sorrend'] : 0,
            'subtract_neve' => isset($admin['subtract_neve']) ? $admin['subtract_neve'] : "",
            'subtract_ellenorzes' => isset($admin['subtract_ellenorzes']) ? $admin['subtract_ellenorzes'] : 0
        );
        $this->data['subtract'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'minimum'   => isset($admin['minimum']) ? $admin['minimum'] : 0,
            'minimum_sorrend' => isset($admin['minimum_sorrend']) ? $admin['minimum_sorrend'] : 0,
            'minimum_neve' => isset($admin['minimum_neve']) ? $admin['minimum_neve'] : "",
            'minimum_ellenorzes' => isset($admin['minimum_ellenorzes']) ? $admin['minimum_ellenorzes'] : 0
        );
        $this->data['minimum'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'sort_order'   => isset($admin['sort_order']) ? $admin['sort_order'] : 0,
            'sort_order_sorrend' => isset($admin['sort_order_sorrend']) ? $admin['sort_order_sorrend'] : 0,
            'sort_order_neve' => isset($admin['sort_order_neve']) ? $admin['sort_order_neve'] : "",
            'sort_order_ellenorzes' => isset($admin['sort_order_ellenorzes']) ? $admin['sort_order_ellenorzes'] : 0
        );
        $this->data['sort_order'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'status'   => isset($admin['status']) ? $admin['status'] : 0,
            'status_sorrend' => isset($admin['status_sorrend']) ? $admin['status_sorrend'] : 0,
            'status_neve' => isset($admin['status_neve']) ? $admin['status_neve'] : "",
            'status_ellenorzes' => isset($admin['status_ellenorzes']) ? $admin['status_ellenorzes'] : 0
        );
        $this->data['status'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'date_added'   => isset($admin['date_added']) ? $admin['date_added'] : 0,
            'date_added_sorrend' => isset($admin['date_added_sorrend']) ? $admin['date_added_sorrend'] : 0,
            'date_added_neve' => isset($admin['date_added_neve']) ? $admin['date_added_neve'] : "",
            'date_added_ellenorzes' => isset($admin['date_added_ellenorzes']) ? $admin['date_added_ellenorzes'] : 0
        );
        $this->data['date_added'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'date_modified'   => isset($admin['date_modified']) ? $admin['date_modified'] : 0,
            'date_modified_sorrend' => isset($admin['date_modified_sorrend']) ? $admin['date_modified_sorrend'] : 0,
            'date_modified_neve' => isset($admin['date_modified_neve']) ? $admin['date_modified_neve'] : "",
            'date_modified_ellenorzes' => isset($admin['date_modified_ellenorzes']) ? $admin['date_modified_ellenorzes'] : 0
        );
        $this->data['date_modified'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'viewed'   => isset($admin['viewed']) ? $admin['viewed'] : 0,
            'viewed_sorrend' => isset($admin['viewed_sorrend']) ? $admin['viewed_sorrend'] : 0,
            'viewed_neve' => isset($admin['viewed_neve']) ? $admin['viewed_neve'] : "",
            'viewed_ellenorzes' => isset($admin['viewed_ellenorzes']) ? $admin['viewed_ellenorzes'] : 0
        );
        $this->data['viewed'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'cikkszam'   => isset($admin['cikkszam']) ? $admin['cikkszam'] : 0,
            'cikkszam_sorrend' => isset($admin['cikkszam_sorrend']) ? $admin['cikkszam_sorrend'] : 0,
            'cikkszam_neve' => isset($admin['cikkszam_neve']) ? $admin['cikkszam_neve'] : "",
            'cikkszam_ellenorzes' => isset($admin['cikkszam_ellenorzes']) ? $admin['cikkszam_ellenorzes'] : 0
        );
        $this->data['cikkszam'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'megyseg'   => isset($admin['megyseg']) ? $admin['megyseg'] : 0,
            'megyseg_sorrend' => isset($admin['megyseg_sorrend']) ? $admin['megyseg_sorrend'] : 0,
            'megyseg_neve' => isset($admin['megyseg_neve']) ? $admin['megyseg_neve'] : "",
            'megyseg_ellenorzes' => isset($admin['megyseg_ellenorzes']) ? $admin['megyseg_ellenorzes'] : 0
        );
        $this->data['megyseg'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'csomagolasi_mennyiseg'   => isset($admin['csomagolasi_mennyiseg']) ? $admin['csomagolasi_mennyiseg'] : 0,
            'csomagolasi_mennyiseg_sorrend' => isset($admin['csomagolasi_mennyiseg_sorrend']) ? $admin['csomagolasi_mennyiseg_sorrend'] : 0,
            'csomagolasi_mennyiseg_neve' => isset($admin['csomagolasi_mennyiseg_neve']) ? $admin['csomagolasi_mennyiseg_neve'] : "",
            'csomagolasi_mennyiseg_ellenorzes' => isset($admin['csomagolasi_mennyiseg_ellenorzes']) ? $admin['csomagolasi_mennyiseg_ellenorzes'] : 0
        );
        $this->data['csomagolasi_mennyiseg'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'csomagolasi_egyseg'   => isset($admin['csomagolasi_egyseg']) ? $admin['csomagolasi_egyseg'] : 0,
            'csomagolasi_egyseg_sorrend' => isset($admin['csomagolasi_egyseg_sorrend']) ? $admin['csomagolasi_egyseg_sorrend'] : 0,
            'csomagolasi_egyseg_neve' => isset($admin['csomagolasi_egyseg_neve']) ? $admin['csomagolasi_egyseg_neve'] : "",
            'csomagolasi_egyseg_ellenorzes' => isset($admin['csomagolasi_egyseg_ellenorzes']) ? $admin['csomagolasi_egyseg_ellenorzes'] : 0
        );
        $this->data['csomagolasi_egyseg'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'date_ervenyes_ig'   => isset($admin['date_ervenyes_ig']) ? $admin['date_ervenyes_ig'] : 0,
            'date_ervenyes_ig_sorrend' => isset($admin['date_ervenyes_ig_sorrend']) ? $admin['date_ervenyes_ig_sorrend'] : 0,
            'date_ervenyes_ig_neve' => isset($admin['date_ervenyes_ig_neve']) ? $admin['date_ervenyes_ig_neve'] : "",
            'date_ervenyes_ig_ellenorzes' => isset($admin['date_ervenyes_ig_ellenorzes']) ? $admin['date_ervenyes_ig_ellenorzes'] : 0
        );
        $this->data['date_ervenyes_ig'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'darabaru_meter'   => isset($admin['darabaru_meter']) ? $admin['darabaru_meter'] : 0,
            'darabaru_meter_sorrend' => isset($admin['darabaru_meter_sorrend']) ? $admin['darabaru_meter_sorrend'] : 0,
            'darabaru_meter_neve' => isset($admin['darabaru_meter_neve']) ? $admin['darabaru_meter_neve'] : "",
            'darabaru_meter_ellenorzes' => isset($admin['darabaru_meter_ellenorzes']) ? $admin['darabaru_meter_ellenorzes'] : 0
        );
        $this->data['darabaru_meter'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'cikkszam2'   => isset($admin['cikkszam2']) ? $admin['cikkszam2'] : 0,
            'cikkszam2_sorrend' => isset($admin['cikkszam2_sorrend']) ? $admin['cikkszam2_sorrend'] : 0,
            'cikkszam2_neve' => isset($admin['cikkszam2_neve']) ? $admin['cikkszam2_neve'] : "",
            'cikkszam2_ellenorzes' => isset($admin['cikkszam2_ellenorzes']) ? $admin['cikkszam2_ellenorzes'] : 0
        );
        $this->data['cikkszam2'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'eredeti_ar'   => isset($admin['eredeti_ar']) ? $admin['eredeti_ar'] : 0,
            'eredeti_ar_sorrend' => isset($admin['eredeti_ar_sorrend']) ? $admin['eredeti_ar_sorrend'] : 0,
            'eredeti_ar_neve' => isset($admin['eredeti_ar_neve']) ? $admin['eredeti_ar_neve'] : "",
            'eredeti_ar_ellenorzes' => isset($admin['eredeti_ar_ellenorzes']) ? $admin['eredeti_ar_ellenorzes'] : 0
        );
        $this->data['eredeti_ar'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'letoltheto'   => isset($admin['letoltheto']) ? $admin['letoltheto'] : 0,
            'letoltheto_sorrend' => isset($admin['letoltheto_sorrend']) ? $admin['letoltheto_sorrend'] : 0,
            'letoltheto_neve' => isset($admin['letoltheto_neve']) ? $admin['letoltheto_neve'] : "",
            'letoltheto_ellenorzes' => isset($admin['letoltheto_ellenorzes']) ? $admin['letoltheto_ellenorzes'] : 0
        );
        $this->data['letoltheto'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'elhelyezkedes'   => isset($admin['elhelyezkedes']) ? $admin['elhelyezkedes'] : 0,
            'elhelyezkedes_sorrend' => isset($admin['elhelyezkedes_sorrend']) ? $admin['elhelyezkedes_sorrend'] : 0,
            'elhelyezkedes_neve' => isset($admin['elhelyezkedes_neve']) ? $admin['elhelyezkedes_neve'] : ""
        );
        $this->data['elhelyezkedes_kiemeles'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'biztonsagi_kod'   => isset($admin['biztonsagi_kod']) ? $admin['biztonsagi_kod'] : 0,
            'biztonsagi_kod_sorrend' => isset($admin['biztonsagi_kod_sorrend']) ? $admin['biztonsagi_kod_sorrend'] : 0,
            'biztonsagi_kod_neve' => isset($admin['biztonsagi_kod_neve']) ? $admin['biztonsagi_kod_neve'] : "",
            'biztonsagi_kod_ellenorzes' => isset($admin['biztonsagi_kod_ellenorzes']) ? $admin['biztonsagi_kod_ellenorzes'] : 0
        );
        $this->data['biztonsagi_kod'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'lista_model'   => isset($admin['lista_model']) ? $admin['lista_model'] : 0,
            'lista_model_sorrend' => isset($admin['lista_model_sorrend']) ? $admin['lista_model_sorrend'] : 0
        );
        $this->data['lista_model'] = $megjelenit_admin_product;


        $lejarat_nelkuli = array(
            'lejarat_nelkuli'           => isset($admin['lejarat_nelkuli']) ? $admin['lejarat_nelkuli'] : 0,
            'lejarat_nelkuli_neve'      => isset($admin['lejarat_nelkuli_neve']) ? $admin['lejarat_nelkuli_neve'] : "",
            'lejarat_nelkuli_default'   => isset($admin['lejarat_nelkuli_default']) ? $admin['lejarat_nelkuli_default'] : 0
        );
        $this->data['lejarat_nelkuli'] = $lejarat_nelkuli;

        # Regisztráció email
        if (isset($this->request->post['megjelenit_regisztracios_email'])) {
            $regemail = $this->request->post['megjelenit_regisztracios_email'];
        } else {
            $regemail = $this->config->get("megjelenit_regisztracios_email");
        }

        $this->data['regisztracios_email'] = $regemail;

        /*Termekful start*/

        if (isset($this->request->post['megjelenit_admin_termekful'])) {
            $admin = $this->request->post['megjelenit_admin_termekful'];
        } else {
            $admin = $this->config->get("megjelenit_admin_termekful");
        }

        $megjelenit_admin_termekful = array(
            'tulajdonsag_ful'   => isset($admin['tulajdonsag_ful']) ? $admin['tulajdonsag_ful'] : 0
        );
        $this->data['tulajdonsag_ful'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'valasztek_ful'   => isset($admin['valasztek_ful']) ? $admin['valasztek_ful'] : 0
        );
        $this->data['valasztek_ful'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'mennyisegi_ful'   => isset($admin['mennyisegi_ful']) ? $admin['mennyisegi_ful'] : 0
        );
        $this->data['mennyisegi_ful'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'vevo_ful'   => isset($admin['vevo_ful']) ? $admin['vevo_ful'] : 0
        );
        $this->data['vevo_ful'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'akcios_ful'   => isset($admin['akcios_ful']) ? $admin['akcios_ful'] : 0
        );
        $this->data['akcios_ful'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'kep_ful'   => isset($admin['kep_ful']) ? $admin['kep_ful'] : 0
        );
        $this->data['kep_ful'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'jutalom_ful'   => isset($admin['jutalom_ful']) ? $admin['jutalom_ful'] : 0
        );
        $this->data['jutalom_ful'] = $megjelenit_admin_termekful;


        $megjelenit_admin_termekful = array(
            'elhelyezkedes'   => isset($admin['elhelyezkedes']) ? $admin['elhelyezkedes'] : 0
        );
        $this->data['elhelyezkedes'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'kepek_helyszinenkent'   => isset($admin['kepek_helyszinenkent']) ? $admin['kepek_helyszinenkent'] : 0
        );
        $this->data['kepek_helyszinenkent'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'tab_videok'   => isset($admin['tab_videok']) ? $admin['tab_videok'] : 0
        );
        $this->data['tab_videok'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'tab_pdf'   => isset($admin['tab_pdf']) ? $admin['tab_pdf'] : 0
        );
        $this->data['tab_pdf'] = $megjelenit_admin_termekful;

        $megjelenit_admin_termekful = array(
            'crm_kapcsolat'   => isset($admin['crm_kapcsolat']) ? $admin['crm_kapcsolat'] : 0
        );
        $this->data['crm_kapcsolat'] = $megjelenit_admin_termekful;



        /*Termekful end*/
        /*Termek adatok start*/

        if (isset($this->request->post['megjelenit_form_admin_model'])) {
            $this->data['megjelenit_form_admin_model'] = $this->request->post['megjelenit_form_admin_model'];
        } else {
            $this->data['megjelenit_form_admin_model'] = $this->config->get('megjelenit_form_admin_model');
        }
        if (isset($this->request->post['megjelenit_form_admin_model_readonly'])) {
            $this->data['megjelenit_form_admin_model_readonly'] = $this->request->post['megjelenit_form_admin_model_readonly'];
        } else {
            $this->data['megjelenit_form_admin_model_readonly'] = $this->config->get('megjelenit_form_admin_model_readonly');
        }

        if (isset($this->request->post['megjelenit_form_admin_cikkszam'])) {
            $this->data['megjelenit_form_admin_cikkszam'] = $this->request->post['megjelenit_form_admin_cikkszam'];
        } else {
            $this->data['megjelenit_form_admin_cikkszam'] = $this->config->get('megjelenit_form_admin_cikkszam');
        }

        if (isset($this->request->post['megjelenit_form_admin_cikkszam2'])) {
            $this->data['megjelenit_form_admin_cikkszam2'] = $this->request->post['megjelenit_form_admin_cikkszam2'];
        } else {
            $this->data['megjelenit_form_admin_cikkszam2'] = $this->config->get('megjelenit_form_admin_cikkszam2');
        }

        if (isset($this->request->post['megjelenit_form_admin_eredeti_ar'])) {
            $this->data['megjelenit_form_admin_eredeti_ar'] = $this->request->post['megjelenit_form_admin_eredeti_ar'];
        } else {
            $this->data['megjelenit_form_admin_eredeti_ar'] = $this->config->get('megjelenit_form_admin_eredeti_ar');
        }

        if (isset($this->request->post['megjelenit_form_admin_szazalek_ar'])) {
            $this->data['megjelenit_form_admin_szazalek_ar'] = $this->request->post['megjelenit_form_admin_szazalek_ar'];
        } else {
            $this->data['megjelenit_form_admin_szazalek_ar'] = $this->config->get('megjelenit_form_admin_szazalek_ar');
        }

        if (isset($this->request->post['megjelenit_form_admin_kifuto'])) {
            $this->data['megjelenit_form_admin_kifuto'] = $this->request->post['megjelenit_form_admin_kifuto'];
        } else {
            $this->data['megjelenit_form_admin_kifuto'] = $this->config->get('megjelenit_form_admin_kifuto');
        }

        if (isset($this->request->post['megjelenit_form_admin_ujdonsag'])) {
            $this->data['megjelenit_form_admin_ujdonsag'] = $this->request->post['megjelenit_form_admin_ujdonsag'];
        } else {
            $this->data['megjelenit_form_admin_ujdonsag'] = $this->config->get('megjelenit_form_admin_ujdonsag');
        }


        if (isset($this->request->post['megjelenit_form_admin_elerendeles'])) {
            $this->data['megjelenit_form_admin_elerendeles'] = $this->request->post['megjelenit_form_admin_elerendeles'];
        } else {
            $this->data['megjelenit_form_admin_elerendeles'] = $this->config->get('megjelenit_form_admin_elerendeles');
        }

        if (isset($this->request->post['megjelenit_form_admin_ar_tiltasa'])) {
            $this->data['megjelenit_form_admin_ar_tiltasa'] = $this->request->post['megjelenit_form_admin_ar_tiltasa'];
        } else {
            $this->data['megjelenit_form_admin_ar_tiltasa'] = $this->config->get('megjelenit_form_admin_ar_tiltasa');
        }

        if (isset($this->request->post['megjelenit_form_admin_megrendelem'])) {
            $this->data['megjelenit_form_admin_megrendelem'] = $this->request->post['megjelenit_form_admin_megrendelem'];
        } else {
            $this->data['megjelenit_form_admin_megrendelem'] = $this->config->get('megjelenit_form_admin_megrendelem');
        }

        if (isset($this->request->post['megjelenit_form_admin_utalvany'])) {
            $this->data['megjelenit_form_admin_utalvany'] = $this->request->post['megjelenit_form_admin_utalvany'];
        } else {
            $this->data['megjelenit_form_admin_utalvany'] = $this->config->get('megjelenit_form_admin_utalvany');
        }

        if (isset($this->request->post['megjelenit_admin_termekadatok'])) {
            $this->data['megjelenit_admin_termekadatok'] = $this->request->post['megjelenit_admin_termekadatok'];
        } else {
            $this->data['megjelenit_admin_termekadatok'] = $this->config->get("megjelenit_admin_termekadatok");
        }

        if (isset($this->request->post['megjelenit_admin_export'])) {
            $this->data['megjelenit_admin_export'] = $this->request->post['megjelenit_admin_export'];
        } else {
            $this->data['megjelenit_admin_export'] = $this->config->get("megjelenit_admin_export");
        }




        /*Termek adatok end*/


        /* Vevők start */
        if (isset($this->request->post['megjelenit_admin_vevok'])) {
            $admin = $this->request->post['megjelenit_admin_vevok'];
        } else {
            $admin = $this->config->get("megjelenit_admin_vevok");
        }

        $megjelenit_admin_vevok = array(
            'csv_vevok'   => isset($admin['csv_vevok']) ? $admin['csv_vevok'] : 0
        );
        $this->data['csv_vevok'] = $megjelenit_admin_vevok;

        $this->data['partner_kod'] = isset($admin['partner_kod']) ? $admin['partner_kod'] : 0;
        $this->data['szazalek_read_only'] = isset($admin['szazalek_read_only']) ? $admin['szazalek_read_only'] : 0;

        $megjelenit_admin_vevok = array(
            'csv_vevok_csoportnev'   => isset($admin['csv_vevok_csoportnev']) ? $admin['csv_vevok_csoportnev'] : ""
        );
        $this->data['csv_vevok_csoportnev'] = $megjelenit_admin_vevok;


        /* Vevők end */

        /*Rendelés start*/
        if (isset($this->request->post['megjelenit_admin_rendeles'])) {
            $admin = $this->request->post['megjelenit_admin_rendeles'];
        } else {
            $admin = $this->config->get("megjelenit_admin_rendeles");
        }

        $megjelenit_admin_rendeles = array(
            'ajandekutalvanyok_ful'   => isset($admin['ajandekutalvanyok_ful']) ? $admin['ajandekutalvanyok_ful'] : 0
        );
        $this->data['ajandekutalvanyok_ful'] = $megjelenit_admin_rendeles;

        $megjelenit_admin_rendeles = array(
            'kupon'   => isset($admin['kupon']) ? $admin['kupon'] : 0
        );
        $this->data['kupon'] = $megjelenit_admin_rendeles;

        $megjelenit_admin_rendeles = array(
            'ajandekutalvany'   => isset($admin['ajandekutalvany']) ? $admin['ajandekutalvany'] : 0
        );
        $this->data['ajandekutalvany'] = $megjelenit_admin_rendeles;


        $megjelenit_admin_rendeles = array(
            'jutalom'   => isset($admin['jutalom']) ? $admin['jutalom'] : 0
        );
        $this->data['jutalom'] = $megjelenit_admin_rendeles;


        $megjelenit_admin_rendeles = array(
            'sap_adatkuldes_hungarosack'   => isset($admin['sap_adatkuldes_hungarosack']) ? $admin['sap_adatkuldes_hungarosack'] : 1
        );
        $this->data['sap_adatkuldes_hungarosack'] = $megjelenit_admin_rendeles;


        $megjelenit_admin_rendeles = array(
            'csv_letolt'   => isset($admin['csv_letolt']) ? $admin['csv_letolt'] : 0
        );
        $this->data['csv_letolt'] = $megjelenit_admin_rendeles;

        $megjelenit_admin_rendeles = array(
            'csv_gls_pont'   => isset($admin['csv_gls_pont']) ? $admin['csv_gls_pont'] : 0
        );
        $this->data['csv_gls_pont'] = $megjelenit_admin_rendeles;

        $megjelenit_admin_rendeles = array(
            'csv_letolt_csoportnev'   => isset($admin['csv_letolt_csoportnev']) ? $admin['csv_letolt_csoportnev'] : ""
        );
        $this->data['csv_letolt_csoportnev'] = $megjelenit_admin_rendeles;

        $megjelenit_admin_rendeles = array(
            'csv_gls_pont_csoportnev'   => isset($admin['csv_gls_pont_csoportnev']) ? $admin['csv_gls_pont_csoportnev'] : ""
        );
        $this->data['csv_gls_pont_csoportnev'] = $megjelenit_admin_rendeles;

        $megjelenit_admin_rendeles = array(
            'csv_integracio'   => isset($admin['csv_integracio']) ? $admin['csv_integracio'] : 0
        );
        $this->data['csv_integracio'] = $megjelenit_admin_rendeles;

        $megjelenit_admin_rendeles = array(
            'csv_integracio_csoportnev'   => isset($admin['csv_integracio_csoportnev']) ? $admin['csv_integracio_csoportnev'] : ""
        );
        $this->data['csv_integracio_csoportnev'] = $megjelenit_admin_rendeles;

        $megjelenit_admin_rendeles = array(
            'xml_szamlazz'   => isset($admin['xml_szamlazz']) ? $admin['xml_szamlazz'] : 0
        );
        $this->data['xml_szamlazz'] = $megjelenit_admin_rendeles;

        /*Rendelés end*/

        /*kosár fizetés start*/
        if (isset($this->request->post['megjelenit_admin_kosar'])) {
            $admin = $this->request->post['megjelenit_admin_kosar'];
        } else {
            $admin = $this->config->get("megjelenit_admin_kosar");
        }

        $megjelenit_admin_kosar = array(
            'model_adat'   => isset($admin['model_adat']) ? $admin['model_adat'] : 0
        );
        $this->data['model_adat'] = $megjelenit_admin_kosar;


        $megjelenit_admin_kosar = array(
            'netto_adat'   => isset($admin['netto_adat']) ? $admin['netto_adat'] : 0
        );
        $this->data['netto_adat'] = $megjelenit_admin_kosar;

        $megjelenit_admin_kosar['cart_shipping_region']  = isset($admin['cart_shipping_region']) ? $admin['cart_shipping_region'] : 1;
        $megjelenit_admin_kosar['cart_shipping_postcode']  = isset($admin['cart_shipping_postcode']) ? $admin['cart_shipping_postcode'] : 1;

        $this->data['megjelenit_csak_vendegkent_vasarol']  = isset($this->request->post['megjelenit_csak_vendegkent_vasarol']) ? $this->request->post['megjelenit_csak_vendegkent_vasarol'] :
            ($this->config->get('megjelenit_csak_vendegkent_vasarol') ? $this->config->get('megjelenit_csak_vendegkent_vasarol') : 0);

        $this->data['megjelenit_admin_kosar'] = $megjelenit_admin_kosar;

        /*kosár fizetés end*/

        /*header start*/
        /*if (isset($this->request->post['megjelenit_header'])) {
            $admin = $this->request->post['megjelenit_header'];
        } else {
            $admin = $this->config->get("megjelenit_header");
        }

        $megjelenit_header = array(
            'penznem_kiiras'   => isset($admin['penznem_kiiras']) ? $admin['penznem_kiiras'] : 0,
            'modularis'   => isset($admin['modularis']) ? $admin['modularis'] : 0
        );
        $this->data['megjelenit_header'] = $megjelenit_header;*/

        /*header end*/


        if (isset($this->request->post['megjelenit_admin_keres_model'])) {
            $this->data['megjelenit_admin_keres_model'] = $this->request->post['megjelenit_admin_keres_model'];
        } else {
            $this->data['megjelenit_admin_keres_model'] = $this->config->get('megjelenit_admin_keres_model');
        }

        if (isset($this->request->post['megjelenit_admin_keres_cikkszam'])) {
            $this->data['megjelenit_admin_keres_cikkszam'] = $this->request->post['megjelenit_admin_keres_cikkszam'];
        } else {
            $this->data['megjelenit_admin_keres_cikkszam'] = $this->config->get('megjelenit_admin_keres_cikkszam');
        }

        if (isset($this->request->post['megjelenit_admin_keres_cikkszam2'])) {
            $this->data['megjelenit_admin_keres_cikkszam2'] = $this->request->post['megjelenit_admin_keres_cikkszam2'];
        } else {
            $this->data['megjelenit_admin_keres_cikkszam2'] = $this->config->get('megjelenit_admin_keres_cikkszam2');
        }


        /*Általános start*/
        if (isset($this->request->post['megjelenit_altalanos'])) {
            $admin = $this->request->post['megjelenit_altalanos'];
        } else {
            $admin = $this->config->get("megjelenit_altalanos");
        }

        $megjelenit_altalanos = array(
            'fixmenu'   => isset($admin['fixmenu']) ? $admin['fixmenu'] : 0
        );
        $this->data['fixmenu'] = $megjelenit_altalanos;

        $megjelenit_altalanos = array(
            'fixfilter'   => isset($admin['fixfilter']) ? $admin['fixfilter'] : 0
        );
        $this->data['fixfilter'] = $megjelenit_altalanos;

        $megjelenit_altalanos = array(
            'cookie_figyelmeztetes'   => isset($admin['cookie_figyelmeztetes']) ? $admin['cookie_figyelmeztetes'] : 0
        );
        $this->data['cookie_figyelmeztetes'] = $megjelenit_altalanos;

        $megjelenit_altalanos = array(
            'megjelenit_popup_eredeti_meret'   => isset($admin['megjelenit_popup_eredeti_meret']) ? $admin['megjelenit_popup_eredeti_meret'] : 0
        );
        $this->data['megjelenit_popup_eredeti_meret'] = $megjelenit_altalanos;

        $megjelenit_altalanos = array(
            'mennyisegre_szur'   => isset($admin['mennyisegre_szur']) ? $admin['mennyisegre_szur'] : 0
        );
        $this->data['mennyisegre_szur'] = $megjelenit_altalanos;


        $megjelenit_altalanos = array(
            'ervenesseg_datumra_szur'   => isset($admin['ervenesseg_datumra_szur']) ? $admin['ervenesseg_datumra_szur'] : 0
        );
        $this->data['ervenesseg_datumra_szur'] = $megjelenit_altalanos;

        $megjelenit_altalanos = array(
            'no_kep_tiltas'   => isset($admin['no_kep_tiltas']) ? $admin['no_kep_tiltas'] : 0
        );
        $this->data['no_kep_tiltas'] = $megjelenit_altalanos;

        $this->data['emlekezz_ram_login'] = isset($admin['emlekezz_ram_login']) ? $admin['emlekezz_ram_login'] : 0;
        $this->data['facebook_login_fiok'] = isset($admin['facebook_login_fiok']) ? $admin['facebook_login_fiok'] : 0;
        $this->data['facebook_login_penztar'] = isset($admin['facebook_login_penztar']) ? $admin['facebook_login_penztar'] : 0;
        $this->data['cookie_figyelmeztetes'] = isset($admin['cookie_figyelmeztetes']) ? $admin['cookie_figyelmeztetes'] : 0;
        $this->data['penznem_hosszu_kiiras'] = isset($admin['penznem_hosszu_kiiras']) ? $admin['penznem_hosszu_kiiras'] : 0;
        $this->data['footer_popup'] = isset($admin['footer_popup']) ? $admin['footer_popup'] : 0;
        $this->data['sebesseg_nyomkovetes'] = isset($admin['sebesseg_nyomkovetes']) ? $admin['sebesseg_nyomkovetes'] : 0;
        $this->data['megjelenit_popup_eredeti_meret'] = isset($admin['megjelenit_popup_eredeti_meret']) ? $admin['megjelenit_popup_eredeti_meret'] : 0;


        /*Általános end*/

        /*email start*/

        if (isset($this->request->post['megjelenit_emailreg'])) {
            $admin = $this->request->post['megjelenit_emailreg'];

        } else {
            $admin = $this->config->get("megjelenit_emailreg");
        }

        $megjelenit_emailreg = array(
            'jelszo'   => isset($admin['jelszo']) ? $admin['jelszo'] : 0
        );
        $this->data['jelszo'] = $megjelenit_emailreg;

        /*email end*/

        /*termék product start*/

        if (isset($this->request->post['megjelenit_product'])) {
            $admin = $this->request->post['megjelenit_product'];

        } else {
            $admin = $this->config->get("megjelenit_product");
        }

        $megjelenit_product = array(
            'product_leiras'   => isset($admin['product_leiras']) ? $admin['product_leiras'] : 0
        );
        $this->data['product_leiras'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_koztes'   => isset($admin['product_koztes']) ? $admin['product_koztes'] : 0
        );
        $this->data['product_koztes'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_garancia'   => isset($admin['product_garancia']) ? $admin['product_garancia'] : 0
        );
        $this->data['product_garancia'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_szallitas'   => isset($admin['product_szallitas']) ? $admin['product_szallitas'] : 0
        );
        $this->data['product_szallitas'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_velemeny'   => isset($admin['product_velemeny']) ? $admin['product_velemeny'] : 0
        );
        $this->data['product_velemeny'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_osszehasonlit_kivansag'   => isset($admin['product_osszehasonlit_kivansag']) ? $admin['product_osszehasonlit_kivansag'] : 0
        );
        $this->data['product_osszehasonlit_kivansag'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_addthis'   => isset($admin['product_addthis']) ? $admin['product_addthis'] : 0
        );
        $this->data['product_addthis'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_uj_ikon'   => isset($admin['product_uj_ikon']) ? $admin['product_uj_ikon'] : 0
        );
        $this->data['product_uj_ikon'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_par_ikon'   => isset($admin['product_par_ikon']) ? $admin['product_par_ikon'] : 0
        );
        $this->data['product_par_ikon'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_szazalek_ikon'   => isset($admin['product_szazalek_ikon']) ? $admin['product_szazalek_ikon'] : 0
        );
        $this->data['product_szazalek_ikon'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_mennyisegi_ikon'   => isset($admin['product_mennyisegi_ikon']) ? $admin['product_mennyisegi_ikon'] : 0
        );
        $this->data['product_mennyisegi_ikon'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_velemeny_megosztas_doboz'   => isset($admin['product_velemeny_megosztas_doboz']) ? $admin['product_velemeny_megosztas_doboz'] : 0
        );
        $this->data['product_velemeny_megosztas_doboz'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_model_termeknevben'   => isset($admin['product_model_termeknevben']) ? $admin['product_model_termeknevben'] : 0
        );
        $this->data['product_model_termeknevben'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_cikkszam_termeknevben'   => isset($admin['product_cikkszam_termeknevben']) ? $admin['product_cikkszam_termeknevben'] : 0
        );
        $this->data['product_cikkszam_termeknevben'] = $megjelenit_product;

        $megjelenit_product = array(
            'product_gyarto_termeknevben'   => isset($admin['product_gyarto_termeknevben']) ? $admin['product_gyarto_termeknevben'] : 0
        );
        $this->data['product_gyarto_termeknevben'] = $megjelenit_product;



        // CSV Export - import
        $this->data['megjelenit_csv_export_import']                     = isset($this->request->post['megjelenit_csv_export_import']) ? $this->request->post['megjelenit_csv_export_import'] : $this->config->get('megjelenit_csv_export_import');
        $this->data['megjelenit_csv_export_import_termek']              = isset($this->request->post['megjelenit_csv_export_import_termek']) ? $this->request->post['megjelenit_csv_export_import_termek'] : $this->config->get('megjelenit_csv_export_import_termek');
        $this->data['megjelenit_csv_export_import_termek_termekek']     = isset($this->request->post['megjelenit_csv_export_import_termek_termekek']) ? $this->request->post['megjelenit_csv_export_import_termek_termekek'] : $this->config->get('megjelenit_csv_export_import_termek_termekek');
        $this->data['megjelenit_csv_export_import_termek_kapcsolatok']  = isset($this->request->post['megjelenit_csv_export_import_termek_kapcsolatok']) ? $this->request->post['megjelenit_csv_export_import_termek_kapcsolatok'] : $this->config->get('megjelenit_csv_export_import_termek_kapcsolatok');
        $this->data['megjelenit_csv_export_import_termek_valasztek']    = isset($this->request->post['megjelenit_csv_export_import_termek_valasztek']) ? $this->request->post['megjelenit_csv_export_import_termek_valasztek'] : $this->config->get('megjelenit_csv_export_import_termek_valasztek');
        $this->data['megjelenit_csv_export_import_termek_akcios']       = isset($this->request->post['megjelenit_csv_export_import_termek_akcios']) ? $this->request->post['megjelenit_csv_export_import_termek_akcios'] : $this->config->get('megjelenit_csv_export_import_termek_akcios');
        $this->data['megjelenit_csv_export_import_termek_mennyisegi']   = isset($this->request->post['megjelenit_csv_export_import_termek_mennyisegi']) ? $this->request->post['megjelenit_csv_export_import_termek_mennyisegi'] : $this->config->get('megjelenit_csv_export_import_termek_mennyisegi');
        $this->data['megjelenit_csv_export_import_termek_vevo_arak']    = isset($this->request->post['megjelenit_csv_export_import_termek_vevo_arak']) ? $this->request->post['megjelenit_csv_export_import_termek_vevo_arak'] : $this->config->get('megjelenit_csv_export_import_termek_vevo_arak');
        $this->data['megjelenit_csv_export_import_kategoriak']          = isset($this->request->post['megjelenit_csv_export_import_kategoriak']) ? $this->request->post['megjelenit_csv_export_import_kategoriak'] : $this->config->get('megjelenit_csv_export_import_kategoriak');
        $this->data['megjelenit_csv_export_import_jellemzo']            = isset($this->request->post['megjelenit_csv_export_import_jellemzo']) ? $this->request->post['megjelenit_csv_export_import_jellemzo'] : $this->config->get('megjelenit_csv_export_import_jellemzo');
        $this->data['megjelenit_csv_export_import_jellemzo_jellemzok']  = isset($this->request->post['megjelenit_csv_export_import_jellemzo_jellemzok']) ? $this->request->post['megjelenit_csv_export_import_jellemzo_jellemzok'] : $this->config->get('megjelenit_csv_export_import_jellemzo_jellemzok');
        $this->data['megjelenit_csv_export_import_jellemzo_csoportok']  = isset($this->request->post['megjelenit_csv_export_import_jellemzo_csoportok']) ? $this->request->post['megjelenit_csv_export_import_jellemzo_csoportok'] : $this->config->get('megjelenit_csv_export_import_jellemzo_csoportok');
        $this->data['megjelenit_csv_export_import_valasztek']           = isset($this->request->post['megjelenit_csv_export_import_valasztek']) ? $this->request->post['megjelenit_csv_export_import_valasztek'] : $this->config->get('megjelenit_csv_export_import_valasztek');
        $this->data['megjelenit_csv_export_import_valasztek_valasztek'] = isset($this->request->post['megjelenit_csv_export_import_valasztek_valasztek']) ? $this->request->post['megjelenit_csv_export_import_valasztek_valasztek'] : $this->config->get('megjelenit_csv_export_import_valasztek_valasztek');
        $this->data['megjelenit_csv_export_import_valasztek_ertek']     = isset($this->request->post['megjelenit_csv_export_import_valasztek_ertek']) ? $this->request->post['megjelenit_csv_export_import_valasztek_ertek'] : $this->config->get('megjelenit_csv_export_import_valasztek_ertek');
        $this->data['megjelenit_csv_export_import_valasztek_szintablazat']  = isset($this->request->post['megjelenit_csv_export_import_valasztek_szintablazat']) ? $this->request->post['megjelenit_csv_export_import_valasztek_szintablazat'] : $this->config->get('megjelenit_csv_export_import_valasztek_szintablazat');
        $this->data['megjelenit_csv_export_import_valasztek_szincsoport']   = isset($this->request->post['megjelenit_csv_export_import_valasztek_szincsoport']) ? $this->request->post['megjelenit_csv_export_import_valasztek_szincsoport'] : $this->config->get('megjelenit_csv_export_import_valasztek_szincsoport');
        $this->data['megjelenit_csv_export_import_valasztek_szinkapcsolat'] = isset($this->request->post['megjelenit_csv_export_import_valasztek_szinkapcsolat']) ? $this->request->post['megjelenit_csv_export_import_valasztek_szinkapcsolat'] : $this->config->get('megjelenit_csv_export_import_valasztek_szinkapcsolat');
        $this->data['megjelenit_csv_export_import_vevo']                = isset($this->request->post['megjelenit_csv_export_import_vevo']) ? $this->request->post['megjelenit_csv_export_import_vevo'] : $this->config->get('megjelenit_csv_export_import_vevo');
        $this->data['megjelenit_csv_export_import_vevo_vevok']          = isset($this->request->post['megjelenit_csv_export_import_vevo_vevok']) ? $this->request->post['megjelenit_csv_export_import_vevo_vevok'] : $this->config->get('megjelenit_csv_export_import_vevo_vevok');
        $this->data['megjelenit_csv_export_import_vevo_cimek']          = isset($this->request->post['megjelenit_csv_export_import_vevo_cimek']) ? $this->request->post['megjelenit_csv_export_import_vevo_cimek'] : $this->config->get('megjelenit_csv_export_import_vevo_cimek');
        $this->data['megjelenit_megrendeles_crm_integracio']            = isset($this->request->post['megjelenit_megrendeles_crm_integracio']) ? $this->request->post['megjelenit_megrendeles_crm_integracio'] : $this->config->get('megjelenit_megrendeles_crm_integracio');






        $megjelenit_product['tab_description']           = isset($admin['tab_description']) ? $admin['tab_description'] : 0;
        $megjelenit_product['tab_meret']                 = isset($admin['tab_meret']) ? $admin['tab_meret'] : 0;
        $megjelenit_product['tab_attribute_groups']      = isset($admin['tab_attribute_groups']) ? $admin['tab_attribute_groups'] : 0;
        $megjelenit_product['tab_review_status']         = isset($admin['tab_review_status']) ? $admin['tab_review_status'] : 0;
        $megjelenit_product['tab_products']              = isset($admin['tab_products']) ? $admin['tab_products'] : 0;
        $megjelenit_product['tab_test_description']      = isset($admin['tab_test_description']) ? $admin['tab_test_description'] : 0;
        $megjelenit_product['tab_image']                 = isset($admin['tab_image']) ? $admin['tab_image'] : 0;
        $this->data['megjelenit_product'] = $megjelenit_product;

        /*termék product  end*/

        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getInstalled('module');
        $no_module = $this->model_setting_extension->getNoModule();

        $this->data['extensions'] = array();

        $files = glob(DIR_APPLICATION . 'controller/module/*.php');

        if ($files) {
            foreach ($files as $file) {
                $extension = basename($file, '.php');
                if (!in_array($extension, $no_module)){

                    $this->load->language('module/' . $extension);

                    $this->data['extensions'][] = array(
                        'name'      => $this->language->get('heading_title'),
                        'filename'  => "nomodul_".$extension,
                        'action'    => 1
                    );
                } else {
                    $this->load->language('module/' . $extension);

                    $this->data['extensions'][] = array(
                        'name'   => $this->language->get('heading_title'),
                        'filename'  => "nomodul_".$extension,
                        'action' => 0
                    );
                }
            }
        }


        $files = glob(DIR_APPLICATION . 'controller/shipping/*.php');
        $this->data['extensions_shipping'] = array();

        if ($files) {
            foreach ($files as $file) {
                $extension = basename($file, '.php');
                if (!in_array($extension, $no_module)){

                    $this->load->language('shipping/' . $extension);

                    $this->data['extensions_shipping'][] = array(
                        'name'      => $this->language->get('heading_title'),
                        'filename'  => "nomodul_".$extension,
                        'action'    => 1
                    );
                } else {
                    $this->load->language('shipping/' . $extension);

                    $this->data['extensions_shipping'][] = array(
                        'name'   => $this->language->get('heading_title'),
                        'filename'  => "nomodul_".$extension,
                        'action' => 0
                    );
                }
            }
        }

        $this->data['extensions_payment'] = array();

        $files = glob(DIR_APPLICATION . 'controller/payment/*.php');

        if ($files) {
            foreach ($files as $file) {
                $extension = basename($file, '.php');
                if (!in_array($extension, $no_module)){

                    $this->load->language('payment/' . $extension);

                    $this->data['extensions_payment'][] = array(
                        'name'      => $this->language->get('heading_title'),
                        'filename'  => "nomodul_".$extension,
                        'action'    => 1
                    );
                } else {
                    $this->load->language('payment/' . $extension);

                    $this->data['extensions_payment'][] = array(
                        'name'   => $this->language->get('heading_title'),
                        'filename'  => "nomodul_".$extension,
                        'action' => 0
                    );
                }
            }
        }


        $this->template = 'design/megjelenito.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
  	}


    private function headerRendez($tomb) {

        $gyujto = array();
        foreach($tomb as $key=>$value) {
            $gyujto[] = array(
                'kulcs' => ''.$key,
                'ertek' => $value
            );
        }
        $gyujto = $this->config->rendezes($gyujto,"kulcs");
        $vissza = array();
        foreach($gyujto as $value) {
            $value['ertek']['templates'] = $this->config->rendezes($value['ertek']['templates'],"sort_order");
            $vissza[$value['kulcs']] = $value['ertek'];

        }
        return $vissza;

    }
}

?>