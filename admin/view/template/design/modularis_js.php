<script>

jQuery(function($) {
    xOffset = 0;
    yOffset = 0;
    $(document).mousemove(function (e) {
        xOffset = e.pageX;
        yOffset = e.pageY;
    });
});

$(document).ready(function() {
    modulFigyelo();
});

function addCsoport(tombatad){
    var html = '<div class="csoportok sortitem" id="';
        var new_div_id = "csoport";
        var tomb = "[";
        for(i in tombatad){
            tomb += tombatad[i]+",";
            new_div_id += '_'+tombatad[i];
        }
        for (var kii=0; $("#"+new_div_id+"_"+kii).length > 0; kii++){
        }
        var old_div_id = new_div_id;
        new_div_id += '_'+kii;
        tomb += kii+"]";

        html += new_div_id+'"';

        html += ' style="padding-left: '+new_div_id.length+'px; top:0px;">';
        html += ' <span class="sorthandle"><img src="view/image/icon-drag-drop.png"></span>';

        html += '<div class="csoportok_nyito"  onclick="csikiCsuki(this)">';
            var paratlan = true;
            for(i in tombatad){
                html += "<span>";
                if (paratlan) {
                    html += "Sor: ";
                    paratlan = false;
                } else {
                    html += "Oszl.: ";
                    paratlan = true;
                }
                html += '<b>'+tombatad[i]+'</b>';
                html += '</span>';
            }
            html += '<span>';
            if (paratlan) {
                html += "Sor: ";
            } else {
                html += "Oszl.: ";
            }
            html += '<b>'+kii+'</b>';
            html += "</span>";

            var new_kulcs = new_div_id.substr(8);
            var new_kulcs_past = "\'"+new_div_id.substr(8)+"\'";
            new_div_id = "#"+new_div_id;

            html += '<a onclick="$(\''+new_div_id+'\').remove();" class="button"><?php echo $button_remove ?></a>';
            html += '<a onclick="addCsoport('+tomb+')" class="button">Új csoport</a>';

            if (typeof(tarolo) == "object" && tarolo.length > 0) {
                html += '<a onclick="addPaste('+new_kulcs_past+')" class="button beillesztes" style="display: inline">Beillesztés</a>';
            } else {
                html += '<a onclick="addPaste('+new_kulcs_past+')" class="button beillesztes" >Beillesztés</a>';
            }

            html += '<a style="text-decoration: none; margin-right: 40px;">';
            html += 'Állapot: ';
            html += '<input type="radio" name="<?php echo $modul_name?>['+new_kulcs+'][default][status]" value="1" />';
            html += '<input type="radio" name="<?php echo $modul_name?>['+new_kulcs+'][default][status]" value="0" checked="checked"/>';
            html += '</a>';
            html += '<a class="input_tol_ig">Felbontás (tól -ig):';
            html += '<input type="text" name="<?php echo $modul_name?>['+new_kulcs+'][default][width_tol]" size="3" value="0"/>- ';
            html += '<input type="text" name="<?php echo $modul_name?>['+new_kulcs+'][default][width_ig]" size="3" value="3000" />';
            html += '</a>';
            html += '<a class="tartalom" style="margin-right: 36px;"></a>';
        html += '</div>';


            html += '<div class="csoport_beallitasok">';
            html += '<div>';
            html += '<div class="scrollbox">';
                <?php $class = 'odd'; ?>
                <?php foreach ($csoportok_files as $fileskey=>$beallitdoboz) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    html += '<div class="<?php echo $class; ?>">';
                        //html += '<span class="template_neve">';
                            html += '<input class="valaszto" onclick="elrejt(this);" type="checkbox" name="<?php echo $modul_name?>['+new_kulcs+'][templates][<?php echo $fileskey?>][status]" value="1" />';
                            html += '<span class="szoveg"><?php echo $fileskey; ?></span>'
                        //html += '</span>';

                        <?php if (count($beallitdoboz) > 0) {?>
                            <?php
                            $key = $fileskey;
                            $melyik_doboz = "<?php echo $modul_name?>[NULL_119][templates]"
                            ?>
                            html += '<span>';
                                <?php $disabled = "disabled"?>

                                <?php foreach ($beallitdoboz as $keydoboz=>$tomb) { ?>
                                    <?php if(count($tomb) > 1) { ?>
                                        html += '<?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>';
                                    <?php } ?>
                                <?php } ?>
                            html += '</span>';
                        <?php } ?>
                        html += '<input disabled class="sorrend" type="text" name="<?php echo $modul_name?>['+new_kulcs+'][templates][<?php echo $fileskey?>][sort_order]" value="0" size="2"/>';
                    html += '</div>';
                <?php } ?>
                html += '</div>';

                html += '<br>';
                html += '<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);$(this).parent().find(\'input, select\').attr(\'disabled\',false)"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false); $(this).parent().find(\'input, select\').attr(\'disabled\',true); $(\'.valaszto\').attr(\'disabled\',false)"><?php echo $text_unselect_all; ?></a>';
                html += '<br>';
            html += '</div>';

            html += '<div class="kijeloles">';
                if (typeof(tarolo) == "object" && tarolo.length > 0) {
                    html += '<a onclick="addCopy('+new_kulcs_past+');" class="button masolas" style="display:none">Kijelölés másolásra</a>';
                } else {
                    html += '<a onclick="addCopy('+new_kulcs_past+');" class="button masolas">Kijelölés másolásra</a>';
                }


                html += '<span> <b>CSS display érték:</b> (pl.: inline-block: )';
                    html += '<input type="text" name="<?php echo $modul_name?>['+new_kulcs+'][default][display]" size="8" value="block"/>';
                html += '</span>';
                html += '<span> <b>CSS class neve:</b>';
                    html += '<input type="text" name="<?php echo $modul_name?>['+new_kulcs+'][default][class]" size="18" value=""/>';
                html += '</span>';
            html += '</div>';
        html += '</div>';
    html += '</div>';

    debugger;

    html = html.replace(/NULL_119/g, new_kulcs);
    if ( $("#"+old_div_id).length > 0) {
        if ($("#"+old_div_id+" > .div_csoport").length == 0) {
            $("#"+old_div_id).append('<div class="div_csoport" style="position: relative"></div>');
        }
        $("#"+old_div_id+" > .div_csoport").append(html);
        $(new_div_id).vSort();
    } else {
        $("#<?php echo isset($hozzafuz) ? $hozzafuz : 'racs_altalanos'?>").append(html);
    }

    modulFigyelo();



}


function copyRemove(){
    $(".beillesztes").css("display","none");
    $(".masolas").fadeIn(300);
    $(".copyremove").fadeOut(300);
    tarolo = new Array();
}

function addCopy(para){
    var eredmeny = $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[checked=checked],[type=text]');
    tarolo = new Array();

    for (var i=0; eredmeny.length > i; i++){
        tarolo[i] = eredmeny[i].name;
    }
    tarolo[i]   = $("#csoport_"+para+ " > .csoport_beallitasok .kijeloles > span:nth(0) > input").val();
    tarolo[i+1] = $("#csoport_"+para+ " > .csoport_beallitasok .kijeloles > span:nth(1) > input").val();


    $(".masolas").css("display","none");
    $("#csoport_"+para+ " > .csoportok_nyito").css("color","blue");
    $(".beillesztes").fadeIn(300);
    $("#csoport_"+para+ " > .csoportok_nyito > .beillesztes").css("display","none");
    $('.csoport_beallitasok').slideUp(300);

    $("#copyremove_"+para).fadeIn(300);
}

function addPaste(para){

    var eredmeny = $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input");
    for (var i=0; eredmeny.length > i; i++){
        for (var j=0; tarolo.length-1 > j; j++) {
            var type = $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("type");
            if (eredmeny[i].name.substr(eredmeny[i].name.indexOf("]")+1) == tarolo[j].substr(tarolo[j].indexOf("]")+1)) {

                if (type == "checkbox"){
                    $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("checked",true);
                } else if(type == "text") {

                    var tarolt_kulcs    = tarolo[j].substring(tarolo[j].indexOf("[")+1,tarolo[j].indexOf("]"));

                    var ertek = $("#csoport_"+tarolt_kulcs+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+tarolo[j]+'"]').val();
                    $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("value",ertek);
                }
                break;

            } else {
                if (type == "checkbox"){
                    $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("checked",false);
                }
            }
        }
    }

    $("#csoport_"+para+ " > .csoport_beallitasok .kijeloles > span:nth(0) > input").val(tarolo[tarolo.length-2]);
    $("#csoport_"+para+ " > .csoport_beallitasok .kijeloles > span:nth(1) > input").val(tarolo[tarolo.length-1]);


    $(".beillesztes").css("display","none");
    $(".copyremove").css("display","none");

    $(".masolas").fadeIn(300);
    $(".csoportok_nyito").css("color","#000");

    if ($("#csoport_"+para+ " > .csoport_beallitasok").css("display") == "none") {
        $('.csoport_beallitasok').css("display","none");
        $("#csoport_"+para+ " > .csoport_beallitasok").slideDown(500);
    }
    tarolo = new Array();
}

function csikiCsuki(para){
    $('.fogalom_magyarazo').remove();

    if (para.id == "sablon") {
        if ($(para).next().css("display") != "none") {
            $(para).next().fadeOut("fast");
            $(para).removeClass("active");

        } else {
            $('.csoportok_nyito').removeClass("active");
            $('.csoport_beallitasok').hide();
            $(para).next().fadeIn("slow");
            $(para).addClass("active");
        }
    } else {

        if (this.event.target == para || this.event.target.localName == "span") {
            if ($(para).next().css("display") != "none") {
                $(para).next().slideUp("slow");
                $(para).removeClass("active");

            } else {
                $('.csoportok_nyito').removeClass("active");
                $('.csoport_beallitasok').hide();
                $(para).next().slideDown("slow");
                $(para).addClass("active");
            }
        }
    }
}

function elrejt(para) {
    var sort_name = para.name.substr(0,para.name.length-8)+'[sort_order]';

    if ($(para).prop("checked")) {
        $(".scrollbox input[name='"+sort_name+"']").parent().find("input, select").attr("disabled",false)

    } else {
        $(".scrollbox input[name='"+sort_name+"']").parent().find("input, select").attr("disabled",true)
    }
    $(para).attr("disabled",false);
    if ($(para).parent().parent().find('input:checked.valaszto').length > 0) {
        if ($(para).parent().parent().parent().parent().parent().find('> .csoportok_nyito > .tartalom > img').length == 0) {
            $(para).parent().parent().parent().parent().parent().find('> .csoportok_nyito > .tartalom').append('<img src="view/image/tartalom1.png">');
        }
    } else {

        $(para).parent().parent().parent().parent().parent().find('> .csoportok_nyito > .tartalom > img').remove();
    }
}


function modulFigyelo() {

    $('.csoportok_nyito .tartalom').mouseover(function(e){
        $('.fogalom_magyarazo').remove();

        a=$(this).parent().next('.csoport_beallitasok');
        c=$(a).children().children('.scrollbox');
        d=$(c).find('div input.valaszto:checked');

            $(document).mousemove(function (e) {
                xOffset = e.pageX;
                yOffset = e.pageY;
            });
        template = [];
        i = 0;
        $(d).each(function(element){
            template[i] = $(this).parent().find('.szoveg').html();
            i++;
        });

        if (template.length > 0) {
            var html = '<div style="top: '+(yOffset-120)+'px; left: '+(xOffset-200)+'px; display: none;" class="fogalom_magyarazo" >';
            //var html = '<div style="top: ' +(yOffset-120)+'px; left:0; display: none;" class="fogalom_magyarazo" >';
            html += '<div class="fogalom_fejlec">';
            html += '<span class="fogalom_name" style="">Bejelölt template-ek</span>';
            html += '</div>';

            html += '<div class="fogalom">';
            for(i=0; template.length > i; i++) {
                html += template[i];
                html += '<br>';
            }
            html += '</div>';
            html += '</div>';

            $("body").append(html);

            var ossz_height = $(window).height()+$(window).scrollTop();
            var ossz_width = $(window).width()+$(window).scrollLeft();

            if ( (ossz_height-yOffset) < $(".fogalom_magyarazo").height()) {
                yOffset -= $(".fogalom_magyarazo").height();
                $(".fogalom_magyarazo").css("top",(yOffset)+'px');
            }

            if ( (ossz_width-xOffset) < $(".fogalom_magyarazo").width()) {
                xOffset -= $(".fogalom_magyarazo").width();
                $(".fogalom_magyarazo").css("left",(xOffset-50)+'px');
            }
            //$('.fogalom_magyarazo').slideDown(100);
            $('.fogalom_magyarazo').css("display","block");
        }
    });

    $('.csoportok_nyito .tartalom img').mouseleave(function(){
        $('.fogalom_magyarazo').css("display","none");
        $('.fogalom_magyarazo').remove();



    });

}
</script>


<script>
    $("#megmutat").bind("click",function(){
        $('#elonezeti_kep').remove();

        var datatombatad= $('input[type=\'text\']:enabled, input[type=\'hidden\'], input[type=\'radio\']:checked, input[type=\'checkbox\']:checked, select:enabled, textarea');
        $.ajax({
            type: 'post',
            data : datatombatad,
            url: 'index.php?route=design/<?php echo $megjelenito?>/&elonezet=1&token=<?php echo $token; ?>',
            dataType: 'json',
            success: function(json){
                var html1  = '<div id="elonezeti_kep" style="width: 80%; ">';
                html1 += '<iframe src="<?php echo HTTP_CATALOG?>index.php?route=product/'+root+'&elonezet=1<?php echo !empty($nezet) ? '&'.$nezet.'=1' : ''?>"';
                html1 += ' style="width: 100%; height: 100%"';
                html1 += ' marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" >';
                html1 += ' </iframe>';
                html1 += '</div>';

                $("#content").before(html1);
                $(function() {
                    var wWidth = $(window).width();
                    var dWidth = wWidth * 0.8;
                    $( "#elonezeti_kep" ).dialog({
                        autoOpen: true,
                        autoResize: false,
                        maxHeight: 1500,
                        width: dWidth,
                        height: <?php echo !empty($height) ? $height : 350?>,
                        modal: true,
                        title: 'Előnézet',
                        position: ['center',20],
                        close: function() {
                        }
                    });
                });

            },
            error: function(e){
                debugger;

            }
        });


    });



    $(document).bind("click",function(e) {
        if (e.target.className == 'ui-widget-overlay') {
            iframeBezar();
        }

    });


    function iframeBezar() {
        $('.ui-dialog').slideUp("slow",function(){
            $('.ui-dialog').remove();
        });
    }

    setTimeout(" $('.warning, .success').slideUp(500)",5000);

    function sablonValaszt() {
        var datatombatad= $('input[type=\'text\']:enabled, input[type=\'hidden\'], input[type=\'radio\']:checked, input[type=\'checkbox\']:checked, select:enabled, textarea');
        debugger;

        $.ajax({
            type: 'post',
            data : datatombatad,
            url: 'index.php?route=design/<?php echo $megjelenito?>&sablonvalaszto=1&token=<?php echo $token; ?>',
            dataType: 'html',
            success: function(html){
                $('#<?php echo $hozzafuz?>').html(html);
                modulFigyelo();


            },
            error: function(e){
                debugger;

            }
        });

    }

    $('#filter_name').autocomplete({
        delay: 0,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.name,
                            value: item.product_id
                            //value: item.name
                        }
                    }));
                }
            });
        },
        select: function(event, ui) {
            $('#filter_name').val(ui.item.label);
            $('#filter_product_id').val(ui.item.value);
            root = "product&product_id="+ui.item.value;

            return false;
        }
    });

</script>

<style>
    .ui-widget-overlay {
        background: #555555;
        opacity: 0.3;
    }
    .ui-widget-header {
        background: #0086ba;
        border: none;
        border-radius: 0;
        text-align: left;
    }
    .ui-dialog {
        padding: 0;
        box-shadow: 2px 2px 5px #4D4D4D;
    }
    .ui-dialog .ui-dialog-titlebar {
        padding: 2px 0 3px 3px;
    }
    .ui-dialog .ui-dialog-title {
        float: none;
    }
    .ui-dialog .ui-dialog-content {
        padding: 0;
        overflow: hidden;
    }
    .ui-widget-content {
        background: #fff;
    }
    .ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
        border-radius: 0;
    }
    .ui-dialog .ui-dialog-titlebar-close {
        right: 0;
    }
</style>