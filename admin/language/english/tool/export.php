<?php
// Heading
$_['heading_title']     = 'Export / Import';

// Text
$_['text_success']      = 'Success: You have successfully imported your categories and products!';

// Entry
$_['entry_restore']     = 'Import from spreadsheet file:';
$_['entry_description'] = 'Use this function to export or import all your categories and products to or from a XLS spreadsheet file.';

// Error
$_['error_permission']                  = 'Warning: You do not have permission to modify exports/imports!';
$_['error_upload']                      = 'Uploaded file is not a valid spreadsheet file or its values are not in the expected formats!';
$_['error_not_upload']                  = 'Not uploaded XLS spreadsheet! (Choose file)';
$_['error_sheet_count']                 = 'Export/Import: Invalid number of worksheets, 7 worksheets expected';
$_['error_categories_header']           = 'Export/Import: Invalid header in the Categories worksheet';
$_['error_products_header']             = 'Export/Import: Invalid header in the Products worksheet';
$_['error_options_header']              = 'Export/Import: Invalid header in the Options worksheet';
$_['error_attributes_header']           = 'Export/Import: Invalid header in the Attributes worksheet';
$_['error_specials_header']             = 'Export/Import: Invalid header in the Specials worksheet';
$_['error_discounts_header']            = 'Export/Import: Invalid header in the Discounts worksheet';
$_['error_rewards_header']              = 'Export/Import: Invalid header in the Rewards worksheet';
$_['warning_download_limit_tol']        = 'Invalid initial value! There is not so much the product ...';
$_['warning_download_limit_ig_kisebb']  = 'Invalid final value! The final value should not be less than the start! (Or do not give a value for the final...)';

// Button labels
$_['button_import']     = 'Import';
$_['button_export']     = 'Export';
?>