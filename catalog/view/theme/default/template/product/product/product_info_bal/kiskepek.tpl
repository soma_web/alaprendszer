<script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>

<link rel="stylesheet" type="text/css" href="catalog/view/javascript/bxslider/jquery.bxslider.css" />
<script type="text/javascript">
    $(document).ready(function(){
        $('.bxslider').bxSlider({
            minSlides: 4,
            maxSlides: 4,
            slideWidth: <?php echo $this->config->get('config_image_additional_width'); ?>,
            slideMargin: 5
        });
    });


</script>
<div class="image-additional">
    <ul class="bxslider">
        <?php foreach ($images as $image) { ?>
            <li>
                <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="fancy" ref="fancy-pics" data-fancybox-group="gallery-1" ><img  src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
            </li>
        <?php } ?>
    </ul>
</div>