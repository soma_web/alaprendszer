<?php

$old_query  = new Mysql_Lekeres(DB_HOSTNAME, DB_USERNAME,DB_PASSWORD, OLD_DB_DATABASE);
$query      = new Mysql_Lekeres(DB_HOSTNAME, DB_USERNAME,DB_PASSWORD, DB_DATABASE, true);

$old_database = new ModelOldDatabaseTabla($old_query);
$old_database->tablaBeolvas();

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $letoltott = array();
            /* L-ekéri a régi adatbázisból a táblákat, végig megy rajta és ha a tábla neve nincs benne a postban, akkor létrehozza az sql file-t és a fileok elérését eltárolja egy tömbben. */
            foreach($old_database->data['table'] as $old_tabla=>$table_data) {
                if ( array_key_exists($old_tabla,$_POST) ) {
                    $file = CSV_UTVONAL . OLD_DB_PREFIX . $old_tabla.'.sql';
                    $letoltott[] = $file;

                    $sql = "SELECT * INTO OUTFILE '".$file."' FROM `". OLD_DB_PREFIX . $old_tabla. "`";
                    $result = $old_query->query($sql);
                }
            }


            /* 2 */
            foreach($old_database->data['table'] as $old_tabla=>$table_data) {

                /* A tábla eldobása az új adatbázisból */
                /* Kezdődik */
                $sql = "DROP TABLE  IF EXISTS`" . DB_PREFIX . $old_tabla. "`";
                $result = $query->query($sql);
                /* Vége */

                /* A régi adatbázisban szereplő tábla lemásolása és létrehozása az új adatbázisban */
                /* Kezdődik */
                $sql = "CREATE TABLE if not exists `" . DB_PREFIX . $old_tabla. "` (";
                $kulcs = array();
                $mezok = array();
                foreach($table_data as $mezo) {
                    if ($mezo['Key']=='PRI') {
                        $kulcs[] = "`".$mezo['Field']."`";
                    } elseif ($mezo['Key']) {
                        echo '';
                    }
                    $mezok[] = '`'.$mezo['Field'].'` '.$mezo['Type'].' '.($mezo['Null']=='NO' ? " NOT NULL " : '').' '.($mezo['Extra']=='auto_increment' ? ' AUTO_INCREMENT ' : '');
                }
                $sql .= implode(',',$mezok);
                $sql .= $kulcs ? ', PRIMARY KEY ('.implode(',',$kulcs).') ' : '';
                $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8";
                $result = $query->query($sql);
                /* Vége */

                /* Az sql fileokból feltölti a táblát a régi adatbázisban tárolt adatokkal és törli az sql fileokat*/
                /* Kezdődik */
                if ( array_key_exists($old_tabla,$_POST) ) {
                    $file = CSV_UTVONAL . OLD_DB_PREFIX . $old_tabla.'.sql';

                    $sql = "LOAD DATA INFILE '".$file."' INTO TABLE `".OLD_DB_PREFIX . $old_tabla."`";
                    $result = $query->query($sql);
                    unlink($file);

                }
                /* Vége */
            }
        }


?>