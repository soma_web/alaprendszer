<?php
// Heading
$_['heading_title']     = 'Gyakran ismételt kérdések';

// Text
$_['text_search']    = 'Keresés:';
$_['text_resetSearch']    = 'Keresés törlése';
$_['text_results']    = 'Találatok';
$_['text_noResults']    = 'Nincs találat...';
$_['text_descriptionNoResults']    = 'A keresés nem hozott találatot...';
$_['text_categoryEmpty']    = 'A kategória üres...';

?>
