<?php

// Heading
$_['heading_title']       = 'Educational attainment';
$_['button_insert']       = 'Insert';
$_['button_delete']       = 'Delete';
$_['error_warning']       = '';
$_['success']             = '';
$_['column_vegzettseg']      = 'Educational attainment';
$_['column_sort_order']   = 'Order';
$_['column_action']       = 'Operations';

?>