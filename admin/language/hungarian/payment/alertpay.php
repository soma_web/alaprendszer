<?php

// Heading
$_['heading_title']      = 'AlertPay';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Sikeresen módosította az Alertpay-jel történő fizetést!';

// Entry
$_['entry_merchant']     = 'Kereskedő ID:';
$_['entry_security']     = 'Biztonsági kód:';
$_['entry_callback']     = 'Figyelmeztetési URL:<br /><span class="help">Ezt a FizetésiFigyelmeztetés vezérlő paneljében kell beállítani. Valamint az "IPN Státusz"-t engedélyezettre kell állítania.</span>';
$_['entry_total']        = 'Összeg:<br /><span class="help">A pénztárban a rendelés értékének el kell érnie ezt az összeget, hogy aktívvá váljon.</span>';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';

// Error
$_['error_permission']   = 'Figyelmeztetés: Az AlertPay történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_merchant']     = 'Kereskedő ID szükséges!';
$_['error_security']     = 'Biztonsági kód szükséges!';
?>
