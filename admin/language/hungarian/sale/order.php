<?php

// Heading
$_['heading_title']         = 'Rendelések';

// Text
$_['text_success']          = 'Siker! Módosította a rendeléseket!';
$_['text_order_id']           = 'Rendelésszám:';
$_['text_invoice_no']         = 'Számlaszám.:';
$_['text_invoice_date']       = 'Számla dátuma:';
$_['text_store_name']         = 'Áruház neve:';
$_['text_store_url']          = 'Áruház weboldala:';
$_['text_customer']           = 'Ügyfél:';
$_['text_customer_group']     = 'Ügyfélcsoport:';
$_['text_email']              = 'E-Mail:';
$_['text_ip']                 = 'IP cím:';
$_['text_telephone']          = 'Telefon:';
$_['text_fax']                = 'Fax:';
$_['text_shipping_method']    = 'Szállítási mód:';
$_['text_payment_method']     = 'Fizetési mód:';
$_['text_total']              = 'Összesen:';
$_['text_reward']             = 'Jutalompontok:';
$_['text_order_status']       = 'Rendelés állapota:';
$_['text_comment']            = 'Megjegyzés:';
$_['text_affiliate']          = 'Partner:';
$_['text_commission']         = 'Jutalék:';
$_['text_date_added']         = 'Rendelés dátuma:';
$_['text_date_modified']      = 'Módosítás dátuma:';
$_['text_firstname']          = 'Keresztnév:';
$_['text_lastname']           = 'Vezetéknév:';
$_['text_company']            = 'Cég:';
$_['text_adoszam']            = 'Adószám:';
$_['text_address_1']          = 'Utca, házszám:';
$_['text_address_2']          = 'Kiegészítő címinformációk:';
$_['text_postcode']           = 'Iránytószám:';
$_['text_city']               = 'Város:';
$_['text_tranzoflex']         = 'Tranzo-flex';
$_['text_gls_pont']           = 'GLS Csomagpont';
$_['text_choose']             = '-- Válassz --';


$_['text_rendeles_szam']      = 'Rendellés szám (tól-ig):';
$_['text_rendeles_datum']     = 'Dátum (tól-ig):';
$_['text_letoltes']           = 'Letöltés';




$_['text_mindent_jelol']      = 'Mindent kijelöl';
$_['text_mindent_vissza']     = 'Kijelölés visszavonása';
$_['text_checkall']           = 'Be';
$_['text_uncheckall']         = 'Ki';
$_['text_noneselectedtext']   = '--- Válasszon ---';
$_['text_selectedtext']       = '- Kiválasztva';
$_['entry_vonalkod_kijeloles']  = 'Összes utalvány endedélyezése / tiltása:';
$_['text_selectedtext_engedelyetve'] = 'Az összes utalvány letöltés: engedélyezve';
$_['text_selectedtext_tiltva']       = 'Az összes utalvány letöltés: tiltva';

$_['entry_eletkor']           = 'Életkor:';
$_['entry_nem']               = 'Nem:';

$_['text_nem']                = 'Nem:';
$_['text_eletkor']            = 'Életkor:';
$_['text_ferfi']              = 'Férfi:';
$_['text_holgy']              = 'Nő:';

$_['text_zone']               = 'Megye / Állam:';
$_['text_zone_code']          = 'Megye / Állam kód:';
$_['text_country']            = 'Ország:';
$_['text_download']           = 'Rendelés letöltések';
$_['text_invoice']            = 'Megrendelés összesítés';
$_['text_to']                 = 'Címzett';
$_['text_ship_to']            = 'Szállítási cím (Ha eltér a szmlázási címtől)';
$_['text_abandoned_orders']   = 'Törölt rendelések';
$_['text_default']            = 'Alapértelmezett';
$_['text_wait']               = 'Kérem várjon!';
$_['text_create_invoice_no']  = 'Számlaszám készítés.';
$_['text_reward_add']         = 'Jutalompont hozzáadás';
$_['text_reward_added']       = 'Sikeresen hozzáadta a jutalompontokat!';
$_['text_reward_remove']      = 'Jutalompontok törlése';
$_['text_reward_removed']     = 'Sikeresen törölte a jutalompontokat!';
$_['text_commission_add']     = 'Jutalék hozzáadása';
$_['text_commission_added']   = 'Sikeresen hozzáadta a jutalékot!';
$_['text_commission_remove']  = 'Jutalék törlése';
$_['text_commission_removed'] = 'Sikeresen törölte a jutalékot!';
$_['text_credit_add']         = 'Kredit hozzáadása';
$_['text_credit_added']       = 'Sikeresen hozzáadta a kreditet!';
$_['text_credit_remove']      = 'Kredit törlése';
$_['text_credit_removed']     = 'Sikeresen törölte a fiók kreditjét!';
$_['text_upload']             = 'Sikeresen feltöltötte a fájlt!';
$_['text_user_agent']         = 'Böngésző:';
$_['text_accept_language']    = 'Böngésző nyelv:';
$_['text_generate']           = 'Generálás';

// Column
$_['column_order_id']         = 'Rendelésszám';
$_['column_customer']         = 'Ügyfél';
$_['column_status']           = 'Állapot';
$_['column_date_added']       = 'Rendelés dátuma';
$_['column_date_modified']    = 'Módosítás dátuma';
$_['column_total']            = 'Összesen';
$_['column_product']          = 'Termék';
$_['column_model']            = 'Modell';
$_['column_quantity']         = 'Mennyiség';
$_['column_price']            = 'Egységár';
$_['column_download']         = 'Letöltés neve';
$_['column_filename']         = 'Fájlnév';
$_['column_remaining']        = 'Fennmaradt letöltések';
$_['column_comment']          = 'Megjegyzés';
$_['column_notify']           = 'Ügyfél bejelentései';
$_['column_action']           = 'Művelet';
$_['column_utalvany']         = 'Utalvány letölthető';

// Entry
$_['entry_shipping_uzlet']    = 'Átvétel helye:';
$_['entry_store']             = 'Áruház:';
$_['entry_customer']          = 'Ügyfél:';
$_['entry_customer_group']    = 'Ügyfélcsoport:';
$_['entry_firstname']         = 'Keresztnév:';
$_['entry_lastname']          = 'Vezetéknév:';
$_['entry_email']             = 'E-Mail:';
$_['entry_telephone']         = 'Telefon:';
$_['entry_fax']               = 'Fax:';
$_['entry_address']           = 'Válassza ki a címet:';
$_['entry_company']           = 'Cég:';
$_['entry_adoszam']           = 'Adószám:';
$_['entry_address_1']         = 'Utca házszám:';
$_['entry_address_2']         = 'Kiegészítő címinformációk:';
$_['entry_city']              = 'Város:';
$_['entry_postcode']          = 'Irányítószám:';
$_['entry_country']           = 'Ország:';
$_['entry_zone']              = 'Megye / Állam:';
$_['entry_zone_code']         = 'Megye / Állam kód:';
$_['entry_product']           = 'Válasszon terméket:';
$_['entry_option']            = 'Válasszon a lehetősékeg közül:';
$_['entry_quantity']          = 'Mennyiség:';
$_['entry_affiliate']         = 'Partner:';
$_['entry_order_status']      = 'Rendelés állapota:';
$_['entry_notify']            = 'Ügyfél értesítése:';
$_['entry_comment']           = 'Megjegyzés:';
$_['entry_shipping']          = 'Szállítási mód:';
$_['entry_payment']           = 'Fizetési mód:';
$_['entry_weblap']            = 'Weblap címe:';

$_['tab_voucher']             = 'Ajándékutalványok:';
$_['text_voucher']            = 'Ajándékutalvány hozzáadása:';

$_['entry_to_name']           = 'Címzett neve:';
$_['entry_to_email']          = 'Címzett e-mail címe:';
$_['entry_from_name']         = 'Feladó neve:';
$_['entry_from_email']        = 'Feladó  e-mail címe:';
$_['entry_theme']             = 'Téma:';
$_['entry_message']           = 'Üzenet:';
$_['entry_amount']            = 'Mennyiség:';
$_['button_add_voucher']      = 'Ajándékutalvány hozzáadása';
$_['text_product']            = 'Termék hozzáadása:';

$_['entry_coupon']            = 'Kupon:';
$_['entry_voucher']           = 'Ajándékutalvány:';
$_['entry_reward']            = 'Jutalom:';
$_['button_update_total']     = 'Összesítés frissítése';
$_['button_csv_export']       = 'Csv Export';
$_['button_xml_export_szamlazz']  = 'Számla készítése a számlázz.hu-n';
$_['select_ferfi']            = 'Férfi';
$_['select_holgy']            = 'Nő';

$_['entry_iskolai_vegzettseg']        = 'Iskolai végzettség';
$_['entry_vallalkozasi_forma']      = 'Vállalkozási forma:';
$_['entry_szekhely']                = 'Székhely:';
$_['entry_ugyvezeto_neve']          = 'Ügyvezető neve:';
$_['entry_ugyvezeto_telefonszama']  = 'Ügyvezető telefonszáma:';





// Error
$_['error_permission']        = 'Figyelmeztetés: Nincs engedélye a rendelés módosítására!';
$_['error_firstname']         = 'A keresztnév legalább 1 és legfeljebb 32 karakterből állhat!';
$_['error_lastname']          = 'A vezetéknév legalább 1 és legfeljebb 32 karakterből állhat!';
$_['error_email']             = 'Nem valós e-mail cím!';
$_['error_telephone']         = 'A telefonszám legalább 3 és legfeljebb 32 karakterből állhat!';
$_['error_password']          = 'A jelszó legalább 3 és legfeljebb 20 karakterből állhat!';
$_['error_confirm']           = 'A két jelszó nem azonos!';
$_['error_address_1']         = 'Az utca, házszám legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_city']              = 'A város legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_postcode']          = 'Az irányítószám legalább 2 és legfeljebb 10 karakterből állhat!';
$_['error_country']           = 'Kérem válasszon országot!';
$_['error_zone']              = 'Kérem válasszon megyét / államot!';
$_['error_upload']            = 'Fel kell tölteni!';
$_['error_filename']          = 'A fájlnév legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_filetype']          = 'Érvénytelen fájltípus!';
$_['error_action']            = 'Figyelem: A műveletet nem sikerült befejezni!';
$_['error_warning']           = 'A műveletet nem sikerült! Kérem, gondosan nézze át az űrlapot!';
$_['error_eletkor']           = 'Kérem válassza ki az életkorát';
$_['error_nem']               = 'Kérem válassza ki a nemét';


$_['error_kelt_datum']              = 'A számla kelte nem lehet korábbi, mint az utoljára elkészített számláé';
$_['error_letezo_rendeles_szam']    = 'Már létező rendelésszám ';
$_['error_mar_van_vegszamla_elso']       = 'Nem található előlegszámla ezzel a rendelésszámmal';
$_['error_mar_van_vegszamla_masodik']       = ', vagy már készült hozzá végszámla.';
$_['error_dijbekero_jovairas']       = 'Díjbekérőhöz jóváírás rögzítése nem lehetséges.';
$_['error_szamla_beolvasas']       = 'XML beolvasási hiba.';
$_['error_szamlaszam_elotag']       = 'Ez a számlaszám előtag nem létezik vagy nincs engedélyezve!';
$_['succes_szamla_feltoltes']       = 'A számla létrejött a szamlazz.hu adatbázisában!';

?>
