<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Hírlevél Készítés és Küldés';

// Entry 
$_['entry_template']						= 'Sablon:';
$_['entry_language']						= 'Nyelv:';
$_['entry_currency']						= 'Valuta:';
$_['entry_yes']								= 'Igen';
$_['entry_no']								= 'Nem';
$_['entry_defined']							= 'A kiválasztott termékek tartalmazzák:';
$_['entry_special']							= 'Speciális termékeket kínálnak:';
$_['entry_product']							= 'Termék hozzáadása:';
$_['entry_latest']							= 'Legutóbbi termékek:';
$_['entry_popular']							= 'Legnépszerűbb termékek:';
$_['entry_defined_categories']				= 'Termékek kategóriák szerint:';
$_['entry_attachments']						= 'Csatolmányok:';
$_['entry_marketing']						= 'Marketing listák:';
$_['entry_section_name']					= 'Kiválasztott termékek részleges neve:';
$_['entry_text_message']					= 'Egyszerű szöveg verzió:';
$_['entry_text_message_products']			= 'Link hozzáfűzése a webes verzióhoz:';

// Text
$_['text_clear_warning']					= '<span style="color:red;font-weight:bold;">Fontos!</span> Sablon, valuta és nyelv változtatásánál - a tárgy és az üzenet változtatásai elvesznek!';
$_['text_message_info']						= 'Tárgy és üzenet alakító mezők: <pre>{name}, {lastname}, {email}</pre> mezők lecserélődnek a meghatározott értékekre. <pre>{reward}</pre> mező csak a címzettnek "Jutalompontok: Összes/Feliratkozott vásárlók".';
$_['text_marketing']						= 'Összes Marketing Feliratkozó';
$_['text_marketing_all']					= 'Összes Marketing';
$_['text_subscriber_all']					= 'Összes Hírlevél Feliratkozó & Marketing Feliratkozók';
$_['text_all']								= 'Összes Vásárló & Marketing';
$_['text_throttle_sent']     				= '<img src="view/image/loading.gif" alt="" /> %d üzenet sorbanáll. Kérjük, várjon, míg a művelet nem ér véget!';
$_['text_sent']     						= '<img src="view/image/loading.gif" alt="" /> %d e-mail küldése. Kérjük, várjon, míg a művelet nem ér véget!';
$_['text_success_save']						= 'Siker: Hírlevél vázlatként sikeresen elmentve!';
$_['text_throttle_success']   				= 'Emailek hozzáadva a sorhoz. Ellenőrizni kéne a folyamatot statisztika résznél.';
$_['text_loading']							= 'Töltés...';
$_['text_success_check']					= 'A Hírlevél elküldve ide: <a href="http://isnotspam.com" target="_blank">isNotSpam.com</a>. A következő ellenőrzések fognak végrehajtódni: SPF Check, Sender-ID Check, DomainKeys Check, DKIM Check, SpamAssassin Check. Az eredmények megtekinthetőek: <a href="http://isnotspam.com/newlatestreport.php?email=%s" target="_blank">here</a>.';
$_['text_only_subscribers']					= 'Csak Feliratkozóknak';
$_['text_only_selected_language']			= 'Küldés csak azoknak a vevőknek akik ezt a nyevet választották. <span class="help">Ha a vevő nem választott elsődleges nyelvet és a jelenlegi nyelv az áruház alapértelmezett nyelve, akkor is fog kapni hírlevelet.</span>';
$_['text_rewards']							= 'Jutalom pontok: Feliratkozott vevők';
$_['text_rewards_all']						= 'Jutalom pontok: Összes vásárló';

// Button 
$_['button_add_file']						= 'Csatolmány hozzáadása';
$_['button_save']							= 'Mentés vázlatként';
$_['button_reset']							= 'Visszaállítás';
$_['button_back']							= 'Vissza';
$_['button_update']							= 'Mentés';
$_['button_preview']						= 'Előnézet';
$_['button_check']							= 'Jelöld be ha spam';
$_['button_add_defined_section']			= 'Új kiválasztott termékrész hozzáadása';
