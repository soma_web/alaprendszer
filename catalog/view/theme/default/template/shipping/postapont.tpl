<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&language=hu[GOOGELMAPSAPIKEY]&callback=initialize"></script>
<style>
#postapontvalasztoapi .button { padding: inherit; }
#postapontvalasztoapi select { margin: inherit; }
#postaPontApi-inner p, #postaPontApi-inner form {display:none;}
#postapontIrsz input {
    font-size: 13px;
    margin-left: 0;
    margin-right: 0;
    padding: 7px 8px 5px;
    width: 270px;
    margin-bottom: 3px;
    border-radius: 4px;
	height: 25px;
	border:1px solid #000;
}
</style>
<div style="display: none" id="lepedo">Be van töltve</div>

<div id="postapontLabel" >
    <label><span class="postapontContent">Kérem válasszon pontot, ha PostaPontra kéri a szállítást!</span>
        <span style="float: right; text-decoration: underline; display: none" id="postapont_bezar">Bezár</span>
        <span style="float: none" id="kivalasztott_postapont"></span>
    </label>
</div>
<div id="postapontContent">
<div id="postapontIrsz" style="width:100%;text-align:center" class="postapontContent"><br /><input type="hidden" id="valasztott_postapont" name="valasztott_postapont" value=""><b>Irányítószám</b><br /><input type="text" id="ugyfelform_iranyitoszam"></label></div>
<div id="postapontvalasztoapi"></div>
</div>
<script type="text/javascript">
	var ppapi = {};
	//var google = {};
	(function($) {
		ppapi = {
			APIURL: 'https://www.postapont.hu/ppapi/',
			map: null,
			pplist: null,
			ppmarkerslist: null,
			onSelect: null,
			opts: {},
			symbols: {},
			infoWindow: {},
			markers: [],
			skipgroups: {},
		
			linkZipField: function(zipfield_id){
				$('#'+zipfield_id).blur( function(){
					if( ppapi.zipcode == $(this).val() ) return;
					ppapi.zipcode = $(this).val();
					ppapi.initZip(ppapi.zipcode);
				});
				$('#'+zipfield_id).keypress( function(e){
					var code = e.which;
					if(code==13){
						ppapi.zipcode = $(this).val();
						ppapi.initZip(ppapi.zipcode);
						$(this).blur();
						return false;
					}
				});
				ppapi.zipcode = $('#'+zipfield_id).val();
				ppapi.initZip(ppapi.zipcode);
			},
		
			apicall: function(method, data, success){
				$.ajax({
					crossDomain: true,
					dataType: 'jsonp',
					cache:false,
					async:false,
					data: data,
					url: ppapi.APIURL+method,
					success: success,
					error: function(jqXHR, textStatus, errorThrown){
						alert('error: '+textStatus);
					}
				});

            },
		
			insertMap: function(field_id, opts){
				//console.log('insertMap start');
				ppapi.setOpts(opts);
				//console.log('insertMap setOpts');
				ppapi.apicall('insertMap', null, function(data, textStatus, jqXHR ){
					//console.log('insertMap apicall start');
					$('#'+field_id).html(data);
		
					$('#pp-select-button').click(function(){
						var ppselectobj = $('#pp-select-postapont');
						if( !ppapi.pplist || ppselectobj.get(0).selectedIndex<0) return;
						ppapi.selectPP(ppapi.pplist[ppselectobj.get(0).selectedIndex]);
					});
					//console.log('insertMap apicall end');
					ppapi.mapInitialize();
				});

            },
		
			initZip: function(zip){
				if(!zip) return;
		
				ppapi.apicall('geozip', {
					'zipCode': zip,
					}, function(data, textStatus, jqXHR ){
		
						if(data.type=='symbolzip'){
							ppapi.zipkrd = new google.maps.LatLng(data.lat, data.lon),
							ppapi.map.setCenter( ppapi.zipkrd );
							ppapi.reloadPP();
		
						} else if(data.type=='mapzip'){
							geocoder = new google.maps.Geocoder();
							geocoder.geocode( { 'address': zip+', Magyarország', 'region':'HU'}, function(results, status) {
								if (status == google.maps.GeocoderStatus.OK) {
									ppapi.zipkrd = results[0].geometry.location;
									ppapi.map.setCenter( ppapi.zipkrd );
									ppapi.reloadPP();
								} else {
									//alert('Geocode was not successful for the following reason: ' + status);
									alert('Érvénytelen irányítószám!');
								}
							});
		
						} else alert('Érvénytelen irányítószám!');
		
					}
				);
		
			},
		
			reloadPP: function(fitBounds){
				var skipgroups = Array();
				for(i in ppapi.skipgroups) if(ppapi.skipgroups[i]) skipgroups.push(i);
				ppapi.apicall('listPP', {
					'zipcode': ppapi.zipcode,
					'lat': ppapi.zipkrd.lat(),
					'lng': ppapi.zipkrd.lng(),
					'group': ppapi.opts.group,
					'skipgroups': skipgroups.join(',')
					}, function(data, textStatus, jqXHR ){
						ppapi.pplist = data;
						ppapi.displayPP(fitBounds);
					}
				);
			},
		
			mapInitialize: function(){
				google.maps.visualRefresh = true;
				//console.log("mapInitialize")
				var mapOptions = {
					backgroundColor: '#ffffff',
					mapTypeControl: true,
					mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
					scaleControl: true,
					zoom: 13,
					center: new google.maps.LatLng(47.499135, 19.043695),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				ppapi.map = new google.maps.Map(document.getElementById('pp-map-canvas'), mapOptions);
				ppapi.infoWindow = new google.maps.InfoWindow({});
				ppapi.symbols['10_posta'] = new google.maps.MarkerImage('catalog/view/theme/default/image/mapsymbol-posta.png',
						new google.maps.Size(25, 20), new google.maps.Point(0,0), new google.maps.Point(10, 19));
				ppapi.symbols['20_molkut'] = new google.maps.MarkerImage('catalog/view/theme/default/image/mapsymbol-molkut.png',
						new google.maps.Size(25, 20), new google.maps.Point(0,0), new google.maps.Point(10, 19));
				ppapi.symbols['30_csomagautomata'] = new google.maps.MarkerImage('catalog/view/theme/default/image/mapsymbol-csomagautomata.png',
						new google.maps.Size(20, 29), new google.maps.Point(0,0), new google.maps.Point(10, 19));
				ppapi.symbols['40_postapontplusz'] = new google.maps.MarkerImage('catalog/view/theme/default/image/mapsymbol-ppplusz.png',
						new google.maps.Size(25, 20), new google.maps.Point(0,0), new google.maps.Point(10, 19));
				ppapi.symbols['50_coop'] = new google.maps.MarkerImage('catalog/view/theme/default/image/mapsymbol-coop.png',
						new google.maps.Size(25, 9), new google.maps.Point(0,0), new google.maps.Point(12, 4));
		
				var skipgroups = Array();
				for(i in ppapi.skipgroups) if(ppapi.skipgroups[i]) skipgroups.push(i);
				ppapi.apicall('listPPMarkers', {
					'group': ppapi.opts.group,
					'skipgroups': skipgroups.join(',')
					}, function(data, textStatus, jqXHR ){
						ppapi.ppmarkerslist = data;
						ppapi.displayPPMarkers();
					}
				);
		
				ppapi.zipkrd = ppapi.map.getCenter();
				ppapi.reloadPP(false);

            },
		
			setOpts: function(opts){
				if(!opts) opts = {};
				ppapi.opts = opts;
				if(!ppapi.opts.group) ppapi.opts.group = '';
			},
		
			displayPP: function(fitBounds){
				if( fitBounds == undefined ) fitBounds = true;
				var ppselectobj = $('#pp-select-postapont');
				ppselectobj.empty();
		
				var lat_min = 90, lat_max = -90, lon_min = 180, lon_max = -180, n = 0;
				if(ppapi.pplist) $.each(ppapi.pplist, function(key, item) {
					ppselectobj.append($("<option />").val(item.id).text( item.name+' ('+item.zip+' '+item.county+', '+item.address+')' ));
					if( n==0 || item.distance<30 ) {
						lat_min = Math.min(lat_min, item.lat);
						lon_min = Math.min(lon_min, item.lon);
						lat_max = Math.max(lat_max, item.lat);
						lon_max = Math.max(lon_max, item.lon);
						n++;
					}
				});
		
				if( fitBounds && n>0 ){
					var southWest = new google.maps.LatLng(lat_min,lon_min);
					var northEast = new google.maps.LatLng(lat_max,lon_max);
					var bounds = new google.maps.LatLngBounds(southWest,northEast);
					if( n==1 ){
						ppapi.map.setCenter( southWest );
						ppapi.map.setZoom(13);
					} else ppapi.map.fitBounds( bounds );
				}
		
			},
		
			displayPPMarkers: function(){
				for (var i = 0; i < ppapi.markers.length; i++) ppapi.markers[i].setMap(null);
				delete(ppapi.markers);
				ppapi.markers = [];
				if (!ppapi.ppmarkerslist) return;
		
				var zIndex = 1;
				$.each(ppapi.ppmarkerslist, function(key, item) {
					if ( ppapi.skipgroups[item.group] ) return;
		
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(item.lat, item.lon),
						map: ppapi.map,
						flat: true,
						icon: ppapi.symbols[item.group],
						title: item.name,
						data: ppapi.ppmarkerslist[key],
						zIndex: zIndex
					});
					zIndex++;
					ppapi.markers.push(marker);
					marker.data.index = key;
		
					(function(marker) {
						google.maps.event.addListener(marker, 'click', function(){ppapi.mapOpenMarkerInfo(marker);});
					}(marker));
				});
			},
		
			mapOpenMarkerInfo: function(marker){
				//console.log(marker.data);
				ppapi.infoWindow.setContent('<div class="pp-map-info" style="overflow: hidden;">'
					+'<b>' + marker.data.name + '</b><br />'
					+'Cím: ' + marker.data.zip + ' ' + marker.data.county + ',<br />' + marker.data.address + '<br />'
					+ ((marker.data.phone) ?'Telefon: ' + marker.data.phone + '<br />' :'')
					//+'<a href="#" style="color:#00f;" onclick="ppapi.mapMarkerSelect(ppapi.ppmarkerslist['+marker.data.index+']); return false;">Kiválaszt</a>'
					+'<\/div>');
				ppapi.infoWindow.setPosition(marker.position);
				ppapi.infoWindow.open(ppapi.map);
				ppapi.mapMarkerSelect(ppapi.ppmarkerslist[marker.data.index]);
			},
		
			mapMarkerSelect: function(item){
				var ppselectobj = $('#pp-select-postapont');
				var optobj = $('option[value="' + item.id + '"]', ppselectobj);
				if(optobj.length) optobj.attr('selected', 'selected');
				else ppselectobj.append($("<option />").val(item.id).text( item.name+' ('+item.zip+' '+item.county+', '+item.address+')' ).attr('selected', 'selected'));
				return ppapi.selectPP(item);
			},
		
			selectPP: function(data){
				ppapi.infoWindow.close();
				if(ppapi.onSelect) ppapi.onSelect(data);
				return false;
			},
			
			onSelect: function(data){
				//console.log(data);
				$('#valasztott_postapont').val( data['name'] + '|' +data['zip'] +'|'+ data['county'] +'|'+ data['address'] );
				if (data['phone']!=null) {
					$('#kivalasztott_postapont').html( '<br><b>(' +data['zip'] + ' ' + data['county'] + ' ' + data['address'] + ' ' + data['name']+ ' /Telefon: ' + data['phone'] +'/'+')</b>' );
                    $("#postapontContent").hide("slow");
                    $("#postapont_bezar").hide();


                }
				else {
					$('#kivalasztott_postapont').html( '<br><b>(' +data['zip'] + ' ' + data['county'] + ' ' + data['address'] + ' ' + data['name']+')</b>' );
				}
				var set_data_nok = data['zip'] + ' ' + data['county'] + ' ' + data['address'] + ' ' + data['name'] ;
				var set_data = encodeURIComponent(set_data_nok);
				//console.log(set_data);
				$.ajax({
					url: 'index.php?route=postapont/pontok/sendAJAX&boltdata='+set_data,
					dataType: 'json',
					cache:false,
					async:false,
					success: function() {
						$('#postapont.postapont').attr('checked',true);
						var postaPontRadioNutton = document.getElementById('postapont.postapont');
						postaPontRadioNutton.style.display = 'block';
						
					}
				});
			},
		
			setMarkers: function(layer, display){
		//		if(layer == '20_molkut') layer = '40_postapontplusz';
				if(display == undefined) display = true;
				ppapi.skipgroups[layer] = (display) ?false :true;
				ppapi.displayPPMarkers();
				ppapi.initZip(ppapi.zipcode);
			}
		};
	})(jQuery);

	function initialize() {
		[DISABLE_POSTAPONT]
		[DISABLE_MOL]
		[DISABLE_COOP]
		[DISABLE_AUTOMATA]
		
		ppapi.linkZipField('ugyfelform_iranyitoszam'); //<-- A megrendelő form input elemének a megjelölése (beállítása a kiválasztó számára)
		ppapi.insertMap('postapontvalasztoapi'); //<-- PostaPont választó API beillesztése ( ilyen azonosítóval rendelkező DOM objektumba)
		var postaPontRadioNutton = document.getElementById('postapont.postapont');
		postaPontRadioNutton.style.display = 'none';
		$("#postapontContent").hide();

    }
	/*$('.highlight').click(function(e){
		$("#postapontContent").hide();
	})*/
	$('.postapontContent, #kivalasztott_postapont').click(function(e){
		    $("#postapontContent").show();
		    google.maps.event.trigger(window, 'resize', {});
		    e.preventDefault();
		    e.stopPropagation();

            $("#postapont_bezar").show("slow");
		    return false;
	});

    $('#postapont_bezar').click(function(e){
        $("#postapontContent").hide("slow");
        $("#postapont_bezar").hide();
    });

    $('label[for="free.free"], input[value="free.free"], label[for="flat1.flat1"], input[value="flat1.flat1"], label[for="flat2.flat2"], input[value="flat2.flat2"]').click(function(){
        $("#postapontContent").hide("slow");
        $("#postapont_bezar").hide();
    })
</script>