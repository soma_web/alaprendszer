<?php
header('Content-type: text/html; charset=utf-8');
//header('Content-Type: text/html; charset=iso-8859-1');


global $query;
global $sorok;


require_once('config.php');
require_once('php/lekerdezes.php');
require_once('php/csv_termek.php');


$query = new Mysql_Lekeres(DB_HOSTNAME, DB_USERNAME,DB_PASSWORD, DB_DATABASE);
$ir_olvas = new ModelCsvTermekek($query);


echo "";


$csv_files=scandir(UTVONAL_NEW);

foreach($csv_files as $file_name){

    $file_info = pathinfo($file_name);

    if  ($file_info['extension'] == "csv") {
        $data = array(
            'filename'      => UTVONAL_NEW."/".$file_name,
            'filefejlec'    => 1,
            'kodolas_rol'   => "LATIN 1",
            'kodolas_ra'    => "UTF-8"
        );
        $ir_olvas->csvBeolvas($data);
        $ir_olvas->csvAdatbazisba($data);
    }
}

?>