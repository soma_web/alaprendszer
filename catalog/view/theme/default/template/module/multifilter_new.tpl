<script type="text/javascript" src="catalog/view/javascript/loopj-jquery-simple-slider-fa64f59/js/simple-slider.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/loopj-jquery-simple-slider-fa64f59/css/simple-slider.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/loopj-jquery-simple-slider-fa64f59/css/simple-slider-volume.css" />

<?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/multifilter/category_php_fv.tpl')) {
    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/multifilter/category_php_fv.tpl';
} else {
    $arkiir = DIR_TEMPLATE.'default/template/module/multifilter/category_php_fv.tpl';
} ?>
<?php require_once($arkiir); ?>



<div class="multicategory" style="">

    <?php if ($heading_latszik){ ?>
        <div class="box-heading"><?php echo $heading_title; ?></div>
    <?php }?>

    <?php if ($beallitasok) { ?>
        <?php foreach($beallitasok as $beallitas) {?>
            <?php if ($beallitas['value'] == 1 ) {?>

                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/multifilter/'.$beallitas['tpl_name'])) {
                    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/multifilter/'.$beallitas['tpl_name'];
                } else {
                    $arkiir = DIR_TEMPLATE.'default/template/module/multifilter/'.$beallitas['tpl_name'];
                } ?>
                <?php include($arkiir); ?>
            <?php }?>
        <?php }?>
    <?php } ?>


</div>




<script>



    var szuro = '';

$('#search input[name=\'filter_name\']').bind('keydown', function(e) {
    if (e.keyCode == 13) {
        url = $('base').attr('href') + 'index.php?route=product/search';

        var filter_name = $('input[name=\'filter_name\']').attr('value');

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        location = url;
    }
});
</script>

<script>
    $("[data-slider]").each(function () {
            var input = $(this);
            $("<span>")
                .addClass("output")
                .insertAfter($(this));
        })
        .bind("slider:ready slider:changed", function (event, data) {
            if ( data['el'].attr('id') == "filter_arszazalek-slider" ) {
                $(this)
                    .nextAll(".output:first")
                    .html("<?php echo $entry_filter_select_artol ?>"+data.value.toFixed(0) +"%");
            } else {
                 $(this)
                     .nextAll(".output:first")
                     .html("<?php echo $entry_filter_select_artol ?>"+data.value.toFixed(0) +"Ft");
            }
        });

        $( "#slider-range" ).slider({
            range: true,
            // min: <?php echo $beallitasok['ar_szures_tol_ig']['tol']?>,
            //max: <?php echo $beallitasok['ar_szures_tol_ig']['ig']?>,

            min: <?php echo  isset($filter_ar_tol_ig[0]) ? $filter_ar_tol_ig[0] : $beallitasok['ar_szures_tol_ig']['tol']?>,
            max: <?php echo  isset($filter_ar_tol_ig[1]) ? $filter_ar_tol_ig[1] : $beallitasok['ar_szures_tol_ig']['ig']?>,

            values: [ <?php echo isset($filter_ar_tol_ig[2]) ? $filter_ar_tol_ig[2] : $beallitasok['ar_szures_tol_ig']['tol']?>,
                      <?php echo isset($filter_ar_tol_ig[3]) ? $filter_ar_tol_ig[3] : $beallitasok['ar_szures_tol_ig']['ig']?> ],

            slide: function( event, ui ) {
                $( "#arvalaszt_tol" ).html(number_format(ui.values[0])+" Ft" );
                $( "#arvalaszt_tol" ).val(ui.values[0] );

                $( "#arvalaszt_ig" ).html(number_format(ui.values[1])+ " Ft" );
                $( "#arvalaszt_ig" ).val(ui.values[1] );
            },
            change: function(event, ui) {

                $('#slider-range').trigger('click');
            }
        });

</script>




<script>


    $('.box-filter-category input').bind("click",function() {
        if (this.checked) {
            szuro = (this.value);
        } else {
            szuro = "";

        }
    });

    $('.box-filter-sub-category').bind("click",function() {
        szuro = "<?php echo isset($_REQUEST['path']) ? $_REQUEST['path']: "";?>";
    });

    $('.box-filter input[type=\'checkbox\'], .box-content input[type=\'button\'], .box-filter_artol, .box-filter_arig').bind('click', function() {
        szuroIndit(this.id)
    });

    function  szuroIndit(para_id) {
        filter = [];
        filter_categorys = [];
        filter_varos = [];
        filter_uzlet = [];
        filter_artol = 0;
        filter_arig = 0;
        filter_arszazalek = 0;

        filter_tulajdonsagok = [];
        filter_tulajdonsagok_szukit = [];
        filter_tulajdonsagok_bovit = [];
        filter_valasztek = [];
        filter_gyarto = [];
        filter_meret = [];
        filter_szin = [];
        filter_raktaron = [];
        filter_ar_tol_ig = [];

        esemeny_kivalto = para_id;

        <?php if ($beallitasok['kategoria_sub']['value'] == 1) { ?>
            //szuro = "<?php echo isset($_REQUEST['path']) ? $_REQUEST['path'] : "";?>";
            szuro = "<?php echo isset($this->request->request['path'] ) ? $this->request->request['path'] : "";?>";
        <?php } ?>

        <?php if ($beallitasok['kategoria_normal']['value'] == 1) { ?>
            //szuro = "<?php echo isset($_REQUEST['path']) ? $_REQUEST['path'] : "";?>";
            szuro = "<?php echo isset($this->request->request['path'] ) ? $this->request->request['path'] : "";?>";

        <?php } ?>

        $('.box-filter-category input[type=\'checkbox\']:checked').each(function(element) {
            filter.push(this.value);
        });

        $('.box-filter-varos input[type=\'checkbox\']:checked').each(function(element) {
            filter_varos.push(this.value);
        });

        $('.box-filter-uzlet input[type=\'checkbox\']:checked').each(function(element) {
            filter_uzlet.push(this.value);
        });


        $('.box-filter-tulajdonsagok.szukit input[type=\'checkbox\']:checked').each(function(element) {
            filter_tulajdonsagok_szukit.push(this.value);
        });
        $('.box-filter-tulajdonsagok.bovit input[type=\'checkbox\']:checked').each(function(element) {
            filter_tulajdonsagok_bovit.push(this.value);
        });

        $('.box-filter-tulajdonsagok input[type=\'checkbox\']:checked').each(function(element) {
            filter_tulajdonsagok.push(this.value);
        });
        $('.box-filter-valasztek input[type=\'checkbox\']:checked').each(function(element) {
            filter_valasztek.push(this.value);
        });

        $('.box-filter-gyarto input[type=\'checkbox\']:checked').each(function(element) {
            filter_gyarto.push(this.value);
        });

        $('.box-filter-meret input[type=\'checkbox\']:checked').each(function(element) {
            filter_meret.push(this.value);
        });

        $('.box-filter-szin input[type=\'checkbox\']:checked').each(function(element) {
            filter_szin.push(this.value);
        });

        $('.box-filter-raktar input[type=\'checkbox\']:checked').each(function(element) {
            filter_raktaron.push(this.value);
        });

        $('#filter_artol').each(function(element) {
            filter_artol = this.value;
        });

        $('#filter_arig').each(function(element) {
            filter_arig = this.value;
        });

        $('#filter_arszazalek').each(function(element) {
            filter_arszazalek = this.value;
        });

        if (esemeny_kivalto == 'mf-artol') {
            if ($("#arvalaszt_tol").attr("value")) {
                filter_ar_tol_ig.push($("#arvalaszt_tol").attr("value"));
            } else {
                filter_ar_tol_ig.push("<?php echo $beallitasok['ar_szures_tol_ig']['tol']?>");
            }
            if ($("#arvalaszt_ig").attr("value")) {
                filter_ar_tol_ig.push($("#arvalaszt_ig").attr("value"));
            } else {
                filter_ar_tol_ig.push("<?php echo $beallitasok['ar_szures_tol_ig']['ig']?>");
            }
        }

        var sort = $(".sort select").prop("selectedIndex");
        //var limit = $(".limit select").prop("selectedIndex");
        var limit = $(".limit select").find('option:selected').html();
        var order = "";

        <?php if ($this->config->get('config_review_status')) { ?>
            if (sort == 0) {
                sort    = "p.sort_order"
                order   = "ASC";
            } else if (sort == 1) {
                sort = "pd.name";
                order   = "ASC";
            } else if (sort == 2) {
                sort = "pd.name";
                order   = "DESC";
            } else if (sort == 3) {
                sort = "p.price";
                order   = "ASC";
            } else if (sort == 4) {
                sort = "p.price";
                order   = "DESC";
            } else if (sort == 5) {
                sort = "rating";
                order   = "DESC";
            } else if (sort == 6) {
                sort = "rating";
                order   = "ASC";
            } else if (sort == 7) {
                sort = "p.model";
                order   = "ASC";
            } else if (sort == 8) {
                sort = "p.model";
                order   = "DESC";
            }

        <?php } else { ?>
            if (sort == 0) {
                sort = "p.sort_order"
                order   = "ASC";
            } else if (sort == 1) {
                sort = "pd.name";
                order   = "ASC";
            } else if (sort == 2) {
                sort = "pd.name";
                order   = "DESC";
            } else if (sort == 3) {
                sort = "p.price";
                order   = "ASC";
            } else if (sort == 4) {
                sort = "p.price";
                order   = "DESC";
            } else if (sort == 5) {
                sort = "p.model";
                order   = "ASC";
            } else if (sort == 6) {
                sort = "p.model";
                order   = "DESC";
            }
        <?php } ?>

        /*if (limit == 0) {
            limit    = <?php echo $this->config->get('config_catalog_limit')?>;
        } else if (limit == 1) {
            limit = "25";
        } else if (limit == 2) {
            limit = "50";
        } else if (limit == 3) {
            limit = "75";
        } else if (limit == 4) {
            limit = "100";

        }*/

        $.ajax({
            url: 'index.php?route=product/multifilter',
            type: 'post',
            dataType: 'html',
            data: 'filter=' + filter.join(',') +
                    '&filter_varos=' + filter_varos.join(',')  +
                    '&filter_tulajdonsagok=' + filter_tulajdonsagok.join(',')  +
                    '&filter_tulajdonsagok_szukit=' + filter_tulajdonsagok_szukit.join(',')  +
                    '&filter_tulajdonsagok_bovit=' + filter_tulajdonsagok_bovit.join(',')  +
                    '&filter_valasztek=' + filter_valasztek.join(',')  +
                    '&filter_gyarto=' + filter_gyarto.join(',')  +
                    '&filter_meret=' + filter_meret.join(',')  +
                    '&filter_szin=' + filter_szin.join(',')  +
                    '&filter_raktaron=' + filter_raktaron.join(',')  +
                    '&filter_uzlet=' + filter_uzlet.join(',') +
                    '&filter_arig='+ filter_arig  +
                    '&filter_artol='+ filter_artol  +
                    '&filter_arszazalek='+ filter_arszazalek +
                    '&filter_ar_tol_ig='+ filter_ar_tol_ig +
                    '&path='+szuro +
                    '&sort='+ sort +
                    '&order='+ order +
                    '&limit='+ limit +
                    '&esemeny_kivalto='+ esemeny_kivalto +
                    '&multiszuro=1&ajax_hivas=1',



            beforeSend: function() {
                $(".szuro_valaszt").remove();

                $('.kozepe').after('<div class="szuro_valaszt" style="position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; z-index: 99999999999; background: #dddddd; opacity: 0.2; display: none"></div>');
                $('.szuro_valaszt').slideDown('fast');
            },

            complete: function(html) {
                $('.szuro_valaszt').slideUp('fast',function(){
                    $(".szuro_valaszt").remove();

                });
            },

            success: function(html) {
                html = '<div id="notification"></div>' + html;
                $('.kozepe').html(html);

                if (typeof(addthis) != "undefined") {
                    var addthis_config = {"data_track_addressbar":true};

                    addthis.toolbox('.addthis_default_style');
                }

                $("#menu a").removeClass("active");

                $("[data-slider]").each(function () {
                    var el = $(this.parentElement).find('.slider');
                    if ( el.attr('id') == "filter_arszazalek-slider" ) {
                        $(this)
                            .nextAll(".output:first")
                            .html("<?php echo $entry_filter_select_artol ?>"+(this.value*1).toFixed(0) +"%");
                    } else {
                        $(this)
                            .nextAll(".output:first")
                            .html("<?php echo $entry_filter_select_artol ?>"+(this.value*1).toFixed(0) +"Ft");
                    }
                });
                mulitifilterScriptInitialize();

            },
            error: function(e) {
            }
        });


        /*location = '<?php echo $action; ?>&filter=' + filter.join(',') +
            '&filter_varos=' + filter_varos.join(',')  +
            '&filter_tulajdonsagok=' + filter_tulajdonsagok.join(',')  +
            '&filter_valasztek=' + filter_valasztek.join(',')  +
            '&filter_gyarto=' + filter_gyarto.join(',')  +
            '&filter_raktaron=' + filter_raktaron.join(',')  +
            '&filter_uzlet=' + filter_uzlet.join(',') +
            '&filter_arig='+ filter_arig  +
            '&filter_artol='+ filter_artol  +
            '&filter_arszazalek='+ filter_arszazalek +
            '&filter_ar_tol_ig='+ filter_ar_tol_ig +
            '&path='+szuro +
            '&multiszuro=1';*/
    }

</script>

