<?php
    $vantab = false;

    if (!empty($description) && isset($beallitas_termek_aloldal['description'])) {
        $vantab = true;
        $tab = "description";

    } elseif ($meret && isset($beallitas_termek_aloldal['meret'])) {
        $vantab = true;
        $tab = "meret";

    } elseif ($attribute_groups && isset($beallitas_termek_aloldal['attribute_groups'])) {
        $vantab = true;
        $tab = "attribute_groups";

    } elseif ($review_status && isset($beallitas_termek_aloldal['review_status'])) {
        $vantab = true;
        $tab = "review_status";

    } elseif (!empty($test_description) && isset($beallitas_termek_aloldal['test_description'])) {
        $vantab = true;
        $tab = "test_description";

    } elseif ($products  && isset($beallitas_termek_aloldal['products'])) {
        $vantab = true;
        $tab = "products";

    } elseif ( $images  && isset($beallitas_termek_aloldal['images'])) {
        $vantab = true;
        $tab = "images";

    } elseif ( $ajandekok  && isset($beallitas_termek_aloldal['ajandek'])) {
        $vantab = true;
        $tab = "ajandek";
    } elseif (isset($letoltesek) && $letoltesek  && isset($beallitas_termek_aloldal['downloads'])) {
        $vantab = true;
        $tab = "downloads";
    }
?>

<?php if ($vantab) { ?>
    <div id="tabs-second" class="htabs">
        <div class="inner-htabs">
            <?php if ($description && isset($beallitas_termek_aloldal['description'])) { ?>
                <a href="#tab-description-second"><?php echo $tab_description; ?></a>
            <?php } ?>

            <?php if ($meret && isset($beallitas_termek_aloldal['meret'])) { ?>
                    <a href="#tab-meret-second"><?php echo $tab_meret; ?> </a>
            <?php } ?>

            <?php if ($attribute_groups  && isset($beallitas_termek_aloldal['attribute_groups'])) { ?>
                    <a  href="#tab-attribute-second"><?php echo $tab_attribute; ?> (<?php echo $attribute_count; ?>) </a>
            <?php } ?>

            <?php if ($review_status  && isset($beallitas_termek_aloldal['review_status'])) { ?>
                    <a  href="#tab-review-second"><?php echo $tab_review; ?></a>
            <?php } ?>

            <?php if ($products  && isset($beallitas_termek_aloldal['products'])) { ?>
                    <a href="#tab-related-second"><?php echo $tab_related; ?> (<?php echo count($products); ?>)</a>
            <?php } ?>

            <?php if (!empty($test_description) && isset($beallitas_termek_aloldal['test_description'])) { ?>
                    <a href="#tab-test_description-second"><?php echo $tab_test_description; ?> </a>
            <?php } ?>

            <?php if (isset($images) && $images  && isset($beallitas_termek_aloldal['images'])) { ?>
                    <a href="#tab-image-second"><?php echo $tab_image; ?> (<?php echo count($images); ?>)</a>
            <?php } ?>

            <?php if (isset($ajandekok) && $ajandekok  && isset($beallitas_termek_aloldal['ajandek'])) { ?>
                    <a href="#tab-ajandek-second"><?php echo $tab_ajandek; ?> (<?php echo count($ajandekok); ?>)</a>
            <?php } ?>

            <?php if (isset($beallitas_termek_aloldal['facebook_comments'])) { ?>
                <a href="#tab-facebook_comments-second"><?php echo $tab_facebook_comments; ?></a>
            <?php } ?>

            <?php if (isset($beallitas_termek_aloldal['downloads']) && isset($letoltesek) && $letoltesek) { ?>
                <a href="#tab-downloads-second"><?php echo $tab_downloads; ?></a>
            <?php } ?>
            <?php if ($qa_status && isset($beallitas_termek_aloldal['questions']) && $beallitas_termek_aloldal['questions']) { ?>
                <a href="#tab-qa-second"><?php echo $tab_qa . (($qa_display_questions && $qa_count) ? ' (' . $qa_count . ')': ''); ?></a>
            <?php } ?>
        </div>
    </div>
<?php } ?>



<?php if (!empty($description) && isset($beallitas_termek_aloldal['description']) ) { ?>
    <div id="tab-description-second" class="tab-content"><?php echo $description; ?></div>
<?php } ?>

<?php if ($meret && isset($beallitas_termek_aloldal['meret']) ) { ?>
    <div id="tab-meret-second" class="tab-content"><?php echo $meret; ?></div>
<?php } ?>

<?php if (!empty($test_description) && isset($beallitas_termek_aloldal['test_description']) ) { ?>
    <div id="tab-test_description-second" class="tab-content"><?php echo $test_description; ?></div>
<?php } ?>

<?php if ($attribute_groups  && isset($beallitas_termek_aloldal['attribute_groups']) ) { ?>
    <div id="tab-attribute-second" class="tab-content">
        <table class="attribute">
            <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                <tr>
                    <td colspan="2"><?php echo $attribute_group['name']; ?></td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                    <tr>
                        <td><?php echo $attribute['name']; ?></td>
                        <td><?php echo $attribute['text']; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            <?php } ?>
        </table>
    </div>
<?php } ?>

<?php if (isset($beallitas_termek_aloldal['facebook_comments']) ) { ?>
    <div id="tab-facebook_comments-second" class="tab-content">
       <?php echo $facebook_comments; ?>
    </div>
<?php } ?>

<?php if ($review_status  && isset($beallitas_termek_aloldal['review_status'])) { ?>
    <div id="tab-review-second" class="tab-content">
        <div id="review"></div>
        <h2 id="review-title"><?php echo $text_write; ?></h2>
        <b><?php echo $entry_name; ?></b><br />
        <input type="text" name="name" value="" />
        <br />
        <br />
        <b><?php echo $entry_review; ?></b>
        <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
        <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
        <br />
        <b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;
        <input type="radio" name="rating" value="1" />
        &nbsp;
        <input type="radio" name="rating" value="2" />
        &nbsp;
        <input type="radio" name="rating" value="3" />
        &nbsp;
        <input type="radio" name="rating" value="4" />
        &nbsp;
        <input type="radio" name="rating" value="5" />
        &nbsp; <span><?php echo $entry_good; ?></span><br />
        <br />
        <b><?php echo $entry_captcha; ?></b><br />
        <input type="text" name="captcha" value="" />
        <br />
        <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />
        <br />
        <div class="buttons">
            <div class="right"><a id="button-review" class="button"><?php echo $button_continue; ?></a></div>
        </div>
    </div>
<?php } ?>

<?php if ($products  && isset($beallitas_termek_aloldal['products'])) { ?>
    <div id="tab-related-second" class="tab-content" style="padding-right: 0">

        <div class="box-product tab_kornyezet" style="width: 100%">
            <ul  class="tab-related">
                <?php foreach ($products as $product) { ?>
                    <li>
                        <div class="termek_tabban">
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>

                    </li>
                <?php } ?>
            </ul>
        </div>

    </div>
<?php } ?>

<?php if ($ajandekok  && isset($beallitas_termek_aloldal['ajandek'])) { ?>
    <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/plus.png')) {
        $plus = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/plus.png';
    } else {
        $plus = DIR_TEMPLATE_IMAGE.'default/image/plus.png';
    } ?>
    <div id="tab-ajandek-second" class="tab-content" style="padding-right: 0">

        <div class="box-product tab_kornyezet" style="width: 100%">
            <ul  class="tab-ajandek">
                <?php $pluszjel = false;?>
                <?php foreach ($ajandekok as $product) { ?>
                    <li>
                        <div class="plus">
                            <?php if ($pluszjel) { ?>
                                <img src="<?php echo $plus?>">
                            <?php } ?>
                        </div>

                        <div class="termek_tabban <?php echo (!$pluszjel) ? "termek" : ""; ?>">
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>
                        <?php $pluszjel = true;?>

                    </li>
                <?php } ?>
            </ul>
        </div>


    </div>
<?php } ?>


<?php if (isset($images) && $images  && isset($beallitas_termek_aloldal['images'])) { ?>
    <div id="tab-image-second" class="tab-content">
        <div class="image-additional">
            <?php foreach ($images as $image) { ?>
                <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="fancybox" ><img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
            <?php } ?>
        </div>
    </div>
<?php } ?>

<?php if (isset($letoltesek) && $letoltesek  && isset($beallitas_termek_aloldal['downloads']) && $beallitas_termek_aloldal['downloads']) { ?>
    <div id="tab-downloads-second" class="tab-content">
        <div class="downloadable_pdfs">
            <?php foreach ($letoltesek as $letoltes) { ?>
               <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/download-16.png')) {
                        $download_icon = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/download-16.png';
                    } else {
                    $download_icon = DIR_TEMPLATE_IMAGE.'default/image/download-16.png';
               } ?>
                <div>
                    <a class="download_pdf" href="<?php echo $letoltes['href']; ?>"><img src="<?php echo $download_icon; ?>" alt="<?php echo $button_download; ?>" title="<?php echo $button_download; ?>" /> <b><?php echo (isset($letoltes['pdf_title']) && !empty($letoltes['pdf_title'])) ? $letoltes['pdf_title'] : $letoltes['filename'] ;?></b></a>
                </div>

            <?php } ?>
        </div>
    </div>
<?php } ?>

<?php if (isset($beallitas_termek_aloldal['questions']) && $beallitas_termek_aloldal['questions']) { ?>
    <?php if ($qa_status) { ?>
        <div id="tab-qa-second" class="tab-content">
            <?php if ($qa_display_questions) { ?>
                <div id="qa"><?php echo $qas; ?></div>
            <?php } ?>
            <h2 id="qa-title"><?php echo $text_ask; ?></h2>
            <div id="q-form">
                <?php if ($qa_form_display_name) { ?>
                    <div class="q_input">
                        <label class="q_input_name<?php echo ($qa_form_require_name) ? ' required' : ''; ?>" for="q_name"><?php echo $entry_name; ?></label>
                        <div class="q_input_value"><input type="text" name="q_name" id="q_name" value="<?php echo $q_name; ?>"<?php echo ($qa_form_require_name) ? ' required' : ''; ?> /></div>
                    </div>
                <?php } ?>
                <?php if ($qa_form_display_email) { ?>
                    <div class="q_input">
                        <label class="q_input_name<?php echo ($qa_form_require_email) ? ' required' : ''; ?>" for="q_email"><?php echo $entry_email; ?></label>
                        <div class="q_input_value"><input type="email" name="q_email" id="q_email" value="<?php echo $q_email; ?>"<?php echo ($qa_form_require_email) ? ' required' : ''; ?> /></div>
                    </div>
                <?php } ?>
                <?php if ($qa_form_display_phone) { ?>
                    <div class="q_input">
                        <label class="q_input_name<?php echo ($qa_form_require_phone) ? ' required' : ''; ?>" for="q_phone"><?php echo $entry_phone; ?></label>
                        <div class="q_input_value"><input type="tel" name="q_phone" id="q_phone" value=""<?php echo ($qa_form_require_phone) ? ' required' : ''; ?> /></div>
                    </div>
                <?php } ?>
                <?php if ($qa_form_display_custom_field) { ?>
                    <div class="q_input">
                        <label class="q_input_name<?php echo ($qa_form_require_custom_field) ? ' required' : ''; ?>" for="q_custom"><?php echo $entry_custom; ?></label>
                        <div class="q_input_value"><input type="text" name="q_custom" id="q_custom" value=""<?php echo ($qa_form_require_custom_field) ? ' required' : ''; ?> /></div>
                    </div>
                <?php } ?>
                <div class="clearfixx"></div>
                <div class="q_input">
                    <label class="q_input_name required" for="q_question"><?php echo $entry_question; ?></label>
                    <div class="q_input_value">
                        <textarea name="q_question" id="q_question" class="q_input_question" rows="8" required></textarea>
                        <div class="q_note"><?php echo $text_note; ?></div>
                    </div>
                </div>
                <?php if ($qa_form_display_captcha) { ?>
                    <div class="clearfixx"></div>
                    <div class="q_input">
                        <label class="q_input_name<?php echo ($qa_form_require_captcha) ? ' required' : ''; ?>" for="q_captcha"><?php echo $entry_captcha; ?></label>
                        <div class="q_input_value"><input type="text" name="q_captcha" value="" autocomplete="off"<?php echo ($qa_form_require_captcha) ? ' required' : ''; ?> /></div>
                        <div><img src="index.php?route=product/product/captcha" id="q_captcha" /></div>
                    </div>
                <?php } ?>
            </div>
            <div class="buttons">
                <div class="right"><a id="button-qa" class="button"><?php echo $button_continue; ?></a></div>
            </div>
        </div>
    <?php } ?>

<?php } ?>

<script>

    $('#tabs-second a').tabs({
        bxSlidesOn: <?php echo isset($beallitas_termek_aloldal['korhinta']) ? 1 : 0?>,
        minSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 3?>,
        maxSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 3?>,
        slideWidth: <?php echo isset($beallitas_termek_aloldal['korhinta_width']) ? $beallitas_termek_aloldal['korhinta_width'] : 260?>
    });
</script>

<?php if (($qa_status) && isset($beallitas_termek_aloldal['questions']) && $beallitas_termek_aloldal['questions']) { ?>
    <script type="text/javascript"><!--
        $("#qa-ask").on("click", function (a) {
            a.preventDefault(), $("a[href='#tab-qa-second']").trigger("click"), $("html,body").animate({scrollTop: $("#qa-title").offset().top}, 1300)
        }), $("#qa .pagination a").on("click", function () {
            var a = this.href;
            return $("#qa").slideUp("slow", function () {
                $("#qa").load(a, function () {
                    $("#qa").slideDown("slow")
                })
            }), !1
        });
        <?php if ($preload != 1) { ?>$('#qa').load('index.php?route=product/product/question&product_id=<?php echo $product_id; ?>');
        <?php } ?>$("#button-qa").bind("click", function () {
            $.ajax({type: "POST", url: "index.php?route=product/product/ask&product_id=<?php echo $product_id; ?>", dataType: "json", data: $("#q-form :input").serialize(), beforeSend: function () {
                $(".success, .warning").remove(), $("#button-qa").attr("disabled", !0), $("#qa-title").after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>')
            }, complete: function () {
                $("#button-qa").attr("disabled", !1), $(".attention").remove()
            }, success: function (t) {
                t.error && $("#qa-title").after('<div class="warning">' + t.error + "</div>"), t.success && ($("#qa-title").after('<div class="success">' + t.success + "</div>"), $("textarea[name='q_question']").val(""), $("input[name='q_captcha']").val(""))
            }, error: function (t) {
                $("#qa-title").after('<div class="warning">' + t.statusText + "</div>")
            }})
        });
        //--></script>
<?php } ?>
