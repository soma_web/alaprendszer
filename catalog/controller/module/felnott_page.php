<?php
class ControllerModuleFelnottPage extends Controller {
	protected function index($setting) {
		static $module = 0;
		
		$this->language->load('module/felnott_page'); 
		
		$this->document->addScript('catalog/view/javascript/jquery/jquery.felnott.js');
        if ( file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/felnott.css')) {
            $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/felnott.css');
        } else {
            $this->document->addStyle('catalog/view/theme/default/stylesheet/felnott.css');
        }
		
		
		$this->data['text_dont_show_again'] = $this->language->get('text_dont_show_again');
		$this->data['text_close'] = $this->language->get('text_close');
		
		$this->data['image'] = HTTP_IMAGE . $setting['image'];
		$image_info = getimagesize($this->data['image']);
		
		$this->data['css_move_up']   = (int)($image_info['1'] / 2 );
		$this->data['css_move_left'] = (int)($image_info['0']  / 2 );
		
		if ($setting['pattern']){
			$this->data['pattern'] = HTTP_IMAGE . $setting['pattern'];
		} else {
			$this->data['pattern'] = false;
		}
		
		$this->data['color'] = $setting['color'];
		
		if ($setting['timer']){
			$this->data['timer'] = $setting['timer'];
		} else {
			$this->data['timer'] = 2;
		}
		
		$show_felnott = 1;
		
		if ($setting['url']){
			if ( strpos($this->request->server['REQUEST_URI'], $setting['url']) === false ){
				$show_felnott = 0;
			}
		}

        if (isset($_COOKIE['meerkat']) && $_COOKIE['meerkat'] == "dontshow"){
                $show_felnott = 0;
        }

        $this->data['show_felnott'] = $show_felnott;
		
		$this->data['module'] = $module++;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/felnott_page.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/felnott_page.tpl';
		} else {
			$this->template = 'default/template/module/felnott_page.tpl';
		}

		$this->render();
	}
}
?>