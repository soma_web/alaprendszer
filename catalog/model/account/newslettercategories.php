<?php

class ModelAccountNewsletterCategories extends Model {
    private $tablename;

    public function __construct($registry) {
        $this->tablename = DB_PREFIX."customer_newsletter_categories";
        parent::__construct($registry);
    }

    public function getCategoriesByCustomer($customerID) {
        $sql = "SELECT * FROM ".$this->tablename." WHERE customer_id='".$customerID."'";
        $query = $this->db->query($sql);
        if(! $query instanceof stdClass) {
            return array();
        }

        return $query->rows;
    }

    public function getCategoryIDsByCustomer($customerID) {
        $categories = $this->getCategoriesByCustomer($customerID);
        $currentCategories = array();
        foreach($categories as $category) {
            $currentCategories[] = $category['category_id'];
        }

        return $currentCategories;
    }

    public function addCategoryToCustomer($customerID, $categoryID) {
        $sql = "INSERT INTO ".$this->tablename."(customer_id, category_id) VALUES('".$customerID."', '".$categoryID."');";
        $this->db->query($sql);
    }

    public function addCategoriesToCustomer($customerID, $categories) {
        foreach($categories as $category) {
            $this->addCategoryToCustomer($customerID, $category);
        }
    }

    public function deleteCategoryFromCustomer($customerID, $categoryID) {
        $sql = "DELETE FROM ".$this->tablename." WHERE customer_id='".$customerID."' AND category_id='".$categoryID."';";
        $this->db->query($sql);
    }

    public function deleteAllCategoryFromCustomer($customerID) {
        $sql = "DELETE FROM ".$this->tablename." WHERE customer_id='".$customerID."';";
        $this->db->query($sql);
    }

    public function setCategoriesToCustomer($customerID, $categories) {
        $currentCategories = $this->getCategoryIDsByCustomer($customerID);
        foreach($categories as $category) {
            if(!in_array($category, $currentCategories)) {
                $this->addCategoryToCustomer($customerID, $category);
            }
        }

        foreach($currentCategories as $category) {
            if(!in_array($category, $categories)) {
                $this->deleteCategoryFromCustomer($customerID, $category);
            }
        }
    }

    public function getCustomerIDsByCategory($categoryID) {
        $sql = "SELECT * FROM ".$this->tablename." WHERE category_id='".$categoryID."';";
        $query = $this->db->query($sql);
        if(! $query instanceof stdClass) {
            return array();
        }

        $customerIDs = array();
        if(property_exists($query, 'rows') && is_array($query->rows)) {
            foreach($query->rows as $line) {
                $customerIDs[] = $line['customer_id'];
            }
        }

        return $customerIDs;
    }

    public function getCustomerIDsByCategories($categories) {
        $sql = "SELECT * FROM ".$this->tablename." WHERE category_id IN (".implode(',', $categories).");";
        $query = $this->db->query($sql);
        if(! $query instanceof stdClass) {
            return array();
        }

        $customerIDs = array();
        if(property_exists($query, 'rows') && is_array($query->rows)) {
            foreach($query->rows as $line) {
                $customerIDs[] = $line['customer_id'];
            }
        }

        $customerIDs = array_unique($customerIDs);

        return $customerIDs;
    }
}

?>