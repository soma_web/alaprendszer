<?php if(isset($logged) && $logged) { ?>
    <div class="header-wishlist-icon">
        <a <?php if($_SERVER['REQUEST_URI'] == "/illobello/index.php?route=account/wishlist"){ echo " class=\"active\""; } ?> href="<?php echo $wishlist; ?>" id="wishlist-total">
            <?php if(isset($template['wishlist_icon']) && $template['wishlist_icon']) { ?>
                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/favourite.png')) {
                    $favourite = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/favourite.png';
                } else {
                    $favourite = DIR_TEMPLATE_IMAGE.'default/image/favourite.png';
                } ?>
                <img class="icon_meret" title="<? if (isset($text_wishlist)) { echo $text_wishlist; }?>"  src="<? echo $favourite; ?>" >
            <?php } else { ?>
                <?php echo $text_wishlist; ?>
            <?php } ?>
        </a>
    </div>
<?php } ?>