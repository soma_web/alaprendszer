<?php
class ModelAccountUtalvanyok extends Model {
	public function getUtalvany($product_vonalkod_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_vonalkod
		 WHERE product_vonalkod_id = '" . $product_vonalkod_id. "' AND ! IFNULL(letoltve, 0)");
		 
		return $query->row;
	}
	

    public function getUtalvanyok($start = 0, $limit = 20) {
        if ($start < 0) {
            $start = 0;
        }
        $language = $this->config->get('config_language_id');
        $query = $this->db->query("SELECT pv.order_id, pv.date_added,  pd.name, pv.vonalkod, pv.product_vonalkod_id, pv.letoltve FROM " . DB_PREFIX . "product_vonalkod pv
            LEFT JOIN `" . DB_PREFIX . "order` o ON (pv.order_id = o.order_id)
            LEFT JOIN "  . DB_PREFIX . "product_description pd ON (pv.product_id = pd.product_id) WHERE
                pd.language_id = '" .$language. "' AND
                o.customer_id = '" . (int)$this->customer->getId() . "' AND
                pv.status > '0'
                ORDER BY pv.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }
	
	public function updateRemaining($product_vonalkod_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "product_vonalkod SET letoltve = 1 WHERE product_vonalkod_id = '" . (int)$product_vonalkod_id . "'");
	}
	
	public function getTotalUtalvnyok() {

        $language = $this->config->get('config_language_id');
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_vonalkod pv
            LEFT JOIN `" . DB_PREFIX . "order` o ON (pv.order_id = o.order_id)
            LEFT JOIN "  . DB_PREFIX . "product_description pd ON (pv.product_id = pd.product_id) WHERE
                pd.language_id = '" .$language. "' AND
                o.customer_id = '" . (int)$this->customer->getId() . "' AND
                pv.status > '0'");

		return $query->row['total'];
	}

    public function jogosultsagEllenorzes($product_id) {
        $sql="SELECT * FROM " . DB_PREFIX . "product_customer_utalvany
		 WHERE product_id = '" . (int)$product_id. "' AND customer_id = '" . (int)$this->customer->getId() . "'";

        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function vonalkodEllenorzes() {
        $sql="SELECT * FROM " . DB_PREFIX . "product_vonalkod
		 WHERE vonalkod = '" . (int)$_REQUEST['vonalkod']. "'";

        $query = $this->db->query($sql);

        return $query->row;

    }

    public function vonalkodBevaltasa() {
        $sql="UPDATE " . DB_PREFIX . "product_vonalkod SET
            felhasznalva = '". (int)$_REQUEST['vonalkod_bavaltasa']. "'
		 WHERE vonalkod = '" . (int)$_REQUEST['vonalkod']. "'";

        $query = $this->db->query($sql);


    }
}
?>