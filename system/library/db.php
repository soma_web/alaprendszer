<?php
class DB {
	private $driver;
	
	public function __construct($driver, $hostname, $username, $password, $database,$alap=0) {
		if (file_exists(DIR_DATABASE . $driver . '.php')) {
			require_once(DIR_DATABASE . $driver . '.php');
		} else {
			exit('Error: Could not load database file ' . $driver . '!');
		}
				
		$this->driver = new $driver($hostname, $username, $password, $database);
	}
		
  	public function query($sql) {
		return $this->driver->query($sql);
  	}
	
	public function escape($value) {
		return $this->driver->escape($value);
	}
	
  	public function countAffected() {
		return $this->driver->countAffected();
  	}

  	public function getLastId() {
		return $this->driver->getLastId();
  	}


    public function sqlSortKozosit($mezo,$tabla,$szuro) {

        $sql = "";
        $tulajdonsag = $this->query("SELECT  product_id, ".$mezo." FROM " . DB_PREFIX . $tabla. "
            WHERE ".$mezo." IN (" .$szuro . ") "  );

        $gyujto = array();
        foreach ($tulajdonsag->rows as $value) {
            $gyujto[$value[$mezo]][] = $value['product_id'];
        }

        if(count($gyujto) == 0) {
            $kozos_pr = false;
        } else {
            $mainfilter = array();
            $kozos_pr = null;
            foreach ($gyujto as $filter) {
                if(is_array($filter)) {
                    $mainfilter[] = $filter;
                    if($kozos_pr == null) {
                        $kozos_pr = $filter;
                    }
                }
            }

            if(count($mainfilter) > 1) {
                foreach($mainfilter as $currentfilter) {
                    $kozos_pr = array_intersect($kozos_pr, $currentfilter);
                }
            }

            $kozos_pr = array_unique($kozos_pr);
            $kozos_pr = implode(",",$kozos_pr);
        }
        if ($kozos_pr) {
            $sql = " AND p.product_id IN (" .$kozos_pr . ")";
        }

        return $sql;
    }



}
?>