<?php
// Heading
$_['heading_title']         = 'Attribute Groups';

// Text
$_['text_success']          = 'Success: You have modified attribute groups!';
$_['text_szukit']           = 'Narrow';
$_['text_bovit']            = 'Expands';
$_['text_alap']             = 'Default';

// Column
$_['column_name']           = 'Attribute Group Name';
$_['column_sort_order']     = 'Sort Order';
$_['column_action']         = 'Action';

// Entry
$_['entry_name']            = 'Attribute Group Name:';
$_['entry_sort_order']      = 'Sort Order:';
$_['entry_szuro_bovit']     = 'Filter  narrow/expands:';
$_['entry_szuro_csuszka']   = 'Slide show:';

// Error
$_['error_permission']      = 'Warning: You do not have permission to modify attribute groups!';
$_['error_name']            = 'Attribute Group Name must be between 3 and 64 characters!';
$_['error_attribute']       = 'Warning: This attribute group cannot be deleted as it is currently assigned to %s attributes!';
$_['error_product']         = 'Warning: This attribute group cannot be deleted as it is currently assigned to %s products!';
?>