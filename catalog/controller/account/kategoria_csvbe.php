<?php
class ControllerAccountKategoriaCsvbe extends Controller {

    public function index() {
        $this->load->language('account/megvasarolt');

        $customer = $this->customer->isLogged();

        if (!$customer) {
            $this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');
            $this->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->request->get['filter_date_start'])) {
            $filter_date_start = $this->request->get['filter_date_start'];
        } else {
            $filter_date_start = '';
        }

        if (isset($this->request->get['filter_date_end'])) {
            $filter_date_end = $this->request->get['filter_date_end'];
        } else {
            $filter_date_end = '';
        }



        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        $url .= '&customer_id=' . $customer;


        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', '', 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title_katagoria'),
            'href'      => $this->url->link('account/kategoria_jelentes', $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->load->model('account/kategoria_jelentes');



        $data = array(
            'filter_date_start'	     => $filter_date_start,
            'filter_date_end'	     => $filter_date_end,
            'customer_id'	         => $customer,
        );
        date_default_timezone_set('Europe/Budapest');

        $data['filter_date_start']  = !empty($data['filter_date_start']) ? $data['filter_date_start'] : "2014-01-01";
        $data['filter_date_end']    = !empty($data['filter_date_end']) ? $data['filter_date_end'] : date("Y-m-d H:i:s");

        $categoryes     = $this->model_account_kategoria_jelentes->getCategory($data);
        $product_total  = $this->model_account_kategoria_jelentes->getCategoryTotals($data);

        if ($categoryes->num_rows > 0) {
            $this->data['sectors'] = array();

            foreach($categoryes->rows as $category) {
                $hirdetok           = $this->model_account_kategoria_jelentes->getCategoryCustomer($category['category_id'],$data);
                $vasaroltak         = $this->model_account_kategoria_jelentes->getCategoryPurchaseSum($category['category_id'],$data);
                $vasaroltak_nalam   = $this->model_account_kategoria_jelentes->getCategoryMyPurchaseSum($category['category_id'],$data);
                $atlag              = $this->model_account_kategoria_jelentes->getCategoryAverage($category['category_id'],$data);
                $lattak             = $this->model_account_kategoria_jelentes->getCategoryViewed($category['category_id'],$data);
                $this->data['sectors'][] = array(
                    'category'          => $category['name'],
                    'hirdetok'          => $hirdetok,
                    'vasaroltak'        => $vasaroltak,
                    'vasaroltak_nalam'  => $vasaroltak_nalam,
                    'atlag'             => $atlag['price'],
                    'atlag_szazalek'    => $atlag['price_szazalek'],
                    'lattak'            => $lattak
                );

            }
        }




        $this->data['heading_title_katagoria']            = $this->language->get('heading_title_katagoria');

        $this->data['text_no_results']          = $this->language->get('text_no_results');
        $this->data['text_all_status']          = $this->language->get('text_all_status');

        $this->data['column_category_name']     = $this->language->get('column_category_name');
        $this->data['column_hirdetok']          = $this->language->get('column_hirdetok');
        $this->data['column_vasaroltak']        = $this->language->get('column_vasaroltak');
        $this->data['column_vasaroltak_nalam']  = $this->language->get('column_vasaroltak_nalam');
        $this->data['column_atlag']             = $this->language->get('column_atlag');
        $this->data['column_atlag_szazalek']    = $this->language->get('column_atlag_szazalek');
        $this->data['column_lattak']            = $this->language->get('column_lattak');

        $this->data['entry_date_start'] = $this->language->get('entry_date_start');
        $this->data['entry_date_end'] = $this->language->get('entry_date_end');
        $this->data['button_nyomtatas'] = $this->language->get('button_nyomtatas');
        $this->data['button_exportalas'] = $this->language->get('button_exportalas');
        $this->data['button_filter'] = $this->language->get('button_filter');


        $url = '';

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        $this->data['csv_be']  = $this->url->link('account/kategoria_csvbe', $url, 'SSL');


        $this->data['filter_date_start'] = $filter_date_start;
        $this->data['filter_date_end'] = $filter_date_end;


        $this->load->model('tool/ExportCSV');
        $header = array(
            'category'            => $this->data['column_category_name'],
            'hirdetok'            => $this->data['column_hirdetok'],
            'vasaroltak'          => $this->data['column_vasaroltak'],
            'vasaroltak_nalam'    => $this->data['column_vasaroltak_nalam'],
            'atlag'               => $this->data['column_atlag'],
            'atlag_szazalek'      => $this->data['column_atlag_szazalek'],
            'lattak'              => $this->data['column_lattak']
        );

        if(array_key_exists('sectors', $this->data)) {
            $this->model_tool_ExportCSV->initialize($header, $this->data['sectors']);
        } else {
            $this->model_tool_ExportCSV->initialize($header, array());
        }

  	}
}
?>
