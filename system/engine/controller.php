<?php

/**
 * =========================================================================================
 * Define your models either here or in the actual controllers using them
 * *******************************************************
 * Mark system/engine/controller.php as Plain Text
 * Mark system/engine/model.php as Plain Text
 * Mark system/engine/loader.php as Plain Text
 * *******************************************************
 * The basic idea is we are telling the IDE what var or property belongs to what class
 * The result gives use code completion and suggestion
 *
 * If you have this file in the project and open the ControllerCatalogProduct class you will
 * see everything is loading correctly in the IDE and code completion works for both core and
 * models for this module
 * =========================================================================================
 *
 * -----------------------------------------------------
 * EXAMPLE - OpenCart's Catalog Product Controller Models
 * Class ControllerCatalogProduct
 * -----------------------------------------------------
 * @property ModelSaleCustomerGroup       $model_sale_customer_group
 * @property ModelCatalogProduct          $model_catalog_product
 * @property ModelToolImage               $model_tool_image
 * @property ModelLocalisationLanguage    $model_localisation_language
 * @property ModelSettingStore            $model_setting_store
 * @property ModelCatalogOption           $model_catalog_option
 * @property ModelLocalisationTaxClass    $model_localisation_tax_class
 * @property ModelLocalisationStockStatus $model_localisation_stock_status
 * @property ModelLocalisationWeightClass $model_localisation_weight_class
 * @property ModelLocalisationLengthClass $model_localisation_length_class
 * @property ModelCatalogManufacturer     $model_catalog_manufacturer
 * @property ModelCatalogFilter           $model_catalog_filter
 * @property ModelCatalogAttribute        $model_catalog_attribute
 * @property ModelCatalogDownload         $model_catalog_download
 * @property ModelAccountCustomer         $model_account_customer
 * @property ModelAccountAddress          $model_account_address
 * @property ModelLocalisationCountry     $model_localisation_country
 * @property ModelSaleCustomer            $model_sale_customer
 * @property ModelLocalisationZone        $model_localisation_zone
 * @property ModelSaleOrder               $model_sale_order
 * @property ModelAccountRendelesek             $model_account_rendelesek
 * @property ModelAccountIskolaiVegzettseg      $model_account_iskolaivegzettseg
 * @property ModelReportSale              $model_report_sale
 * @property ModelReportCustomer          $model_report_customer
 * @property ModelAccountProduct                $model_account_product
 * @property ModelAccountNewsletterCategories   $model_account_newslettercategories
 * @property ModelSaleNewsletterCategories      $model_sale_newslettercategories
 * @property ModelSaleNewsSubscribers           $model_sale_newssubscribers
 * @property ModelSaleVegzettseg                $model_sale_vegzettseg
 * @property CartElhelyezkedes                  $cart_elhelyezkedes
 * @property ModelToolImage                     $resize
 * @property ModelCheckoutOrder                 $model_checkout_order
 * @property ModelCatalogProductImagePlaces     $model_catalog_productimageplaces
 * @property ModelSettingExtension              $model_setting_extension
 * @property ModelAccountElhelyezkedes          $model_account_elhelyezkedes
 * @property ModelAccountElhelyezkedesAlcsoport $model_account_elhelyezkedes_alcsoport
 * @property ModelAccountKiemelesek             $model_account_kiemelesek
 * @property ModelCatalogMultifilter            $model_catalog_multifilter
 * @property ModelAccountPostcode               $model_account_postcode
 * @property ModelAccountRememberMe             $model_account_rememberme
 * @property ModelCatalogInformation            $model_catalog_information
 * @property ModelAccountKategoriaJelentes      $getCategoryTotals
 * @property ModelAccountKategoriaJelentes      $getCategory
 * @property ModelModuleArajanlat               $model_module_arajanlat
 * @property ModelAccountArajanlat              $model_account_arajanlat
 * @property ModelCatalogMultiSzurok            $model_catalog_multi_szurok
 * @property ModelCatalogMultiKozosites         $model_catalog_multi_kozosites
 * @property ModelAccountNewslettersubscribe    $model_account_newslettersubscribe
 * @property ModelCsvProduct                    $model_csv_product
 * @property ModelCronWebshopMegrendelesToCrm     $model_cron_webshop_megrendeles_to_crm
 * @property ModelCronCrmSzamlaSzamlazz         $model_cron_crm_szamla_szamlazz
 * @property ModelCronCrmMegrendelesSzamlazz    $model_cron_crm_megrendeles_szamlazz
 * @property ModelCronAltalanos                 $model_cron_altalanos
 * @property ModelCronCrmVevojeloltToKapcsolat  $model_cron_crm_vevojelolt_to_kapcsolat
 * @property ModelCronEusolarVtigerProducts     $model_cron_eusolar_vtiger_products
 * @property ModelModuleCron                    $model_module_cron
 * @property ModelCronEusolarVtigerCsomag       $model_cron_eusolar_vtiger_csomag
 * @property ModelCronCrmSzamlaSzamlazzKiegyenlitve       $model_cron_crm_szamla_szamlazz_kiegyenlitve
 * @property ModelModuleArgep                   $model_module_argep
 * @property ModelModuleMultiCategory           $model_module_multi_category
 * @property ModelModuleTermekIntegracio        $model_module_termek_integracio
 * @property ModelCronAltalanosTermekIntegracio $model_cron_altalanos_termek_integracio
 * @property ModelCronLehetosegekToEngedelyeztetes $model_cron_lehetosegek_to_engedelyeztetes
 * @property ModelCatalogArajanlatotKerek       $model_catalog_arajanlatot_kerek
 * @property ModelSalePreOrder                  $model_sale_pre_order
 * @property ModelCatalogCrmKapcsolat           $model_catalog_crm_kapcsolat
 * @property ModelCronCrmMegrendelesSzamlazzKiegyenlitve           $model_cron_crm_megrendeles_szamlazz_kiegyenlitve
 * @property ModelModuleErkeztetooldal           $model_module_erkezteto_oldal


 * -----------------------------------------------------
 * EXAMPLE - A couple of OpenCart's Catalog Category Controller Models
 * -----------------------------------------------------
 * @property ModelCatalogCategory         $model_catalog_category
 * @property ModelSettingSetting          $model_setting_setting
 * @property ModelDesignLayout            $model_design_layout
 *
 * =========================================================================================
 * =========================================================================================
 * =========================================================================================
 * =========================================================================================
 * -----------------------------------------------------
 * OpenCart Library Classes
 * -----------------------------------------------------
 * @property Affiliate                    $affiliate
 * @property Cache                        $cache
 * @property Captcha                      $captcha
 * @property Cart                         $cart
 * @property Config                       $config
 * @property Currency                     $currency
 * @property Customer                     $customer
 * @property MySQL                        $db
 * @property Document                     $document
 * @property Encryption                   $encryption
 * @property Loader                       $load
 * @property Image                        $image
 * @property Language                     $language
 * @property Length                       $length
 * @property Log                          $log
 * @property Mail                         $mail
 * @property Pagination                   $pagination
 * @property Request                      $request
 * @property Response                     $response
 * @property Session                      $session
 * @property Tax                          $tax
 * @property Template                     $template
 * @property Url                          $url
 * @property User                         $user
 * @property Weight                       $weight
 *
 * -----------------------------------------------------
 * The actual Controller Class properties and methods
 * -----------------------------------------------------
 * @property                              $registry
 * @property                              $id
 * @property                              $layout
 * @property                              $children
 * @property                              $data
 * @property                              $output
 *
 */

abstract class Controller {
    /**
     * @var Registry $registry
     */
    protected $registry;
	protected $id;
	protected $layout;
	protected $template;
	protected $children = array();
	protected $data = array();
	protected $output;
	
	public function __construct($registry) {
		$this->registry = $registry;
	}
	
	public function __get($key) {
		return $this->registry->get($key);
	}
	
	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
			
	protected function forward($route, $args = array()) {
		return new Action($route, $args);
	}

	protected function redirect($url, $status = 302) {
		header('Status: ' . $status);
		header('Location: ' . str_replace('&amp;', '&', $url));
		exit();
	}
	
	protected function getChild($child, $args = array()) {
		$action = new Action($child, $args);
		$file = $action->getFile();
		$class = $action->getClass();
		$method = $action->getMethod();
	
		if (file_exists($file)) {
			require_once($file);

			$controller = new $class($this->registry);
			
			$controller->$method($args);
			
			return $controller->output;
		} else {
			trigger_error('Error: Could not load controller ' . $child . '!');
			exit();					
		}		
	}
	
	protected function render() {
		foreach ($this->children as $child) {
			$this->data[basename($child)] = $this->getChild($child);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->template)) {
			extract($this->data);
			
      		ob_start();
            if (isset($banners)){

            }
	  		require(DIR_TEMPLATE . $this->template);
      
	  		$this->output = ob_get_contents();

      		ob_end_clean();
      		
			return $this->output;
    	} else {
			trigger_error('Error: Could not load template ' . DIR_TEMPLATE . $this->template . '!');
			exit();				
    	}
	}
}
?>