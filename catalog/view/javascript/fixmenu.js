/* Fix menü az oldal tetején */
$(document).ready(function(){
    $(document).scroll(function() {
        if($("body").width() >= 1237){

            var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
            var oldalmagas = $(document).height();
            var legordul = ($("#header").height())-$("#menu").height();
            var content = $("#menu").html();
            if ($(document).scrollTop() >= legordul) {
                var menuszelesseg = $("#menu").width();
                var menumagas = $("#menu").height();
                var menuleft = $("#menu").offset();
                var menumagassag = $("#menu").height();
                if((!$(".menutorolni").length)){
                $('#menu').replaceWith("<div class='menutorolni' style='width: 100%;'><div id='menu'>"+content+"</div></div>");
                }
                $(".menutorolni").css('position', 'fixed');
                $(".menutorolni").css('top', '0px');
                $(".menutorolni").css('left', '0px');
                $(".menutorolni").css('background', '#a0d36e');
                $(".menutorolni").css('height', menumagassag);
                if($("#menu").css('position')!='absolute'){
                $("#menu").css('position', 'absolute');
                $("#menu").css('left', menuleft.left+"px");
                $("#menu").css('top', 0+"px");
                $("#menu").css('margin-left', 0+"px");
                $("#menu").css('display', 'table');
                }
                if((!$(".torolni").length)){
                $(".header_jobboldal").append("<div class='torolni' style='width: "+menuszelesseg+"px; height: "+menumagassag+"px;'></div>");
                }
            } else {
                if(($(".menutorolni").length)){
                $(".menutorolni").css('position', '');
                $(".menutorolni").css('top', '');
                $(".menutorolni").css('backround-color', '');
                $( ".menutorolni" ).remove();
                if(menumagassag){var magas = menumagassag;}
                    $(".header_jobboldal").append("<div id='menu' style=''>"+content+"</div>");
                }
                $( ".torolni" ).remove();
            }
        }
    });
    $(window).resize(function(){
        var menuleft = $("#content").offset();
        var productleft = $("#content .box-product").offset();
        if($("body").width() >= 1237){
            $("#menu").css('left', menuleft.left+"px");
            $(".marad2").css("left",productleft.left);
        }
    });
});
/* Vége */