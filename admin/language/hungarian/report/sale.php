<?php
// Heading
$_['heading_title']     = 'Eladások jelentése';

// Text
$_['text_year']         = 'Év';
$_['text_month']        = 'Hónap';
$_['text_week']         = 'Hét';
$_['text_day']          = 'Nap';
$_['text_all_status']   = 'Minden állapot';

// Column
$_['column_date_start'] = 'Kezdődátum';
$_['column_date_end']   = 'Végdátum';
$_['column_orders']     = 'Rendelésszám';
$_['column_total']      = 'Összesen';

// Entry
$_['entry_status']      = 'Állapot:';
$_['entry_date_start']  = 'Dátum kezdés:';
$_['entry_date_end']    = 'Dátum vége:';
$_['entry_group']       = 'Csoport szerint:';
?>
