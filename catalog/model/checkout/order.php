<?php
date_default_timezone_set('Europe/Budapest');
/**
 * @property mixed db
 */
class ModelCheckoutOrder extends Model {
	public function addOrder($data) {

        $eletkor = isset($data['eletkor']) ? $this->db->escape($data['eletkor']) : "";
        $nem     = isset($data['nem']) ? $this->db->escape($data['nem']) : "";
        $iskolai_vegzettseg     = isset($data['iskolai_vegzettseg']) ? $this->db->escape($data['iskolai_vegzettseg']) : "";

        $shipping_vallalkozasi_forma         = isset($data['shipping_vallalkozasi_forma']) ? $this->db->escape($data['shipping_vallalkozasi_forma']) : "";
        $shipping_szekhely                   = isset($data['shipping_szekhely']) ? $this->db->escape($data['shipping_szekhely']) : "";
        $shipping_ugyvezeto_neve             = isset($data['shipping_ugyvezeto_neve']) ? $this->db->escape($data['shipping_ugyvezeto_neve']) : "";
        $shipping_ugyvezeto_telefonszama     = isset($data['shipping_ugyvezeto_telefonszama']) ? $this->db->escape($data['shipping_ugyvezeto_telefonszama']) : "";

        $payment_vallalkozasi_forma          = isset($data['payment_vallalkozasi_forma']) ? $this->db->escape($data['payment_vallalkozasi_forma']) : "";
        $payment_szekhely                    = isset($data['payment_szekhely']) ? $this->db->escape($data['payment_szekhely']) : "";
        $payment_ugyvezeto_neve              = isset($data['payment_ugyvezeto_neve']) ? $this->db->escape($data['payment_ugyvezeto_neve']) : "";
        $payment_ugyvezeto_telefonszama      = isset($data['payment_ugyvezeto_telefonszama']) ? $this->db->escape($data['payment_ugyvezeto_telefonszama']) : "";
        $vallalkozasi_forma                  = isset($data['vallalkozasi_forma']) ? $this->db->escape($data['vallalkozasi_forma']) : "";
        $szekhely                            = isset($data['szekhely']) ? $this->db->escape($data['szekhely']) : "";
        $ugyvezeto_neve                      = isset($data['ugyvezeto_neve']) ? $this->db->escape($data['ugyvezeto_neve']) : "";
        $ugyvezeto_telefonszama              = isset($data['ugyvezeto_telefonszama']) ? $this->db->escape($data['ugyvezeto_telefonszama']) : "";
        $weblap                              = isset($data['weblap']) ? $this->db->escape($data['weblap']) : "";

        $this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "',
		                                store_id                        = '" . (int)$data['store_id'] . "',
		                                store_name                      = '" . $this->db->escape($data['store_name']) . "',
		                                store_url                       = '" . $this->db->escape($data['store_url']) . "',
		                                customer_id                     = '" . (int)$data['customer_id'] . "',
		                                customer_group_id               = '" . (int)$data['customer_group_id'] . "',
		                                firstname                       = '" . $this->db->escape($data['firstname']) . "',
		                                lastname                        = '" . $this->db->escape($data['lastname']) . "',
		                                email                           = '" . $this->db->escape($data['email']) . "',
		                                telephone                       = '" . $this->db->escape($data['telephone']) . "',
		                                fax                             = '" . $this->db->escape($data['fax']) . "',
		                                nem                             = '" . $nem . "',
		                                eletkor                         = '" . $eletkor . "',
		                                shipping_firstname              = '" . $this->db->escape($data['shipping_firstname']) . "',
		                                shipping_lastname               = '" . $this->db->escape($data['shipping_lastname']) . "',
		                                shipping_company                = '" . $this->db->escape($data['shipping_company']) . "',
		                                shipping_adoszam                = '" . $this->db->escape($data['shipping_adoszam']) . "',
		                                shipping_address_1              = '" . $this->db->escape($data['shipping_address_1']) . "',
		                                shipping_address_2              = '" . $this->db->escape($data['shipping_address_2']) . "',
		                                shipping_city                   = '" . $this->db->escape($data['shipping_city']) . "',
		                                shipping_postcode               = '" . $this->db->escape($data['shipping_postcode']) . "',
		                                shipping_country                = '" . $this->db->escape($data['shipping_country']) . "',
		                                shipping_country_id             = '" . (int)$data['shipping_country_id'] . "',
		                                shipping_zone                   = '" . $this->db->escape($data['shipping_zone']) . "',
		                                shipping_zone_id                = '" . (int)$data['shipping_zone_id'] . "',
		                                shipping_address_format         = '" . $this->db->escape($data['shipping_address_format']) . "',
		                                shipping_method                 = '" . $this->db->escape($data['shipping_method']) . "',
		                                shipping_gls_pont_shopid       = '" . (isset($data['shipping_gls_pont_shopid']) ? $data['shipping_gls_pont_shopid'] : '') . "',
		                                shipping_gls_pont_name       = '" . (isset($data['shipping_gls_pont_name']) ? $data['shipping_gls_pont_name'] : '') . "',
		                                shipping_gls_pont_city       = '" . (isset($data['shipping_gls_pont_city']) ? $data['shipping_gls_pont_city'] : '') . "',
		                                shipping_gls_pont_address      = '" . (isset($data['shipping_gls_pont_address']) ? $data['shipping_gls_pont_address'] : '') . "',
		                                shipping_gls_pont_zipcode       = '" . (isset($data['shipping_gls_pont_zipcode']) ? $data['shipping_gls_pont_zipcode'] : '') . "',
		                                shipping_warehouse_id           = '" . (isset($data['shipping_warehouse_id']) ? $data['shipping_warehouse_id'] : 0) . "',
		                                shipping_warehouse_name         = '" . (isset($data['shipping_warehouse_name']) ? $data['shipping_warehouse_name'] : '') . "',
		                                shipping_warehouse_email        = '" . (isset($data['shipping_warehouse_email']) ? $data['shipping_warehouse_email'] : '') . "',
		                                shipping_code                   = '" . $this->db->escape($data['shipping_code']) . "',
		                                shipping_vallalkozasi_forma     = '" . $shipping_vallalkozasi_forma . "',
      	                                shipping_szekhely               = '" . $shipping_szekhely . "',
      	                                shipping_ugyvezeto_neve         = '" . $shipping_ugyvezeto_neve . "',
      	                                shipping_ugyvezeto_telefonszama = '" . $shipping_ugyvezeto_telefonszama . "',
		                                payment_firstname               = '" . $this->db->escape($data['payment_firstname']) . "',
		                                payment_lastname                = '" . $this->db->escape($data['payment_lastname']) . "',
		                                payment_company                 = '" . $this->db->escape($data['payment_company']) . "',
		                                payment_adoszam                 = '" . $this->db->escape($data['payment_adoszam']) . "',
		                                payment_address_1               = '" . $this->db->escape($data['payment_address_1']) . "',
		                                payment_address_2               = '" . $this->db->escape($data['payment_address_2']) . "',
		                                payment_city                    = '" . $this->db->escape($data['payment_city']) . "',
		                                payment_postcode                = '" . $this->db->escape($data['payment_postcode']) . "',
		                                payment_country                 = '" . $this->db->escape($data['payment_country']) . "',
		                                payment_country_id              = '" . (int)$data['payment_country_id'] . "',
		                                payment_zone                    = '" . $this->db->escape($data['payment_zone']) . "',
		                                payment_zone_id                 = '" . (int)$data['payment_zone_id'] . "',
		                                payment_address_format          = '" . $this->db->escape($data['payment_address_format']) . "',
		                                payment_method                  = '" . $this->db->escape($data['payment_method']) . "',
		                                payment_code                    = '" . $this->db->escape($data['payment_code']) . "',
		                                payment_vallalkozasi_forma      = '" . $payment_vallalkozasi_forma . "',
      	                                payment_szekhely                = '" . $payment_szekhely . "',
      	                                payment_ugyvezeto_neve          = '" . $payment_ugyvezeto_neve . "',
      	                                payment_ugyvezeto_telefonszama  = '" . $payment_ugyvezeto_telefonszama . "',
		                                comment                         = '" . (isset($data['comment']) ? $this->db->escape($data['comment']) : '') . "',
		                                total                           = '" . (float)$data['total'] . "',
		                                affiliate_id                    = '" . (int)$data['affiliate_id'] . "',
		                                commission                      = '" . (float)$data['commission'] . "',
		                                language_id                     = '" . (int)$data['language_id'] . "',
		                                currency_id                     = '" . (int)$data['currency_id'] . "',
		                                currency_code                   = '" . $this->db->escape($data['currency_code']) . "',
		                                currency_value                  = '" . (float)$data['currency_value'] . "',
		                                ip                              = '" . $this->db->escape($data['ip']) . "',
		                                forwarded_ip                    = '" .  $this->db->escape($data['forwarded_ip']) . "',
		                                user_agent                      = '" . $this->db->escape($data['user_agent']) . "',
		                                accept_language                 = '" . $this->db->escape($data['accept_language']) . "',
		                                iskolai_vegzettseg              = '" . $iskolai_vegzettseg . "',
		                                vallalkozasi_forma              = '" . $vallalkozasi_forma . "',
      	                                szekhely                        = '" . $szekhely . "',
      	                                ugyvezeto_neve                  = '" . $ugyvezeto_neve . "',
      	                                ugyvezeto_telefonszama          = '" . $ugyvezeto_telefonszama . "',
      	                                weblap                          = '" . $weblap . "',
		                                date_added                      = NOW(),
		                                date_modified                   = NOW()");

		$order_id = $this->db->getLastId();


        $result_quantity = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "order_option LIKE 'quantity';");
        if ($result_quantity->num_rows == 0) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "order_option ADD COLUMN quantity int(4) NOT NULL DEFAULT 0;");
        }
        foreach ($data['products'] as $product) {
            $ingyenes= isset($product['ingyenes']) ? $product['ingyenes'] : 0;

                $name       = isset($product['name'])       ? $product['name']      : "";
			    $model      = isset($product['model'])      ? $product['model']     : "";
                $quantity   = isset($product['quantity'])   ? $product['quantity']  : 0;
			    $price      = isset($product['price'])      ? $product['price']     : 0;
			    $total      = isset($product['total'])      ? $product['total']     : 0;
			    $tax        = isset($product['tax'])        ? $product['tax']       : 0;
			    $ingyenes   = isset($ingyenes)              ? $ingyenes             : 0;
			    $reward     = isset($product['reward'])     ? $product['reward']    : 0;


            $this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET
			    order_id    = '" . (int)$order_id . "',
			    product_id  = '" . (int)$product['product_id'] . "',
			    `name`      = '" . $this->db->escape($name) . "',
			    model       = '" . $this->db->escape($model) . "',
			    quantity    = '" . (int)$quantity . "',
			    price       = '" . (float)$price . "',
			    total       = '" . (float)$total . "',
			    tax         = '" . (float)$tax . "',
			    ingyenes    = '" . (int)$ingyenes . "',
			    reward      = '" . (int)$reward . "'");


			$order_product_id = $this->db->getLastId();

			foreach ($product['option'] as $option) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET quantity='".(int)$option['subtract']."', order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
			}
				
			foreach ($product['download'] as $download) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_download SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', name = '" . $this->db->escape($download['name']) . "', filename = '" . $this->db->escape($download['filename']) . "', mask = '" . $this->db->escape($download['mask']) . "', remaining = '" . (int)($download['remaining'] * $product['quantity']) . "'");
			}
		}

        if (isset($data['vouchers'])) {
		    foreach ($data['vouchers'] as $voucher) {
			    $this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");
		    }
		}

        if (isset($data['totals'])) {
		    foreach ($data['totals'] as $total) {
                $ado = isset($total['tax']) ? $total['tax'] : "";

                $this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET
			        order_id    = '" . (int)$order_id . "',
			        code        = '" . $this->db->escape($total['code']) . "',
			        title       = '" . $this->db->escape($total['title']) . "',
			        text        = '" . $this->db->escape($total['text']) . "',
			        `value`     = '" . (float)$total['value'] . "',
			        `ado`       = '" . $ado . "',
			        sort_order  = '" . (int)$total['sort_order'] . "'");
		    }
		}

		return $order_id;
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");
			
		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");
			
			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';				
			}
			
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");
			
			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}
			
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");
			
			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';				
			}
			
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");
			
			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$this->load->model('localisation/language');
			
			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);
			
			if ($language_info) {
				$language_code = $language_info['code'];
				$language_filename = $language_info['filename'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_filename = '';
				$language_directory = '';
			}
		 			
			return array(
				'order_id'                          => $order_query->row['order_id'],
				'invoice_no'                        => $order_query->row['invoice_no'],
				'invoice_prefix'                    => $order_query->row['invoice_prefix'],
				'store_id'                          => $order_query->row['store_id'],
				'store_name'                        => $order_query->row['store_name'],
				'store_url'                         => $order_query->row['store_url'],
				'customer_id'                       => $order_query->row['customer_id'],
				'firstname'                         => $order_query->row['firstname'],
				'lastname'                          => $order_query->row['lastname'],
				'telephone'                         => $order_query->row['telephone'],
				'fax'                               => $order_query->row['fax'],
				'email'                             => $order_query->row['email'],
				'weblap'                            => (isset($order_query->row['weblap']) ? $order_query->row['weblap'] : ''),
				'shipping_firstname'                => $order_query->row['shipping_firstname'],
				'shipping_lastname'                 => $order_query->row['shipping_lastname'],
				'shipping_company'                  => $order_query->row['shipping_company'],
				'shipping_adoszam'                  => $order_query->row['shipping_adoszam'],
				'shipping_address_1'                => $order_query->row['shipping_address_1'],
				'shipping_address_2'                => $order_query->row['shipping_address_2'],
				'shipping_postcode'                 => $order_query->row['shipping_postcode'],
				'shipping_city'                     => $order_query->row['shipping_city'],
				'shipping_zone_id'                  => $order_query->row['shipping_zone_id'],
				'shipping_zone'                     => $order_query->row['shipping_zone'],
				'shipping_zone_code'                => $shipping_zone_code,
				'shipping_country_id'               => $order_query->row['shipping_country_id'],
				'shipping_country'                  => $order_query->row['shipping_country'],
				'shipping_iso_code_2'               => $shipping_iso_code_2,
				'shipping_iso_code_3'               => $shipping_iso_code_3,
				'shipping_address_format'           => $order_query->row['shipping_address_format'],
				'shipping_method'                   => $order_query->row['shipping_method'],
                'shipping_gls_pont_shopid'          => $order_query->row['shipping_gls_pont_shopid'],
                'shipping_gls_pont_name'            => $order_query->row['shipping_gls_pont_name'],
                'shipping_gls_pont_address'         => $order_query->row['shipping_gls_pont_address'],
                'shipping_gls_pont_zipcode'         => $order_query->row['shipping_gls_pont_zipcode'],
                'shipping_gls_pont_city'            => $order_query->row['shipping_gls_pont_city'],
				'shipping_warehouse_id'             => $order_query->row['shipping_warehouse_id'],
				'shipping_warehouse_name'           => $order_query->row['shipping_warehouse_name'],
				'shipping_warehouse_email'          => $order_query->row['shipping_warehouse_email'],
                'shipping_vallalkozasi_forma'       => $order_query->row['shipping_vallalkozasi_forma'],
                'shipping_szekhely'                 => $order_query->row['shipping_szekhely'],
                'shipping_ugyvezeto_neve'           => $order_query->row['shipping_ugyvezeto_neve'],
                'shipping_ugyvezeto_telefonszama'   => $order_query->row['shipping_ugyvezeto_telefonszama'],
				'payment_firstname'                 => $order_query->row['payment_firstname'],
				'payment_lastname'                  => $order_query->row['payment_lastname'],
				'payment_company'                   => $order_query->row['payment_company'],
				'payment_adoszam'                   => $order_query->row['payment_adoszam'],
				'payment_address_1'                 => $order_query->row['payment_address_1'],
				'payment_address_2'                 => $order_query->row['payment_address_2'],
				'payment_postcode'                  => $order_query->row['payment_postcode'],
				'payment_city'                      => $order_query->row['payment_city'],
				'payment_zone_id'                   => $order_query->row['payment_zone_id'],
				'payment_zone'                      => $order_query->row['payment_zone'],
				'payment_zone_code'                 => $payment_zone_code,
				'payment_country_id'                => $order_query->row['payment_country_id'],
				'payment_country'                   => $order_query->row['payment_country'],
				'payment_iso_code_2'                => $payment_iso_code_2,
				'payment_iso_code_3'                => $payment_iso_code_3,
				'payment_address_format'            => $order_query->row['payment_address_format'],
				'payment_method'                    => $order_query->row['payment_method'],
                'payment_vallalkozasi_forma'        => $order_query->row['payment_vallalkozasi_forma'],
                'payment_szekhely'                  => $order_query->row['payment_szekhely'],
                'payment_ugyvezeto_neve'            => $order_query->row['payment_ugyvezeto_neve'],
                'payment_ugyvezeto_telefonszama'    => $order_query->row['payment_ugyvezeto_telefonszama'],
                'vallalkozasi_forma'                => $order_query->row['vallalkozasi_forma'],
                'szekhely'                          => $order_query->row['szekhely'],
                'ugyvezeto_neve'                    => $order_query->row['ugyvezeto_neve'],
                'ugyvezeto_telefonszama'            => $order_query->row['ugyvezeto_telefonszama'],
				'comment'                           => $order_query->row['comment'],
				'total'                             => $order_query->row['total'],
				'order_status_id'                   => $order_query->row['order_status_id'],
				'order_status'                      => $order_query->row['order_status'],
				'language_id'                       => $order_query->row['language_id'],
				'language_code'                     => $language_code,
				'language_filename'                 => $language_filename,
				'language_directory'                => $language_directory,
				'currency_id'                       => $order_query->row['currency_id'],
				'currency_code'                     => $order_query->row['currency_code'],
				'currency_value'                    => $order_query->row['currency_value'],
				'ip'                                => $order_query->row['ip'],
				'forwarded_ip'                      => $order_query->row['forwarded_ip'],
				'user_agent'                        => $order_query->row['user_agent'],
				'accept_language'                   => $order_query->row['accept_language'],
                'iskolai_vegzettseg'                => $order_query->row['iskolai_vegzettseg'],
				'date_modified'                     => $order_query->row['date_modified'],
				'date_added'                        => $order_query->row['date_added']
			);
		} else {
			return false;	
		}
	}

    public function getOrderElhelyezkedes() {

        $this->load->model('account/address');
        $payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);

        if ($payment_address) {
            $language_id = $this->config->get('config_language_id');
            $this->load->model('localisation/language');

            $language_info = $this->model_localisation_language->getLanguage($language_id);

            if ($language_info) {
                $language_code = $language_info['code'];
                $language_filename = $language_info['filename'];
                $language_directory = $language_info['directory'];
            } else {
                $language_code = '';
                $language_filename = '';
                $language_directory = '';
            }


            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                $forwarded_ip = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif(!empty($this->request->server['HTTP_CLIENT_IP'])) {
                $forwarded_ip = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                $forwarded_ip = '';
            }

            if (isset($this->request->server['HTTP_USER_AGENT'])) {
                $user_agent = $this->request->server['HTTP_USER_AGENT'];
            } else {
                $user_agent = '';
            }

            if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                $accept_language = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
            } else {
                $accept_language = '';
            }

            $this->db->query("INSERT INTO `" . DB_PREFIX . "order` set  date_added              = NOW(), date_modified = NOW()");
    		$order_id = $this->db->getLastId();

            $this->session->data['order_id'] = $order_id;

            return array(
                'order_id'                => $order_id,
                'invoice_no'              => $order_id,
                'invoice_prefix'          => "Elhelyezkedes",
                'store_id'                => $this->config->get("config_store_id"),
                'store_name'              => $this->config->get("config_name"),
                'store_url'               => $this->config->get('config_url') ? $this->config->get('config_url') : HTTP_SERVER,
                'customer_id'             => $this->customer->getId(),
                'firstname'               => $this->customer->getFirstName(),
                'lastname'                => $this->customer->getLastName(),
                'telephone'               => $this->customer->getTelephone(),
                'fax'                     => $this->customer->getFax(),
                'email'                   => $this->customer->getEmail(),
                'vallalkozasi_forma'      => $this->customer->getVallalkozasiForma(),
                'szekhely'                => $this->customer->getSzekhely(),
                'ugyvezeto_neve'          => $this->customer->getUgyvezetoNeve(),
                'ugyvezeto_telefonszama'  => $this->customer->getUgyvezetoTelefonszama(),
                'customer_group_id'       => $this->customer->getCustomerGroupId(),
                'nem'                     => $this->customer->getNem(),
                'eletkor'                 => $this->customer->getEletkor(),
                'iskolai_vegzettseg'      => $this->customer->getIskolaiVegzettseg(),
                'weblap'                  => $this->customer->getWeblap(),
                'shipping_firstname'      => "",
                'shipping_lastname'       => "",
                'shipping_company'        => "",
                'shipping_adoszam'        => "",
                'shipping_address_1'      => "",
                'shipping_address_2'      => "",
                'shipping_postcode'       => "",
                'shipping_city'           => "",
                'shipping_zone_id'        => "",
                'shipping_zone'           => "",
                'shipping_zone_code'      => "",
                'shipping_country_id'     => "",
                'shipping_country'        => "",
                'shipping_iso_code_2'     => "",
                'shipping_iso_code_3'     => "",
                'shipping_address_format' => "",
                'shipping_method'         => "",
                'shipping_vallalkozasi_forma'       => "",
                'shipping_szekhely'                 => "",
                'shipping_ugyvezeto_neve'           => "",
                'shipping_ugyvezeto_telefonszama'   => "",

                'payment_firstname'       => isset($payment_address['firstname'])      ? $payment_address['firstname'] : "",
                'payment_lastname'        => isset($payment_address['lastname'])       ? $payment_address['lastname'] : "",
                'payment_company'         => isset($payment_address['company'])        ? $payment_address['company'] : "",
                'payment_adoszam'         => isset($payment_address['adoszam'])        ? $payment_address['adoszam'] : "",
                'payment_address_1'       => isset($payment_address['address_1'])      ? $payment_address['address_1'] : "",
                'payment_address_2'       => isset($payment_address['address_2'])      ? $payment_address['address_2'] : "",
                'payment_postcode'        => isset($payment_address['postcode'])       ? $payment_address['postcode'] : "",
                'payment_city'            => isset($payment_address['city'])           ? $payment_address['city'] : "",
                'payment_zone_id'         => isset($payment_address['zone_id'])        ? $payment_address['zone_id'] : "",
                'payment_zone'            => isset($payment_address['zone'])           ? $payment_address['zone'] : "",
                'payment_zone_code'       => isset($payment_address['zone_code'])      ? $payment_address['zone_code'] : "",
                'payment_country_id'      => isset($payment_address['country_id'])     ? $payment_address['country_id'] : "",
                'payment_country'         => isset($payment_address['country'])        ? $payment_address['country'] : "",
                'payment_iso_code_2'      => isset($payment_address['iso_code_2'])     ? $payment_address['iso_code_2'] : "",
                'payment_iso_code_3'      => isset($payment_address['iso_code_3'])     ? $payment_address['iso_code_3'] : "",
                'payment_address_format'  => isset($payment_address['address_format']) ? $payment_address['address_format'] : "",

                'payment_method'          => $this->session->data['elhelyezkedes']['payment_method'],
                'payment_vallalkozasi_forma'        => isset($payment_address['vallalkozasi_forma'])     ? $payment_address['vallalkozasi_forma'] : "",
                'payment_szekhely'                  => isset($payment_address['szekhely'])               ? $payment_address['szekhely'] : "",
                'payment_ugyvezeto_neve'            => isset($payment_address['ugyvezeto_neve'])         ? $payment_address['ugyvezeto_neve'] : "",
                'payment_ugyvezeto_telefonszama'    => isset($payment_address['ugyvezeto_telefonszama']) ? $payment_address['ugyvezeto_telefonszama'] : "",

                'language_code'           => $language_code,
                'language_filename'       => $language_filename,
                'language_directory'      => $language_directory,

                'comment'                 => $this->session->data['elhelyezkedes']['comment'],
                'language_id'             => $language_id,

                'currency_id'             => $this->currency->getId(),
                'currency_code'           => $this->currency->getCode(),
                'currency_value'          => $this->currency->getValue($this->currency->getCode()),
                'ip'                      => $this->request->server['REMOTE_ADDR'],
                'forwarded_ip'            => $forwarded_ip,

                'user_agent'              => $user_agent,
                'accept_language'         => $accept_language,
                'date_modified'           => date("Y-m-d"),
                'date_added'              => date("Y-m-d"),
                'total'                   => $this->getTotalsElhelyezkedes(),
                'order_status_id'         => 0,
                'order_status'            => 0

            );

        } else {
            return false;
        }
    }

    public function updateElhelyezkedes () {
        $kiir["penzugyi_status"] = $this->config->get("penzugyi_status");
        foreach ($kiir["penzugyi_status"] as $value) {
            if ($value['alapertelmezett_sor'] == 1) {
                $penzugyi_status_id = $value['penzugyi_status_id'];
            }
        }

        $kiir["fizetes_elbiralas"] = $this->config->get("fizetes_elbiralas");
        foreach ($kiir["fizetes_elbiralas"] as $value) {
            if ($value['alapertelmezett_sor'] == 1) {
                $fizetes_elbiralas_status_id = $value['fizetes_elbiralas_status_id'];
            }
        }


        foreach ($this->cart_elhelyezkedes->getProducts() as $product) {
            foreach ($product['kiemelesek'] as $key=>$value) {
                $this->db->query("UPDATE " . DB_PREFIX . "product_elhelyezes SET
                    penzugyi_status_id          = '" .$penzugyi_status_id. "',
                    fizetes_elbiralas_status_id = '" .$fizetes_elbiralas_status_id. "'
                    WHERE product_id = '" .$product['key']. "' AND sor = '" .$key. "'");

            }
        }

        $this->cart_elhelyezkedes->clear();

        unset($this->session->data['cart_elhelyezkedes']);
        unset($this->session->data['elhelyezkedes']);

    }

    public function getProductTotalElhelyezkedes ($product_id) {

        $total = 0;
        foreach ($this->session->data['cart_elhelyezkedes'][$product_id] as $value) {
            $total += $value['brutto'];
        }

        return $total;
    }

    public function getTotalsElhelyezkedes() {

        $total = 0;
        foreach ($this->session->data['cart_elhelyezkedes'] as $termek) {
            foreach($termek as $value) {
                $total += $value['brutto'];
            }
        }

        return $total;
    }

    public function confirm($order_id, $order_status_id, $comment = '', $notify = false, $letoltes = false) {


        $megjelenit = $this->config->get('megjelenit_admin_rendeles');


        if (isset($megjelenit['sap_adatkuldes_hungarosack']) && $megjelenit['sap_adatkuldes_hungarosack'] == 1) {
            $this->Sap_nak($order_id,$order_status_id);
        } elseif (!isset($megjelenit['sap_adatkuldes_hungarosack']) ) {
            $this->Sap_nak($order_id,$order_status_id);
        }


		$order_info = $this->getOrder($order_id);

		if ($order_info && (!$order_info['order_status_id'] || $letoltes) ) {
			// Fraud Detection
			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');
				
				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);
				
				if ($risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}

			// Blacklist
			$status = false;
			
			$this->load->model('account/customer');
			
			if ($order_info['customer_id']) {
				$results = $this->model_account_customer->getIps($order_info['customer_id']);
				
				foreach ($results as $result) {
					if ($this->model_account_customer->isBlacklisted($result['ip'])) {
						$status = true;
						
						break;
					}
				}
			} else {
				$status = $this->model_account_customer->isBlacklisted($order_info['ip']);
			}
			
			if ($status) {
				$order_status_id = $this->config->get('config_order_status_id');
			}		
				
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET
			    order_status_id = '" . (int)$order_status_id . "',
			    tranzakcio_azonosito = '" . $comment . "',
			    date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET
			    order_id = '" . (int)$order_id . "',
			    order_status_id = '" . (int)$order_status_id . "',
			    notify = '1',
			    comment = '" . $this->db->escape(($comment && $notify) ? $comment : '') . "',
			    date_added = NOW()");

			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
            $letoltheto_vonalkod = false;


            if (isset($megjelenit['csv_integracio']) && $megjelenit['csv_integracio'] == 1) {
                if (isset($megjelenit['csv_integracio_csoportnev']) && !empty($megjelenit['csv_integracio_csoportnev']) ) {


                    $csv_beallitasok = $this->config->get("csv_beallitasok");
                    $csv_sor = false;
                    foreach($csv_beallitasok as $value) {
                        if ($value['export_name']  == $megjelenit['csv_integracio_csoportnev']) {
                            $csv_sor = $value;
                            break;
                        }
                    }
                    if ($csv_sor) {
                        $this->vasarlasrolCsv($order_id,$megjelenit['csv_integracio_csoportnev']);
                    }
                }
            }

            foreach ($order_product_query->rows as $order_product) {
                if ($this->config->get('megjelenit_vonalkod_kuldes') == 1) {
                    $product_query = $this->db->query("SELECT utalvany FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$order_product['product_id'] . "'");
                    if ($product_query->row['utalvany'] == 0) {
                        for ($i = 1; $i<= $order_product['quantity']; $i++) {
                            $this->vonalkodKeszit($order_product['order_product_id'],$order_product['order_id'],$order_product['product_id'],$order_status_id);
                            $letoltheto_vonalkod = true;
                        }
                    }
                }

                $sql = "SELECT * FROM " . DB_PREFIX . "product_package  WHERE product_id = '" . (int)$order_product['product_id'] . "'";
                $query = $this->db->query($sql);
                if ($query->num_rows > 0) {
                    foreach($query->rows as $value) {
                        $this->db->query("UPDATE " . DB_PREFIX . "product SET
                            quantity = (quantity - " . (int)$value['quantity']*(int)$order_product['quantity'] . ")
                            WHERE product_id = '" . (int)$value['package_id'] . "'
                                AND subtract = '1'");
                    }

                }

                $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");

				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");
			
				foreach ($order_option_query->rows as $option) {
                    $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$order_product['quantity'] * (int)$option['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract > '0'");

                    //$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
				}
			}
			
			$this->cache->delete('product');
			
			// Downloads
			$order_download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");
			
			// Gift Voucher
			$this->load->model('checkout/voucher');
			
			$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
			
			foreach ($order_voucher_query->rows as $order_voucher) {
				$voucher_id = $this->model_checkout_voucher->addVoucher($order_id, $order_voucher);
				
				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher['order_voucher_id'] . "'");
			}			
			
			// Send out any gift voucher mails
			if ($this->config->get('config_complete_status_id') == $order_status_id) {
				$this->model_checkout_voucher->confirm($order_id);
			}
					
			// Order Totals			
			$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");
			
			foreach ($order_total_query->rows as $order_total) {
				$this->load->model('total/' . $order_total['code']);
				
				if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
					$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
				}
			}
			
			// Send out order confirmation mail
			$language = new Language($order_info['language_directory'],$this->config->get('config_template'));
			$language->load($order_info['language_filename']);
			$language->load('mail/order');
		 
			$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
			
			if ($order_status_query->num_rows) {
				$order_status = $order_status_query->row['name'];	
			} else {
				$order_status = '';
			}
			
			$subject = sprintf($language->get('text_new_subject'), $order_info['store_name'], $order_id);
		
			// HTML Mail
			$template = new Template();

			$template->data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
			
			$template->data['text_greeting']        = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
			$template->data['text_link']            = $language->get('text_new_link');
			$template->data['text_download']        = $language->get('text_new_download');
			$template->data['text_order_detail']    = $language->get('text_new_order_detail');
			$template->data['text_instruction']     = $language->get('text_new_instruction');
			$template->data['text_order_id']        = $language->get('text_new_order_id');
			$template->data['text_date_added']      = $language->get('text_new_date_added');
			$template->data['text_payment_method']  = $language->get('text_new_payment_method');
			$template->data['text_shipping_method'] = $language->get('text_new_shipping_method');
			$template->data['text_shipping_warehouse'] = $language->get('text_shipping_warehouse');
			$template->data['text_shipping_gls_pont'] = $language->get('text_shipping_gls_pont');
			$template->data['text_email']           = $language->get('text_new_email');
			$template->data['text_telephone']       = $language->get('text_new_telephone');
			$template->data['text_ip']              = $language->get('text_new_ip');
			$template->data['text_payment_address'] = $language->get('text_new_payment_address');
			$template->data['text_shipping_address'] = $language->get('text_new_shipping_address');
			$template->data['text_product']         = $language->get('text_new_product');
			$template->data['text_model']           = $language->get('text_new_model');
			$template->data['text_quantity']        = $language->get('text_new_quantity');
			$template->data['text_price']           = $language->get('text_new_price');
			$template->data['text_total']           = $language->get('text_new_total');
			$template->data['text_footer']          = $language->get('text_new_footer');
			$template->data['text_powered']         = $language->get('text_new_powered');
			$template->data['text_megjegyzes']      = $language->get('text_megjegyzes');

            $template->data['megjelenit_kosar'] = $this->config->get('megjelenit_admin_kosar');
            $template->data['column_netto'] = $language->get('column_netto');
            $template->data['termek_ar_osszes'] = $language->get('termek_ar_osszes');
            $template->data['column_netto_total_price'] = $language->get('column_netto_total_price');
            $template->data['column_price_netto_ar'] = $language->get('column_price_netto_ar');
            $template->data['column_total_netto_ar'] = $language->get('column_total_netto_ar');

            $template->data['netto_osszes'] = $language->get('netto_osszes');
            $template->data['afa_osszes'] = $language->get('afa_osszes');
            $template->data['fizetendo_osszes'] = $language->get('fizetendo_osszes');

            $template->data['column_total'] = $language->get('column_total');
            $template->data['szallitas'] = $language->get('szallitas');
            $template->data['szallitas_netto_ar'] = $language->get('szallitas_netto_ar');
            $template->data['szallitas_brutto_ar'] = $language->get('szallitas_brutto_ar');

			$template->data['logo'] = HTTP_IMAGE . $this->config->get('config_logo');		
			$template->data['store_name'] = $order_info['store_name'];
			$template->data['store_url'] = $order_info['store_url'];
			$template->data['customer_id'] = $order_info['customer_id'];
			$template->data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
			
			if ($order_download_query->num_rows) {
				$template->data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
			} else {
				$template->data['download'] = '';
			}

            if ($letoltheto_vonalkod) {
                $template->data['letoltheto_vonalkod'] = $order_info['store_url'] . 'index.php?route=account/utalvanyok';
            } else {
                $template->data['letoltheto_vonalkod'] = '';
            }
			
			$template->data['order_id']                 = $order_id;
			$template->data['date_added']               = date($language->get('date_format_short'), strtotime($order_info['date_added']));
			$template->data['payment_method']           = $order_info['payment_method'];
			$template->data['shipping_method']          = $order_info['shipping_method'];
			$template->data['shipping_warehouse_name']  = $order_info['shipping_warehouse_name'];
			$template->data['shipping_gls_pont_name']   = $order_info['shipping_gls_pont_name'];
			$template->data['shipping_gls_pont_name']   = $order_info['shipping_gls_pont_name'];
			$template->data['shipping_gls_pont_city']   = $order_info['shipping_gls_pont_city'];
			$template->data['shipping_gls_pont_address']= $order_info['shipping_gls_pont_address'];
			$template->data['shipping_gls_pont_zipcode']= $order_info['shipping_gls_pont_zipcode'];
			$template->data['email']                    = $order_info['email'];
			$template->data['telephone']                = $order_info['telephone'];
			$template->data['ip']                       = $order_info['ip'];
			$template->data['megjegyzes']               = $order_info['comment'];

			if ($comment && $notify) {
				$template->data['comment'] = nl2br($comment);
			} else {
				$template->data['comment'] = '';
			}
									
			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{adoszam}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{adoszam}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
		
			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'adoszam'   => $order_info['shipping_adoszam'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']  
			);
		
			$template->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{adoszam}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{adoszam}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
		
			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'adoszam'   => $order_info['payment_adoszam'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']  
			);
		
			$template->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			// Products
			$template->data['products'] = array();
            $products2 = $this->cart->getProducts();



			foreach ($order_product_query->rows as $product) {
                $products2 =  $this->cart->getProducts();
                foreach($products2 as $key=>$termek2) {
                    if ( strpos($key,":") ) {
                        unset ($products2[$key]);
                        $products2[$product['product_id']] = $termek2;
                    }
                }
				$option_data = array();
                $products_brutto = 0;
                $products_netto = 0;

				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
				
				foreach ($order_option_query->rows as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 40 ? utf8_substr($value, 0, 40) . '...' : $value)
					);					
				}
                if($product['ingyenes'] == '0') {
                    $netto_price = $this->currency->format(($product['price']));
                    $netto_total_price = $this->currency->format(($product['total']));
                    $netto = isset($_SESSION['shipping_method']['netto_text']) ? $_SESSION['shipping_method']['netto_text'] : "";
                    $brutto = isset($_SESSION['shipping_method']['text']) ? $_SESSION['shipping_method']['text'] : "";

                    if (isset($products2[$product['product_id']])) {
                        $price = $this->currency->format($this->tax->calculate($product['price'], $products2[$product['product_id']]['tax_class_id'], $this->config->get('config_tax')));
                        $total = $this->currency->format($this->tax->calculate($product['total'], $products2[$product['product_id']]['tax_class_id'], $this->config->get('config_tax')));
                        $tax = $products2[$product['product_id']]['tax_class_id'];
                    } else {
                        $price  = $this->currency->format((int)$product['price']+((int)$product['tax']/(int)$product['quantity']));
                        $total  = $this->currency->format($product['total']+(int)$product['tax']);
                        $tax    = $this->currency->format($product['tax']);
                    }




                } else {
                    $netto_price = $this->currency->format(0);
                    $netto_total_price = $this->currency->format(0);
                    $netto = 0;
                    $brutto = 0;
                    $price = $price = $this->currency->format(0);
                    $total = $total = $this->currency->format(0);
                    $tax = 0;
                }

                $sql = "SELECT cikkszam, manufacturer_id, cikkszam2 FROM `" .DB_PREFIX. "product` WHERE product_id='" .(int)$product['product_id']."'";
                $product_egyeb = $this->db->query($sql);

                $this->load->model("catalog/manufacturer");
                $manufacturer = $this->model_catalog_manufacturer->getManufacturer($product_egyeb->row['manufacturer_id']);
                $manufacturer = isset($manufacturer['name']) && $manufacturer['name'] ? $manufacturer['name'] : "";
                $template->data['megjelenit_product'] = $this->config->get('megjelenit_product');

				$template->data['products'][] = array(
					'name'              => $product['name'],
					'model'             => $product['model'],
					'cikkszam'          => $product_egyeb->row['cikkszam'],
					'cikkszam2'          => $product_egyeb->row['cikkszam2'],
					'manufacturer'   => $manufacturer,
					'option'            => $option_data,
					'quantity'          => $product['quantity'],
                    'product_id'        => $product['product_id'],
                    'subtract'          => isset($product['subtract']) ? $product['subtract'] : "",
                    'netto_price'       => $netto_price,
                    'netto_total_price' => $netto_total_price,
                    'netto'             => $netto,
                    'brutto'            => $brutto,
                    'title'             => isset($_SESSION['shipping_method']['title']) ? $_SESSION['shipping_method']['title'] : "",
                    'href'              => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    'price'             => $price,
                    'total'             => $total,
                    'tax'               => $tax
				);
			}

            $products_brutto = 0;
            $products_netto  = 0;
            foreach ($this->cart->getProducts() as $product) {
                if($product['utalvany'] == '0') {
                    $products_brutto += $this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'));
                    $products_netto += $product['total'];
                }
            }
            $products_brutto = $this->currency->format(($products_brutto));
            $products_netto = $this->currency->format(($products_netto));
            $template->data['products_brutto'] = $products_brutto;
            $template->data['products_netto'] = $products_netto;

			// Vouchers
			$template->data['vouchers'] = array();

			foreach ($order_voucher_query->rows as $voucher) {
				$template->data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}

			$template->data['totals'] = $order_total_query->rows;

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
				$html = $template->fetch($this->config->get('config_template') . '/template/mail/order.tpl');
			} else {
				$html = $template->fetch('default/template/mail/order.tpl');
			}

			// Text Mail
			$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
			$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
			$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
			$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

			if ($comment && $notify) {
				$text .= $language->get('text_new_instruction') . "\n\n";
				$text .= $comment . "\n\n";
			}

			// Products
			$text .= $language->get('text_new_products') . "\n";

			foreach ($order_product_query->rows as $result) {
				$text .= $result['quantity'] . 'x ' . $result['name'] . ' (' . $result['model'] . ') ' . html_entity_decode($this->currency->format($result['total'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $result['order_product_id'] . "'");

				foreach ($order_option_query->rows as $option) {
					$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value']) > 40 ? utf8_substr($option['value'], 0, 40) . '...' : $option['value']) . "\n";
				}
			}

			foreach ($order_voucher_query->rows as $voucher) {
				$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
			}

			$text .= "\n";

			$text .= $language->get('text_new_order_total') . "\n";

			foreach ($order_total_query->rows as $result) {
				$text .= $result['title'] . ': ' . html_entity_decode($result['text'], ENT_NOQUOTES, 'UTF-8') . "\n";
			}

			$text .= "\n";

			if ($order_info['customer_id']) {
				$text .= $language->get('text_new_link') . "\n";
				$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
			}

			if ($order_download_query->num_rows) {
				$text .= $language->get('text_new_download') . "\n";
				$text .= $order_info['store_url'] . 'index.php?route=account/download' . "\n\n";
			}

            if ($letoltheto_vonalkod) {
                if (!$order_download_query->num_rows) {
                    $text .= $language->get('text_new_download') . "\n";
                }
                $text .= $order_info['store_url'] . 'index.php?route=account/utalvanyok' . "\n\n";

            }

			if ($order_info['comment']) {
				$text .= $language->get('text_new_comment') . "\n\n";
				$text .= $order_info['comment'] . "\n\n";
			}

			$text .= $language->get('text_new_footer') . "\n\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo($order_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($order_info['store_name']);
            $mail->setSubject($subject);

            $mail->setHtml($html);
			$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
			$mail->send();

			$shipping_mails = false;
			if (isset($this->session->data['shipping_method']['mail']) && $this->session->data['shipping_method']['mail']) {
				$shipping_mails = $this->session->data['shipping_method']['mail'];
			}

			// Admin Alert Mail
			if ($this->config->get('config_alert_mail') || (isset($order_info['shipping_warehouse_email']) && $order_info['shipping_warehouse_email']) || $shipping_mails) {
                $subject = $language->get('text_order'). " " .$order_info['order_id']. " " . $order_info['firstname']. " " .$order_info['lastname']." - ".$order_info['email'];

				// Text
				$text  = $language->get('text_new_received') . "\n\n";
                if (isset($order_info['shipping_gls_pont_shopid']) && !empty($order_info['shipping_gls_pont_shopid'])) {
				    $text .= $language->get('text_shipping_gls_pont_shopid') .": ". $order_info['shipping_gls_pont_shopid'] ."\n\n";
                }
				$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
				$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
				$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
				$text .= $language->get('text_new_products') . "\n";

				foreach ($order_product_query->rows as $result) {
					$text .= $result['quantity'] . 'x ' . $result['name'] . ' (' . $result['model'] . ') ' . html_entity_decode($this->currency->format($result['total'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $result['order_product_id'] . "'");

					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
						}

						$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 40 ? utf8_substr($value, 0, 40) . '...' : $value) . "\n";
					}
				}

				foreach ($order_voucher_query->rows as $voucher) {
					$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
				}

				$text .= "\n";

				$text .= $language->get('text_new_order_total') . "\n";

				foreach ($order_total_query->rows as $result) {
					$text .= $result['title'] . ': ' . html_entity_decode($result['text'], ENT_NOQUOTES, 'UTF-8') . "\n";
				}

				$text .= "\n";

				if ($order_info['comment']) {
					$text .= $language->get('text_new_comment') . "\n\n";
					$text .= $order_info['comment'] . "\n\n";
				}


                $template->data['text_greeting'] = $language->get('text_new_received')."\n\n";
                if (isset($order_info['shipping_gls_pont_shopid']) && !empty($order_info['shipping_gls_pont_shopid'])) {
                    $template->data['text_greeting'] .= $language->get('text_shipping_gls_pont_shopid') ." ". $order_info['shipping_gls_pont_shopid'] ."\n\n";
                }

                $template->data['text_link'] = "";

                $template->data['logo'] = "";
                $template->data['store_name'] = "";
                $template->data['store_url'] = "";
                $template->data['customer_id'] = $order_info['customer_id'];
                $template->data['link'] = "";
                $template->data['text_footer'] ="";
                $template->data['text_powered'] = "";



                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
                    $html = $template->fetch($this->config->get('config_template') . '/template/mail/order.tpl');
                } else {
                    $html = $template->fetch('default/template/mail/order.tpl');
                }

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom(iconv("UTF-8","latin2",$this->config->get('config_email')));
				$mail->setSender($order_info['store_name']);
                $mail->setSubject($subject);

                $mail->setHtml($html);
				$mail->send();

				// Send to additional alert emails
				$emails = explode(',', $this->config->get('config_alert_emails'));
                if (isset($order_info['shipping_warehouse_email']) && $order_info['shipping_warehouse_email']) {
                    $emails[] = $order_info['shipping_warehouse_email'];
                }

				if ($shipping_mails) {
					$s_emails = explode(',', $shipping_mails);
					foreach($s_emails as $s_email) {
						$emails[] = trim($s_email);
					}
				}

				foreach ($emails as $email) {
					if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
						$mail->setTo($email);
						$mail->send();
					}
				}
			}
            foreach ($_SESSION['cart'] as $key=>$value) {
                $query = $this->db->query("SELECT utalvany FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$key . "'");
                if ($query->row['utalvany'] == 1) {
                    unset($this->session->data['cart'][$key]);
                    unset($_SESSION['cart'][$key]);
                }
            }


            $szamlazz_settings = $this->config->get("szamlazz_hu_module");

            if (!empty($szamlazz_settings['automatikus']) && $szamlazz_settings['automatikus'] == 1 ) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "'");
                if (!empty($query->row['szamla_tipusa'])) {
                    require_once(DIR_APPLICATION.'controller/checkout/xml_export_szamlazz.php');
                    $szamlazz_hu = new ControllerCheckoutXmlExportSzamlazz($this->registry);

                    $szamlazz_hu->xmlSql($order_id,$query->row['szamla_tipusa']);
                }
            }
		}
	}

	public function update($order_id, $order_status_id, $comment = '', $notify = false) {
		$order_info = $this->getOrder($order_id);

		if ($order_info && $order_info['order_status_id']) {
			// Fraud Detection
			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');

				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);

				if ($risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}

			// Blacklist
			$status = false;

			if ($order_info['customer_id']) {
				$this->load->model('account/customer');

				$results = $this->model_account_customer->getIps($order_info['customer_id']);

				foreach ($results as $result) {
					if ($this->model_account_customer->isBlacklisted($result['ip'])) {
						$status = true;

						break;
					}
				}
			} else {
				$status = $this->model_account_customer->isBlacklisted($order_info['ip']);
			}

			if ($status) {
				$order_status_id = $this->config->get('config_order_status_id');
			}

			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");

			// Send out any gift voucher mails
			if ($this->config->get('config_complete_status_id') == $order_status_id) {
				$this->load->model('checkout/voucher');

				$this->model_checkout_voucher->confirm($order_id);
			}

			if ($notify) {
                $language = new Language($order_info['language_directory'],$this->config->get('config_template'));
				$language->load($order_info['language_filename']);
				$language->load('mail/order');

				$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

				$message  = $language->get('text_update_order') . ' ' . $order_id . "\n";
				$message .= $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

				if ($order_status_query->num_rows) {
					$message .= $language->get('text_update_order_status') . "\n\n";
					$message .= $order_status_query->row['name'] . "\n\n";
				}

				if ($order_info['customer_id']) {
					$message .= $language->get('text_update_link') . "\n";
					$message .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
				}

				if ($comment) {
					$message .= $language->get('text_update_comment') . "\n\n";
					$message .= $comment . "\n\n";
				}

				$message .= $language->get('text_update_footer');

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				$mail->setTo($order_info['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($order_info['store_name']);
                $mail->setSubject($subject);
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
		}
	}

    public function Sap_nak($order_id) {

        $letrehoz_termek="CREATE TABLE if not exists sapnak_rendeles_termek(
                            rendeles_azonosito      text default null,
                            ItemCode                text default null,
                            Quantity                text default null,
                            PriceAfterVAT           text default null)   engine=MyISAM default charset=UTF8";
        $this->db->query($letrehoz_termek);



        $letrehoz_vevo="CREATE TABLE if not exists sapnak_rendeles_vevo(
                          rendeles_azonosito    text default null,
                          CardCode              text default null,
                          CardName              text default null,
                          PaymentGroupCode      text default null,
                          TaxDate               text default null,
                          DocDueDate            text default null,
                          NumAtCard             text default null,
                          Comments              text default null,
                          Address               text default null,
                          Address2              text default null
                          )   engine=MyISAM default charset=UTF8";


        $this->db->query($letrehoz_vevo);


        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");

        $vevo_azonosito             = $query->row['customer_id'];
		$vevo_csoport_azonosito     = $query->row['customer_group_id'];

        $query_vevo=$this->db->query( "SELECT * FROM `".DB_PREFIX."customer` WHERE `customer_id` = ".(int)$vevo_azonosito);
        if ($query_vevo->num_rows > 0)
            $sap_vevokod=$query_vevo->row['vevokod'];
        else
            $sap_vevokod="W00001";

        $datum=date('Ymd');

        $payment_kod=$query->row['payment_code'];
        /* SAP által megadott kódok  */
        if ($payment_kod == "free_checkout")
            $payment_kod=-1;
        elseif ($payment_kod == "bank_transfer")
            $payment_kod=35;
        elseif ($payment_kod == "cod")
            $payment_kod=10;
        elseif ($payment_kod == "payu")
            $payment_kod=35;

        if ( !empty($query->row['payment_company']) ){
            $cegneve = $query->row['payment_company'];
            $szamlazas =$query->row['payment_firstname']." ".$query->row['payment_lastname']."\r\n";
            $szamlazas.=$query->row['payment_postcode']." ".$query->row['payment_city']."\r\n";
            $szamlazas.=$query->row['payment_address_1'];

            $szallitasi =$query->row['shipping_firstname']." ".$query->row['shipping_lastname']."\r\n";
            $szallitasi.=$query->row['shipping_postcode']." ".$query->row['shipping_city']."\r\n";
            $szallitasi.=$query->row['shipping_address_1'];
        } else {
            $cegneve =$query->row['payment_firstname']." ".$query->row['payment_lastname'];
            $szamlazas =$query->row['payment_postcode']." ".$query->row['payment_city']."\r\n";
            $szamlazas.=$query->row['payment_address_1'];

            $szallitasi=$query->row['shipping_postcode']." ".$query->row['shipping_city']."\r\n";
            $szallitasi.=$query->row['shipping_address_1'];
        }


            $this->db->query("INSERT INTO sapnak_rendeles_vevo SET
                          rendeles_azonosito    = '" .$order_id."',
                          CardCode              = '".$sap_vevokod."',
                          CardName              = '".$cegneve."',
                          PaymentGroupCode      = '".$payment_kod."',
                          TaxDate               = '".$datum."',
                          DocDueDate            = '".$datum."',
                          NumAtCard             = '".$order_id."',
                          Comments              = '".$query->row['comment']."',
                          Address               = '".$szamlazas."',
                          Address2              = '".$szallitasi."'");



        $query=$this->db->query( "SELECT * FROM `".DB_PREFIX."order_product` WHERE `order_id` = ".(int)$order_id);

        foreach($query->rows as $sor){
            $termek = $this->db->query( "SELECT * FROM `".DB_PREFIX."product` WHERE `product_id` = ".(int)$sor['product_id']);

            $total_darab=$sor['price']+$sor['tax']/$sor['quantity'];
            $termek_kiir="INSERT INTO sapnak_rendeles_termek SET
                            rendeles_azonosito   = '" .$sor['order_id']."',
                            ItemCode             = '" .$termek->row['cikkszam']."',
                            Quantity             = '" .$sor['quantity']."',
                            PriceAfterVAT        = '" .$total_darab."'";




            $this->db->query($termek_kiir);
        }

        $termek_kiir = false;

        if (isset($_SESSION['shipping_method'])) {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "tax_rule` WHERE tax_class_id = '" . (int)$_SESSION['shipping_method']['tax_class_id'] . "'");
            $adokulcs=0;
            foreach($query->rows as $ado){
                $query_adok= $this->db->query("SELECT * FROM `" . DB_PREFIX . "tax_rate` WHERE tax_rate_id = '" . (int)$ado['tax_rate_id']. "'");
                $query_tax_customer= $this->db->query("SELECT * FROM `" . DB_PREFIX . "tax_rate_to_customer_group` WHERE tax_rate_id = '" . (int)$ado['tax_rate_id']. "' AND customer_group_id='".(int)$vevo_csoport_azonosito."'");
                if ($query_tax_customer->num_rows > 0)
                    $adokulcs+=$query_adok->row['rate'];
            }

            $shipping_tomb=explode(".",$_SESSION['shipping_method']['code']);
            $shipping=substr($shipping_tomb[0],4,1);
            if ($_SESSION['shipping_method']['cost'] > 0 ){
                if (!$shipping) $shipping=0;
                $shipping_total=$_SESSION['shipping_method']['cost']*($adokulcs/100+1);

                $termek_kiir="INSERT INTO sapnak_rendeles_termek SET
                                rendeles_azonosito   = '" .$order_id."',
                                ItemCode             =  'E074',
                                Quantity             = '1',
                                PriceAfterVAT        = '" .$shipping_total."'";
            }
        } else {
            $termek_kiir="INSERT INTO sapnak_rendeles_termek SET
                                rendeles_azonosito   = '" .$order_id."',
                                ItemCode             =  'E074',
                                Quantity             = '1',
                                PriceAfterVAT        = ''";
        }


        if ($vevo_azonosito > 0) {
            $this->SAP_nak_vevo($sap_vevokod,$vevo_azonosito);
        }
        if ($termek_kiir) {
            $this->db->query($termek_kiir);
        }
    }

    public function SAP_nak_vevo($vevokod,$cust_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET
                    atkuldve = 1,
                    vevokod = '".strtoupper($vevokod)."'
            WHERE customer_id = '" . (int)$cust_id. "'");

        $result= $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$cust_id . "'");

        $megnevezes="";
        if (isset($_SESSION['payment_method']['code'])){

            if ($_SESSION['payment_method']['code'] == "bank_transfer"){  //bankiátutalás
                $megnevezes=35;
            } elseif ($_SESSION['payment_method']['code'] == "otp"){    //otp banki átutalás
                $megnevezes=35;
            } elseif ($_SESSION['payment_method']['code'] == "payu"){    //otp banki átutalás
                $megnevezes=35;
            } elseif ($_SESSION['payment_method']['code'] == "cod"){    //utánvétel
                $megnevezes=10;
            } elseif ($_SESSION['payment_method']['code'] == "free_checkout"){    //készpénz
                $megnevezes=-1;
            }
        }
        $this->addCustomerSap($cust_id,$megnevezes);
    }

    public function addCustomerSap($customer_id,$megnevezes) {

        $data=$this->db->query("SELECT * FROM ".DB_PREFIX."customer WHERE customer_id='".(int)$customer_id."'");
        $cust_data=array();
        $cust_data=$data->row;

        $letrehoz="CREATE TABLE if not exists sapnak_rendeles_vevo_reg
        ( `vevokod`  text default null,
         `vevo_neve`  text default null,
         `vevo_idegen_neve`  text default null,
         `vevo_sap_csoportkod`  text default null,
         `fizetesi_mod`  text default null,
         `telefonszam`  text default null,
         `mail`  text default null,
         `adoszam`  text default null,
         `szall_iranyitoszam`  text default null,
         `szall_varos`  text default null,
         `szall_utca`  text default null,
         `szall_orszagkod`  text default null,
         `szaml_iranyitoszam`  text default null,
         `szaml_varos`  text default null,
         `szaml_utca`  text default null,
         `szaml_orszagkod`  text default null,
         `vevo_csoport`  text default null)   engine=MyISAM default charset=UTF8";
        $this->db->query($letrehoz);


        $vevocsoport=$this->getCustomerGroupName($cust_data['customer_group_id']);

        $cimek= $this->getAddresses($customer_id);
        foreach($cimek as $cim){
            if (!isset($szamlazasi)){
                $szamlazasi=array(
                    'szaml_iranyitoszam'    =>$cim['postcode'],
                    'szaml_varos'           =>$cim['city'],
                    'szaml_utca'            =>$cim['address_1'],
                    'szaml_orszagkod'       =>$cim['iso_code_2'],
                    'szaml_cegnev'          =>$cim['company']
                );
            } else if(!isset($szallitasi)){
                $szallitasi=array(
                    'szall_iranyitoszam'    =>$cim['postcode'],
                    'szall_varos'           =>$cim['city'],
                    'szall_utca'            =>$cim['address_1'],
                    'szall_orszagkod'       =>$cim['iso_code_2'],
                    'szall_cegnev'          =>$cim['company']
                );
            }
        }
        if (!isset($szallitasi)){
            $szallitasi=array(
                'szall_iranyitoszam'    =>'',
                'szall_varos'           =>'',
                'szall_utca'            =>'',
                'szall_orszagkod'       =>'',
                'szall_cegnev'       =>''
            );
        }
        if(!isset($szamlazasi)){
            $szamlazasi=array(
                'szaml_iranyitoszam'    =>'',
                'szaml_varos'           =>'',
                'szaml_utca'            =>'',
                'szaml_orszagkod'       =>'',
                'szaml_cegnev'          =>''
            );
        }


        $this->db->query("INSERT INTO sapnak_rendeles_vevo_reg SET
                           `vevokod`               =    '".$cust_data['vevokod']."',
                           `vevo_neve`             =    '".$cust_data['firstname']." ".$cust_data['lastname']."',
                           `fizetesi_mod`          =    '".$megnevezes."',
                           `telefonszam`           =    '".$cust_data['telephone']."',
                           `mail`                  =    '".$cust_data['email']."',
                           `adoszam`               =    '".$cust_data['adoszam']."',
                           `szall_iranyitoszam`    =    '".$szallitasi['szall_iranyitoszam']."',
                           `szall_varos`           =    '".$szallitasi['szall_varos']."',
                           `szall_utca`            =    '".$szallitasi['szall_utca']."',
                           `szall_orszagkod`       =    '".$szallitasi['szall_orszagkod']."',

                           `szaml_iranyitoszam`    =    '".$szamlazasi['szaml_iranyitoszam']."',
                           `szaml_varos`           =    '".$szamlazasi['szaml_varos']."',
                           `szaml_utca`            =    '".$szamlazasi['szaml_utca']."',
                           `szaml_orszagkod`       =    '".$szamlazasi['szaml_orszagkod']."',
                           `vevo_csoport`          =    '".$vevocsoport."'");


    }


    function getCustomerGroupName($customer_group_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group WHERE `customer_group_id` = '" . $customer_group_id . "'");

        return $query->row['name'];

    }


    public function getAddresses($customer_id) {
        $address_data = array();

        $query = $this->db->query("SELECT address_id FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        foreach ($query->rows as $result) {
            $address_info = $this->getAddress($result['address_id']);

            if ($address_info) {
                $address_data[] = $address_info;
            }
        }

        return $address_data;
    }

    public function getAddress($address_id) {
        $address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "'");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            return array(
                'address_id'     => $address_query->row['address_id'],
                'customer_id'    => $address_query->row['customer_id'],
                'firstname'      => $address_query->row['firstname'],
                'lastname'       => $address_query->row['lastname'],
                'company'        => $address_query->row['company'],
                'adoszam'        => $address_query->row['adoszam'],
                'address_1'      => $address_query->row['address_1'],
                'address_2'      => $address_query->row['address_2'],
                'postcode'       => $address_query->row['postcode'],
                'city'           => $address_query->row['city'],
                'zone_id'        => $address_query->row['zone_id'],
                'zone'           => $zone,
                'zone_code'      => $zone_code,
                'country_id'     => $address_query->row['country_id'],
                'country'        => $country,
                'iso_code_2'     => $iso_code_2,
                'iso_code_3'     => $iso_code_3,
                'address_format' => $address_format
            );
        }
    }

    public function vonalkodKeszit($order_product_id,$order_id,$product_id,$status=0) {


        if ( $status == $this->config->get('config_complete_status_id') ) {
            $status = 1;
        } else {
            $status = 0;
        }

        $sql = "INSERT INTO " . DB_PREFIX . "product_vonalkod SET
			    order_product_id    = '" . (int)$order_product_id . "',
			    order_id            = '" . (int)$order_id . "',
			    product_id          = '" . (int)$product_id . "',
			    status              = '" . (int)$status . "',
			    date_added = NOW()";

        $this->db->query($sql);

        $product_vonalkod_id = $this->db->getLastId();
        $vonalkod = $this->vonalkodGeneral($product_id,$product_vonalkod_id);

        $sql = "UPDATE " . DB_PREFIX . "product_vonalkod SET
         	vonalkod            = '" . $vonalkod . "'

         WHERE product_vonalkod_id = '" .$product_vonalkod_id."'";
        $this->db->query($sql);

        $this->utalvanyGenereal($product_id, $vonalkod);

    }

    public function vonalkodGeneral($product_id,$product_vonalkod_id) {
        $harmas_1 = substr(($product_id*100),0,3);
        $harmas_2 = rand(1000,9999);
        $harmas_3 = substr(($product_vonalkod_id*100),0,3);
        $eleje    = $harmas_1 . $harmas_2 . $harmas_3;

        $vege = 0;
        for ($i = 0; $i<= 9; $i++) {
            $vege += (int)substr($eleje,$i,1);
        }
        $vege= substr(("000".$vege),-3);


        return $eleje.$vege;

    }

    public function utalvanyGenereal($product_id, $vonalkod) {


        $utvonal1= DIR_DOWNLOAD.$vonalkod."file.jpg";
        $bc = new barCode("jpeg",30);
        $bc->build('1234567890123',false,$utvonal1);

        header("Content-Type: image/jpg");
        $this->load->model('account/product');
        $this->load->model('tool/image');

        $results = $this->model_account_product->getProduct($product_id);

        $this->language->load('product/product');
        $kaphato = $this->language->get('text_kaphato');
        $erteke = $this->language->get('text_utalvany_erteke');

        $src_width  = $this->config->get('config_image_product_width');
        $src_height = $this->config->get('config_image_product_height');

        $image = $this->model_tool_image->resize($results['image'], $src_width, $src_height);

        $src = imagecreatefromjpeg($image);
        $vonalkod_src = imagecreatefromjpeg(DIR_DOWNLOAD.$vonalkod."file.jpg");


        $szovegek = strip_tags(html_entity_decode($results['description'], ENT_QUOTES, 'UTF-8'));
        $szovegek = str_replace("\r","",$szovegek);
        $szovegek = str_replace("\n","",$szovegek);
        $szovegek = str_replace("\t","",$szovegek);
        $szovegek = str_replace("&nbsp;","",$szovegek);



        $szovegek = explode(" ",$szovegek);

        $sorkiir = "";
        $i = 1;
        foreach ($szovegek as $szoveg) {
            $szoveg = substr($szoveg,0,50);
            if ( (strlen($szoveg)+strlen($sorkiir)) > 60 ) {
                $sorkiir = "";
                $i++;
            }
            $sorkiir .= $szoveg . " ";
        }

        if (!empty($sorkiir)) {
            $i++;
        }


        $width = $src_width + 350;
        $height = $src_height + (30*$i)+120;


        $im = @imagecreate($width, $height);
        $background_color = imagecolorallocate($im, 250, 250, 250);
        $text_color = imagecolorallocate($im, 0, 0, 0);
        $text_color1 = imagecolorallocate($im, 90, 90, 90);


        $font = DIR_TEMPLATE.'default/stylesheet/impact.ttf';
        $font2 = DIR_TEMPLATE.'default/stylesheet/arial.ttf';

        $nevek = explode(" ",$results['name']);
        $sorkiier_nev = "";
        $t = 0;
        foreach ($nevek as $nev) {
            $nev = substr($nev,0,20);
            if ( (strlen($nev)+strlen($sorkiier_nev)) > 30 ) {
                imagettftext($im, 20, 0, $src_width+10, 30+($t*30), $text_color1, $font, $sorkiier_nev);
                $sorkiier_nev = "";
                $t++;
            }
            $sorkiier_nev .= $nev . " ";
        }
        if (!empty($sorkiier_nev) ) {
            imagettftext($im, 20, 0, $src_width+10, 30+($t*30), $text_color1, $font, $sorkiier_nev);
        }

        $ar = $this->currency->format($this->tax->calculate($results['price'], $results['tax_class_id'], $this->config->get('config_tax')),4);
        imagettftext($im, 12, 0, $src_width+10, 70+($t*30), $text_color, $font2, $erteke);
        imagettftext($im, 14, 0, $src_width+160, 70+($t*30), $text_color, $font, $ar);

        if (!empty($results['date_ervenyes_ig']) && $results['date_ervenyes_ig'] > "0000-00-00" ) {
            imagettftext($im, 12, 0, $src_width+10, 100+($t*30), $text_color1, $font2, $kaphato);
            imagettftext($im, 13, 0, $src_width+160, 100+($t*30), $text_color1, $font, $results['date_ervenyes_ig']."-ig");
        }
        imagettftext($im, 13, 0, 50, $src_height+75, $text_color1, $font, $vonalkod);

        $sorkiir = "";
        $i = 1;
        foreach ($szovegek as $szoveg) {
            $szoveg = substr($szoveg,0,50);
            if ( (strlen($szoveg)+strlen($sorkiir)) > 62 ) {
                imagettftext($im, 12, 0, 10, $src_height+(30*$i)+100, $text_color1, $font2,$sorkiir);
                $sorkiir = "";
                $i++;
            }
            $sorkiir .= $szoveg . " ";
        }
        imagettftext($im, 12, 0, 10, $src_height+(30*$i)+100, $text_color1, $font2,$sorkiir);

        $utvonal= DIR_DOWNLOAD.$vonalkod.".jpg";
        imagecopymerge($im, $src, 0, 0, 0, 0, $src_width, $src_height, 100);
        imagecopymerge($im, $vonalkod_src, 10, $src_height+20, 0, 0, 200, 30, 100);

        imagejpeg($im,$utvonal);
        unlink($utvonal1);

    }

    public function vasarlasrolCsv($order_id, $export_csoport){
        require_once(DIR_ARUHAZ . 'admin/controller/tool/csv_export.php');
        $csv = new ControllerToolCsvExport($this->registry);
        $feltetel =" WHERE ".DB_PREFIX."order.order_id = ".$order_id;
        $csv->csvSql($export_csoport,$feltetel);



    }

}

class barCode
{
    public $bcHeight, $bcThinWidth, $bcThickWidth, $bcFontSize, $mode;

    function __construct($mode='gif', $height=50, $thin=2, $thick=3, $fSize=2)
    {
        $this->bcHeight = $height;
        $this->bcThinWidth = $thin;
        $this->bcThickWidth = $this->bcThinWidth * $thick;
        $this->fontSize = $fSize;
        $this->mode = $mode;
        $this->outMode = array('gif'=>'gif', 'png'=>'png', 'jpeg'=>'jpeg', 'wbmp'=>'vnd.wap.wbmp');
        $this->codeMap = array(
            '0'=>'000110100',	'1'=>'100100001',	'2'=>'001100001',	'3'=>'101100000',
            '4'=>'000110001',	'5'=>'100110000',	'6'=>'001110000',	'7'=>'000100101',
            '8'=>'100100100',	'9'=>'001100100',	'A'=>'100001001',	'B'=>'001001001',
            'C'=>'101001000',	'D'=>'000011001',	'E'=>'100011000',	'F'=>'001011000',
            'G'=>'000001101',	'H'=>'100001100',	'I'=>'001001100',	'J'=>'000011100',
            'K'=>'100000011',	'L'=>'001000011',	'M'=>'101000010',	'N'=>'000010011',
            'O'=>'100010010',	'P'=>'001010010',	'Q'=>'000000111',	'R'=>'100000110',
            'S'=>'001000110',	'T'=>'000010110',	'U'=>'110000001',	'V'=>'011000001',
            'W'=>'111000000',	'X'=>'010010001',	'Y'=>'110010000',	'Z'=>'011010000',
            ' '=>'011000100',	'$'=>'010101000',	'%'=>'000101010',	'*'=>'010010100',
            '+'=>'010001010',	'-'=>'010000101',	'.'=>'110000100',	'/'=>'010100010'
        );
    }

    public function build($text='', $showText=false, $fileName=null)
    {
        if (trim($text) <= ' ')
            throw new exception('barCode::build - must be passed text to operate');
        if (!$fileType = $this->outMode[$this->mode])
            throw new exception("barCode::build - unrecognized output format ({$this->mode})");
        if (!function_exists("image{$this->mode}"))
            throw new exception("barCode::build - unsupported output format ({$this->mode} - check phpinfo)");

        $text  =  strtoupper($text);
        $dispText = "* $text *";
        $text = "*$text*"; // adds start and stop chars
        $textLen  =  strlen($text);
        $barcodeWidth  =  $textLen * (7 * $this->bcThinWidth + 3 * $this->bcThickWidth) - $this->bcThinWidth;
        $im = imagecreate($barcodeWidth, $this->bcHeight);
        $black = imagecolorallocate($im, 0, 0, 0);
        $white = imagecolorallocate($im, 255, 255, 255);
        imagefill($im, 0, 0, $white);

        $xpos = 0;
        for ($idx=0; $idx<$textLen; $idx++)
        {
            if (!$char = $text[$idx]) $char = '-';
            for ($ptr=0; $ptr<=8; $ptr++)
            {
                $elementWidth = ($this->codeMap[$char][$ptr]) ? $this->bcThickWidth : $this->bcThinWidth;
                if (($ptr + 1) % 2)
                    imagefilledrectangle($im, $xpos, 0, $xpos + $elementWidth-1, $this->bcHeight, $black);
                $xpos += $elementWidth;
            }
            $xpos += $this->bcThinWidth;
        }

        if ($showText)
        {
            $pxWid = imagefontwidth($this->fontSize) * strlen($dispText) + 10;
            $pxHt = imagefontheight($this->fontSize) + 2;
            $bigCenter = $barcodeWidth / 2;
            $textCenter = $pxWid / 2;
            imagefilledrectangle($im, $bigCenter - $textCenter, $this->bcHeight - $pxHt, $bigCenter + $textCenter, $this->bcHeight, $white);
            imagestring($im, $this->fontSize, ($bigCenter - $textCenter) + 5, ($this->bcHeight - $pxHt) + 1, $dispText, $black);
        }

        if (!$fileName) header("Content-type:  image/{$fileType}");
        switch($this->mode)
        {
            case 'gif':
                imagegif($im, $fileName);
                break;
            case 'png':
                imagepng($im, $fileName);
                break;
            case 'jpeg':
                imagejpeg($im, $fileName);
                break;
            case 'wbmp':
                imagewbmp($im, $fileName);
                break;
        }

        imagedestroy($im);
    }


}

?>