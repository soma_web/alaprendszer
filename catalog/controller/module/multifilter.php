<?php  
class ControllerModuleMultiFilter extends Controller {
	protected function index($setting) {

        $this->language->load('module/multifilter');

        $this->data['heading_title']                = $this->language->get('heading_title');
        $this->data['heading_title_category']       = $this->language->get('heading_title_category');
        $this->data['heading_title_filter_group']   = $this->language->get('heading_title_filter_group');
        $this->data['heading_title_filter']         = $this->language->get('heading_title_filter');
        $this->data['heading_title_gyarto']         = $this->language->get('heading_title_gyarto');
        $this->data['heading_title_raktaron']       = $this->language->get('heading_title_raktaron');
        $this->data['heading_title_arszuro']        = $this->language->get('heading_title_arszuro');
        $this->data['text_cikkszam']                = $this->language->get('text_cikkszam');
        $this->data['entry_raktaron']               = $this->language->get('entry_raktaron');
        $this->data['heading_title_meret']          = $this->language->get('heading_title_meret');
        $this->data['heading_title_szin']           = $this->language->get('heading_title_szin');
        $this->data['entry_nincs_raktaron']         = $this->language->get('entry_nincs_raktaron');
        $this->data['entry_filter_select_artol']    = $this->language->get('entry_filter_select_artol');
        $this->data['entry_filter_select_arig']     = $this->language->get('entry_filter_select_arig');
        $this->data['entry_filter_ar_osszevont']    = $this->language->get('entry_filter_ar_osszevont');
        $this->data['text_search']                  = $this->language->get('text_search');
        $this->data['text_szurok_torlese']          = $this->language->get('text_szurok_torlese');

        $beallitasok = $this->config->get('multifilter_beallitasok');

        $path                   = isset($this->request->request['path']) &&  !empty($this->request->request['path']) ? $this->request->request['path'] : "";
        if ($path) {
            $this->load->model('module/multifilter');
            $this->model_module_multifilter->sessionUrit();
        }


        $path                       = !empty($this->request->request['path'])                       ? $this->request->request['path'] : "";
        $filter                     = !empty($this->request->request['filter'])                     ? $this->request->request['filter'] : "";
        $filter_varos               = !empty($this->request->request['filter_varos'])               ? $this->request->request['filter_varos'] : "";
        $filter_artol               = !empty($this->request->request['filter_artol'])               ? $this->request->request['filter_artol'] : "";
        $filter_arig                = !empty($this->request->request['filter_arig'])                ? $this->request->request['filter_arig'] : "";
        $filter_ar_tol_ig           = !empty($this->request->request['filter_ar_tol_ig'])           ? $this->request->request['filter_ar_tol_ig'] : "";
        $filter_tulajdonsag_csuszka = !empty($this->request->request['filter_tulajdonsag_csuszka']) ? $this->request->request['filter_tulajdonsag_csuszka'] : "";
        $filter_arszazalek          = !empty($this->request->request['filter_arszazalek'])          ? $this->request->request['filter_arszazalek'] : "";
        $filter_uzlet               = !empty($this->request->request['filter_uzlet'])               ? $this->request->request['filter_uzlet'] : "";
        $filter_tulajdonsagok       = !empty($this->request->request['filter_tulajdonsagok'])       ? $this->request->request['filter_tulajdonsagok'] : "";
        $filter_valasztek           = !empty($this->request->request['filter_valasztek'])           ? $this->request->request['filter_valasztek'] : "";
        $filter_gyarto              = !empty($this->request->request['filter_gyarto'])              ? $this->request->request['filter_gyarto'] : "";
        $filter_raktaron            = !empty($this->request->request['filter_raktaron'])            ? $this->request->request['filter_raktaron'] : "";
        $filter_meret               = !empty($this->request->request['filter_meret'])               ? $this->request->request['filter_meret'] : "";
        $filter_szin                = !empty($this->request->request['filter_szin'])                ? $this->request->request['filter_szin'] : "";

        $this->data['filter_category']          = $filter               ? explode(',', $filter) : array();
        $this->data['filter_varos']             = $filter_varos         ? explode(',', $filter_varos) : array();
        $this->data['filter_uzletek']           = $filter_uzlet         ? explode(',', $filter_uzlet) : array();
        $this->data['filter_tulajdonsagok']     = $filter_tulajdonsagok ? explode(',', $filter_tulajdonsagok) : array();
        $this->data['filter_valasztek']         = $filter_valasztek     ? explode(',', $filter_valasztek) : array();
        $this->data['filter_gyarto']            = $filter_gyarto        ? explode(',', $filter_gyarto) : array();
        $this->data['filter_raktaron']          = $filter_raktaron      ? explode(',', $filter_raktaron) : array();
        $this->data['filter_meret']             = $filter_meret         ? explode(',', $filter_meret) : array();
        $this->data['filter_szin']              = $filter_szin          ? explode(',', $filter_szin) : array();
        $this->data['filter_artol']             = $filter_artol         ? $filter_artol : 0;
        $this->data['filter_arig']              = $filter_arig          ? $filter_arig : 0;
        $this->data['filter_ar_tol_ig']         = $filter_ar_tol_ig     ? explode(',', $filter_ar_tol_ig) : array();
        $this->data['filter_tulajdonsag_csuszka']= $filter_tulajdonsag_csuszka     ? explode(',', $filter_tulajdonsag_csuszka) : array();
        $this->data['filter_arszazalek']        = $filter_arszazalek    ? $filter_arszazalek : 0;

        if (isset($this->request->request['filter_name'])) {
            $this->data['filter_name'] = $this->request->request['filter_name'];
        } else {
            $this->data['filter_name'] = '';
        }

        if ($this->config->get('multifilter_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

        if ($this->config->get('multifilter_module_ajax') == 1) {
            $this->data['ajaxos_kuldes'] = true;
        } else {
            $this->data['ajaxos_kuldes'] = false;
        }



        if ($path) {
            $parts = explode('_', (string)$path);
        } else {
            $parts = array();
        }

        if (isset($parts[0]) && count($parts) == 1) {
            $this->data['category_id'] = $parts[0];
        } else {
            $this->data['category_id'] = 0;
        }
        $this->data['module_name'] = "multifilter";

        if (isset($parts[1])) {
            $this->data['child_id'] = $parts[1];
        } else {
            $this->data['child_id'] = 0;
        }

        $this->data['parts'] = $parts;
        $this->load->model('catalog/multi_szurok');
        $this->load->model('catalog/multi_kozosites');

        $this->load->model('catalog/multifilter');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');



        $beallitasok = $this->config->rendezes($beallitasok, "sort_order", "ASC", false, "ASC", false, "ASC", false, "ASC", false);


        if ($beallitasok['szuro']['value'] == 1 &&  $beallitasok['szuro']['teljes'] == 1) {
            $this->data['filter_selects_teljes'] = $this->model_catalog_multi_szurok->veglegesuzletek();
        }
        if ($beallitasok['szuro_csoport']['value'] == 1 &&  $beallitasok['szuro_csoport']['teljes'] == 1) {
            $this->data['filter_csoportok_teljes'] =  $this->model_catalog_multi_szurok->veglegesVarosok();
        }
        if ($beallitasok['kategoria_checkbox']['value'] == 1 &&  $beallitasok['kategoria_checkbox']['teljes'] == 1) {
            $this->data['categories_checkbox_teljes'] =  $this->model_catalog_multi_szurok->veglegesFoKategoria();
        }
        if ($beallitasok['kategoria_sub']['value'] == 1 &&  $beallitasok['kategoria_sub']['teljes'] == 1) {
            //$this->data['al_categoria_marad_teljes'] =  $this->model_catalog_multi_szurok->veglegesAlKategoria($this->data['category_id']);
            $this->data['al_categoria_marad_teljes'] =  $this->model_catalog_multi_szurok->veglegesAlKategoria($parts);
            if (empty($this->data['al_categoria_marad_teljes'])) {
                $this->data['al_categoria_marad_teljes'] =  $this->model_catalog_multi_szurok->veglegesFoKategoria();
            }
        }
        if ($beallitasok['kategoria_normal']['value'] == 1 &&  $beallitasok['kategoria_normal']['teljes'] == 1) {
            $this->data['fo_categoria_marad_teljes'] =  $this->model_catalog_multi_szurok->veglegesFoKategoria();
        }
        if ($beallitasok['gyarto']['value'] == 1 &&  $beallitasok['gyarto']['teljes'] == 1) {
            $this->data['manufacturers_teljes'] =  $this->model_catalog_multi_szurok->veglegesGyartok();
        }
        if ($beallitasok['tulajdonsagok']['value'] == 1 &&  $beallitasok['tulajdonsagok']['teljes'] == 1) {
            $this->data['attributes_teljes'] =  $this->model_catalog_multi_szurok->veglegesTulajdonsag();
        }
        if ($beallitasok['valasztek']['value'] == 1 &&  $beallitasok['valasztek']['teljes'] == 1) {
            $this->data['options_teljes'] =  $this->model_catalog_multi_szurok->veglegesValasztek();
        }
        if ($beallitasok['raktaron']['value'] == 1 &&  $beallitasok['raktaron']['teljes'] == 1) {
            $this->data['stocks_teljes'] =  $this->model_catalog_multi_szurok->veglegesRaktar();
        }
        if (isset($beallitasok['meret']['value']) && $beallitasok['meret']['value'] == 1 &&  $beallitasok['meret']['teljes'] == 1) {
            $this->data['meretek_teljes'] =  $this->model_catalog_multi_szurok->veglegesMeret();
        }

        $this->data['beallitasok'] = $beallitasok;



        $url = '';
        if (isset($this->request->request['sort'])) {
            $url .= '&sort=' . $this->request->request['sort'];
        }
        if (isset($this->request->request['order'])) {
            $url .= '&order=' . $this->request->request['order'];
        }
        if (isset($this->request->request['limit'])) {
            $url .= '&limit=' . $this->request->request['limit'];
        }
        $this->data['action'] = str_replace('&amp;', '&', $this->url->link('product/multifilter', $url));
        $this->data['button_filter'] = $this->language->get('button_filter');

        if (!$this->data['filter_category'] && !$this->data['filter_varos'] && !$this->data['filter_uzletek'] &&
            !$this->data['filter_tulajdonsagok'] && !$this->data['filter_valasztek'] && !$this->data['filter_gyarto'] &&
             !$this->data['filter_artol'] && !$this->data['filter_arig'] &&
            !$this->data['filter_ar_tol_ig'] && !$this->data['filter_arszazalek'] && !$this->data['filter_meret'] && !$this->data['filter_szin'] && !$parts) {

            $artol_[0] = $beallitasok['ar_szures_tol_ig']['tol'];
            $artol_[1] = $beallitasok['ar_szures_tol_ig']['ig'];
            $artol_[2] = $beallitasok['ar_szures_tol_ig']['tol'];
            $artol_[3] = $beallitasok['ar_szures_tol_ig']['ig'];

            $this->data['categories_checkbox']  = $this->model_catalog_multi_szurok->veglegesFoKategoria();
            $this->data['al_categoria_marad']   = $this->data['categories_checkbox'];
            $this->data['fo_categoria_marad']   = $this->data['categories_checkbox'];
            $this->data['filter_csoportok']     = $this->model_catalog_multi_szurok->veglegesVarosok();
            $this->data['filter_selects']       = $this->model_catalog_multi_szurok->veglegesuzletek();
            $this->data['attributes']           = $this->model_catalog_multi_szurok->veglegesTulajdonsag();
            $this->data['options']              = $this->model_catalog_multi_szurok->veglegesValasztek();
            $this->data['manufacturers']        = $this->model_catalog_multi_szurok->veglegesGyartok();
            $this->data['stocks']               = $this->model_catalog_multi_szurok->veglegesRaktar();
            $this->data['meretek']              = $this->model_catalog_multi_szurok->veglegesMeret();
            $this->data['szinek']               = $this->model_catalog_multi_szurok->veglegesSzin();
            $this->data['filter_ar_tol_ig']     = $artol_;
            $this->data['teljes_kategoria']     = 1;

        } else {
            if (empty($this->data['filter_category']) && empty($path)) {
                $this->data['teljes_kategoria']     = 1;
            }
            $para = array();
            if ($path)                  $para['path'] = $path;
            if ($filter)                $para['filter'] = $filter;
            if ($filter_varos)          $para['filter_varos'] = $filter_varos;
            if ($filter_artol)          $para['filter_artol'] = $filter_artol;
            if ($filter_arig)           $para['filter_arig'] = $filter_arig;
            if ($filter_ar_tol_ig)      $para['filter_ar_tol_ig'] = $filter_ar_tol_ig;
            if ($filter_tulajdonsag_csuszka) $para['filter_tulajdonsag_csuszka'] = $filter_tulajdonsag_csuszka;
            if ($filter_arszazalek)     $para['filter_arszazalek'] = $filter_arszazalek;
            if ($filter_uzlet)          $para['filter_uzlet'] = $filter_uzlet;
            if ($filter_tulajdonsagok)  $para['filter_tulajdonsagok'] = $filter_tulajdonsagok;
            if ($filter_valasztek)      $para['filter_valasztek'] = $filter_valasztek;
            if ($filter_gyarto)         $para['filter_gyarto'] = $filter_gyarto;
            if ($filter_raktaron)       $para['filter_raktaron'] = $filter_raktaron;
            if ($filter_meret)          $para['filter_meret'] = $filter_meret;
            if ($filter_szin)           $para['filter_szin'] = $filter_szin;
            if (!empty($this->request->request['esemeny_kivalto']))  $para['esemeny_kivalto'] = $this->request->request['esemeny_kivalto'];

            $vissza=$this->model_catalog_multifilter->getGlobalSzures($para);

            $this->data['categories_checkbox']  = $vissza['box_categoria_marad'];
            $this->data['al_categoria_marad']   = !empty($vissza['al_categoria_marad']) ? $vissza['al_categoria_marad']: (isset($this->data['al_categoria_marad_teljes']) ? $this->data['al_categoria_marad_teljes'] : false);
            $this->data['fo_categoria_marad']   = $vissza['fo_categoria_marad'];
            $this->data['filter_csoportok']     = $vissza['varos_marad'];
            $this->data['filter_selects']       = $vissza['uzlet_marad'];
            $this->data['attributes']           = $vissza['tulajdonsag_marad'];
            $this->data['options']              = $vissza['valasztek_marad'];
            $this->data['manufacturers']        = $vissza['gyarto_marad'];
            $this->data['stocks']               = $vissza['raktaron_marad'];
            $this->data['meretek']              = $vissza['meret_marad'];
            $this->data['szinek']               = $vissza['szin_marad'];
            $this->data['filter_ar_tol_ig']     = $vissza['artol_ig_marad'];
            $this->data['scroll_position']      = !empty($this->request->request['scroll_position'])  ? $this->request->request['scroll_position'] : 0;

            $this->data['filter_szin_group'] = array();
            if (!empty($this->request->request['filter_szin_group'])) {
                $this->data['filter_szin_group'] = explode(',',$this->request->request['filter_szin_group']);
            }

            foreach($vissza['tulajdonsag_csuszka_marad'] as $attribute_id=>$value) {
                $csuszka_tol_ig[$attribute_id] = $value;
            }

            $this->data['csuszka_tol_ig']       = !empty($csuszka_tol_ig) ? $csuszka_tol_ig : '';



        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/pos_r_opened.png')) {
            $this->data['arrow_opened'] = DIR_TEMPLATE. $this->config->get('config_template') . '/image/pos_r_opened.png';
        } elseif(file_exists(DIR_TEMPLATE.'default/image/pos_r_opened.png')) {
            $this->data['arrow_opened'] = DIR_TEMPLATE.'default/image/pos_r_opened.png';
        } else {
            $this->data['arrow_opened'] = "";
        }

        if (isset($beallitasok['szin']['value']) && $beallitasok['szin']['value'] == 1
            &&  isset($beallitasok['szin']['szinek']) && $beallitasok['szin']['szinek']) {
            $this->data['szin_groups'] =  $this->model_catalog_multifilter->getSzinGroups($this->data['szinek']);
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/multifilter.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/multifilter.tpl';
        } else {
            $this->template = 'default/template/module/multifilter.tpl';
        }

        $this->render();
	}


}
?>