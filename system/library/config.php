<?php
class Config
{
    private $data = array();

    /**
     * @param $key
     * @return string|int|null
     */
    public function get($key)
    {
        return (isset($this->data[$key]) ? $this->data[$key] : null);
    }

    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function has($key)
    {
        return isset($this->data[$key]);
    }

    public function load($filename)
    {
        $file = DIR_CONFIG . $filename . '.php';

        if (file_exists($file)) {
            $_ = array();

            require($file);

            $this->data = array_merge($this->data, $_);
        } else {
            trigger_error('Error: Could not load config ' . $filename . '!');
            exit();
        }
    }

    public function rendezes($tomb, $rendez0 = false, $merre0 = "ASC", $rendez1 = false, $merre1 = "ASC", $rendez2 = false, $merre2 = "ASC", $rendez3 = false, $merre3 = "ASC", $reNumbering = false)
    {

        $sortingSettings = array();
        if ($rendez0) {
            $sortingSettings[] = array(

                'orderby' => $rendez0,
                'sortorder' => $merre0
            );
        }
        if ($rendez1) {
            $sortingSettings[] = array(

                'orderby' => $rendez1,
                'sortorder' => $merre1
            );
        }
        if ($rendez2) {
            $sortingSettings[] = array(
                'orderby' => $rendez2,
                'sortorder' => $merre2
            );
        }
        if ($rendez3) {
            $sortingSettings[] = array(
                'orderby' => $rendez3,
                'sortorder' => $merre1
            );
        }


        $rendezo = new ArrayOfArrays($tomb);
        $rendezo->multiSorting($sortingSettings, $reNumbering);
        return $rendezo->getArrayCopy();
    }

    public function in_array_key_value($array, $key, $val)
    {
        foreach ($array as $item) {
            if (isset($item[$key]) && $item[$key] == $val) {
                return true;
            }
        }
        return false;
    }

    function in_array_category($array, $category_id) {
        foreach ($array as $value) {
            if ($value['category_id'] == $category_id) {
                return $value;

            } elseif (!empty($value['children'])) {
                foreach($value['children'] as $value1){
                    if ($value1['category_id'] == $category_id) {
                        return $value1;

                    } elseif (!empty($value1['children'])) {
                        foreach($value1['children'] as $value2){
                            if ($value2['category_id'] == $category_id) {
                                return $value2;

                            } elseif (!empty($value2['children'])) {
                                foreach($value2['children'] as $value3){
                                    if ($value3['category_id'] == $category_id) {
                                        return $value3;

                                    } elseif (!empty($value3['children'])) {
                                        foreach($value3['children'] as $value4){
                                            if ($value4['category_id'] == $category_id) {
                                                return $value4;

                                            } elseif (!empty($value4['children'])) {
                                                foreach($value4['children'] as $value5){
                                                    if ($value5['category_id'] == $category_id) {
                                                        return $value5;

                                                    } elseif (!empty($value5['children'])) {
                                                        foreach($value5['children'] as $value6){
                                                            if ($value6['category_id'] == $category_id) {
                                                                return $value6;

                                                            } elseif (!empty($value6['children'])) {
                                                                foreach($value6['children'] as $value7){
                                                                    if ($value7['category_id'] == $category_id) {
                                                                        return $value7;

                                                                    } elseif (!empty($value7['children'])) {
                                                                        foreach($value7['children'] as $value8){
                                                                            if ($value8['category_id'] == $category_id) {
                                                                                return $value8;

                                                                            } elseif (!empty($value8['children'])) {
                                                                                foreach($value8['children'] as $value9){
                                                                                    if ($value9['category_id'] == $category_id) {
                                                                                        return $value9;

                                                                                    } elseif (!empty($value9['children'])) {
                                                                                        foreach($value9['children'] as $value10){
                                                                                            if ($value10['category_id'] == $category_id) {
                                                                                                return $value10;

                                                                                            } elseif (!empty($value10['children'])) {
                                                                                                foreach($value10['children'] as $value11){
                                                                                                    if ($value11['category_id'] == $category_id) {
                                                                                                        return $value11;

                                                                                                    } elseif (!empty($value11['children'])) {
                                                                                                        foreach($value11['children'] as $value12){
                                                                                                            if ($value12['category_id'] == $category_id) {
                                                                                                                return $value12;

                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        return array();
    }





    public function searchItemsByKey($array, $key) {
        $results = array();

        if (is_array($array))
        {
            if (isset($array[$key]) && key($array)==$key)
                $results[] = $array[$key];

            foreach ($array as $sub_array)
                $results = array_merge($results, $this->searchItemsByKey($sub_array, $key));
        }

        return  $results;
    }
}

class ArrayOfArrays extends ArrayObject {
    private $sortSettings;

    public function filterByValue($searchedKey, $value) {
        $iterator = $this->getIterator();

        $newArray = array();
        foreach($iterator as $key=>$object) {
            if($object[$searchedKey] == $value) {
                $newArray[$key] = $object;
            }
        }

        return $newArray;
    }

    /**
     * @param array $sortingSettings
     * @param bool $reNumbering
     */
    public function multiSorting($sortingSettings, $reNumbering=false) {

        $this->sortSettings = $sortingSettings;
        $this->uasort(array($this, 'multisort'));

        if($reNumbering) {
            $n = 0;
            $newArray = array();
            foreach($this->getArrayCopy() as $value) {
                $newArray[$n] = $value;
                $n++;
            }
            $this->exchangeArray($newArray);
        }
    }

    public function multisort($a, $b) {
        foreach($this->sortSettings as $settings) {
            $key = $settings['orderby'];
            $sortorder = !empty($settings['sortorder']) ? $settings['sortorder'] : "ASC";
            if($a[$key] > $b[$key]) {
                if($sortorder == "ASC") {
                    return 1;
                } else {
                    return -1;
                }
            } elseif($a[$key] < $b[$key]) {
                if($sortorder == "ASC") {
                    return -1;
                } else {
                    return 1;
                }
            }
        }
        return 0;
    }




}
?>