<?php
// Heading
$_['heading_title']       = 'Manufacturer pictures';

// Text
$_['text_module']         = 'Moduls';
$_['text_success']        = 'Successful change';
$_['text_content_top']    = 'Top of content';
$_['text_content_bottom'] = 'Bottom of content';
$_['text_column_left']    = 'Left column';
$_['text_column_right']   = 'Right column';
$_['text_horizontal']     = 'Horizontal';
$_['text_vertical']       = 'vertical';

// Entry
$_['entry_limit']        = 'Limit:';
$_['entry_scroll']       = 'Scroll:';
$_['entry_dimension']    = 'Size (W x H):';
$_['entry_image']        = 'Image (W x H):';
$_['entry_axis']         = 'Axis:';
$_['entry_layout']       = 'Layout:';
$_['entry_position']     = 'Position:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Order:';

// Error
$_['error_permission']   = 'No authorization for the change!';
$_['error_dimension']    = 'Méret szélesség &amp; magasság méretek szükségesek!';
$_['error_image']        = 'Kép Szélesség &amp; Magasság szükséges!';
?>