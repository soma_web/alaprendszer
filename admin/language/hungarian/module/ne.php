<?php
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Hírlevélkampány szoftver';

// Text
$_['text_success']							= 'Siker: Módosítottad a Hírlevélkampány szoftver modult!';
$_['text_module']							= 'Modulok';
$_['text_module_localization']  			= 'Lokalizáció';
$_['text_ne']  								= 'Hírlevél';
$_['text_ne_email']  						= 'Létrehoz és Küld';
$_['text_ne_draft']  						= 'Hírlevél Piszkozat';
$_['text_ne_marketing']  					= 'Marketing Előfizetők';
$_['text_ne_subscribers']  					= 'Előfizetők Listája';
$_['text_ne_stats']  						= 'Hírlevél Statisztika';
$_['text_ne_robot']  						= 'Ütemezett Hírlevél';
$_['text_ne_template']  					= 'Hírlevél Sablonok';
$_['text_ne_subscribe_box']  				= 'Feliratkozó Doboz';
$_['text_ne_blacklist']  					= 'Feketelistás E-mailek';
$_['text_ne_support']  						= 'Támogatás';
$_['text_ne_support_register']  			= 'Regisztráljon a Támogatásért';
$_['text_ne_support_login']  				= 'Jeletkezz be a Támogatásért';
$_['text_ne_support_dashboard']  			= 'Irányítópult Támogatás';
$_['text_ne_update_check']  				= 'Frissítések Ellenőrzése';
$_['text_default']		  					= 'Alap';
$_['text_general_settings']		  			= 'Általános';
$_['text_bounce_settings']		  			= 'Sikertelen E-mail küldések';
$_['text_throttle_settings']		  		= 'Throttle';/*???*/

$_['text_personalisation_tags']		  		= 'Tárgy és üzenet átalakító tag-ek: <pre style="display:inline;">{name}, {lastname}, {email}</pre> mezők lecserélődnek a megfelelő értékekre. <pre style="display:inline;">{link}</pre> mező átalakul url-lé.';

$_['text_cron_command']   					= '*/5 * * * * /usr/bin/wget -O /dev/null -q "%s" >/dev/null 2>&1';
$_['text_help'] 							= '* * * * * command to be executed<br/>- - - - -<br/>| | | | |<br/>| | | | +- - - - day of week (0 - 6) (Sunday=0)<br/>| | | +- - - - - month (1 - 12)<br/>| | +- - - - - - day of month (1 - 31)<br/>| +- - - - - - - hour (0 - 23)<br/>+- - - - - - - - minute (0 - 59)<br/><br/>I.e. configure cron job to run command each 5 minutes.';
$_['text_licence_info'] 					= 'To activate module please provide your email which was used for module purchasing and order ID.';
$_['text_licence_text'] 					= '<p>The ID of order may be found in the <a href="https://www.opencart.com/index.php?route=account/order" target="_blank">Order History</a> of your OpenCart account and the e-mail is used in your account.</p><br/><ul><li>I agree to Use purchased license for ONE website ONLY</li><li>Reselling or giving away for free the downloaded work is NOT ALLOWED</li><li>Copyrights removal is NOT ALLOWED</li></ul><br/><p>Please, read the installation Guide before opening a ticket</p>';

// Entry
$_['entry_use_throttle']					= 'Throttle használata e-mail küldésnél:<br /><span class="help">Ez a funkció cron szerver beállítást igényel.</span>';
$_['entry_use_embedded_images']				= 'Beágyazott képek használata hírlevél küldésnél:<br /><span class="help">Gmail web app nem támogatja.</span>';
$_['entry_throttle_emails']					= 'E-mailküldés ismétlése:<br /><span class="help">Elküldendő e-mailek száma.</span>';
$_['entry_throttle_time']					= 'Throttle intervallum(következő küldés):<br /><span class="help">Idő percben.</span>';
$_['entry_sent_retries']					= 'Újrapróbálkozások száma:<br /><span class="help">A próbálkozások száma küldésnél.</span>';
$_['entry_name']							= 'Név';
$_['entry_yes']								= 'Igen';
$_['entry_no']								= 'Nem';
$_['entry_cron_code']						= 'Cron parancs:';
$_['entry_cron_help']						= 'Cron parancs súgó:';
$_['entry_list']							= 'Marketing listák:';
$_['entry_weekdays']						= 'Hétköznapok:';
$_['entry_months']							= 'Hónapok:';
$_['entry_january']							= 'Január';
$_['entry_february']						= 'Február';
$_['entry_march']							= 'Március';
$_['entry_april']							= 'Április';
$_['entry_may']								= 'Május';
$_['entry_june']							= 'Június';
$_['entry_july']							= 'Július';
$_['entry_august']							= 'Augusztus';
$_['entry_september']						= 'Szeptember';
$_['entry_october']							= 'Október';
$_['entry_november']						= 'November';
$_['entry_december']						= 'December';
$_['entry_sunday']							= 'Vasárnap';
$_['entry_monday']							= 'Hétfő';
$_['entry_tuesday']							= 'Kedd';
$_['entry_wednesday']						= 'Szerda';
$_['entry_thursday']						= 'Csütörtök';
$_['entry_friday']							= 'Péntek';
$_['entry_saturday']						= 'Szombat';
$_['entry_use_bounce_check']				= 'Nem kézbesített e-mailek ellenőrzése';
$_['entry_bounce_email']					= 'E-mail cím a lepattanó üzenetekhez<br/><span class="help">E-mail cím, ahova az lepattanó üzenetek érkeznek</span>';
$_['entry_bounce_pop3_server']				= 'POP3 szerver a lepattanó üzenetekhez<br/><span class="help">POP3 szerver név vagy IP cím</span>';
$_['entry_bounce_pop3_user']				= 'POP3 szerver login<br/><span class="help">Login a szerver ellenőrzéshez</span>';
$_['entry_bounce_pop3_password']			= 'POP3 szerver jelszó<br/><span class="help">Jelszó a szerver ellenőrzéshez</span>';
$_['entry_bounce_pop3_port']				= 'POP3 szerver port<br/><span class="help">Ha üres, 110 portot használja</span>';
$_['entry_bounce_delete']					= 'Lepattanó üzenetek törlése<br/><span class="help">Ha igen, az ismert lepattanó e-mailek törlésre kerülnek a postaládában.</span>';
$_['entry_transaction_id'] 					= 'Rendelési Azonosító:';
$_['entry_transaction_email'] 				= 'E-mail:';
$_['entry_hide_marketing'] 					= 'Marketing listák elrejtése:';
$_['entry_subscribe_confirmation'] 			= 'Előfizetés Megerősítés:';
$_['entry_subject'] 						= 'Tárgy:';
$_['entry_message'] 						= 'Üzenet:';
$_['entry_website'] 						= 'Weboldal:';

// Button
$_['button_add_list']   					= 'Hozzáad';
$_['button_activate']   					= 'Aktivál';

// Error
$_['error_permission']						= 'Figyelem: Nincs jogod módosítani a Hírlevélkampány szoftver modult!';

$_['entry_use_smtp']						= 'Egyéni levélbeállítások:';
$_['entry_mail_protocol']					= 'E-mail Protokoll:<span class="help">';
$_['text_mail']								= 'Levél';
$_['text_mail_phpmailer']					= 'Levél (PHPMailer)';
$_['text_smtp']								= 'SMTP';
$_['text_smtp_phpmailer']					= 'SMTP (PHPMailer)';
$_['entry_email']							= 'E-mail cím:';
$_['entry_mail_parameter']					= 'E-mail Parametérek:<span class="help">';
$_['entry_smtp_host']						= 'SMTP Host:';
$_['entry_smtp_username']					= 'SMTP Felhasználónév:';
$_['entry_smtp_password']					= 'SMTP Jelszó:';
$_['entry_smtp_port']						= 'SMTP Port:';
$_['entry_smtp_timeout']					= 'SMTP Időkorlát:';
$_['entry_stores']							= 'Üzletek:';
$_['text_smtp_settings']					= 'Levél Beállítások';

?>
