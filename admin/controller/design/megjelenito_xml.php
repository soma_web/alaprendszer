<?php
class ControllerDesignMegjelenitoXml extends Controller {
    private $error = array();

    public function index() {

        $this->load->language('design/megjelenito_xml');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            foreach($this->request->post as $key=>$value) {
                $beallit[$key] = $value;
            }

            if (isset($beallit['xml_beallitasok'])) {


                $this->document->setTitle($this->language->get('heading_title'));
                $this->load->model('setting/setting');

                $this->model_setting_setting->editSetting('xml_beallitasok', $beallit);

                $this->load->model('setting/extension');

                $this->session->data['success'] = $this->language->get('text_success');
            }


            $this->redirect($this->url->link('design/megjelenito_xml', 'nyitott_sor='.$this->request->post['nyitott_sor']. '&nyitott_block='.$this->request->post['nyitott_block']. '&nyitott_tabla='.$this->request->post['nyitott_tabla']. '&scroll_top='.$this->request->post['scroll_top']. '&token=' . $this->session->data['token'], 'SSL'));
        }

        $this->load->model('catalog/information');
        $this->data['information_pages'] 	= $this->model_catalog_information->getInformations();


        $this->data['text_yes']         = $this->language->get('text_yes');
        $this->data['text_no']          = $this->language->get('text_no');
        $this->data['button_save']      = $this->language->get('button_save');
        $this->data['button_cancel']    = $this->language->get('button_cancel');
        $this->data['button_remove']    = $this->language->get('button_remove');
        $this->data['text_select_all']  = $this->language->get('text_select_all');
        $this->data['text_unselect_all']= $this->language->get('text_unselect_all');
        $this->data['entry_status']     = $this->language->get('entry_status');

        if (isset($this->session->data['success']))
            $this->data['success'] = $this->language->get('text_success');
        else
            $this->data['success'] = false;

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('design/megjelenito_xml', 'token=' . $this->session->data['token'], 'SSL'),

            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('design/megjelenito_xml', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['token'] = $this->session->data['token'];

        $this->data['nyitott_sor'] = isset($this->request->request['nyitott_sor']) ? $this->request->request['nyitott_sor'] : "";
        $this->data['nyitott_block'] = isset($this->request->request['nyitott_block']) ? $this->request->request['nyitott_block'] : "";
        $this->data['nyitott_tabla'] = isset($this->request->request['nyitott_tabla']) ? $this->request->request['nyitott_tabla'] : "";
        $this->data['scroll_top'] = isset($this->request->request['scroll_top']) ? $this->request->request['scroll_top'] : "";


        $query= $this->db->query("show tables");
        foreach($query->rows as $tablak) {
            foreach($tablak as $tabla) {
                $mezok = $this->db->query("SHOW columns from `".$tabla."`");
                $szerkezet[$tabla] = $mezok->rows;
            }
        }

        $this->data['szerkezet'] = $szerkezet;


        $xml_beallitasok = $this->config->get('xml_beallitasok');
        if (is_null($xml_beallitasok)) {
            $xml_beallitasok = array();
        }


        foreach($xml_beallitasok as $beallitas_key=>$xml_beallitas) {
            if (isset($xml_beallitas['xml_block'])) {

                foreach($xml_beallitas['xml_block'] as $block_key=>$xml_block) {


                    if (isset($xml_block['tabla']) ) {
                        foreach($xml_block['tabla'] as $tabla_key=>$tabla) {
                            if ($tabla['name'] && $tabla['name'] != '--- Válasszon ---') {
                                $mezok = $this->db->query("SHOW columns from `".$tabla['name']."`");
                                if ($mezok->num_rows > 0) {
                                    foreach($mezok->rows as $mezo) {
                                        $xml_beallitasok[$beallitas_key]['xml_block'][$block_key]['tabla'][$tabla_key]['mezok'][] = $mezo['Field'];
                                    }
                                }
                            }
                        }
                    }

                    if (isset($xml_block['mezok']) ) {
                        foreach($xml_block['mezok'] as $mezok_key=>$mezok) {
                            if ( isset($mezok['tabla']['tabla']) && $mezok['tabla']['tabla']) {
                                $query = $this->db->query("SHOW columns from `".$mezok['tabla']['tabla']."`")->rows;
                                foreach($query as $eredmeny) {
                                    $xml_beallitasok[$beallitas_key]['xml_block'][$block_key]['mezok'][$mezok_key]['tabla']['vonatkozo_mezok'][] = $eredmeny['Field'];

                                }

                            }
                        }

                    }
                    if (substr($block_key,0,4) == 'nyit') {
                        $xml_beallitasok[$beallitas_key]['xml_block'][$block_key]['tablak'] = $this->tablaKigyujt($block_key,$xml_beallitas['xml_block']);
                    }


                }



            }
        }



        $this->data['xml_beallitasok'] = $xml_beallitasok;

        $this->template = 'design/megjelenito_xml.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    public function tablaKigyujt($kulcs,$tomb) {

        $vissza = array();
        //nyit-0-0-0
        foreach($tomb as $key=>$xml_sor) {

            if (strlen($key) >= strlen($kulcs)) {
                break;
            }


            if (substr($kulcs,0,strlen($key)) == $key ) {
                if ( isset($xml_sor['tabla'])) {
                    foreach($xml_sor['tabla'] as $tabla) {
                        $vissza[$tabla['name']] = $tabla['name'];
                    }
                }
            }

        }
        return $vissza;

    }

    public function mezoLista() {
        $json = array();
        //$mezok = $this->db->query("SHOW columns from `".$tabla."`");

        $query = $this->db->query("show tables")->rows;

        $talalt = false;
        foreach($query as $value) {
            if (in_array($this->request->request['tabla_nev'],$value)) {
                $talalt = true;
                break;
            }
        }

        if ($talalt) {
            $mezok = $this->db->query("SHOW columns from `".$this->request->request['tabla_nev']."`")->rows;
            foreach ($mezok as $mezo) {
                $json[] = $mezo['Field'];
            }
        }


        $this->response->setOutput(json_encode($json));

    }

    public function oszlopTablaLista() {
        $json = array();

        $query = $this->db->query("show tables")->rows;

        $tablak = explode(',',$this->request->request['tabla_nevek']);
        if ($tablak) {

            foreach($tablak as $tabla) {
                $talalt = false;

                foreach($query as $value) {
                    if (in_array($tabla,$value)) {
                        $talalt = true;
                        break;
                    }
                }
                if ($talalt) {
                    if (!in_array($tabla,$json)) {
                        $json[] = $tabla;
                    }
                }
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    public function oszlopMezoLista() {
        $json = array();

        $query = $this->db->query("show tables")->rows;

        $tabla = $this->request->request['tabla_nev'];
        if ($tabla) {

            $talalt = false;

            foreach($query as $value) {
                if (in_array($tabla,$value)) {
                    $talalt = true;
                    break;
                }
            }

            if ($talalt) {
                $mezok = $this->db->query("SHOW columns from `".$tabla."`")->rows;
                foreach ($mezok as $mezo) {
                    $json[] = $mezo['Field'];
                }
            }

        }


        $this->response->setOutput(json_encode($json));

    }



}
?>