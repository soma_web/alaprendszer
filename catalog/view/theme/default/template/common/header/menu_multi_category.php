<?php
$_['fokategoria']          = array(
    'type'              => 'checkbox',
    'name'              => 'fokategoria',
    'default'           => "1",
    'heading'           => 'Főcím eltűntetése'
);

$_['main_link']          = array(
    'type'              => 'checkbox',
    'name'              => 'main_link',
    'default'           => "1",
    'heading'           => 'Főcím link'
);

$_['responsive']          = array(
    'type'              => 'checkbox',
    'name'              => 'responsive',
    'default'           => "1",
    'heading'           => 'Responsive'
);

$_['img_icon']          = array(
    'type'              => 'checkbox',
    'name'              => 'img_icon',
    'default'           => "1",
    'heading'           => 'Főkat. ikon'
);

$_['sub_icon']          = array(
    'type'              => 'checkbox',
    'name'              => 'sub_icon',
    'default'           => "1",
    'heading'           => 'Alkat. ikon'
);

$_['cat_detail']          = array(
    'type'              => 'checkbox',
    'name'              => 'cat_detail',
    'default'           => "1",
    'heading'           => 'Kat. leírás'
);

$_['sub_detail']          = array(
    'type'              => 'checkbox',
    'name'              => 'sub_detail',
    'default'           => "1",
    'heading'           => 'Alkat. leírás'
);

$_['img_left']          = array(
    'type'              => 'checkbox',
    'name'              => 'img_left',
    'default'           => "1",
    'heading'           => 'Kat. kép balra'
);

$_['img_right']          = array(
    'type'              => 'checkbox',
    'name'              => 'img_right',
    'default'           => "1",
    'heading'           => 'Kat. kép jobbra'
);

$_['columns']          = array(
    'type'              => 'text',
    'name'              => 'columns',
    'default'           => "0",
    'size'              => "2",
    'heading'           => 'Oszlop'
);

$_['sub_columns']          = array(
    'type'              => 'text',
    'name'              => 'sub_columns',
    'default'           => "0",
    'size'              => "2",
    'heading'           => 'Alkat. oszlop'
);

$_['cat_hover']          = array(
    'type'              => 'checkbox',
    'name'              => 'cat_hover',
    'default'           => "1",
    'heading'           => 'Képváltás hoverre'
);

$_['cat_arrow']          = array(
    'type'              => 'checkbox',
    'name'              => 'cat_arrow',
    'default'           => "1",
    'heading'           => 'Kat. nyíl'
);

$_['cat_measure']          = array(
    'type'              => 'checkbox',
    'name'              => 'cat_measure',
    'default'           => "1",
    'heading'           => 'Li méretkövetés'
);

?>