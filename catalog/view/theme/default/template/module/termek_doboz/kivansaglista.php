<?php
$_['kivansaglista']          = array(
    'type'              => 'checkbox',
    'name'              => 'kivansaglista',
    'default'           => "0",
    'heading'           => 'Felirat eltűntetése'
);

$_['kivansaglista_ikon']          = array(
    'type'              => 'checkbox',
    'name'              => 'kivansaglista_ikon',
    'default'           => "0",
    'heading'           => 'Ikon megjelenítése'
);
?>