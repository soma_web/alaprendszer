<?php
$_['karakter']          = array(
    'type'              => 'text',
    'name'              => 'karakter',
    'size'              => "2",
    'default'           => "100",
    'heading'           => "Karakterek száma"
);

$_['lenyilo']          = array(
    'type'              => 'text',
    'name'              => 'lenyilo',
    'default'           => '',
    'heading'           => "Melyik div alá (class)"
);

$_['lenyilo_mit']          = array(
    'type'              => 'text',
    'name'              => 'lenyilo_mit',
    'default'           => '',
    'heading'           => "Melyik divet (class)"
);

?>