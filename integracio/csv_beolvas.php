<?php
class ModelCsvBeolvas {

    private $db;
    public $csv_tartalom;
    public $csv_fejlec = array();

    public function __construct($query) {
        $this->db = $query;
    }

    public function csvBeolvas($data) {


        /*$handle_kodolasnak = fopen($data['filename'], "r");
        $kodolva = mb_detect_encoding(fgets($handle_kodolasnak));
        $kodolva1 = mb_detect_encoding(fgets($handle_kodolasnak));
        $kodolva2 = mb_detect_encoding(fgets($handle_kodolasnak));*/


        $kodolas = false;
        if (!empty($data['kodolas_rol'])){
            $kodolas = true;
            if (empty($data['kodolas_ra'])){
                $data['kodolas_ra'] = "UTF-8";
            }
        }

        if ($kodolas) {
            $fc = iconv($data['kodolas_rol'], $data['kodolas_ra'], file_get_contents($data['filename']));
            file_put_contents('import.csv', $fc);
            $handle = fopen('import.csv', "r");
        } else {
            $handle = fopen($data['filename'], "r");
        }

        for ($i=0; $data['filefejlec'] > $i; $i++) {
            $this->csv_fejlec[] = fgetcsv($handle, NULL, !empty($data['elvalaszto']) ? ($data['elvalaszto'] == "\\t" ? "\t" : $data['elvalaszto'])  : "\t");
        }

        $this->csv_tartalom = array();

        if ($handle){

            $i = 0;

            while (($sor = fgetcsv($handle, NULL, !empty($data['elvalaszto']) ? ($data['elvalaszto'] == "\\t" ? "\t" : $data['elvalaszto'])  : "\t"))) {
                if ($i >= (is_numeric($data['beolvas_sor_tol']) ? $data['beolvas_sor_tol'] : 0) ) {
                    $this->csv_tartalom[] = $sor;
                }
                if (is_numeric($data['beolvas_sor_ig']) && $data['beolvas_sor_ig'] > $data['beolvas_sor_tol'] && $i > $data['beolvas_sor_ig'] ) {
                     break;
                }


                $i++;



            }
        }
        fclose($handle);
        if ($kodolas) {
            unlink('import.csv');
        }

    }
}
?>
