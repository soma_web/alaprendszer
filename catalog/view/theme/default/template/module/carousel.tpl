<!--<div id="carousel<?php echo $module; ?>">
  <ul class="jcarousel-skin-opencart">
    <?php foreach ($banners as $banner) { ?>
    <li><a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /></a></li>
    <?php } ?>
  </ul>
</div>
<script type="text/javascript">
$('#carousel<?php echo $module; ?> ul').jcarousel({
	vertical: false,
	visible: <?php echo $limit; ?>,
	scroll: <?php echo $scroll; ?>
});
</script>-->

    <style>
#carousel_cont {
    background: #ffffff;
    width: 954px;
    margin-left: auto;
    margin-right: auto;
    text-align: left;
    position: relative;
    top: -0px;
    left: -23px;
}
/*#carousel_man {
    float: left;
    width: 1000px !important;
    text-align:center;
    vertical-align:middle;
    overflow: hidden;
    padding-top: 0px;
    padding-bottom: 0px;
    border-top:solid #474747 1px;
    border-bottom:solid #474747 1px;
    background-color: #000000;

}*/
#carousel_man li{
    margin-left: 5px;
    margin-right: 5px;
    width:230px;;
    height:100px;
}

#carousel_man li a{
    color:#000000;
}
#carousel_cont a.prevsmall {
    float:left;
    margin-top: 40px;
    margin-left: 10px;
    margin-right: 10px;
}
#carousel_cont a.nextsmall {
    float:left;
    margin-top: 40px;
    margin-left: 10px;
    margin-right: 10px;
}

</style>

<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.jcarousel-lite.js"></script>
<div class="clear"></div>
<div id="carousel_cont">
    <!--<a href="" class="prevsmall"><img src="./image/prev.png" alt="" /></a>-->
    <div id="carousel_man">
        <ul><?php foreach ($banners as $banner) { ?>
                <li><a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="clear"></div>
</div>

<script>
    $(function() {
        $("#carousel_man").jCarouselLite({
            btnNext: ".next, .nextsmall",
            btnPrev: ".prev, .prevsmall",
            mouseWheel: true,
            circular: true,
            auto: 2000,
            visible: 8,  //set this to 8 if you have more than 8 brands else set to manufacturer number you have
            //		scroll: 7,	// uncomment (enable) this option if you have more than 8 brands. or set it as you like ;)
            speed: 1000
        });
    });
</script>