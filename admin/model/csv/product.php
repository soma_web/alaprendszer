<?php
class ModelCsvProduct extends Model {

    private $ertek=array();
    private $id;
    private $tabla;
    private $tablak;
    private $nyelvek=array();
    private $store;

    public function getFields($table) {

        $this->load->model("setting/store");
        $stores = $this->model_setting_store->getStores();

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX.$table);
        $product = array();



        if ($table == "product_customer_special") {
            $product[] = DB_PREFIX.$table.'.product_customer_special_id';
            $product[] = DB_PREFIX.$table.'.product_id';
            $product[] = DB_PREFIX.$table.'.customer_id';
            $product[] = DB_PREFIX.$table.'.quantity';
            $product[] = DB_PREFIX.$table.'.price';
            $product[] = DB_PREFIX.$table.'.date_start';
            $product[] = DB_PREFIX.$table.'.date_end';
        } else {
            foreach($oszlop->rows as $curoszlop) {
                $product[] = DB_PREFIX.$table.'.'.$curoszlop['Field'];
            }
        }


        $sql = "SHOW TABLES";
        $query = $this->db->query($sql);
        $this->store = false;
        $description = false;

        foreach($query->rows as $value) {
            if (in_array(DB_PREFIX.$table."_to_store",$value)) {
                $this->store = true;
            }
            if (in_array(DB_PREFIX.$table."_description",$value)) {
                $description = true;
            }
        }

        if ($this->store) {
            if ($stores) $product[] = DB_PREFIX.$table.'_to_store.store_id';
        }


        if ($description) {
            $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX.$table."_description");
            foreach($oszlop->rows as $curoszlop) {
                if($curoszlop['Field'] != $table.'_id' && $curoszlop['Field'] != 'language_id' ) {
                    $product[] = DB_PREFIX.$table.'_description.'.$curoszlop['Field'];
                }
            }
        }

        $product[] = "Delete";

        if ($table == 'product') {
            if ($key=array_keys($product,DB_PREFIX."product.viewed")) {
                unset ($product[$key[0]]);
            }
            if (!$this->config->get('megjelenit_form_admin_model')){
                if ($key=array_keys($product,DB_PREFIX."product.model")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_form_admin_cikkszam')){
                if ($key=array_keys($product,DB_PREFIX."product.cikkszam")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_form_admin_cikkszam2')){
                if ($key=array_keys($product,DB_PREFIX."product.cikkszam2")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get("megjelenit_form_admin_eredeti_ar" )){
                if ($key=array_keys($product,DB_PREFIX."product.eredeti_ar")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_form_admin_szazalek_ar')){
                if ($key=array_keys($product,DB_PREFIX."product.szazalek")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_form_admin_kifuto')){
                if ($key=array_keys($product,DB_PREFIX."product.kifuto")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_form_admin_ujdonsag')){
                if ($key=array_keys($product,DB_PREFIX."product.ujdonsag")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_form_admin_elerendeles' )){
                if ($key=array_keys($product,DB_PREFIX."product.elorendeles")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_form_admin_utalvany')){
                if ($key=array_keys($product,DB_PREFIX."product.utalvany")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_meddig_kaphato')){
                if ($key=array_keys($product,DB_PREFIX."product.date_ervenyes_ig")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_mikortol_kaphato' )){
                if ($key=array_keys($product,DB_PREFIX."product.date_available")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$this->config->get('megjelenit_csomagolas_admin' )){
                if ($key=array_keys($product,DB_PREFIX."product.csomagolasi_egyseg")) {
                    unset ($product[$key[0]]);
                }
                if ($key=array_keys($product,DB_PREFIX."product.csomagolasi_mennyiseg")) {
                    unset ($product[$key[0]]);
                }
            }

            $megjelenit_termekadat = $this->config->get("megjelenit_admin_termekadatok" );
            if (!$megjelenit_termekadat['egyedi_azonosito']) {
                if ($key=array_keys($product,DB_PREFIX."product.sku")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['vonalkod']){
                if ($key=array_keys($product,DB_PREFIX."product.upc")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['max_szazalek_kedvezmeny']){
                if ($key=array_keys($product,DB_PREFIX."product.max_szazalek_kedvezmeny")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['form_min_mennyiseg']){
                if ($key=array_keys($product,DB_PREFIX."product.minimum")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['form_szallitando']){
                if ($key=array_keys($product,DB_PREFIX."product.shipping")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['stock_date']){
                if ($key=array_keys($product,DB_PREFIX."product.stock_in_date")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['form_meret']){
                if ($key=array_keys($product,DB_PREFIX."product.length")) {
                    unset ($product[$key[0]]);
                }
                if ($key=array_keys($product,DB_PREFIX."product.width")) {
                    unset ($product[$key[0]]);
                }
                if ($key=array_keys($product,DB_PREFIX."product.height")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['form_hosszmertek']){
                if ($key=array_keys($product,DB_PREFIX."product.length_class_id")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['form_suly']){
                if ($key=array_keys($product,DB_PREFIX."product.weight")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['form_sulymertek']){
                if ($key=array_keys($product,DB_PREFIX."product.weight_class_id")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['kep_megjelenites_kapcsolas']){
                if ($key=array_keys($product,DB_PREFIX."product.imagedesabled")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['letoltheto_kep']){
                if ($key=array_keys($product,DB_PREFIX."product.letoltheto_kep")) {
                    unset ($product[$key[0]]);
                }
            }
            if (isset($megjelenit_termekadat['location']) && !$megjelenit_termekadat['location']){
                if ($key=array_keys($product,DB_PREFIX."product.location")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['rovid_leiras']){
                if ($key=array_keys($product,DB_PREFIX."product_description.short_description")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['tesztek']){
                if ($key=array_keys($product,DB_PREFIX."product_description.test_description")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['termekenkenti_szallitasi_dij']){
                if ($key=array_keys($product,DB_PREFIX."product_description.egyedi_szallitas")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['extra_garancia']){
                if ($key=array_keys($product,DB_PREFIX."product_description.garancia_ertek")) {
                    unset ($product[$key[0]]);
                }
                if ($key=array_keys($product,DB_PREFIX."product_description.garancia_egyseg")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['video']){
                if ($key=array_keys($product,DB_PREFIX."product_description.video")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!$megjelenit_termekadat['lizing']){
                if ($key=array_keys($product,DB_PREFIX."product_description.lizing")) {
                    unset ($product[$key[0]]);
                }
            }
            if (!isset($megjelenit_termekadat['securitycode']) || (isset($megjelenit_termekadat['securitycode']) && !$megjelenit_termekadat['securitycode'])){
                if ($key=array_keys($product,DB_PREFIX."product.securitycode")) {
                    unset ($product[$key[0]]);
                }
                if ($key=array_keys($product,DB_PREFIX."product.correctsecuritycode")) {
                    unset ($product[$key[0]]);
                }
            }
        }

        if ($table == 'customer') {
            if ($key=array_keys($product,DB_PREFIX."customer.cart")) {
                unset ($product[$key[0]]);
            }
            if ($key=array_keys($product,DB_PREFIX."customer.wishlist")) {
                unset ($product[$key[0]]);
            }
            if ($key=array_keys($product,DB_PREFIX."customer.token")) {
                unset ($product[$key[0]]);
            }
            if (!$this->store) {
                if ($key=array_keys($product,DB_PREFIX."customer.store_id")) {
                    unset ($product[$key[0]]);
                }
            }
        }


        return $product;
    }

    public function backup($tabla) {
        date_default_timezone_set('Europe/Budapest');

        $this->load->model("localisation/language");
        $nyelvek = $this->model_localisation_language->getLanguages();
        $filename = $tabla.'_'.date('Y-m-d_H:i:s', time()).'.csv';

        $sql = "SELECT ";

        $products   = array();
        $fejlec     = array();
        foreach($this->request->post['backup'] as $value) {
            if (strstr($value,".",1) == DB_PREFIX.$tabla) {
                $products[] = $value;
                $fejlec[]   = $tabla.'.'.substr(strstr($value,".",0),1);
            } elseif (strstr($value,".",1) == DB_PREFIX.$tabla."_description") {
                foreach($nyelvek as $nyelv) {
                    $products[] = "(SELECT ".$value." FROM ".DB_PREFIX.$tabla."_description WHERE ".DB_PREFIX.$tabla."_description.".$tabla."_id=".DB_PREFIX.$tabla.".".$tabla."_id AND language_id=".$nyelv['language_id'].") AS ".$nyelv['name'].'_'.substr(strstr($value,".",0),1);
                    $fejlec[]   = $tabla.'_description.'.substr(strstr($value,".",0),1).'-'.$nyelv['name'];

                }
            } elseif ($value == "Delete") {
                $products[] = "'N'";
                $fejlec[] = "Delete";
            } elseif (strstr($value,".",1) == DB_PREFIX.$tabla."_to_store") {
                $products[] = "(SELECT ".$value." FROM ".DB_PREFIX.$tabla."_to_store WHERE ".DB_PREFIX.$tabla."_to_store.".$tabla."_id=".DB_PREFIX.$tabla.".".$tabla."_id) AS store_id";
                $fejlec[]   = $tabla.'_to_store.store_id';

            }
        }


        $sql .= implode(',',$products);


        $sql .= " FROM ".DB_PREFIX.$tabla." WHERE 1";

        //SELECT `firstname` FROM `hungaro_customer` WHERE lcase(`firstname`) > 'b' AND substring(lcase(`firstname`),1,2) <= 'me' ORDER BY lcase(`firstname`) asc
        if (!empty($this->request->post['filter'])) {
            foreach($this->request->post['filter'] as $filter_key=>$value) {
                $mezo = explode('-',$filter_key);
                $present = substr(strstr($mezo[0],".",0),1);



                if ($mezo[1] == "tol" && $value){
                    if (ctype_digit($value)) {
                        $sql .=" AND ".$mezo[0]." >= ".$value;
                    } else {
                        $sql .=" AND LCASE(".$mezo[0].") LIKE '%" . $this->db->escape(utf8_strtolower($value)) . "%'";
                        //$sql .=" AND LCASE(".$mezo[0].") LIKE '%" . $this->db->escape(utf8_strtolower($value)) . "%'";
                    }

                } elseif ($mezo[1] == "ig" && $value ){
                    if (ctype_digit($value)) {
                        $sql .=" AND ".$mezo[0]." <= ".$value;
                    } else {
                        $sql .=" AND LCASE(".$mezo[0].") LIKE '%" . $this->db->escape(utf8_strtolower($value)) . "%'";
                    }

                } elseif ($mezo[1] == "select" && $value != "no" ){
                    $sql .=" AND ".$mezo[0]." = ".$value;
                }
            }
        }
        //$sql .= " ORDER BY ".DB_PREFIX.$tabla.".".$tabla."_id";
        $sql .= " ORDER BY ".DB_PREFIX.$fejlec[0];

        $sql .= " LIMIT ".(int)$this->request->post['limit_tol'].",";
        $sql .= $this->request->post['limit_db'] ? $this->request->post['limit_db'] : 1844674407370955161;
        $query = $this->db->query($sql);


        if ($query->num_rows > 0) {
            $handle = fopen('../csv/'.$filename, "w");
            //fputcsv ( $handle, $fejlec, ";", '"' ,"\\" );
            fputcsv ( $handle, $fejlec, ";", '"');

            foreach($query->rows as $fields) {
                foreach($fields as $key=>$value) {
                    $value = str_ireplace(array("\r","\n","\t",'"','','','','\\"'),'', $value);
                    $fields[$key] = $value;
                }
                //fputcsv ( $handle, $fields, ";", '"' ,"\\" );
                fputcsv ( $handle, $fields, ";", '"');
            }
            fclose($handle);

        }
        return '../csv/'.$filename;
    }

    public function restore($csv) {
        /*$handle_kodolasnak = fopen($data['filename'], "r");
          $kodolva = mb_detect_encoding(fgets($handle_kodolasnak));
          $kodolva1 = mb_detect_encoding(fgets($handle_kodolasnak));
          $kodolva2 = mb_detect_encoding(fgets($handle_kodolasnak));*/


        require_once(DIR_SYSTEM."library/Encoding.php");
        $encode = new Encoding();
        $csv = $encode->toUTF8($csv);

        $this->load->model("localisation/language");
        $this->nyelvek = $this->model_localisation_language->getLanguages();

        $sorok = str_getcsv($csv, "\n");
        $fejlec = str_getcsv($sorok[0], ";",'"','\\');

        $this->tabla = strstr($fejlec[0],".",1);

        $sql = "SHOW TABLES";
        $query = $this->db->query($sql);
        $this->store = false;
        foreach($query->rows as $value) {
            if (in_array(DB_PREFIX.$this->tabla."_to_store",$value)) {
                $this->store = true;
                break;
            }
        }


        if (!$this->formatumEllenorzes($fejlec)) return false;

        $van_torles = false;
        if ($torol_elem = array_keys($fejlec,"Delete")) {
            $torol_elem = $torol_elem[0];
            $van_torles = true;
        }
        $this->load->model("catalog/product");

        foreach($fejlec as $key=>$value) {
            if ($value != "Delete") {
                $this->tablak[strstr($value,".",1)][substr(strstr($value,".",0),1)] = $key;
            }
        }

        $i = 0;
        $sorok_szama = 0;
        foreach($sorok as $sor) {
            if ($i == 0) {
                $i++;
                continue;
            }
            if (!empty($sor)) {
                $this->ertek = str_getcsv($sor, ";",'"','\\');
                $store_ertek = 0;
                if ($this->store) {
                    if ( $key=array_keys($fejlec,strstr($fejlec[0],".",1)."_to_store.store_id") ) {
                        $store_ertek = $this->ertek[$key[0]];
                    }
                }
                $this->id = $this->letezesVizsgalat(DB_PREFIX.strstr($fejlec[0],".",1),DB_PREFIX.$fejlec[0],$this->ertek[0],$store_ertek);

                if ($van_torles &&  (strtoupper($this->ertek[$torol_elem]) == "I" || strtoupper($this->ertek[$torol_elem]) == "Y") ) {
                    if (strstr($fejlec[0],".",1) == "product") {
                       $this->model_catalog_product->deleteProduct($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "category") {
                        $this->model_catalog_category->deleteCategory($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "attribute_group") {
                        $this->load->model("catalog/attribute_group");
                        $this->model_catalog_attribute_group->deleteAttributeGroup($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "attribute") {
                        $this->load->model("catalog/attribute");
                        $this->model_catalog_attribute->deleteAttribute($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "option") {
                        $this->load->model("catalog/option");
                        $this->model_catalog_option->deleteOption($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "option_value") {
                        $this->load->model("catalog/option");
                        $this->model_catalog_option->deleteOptionValue($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "option_szin") {
                        $this->load->model("catalog/option_szin");
                        $this->model_catalog_option_szin->deleteOptionSzin($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "option_szin_group") {
                        $this->load->model("catalog/option_szin_group");
                        $this->model_catalog_option_szin_group->deleteOptionSzinGroup($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "customer") {
                        $this->load->model("sale/customer");
                        $this->model_sale_customer->deleteCustomer($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "address") {
                        $this->load->model("sale/customer");
                        $this->model_sale_customer->deleteAddress($this->id);

                    } elseif (strstr($fejlec[0],".",1) == "product_special") {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_special_id = '" . (int)$this->id . "'");

                    } elseif (strstr($fejlec[0],".",1) == "product_discount") {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_discount_id = '" . (int)$this->id . "'");

                    } elseif (strstr($fejlec[0],".",1) == "product_customer_special") {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "product_customer_special WHERE product_customer_special_id = '" . (int)$this->id . "'");

                    }
                    continue;
                }

                $i=0;
                $sqls = array();

                foreach($this->ertek as $oszlop_key=>$oszlop) {
                    if ($i == 0) {
                        $i++;
                        continue;
                    }
                    $i++;
                    $language_id = false;
                    if ($nyelvesitett = strstr($fejlec[$oszlop_key],"-")) {
                        $nyelvesitett = substr($nyelvesitett,1);
                        foreach($this->nyelvek as $nyelv) {
                            if ($nyelv['name'] == $nyelvesitett) {
                                $language_id = $nyelv['language_id'];
                                break;
                            }
                        }
                    }
                    if ($fejlec[$oszlop_key] != "Delete") {

                        if ($language_id !== false) {
                            if (!isset($sqls[strstr($fejlec[$oszlop_key],".",1)][$language_id]['update'])) {
                                $sqls[strstr($fejlec[$oszlop_key],".",1)][$language_id]['update'] = "UPDATE ".DB_PREFIX.strstr($fejlec[$oszlop_key],'.',1)." SET ";
                            }
                            $sqls[strstr($fejlec[$oszlop_key],".",1)][$language_id]['update'] = $sqls[strstr($fejlec[$oszlop_key],".",1)][$language_id]['update'].DB_PREFIX.strstr($fejlec[$oszlop_key],'-',1)."='%s',";

                            $sqls[strstr($fejlec[$oszlop_key],".",1)][$language_id]['ertek'][] = '"'.$this->db->escape($oszlop).'"';

                        } else {
                            if (!isset($sqls[strstr($fejlec[$oszlop_key],".",1)][0]['update'])) {
                                $sqls[strstr($fejlec[$oszlop_key],".",1)][0]['update'] = "UPDATE ".DB_PREFIX.strstr($fejlec[$oszlop_key],'.',1)." SET ";
                            }
                            $sqls[strstr($fejlec[$oszlop_key],".",1)][0]['update'] = $sqls[strstr($fejlec[$oszlop_key],".",1)][0]['update'].DB_PREFIX.$fejlec[$oszlop_key]."='%s',";
                            $sqls[strstr($fejlec[$oszlop_key],".",1)][0]['ertek'][] = $this->db->escape($oszlop);
                        }
                    }

                    echo '';
                }

                foreach($sqls as $tabla_name=>$tabla) {
                    foreach($tabla as $language_id=>$felvivo){
                        $sql = substr($sqls[$tabla_name][$language_id]['update'],0,-1)." WHERE ".$this->tabla."_id = '".$this->id."'";
                        if ($language_id) {
                            $sql .= " AND language_id='".$language_id."'";
                        }
                        $sql_update = vsprintf($sql,$felvivo['ertek']);
                        $query = $this->db->query($sql_update);
                        echo '';
                    }
                }
                echo '';

            }
            $sorok_szama++;
        }

        return $sorok_szama;
    }

    public function letezesVizsgalat($tabla,$oszlop,$ertek,$store_id=0) {
        $id = $this->tabla."_id";

        $sql = "SELECT ".$id." FROM ".$tabla." WHERE ".$oszlop."='".$ertek."'";
        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $sql = "INSERT INTO ".DB_PREFIX.$this->tabla." SET ";
            $sql .= substr(strstr($oszlop,'.'),1)."='".$this->db->escape($ertek)."'";
            $query = $this->db->query($sql);
            $vissza = $this->db->getLastId();

        } else {
            $vissza = $query->row[$id];
        }
        if ($this->store) {
            $sql = "DELETE FROM ".$tabla."_to_store WHERE ".$id."=".$vissza;
            $query = $this->db->query($sql);
            $sql = "INSERT IGNORE INTO ".$tabla."_to_store SET ".$id."=".$vissza.", store_id=".$store_id ;
            $query = $this->db->query($sql);
        }

        foreach($this->tablak as $table_key=>$table) {
            if ($table_key != str_replace(DB_PREFIX,'',$this->tabla)) {

                $language_ids = array();
                foreach($table as $mezo_key=>$value){
                    $mezo_nev = explode('-',$mezo_key);
                    if (isset($mezo_nev[1])) {
                        foreach($this->nyelvek as $nyelv) {
                            if ($mezo_nev[1] == $nyelv['name']) {
                                $language_ids[$nyelv['language_id']]=1;
                                break;
                            }
                        }
                    }
                }
                if ($language_ids) {
                    echo '';
                    foreach($language_ids as $language_id=>$value ) {
                        $sql = "INSERT IGNORE INTO ".DB_PREFIX.$table_key." SET ".$id."=".$vissza.", language_id=".$language_id;
                        $query = $this->db->query($sql);
                    }
                }
            }
        }


        return $vissza;
    }

    public function formatumEllenorzes($fejlec) {

        $sql = "SHOW TABLES";
        $query = $this->db->query($sql);
        $mehet = false;
        $description = false;
        foreach($query->rows as $value) {
            if (in_array(DB_PREFIX.$this->tabla,$value)) {
                $mehet = true;
            }
            if (in_array(DB_PREFIX.$this->tabla."_description",$value)) {
                $description = true;
            }
        }
        $vissza = false;

        if ($mehet) {
            $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX.$this->tabla);
            $tartalom = array();
            foreach($oszlop->rows as $curoszlop) {
                $tartalom[] = $this->tabla.'.'.$curoszlop['Field'];
            }

            if ($this->store) {
                $tartalom[] = $this->tabla.'_to_store.store_id';
            }

            if ($description) {
                $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX.$this->tabla."_description");
                foreach($oszlop->rows as $curoszlop) {
                    if($curoszlop['Field'] != $this->tabla.'_id' && $curoszlop['Field'] != 'language_id' ) {
                        foreach($this->nyelvek as $nyelv) {
                            $tartalom[] = $this->tabla.'_description.'.$curoszlop['Field']."-".$nyelv['name'];
                        }
                    }
                }
            }
            $tartalom[] = "Delete";
            $vissza = true;
            foreach($fejlec as $value) {
                if (!in_array($value,$tartalom)) {
                    $vissza = false;
                    break;
                }
            }
        }
        return $vissza;
    }

}