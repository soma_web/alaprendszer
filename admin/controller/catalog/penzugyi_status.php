<?php
class ControllerCatalogPenzugyiStatus extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/penzugyi_status');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/penzugyi_status');

        $this->data['heading_title']    = $this->language->get('heading_title');
        $this->data['button_insert']    = $this->language->get('button_insert');
        $this->data['button_delete']    = $this->language->get('button_delete');
        
        $this->data['column_megnevezes']   = $this->language->get('column_megnevezes');
        $this->data['column_sort_order']= $this->language->get('column_sort_order');
        $this->data['column_action']    = $this->language->get('column_action');
        $this->data['column_status']    = $this->language->get('column_status');

        $this->data['text_no_results']  = $this->language->get('text_no_results');
        $this->data['text_engedelyezett']  = $this->language->get('text_engedelyezett');
        $this->data['text_letiltott']  = $this->language->get('text_letiltott');
        $this->data['error_warning']    = '';
        $this->data['success'] = '';
        $this->data['entry_teljesitett_lista'] = $this->language->get('entry_teljesitett_lista');
        $this->data['entry_alapertelmezett_lista'] = $this->language->get('entry_alapertelmezett_lista');


        $this->getList();
    }

    public function insert() {
        $this->load->language('catalog/penzugyi_status');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/penzugyi_status');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $penzugyi_status = $this->config->get('penzugyi_status');
            $last_id = 0;

            if ($penzugyi_status) {
                if (is_array($penzugyi_status) ) {
                    foreach ($penzugyi_status as $value) {
                        if ($value['penzugyi_status_id'] > $last_id) {
                            $last_id = $value['penzugyi_status_id'];
                        }
                    }
                }
            }
            $last_id++;
            if ( !empty($_REQUEST['megnevezes']) ) {
                $kiir["penzugyi_status"] = $this->config->get("penzugyi_status");
                $kiir["penzugyi_status"][] = array(
                    'megnevezes'        => $_REQUEST['megnevezes'],
                    'sort_order'        => $_REQUEST['sort_order'],
                    'alapertelmezett_sor'  =>  isset($_REQUEST['alapertelmezett_sor']) ? 1 : 0,
                    'teljesitett_sor'    =>  isset($_REQUEST['teljesitett_sor']) ? 1 : 0,
                    'penzugyi_status_id' => $last_id,
                    'status'            => $_REQUEST['status']
                );

                if ( (isset($_REQUEST['teljesitett_sor']) && $_REQUEST['teljesitett_sor'] == 1) || (isset($_REQUEST['alapertelmezett_sor']) && $_REQUEST['alapertelmezett_sor'] == 1) ){
                    foreach ($kiir["penzugyi_status"] as $value) {
                        if ($value['penzugyi_status_id'] ==  $last_id) {

                            if ( isset($_REQUEST['teljesitett_sor']) && $_REQUEST['teljesitett_sor'] == 1 ) {
                                $teljesitett = 1;
                            } else {
                                $teljesitett = 0;
                            }

                            if (isset($_REQUEST['alapertelmezett_sor']) && $_REQUEST['alapertelmezett_sor'] == 1 ){
                                $alapertelmezett = 1;
                            } else {
                                $alapertelmezett = 0;
                            }

                            $modositott["penzugyi_status"][] = array(
                                'megnevezes'           => $_REQUEST['megnevezes'],
                                'sort_order'           => $_REQUEST['sort_order'],
                                'penzugyi_status_id'   => $last_id,
                                'alapertelmezett_sor'      => $alapertelmezett,
                                'teljesitett_sor'      => $teljesitett,
                                'status'               => $_REQUEST['status']

                            );
                        } else {
                            $modositott["penzugyi_status"][] = array(
                                'megnevezes'          => $value['megnevezes'],
                                'sort_order'          => $value['sort_order'],
                                'penzugyi_status_id'  => $value['penzugyi_status_id'],
                                'alapertelmezett_sor' => isset($_REQUEST['alapertelmezett_sor']) ? 0 : $value['alapertelmezett_sor'],
                                'teljesitett_sor'     => isset($_REQUEST['teljesitett_sor']) ? 0 : $value['alapertelmezett_sor'],
                                'status'              => $value['status']
                            );
                        }
                    }
                } else {
                    $modositott = $kiir;
                }
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('penzugyi_status', $modositott);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('catalog/penzugyi_status', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function update() {
        $this->load->language('catalog/penzugyi_status');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/penzugyi_status');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $kiir["penzugyi_status"] = $this->config->get("penzugyi_status");
            foreach ($kiir["penzugyi_status"] as $value) {
                if ($value['penzugyi_status_id'] ==  $_REQUEST['penzugyi_status_id']) {
                    $modositott["penzugyi_status"][] = array(
                        'megnevezes'           => $_REQUEST['megnevezes'],
                        'sort_order'           => $_REQUEST['sort_order'],
                        'penzugyi_status_id'   => $_REQUEST['penzugyi_status_id'],
                        'alapertelmezett_sor'  => isset($_REQUEST['alapertelmezett_sor']) ? 1 : 0,
                        'teljesitett_sor'      => isset($_REQUEST['teljesitett_sor']) ? 1 : 0,
                        'status'               => $_REQUEST['status']

                    );
                } else {
                    $modositott["penzugyi_status"][] = array(
                        'megnevezes'          => $value['megnevezes'],
                        'sort_order'          => $value['sort_order'],
                        'penzugyi_status_id'  => $value['penzugyi_status_id'],
                        'alapertelmezett_sor' => isset($_REQUEST['alapertelmezett_sor']) ? 0 : $value['alapertelmezett_sor'],
                        'teljesitett_sor'     => isset($_REQUEST['teljesitett_sor']) ? 0 : $value['teljesitett_sor'],
                        'status'              => $value['status']
                    );
                }
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('penzugyi_status', $modositott);
            $this->session->data['success'] = $this->language->get('text_success');


            $this->redirect($this->url->link('catalog/penzugyi_status', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/penzugyi_status');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/penzugyi_status');

        if (isset($this->request->post['selected']) ) {

            $kiir["penzugyi_status"] = $this->config->get("penzugyi_status");

            foreach ($kiir["penzugyi_status"] as $value) {

                if ( !in_array($value['penzugyi_status_id'],$_REQUEST['selected']) ) {
                    $modositott["penzugyi_status"][] = array(
                        'megnevezes'        => $value['megnevezes'],
                        'sort_order'        => $value['sort_order'],
                        'penzugyi_status_id'  => $value['penzugyi_status_id'],
                        'alapertelmezett_sor'  => $value['alapertelmezett_sor'],
                        'teljesitett_sor'  => $value['teljesitett_sor'],
                        'status'            => $value['status']
                    );
                }
            }


            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('penzugyi_status', $modositott);
            $this->session->data['success'] = $this->language->get('text_success');


            $this->redirect($this->url->link('catalog/penzugyi_status', 'token=' . $this->session->data['token'], 'SSL'));

        }

        $this->getList();
    }

    private function getList() {

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/penzugyi_status', 'token=' . $this->session->data['token'] , 'SSL'),
            'separator' => ' :: '
        );

        $this->data['insert'] = $this->url->link('catalog/penzugyi_status/insert', 'token=' . $this->session->data['token'] , 'SSL');
        $this->data['delete'] = $this->url->link('catalog/penzugyi_status/delete', 'token=' . $this->session->data['token'] , 'SSL');



        $this->data['penzugyi_statuss'] = array();
        $penzugyi_status = $this->config->get("penzugyi_status");

        if ($penzugyi_status) {
            foreach ($penzugyi_status as $value) {
                $action = array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->url->link('catalog/penzugyi_status/update', 'token=' . $this->session->data['token'].'&penzugyi_status_id='.$value['penzugyi_status_id'], 'SSL')
                );

                if ($value['status'] == 0) {
                    $statusza = $this->data['text_letiltott'];
                } else {
                    $statusza = $this->data['text_engedelyezett'];
                }

                $this->data['penzugyi_statuss'][] = array(
                    'megnevezes'     => $value['megnevezes'],
                    'sort_order'     => $value['sort_order'],
                    'penzugyi_status_id' => $value['penzugyi_status_id'],
                    'alapertelmezett_sor' => $value['alapertelmezett_sor'],
                    'teljesitett_sor' => $value['teljesitett_sor'],
                    'status'         => $value['status'],
                    'statusza'       => $statusza,
                    'action'         => $action,
                    'selected'       => isset($this->request->post['selected']) && in_array($value['penzugyi_status_id'], $this->request->post['selected']),

                );
            }
        }


        $sortingSettings = array(
            0       =>  array(
                'orderby' =>  'sort_order',
                'sortorder'     => 'ASC'
            )
        );

        $rendezo = new ArrayOfArrays($this->data['penzugyi_statuss']);
        $rendezo->multiSorting($sortingSettings,true);
        $this->data['penzugyi_statuss'] = $rendezo->getArrayCopy();


        $this->template = 'catalog/penzugyi_status_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function getForm() {
        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_percent'] = $this->language->get('text_percent');
        $this->data['text_amount'] = $this->language->get('text_amount');

        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['entry_description'] = $this->language->get('entry_description');
        $this->data['entry_code'] = $this->language->get('entry_code');
        $this->data['entry_discount'] = $this->language->get('entry_discount');
        $this->data['entry_logged'] = $this->language->get('entry_logged');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_sorrend'] = $this->language->get('entry_sorrend');
        $this->data['entry_teljesitett'] = $this->language->get('entry_teljesitett');
        $this->data['entry_alapertelmezett'] = $this->language->get('entry_alapertelmezett');


        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['token'] = $this->session->data['token'];

        $this->data['text_engedelyezett']  = $this->language->get('text_engedelyezett');
        $this->data['text_letiltott']  = $this->language->get('text_letiltott');

        if (isset($this->request->get['penzugyi_status_id'])) {
            $penzugyi_status = $this->config->get("penzugyi_status");
            foreach ($penzugyi_status as $value) {
                if ($value['penzugyi_status_id'] == $this->request->get['penzugyi_status_id']) {
                    $this->data['penzugyi_status_id'] = $value['penzugyi_status_id'];
                    $this->data['megnevezes']       = $value['megnevezes'];
                    $this->data['alapertelmezett_sor']  = $value['alapertelmezett_sor'];
                    $this->data['teljesitett_sor']  = $value['teljesitett_sor'];
                    $this->data['sort_order']       = $value['sort_order'];
                    $this->data['status']           = $value['status'];


                }
            }

        } else {
            $this->data['penzugyi_status_id'] = 0;
            $this->data['megnevezes'] = '';
            $this->data['sort_order'] = 0;
            $this->data['alapertelmezett_sor'] = 0;
            $this->data['teljesitett_sor'] = 0;
            $this->data['status']    = 0;

        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }



        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/penzugyi_status', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        if (!isset($this->request->get['penzugyi_status_id'])) {
            $this->data['action'] = $this->url->link('catalog/penzugyi_status/insert', 'token=' . $this->session->data['token'], 'SSL');
        } else {
            $this->data['action'] = $this->url->link('catalog/penzugyi_status/update', 'token=' . $this->session->data['token'] . '&penzugyi_status_id=' . $this->request->get['penzugyi_status_id'], 'SSL');
        }

        $this->data['cancel'] = $this->url->link('catalog/penzugyi_status', 'token=' . $this->session->data['token'], 'SSL');


        $this->template = 'catalog/penzugyi_status_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

}
?>