<?php
class Image {
    private $file;
    private $image;
    private $info;
    private $convertPDFWithImagick=true;

    public function __construct($file) {
        if (file_exists($file)) {
            /*
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_file($finfo, $file);
            finfo_close($finfo);*/

            if(preg_match('/\.pdf/', $file)) {
                $this->convertPDFFirstPageToJPEG($file);
            } else {
                $this->file = $file;

                $info = getimagesize($file);

                $this->info = array(
                    'width'  => $info[0],
                    'height' => $info[1],
                    'bits'   => $info['bits'],
                    'mime'   => $info['mime']
                );

                $this->image = $this->create($file);
            }
        } else {
            exit('Error: Could not load image ' . $file . '!');
        }
    }

    private function convertPDFFirstPageToJPEG($file) {
        $filenameArray = array();
        if(preg_match('/([^\/]+)\.pdf$/', $file, $filenameArray)) {
            $newFile = DIR_CACHE.$filenameArray[1].".jpg";
            if($this->convertPDFWithImagick) {
                $this->convertPDFFirstPageToJPEGWithImagick($file, $newFile);
            } else {
                $this->convertPDFFirstPageToJPEGWithConvert($file, $newFile);
            }
            $this->file = $newFile;

            $info = getimagesize($newFile);

            $this->info = array(
                'width'  => $info[0],
                'height' => $info[1],
                'bits'   => $info['bits'],
                'mime'   => $info['mime']
            );

            $this->image = $this->create($newFile);
        }
    }

    private function convertPDFFirstPageToJPEGWithImagick($file, $newFile) {
        $imagick = new Imagick();
        $imagick->readImage($file.'[0]');
        //$imagick = $imagick->flattenImages();
        $imagick->writeimage($newFile);
    }

    private function convertPDFFirstPageToJPEGWithConvert($file, $newFile) {
        $command = 'convert "'.$file.'[0]" "'.$newFile.'"';
        exec($command);
    }

    private function create($image) {
        $mime = $this->info['mime'];

        if ($mime == 'image/gif') {
            return imagecreatefromgif($image);
        } elseif ($mime == 'image/png') {
            return imagecreatefrompng($image);
        } elseif ($mime == 'image/jpeg') {
            return imagecreatefromjpeg($image);
        } elseif ($mime == 'image/x-ms-bmp') {
            return $this->imagecreatefrombmp($image);
        }
    }

    public function save($file, $quality = 100) {
        $info = pathinfo($file);

        $extension = strtolower($info['extension']);

        if (is_resource($this->image)) {
            if ($extension == 'jpeg' || $extension == 'jpg') {
                imagejpeg($this->image, $file, $quality);
            } elseif($extension == 'png') {
                imagepng($this->image, $file, 0);
            } elseif($extension == 'gif') {
                imagegif($this->image, $file);
            } elseif($extension == 'bmp') {
                $this->imagebmp($this->image, $file);
            }

            imagedestroy($this->image);
        }
    }

    public function resize($width = 0, $height = 0) {
        if (!$this->info['width'] || !$this->info['height']) {
            return;
        }

        if (!$width || !$height) {
            return;
        }

        $xpos = 0;
        $ypos = 0;

        $scale = min($width / $this->info['width'], $height / $this->info['height']);

        if ($scale == 1) {
            return;
        }

        $new_width = (int)($this->info['width'] * $scale);
        $new_height = (int)($this->info['height'] * $scale);
        $xpos = (int)(($width - $new_width) / 2);
        $ypos = (int)(($height - $new_height) / 2);

        $image_old = $this->image;
        $this->image = imagecreatetruecolor($width, $height);

        if (isset($this->info['mime']) && $this->info['mime'] == 'image/png') {
            imagealphablending($this->image, false);
            imagesavealpha($this->image, true);
            $background = imagecolorallocatealpha($this->image, 255, 255, 255, 127);
            imagecolortransparent($this->image, $background);
        } else {
            $background = imagecolorallocate($this->image, 255, 255, 255);
        }

        imagefilledrectangle($this->image, 0, 0, $width, $height, $background);

        imagecopyresampled($this->image, $image_old, $xpos, $ypos, 0, 0, $new_width, $new_height, $this->info['width'], $this->info['height']);
        imagedestroy($image_old);

        $this->info['width']  = $width;
        $this->info['height'] = $height;
    }

    public function watermark($file, $position = 'center') {
        //$watermark = $this->create($file);
        $watermark = imagecreatefrompng($file);
        $watermark_width = imagesx($watermark);
        $watermark_height = imagesy($watermark);

        $image_width = imagesx($this->image);
        $image_height = imagesy($this->image);

        switch($position) {
            case 'topleft':
                $watermark_pos_x = 0;
                $watermark_pos_y = 0;
                break;
            case 'topright':
                $watermark_pos_x = $this->info['width'] - $watermark_width;
                $watermark_pos_y = 0;
                break;
            case 'bottomleft':
                $watermark_pos_x = 0;
                $watermark_pos_y = $this->info['height'] - $watermark_height;
                break;
            case 'bottomright':
                $watermark_pos_x = $this->info['width'] - $watermark_width;
                $watermark_pos_y = $this->info['height'] - $watermark_height;
                break;
            case 'center':
                $watermark_pos_x = ($this->info['width']- $watermark_width)/2;
                $watermark_pos_y = ($this->info['height']- $watermark_height)/2;
                break;
        }


        // $valt1=  imagepng($this->image,DIR_IMAGE. '/teszt1.png');


        imagecopy($this->image,$watermark, 0, 0, 0, 0,  $image_width, $image_height);
        //imagecopy($this->image,$watermark, $watermark_pos_x, $watermark_pos_y, 0, 0,  $watermark_width, $watermark_height);

        // $valt2=  imagepng($this->image,DIR_IMAGE. '/teszt2.png');
        imagejpeg($this->image,$this->file,100);

        imagedestroy($this->image);
    }
    public function crop($top_x, $top_y, $bottom_x, $bottom_y) {
        $image_old = $this->image;
        $this->image = imagecreatetruecolor($bottom_x - $top_x, $bottom_y - $top_y);

        imagecopy($this->image, $image_old, 0, 0, $top_x, $top_y, $this->info['width'], $this->info['height']);
        imagedestroy($image_old);

        $this->info['width'] = $bottom_x - $top_x;
        $this->info['height'] = $bottom_y - $top_y;
    }

    public function rotate($degree, $color = 'FFFFFF') {
        $rgb = $this->html2rgb($color);

        $this->image = imagerotate($this->image, $degree, imagecolorallocate($this->image, $rgb[0], $rgb[1], $rgb[2]));

        $this->info['width'] = imagesx($this->image);
        $this->info['height'] = imagesy($this->image);
    }

    private function filter($filter) {
        imagefilter($this->image, $filter);
    }

    private function text($text, $x = 0, $y = 0, $size = 5, $color = '000000') {
        $rgb = $this->html2rgb($color);

        imagestring($this->image, $size, $x, $y, $text, imagecolorallocate($this->image, $rgb[0], $rgb[1], $rgb[2]));
    }

    private function merge($file, $x = 0, $y = 0, $opacity = 100) {
        $merge = $this->create($file);

        $merge_width = imagesx($this->image);
        $merge_height = imagesy($this->image);

        imagecopymerge($this->image, $merge, $x, $y, 0, 0, $merge_width, $merge_height, $opacity);
    }

    private function html2rgb($color) {
        if ($color[0] == '#') {
            $color = substr($color, 1);
        }

        if (strlen($color) == 6) {
            list($r, $g, $b) = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);
        } elseif (strlen($color) == 3) {
            list($r, $g, $b) = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
        } else {
            return false;
        }

        $r = hexdec($r);
        $g = hexdec($g);
        $b = hexdec($b);

        return array($r, $g, $b);
    }


    public function imagecreatefrombmp($p_sFile)
    {
        $file    =    fopen($p_sFile,"rb");
        $read    =    fread($file,10);
        while(!feof($file)&&($read<>""))
            $read    .=    fread($file,1024);
        $temp    =    unpack("H*",$read);
        $hex    =    $temp[1];
        $header    =    substr($hex,0,108);
        if (substr($header,0,4)=="424d")
        {
            $header_parts    =    str_split($header,2);
            $width            =    hexdec($header_parts[19].$header_parts[18]);
            $height            =    hexdec($header_parts[23].$header_parts[22]);
            unset($header_parts);
        }
        $x                =    0;
        $y                =    1;
        $image            =    imagecreatetruecolor($width,$height);
        $body            =    substr($hex,108);
        $body_size        =    (strlen($body)/2);
        $header_size    =    ($width*$height);
        $usePadding        =    ($body_size>($header_size*3)+4);
        for ($i=0;$i<$body_size;$i+=3)
        {
            if ($x>=$width)
            {
                if ($usePadding)
                    $i    +=    $width%4;
                $x    =    0;
                $y++;
                if ($y>$height)
                    break;
            }
            $i_pos    =    $i*2;
            $r        =    hexdec($body[$i_pos+4].$body[$i_pos+5]);
            $g        =    hexdec($body[$i_pos+2].$body[$i_pos+3]);
            $b        =    hexdec($body[$i_pos].$body[$i_pos+1]);
            $color    =    imagecolorallocate($image,$r,$g,$b);
            imagesetpixel($image,$x,$height-$y,$color);
            $x++;
        }
        unset($body);
        return $image;
    }

    public function imagebmp(&$img, $filename = false) {
        $wid = imagesx($img);
        $hei = imagesy($img);
        $wid_pad = str_pad('', (4-ceil($wid/8) % 4) %4, "\0");

        $size = 62 + ( ceil($wid/8) + strlen($wid_pad)) * $hei;

        //prepare & save header
        $header['identifier']       = 'BM';
        $header['file_size']        = $this->dword($size);
        $header['reserved']         = $this->dword(0);
        $header['bitmap_data']      = $this->dword(62);
        $header['header_size']      = $this->dword(40);
        $header['width']            = $this->dword($wid);
        $header['height']           = $this->dword($hei);
        $header['planes']           = $this->word(1);
        $header['bits_per_pixel']   = $this->word(1);
        $header['compression']      = $this->dword(0);
        $header['data_size']        = $this->dword(0);
        $header['h_resolution']     = $this->dword(0);
        $header['v_resolution']     = $this->dword(0);
        $header['colors']           = $this->dword(0);
        $header['important_colors'] = $this->dword(0);
        $header['white']    = chr(255).chr(255).chr(255).chr(0);
        $header['black']    = chr(0).chr(0).chr(0).chr(0);

        if ($filename) {
            $f = fopen($filename, "wb");
            foreach ($header AS $h) {
                fwrite($f, $h);
            }

            //save pixels
            $str="";
            for ($y=$hei-1; $y>=0; $y--) {
                $str="";
                for ($x=0; $x<$wid; $x++)
                {
                    $rgb = imagecolorat($img, $x, $y);
                    $r = ($rgb >> 16) & 0xFF;
                    $g = ($rgb >> 8) & 0xFF;
                    $b = $rgb & 0xFF;
                    $gs = (($r*0.299)+($g*0.587)+($b*0.114));
                    if($gs>150) $color=0;
                    else $color=1;
                    $str=$str.$color;
                    if($x==$wid-1){
                        $str=str_pad($str, 8, "0");
                    }
                    if(strlen($str)==8){
                        fwrite($f, chr((int)bindec($str)));
                        $str="";
                    }
                }
                fwrite($f, $wid_pad);
            }
            fclose($f);

        } else {
            foreach ($header AS $h)
            {
                echo $h;
            }

            //save pixels
            for ($y=$hei-1; $y>=0; $y--)
            {
                for ($x=0; $x<$wid; $x++)
                {
                    $rgb = imagecolorat($img, $x, $y);
                    $r = ($rgb >> 16) & 0xFF;
                    $g = ($rgb >> 8) & 0xFF;
                    $b = $rgb & 0xFF;
                    $gs = (($r*0.299)+($g*0.587)+($b*0.114));
                    if($gs>150) $color=0;
                    else $color=1;
                    $str=$color;
                    if($x==$wid-1){
                        $str=str_pad($str, 8, "0");
                    }
                    if(strlen($str)==8){
                        echo chr((int)bindec($str));
                        $str="";
                    }
                }
                echo $wid_pad;
            }
        }
    }


    function dword($n)
    {
        return pack("V", $n);
    }
    function word($n)
    {
        return pack("v", $n);
    }

}
?>