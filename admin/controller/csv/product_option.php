<?php
class ControllerCsvProductOption extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('csv/product');
        $this->load->language('csv/product_option');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('csv/product_option');

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
            if (is_uploaded_file($this->request->files['import']['tmp_name'])) {
                $content = file_get_contents($this->request->files['import']['tmp_name']);
            } else {
                $content = false;
            }

            if ($content) {
                if ($i=$this->model_csv_product_option->restore($content)) {
                    $this->session->data['success'] = $this->language->get('text_success').' ('.$i.'-tétel)';
                } else {
                    $this->session->data['warning'] = $this->language->get('error_format');
                }
                $this->redirect($this->url->link('csv/product_option', 'token=' . $this->session->data['token'], 'SSL'));
            } else {
                $this->error['warning'] = $this->language->get('error_empty');
            }
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_select'] = $this->language->get('text_select');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_tol'] = $this->language->get('text_tol');
        $this->data['text_db'] = $this->language->get('text_db');
        $this->data['text_sorrend'] = $this->language->get('text_sorrend');

        $this->data['text_select_all'] = $this->language->get('text_select_all');
        $this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

        $this->data['entry_restore'] = $this->language->get('entry_restore');
        $this->data['entry_backup'] = $this->language->get('entry_backup');

        $this->data['button_backup'] = $this->language->get('button_backup');
        $this->data['button_restore'] = $this->language->get('button_restore');


        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];

        } elseif (isset($this->session->data['warning'])) {
            $this->data['error_warning'] = $this->session->data['warning'];
            unset($this->session->data['warning']);

        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('csv/product_option', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['restore'] = $this->url->link('csv/product_option', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['backup'] = $this->url->link('csv/product_option/backup', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['fields'] = $this->model_csv_product_option->getFields('product_option');

        $tol_ig = array();
        $tol_ig["product_option_value_id"]  = "input";
        $tol_ig["product_option_id"]        = "input";
        $tol_ig["product_id"]               = "input";
        $tol_ig["model"]                    = "input";
        $tol_ig["product_name"]             = "input";
        $tol_ig["option_id"]                = "input";
        $tol_ig["option_name"]              = "input";
        $tol_ig["option_type"]              = "input";
        $tol_ig["option_value_id"]          = "input";
        $tol_ig["option_value_name"]        = "input";
        $tol_ig["quantity"]                 = "input";
        $tol_ig["price"]                    = "input";
        $tol_ig["option_szin_id"]           = "input";
        $tol_ig["azonosito"]                = "input";
        $tol_ig["required"]                 = "select";
        $this->data['tol_ig'] = $tol_ig;

        $megjeloles = array();
        $megjeloles["product_id"]  = "*";
        $megjeloles["model"]  = "*";
        $megjeloles["option_id"]  = "1";
        $megjeloles["option_szin_id"]  = "1";
        $this->data['megjeloles'] = $megjeloles;

        $this->data['megjeloles_magyarazat'] = $this->language->get("text_magyarazat");


        $this->data['limit_tol'] = '';
        $this->data['limit_ig'] = '';

        $this->data['no_checked'] = true;
        $this->template = 'csv/product.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    public function backup() {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->load->language('csv/product');
            $this->load->language('csv/product_option');
            //$this->load->model('csv/product_option');
            $this->load->model('csv/product_option');
            //$file = $this->model_csv_product_option->backup('product_option');
            $file = $this->model_csv_product_option->backup('product_option_value');

            if (file_exists($file)) {

                header("Content-Type: text/csv;charset=UTF-8" );
                header('Content-Disposition: attachment; filename='.basename($file));
                header("Pragma: no-cache");
                header("Expires: 0");


                /*header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($file));
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));*/
                readfile($file);
                exit;
            } else {
                $this->error['warning'] = $this->language->get('error_empty_result');
                $this->index();
            }

        } else {
            return $this->forward('error/permission');
        }
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'csv/product_option')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}
?>