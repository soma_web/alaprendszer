<?php $modularis = $this->config->get('lista_beallitas_lista');?>
<?php $modularis = $modularis['modularis']?>

<?php if (!$modularis) { ?>
    <?php if ($this->config->get('megjelenit_lista_modularis') != 1) { ?>
        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/sale.png')) {
            $sale = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/sale.png';
        } else {
            $sale = DIR_TEMPLATE_IMAGE.'default/image/sale.png';
        } ?>

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/ujtermek.png')) {
            $ujtermek = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/ujtermek.png';
        } else {
            $ujtermek = DIR_TEMPLATE_IMAGE.'default/image/ujtermek.png';
        } ?>

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/pardarab.png')) {
            $pardarab = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/pardarab.png';
        } else {
            $pardarab = DIR_TEMPLATE_IMAGE.'default/image/pardarab.png';
        } ?>

        <div>
            <div class="left">
                <?php if ($product['thumb']) { ?>
                    <div style="" class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                <?php } ?>

            </div>

            <div class="right">
                <?php if($this->config->get('megjelenit_lista_kosarba') == 1){ ?>
                    <div class="cart" style="min-width: 160px" >
                        <?php if($this->config->get('megjelenit_lista_mennyiseg') == 1){ ?>
                            <input class="mennyit_vasarol" id="category-list-quantity_<?php echo $product['product_id']; ?>" type="text" value="1" />
                        <? } else {?>
                            <input class="mennyit_vasarol" id="category-list-quantity_<?php echo $product['product_id']; ?>" type="hidden" value="1" />
                        <? } ?>

                        <?if ($product['csomagolasi_mennyiseg'] > 1 && $this->config->get('megjelenit_csomagolas_admin') == 1 && $this->config->get('megjelenit_lista_csomagolasi_mennyiseg') == 1){ ?>
                            <div class="csomagolasi_mennyiseg_lista" >
                                <span style="margin-left: 4px;"><?echo 'x '.$product['csomagolasi_mennyiseg'].' '. $product['megyseg'].'/'.$product['csomagolasi_egyseg']?></span>
                            </div>
                            <div style="clear: both">
                                <span class="atmenet-hatter">
                                <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>',$('#category-list-quantity_<?php echo $product['product_id']; ?>').val()*<? echo $product['csomagolasi_mennyiseg']?>,'<?php echo $this->config->get('config_template')?>','<?php echo $module_name.$product['product_id']?>')" class="button" />
                                </span>
                            </div>
                        <?} else { $product['csomagolasi_mennyiseg'] = 1?>
                        <span class="atmenet-hatter">
                            <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>',
                                                $('#category-list-quantity_<?php echo $product['product_id']; ?>').val()*<? echo $product['csomagolasi_mennyiseg']?>,
                                                '<?php echo $this->config->get('config_template')?>','<?php echo $module_name.$product['product_id']?>')" class="button" />
                        </span>
                        <?}?>
                    </div>
                <? } else {?>
                    <div class="cart">
                        <a href="<?php echo $product['href']; ?>">
                            <input  type="button" style="" value="<?php echo $button_bovebben; ?>" class="button" />
                        </a>
                    </div>
                <? } ?>


                <?php if($this->config->get('megjelenit_lista_kivansaglista') == 1){ ?>
                    <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
                <?php }?>
                <?php if($this->config->get('megjelenit_lista_osszehasonlitas') == 1){ ?>
                    <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></div>
                <?php }?>
            </div>

            <div class="right">

                <?php if( $this->config->get('megjelenit_lista_brutto_ar') == 1 && $this->config->get('megjelenit_lista_netto_ar') == 1 &&  $this->config->get('megjelenit_lista_brutto_netto') == 1) { ?>
                    <?php if ($product['price']) { ?>
                        <div style="" class="price">
                            <?php if (!$product['special']) { ?>
                                <span class="price-new"><?php echo $product['price']; ?></span>
                                <br />
                                <span class="price-tax"><?php echo "Netto:"?> <?php echo $product['price_netto']; ?></span>

                            <?php } else { ?>
                                <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                <br />
                                <span class="price-tax"><?php echo "Netto:"?> <?php echo $product['special_netto']; ?></span>
                            <?php } ?>

                        </div>
                    <?php } ?>

                <?php } elseif($this->config->get('megjelenit_lista_brutto_ar') == 1 && $this->config->get('megjelenit_lista_netto_ar') == 1 &&  $this->config->get('megjelenit_brutto_netto') == 0 ) {?>
                    <?php if ($product['price']) { ?>
                        <div style="" class="price">
                            <?php if (!$product['special_netto']) { ?>
                                <span class="price-new"><?php echo $product['price_netto']; ?></span>
                                <br />
                                <span class="price-tax"><?php echo "Brutto:"; ?> <?php echo $product['price']; ?></span>

                            <?php } else { ?>
                                <span class="price-old"><?php echo $product['price_netto']; ?></span> <span class="price-new"><?php echo $product['special_netto']; ?></span>
                                <br />
                                <span class="price-tax"><?php echo "Brutto:"; ?> <?php echo $product['special']; ?></span>
                            <?php } ?>
                        </div>
                    <?php }?>

                <?php } elseif($this->config->get('megjelenit_lista_brutto_ar') == 1 && $this->config->get('megjelenit_lista_netto_ar') == 0  ) {?>
                    <?php if ($product['price']) { ?>
                        <div style="" class="price">
                            <?php if (!$product['special']) { ?>
                                <span class="price-new"><?php echo $product['price']; ?></span>
                            <?php } else { ?>
                                <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>

                <?php } elseif($this->config->get('megjelenit_lista_brutto_ar') == 0 && $this->config->get('megjelenit_lista_netto_ar') == 1  ) {?>
                    <?php if ($product['price']) { ?>
                        <div style="" class="price">
                            <?php if (!$product['special_netto']) { ?>
                                <span class="price-tax"><?php echo "Netto:"; ?></span>
                                <span class="price-new"><?php echo $product['price_netto']; ?></span>
                            <?php } else { ?>
                                <span class="price-tax"><?php echo "Netto:"; ?></span>
                                <span class="price-old"><?php echo $product['price_netto']; ?></span> <span class="price-new"><?php echo $product['special_netto']; ?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>

            </div>

            <div>
                <div class="ikonok">
                    <?php if ($product['special']) { ?>
                        <?php if($this->config->get('megjelenit_lista_szazalek') == 1){ ?>
                            <img class="icon_meret" title="<? if (isset($text_akcio_title)) { echo $text_akcio_title; }?>"  src="<? echo $sale?>" >
                        <?php }?>
                    <?}?>
                    <?php if ($product['pardarab']) { ?>
                        <?php if($this->config->get('megjelenit_lista_pardarab') == 1){ ?>
                            <img class="icon_meret" title="<? if (isset($text_pardarab_title)) { echo $text_pardarab_title; }?>" src="<? echo $pardarab?>" >
                        <?}?>
                    <?}?>
                    <?php if ($product['uj']) { ?>
                        <?php if($this->config->get('megjelenit_lista_uj') == 1){ ?>
                            <img class="icon_meret" title="<? if (isset($text_uj_title)) { echo $text_uj_title; }?>" src="<? echo $ujtermek?>" >
                        <?}?>
                    <?}?>
                </div>

                <div  class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>

                <?php if($this->config->get('megjelenit_lista_description') == 1){ ?>
                    <div class="description">
                        <?php echo mb_substr($product['description'], 0, $leiras_karakter_lista, "UTF-8")."..."; ?>
                    </div>
                <?php }?>


                <?php if ($product['rating']) { ?>
                    <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
                <?php } ?>
            </div>


        </div>
    <?php } else {


        $termek_dobozok = $this->config->get('megjelenit_lista_termek_doboz');

        if ($termek_dobozok) {

            $oszlop = -1;
            $sor = 0;
            $nyitott = false;
            $module_name .= "_lista";

            echo "<div>";
            foreach($termek_dobozok as $key=>$beallitas) {
                if ($beallitas['status'] == 1 ) {

                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/termek_doboz/'. $key .'.tpl')) {
                        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/termek_doboz/'.$key . '.tpl';
                    } else {
                        $arkiir = DIR_TEMPLATE.'default/template/module/termek_doboz/'.$key.'.tpl';
                    }
                    if ($oszlop != $beallitas['sort_order_oszlop']) {
                        $oszlop = $beallitas['sort_order_oszlop'];
                        if ($nyitott) {
                            $nyitott = false;
                            echo "</div>";
                        }

                        ?>
                        <div class="lista-oszlop-<?php echo $beallitas['sort_order_oszlop']?>"> <?php
                        $nyitott = true;
                    }

                    include($arkiir);

                }

            }

            if ($nyitott) {
                echo "</div>";
            }
        echo "</div>";

        }
    } ?>
<?php } else { ?>
    <?php $megjelenit_lista_csoportok = $this->config->get('lista_beallitas_lista_csoportok'); ?>
    <?php if ($megjelenit_lista_csoportok)  { ?>

        <?php
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_lista.php')) {
            $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/stylesheet/stylesheet_lista.php';
            include_once($arkiir);

        } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_lista.php')) {
            $arkiir = DIR_TEMPLATE.'default/stylesheet/stylesheet_lista.php';
            include_once($arkiir);

        } ?>

        <?php   $elso       = true;
        $elozo      = 0;
        $divszamol  = 1;
        $div_nyit   = 0;
        $div_zar    = 0;
        $module_name .= "_lista";

        ?>

        <?php foreach ($megjelenit_lista_csoportok as $key=>$value) { ?>
            <?php
            if ($elso) {$elso = false;}
            else {
                $elozo = $divszamol;
            }
            $divszamol = count(explode('_',$key));

            if($divszamol <= $elozo) {
                for($i=$divszamol; $elozo >=$i; $i++) {
                    echo "</div>";
                    $div_zar ++;

                }
            }
            $elotag = ($divszamol%2 == 0) ? "lista_oszlop_" : "lista_sor_";
            $style_inlines = isset($value['default']['inline']) && $value['default']['inline'] ? htmlspecialchars_decode($value['default']['inline']) : '';
            if ($style_inlines) {
                $style_inlines = str_replace(' ','',$style_inlines);
                $style_inline = explode(';',$style_inlines);

                foreach($style_inline as $key_inline=>$inline) {
                    $pos = strpos("$inline","$");
                    if ($pos !== false) {
                        $pos1 = strpos($inline,"this",$pos);
                        if ($pos1 == $pos+1) {
                            $pos2 = strpos($inline,"-",$pos1);
                            if ($pos2 == $pos1+4) {
                                $pos3 = strpos($inline,">",$pos2);
                                if ($pos3 == $pos2+1) {
                                    $pos4 = strpos($inline,"config",$pos3);
                                    if ($pos4 == $pos3+1) {
                                        $pos5 = strpos($inline,"-",$pos4);
                                        if ($pos5 == $pos4+6) {
                                            $pos6 = strpos($inline,">",$pos5);
                                            if ($pos6 == $pos5+1) {
                                                $pos7 = strpos($inline,"get",$pos6);
                                                if ($pos7 == $pos6+1) {
                                                    $parameter_pos_tol = strpos($inline,"'",$pos7);
                                                    if ($parameter_pos_tol == $pos7+4) {
                                                        $parameter_pos_tol ++;
                                                        $parameter_pos_ig = strpos($inline,"'",$parameter_pos_tol);
                                                        $parameter_pos_ig;
                                                       $parameter = substr($inline,$parameter_pos_tol,($parameter_pos_ig-$parameter_pos_tol));

                                                    } else {
                                                        $parameter_pos_tol = strpos($inline,'"',$pos7);
                                                        if ($parameter_pos_tol == $pos7+4) {
                                                            $parameter_pos_tol ++;
                                                            $parameter_pos_ig = strpos($inline,"'",$parameter_pos_tol);
                                                            $parameter = substr($inline,$parameter_pos_tol,($parameter_pos_ig-$parameter_pos_tol));
                                                        }
                                                    }

                                                    if (isset($parameter) && $parameter) {
                                                        $eredmeny=$this->config->get($parameter);
                                                        $style_inline[$key_inline] = substr($inline,0,$pos);
                                                        $style_inline[$key_inline] .= $eredmeny;
                                                        $style_inline[$key_inline] .= substr($inline,$parameter_pos_ig+2);
                                                    }

                                                }

                                            }

                                        }

                                    }

                                }

                            }
                        }
                    }
                }
                $style_inlines = implode(';',$style_inline);
            }

            ?>

            <div class="<?php echo $elotag.$key. ' ' . $value['default']['class']?> " style="<?php echo $style_inlines?>">
                <?php $div_nyit++?>
                <?php if ($value['default']['status'] == 1) { ?>
                    <?php if(isset($value['templates']) && !empty($value['templates'])) { ?>
                        <?php foreach ($value['templates'] as $keytemplate=>$beallitas) { ?>
                            <?php if (isset($beallitas['status']) && $beallitas['status'] == 1) {?>
                                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/termek_doboz/'. $keytemplate .'.tpl')) {
                                    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/termek_doboz/'.$keytemplate . '.tpl';
                                } else {
                                    $arkiir = DIR_TEMPLATE.'default/template/module/termek_doboz/'.$keytemplate.'.tpl';
                                } ?>
                                <?php include($arkiir); ?>
                            <?php } ?>

                        <?php } ?>
                    <?php } ?>
                <?php } ?>
        <?php }


        $div_nyitva = $div_nyit - $div_zar;
        if ($div_nyit - $div_zar > 0) {
            $div_nyitva = $div_nyit - $div_zar;
            for ($i=0; $div_nyitva > $i; $i++) {
                echo "</div>";
            }
        }
        ?>
    <?php } ?>
<?php } ?>



<script type="text/javascript"><!--

    $(document).ready(function() {
        <?php if (!empty($nezet) ) { ?>
            $.cookie('display', '<?php echo $nezet?>');
            <?php if ($nezet == "list") { ?>
                var listposition = $(".product-list").offset().top;
        <?php } else { ?>
                var listposition = $(".box-product").offset().top;
            <?php } ?>
            $('html body').animate({ scrollTop: listposition-10 }, 'fast');
        <?php } ?>

    });

    function display(view) {
        if (view == 'list') {

            $('#content .box-product').css('display', 'none');
            $('#content .product-list').css('display', 'block');

            $('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
            $.cookie('display', 'list');
        } else {
            $('#content .product-list').css('display', 'none');
            $('#content .box-product').css('display', 'block');

            $('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');
            $.cookie('display', 'grid');
        }
    }

    function display_iconnal(view) {
        if (view == 'list') {

            $('#content .product-list').css('display', 'block');
            $('#content .box-product').css('display', 'none');

            $('.display').html('<b><?php echo $text_display; ?></b> <div id="list_b"></div>  <a id="grid_a" onclick="display(\'grid\');"  title="<? echo $text_grid?>"></a>');
            $.cookie('display', 'list');
        } else {
            $('#content .box-product').css('display', 'block');
            $('#content .product-list').css('display', 'none');

            $('.display').html('<b><?php echo $text_display; ?></b> <a id="list_a" onclick="display(\'list\');" title="<? echo $text_list?>"><?php echo $text_list; ?></a> <div id="grid_b"></div>');
            $.cookie('display', 'grid');
        }
    }




    view = $.cookie('display');

    if (view) {
        <?php if (isset($nezet_icon)){ ?>
            display_iconnal(view);
        <?php } else {?>
            display(view);
        <?php }?>

    } else {
        <?php if (isset($nezet_icon)){ ?>
            display_iconnal('grid');
        <?php } else {?>
            display('grid');
        <?php }?>
    }
//--></script>