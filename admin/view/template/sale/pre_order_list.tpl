<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>

  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>

  <div class="box">
    <div class="heading">
      <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <!--<a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>-->
          <a onclick="$('#form').attr('action', '<?php echo $delete; ?>'); $('#form').attr('target', '_self'); $('#form').submit();" class="button"><?php echo $button_delete; ?></a>
      </div>
    </div>

    <div class="content">
      <form action="" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

                <td class="right">
                    <?php if ($sort == 'o.pre_order_id') { ?>
                        <a href="<?php echo $sort_pre_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_pre_order_id; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo $sort_pre_order; ?>"><?php echo $column_pre_order_id; ?></a>
                    <?php } ?>
                </td>

                <td class="right">
                    <?php if ($sort == 'o.product_name') { ?>
                        <a href="<?php echo $sort_product_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_name; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo $sort_product_name; ?>"><?php echo $column_product_name; ?></a>
                    <?php } ?>
                </td>

                <td class="right">
                    <?php if ($sort == 'o.model') { ?>
                        <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                    <?php } ?>
                </td>


                <td class="right">
                    <?php if ($sort == 'o.megrendelo_name') { ?>
                        <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                    <?php } ?>
                </td>

                <td class="right">
                    <?php if ($sort == 'o.megrendelo_email') { ?>
                        <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                    <?php } ?>
                </td>


                <td class="right">
                    <?php if ($sort == 'o.date_added') { ?>
                        <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                    <?php } ?>
                </td>



              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>

          <tbody>
            <tr class="filter">
                <td></td>
                <td align="right">
                    <input type="text" name="filter_pre_order_id" value="<?php echo $filter_pre_order_id; ?>" size="4" style="text-align: right;" />
                </td>

                <td align="right">
                    <input type="text" name="filter_product_name" value="<?php echo $filter_product_name; ?>" size="14" style="text-align: right;" />
                </td>

                <td align="right">
                    <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" size="14" style="text-align: right;" />
                </td>

                <td align="right">
                    <input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" size="14" style="text-align: right;" />
                </td>

                <td align="right">
                    <input type="text" name="filter_email" value="<?php echo $filter_email; ?>" size="14" style="text-align: right;" />
                </td>

                <td align="right">
                    <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" class="date" />
                </td>


                <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
            </tr>

            <?php if ($pre_orders) { ?>
                <?php foreach ($pre_orders as $pre_order) { ?>
                    <tr>
                        <td style="text-align: center;">
                            <?php if ($pre_order['selected']) { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $pre_order['pre_order_id']; ?>" checked="checked" />
                            <?php } else { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $pre_order['pre_order_id']; ?>" />
                            <?php } ?>
                        </td>
                        <td class="right"><?php echo $pre_order['pre_order_id']; ?></td>
                        <td class="right"><?php echo $pre_order['product_name']; ?></td>
                        <td class="right"><?php echo $pre_order['model']; ?></td>
                        <td class="right"><?php echo $pre_order['megrendelo_name']; ?></td>
                        <td class="right"><?php echo $pre_order['megrendelo_email']; ?></td>
                        <td class="right"><?php echo $pre_order['date_added']; ?></td>


                        <td class="right">
                            [ <a href="<?php echo $pre_order['action']['href']; ?>"><?php echo $pre_order['action']['text']; ?></a> ]
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>


      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--


    function filter() {
        url = 'index.php?route=sale/pre_order&token=<?php echo $token; ?>';

        var filter_pre_order_id = $('input[name=\'filter_pre_order_id\']').attr('value');
        if (filter_pre_order_id) {
            url += '&filter_pre_order_id=' + encodeURIComponent(filter_pre_order_id);
        }

        var filter_product_name = $('input[name=\'filter_product_name\']').attr('value');
        if (filter_product_name) {
            url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
        }

        var filter_model = $('input[name=\'filter_model\']').attr('value');
        if (filter_model) {
            url += '&filter_model=' + encodeURIComponent(filter_model);
        }

        var filter_customer = $('input[name=\'filter_customer\']').attr('value');
        if (filter_customer) {
            url += '&filter_customer=' + encodeURIComponent(filter_customer);
        }

        var filter_email = $('input[name=\'filter_email\']').attr('value');
        if (filter_email) {
            url += '&filter_email=' + encodeURIComponent(filter_email);
        }

        var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');
        if (filter_date_added) {
            url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
        }

        var filter_date_modified = $('input[name=\'filter_date_modified\']').attr('value');
        if (filter_date_modified) {
            url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
        }

        location = url;
    }
//--></script>  
<script type="text/javascript"><!--
    $(document).ready(function() {
        $('.date').datepicker({dateFormat: 'yy-mm-dd'});
    });
//--></script> 
<script type="text/javascript"><!--
    $('#form input').keydown(function(e) {
        if (e.keyCode == 13) {
            filter();
        }
    });
//--></script> 
<script type="text/javascript"><!--
    $.widget('custom.catcomplete', $.ui.autocomplete, {
        _renderMenu: function(ul, items) {
            var self = this, currentCategory = '';

            $.each(items, function(index, item) {
                if (item.category != currentCategory) {
                    ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');

                    currentCategory = item.category;
                }

                self._renderItem(ul, item);
            });
        }
    });

    $('input[name=\'filter_customer\']').catcomplete({
        delay: 0,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            category: item.customer_group,
                            label: item.name,
                            value: item.customer_id
                        }
                    }));
                }
            });
        },
        select: function(event, ui) {
            $('input[name=\'filter_customer\']').val(ui.item.label);

            return false;
        }
    });
//--></script> 
<?php echo $footer; ?>