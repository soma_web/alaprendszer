<?php
class ModelCatalogCrmKapcsolat extends Model {

    /*----- Products ------*/

    public function getCrmProducts($crm_category_id='') {
        $vissza = array();
        if (count($this->db_crm)) {
            $sql = "SELECT productid, product_no, productname FROM vtiger_products ";
            if ($crm_category_id) {
                $name = $this->getCrmProductCategory($crm_category_id);
                $sql .= " WHERE productcategory LIKE '".$name."'";
            }
            $query = $this->db_crm->query($sql);
            $vissza = $query->rows;
        }
        return $vissza;
    }

    public function getCrmProductCategoryes($data=array()) {
        $vissza = array();
        if (count($this->db_crm)) {
            $sql = "SELECT pc.productcategoryid, pc.productcategory FROM vtiger_productcategory pc ";
            $sql .= " INNER JOIN vtiger_products p ON(p.productcategory=pc.productcategory)";
            $sql .= " WHERE pc.presence=1 ";
            $sql .= " GROUP BY pc.productcategoryid ";
            $query = $this->db_crm->query($sql);
            $vissza = $query->rows;
        }
        return $vissza;
    }

    public function getCrmProductCategory($crm_category_id) {
        $vissza = '';
        if (count($this->db_crm)) {
            $sql = "SELECT productcategory FROM vtiger_productcategory WHERE productcategoryid = '".$crm_category_id."'";
            $query = $this->db_crm->query($sql);
            $vissza = !empty($query->row['productcategory']) ? $query->row['productcategory'] : '';
        }
        return $vissza;
    }




    /*----- Szolgáltatás ------*/

    public function getCrmServices($crm_category_id='') {
        $vissza = array();
        if (count($this->db_crm)) {
            $sql = "SELECT serviceid AS productid, service_no AS product_no, servicename AS productname FROM vtiger_service ";
            if ($crm_category_id) {
                $name = $this->getCrmServiceCategory($crm_category_id);
                $sql .= " WHERE servicecategory LIKE '".$name."'";
            }
            $query = $this->db_crm->query($sql);
            $vissza = $query->rows;
        }
        return $vissza;
    }

    public function getCrmServiceCategoryes($data=array()) {
        $vissza = array();
        if (count($this->db_crm)) {
            $sql = "SELECT sc.servicecategoryid AS productcategoryid, sc.servicecategory AS productcategory FROM vtiger_servicecategory sc ";
            $sql .= " INNER JOIN vtiger_service s ON(s.servicecategory=sc.servicecategory)";
            $sql .= " WHERE sc.presence=1";
            $sql .= " GROUP BY sc.servicecategoryid ";

            $query = $this->db_crm->query($sql);
            $vissza = $query->rows;
        }
        return $vissza;
    }

    public function getCrmServiceCategory($crm_category_id) {
        $vissza = '';
        if (count($this->db_crm)) {
            $sql = "SELECT servicecategory FROM vtiger_servicecategory WHERE servicecategoryid = '".$crm_category_id."'";
            $query = $this->db_crm->query($sql);
            $vissza = $query->row['servicecategory'];
        }
        return $vissza;
    }

    /*----- Csomag ------*/
    public function getCrmCsomag($crm_category_id='') {
        $vissza = array();
        if (count($this->db_crm)) {
            $sql = "SELECT pricebookid AS productid, pricebook_no AS product_no, bookname AS productname FROM vtiger_pricebook ";
            $query = $this->db_crm->query($sql);
            $vissza = $query->rows;
        }
        return $vissza;
    }



}
?>