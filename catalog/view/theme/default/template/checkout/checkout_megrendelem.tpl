<?php echo $header; ?>
    <!--<div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>-->
<div id="content">

    <h1><?php echo $heading_title; ?></h1>
    <div class="checkout">
    <div id="checkout" style="<?php echo $this->config->get('megjelenit_csak_vendegkent_vasarol') ? 'display: none' : ''?>">
        <div class="checkout-heading"><span class="checkout-number"><?php echo $text_checkout_option_number; ?></span><span class="checkout-label"><?php echo $text_checkout_option; ?></span></div>
        <div class="checkout-content"></div>
    </div>

    <input type="hidden" name="shipping_required" value="<?php echo $shipping_required;?>" >
    <input type="hidden" name="fizetos" value="<?php echo $fizetos;?>" >



<?php if (!$logged) { ?>
    <div id="payment-address">
        <div class="checkout-heading"><span class="checkout-number" ><?php echo $text_checkout_account_number; ?></span><span class="checkout-label" ><?php echo $text_checkout_account; ?></span></div>
        <div class="checkout-content"></div>
    </div>
<?php } else { ?>
    <div id="payment-address">
        <div class="checkout-heading"><span class="checkout-number" ><?php echo $text_checkout_payment_address_number; ?></span><span class="checkout-label" ><?php echo $text_checkout_payment_address; ?></span></div>
        <div class="checkout-content"></div>
    </div>
<?php } ?>



<?php if ($shipping_required) { ?>
    <div id="shipping-address">
        <div class="checkout-heading"><span class="checkout-number" ><?php echo $text_checkout_shipping_address_number; ?></span><span class="checkout-label" ><?php echo $text_checkout_shipping_address; ?></span></div>
        <div class="checkout-content"></div>
    </div>

    <div id="shipping-method">
        <div class="checkout-heading"><span class="checkout-number" ><?php echo $text_checkout_shipping_method_number; ?></span><span class="checkout-label" ><?php echo $text_checkout_shipping_method; ?></span></div>
        <div class="checkout-content"></div>
    </div>

<?php } ?>



<?php if ($fizetos || (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) ) {?>

    <?php if ($this->config->get('megjelenit_penztar_fizetesi_mod') == 1) {?>
        <div id="payment-method">
            <div class="checkout-heading"><span class="checkout-number" ><?php echo $text_checkout_payment_method_number; ?></span><span class="checkout-label" ><?php echo $text_checkout_payment_method; ?></span></div>
            <div class="checkout-content"></div>
        </div>
    <? }?>
    <div id="confirm">
        <div class="checkout-heading"><span class="checkout-number" ><?php echo $text_checkout_confirm_number; ?></span><span class="checkout-label" ><?php echo $text_checkout_confirm; ?></span></div>
        <div class="checkout-content"></div>
    </div>
    </div>
    <?php echo $content_bottom; ?></div>
<?php } ?>

    <script type="text/javascript"><!--
    <?php if (!$shipping_required) { ?>
    $("#payment-method .checkout-number").html("3");
    $("#confirm .checkout-number").html("4");
    <?php } ?>

    $("#header_hatternek").css("display","none");
    $('#checkout .checkout-content input[name=\'account\']').on('change', function() {
        if ($(this).attr('value') == 'register') {
            $('#payment-address .checkout-heading span.checkout-label').html('<?php echo $text_checkout_account; ?>');
        } else {
            $('#payment-address .checkout-heading span.checkout-label').html('<?php echo $text_checkout_payment_address; ?>');
        }
    });

    $('.checkout-heading a').on('click', function() {
        $('.checkout-content').slideUp('slow');

        $('.checkout-number').removeClass('checkout-number-active');
        $(this).parent().parent().find('.checkout-number').addClass('checkout-number-active');

        $(this).parent().parent().find('.checkout-content').slideDown('slow');
    });
    <?php if (!$logged) { ?>
    $(document).ready(function() {
        $.ajax({
            url: 'index.php?route=checkout/login_megrendelem',
            dataType: 'html',
            success: function(html) {

                $('#checkout .checkout-content').html(html);

                $('.checkout-number').removeClass('checkout-number-active');
                $('#checkout .checkout-number').addClass('checkout-number-active');

                <?php if ($this->config->get('megjelenit_csak_vendegkent_vasarol') ) { ?>
                $('#register').prop('checked',false);
                $('#guest').prop('checked',true);
                $('#button-account').trigger("click");
                $('#checkout').remove();
                <?php  } else { ?>
                $('#checkout .checkout-content').slideDown('slow');
                <?php } ?>



            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    <?php } else { ?>
    $(document).ready(function() {
        $.ajax({
            url: 'index.php?route=checkout/payment_address',
            dataType: 'html',
            success: function(html) {
                $('#payment-address .checkout-content').html(html);

                $('.checkout-number').removeClass('checkout-number-active');
                $('#payment-address .checkout-number').addClass('checkout-number-active');

                $('#payment-address .checkout-content').slideDown('slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {


                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    <?php } ?>

    // Checkout


    $('#button-account').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/' + $('input[name=\'account\']:checked').attr('value'),
            dataType: 'html',
            beforeSend: function() {
                $('#button-account').attr('disabled', true);
                $('#button-account').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-account').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(html) {
                $('.warning').remove();

                $('#payment-address .checkout-content').html(html);

                $('#checkout .checkout-content').slideUp('slow');

                $('.checkout-number').removeClass('checkout-number-active');
                $('#payment-address .checkout-number').addClass('checkout-number-active');

                $('#payment-address .checkout-content').slideDown('slow');

                $('.checkout-heading a').remove();

                $('#checkout .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
            },
            error: function(xhr, ajaxOptions, thrownError) {


                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    // Login
    $('#button-login').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/login_megrendelem/validate',
            type: 'post',
            data: $('#checkout #login :input'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-login').attr('disabled', true);
                $('#button-login').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-login').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                $('.warning').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['error']) {
                    $('#checkout .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');

                    $('.warning').fadeIn('slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {


                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    // Register
    $('#button-register').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/register_megrendelem/validate',
            type: 'post',
            data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-register').attr('disabled', true);
                $('#button-register').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-register').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {

                $('.warning, .error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['error']) {

                    if (json['error']['warning']) {
                        if (json['error']['warning']['agree']) {
                            $('#payment-address .checkout-content .buttons').prepend('<div class="warning" style="display: none;">' + json['error']['warning']['agree'] + '</div>');
                        } else {
                            $('#payment-address .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
                        }

                        $('.warning').fadeIn('slow');
                    }


                    if (json['error']['firstname']) {
                        $('#payment-address input[name=\'firstname\'] + br').after('<span class="error">' + json['error']['firstname'] + '</span>');
                    }

                    if (json['error']['lastname']) {
                        $('#payment-address input[name=\'lastname\'] + br').after('<span class="error">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['email']) {
                        $('#payment-address input[name=\'email\'] + br').after('<span class="error">' + json['error']['email'] + '</span>');
                    }

                    if (json['error']['email_again']) {
                        $('#payment-address input[name=\'email_again\'] + br').after('<span class="error">' + json['error']['email_again'] + '</span>');
                    }

                    if (json['error']['telephone']) {
                        $('#payment-address input[name=\'telephone\'] + br').after('<span class="error">' + json['error']['telephone'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#payment-address input[name=\'address_1\'] + br').after('<span class="error">' + json['error']['address_1'] + '</span>');
                    }

                    if (json['error']['city']) {
                        $('#payment-address input[name=\'city\'] + br').after('<span class="error">' + json['error']['city'] + '</span>');
                    }

                    if (json['error']['nem']) {
                        $('#payment-address select[name=\'nem\'] + br').after('<span class="error">' + json['error']['nem'] + '</span>');
                    }

                    if (json['error']['eletkor']) {
                        $('#payment-address select[name=\'eletkor\'] + br').after('<span class="error">' + json['error']['eletkor'] + '</span>');
                    }

                    if (json['error']['postcode']) {
                        $('#payment-address input[name=\'postcode\'] + br').after('<span class="error">' + json['error']['postcode'] + '</span>');
                    }

                    if (json['error']['country']) {
                        $('#payment-address select[name=\'country_id\'] + br').after('<span class="error">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#payment-address select[name=\'zone_id\'] + br').after('<span class="error">' + json['error']['zone'] + '</span>');
                    }

                    if (json['error']['password']) {
                        $('#payment-address input[name=\'password\'] + br').after('<span class="error">' + json['error']['password'] + '</span>');
                    }

                    if (json['error']['confirm']) {
                        $('#payment-address input[name=\'confirm\'] + br').after('<span class="error">' + json['error']['confirm'] + '</span>');
                    }
                } else if (!json['redirect']) {
                    <?php if ($shipping_required) { ?>
                    var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').attr('value');

                    if (shipping_address) {
                        $.ajax({
                            url: 'index.php?route=checkout/shipping_method_megrendelem',
                            dataType: 'html',
                            success: function(html) {

                                $('#shipping-method .checkout-content').html(html);

                                $('#payment-address .checkout-content').slideUp('slow');

                                $('.checkout-number').removeClass('checkout-number-active');
                                $('#shipping-method .checkout-number').addClass('checkout-number-active');

                                $('#shipping-method .checkout-content').slideDown('slow');

                                $('#checkout .checkout-heading a').remove();
                                $('#payment-address .checkout-heading a').remove();
                                $('#shipping-address .checkout-heading a').remove();
                                $('#shipping-method .checkout-heading a').remove();
                                $('#payment-method .checkout-heading a').remove();

                                $('#shipping-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                                $('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');

                                $.ajax({
                                    url: 'index.php?route=checkout/shipping_address_megrendelem',
                                    dataType: 'html',
                                    success: function(html) {
                                        $('#shipping-address .checkout-content').html(html);
                                    },
                                    error: function(xhr, ajaxOptions, thrownError) {


                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            },
                            error: function(xhr, ajaxOptions, thrownError) {


                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/shipping_address_megrendelem',
                            dataType: 'html',
                            success: function(html) {

                                $('#shipping-address .checkout-content').html(html);

                                $('#payment-address .checkout-content').slideUp('slow');

                                $('.checkout-number').removeClass('checkout-number-active');
                                $('#shipping-address .checkout-number').addClass('checkout-number-active');

                                $('#shipping-address .checkout-content').slideDown('slow');

                                $('#checkout .checkout-heading a').remove();
                                $('#payment-address .checkout-heading a').remove();
                                $('#shipping-address .checkout-heading a').remove();
                                $('#shipping-method .checkout-heading a').remove();
                                $('#payment-method .checkout-heading a').remove();

                                $('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {


                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                    <?php } else { ?>
                    $.ajax({
                        url: 'index.php?route=checkout/payment_method_megrendelem',
                        dataType: 'html',
                        success: function(html) {

                            $('#payment-method .checkout-content').html(html);

                            $('#payment-address .checkout-content').slideUp('slow');

                            $('.checkout-number').removeClass('checkout-number-active');
                            $('#payment-method .checkout-number').addClass('checkout-number-active');

                            $('#payment-method .checkout-content').slideDown('slow');

                            $('#checkout .checkout-heading a').remove();
                            $('#payment-address .checkout-heading a').remove();
                            $('#payment-method .checkout-heading a').remove();

                            $('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {


                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                    <?php } ?>

                    $.ajax({
                        url: 'index.php?route=checkout/payment_address',
                        dataType: 'html',
                        success: function(html) {
                            $('#payment-address .checkout-content').html(html);

                            $('#payment-address .checkout-heading span.checkout-label').html('<?php echo $text_checkout_payment_address; ?>');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });

                    <?php if ($ingyenes) { ?>
                    $.ajax({
                        url: 'index.php?route=checkout/ingyen_method',
                        data: $('input[name="shipping_required"], input[name="fizetos"]'),
                        dataType: 'html',
                        success: function(html) {
                            $('#nyomtatas .checkout-content').html(html);
                            $('#nyomtatas .checkout-content').slideDown("slow");
                            $('#payment-address .checkout-content').slideUp("slow");
                            $('#shipping-method .checkout-content').slideUp("slow");
                            $('#payment-method .checkout-content').slideUp("slow");

                            setTimeout(function() {
                                $('#payment-address .checkout-content').slideUp("slow");
                                $('#shipping-method .checkout-content').slideUp("slow");
                                $('#payment-method .checkout-content').slideUp("slow");
                            }, 1000);

                        },


                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                    <?php } ?>

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    // Payment Address
    $('#button-payment-address').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/payment_address_megrendelem/validate',
            type: 'post',
            data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-payment-address').attr('disabled', true);
                $('#button-payment-address').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-payment-address').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                //
                $('.error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['error']) {
                    if (json['error']['firstname']) {
                        $('#payment-address input[name=\'firstname\']').after('<span class="error">' + json['error']['firstname'] + '</span>');
                    }

                    if (json['error']['lastname']) {
                        $('#payment-address input[name=\'lastname\']').after('<span class="error">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['telephone']) {
                        $('#payment-address input[name=\'telephone\']').after('<span class="error">' + json['error']['telephone'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#payment-address input[name=\'address_1\']').after('<span class="error">' + json['error']['address_1'] + '</span>');
                    }

                    if (json['error']['city']) {
                        $('#payment-address input[name=\'city\']').after('<span class="error">' + json['error']['city'] + '</span>');
                    }

                    if (json['error']['postcode']) {
                        $('#payment-address input[name=\'postcode\']').after('<span class="error">' + json['error']['postcode'] + '</span>');
                    }

                    if (json['error']['country']) {
                        $('#payment-address select[name=\'country_id\']').after('<span class="error">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#payment-address select[name=\'zone_id\']').after('<span class="error">' + json['error']['zone'] + '</span>');
                    }
                } else if (!json['redirect']) {
                    <?php if ($shipping_required) { ?>
                    $.ajax({
                        url: 'index.php?route=checkout/shipping_address_megrendelem',
                        dataType: 'html',
                        success: function(html) {
                            $('#shipping-address .checkout-content').html(html);

                            $('#payment-address .checkout-content').slideUp('slow');

                            $('.checkout-number').removeClass('checkout-number-active');
                            $('#shipping-address .checkout-number').addClass('checkout-number-active');

                            $('#shipping-address .checkout-content').slideDown('slow');

                            $('#payment-address .checkout-heading a').remove();
                            $('#shipping-address .checkout-heading a').remove();
                            $('#shipping-method .checkout-heading a').remove();
                            $('#payment-method .checkout-heading a').remove();

                            $('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                    <?php } else { ?>
                    $.ajax({
                        url: 'index.php?route=payment_method_megrendelem',
                        dataType: 'html',
                        success: function(html) {
                            $('#payment-method .checkout-content').html(html);

                            $('#payment-address .checkout-content').slideUp('slow');

                            <?php if (!$ingyenes) { ?>

                            $('.checkout-number').removeClass('checkout-number-active');
                            $('#payment-method .checkout-number').addClass('checkout-number-active');

                            $('#payment-method .checkout-content').slideDown('slow');
                            <?php } ?>

                            $('#payment-address .checkout-heading a').remove();
                            $('#payment-method .checkout-heading a').remove();

                            $('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });

                    <?php if ($ingyenes) { ?>
                    $.ajax({
                        url: 'index.php?route=checkout/ingyen_method',
                        data: $('input[name="shipping_required"], input[name="fizetos"]'),
                        dataType: 'html',
                        success: function(html) {
                            $('#nyomtatas .checkout-content').html(html);
                            $('#nyomtatas .checkout-content').slideDown("slow");
                            $('#payment-address .checkout-content').slideUp("slow");
                            $('#shipping-method .checkout-content').slideUp("slow");
                            $('#payment-method .checkout-content').slideUp("slow");

                            setTimeout(function() {
                                $('#payment-address .checkout-content').slideUp("slow");
                                $('#shipping-method .checkout-content').slideUp("slow");
                                $('#payment-method .checkout-content').slideUp("slow");
                            }, 1000);

                        },


                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                    <?php } ?>

                    <?php } ?>

                    $.ajax({
                        url: 'index.php?route=checkout/payment_address',
                        dataType: 'html',
                        success: function(html) {
                            $('#payment-address .checkout-content').html(html);
                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    // Shipping Address
    $('#button-shipping-address').on('click', function() {

        $.ajax({
            url: 'index.php?route=checkout/shipping_address_megrendelem/validate',
            type: 'post',
            data: $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'hidden\'], #shipping-address input[type=\'password\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address select'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-shipping-address').attr('disabled', true);
                $('#button-shipping-address').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-shipping-address').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                $('.error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['error']) {
                    if (json['error']['firstname']) {
                        $('#shipping-address input[name=\'firstname\']').after('<span class="error">' + json['error']['firstname'] + '</span>');
                    }

                    if (json['error']['lastname']) {
                        $('#shipping-address input[name=\'lastname\']').after('<span class="error">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['email']) {
                        $('#shipping-address input[name=\'email\']').after('<span class="error">' + json['error']['email'] + '</span>');
                    }

                    if (json['error']['telephone']) {
                        $('#shipping-address input[name=\'telephone\']').after('<span class="error">' + json['error']['telephone'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#shipping-address input[name=\'address_1\']').after('<span class="error">' + json['error']['address_1'] + '</span>');
                    }

                    if (json['error']['city']) {
                        $('#shipping-address input[name=\'city\']').after('<span class="error">' + json['error']['city'] + '</span>');
                    }

                    if (json['error']['postcode']) {
                        $('#shipping-address input[name=\'postcode\']').after('<span class="error">' + json['error']['postcode'] + '</span>');
                    }

                    if (json['error']['country']) {
                        $('#shipping-address select[name=\'country_id\']').after('<span class="error">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#shipping-address select[name=\'zone_id\']').after('<span class="error">' + json['error']['zone'] + '</span>');
                    }
                } else if (!json['redirect']){
                    $.ajax({
                        url: 'index.php?route=checkout/shipping_method_megrendelem',
                        dataType: 'html',
                        success: function(html) {
                            $('#shipping-method .checkout-content').html(html);

                            $('#shipping-address .checkout-content').slideUp('slow');
                            <?php if (!$ingyenes) { ?>
                            $('.checkout-number').removeClass('checkout-number-active');
                            $('#shipping-method .checkout-number').addClass('checkout-number-active');

                            $('#shipping-method .checkout-content').slideDown('slow');
                            <?php } ?>

                            $('#shipping-address .checkout-heading a').remove();
                            $('#shipping-method .checkout-heading a').remove();
                            $('#payment-method .checkout-heading a').remove();

                            $('#shipping-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');

                            $.ajax({
                                url: 'index.php?route=checkout/shipping_address_megrendelem',
                                dataType: 'html',
                                success: function(html) {
                                    $('#shipping-address .checkout-content').html(html);
                                },
                                error: function(xhr, ajaxOptions, thrownError) {

                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });

                    <?php if ($ingyenes) { ?>

                    $.ajax({
                        url: 'index.php?route=checkout/ingyen_method',
                        dataType: 'html',
                        data: $('input[name="shipping_required"], input[name="fizetos"]'),
                        success: function(html) {
                            $('#nyomtatas .checkout-content').html(html);
                            $('#nyomtatas .checkout-content').slideDown("slow");
                            $('#payment-address .checkout-content').slideUp("slow");
                            $('#shipping-method .checkout-content').slideUp("slow");
                            $('#payment-method .checkout-content').slideUp("slow");

                            setTimeout(function() {
                                $('#payment-address .checkout-content').slideUp("slow");
                                $('#shipping-method .checkout-content').slideUp("slow");
                                $('#payment-method .checkout-content').slideUp("slow");
                            }, 1000);

                        },

                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                    <?php } ?>
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    // Guest

    $('#button-nyomtat-method').on('click', function() {

        $('#button-nyomtat-method').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');

        $("#slidecontent").load("index.php #slidecontent");
        <?php if (!$fizetos) {?>
        location = 'index.php?route=checkout/ingyen_method/tovabb&vanmeg=0';
        //location = 'index.php?route=checkout/cart';
        <?php } else { ?>
        location = 'index.php?route=checkout/ingyen_method/tovabb&vanmeg=1';

        //location = 'index.php?route=checkout/checkout';

        <?php } ?>

    });


    $('#button-guest').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/guest_megrendelem/validate',
            type: 'post',
            data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'hidden\'], #payment-address select'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-guest').attr('disabled', true);
                $('#button-guest').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-guest').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                $('.error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['error']) {
                    if (json['error']['firstname']) {
                        $('#payment-address input[name=\'firstname\'] + br').after('<span class="error">' + json['error']['firstname'] + '</span>');
                    }

                    if (json['error']['lastname']) {
                        $('#payment-address input[name=\'lastname\'] + br').after('<span class="error">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['email']) {
                        $('#payment-address input[name=\'email\'] + br').after('<span class="error">' + json['error']['email'] + '</span>');
                    }

                    if (json['error']['telephone']) {
                        $('#payment-address input[name=\'telephone\'] + br').after('<span class="error">' + json['error']['telephone'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#payment-address input[name=\'address_1\'] + br').after('<span class="error">' + json['error']['address_1'] + '</span>');
                    }

                    if (json['error']['city']) {
                        $('#payment-address input[name=\'city\'] + br').after('<span class="error">' + json['error']['city'] + '</span>');
                    }

                    if (json['error']['postcode']) {
                        $('#payment-address input[name=\'postcode\'] + br').after('<span class="error">' + json['error']['postcode'] + '</span>');
                    }

                    if (json['error']['country']) {
                        $('#payment-address select[name=\'country_id\'] + br').after('<span class="error">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['nem']) {
                        $('#payment-address select[name=\'nem\'] + br').after('<span class="error">' + json['error']['nem'] + '</span>');
                    }
                    if (json['error']['eletkor']) {
                        $('#payment-address select[name=\'eletkor\'] + br').after('<span class="error">' + json['error']['eletkor'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#payment-address select[name=\'zone_id\'] + br').after('<span class="error">' + json['error']['zone'] + '</span>');
                    }
                } else if (!json['redirect']) {
                    <?php if ($shipping_required) { ?>
                    var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').attr('value');

                    if (shipping_address) {
                        $.ajax({
                            url: 'index.php?route=checkout/shipping_method_megrendelem',
                            dataType: 'html',
                            success: function(html) {
                                $('#shipping-method .checkout-content').html(html);

                                $('#payment-address .checkout-content').slideUp('slow');

                                <?php if (!$ingyenes) { ?>
                                $('.checkout-number').removeClass('checkout-number-active');
                                $('#shipping-method .checkout-number').addClass('checkout-number-active');

                                $('#shipping-method .checkout-content').slideDown('slow');
                                <?php } ?>
                                $('#payment-address .checkout-heading a').remove();
                                $('#shipping-address .checkout-heading a').remove();
                                $('#shipping-method .checkout-heading a').remove();
                                $('#payment-method .checkout-heading a').remove();

                                $('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                                $('#shipping-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');

                                $.ajax({
                                    url: 'index.php?route=checkout/guest_shipping',
                                    dataType: 'html',
                                    success: function(html) {
                                        $('#shipping-address .checkout-content').html(html);
                                    },
                                    error: function(xhr, ajaxOptions, thrownError) {

                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });



                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/guest_shipping',
                            dataType: 'html',
                            success: function(html) {
                                $('#shipping-address .checkout-content').html(html);

                                $('#payment-address .checkout-content').slideUp('slow');

                                $('.checkout-number').removeClass('checkout-number-active');
                                $('#shipping-address .checkout-number').addClass('checkout-number-active');

                                $('#shipping-address .checkout-content').slideDown('slow');

                                $('#payment-address .checkout-heading a').remove();
                                $('#shipping-address .checkout-heading a').remove();
                                $('#shipping-method .checkout-heading a').remove();
                                $('#payment-method .checkout-heading a').remove();

                                $('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                    <?php } else { ?>
                    $.ajax({
                        url: 'index.php?route=payment_method_megrendelem',
                        dataType: 'html',
                        success: function(html) {
                            $('#payment-method .checkout-content').html(html);

                            $('#payment-address .checkout-content').slideUp('slow');

                            <?php if (!$ingyenes) { ?>
                            $('.checkout-number').removeClass('checkout-number-active');
                            $('#payment-method .checkout-number').addClass('checkout-number-active');

                            $('#payment-method .checkout-content').slideDown('slow');
                            <?php } ?>

                            $('#payment-address .checkout-heading a').remove();
                            $('#payment-method .checkout-heading a').remove();

                            $('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                    <?php } ?>

                    <?php if ($ingyenes) { ?>
                    $.ajax({
                        url: 'index.php?route=checkout/ingyen_method',
                        dataType: 'html',
                        data: $('input[name="shipping_required"], input[name="fizetos"]'),
                        success: function(html) {
                            $('#nyomtatas .checkout-content').html(html);
                            $('#nyomtatas .checkout-content').slideDown("slow");


                            $('#payment-address .checkout-content').slideUp('slow');
                            $('#shipping-method .checkout-content').slideUp('slow');
                            $('#payment-method .checkout-content').slideUp('slow');

                            setTimeout(function() {
                                $('#shipping-method .checkout-content').slideUp('slow');
                                $('#payment-method .checkout-content').slideUp('slow');
                            }, 1000);

                        },


                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                    <?php } ?>

                }
            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });


    // Guest Shipping
    $('#button-guest-shipping').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/guest_shipping/validate',
            type: 'post',
            data: $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'hidden\'], #shipping-address select'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-guest-shipping').attr('disabled', true);
                $('#button-guest-shipping').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-guest-shipping').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                $('.error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['error']) {
                    if (json['error']['firstname']) {
                        $('#shipping-address input[name=\'firstname\']').after('<span class="error">' + json['error']['firstname'] + '</span>');
                    }

                    if (json['error']['lastname']) {
                        $('#shipping-address input[name=\'lastname\']').after('<span class="error">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#shipping-address input[name=\'address_1\']').after('<span class="error">' + json['error']['address_1'] + '</span>');
                    }

                    if (json['error']['city']) {
                        $('#shipping-address input[name=\'city\']').after('<span class="error">' + json['error']['city'] + '</span>');
                    }

                    if (json['error']['postcode']) {
                        $('#shipping-address input[name=\'postcode\']').after('<span class="error">' + json['error']['postcode'] + '</span>');
                    }

                    if (json['error']['country']) {
                        $('#shipping-address select[name=\'country_id\']').after('<span class="error">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#shipping-address select[name=\'zone_id\']').after('<span class="error">' + json['error']['zone'] + '</span>');
                    }
                } else if (!json['redirect']) {
                    $.ajax({
                        url: 'index.php?route=checkout/shipping_method_megrendelem',
                        dataType: 'html',
                        success: function(html) {
                            $('#shipping-method .checkout-content').html(html);

                            $('#shipping-address .checkout-content').slideUp('slow');

                            <?php if (!$ingyenes) { ?>
                            $('.checkout-number').removeClass('checkout-number-active');
                            $('#shipping-method .checkout-number').addClass('checkout-number-active');

                            $('#shipping-method .checkout-content').slideDown('slow');
                            <?php } ?>

                            $('#shipping-address .checkout-heading a').remove();
                            $('#shipping-method .checkout-heading a').remove();
                            $('#payment-method .checkout-heading a').remove();

                            $('#shipping-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });

                    <?php if ($ingyenes) { ?>
                    $.ajax({
                        url: 'index.php?route=checkout/ingyen_method',
                        dataType: 'html',
                        data: $('input[name="shipping_required"], input[name="fizetos"]'),
                        success: function(html) {
                            $('#nyomtatas .checkout-content').html(html);
                            $('#nyomtatas .checkout-content').slideDown("slow");
                            $('#payment-address .checkout-content').slideUp("slow");
                            $('#shipping-method .checkout-content').slideUp("slow");
                            $('#payment-method .checkout-content').slideUp("slow");

                            setTimeout(function() {
                                $('#payment-address .checkout-content').slideUp("slow");
                                $('#shipping-method .checkout-content').slideUp("slow");
                                $('#payment-method .checkout-content').slideUp("slow");
                            }, 1000);

                        },


                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                    <?php } ?>

                }
            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('#button-shipping-method').on('click', function() {

        var mehet = true;
        if ($("input[name='shipping_method']:checked").val() == "pickup.pickup") {
            if ($("input[name='uzlet']").length > 0) {
                if ($("input[name='uzlet']:checked").length == 0) {
                    mehet = false;
                }
            }
        }



        if (mehet) {
            $.ajax({
                url: 'index.php?route=checkout/shipping_method_megrendelem/validate',
                type: 'post',
                data: $('#shipping-method input[type=\'radio\']:checked, #shipping-method textarea'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-shipping-method').attr('disabled', true);
                    $('#button-shipping-method').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
                },
                complete: function() {
                    $('#button-shipping-method').attr('disabled', false);
                    $('.wait').remove();
                },
                success: function(json) {
                    $('.warning').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    }

                    if (json['error']) {
                        if (json['error']['warning']) {
                            $('#shipping-method .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');

                            $('.warning').fadeIn('slow');
                        }
                    } else  if (!json['redirect']) {

                        <?php if ($this->config->get('megjelenit_penztar_fizetesi_mod') == 1) {?>
                        $.ajax({
                            url: 'index.php?route=payment_method_megrendelem',
                            dataType: 'html',
                            success: function(html) {
                                $('#payment-method .checkout-content').html(html);

                                $('#shipping-method .checkout-content').slideUp('slow');

                                $('.checkout-number').removeClass('checkout-number-active');
                                $('#payment-method .checkout-number').addClass('checkout-number-active');

                                $('#payment-method .checkout-content').slideDown('slow');

                                $('#shipping-method .checkout-heading a').remove();
                                $('#payment-method .checkout-heading a').remove();

                                $('#shipping-method .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                        <?php }?>

                        <?php if($this->config->get('megjelenit_penztar_fizetesi_mod') == 0){ ?>
                        $.ajax({
                            url: 'index.php?route=checkout/confirm',
                            dataType: 'html',
                            success: function(html) {
                                $('#confirm .checkout-content').html(html);

                                $('#shipping-method .checkout-content').slideUp('slow');

                                $('.checkout-number').removeClass('checkout-number-active');
                                $('#confirm .checkout-number').addClass('checkout-number-active');

                                $('#confirm .checkout-content').slideDown('slow');

                                $('#shipping-method .checkout-heading a').remove();

                                $('#shipping-method .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                        <?php }?>
                    }


                },
                error: function(xhr, ajaxOptions, thrownError) {

                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        } else {
            $('.warning').remove();
            $('#shipping-method .warehouse_shipping').prepend('<div class="warning" style="display: none;">' + "Kérem válasszon átvételi pontot" + '</div>');
            $('.warning').fadeIn('slow');
            setTimeout(" $('.warning, .success').slideUp(500)",4000);

        }
    });

    $('#button-payment-method').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/payment_method_megrendelem/validate',
            type: 'post',
            data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-payment-method').attr('disabled', true);
                $('#button-payment-method').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-payment-method').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                $('.warning').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['error']) {
                    if (json['error']['warning']) {
                        if (json['error']['payment']) {
                            $('#payment-method .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
                        } else {
                            $('#payment-method .checkout-content .buttons').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
                        }

                        $('.warning').fadeIn('slow');
                    }
                } else if (!json['redirect']) {
                    $.ajax({
                        url: 'index.php?route=checkout/confirm_megrendelem',
                        dataType: 'html',
                        success: function(html) {
                            $('#confirm .checkout-content').html(html);

                            $('#payment-method .checkout-content').slideUp('slow');

                            $('.checkout-number').removeClass('checkout-number-active');
                            $('#confirm .checkout-number').addClass('checkout-number-active');

                            $('#confirm .checkout-content').slideDown('slow');

                            $('#payment-method .checkout-heading a').remove();

                            $('#payment-method .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });


    //--></script>
<?php //echo $footer; ?>