<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="pa-button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="pa-button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
	<div id="total-recovered"></div>
  
	<div id="tabs" class="vtabs">
		<a href="#tab-setting"><?php echo $tab_setting; ?></a>
		<a href="#tab-popup"><?php echo $tab_popup; ?></a>
		<a href="#tab-customer-email"><?php echo $tab_customer_email; ?></a>
		<a href="#tab-admin-email"><?php echo $tab_admin_email; ?></a>
		<a href="#tab-price-alert"><?php echo $tab_alert; ?></a>
		<a href="#tab-history"><?php echo $tab_history; ?></a>
		<!--<a href="#tab-help"><?php echo $tab_help; ?></a>-->
	</div>
  
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
	
	<div id="tab-setting" class="vtabs-content">
		 
		  <table class="form">
			<div class="attention" id="status-attention"><?php echo $text_status_attention; ?></div>
			<tr>
				<td class="left"><?php echo $entry_status; ?></td>
				<td><select name="price_alert_status">
					<?php if ($price_alert_status) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				</select>
				</td>
			</tr>		  
			<tr>
				<td class="left"><span class="required">*</span><?php echo $entry_secret_code; ?></td>
				<td class="left"><input type="text" name="price_alert_secret_code" value="<?php echo $price_alert_secret_code; ?>">
				<?php if ($error_secret_code) { ?>
				<span class="error"><?php echo $error_secret_code; ?></span>
				<?php } ?>
				</td>
			</tr>
			<tr>
				<td class="left"><?php echo $entry_admin_notification; ?></td>
				<td><select name="price_alert_admin_notification">
					<?php if ($price_alert_admin_notification) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				</select>
				</td>
			</tr>						
			<tr style="display:none;">
				<td class="left"><?php echo $entry_use_html_email; ?></td>
				<td><select name="price_alert_use_html_email">
					<?php if ($price_alert_use_html_email) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				</select>
				
				<?php if ($error_use_html_email){  ?>
				<span class="error"><?php echo $error_use_html_email; ?></span>
				<?php } ?>
				</td>
			</tr>
		  </table>
	</div>

	<div id="tab-popup" class="vtabs-content">		
		<div id="popup-languages" class="htabs">
			<?php foreach ($languages as $language) { ?>
			<a href="#popup-language-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
			<?php } ?>
		</div>
		
		<?php foreach ($languages as $language) { ?>
		<div id="popup-language-<?php echo $language['language_id']; ?>">
			<table class="form">
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_set_alert; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][set_alert]" size="100" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['set_alert'] : ''; ?>" />
					<?php if (isset($error_set_alert[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_set_alert[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_short_explanation; ?></td>
					<td><textarea name="price_alert_popup[<?php echo $language['language_id']; ?>][short_explanation]" id="price-alert-popup-short-explanation-<?php echo $language['language_id']; ?>" cols="120" rows="8"><?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['short_explanation'] : ''; ?></textarea>
					<?php if (isset($error_short_explanation[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_short_explanation[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>	
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_name; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['name'] : ''; ?>" />
					<?php if (isset($error_name[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_email; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][email]" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['email'] : ''; ?>" />
					<?php if (isset($error_email[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_email[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>	
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_desired_price; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][desired_price]" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['desired_price'] : ''; ?>" />
					<?php if (isset($error_desired_price[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_desired_price[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_button_set_alert; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][button_set_alert]" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['button_set_alert'] : ''; ?>" />
					<?php if (isset($error_button_set_alert[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_button_set_alert[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>				
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_error_name; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][error_name]" size="100" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['error_name'] : ''; ?>" />
					<?php if (isset($error_error_name[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_error_name[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_error_email; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][error_email]" size="100" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['error_email'] : ''; ?>" />
					<?php if (isset($error_error_email[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_error_email[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>	
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_error_desired_price; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][error_desired_price]" size="100" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['error_desired_price'] : ''; ?>" />
					<?php if (isset($error_error_desired_price[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_error_desired_price[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_error_price_already; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][error_price_already]" size="100" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['error_price_already'] : ''; ?>" />
					<?php if (isset($error_error_price_already[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_error_price_already[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>				
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_popup_success; ?></td>
					<td><input name="price_alert_popup[<?php echo $language['language_id']; ?>][success]" size="100" value="<?php echo isset($price_alert_popup[$language['language_id']]) ? $price_alert_popup[$language['language_id']]['success'] : ''; ?>" />
					<?php if (isset($error_success[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_success[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>				
			</table>
		</div>
		<?php } ?>		
	</div>
	
	<div id="tab-customer-email" class="vtabs-content">		
		<div id="customer-languages" class="htabs">
			<?php foreach ($languages as $language) { ?>
			<a class="customer-language" href="#customer-language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
			<?php } ?>
		</div>
		<?php $first = true; ?>
		<?php foreach ($languages as $language) { ?>
		<div id="cust-language<?php echo $language['language_id']; ?>" <?php if($first) {echo "style='display:block;'"; $first = false;} else {echo "style='display:none;'";} ?>>
			<table class="form">
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_subject; ?></td>
					<td><input name="price_alert_customer_mail[<?php echo $language['language_id']; ?>][subject]" size="100" value="<?php echo isset($price_alert_customer_mail[$language['language_id']]) ? $price_alert_customer_mail[$language['language_id']]['subject'] : ''; ?>" />
					<?php if (isset($error_customer_subject[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_customer_subject[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_message; ?></td>
					<td><textarea name="price_alert_customer_mail[<?php echo $language['language_id']; ?>][message]" id="price-alert-customer-mail-message-<?php echo $language['language_id']; ?>" cols="120" rows="8"><?php echo isset($price_alert_customer_mail[$language['language_id']]) ? $price_alert_customer_mail[$language['language_id']]['message'] : ''; ?></textarea>
					<?php if (isset($error_customer_message[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_customer_message[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>			
			</table>
		</div>
		<?php } ?>		
	</div>	
	
	<div id="tab-admin-email" class="vtabs-content">		
		<div id="admin-languages" class="htabs">
			<?php foreach ($languages as $language) { ?>
			<a class="admin-language" href="#admin-language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
			<?php } ?>
		</div>
		<?php $first = true; ?>
		<?php foreach ($languages as $language) { ?>
		<div id="ad-language<?php echo $language['language_id']; ?>" <?php if($first) {echo "style='display:block;'"; $first = false;} else {echo "style='display:none;'";} ?>>
			<table class="form">
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_subject; ?></td>
					<td><input name="price_alert_admin_mail[<?php echo $language['language_id']; ?>][subject]" size="100" value="<?php echo isset($price_alert_admin_mail[$language['language_id']]) ? $price_alert_admin_mail[$language['language_id']]['subject'] : ''; ?>" />
					<?php if (isset($error_admin_subject[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_admin_subject[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td class="left"><span class="required">* </span><?php echo $entry_admin_message; ?></td>
					<td><textarea name="price_alert_admin_mail[<?php echo $language['language_id']; ?>][message]" id="price-alert-admin-mail-message-<?php echo $language['language_id']; ?>" cols="120" rows="8"><?php echo isset($price_alert_admin_mail[$language['language_id']]) ? $price_alert_admin_mail[$language['language_id']]['message'] : ''; ?></textarea>
					<?php if (isset($error_admin_message[$language['language_id']])) { ?>
					<span class="error"><?php echo $error_admin_message[$language['language_id']]; ?></span>
					<?php } ?>
					</td>
				</tr>			
			</table>
		</div>
		<?php } ?>		
	</div>	
	
	<div id="tab-price-alert" class="vtabs-content">
		<div class="pa-information"><?php echo $text_check_send; ?><a class="pa-button" style="position:absolute; top:13px; right:10px;" onclick="sendNow();"><?php echo $button_check_send; ?></a></div>
		
		<table class="form">
			<tr>
				<td class="center"><?php echo $entry_customer; ?><br /><input type="text" name="pa_list_filter_customer" /></td>
				<td class="center"><?php echo $entry_email; ?><br /><input type="text" name="pa_list_filter_email" /></td>
				<td class="center"><?php echo $entry_product; ?><br /><input type="text" name="pa_list_filter_product" /></td>
				<td class="center"><?php echo $entry_date_added; ?><br /><input type="text" class="date" name="pa_list_filter_date_added" /></td>
				<td class="center"><a class="pa-button big" onclick="filterActiveAlerts();"><?php echo $button_filter; ?></a></td>
			</tr>
		</table>
		
		<div id="price-alert-list"><div class="pa-loading-spinner"></div></div>
	</div>
	
	<div id="tab-history" class="vtabs-content">	
		<table class="form">
			<tr>
				<td class="center"><?php echo $entry_customer; ?><br /><input type="text" name="pa_history_filter_customer" /></td>
				<td class="center"><?php echo $entry_email; ?><br /><input type="text" name="pa_history_filter_email" /></td>
				<td class="center"><?php echo $entry_date_added; ?><br /><input type="text" class="date" name="pa_history_filter_date_added" /></td>
				<td class="center"><a class="pa-button big" onclick="filterHistoryAlerts();"><?php echo $button_filter; ?></a></td>
			</tr>
		</table>
		
		<div id="price-alert-history"><div class="pa-loading-spinner"></div></div>
	</div>
	
	<!--<div id="tab-help" class="vtabs-content">
		Changelog and HELP guide is available  : <a href="http://oc-extensions.com/Price-Alert" target="blank">HERE</a> (please check tab Help)<br /><br />
		If you need support email us at <strong>support@oc-extensions.com</strong><br /><br /><br />
		
		<u><strong>Become a Premium Member:</strong></u><br /><br />
		<div class="pa-information">Price Alert & other 100+ extensions are included in Premium Membership.</div> 
		Subscribe to Premium Membership and you can download all our products (past, present and future).<br />Find more on <a href="http://www.oc-extensions.com">www.oc-extensions.com</a>
		
	</div>-->
	</form>
  </div>
</div>

<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('price-alert-popup-short-explanation-<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});

CKEDITOR.replace('price-alert-customer-mail-message-<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});

CKEDITOR.replace('price-alert-admin-mail-message-<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
//--></script>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#popup-languages a').tabs();
$('#customer-languages a').tabs();
$('#admin-languages a').tabs();

$('select[name=\'price_alert_status\']').bind('change', function() {
	if ($(this).val() == 1) {
		$('#status-attention').hide();
	} else {
		$('#status-attention').show();
	}
});

$('select[name=\'price_alert_status\']').trigger('change');

$('#price-alert-list').load('index.php?route=module/price_alert/getPriceAlerts&token=<?php echo $token; ?>');
$('#price-alert-history').load('index.php?route=module/price_alert/getPriceAlertHistory&token=<?php echo $token; ?>');

$('#price-alert-list .pagination a').on('click', function() {
	$('#price-alert-list').html('<div class="pa-loading-spinner"></div>');
	$('#price-alert-list').load(this.href);
	
	return false;
});

$('#price-alert-history .pagination a').on('click', function() {
	$('#price-alert-history').html('<div class="pa-loading-spinner"></div>');
	$('#price-alert-history').load(this.href);
	
	return false;
});

function sendNow(){
	
	$('#dialog').remove();
	
	var iframe_url = '<?php echo $front_base_url; ?>index.php?route=cron/price_alert&secret_code=<?php echo $price_alert_secret_code; ?>';
	
	$('#content').prepend('<div id="dialog" style="padding: 10px; background: #FFF;"><iframe id="iframe-send-now" src="" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo addslashes($tab_alert); ?>',
		bgiframe: false,
		<?php  if ($price_alert_use_html_email) { ?>
		width: 740,
		<?php } else { ?>
		width: 640,
		<?php } ?>
		height: 520,
		resizable: false,
		modal: true,
		open: function(event, ui) {
			$('#iframe-send-now').attr('src', iframe_url);
		},
		close: function() {
			$('#iframe-send-now').attr('src', '');
			
			$('#price-alert-list').html('<div class="pa-loading-spinner"></div>');
			$('#price-alert-list').load('index.php?route=module/price_alert/getPriceAlerts&token=<?php echo $token; ?>');
			
			$('#price-alert-history').html('<div class="pa-loading-spinner"></div>');
			$('#price-alert-history').load('index.php?route=module/price_alert/getPriceAlertHistory&token=<?php echo $token; ?>');

		}
	});
}

function showHistoryEmail(history_id) {
	$('#dialog').remove();
	
	var iframe_url = '<?php echo $front_base_url; ?>index.php?route=cron/price_alert/getHistoryEmail&secret_code=<?php echo $price_alert_secret_code; ?>&history_id=' + history_id;
	
	$('#content').prepend('<div id="dialog" style="padding: 10px; background: #FFF;"><iframe id="iframe-history" src="" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: 'Alert #' +  history_id,
		bgiframe: false,
		<?php  if ($price_alert_use_html_email) { ?>
		width: 740,
		<?php } else { ?>
		width: 640,
		<?php } ?>
		height: 520,
		resizable: false,
		modal: true,
		open: function(event, ui) {
			$('#iframe-history').attr('src', iframe_url);
		},
		close: function(event, ui) {
			$('#iframe-history').attr('src', '');
		}
	});
}

    $( ".admin-language" ).click(function() {

        href = $(this).attr('href');
        hrefArray = href.split("-");
        lang = hrefArray[1];
       // alert( "Handler for .click() called."+lang );
        //debugger;
        $('#ad-language1').css('display', 'none');
        $('#ad-language2').css('display', 'none');
        $('#ad-'+lang).css('display', 'block');
    });

    $( ".customer-language" ).click(function() {

        href = $(this).attr('href');
        hrefArray = href.split("-");
        lang = hrefArray[1];
        // alert( "Handler for .click() called."+lang );
        //debugger;
        $('#cust-language1').css('display', 'none');
        $('#cust-language2').css('display', 'none');
        $('#cust-'+lang).css('display', 'block');
    });

//--></script> 

<script type="text/javascript"><!--
function filterActiveAlerts() {
	url = 'index.php?route=module/price_alert/getPriceAlerts&token=<?php echo $token; ?>';
	
	var filter_customer = $('input[name=\'pa_list_filter_customer\']').attr('value');
	
	if (filter_customer) {
		url += '&pa_list_filter_customer=' + encodeURIComponent(filter_customer);
	}	
	
	var filter_email = $('input[name=\'pa_list_filter_email\']').attr('value');
	
	if (filter_email) {
		url += '&pa_list_filter_email=' + encodeURIComponent(filter_email);
	}

	var filter_product = $('input[name=\'pa_list_filter_product\']').attr('value');
	
	if (filter_product) {
		url += '&pa_list_filter_product=' + encodeURIComponent(filter_product);
	}	
	
	var filter_date_added = $('input[name=\'pa_list_filter_date_added\']').attr('value');
	
	if (filter_date_added) {
		url += '&pa_list_filter_date_added=' + encodeURIComponent(filter_date_added);
	}
	
	$('#price-alert-list').html('<div class="pa-loading-spinner"></div>');

	$('#price-alert-list').load(url);
}

function filterHistoryAlerts() {
	url = 'index.php?route=module/price_alert/getPriceAlertHistory&token=<?php echo $token; ?>';
	
	var filter_customer = $('input[name=\'pa_history_filter_customer\']').attr('value');
	
	if (filter_customer) {
		url += '&pa_history_filter_customer=' + encodeURIComponent(filter_customer);
	}	
	
	var filter_email = $('input[name=\'pa_history_filter_email\']').attr('value');
	
	if (filter_email) {
		url += '&pa_history_filter_email=' + encodeURIComponent(filter_email);
	}	
	
	var filter_date_added = $('input[name=\'pa_history_filter_date_added\']').attr('value');
	
	if (filter_date_added) {
		url += '&pa_history_filter_date_added=' + encodeURIComponent(filter_date_added);
	}
	
	$('#price-alert-history').html('<div class="pa-loading-spinner"></div>');
	$('#price-alert-history').load(url);
}
//--></script>  
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});

$('input[name=\'pa_list_filter_product\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'pa_list_filter_product\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<?php echo $footer; ?>