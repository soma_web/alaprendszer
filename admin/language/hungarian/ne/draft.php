<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Hírlevél Vázlat';

// Text
$_['text_view']								= 'Nézet';
$_['text_marketing']						= 'Összes Marketing Feliratkozó';
$_['text_marketing_all']					= 'Összes Marketing';
$_['text_subscriber_all']					= 'Összes Hírlevél Feliratkozó & Marketing Előfizető';
$_['text_all']								= 'Összes Várásló & Marketing';
$_['text_success']							= 'Siker: A kiválasztott elemek törölve';

// Column 
$_['column_actions']						= 'Akciók';
$_['column_subject']						= 'Tárgy';
$_['column_to']								= 'Irányában';
$_['column_date']							= 'Dátum';
$_['column_store']							= 'Áruház';

?>
