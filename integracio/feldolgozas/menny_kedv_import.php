<?php
class Menny_Kedv_Import {               /* SAP_vevok segédtábla feldolgozása a:
                                                product
                                                customer_group
                                                product_discount        táblákba.
                                        */

    private $query;
    private $vevo_id;
    public $azonosito;
    public $csvadatok;
    public $sap_vevocsoport;

    public function __construct($query) {
        $this->query = $query;
        $this->csvadatok=$this->query->query("SELECT * FROM sap_mennyisegi_kedvezmenyek");
    }

    public function Mennyisegi_Kedvezmeny($sorok) {
        $sap_cikkszam=$sorok['cikkszam'];

        $sql="SELECT * FROM ".DB_PREFIX."product WHERE cikkszam = '$sap_cikkszam'";
        $query= $this->query->query($sql);

        if (isset($query->num_rows) && $query->num_rows > 0){
            $azonosito=$query->row["product_id"];
            $this->Termek_Kedvezmeny($sorok,$azonosito);

        } else {
            $m_datum = date('Y-m-d');
            $modosit="INSERT INTO ".DB_PREFIX."product SET
                                                product_id          = '',
                                                cikkszam            = '$sap_cikkszam',
                                                date_added          = '$m_datum',
                                                minimum             = '1',
                                                quantity            = '0',
                                                subtract            = '0',
                                                status              = '0' ";

            mysql_query($modosit) or  Hibalista("Mennyiségi kedvezmény", "product", "felvitel", $sap_cikkszam);

            $sql="SELECT * FROM ".DB_PREFIX."product WHERE cikkszam = '$sap_cikkszam'";
            $query= $this->query->query($sql);
            $azonosito=$query->row['product_id'];
            $this->Termek_Kedvezmeny($sorok,$azonosito);
        }
    }

    public function Vevo_Csoport($sap_vevocsoport){
        $sql="SELECT * FROM ".DB_PREFIX."customer_group WHERE csoportkod LIKE '$sap_vevocsoport'";
        $query= $this->query->query($sql);
        if (isset($query->num_rows) && $query->num_rows > 0){
            $this->sap_vevocsoport= $query->row['customer_group_id'];
        } else {
            $modosit="INSERT INTO ".DB_PREFIX."customer_group SET
                                                `customer_group_id`     =   '',
                                                `csoportkod`            =   '$sap_vevocsoport',
                                                `name`                  =   '$sap_vevocsoport' ";

            mysql_query($modosit) or Hibalista("Mennyiségi kedvezmény", "customer_group", "felvitel", $sap_vevocsoport);

            $sql="SELECT * FROM ".DB_PREFIX."customer_group WHERE csoportkod LIKE '$sap_vevocsoport'";
            $query= $this->query->query($sql);
            $this->sap_vevocsoport= $query->row['customer_group_id'];
        }
    }

    public function Termek_Kedvezmeny($sorok,$azonosito){
        if ($sorok['mennyisegtol'] > 0) $sap_quantity=$sorok['mennyisegtol'];
        else $sap_quantity=0;

        $sap_ar         =$sorok['kedvezmenyes_ar'];
        $sap_datum_tol  =$sorok['kedvezmeny_kezdete'];
        $sap_datum_ig   =$sorok['kedvezmeny_vege'];
        $sap_vevokod    =$sorok['vevokod'];
        $csoport        =false;

        if (!empty($sap_vevokod) && $sap_vevokod != "W00001"){
            $this->VevokFelvisz($sap_vevokod);
            $sql="SELECT * FROM ".DB_PREFIX."product_customer_special WHERE product_id = $azonosito AND customer_id =$this->vevo_id AND quantity=$sap_quantity";
            $query= $this->query->query($sql);
            if (isset($query->num_rows) && $query->num_rows > 0){
                $modosit="UPDATE ".DB_PREFIX."product_customer_special SET
                                                `price`                 =   '$sap_ar',
                                                `date_start`	        =   '$sap_datum_tol',
                                                `date_end`              =   '$sap_datum_ig'
                                 WHERE product_id = $azonosito AND customer_id =$this->vevo_id AND quantity=$sap_quantity";

                mysql_query($modosit) or Hibalista("Mennyiségi kedvezmény", "product_customer_special", "módosítás", $azonosito, $sap_vevokod);

            } else {
                $modosit="INSERT INTO ".DB_PREFIX."product_customer_special SET
                                                `product_id`            =   '$azonosito',
                                                `customer_id`	        =   '$this->vevo_id',
                                                `quantity`	            =   '$sap_quantity',
                                                `price`                 =   '$sap_ar',
                                                `date_start`	        =   '$sap_datum_tol',
                                                `date_end`              =   '$sap_datum_ig' ";

                mysql_query($modosit) or Hibalista("Mennyiségi kedvezmény", "product_customer_special", "felvitel", $azonosito, $sap_vevokod);
            }

        } elseif (!empty($sorok['vevocsoport'])) {
            $this->Vevo_Csoport($sorok['vevocsoport']);
            $csoport=true;


        } else{
            $this->sap_vevocsoport=123;
            $csoport=true;

        }

        if ($csoport){
            $sql="SELECT * FROM ".DB_PREFIX."product_discount WHERE product_id = $azonosito AND quantity= $sap_quantity AND customer_group_id=$this->sap_vevocsoport";
            $query= $this->query->query($sql);
            if (isset($query->num_rows) && $query->num_rows > 0){
                $modosit="UPDATE ".DB_PREFIX."product_discount SET
                                                `price`                 =   '$sap_ar',
                                                `date_start`	        =   '$sap_datum_tol',
                                                `date_end`              =   '$sap_datum_ig'
                           WHERE product_id = $azonosito AND quantity= $sap_quantity AND customer_group_id=$this->sap_vevocsoport";
                mysql_query($modosit)  or Hibalista("Mennyiségi kedvezmény", "product_discount", "módosítás", $azonosito, $this->sap_vevocsoport);

            } else {
                $modosit="INSERT INTO ".DB_PREFIX."product_discount SET
                                                `product_discount_id`   =   '',
                                                `product_id`            =   '$azonosito',
                                                `customer_group_id`	    =   '$this->sap_vevocsoport',
                                                `quantity`	            =   '$sap_quantity',
                                                `priority`              =   '0',
                                                `price`                 =   '$sap_ar',
                                                `date_start`	        =   '$sap_datum_tol',
                                                `date_end`              =   '$sap_datum_ig' ";
                mysql_query($modosit) or Hibalista("Mennyiségi kedvezmény", "product_discount", "felvitel", $azonosito, $this->sap_vevocsoport);
            }
        }
    }

    private function VevokFelvisz($sap_vevokod) {
        $sap_datum              = date('Y-m-d');

        $sql="SELECT * FROM ".DB_PREFIX."customer WHERE vevokod LIKE '$sap_vevokod'";
        $query= $this->query->query($sql);

        if (isset($query->num_rows) && $query->num_rows > 0){
            $this->vevo_id=$query->row['customer_id'];
        } else {
            $modosit="INSERT INTO ".DB_PREFIX."customer SET
                                                        customer_id	        = '',
                                                        vevokod             = '$sap_vevokod',
                                                        store_id	        = '0',
                                                        firstname	        = '',
                                                        lastname            = '',
                                                        email	            = '',
                                                        telephone	        = '',
                                                        password	        = '',
                                                        cart	            = '',
                                                        wishlist	        = '',
                                                        newsletter	        = 0,
                                                        customer_group_id	= '',
                                                        ip	                = '',
                                                        status	            = '0',
                                                        approved	        = '1',
                                                        adoszam	            = '',
                                                        date_added	        = '$sap_datum'";
            mysql_query($modosit) or   Hibalista("Akciós árak", "customer", "felvitel",  $sap_vevokod);


            $sql="SELECT * FROM ".DB_PREFIX."customer WHERE vevokod LIKE '$sap_vevokod'";
            $query= $this->query->query($sql);
            $this->vevo_id=$query->row['customer_id'];
        }
    }
}