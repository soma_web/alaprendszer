<?php  
class ControllerCheckoutCheckout extends Controller { 
	public function index() {
		// Validate cart has products and has stock.

        if (isset($_GET["visszalepett_paypal"])) {
            if (!empty($_SESSION['checkout_click_history_id'])) {
                $this->load->model("checkout/guest_history");
                $this->model_checkout_guest_history->addHistory_To(array('payment_paypal' => 'cancel'));
                unset ($_SESSION['checkout_click_history_id']);
            }
        }

		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
	  		$this->redirect($this->url->link('checkout/cart'));
    	}	
		
		// Validate minimum quantity requirments.			
		$products = $this->cart->getProducts();

        $this->data['ingyenes'] = false;
        $this->data['fizetos'] = false;

        $erkezteto_oldal = false;
        if (isset($this->request->request['ekezteto_oldal']) && $this->request->request['ekezteto_oldal'] == 1)  {
            $erkezteto_oldal = true;
        }


        foreach ($products as $product) {
            if ($product['utalvany'] == null || $product['utalvany'] == 0) {
                $this->data['fizetos'] = true;
            } else {
                $this->data['ingyenes'] = true;
            }
			$product_total = 0;
				
			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}		
			
			if ($product['minimum'] > $product_total) {
				$this->redirect($this->url->link('checkout/cart'));
			}				
		}
				
		$this->language->load('checkout/checkout');
		
		$this->document->setTitle($this->language->get('heading_title')); 
		
		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_cart'),
			'href'      => $this->url->link('checkout/cart'),
        	'separator' => $this->language->get('text_separator')
      	);
		
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
					
	    $this->data['heading_title'] = $this->language->get('heading_title');
        if ($this->data['ingyenes']) {
            $this->data['heading_title'] .= " / " . $this->language->get('heading_nyomtat');
        }
		
		$this->data['text_checkout_option']             = sprintf($this->language->get('text_checkout_option'));
		$this->data['text_checkout_account']            = $this->language->get('text_checkout_account');
		$this->data['text_checkout_payment_address']    = $this->language->get('text_checkout_payment_address');
		$this->data['text_checkout_shipping_address']   = $this->language->get('text_checkout_shipping_address');
		$this->data['text_checkout_shipping_method']    = $this->language->get('text_checkout_shipping_method');
		$this->data['text_checkout_payment_method']     = $this->language->get('text_checkout_payment_method');
        $this->data['text_checkout_confirm']            = $this->language->get('text_checkout_confirm');

        $this->data['text_checkout_option_number']             = sprintf($this->language->get('text_checkout_option_number'));
        $this->data['text_checkout_account_number']            = $this->language->get('text_checkout_account_number');
        $this->data['text_checkout_payment_address_number']    = $this->language->get('text_checkout_payment_address_number');
        $this->data['text_checkout_shipping_address_number']   = $this->language->get('text_checkout_shipping_address_number');
        $this->data['text_checkout_shipping_method_number']    = $this->language->get('text_checkout_shipping_method_number');
        $this->data['text_checkout_payment_method_number']     = $this->language->get('text_checkout_payment_method_number');
        $this->data['text_checkout_confirm_number']            = $this->language->get('text_checkout_confirm_number');


        $this->data['text_checkou_nyomtat']             = $this->language->get('text_checkou_nyomtat');


        if ($this->config->get('megjelenit_penztar_fizetesi_mod') == 0){
            $this->data['text_checkout_confirm'] = $this->language->get('text_checkout_confirm_no_payment');
            $this->data['text_checkout_shipping_method'] = $this->language->get('text_checkout_shipping_method_no_payment');
        }

		$this->data['text_modify'] = $this->language->get('text_modify');
		
		$this->data['logged'] = $this->customer->isLogged();

        $this->data['feltolto'] = false;
        if ($this->data['logged'] != null && isset($_SESSION['customer_id']) && $_SESSION['customer_id'] > 0  )  {
            $this->load->model('account/customer');
            $customer_info = $this->model_account_customer->getCustomer($_SESSION['customer_id']);
            if ($customer_info['feltolto'] == 1) {
                $this->data['feltolto'] = true;
            }
        }

		$this->data['shipping_required'] = $this->cart->hasShipping();	

        if ($erkezteto_oldal) {
            $this->data['title'] = $this->document->getTitle();
            if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
                $this->data['base'] = $this->config->get('config_ssl');
            } else {
                $this->data['base'] = $this->config->get('config_url');
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/erkezteto_checkout.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/checkout/erkezteto_checkout.tpl';
            } else {
                $this->template = 'default/template/checkout/erkezteto_checkout.tpl';
            }
        } else {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/checkout.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/checkout/checkout.tpl';
            } else {
                $this->template = 'default/template/checkout/checkout.tpl';
            }

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );
        }
				
		$this->response->setOutput($this->render());
  	}
}
?>