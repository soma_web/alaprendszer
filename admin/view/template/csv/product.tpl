
<?php echo $header; ?>
    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="success"><?php echo $success; ?></div>
        <?php } ?>

        <div class="box">
            <div class="heading">
                <h1><img src="view/image/backup.png" alt="" /> <?php echo $heading_title; ?></h1>
                <div class="buttons">
                    <a onclick="$('#restore').submit();" class="button helyreallit" style="display: none"><?php echo $button_restore; ?></a>
                    <a onclick="$('#backup').submit();" class="button"><?php echo $button_backup; ?></a>
                </div>
            </div>
            <div class="content">
                <form action="<?php echo $restore; ?>" method="post" enctype="multipart/form-data" id="restore">
                    <table class="form" style="margin-bottom: 0">
                        <tr>
                            <td><?php echo $entry_restore; ?></td>
                            <td><input type="file" name="import" accept=".csv" onchange="Valasztva(this)"/></td>
                        </tr>


                    </table>
                </form>
                <form action="<?php echo $backup; ?>" method="post" enctype="multipart/form-data" id="backup">
                    <div style="margin:0 0 20px 10px;">
                        <span style="margin-right: 10px;"><?php echo $text_tol?><input type="text" name="limit_tol" size="4"></span>
                        <span><?php echo $text_db?><input type="text" name="limit_db" size="4"></span>
                    </div>

                    <table class="form">
                        <tr>
                            <td><?php echo $entry_backup; ?></td>
                            <td>
                                <div style="display: table; margin-left: auto;"><?php echo isset($text_sorrend) ? $text_sorrend: ""; ?></div>

                                <div class="scrollbox " style="margin-bottom: 5px; height: 500px; width: 600px;">

                                    <?php $class = 'odd'; ?>
                                    <?php foreach ($fields as $field) { ?>
                                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                        <div class="<?php echo $class; ?> <?php echo !isset($no_checked) ? 'sortitem' : ''; ?>" >
                                            <?php if (!isset($no_checked) ) { ?>
                                                <span style="top: 3px;" class="sorthandle"><img src="view/image/icon-drag-drop.png"></span>
                                            <?php } ?>

                                            <?php $readonly = isset($no_checked) && $no_checked && $field != 'Delete' ? 'onclick="return false"' : ''; ?>
                                            <?php $opacicy  = isset($no_checked) && $no_checked && $field != 'Delete' ? 'opacity:0.5 ' : ''; ?>


                                            <?php $piros    = !empty($megjeloles[substr(strstr($field,".",0),1)]) ? 'color: #AF0000' : ''; ?>
                                            <?php $kiegeszites = !empty($megjeloles[substr(strstr($field,".",0),1)]) && $megjeloles[substr(strstr($field,".",0),1)] != 1 ? $megjeloles[substr(strstr($field,".",0),1)] : ''; ?>


                                            <input style="margin-left: 16px; <?php echo $opacicy?>" type="checkbox" name="backup[]" value="<?php echo $field; ?>" checked="checked" <?php echo $readonly?> onchange="tolIgMegjelenit(this)"/>
                                            <span style="<?php echo $piros;?>">
                                                <?php echo strstr($field,'.') ? substr(strstr($field,'.',0),1) : $field; ?>
                                                <span><?php echo $kiegeszites;?></span>
                                            </span>


                                            <?php if (isset($text_sorrend) && $field != "Delete") { ?>
                                                <input style="float: right" type="radio" name="sorrend" value="<?php echo $field; ?>">
                                            <?php } ?>

                                            <?php if (strstr($field,".",0)) { ?>
                                                <?php $mezo = substr(strstr($field,".",0),1); ?>
                                                <?php if (array_key_exists($mezo,$tol_ig) && $tol_ig[$mezo] == "input") { ?>
                                                    <span class="tol_ig" style="float: right">
                                                        <input type="text" name="filter[<?php echo $field; ?>-tol]" size="4"> -
                                                        <input type="text" name="filter[<?php echo $field; ?>-ig]" size="4">
                                                    </span>
                                                <?php } elseif (array_key_exists($mezo,$tol_ig) && $tol_ig[$mezo] == "select") { ?>
                                                    <select name="filter[<?php echo $field; ?>-select]" style="float: right; height: 18px;" class="tol_ig">
                                                        <option value='no'><?php echo $text_select?></option>
                                                        <option value="0"><?php echo $text_no?></option>
                                                        <option value="1"><?php echo $text_yes?></option>
                                                    </select>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php if (!isset($no_checked) ) { ?>
                                    <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
                                <?php } ?>
                                <?php if (!empty($megjeloles_magyarazat)) { ?>
                                    <div style="color: #AF0000; font-size: 11px; margin: 10px 0"><?php echo $megjeloles_magyarazat;?></div>
                                <?php } ?>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php echo $footer; ?>

<script>


    $(document).ready(function(){
        setTimeout(" $('.warning, .success').slideUp(500)",5000);

    });

    function tolIgMegjelenit(para) {
        if ($(para).prop("checked")  ) {
            if ($(para).next(".tol_ig").length > 0) {
                $(para).next().fadeIn("slow");
            }
        } else {
            if ($(para).next(".tol_ig").length > 0) {
                $(para).next().fadeOut("slow");
            }
        }

    }


    function Valasztva(para) {
        if (para.value.length > 0) {
            $(".helyreallit").fadeIn("slow");
        } else {
            $(".helyreallit").fadeOut("slow");

        }
    }
</script>