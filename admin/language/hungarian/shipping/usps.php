<?php

// Heading
$_['heading_title']         = 'Egyesült Államok postaszolgálata (USPS)';

// Text
$_['text_shipping']         = 'Szállítás';
$_['text_success']          = 'Siker: Az Egyesült Államok postaszolgálata szállítási mód módosítása megtörtént!';
$_['text_domestic_0']       = 'Első-osztály';
$_['text_domestic_1']       = 'Elsőbbségi posta';
$_['text_domestic_2']       = 'Expressz posta visszatartva felvételre';
$_['text_domestic_3']       = 'Expressz posta postafiókból címre';
$_['text_domestic_4']       = 'Csomagposta';
$_['text_domestic_5']       = 'Összekötözött nyomtatványok';
$_['text_domestic_6']       = 'Média posta';
$_['text_domestic_7']       = 'Könyvtár';
$_['text_domestic_12']      = 'Elsőosztályú bélyeges képeslap';
$_['text_domestic_13']      = 'Expressz posta átalánydíjas boríték';
$_['text_domestic_16']      = 'Elsőbbségi posta átalánydíjas boríték';
$_['text_domestic_17']      = 'Elsőbbségi posta általános átalánydíjas doboz';
$_['text_domestic_18']      = 'Elsőbbségi posta kulcsok és azonosítók';
$_['text_domestic_19']      = 'Elsőosztályú kulcsok és azonosítók';
$_['text_domestic_22']      = 'Elsőbbségi posta átalánydíjas nagy doboz';
$_['text_domestic_23']      = 'Expressz posta vasárnap/munkaszüneti nap';
$_['text_domestic_25']      = 'Expressz posta átalánydíjas boríték vasárnap/munkaszüneti nap';
$_['text_domestic_27']      = 'Expressz posta átalánydíjas boríték visszatartva felvételre';
$_['text_domestic_28']      = 'Elsőbbségi posta átalánydíjas kis doboz';
$_['text_international_1']  = 'Expressz posta';
$_['text_international_2']  = 'Elsőbbségi nemzetközi posta';
$_['text_international_4']  = 'Globális garantált expressz (dokumentum és nem-dokumentun)';
$_['text_international_5']  = 'Globális garantált expressz dokumentum';
$_['text_international_6']  = 'Globális garantált expressz nem-dokumentum négyszögletes';
$_['text_international_7']  = 'Globális garantált expressz nem-dokumentum nem négyszögletes';
$_['text_international_8']  = 'Elsőbbségi posta átalánydíjas boríték';
$_['text_international_9']  = 'Elsőbbségi posta átalánydíjas doboz';
$_['text_international_10'] = 'Expressz nemzetközi posta átalánydíjas boríték';
$_['text_international_11'] = 'Elsőbbségi posta átalánydíjas nagy doboz';
$_['text_international_12'] = 'Globális garantált expressz boríték';
$_['text_international_13'] = 'Elsőosztályú nemzetközi posta levelek';
$_['text_international_14'] = 'Elsőosztályú nemzetközi posta lapos';
$_['text_international_15'] = 'Elsőosztályú nemzetközi posta csomag';
$_['text_international_16'] = 'Elsőbbségi posta átalánydíjas kis doboz';
$_['text_international_21'] = 'Képeslapok';
$_['text_regular']          = 'Szabványos';
$_['text_large']            = 'Nagy';
$_['text_oversize']         = 'Túlméretes';
$_['text_rectangular']      = 'Négyszögletes';
$_['text_non_rectangular']  = 'Nem négyszögletes';
$_['text_variable']         = 'Módosítható';

// Entry
$_['entry_user_id']         = 'Felhasználó ID:';
$_['entry_password']        = 'Jelszó:';
$_['entry_postcode']        = 'Irányítószám:';
$_['entry_domestic']        = 'Hazai szolgáltatások:';
$_['entry_international']   = 'Nemzetközi szolgáltatások:';
$_['entry_size']            = 'Méret:';
$_['entry_container']       = 'Tároló:';
$_['entry_machinable']      = 'Megmunkálható:';
$_['entry_dimension']       = 'Méretek (H x Sz x M):';
$_['entry_girth']           = 'Girth:';
$_['entry_display_time']    = 'Szállítási idő kijelzése:<br /><span class="help">Akarja, hogy ki legyen jelezve a szállítási idő? (pl. Szállítás 3-5 napon belül)</span>';
$_['entry_display_weight']  = 'Szállítási súly kijelzése:<br /><span class="help">Akarja, hogy ki legyen jelezve a szállítási súly? (pl. Szállítási súly : 2.7674 Kg)</span>';
$_['entry_weight_class']    = 'Súlymérték:<br /><span class="help">fontra állítva.</span>';
$_['entry_tax']             = 'Adóosztály:';
$_['entry_geo_zone']        = 'Földrajzi zóna:';
$_['entry_status']          = 'Állapot:';
$_['entry_sort_order']      = 'Sorrend:';
$_['entry_title_heading'] = 'Fejléc megnevezése:';
$_['entry_title_text'] = 'Szállítási mód megnevezése:';
$_['entry_mail']        = 'Egyéb figyelmeztető e-mailek:<br /><span class="help">Egyéb figyelmeztető e-mailek, amiket kapni szeretne, a közponzi áruház  e-mailjei mellett. (vesszővel elválasztva)</span>';

// Error
$_['error_permission']      = 'Figyelmeztetés: Az Egyesült Államok postaszolgálata szállítási mód módosítása az Ön számára nem engedélyezett!';
$_['error_user_id']         = 'Felhasználó ID szükséges!';
$_['error_postcode']        = 'Irányítószám szükséges!';
?>
