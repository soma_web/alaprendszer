<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/jquery.mCustomScrollbar.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.mCustomScrollbar.js"></script>
<?php $template=$this->config->get("config_template");?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $template?>/stylesheet/carousel22.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.jcarousel.min22.js"></script>

<?php
//$show= 2;
$vantab = false;

if (isset($products_bestseller) && $products_bestseller) {
    $vantab = true;
    $tab = "products_bestseller";

} elseif (isset($products_featured) && $products_featured) {
    $vantab = true;
    $tab = "products_featured";

} elseif (isset($products_latest) &&  $products_latest) {
    $vantab = true;
    $tab = "products_latest";
}
?>
<?php if ($vantab) { ?>
    <div id="tabs_harmas" class="htabs">

        <?php if ($products_bestseller) { ?>
            <?php if ($tab == "products_bestseller") { ?>
                <a  style="position: relative; left: 8px; margin-right: 18px;"  href="#tab-bestseller"><?php echo $heading_title_bestseller; ?> (<?php echo count($products_bestseller); ?>) </a>
            <?php } else {?>
                <a  href="#tab-bestseller"><?php echo $heading_title_bestseller; ?> (<?php echo count($products_bestseller); ?>) </a>
            <?php } ?>
        <?php } ?>

        <?php if ($products_featured) { ?>
            <?php if ($tab == "products_featured") { ?>
                <a style="position: relative; left: 8px; margin-right: 18px;"  href="#tab-featured"><?php echo $heading_title_featured; ?> (<?php echo count($products_featured); ?>)</a>
            <?php } else {?>
                <a href="#tab-featured"><?php echo $heading_title_featured; ?> (<?php echo count($products_featured); ?>)</a>
            <?php } ?>
        <?php } ?>

        <?php if ($products_latest) { ?>
            <?php if ($tab == "products_latest") { ?>
                <a style="position: relative; left: 8px; margin-right: 18px;"  href="#tab-latest"><?php echo $heading_title_latest; ?> (<?php echo count($products_latest); ?>)</a>
            <?php } else {?>
                <a href="#tab-latest"><?php echo $heading_title_latest; ?> (<?php echo count($products_latest); ?>)</a>
            <?php } ?>
        <?php } ?>

    </div>
<?php } ?>



<!-- Legnépszerűbb termékek-->
<?php if ($products_bestseller) { ?>
    <div id="tab-bestseller"  class="tab-content" style="overflow:visible">
        <?php $id_name="nepszeru"?>
        <?php $prs_name="products_bestseller"?>
        <?php $module_name="harmas_nepszeru"?>

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module//korhinta.tpl')) {
            // $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/korhinta.tpl';
        } else {
            //$arkiir = DIR_TEMPLATE.'default/template/module/korhinta.tpl';
        } ?>
        <?php //include($arkiir); ?>



        <div class="box-product tab_kornyezet" style="width: 100%">
            <ul  class="tab-bestseller">
                <?php foreach ($products_bestseller as $product) { ?>
                    <li>
                        <div class="termek" >
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>

                    </li>
                <?php } ?>
            </ul>
        </div>





    </div>
<?php } ?>

<!-- Kiemelt termékek-->
<?php if ($products_featured) { ?>
    <div id="tab-featured"  class="tab-content" style="overflow:visible">
        <?php $id_name="kiemelt"?>
        <?php $prs_name="products_featured"?>
        <?php $module_name="harmas_kiemelt"?>

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module//korhinta.tpl')) {
            //$arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/korhinta.tpl';
        } else {
            //$arkiir = DIR_TEMPLATE.'default/template/module/korhinta.tpl';
        } ?>
        <?php //include($arkiir); ?>


        <div class="box-product tab_kornyezet" style="width: 100%">
            <ul  class="tab-featured">
                <?php foreach ($products_featured as $product) { ?>
                    <li>
                        <div class="termek"  >
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>

                    </li>
                <?php } ?>
            </ul>
        </div>


    </div>
<?php } ?>

<!-- Legújabb termékek-->
<?php if ($products_latest) { ?>
    <div id="tab-latest"  class="tab-content" style="overflow: visible">
        <?php $id_name="legujabb"?>
        <?php $prs_name="products_latest"?>
        <?php $module_name="harmas_legujabb"?>

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module//korhinta.tpl')) {
            //$arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/korhinta.tpl';
        } else {
            //$arkiir = DIR_TEMPLATE.'default/template/module/korhinta.tpl';
        } ?>
        <?php //include($arkiir); ?>

        <div class="box-product tab_kornyezet" style="width: 100%">
            <ul  class="tab-latest">
                <?php foreach ($products_latest as $product) { ?>
                    <li>
                        <div class="termek">
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>

                    </li>
                <?php } ?>
            </ul>
        </div>

    </div>
<?php } ?>

<script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/bxslider/jquery.bxslider.css" />
<script>


    //$('#tabs_harmas a').tabs();
    $('#tabs_harmas a').tabs({
        bxSlidesOn: true,
        minSlides: 4,
        maxSlides: 4
    });
</script>