BEGIN
DECLARE product_customer_azonosito INT(11) default 0;
DECLARE customer_azonosito INT(11) default 0;
DECLARE product_azonosito INT(11) default 0;
DECLARE listaar INT(11) default 0;

if uj_TermekKod > 0 then
call termek_felvitel(uj_TermekKod);
end if;

select product_id  into product_azonosito  from trodimp_aruhaz.product  WHERE cikkszam LIKE uj_TermekKod limit 1;
select customer_id into customer_azonosito from trodimp_aruhaz.customer WHERE customer_id = uj_PartnerKod OR vevokod LIKE uj_PartnerKod limit 1;

if customer_azonosito != 0 then
update trodimp_aruhaz.customer set szazalek = uj_Engedmeny  WHERE customer_id = customer_azonosito;

select product_id into product_customer_azonosito from trodimp_aruhaz.product_customer_special  where product_id = product_azonosito and customer_id = customer_azonosito limit 1;

if product_customer_azonosito = 0 and uj_SpeciAr > 0 then
insert into trodimp_aruhaz.product_customer_special set customer_id = customer_azonosito,
product_id  = product_azonosito,
quantity	  = 0,
price	      = uj_SpeciAr,
date_start  = uj_Datumtol,
date_end    = uj_Datumig,
szazalekban  = 0;
elseif uj_SpeciAr > 0 then
update trodimp_aruhaz.product_customer_special set      quantity	  = 0,
price	      = uj_SpeciAr,
date_start  = uj_Datumtol,
date_end    = uj_Datumig,
szazalekban  = 0              where product_id = product_azonosito and customer_id = customer_azonosito;
end if;

select price into listaar from trodimp_aruhaz.product where product_id = product_azonosito limit 1;
set listaar=listaar*(100-uj_Engedmeny2)/100;

set product_customer_azonosito = 0;
select product_id into product_customer_azonosito from trodimp_aruhaz.product_customer_special where product_id = product_azonosito and customer_id = customer_azonosito limit 1;

if product_customer_azonosito = 0 and uj_Engedmeny2 > 0 then
insert into trodimp_aruhaz.product_customer_special set  customer_id = customer_azonosito,
product_id  = product_azonosito,
quantity	  = 0,
price	      = listaar,
date_start  = uj_Datumtol,
date_end    = uj_Datumig,
szazalekban = 1;
elseif uj_Engedmeny2 > 0 then
update trodimp_aruhaz.product_customer_special set       quantity	  = 0,
price	      = listaar,
date_start  = uj_Datumtol,
date_end    = uj_Datumig,
szazalekban  = 1             where product_id = product_azonosito and customer_id = customer_azonosito;
end if;

end if;

END