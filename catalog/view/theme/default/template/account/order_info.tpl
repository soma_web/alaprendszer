<?php echo $header; ?>
<?php $megjelenit_product = $this->config->get('megjelenit_product');?>

<div class="breadcrumb">
    <?php $elemszam=count($breadcrumbs);
    $i=0;
    foreach ($breadcrumbs as $breadcrumb) { $i++;
        if ($i==$elemszam) { ?>
            <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <? } else {
            echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <? }
    } ?>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <h1><?php echo $heading_title; ?></h1>
  <table class="list">
    <thead>
      <tr>
        <td class="left" colspan="2"><?php echo $text_order_detail; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left" style="width: 50%;"><?php if ($invoice_no) { ?>
          <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
          <?php } ?>
          <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
          <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
        <td class="left" style="width: 50%;"><?php if ($payment_method) { ?>
          <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
          <?php } ?>
          <?php if ($shipping_method) { ?>
          <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
          <?php } ?></td>
      </tr>
    </tbody>
  </table>
  <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $text_payment_address; ?></td>
        <?php if ($shipping_address) { ?>
        <td class="left"><?php echo $text_shipping_address; ?></td>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left"><?php echo $payment_address; ?></td>
        <?php if ($shipping_address) { ?>
        <td class="left"><?php echo $shipping_address; ?></td>
        <?php } ?>
      </tr>
    </tbody>
  </table>
    <?php if ( $megjelenit_kosar['netto_adat'] == 1) {?>
        <table class="list">
            <thead>
            <tr>
                <td class="left"><?php echo $column_name; ?></td>
                <!--<td class="left"><?php echo $column_model; ?></td>-->

                <td class="right"><?php echo $column_price_netto_egysegar; ?></td>
                <td class="right"><?php echo $column_price_brutto_egysegar; ?></td>
                <td class="right"><?php echo $column_quantity; ?></td>
                <td class="right"><?php echo $column_price_netto; ?></td>
                <td class="right"><?php echo $column_price_brutto; ?></td>

                <?php if ($products) { ?>
                    <td style="width: 1px;"></td>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product) { ?>
                <tr>
                    <td class="left">
                        <?php if ($megjelenit_product['product_model_termeknevben']) { ?>
                            <?php if (isset($product['model']) && $product['model']) { ?>
                                <span class="model_cart"><?php echo $product['model'];?></span> <?php echo " - ";?>
                            <?php }?>
                        <?php }?>

                        <?php if ($megjelenit_product['product_cikkszam_termeknevben']) { ?>
                            <?php if (isset($product['cikkszam']) && $product['cikkszam']) { ?>
                                <span class="model_cart"><?php echo $product['cikkszam'];?></span> <?php echo " - ";?>
                            <?php }?>
                        <?php }?>

                        <?php if ($megjelenit_product['product_gyarto_termeknevben']) { ?>
                            <?php if (isset($product['manufacturer']) && $product['manufacturer']) { ?>
                                <span class="manufacturer_cart"><?php echo $product['manufacturer'];?></span> <?php echo " - ";?>
                            <?php }?>
                        <?php }?>
                        <?php echo $product['name']; ?>
                        <?php foreach ($product['option'] as $option) { ?>
                            <br />
                            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                        <?php } ?></td>
                    <!--<td class="left"><?php echo $product['model']; ?></td>-->
                    <td class="right"><?php echo $product['price']; ?></td>
                    <td class="right"><?php echo $product['price_brutto']; ?></td>
                    <td class="right"><?php echo $product['quantity']; ?></td>
                    <td class="right"><?php echo $product['total']; ?></td>
                    <td class="right"><?php echo $product['price_brutto_osszesen']; ?></td>
                    <td class="right"><a href="<?php echo $product['return']; ?>"><img src="catalog/view/theme/default/image/return.png" alt="<?php echo $button_return; ?>" title="<?php echo $button_return; ?>" /></a></td>
                </tr>
            <?php } ?>
            <?php foreach ($vouchers as $voucher) { ?>
                <tr>
                    <td class="left"><?php echo $voucher['description']; ?></td>
                    <td class="left"></td>
                    <td class="right">1</td>
                    <td class="right"><?php echo $voucher['amount']; ?></td>
                    <td class="right"><?php echo $voucher['amount']; ?></td>
                    <?php if ($products) { ?>
                        <td></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <?php foreach ($totals as $total) { ?>


                <?php if ($total['code'] == "shipping") { ?>
                    <!--<td colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 5; } else {echo 4;}?>"><?php echo $shipping_arak['title']?></td>-->


                    <tr>
                        <td  colspan="4" class="right"><b><?php echo $total['title']; ?>:</b></td>
                        <td class="right"><?php echo $total['text']; ?></td>
                        <td  colspan="1" class="right"><?php echo $this->currency->format($total['value']+$total['ado'], $currency_code, $currency_value)?></td>
                        <td></td>
                    </tr>


                    <tr style="border:none">
                        <td style="border:none" colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 7; } else {echo 6;}?>">&nbsp;</td>
                    </tr>

                <?php } else {?>
                    <tr>
                        <td  colspan="5" class="right"><b><?php echo $total['title']; ?>:</b></td>
                        <td class="right"><?php echo $total['text']; ?></td>
                        <?php if ($products) { ?>
                            <td></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tfoot>
        </table>
    <?php } else {?>
        <table class="list">
            <thead>
              <tr>
                <td class="left"><?php echo $column_name; ?></td>
                <td class="left"><?php echo $column_model; ?></td>
                <td class="right"><?php echo $column_quantity; ?></td>
                <td class="right"><?php echo $column_price; ?></td>
                <td class="right"><?php echo $column_total; ?></td>
                <?php if ($products) { ?>
                <td style="width: 1px;"></td>
                <?php } ?>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td class="left">
                    <?php if ($megjelenit_product['product_model_termeknevben']) { ?>
                        <?php if (isset($product['model']) && $product['model']) { ?>
                            <span class="model_cart"><?php echo $product['model'];?></span> <?php echo " - ";?>
                        <?php }?>
                    <?php }?>

                    <?php if ($megjelenit_product['product_cikkszam_termeknevben']) { ?>
                        <?php if (isset($product['cikkszam']) && $product['cikkszam']) { ?>
                            <span class="model_cart"><?php echo $product['cikkszam'];?></span> <?php echo " - ";?>
                        <?php }?>
                    <?php }?>

                    <?php if ($megjelenit_product['product_gyarto_termeknevben']) { ?>
                        <?php if (isset($product['manufacturer']) && $product['manufacturer']) { ?>
                            <span class="manufacturer_cart"><?php echo $product['manufacturer'];?></span> <?php echo " - ";?>
                        <?php }?>
                    <?php }?>
                    <?php echo $product['name']; ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?></td>
                <td class="left"><?php echo $product['model']; ?></td>
                <td class="right"><?php echo $product['quantity']; ?></td>
                <td class="right"><?php echo $product['price']; ?></td>
                <td class="right"><?php echo $product['total']; ?></td>
                <td class="right"><a href="<?php echo $product['return']; ?>"><img src="catalog/view/theme/default/image/return.png" alt="<?php echo $button_return; ?>" title="<?php echo $button_return; ?>" /></a></td>
              </tr>
              <?php } ?>
              <?php foreach ($vouchers as $voucher) { ?>
              <tr>
                <td class="left"><?php echo $voucher['description']; ?></td>
                <td class="left"></td>
                <td class="right">1</td>
                <td class="right"><?php echo $voucher['amount']; ?></td>
                <td class="right"><?php echo $voucher['amount']; ?></td>
                <?php if ($products) { ?>
                <td></td>
                <?php } ?>
              </tr>
              <?php } ?>
            </tbody>
            <tfoot>
              <?php foreach ($totals as $total) { ?>
              <tr>
                <td colspan="3"></td>
                <td class="right"><b><?php echo $total['title']; ?>:</b></td>
                <td class="right"><?php echo $total['text']; ?></td>
                <?php if ($products) { ?>
                <td></td>
                <?php } ?>
              </tr>
              <?php } ?>
            </tfoot>
          </table>

    <?php } ?>



  <?php if ($comment) { ?>
  <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $text_comment; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left"><?php echo $comment; ?></td>
      </tr>
    </tbody>
  </table>
  <?php } ?>
  <?php if ($histories) { ?>
  <h2><?php echo $text_history; ?></h2>
  <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $column_date_added; ?></td>
        <td class="left"><?php echo $column_status; ?></td>
        <td class="left"><?php echo $column_comment; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($histories as $history) { ?>
      <tr>
        <td class="left"><?php echo $history['date_added']; ?></td>
        <td class="left"><?php echo $history['status']; ?></td>
        <td class="left"><?php echo $history['comment']; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  <?php } ?>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 