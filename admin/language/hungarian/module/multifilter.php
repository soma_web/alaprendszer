<?php
// Heading
$_['heading_title']       = 'Multi Szűrő';

// Text
$_['text_module']                   = 'Modulok';
$_['text_success']                  = 'Sikerült: Ön módosította multi szűrő modult!';
$_['text_content_top']              = 'Tartalom felül';
$_['text_content_bottom']           = 'Tartalom alul';
$_['text_column_left']              = 'Bal oldal';
$_['text_column_right']             = 'Jobb oldal';
$_['text_szukit']                   = 'Szűkít';
$_['text_bovit']                    = 'Bővít';
$_['text_netto_ar']                 = 'Nettó ár:';
$_['text_name']                     = 'Név';
$_['text_szin']                     = 'Szín';
$_['text_megjelenites']             = 'Megjelenít';
$_['text_megjelenites_szincsoport'] = 'Színcsoport';
$_['text_megjelenites_szinek']      = 'Szinek';
$_['checkox_tpl']                   = 'Checkbox';
$_['color_tpl']                     = 'Szín';
$_['negyzetes_tpl']                 = 'Négyzetrács';
// Entry

$_['entry_szuro_neve']    = 'Szűrő neve';
$_['entry_folyamat']      = 'Szűrés módja';

$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_status']        = 'Státusz:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_template']      = 'Sablon neve:';
$_['entry_teljes_lista']  = 'Teljes lista:';
$_['entry_szuresgomb_latszik']  = 'Szűrés gomb:';
$_['entry_kereses_latszik']  = 'Általános keresés:';
$_['entry_ajaxos']          = 'Ajaxos hívás:';


//$_['entry_kategoria_vezerel']   = 'Kategoria vezereli a szűréseket:';
$_['entry_kategoria_checkbox']  = 'Checkbox-os <b>kategoria</b> ';
$_['entry_kategoria_normal']    = 'Normál <b>kategoria</b> ';
$_['entry_kategoria_sub']       = '<b>Alkategoria</b> ';
$_['entry_szuro_csoport']       = 'Szűrő csoport ';
$_['entry_szuro']               = 'Alszűrő ';
$_['entry_gyarto']              = 'Gyártó ';
$_['entry_meret']               = 'Méret választék';
$_['entry_szin']                = 'Szín választék';
$_['entry_szurok_torlese']      = 'Szűrők törlése gomb';

$_['entry_tulajdonsagok']       = 'Tulajdonság ';
$_['entry_valasztek']           = 'Választék ';
$_['entry_raktaron']            = 'Raktáron ';
$_['entry_ar_szures']           = 'Szűrés árra ';
$_['entry_ar_szures_szazalek']  = 'Szűrés százalékos árra';
$_['entry_ar_szures_tol_ig']    = 'Szűrés árra min-max.';
// Error
$_['error_permission']    = 'Figyelmeztetés: Önnek nincs jogosultsága módosítani a szűrő modult!';
?>