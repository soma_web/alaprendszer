<?php if(!(isset($beallitas['null_off_brutto']) && $beallitas['null_off_brutto'] == 1)) { ?>
    <?php $product['not_null'] = true; ?>
<?php } ?>
<?php if($product['not_null'] && empty($product['ar_tiltasa'])) { ?>
    <?php if ($product['price']) { ?>
        <div class="price <?php echo !$product['mutat_arat'] ? 'price_nemkell' : ''?>" >
            <?php if (isset($beallitas['brutto'])) { ?>
                <?php $brutto = $this->language->get('text_brutto'); ?>
                <span><?php echo $brutto?>: </span>
            <?php } ?>

            <?php if (!$product['special']) { ?>
                <span class="price-new"><?php echo $product['price']; ?></span>
            <?php } else { ?>
                <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
            <?php } ?>
        </div>
    <?php }?>
<?php }?>