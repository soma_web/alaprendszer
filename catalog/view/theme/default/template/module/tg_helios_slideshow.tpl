		<div id="top"> 
			<div class="slideshow"> 
			<div class="controlnav-thumbs">
  			<div id="tg_slideshow<?php echo $module; ?>" class="nivoSlider" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;">
    		<?php foreach ($banners as $banner) { ?>
    			<?php if ($banner['link']) { ?>
   				 	<a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" rel="<?php echo HTTPS_SERVER; ?>catalog/view/theme/default/stylesheet/timthumb.php?src=<?php echo $banner['image']; ?>&amp;w=170&amp;h=34&amp;zc=1" alt="<?php echo $banner['title']; ?>" /></a>
    			<?php } else { ?>
    				<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" rel="<?php echo HTTPS_SERVER; ?>catalog/view/theme/default/stylesheet/timthumb.php?src=<?php echo $banner['image']; ?>&amp;w=170&amp;h=34&amp;zc=1" />
				<?php } ?>
    		<?php } ?>
  			</div>
</div>
	
			</div>
			
		</div>	
			<script type="text/javascript">
    			$(document).ready(function() {
     	    	$('#tg_slideshow<?php echo $module; ?>').nivoSlider({
				animSpeed:'<?php echo $speed; ?>',
				effect:'<?php echo $effect; ?>',
				slices:'<?php echo $slices; ?>',
				boxCols:'<?php echo $boxcolumns; ?>',
     	   		boxRows:'<?php echo $boxrows; ?>',
     	 	  	directionNav:true,
				directionNavHide:false,
				captionOpacity:0.6,
				controlNav:true,
				controlNavThumbs:true,
				controlNavThumbsFromRel:true,
				pauseTime:<?php echo $delay; ?>,		
				pauseOnHover:<?php echo $pause; ?>
				});
    			});
    		</script>