<?php
// Heading
$_['heading_title']               = 'Responsive Slideshow';
$_['heading_banner_title']        = 'Responsive Slideshow - Banners';
$_['heading_import_banner_title'] = 'Responsive Slideshow - Import Banners';
// Text
$_['button_save_stay']	   = 'Mentés & Maradás';
$_['button_import']		   = 'Import';
$_['button_export']		   = 'Export';
$_['text_module']          = 'Modulok';
$_['text_success']         = 'Sikeresen módosította az Responsive Slideshow modult!';
$_['text_content_top']     = 'Tartalom teteje';
$_['text_content_bottom']  = 'Taratalom alja';
$_['text_column_left']     = 'Bal oszlop';
$_['text_column_right']    = 'Jobb oszlop';
$_['text_mainmenu']        = 'Főmenü';
$_['text_slideshow']       = 'Slideshow';
$_['text_footer_top']      = 'Footer teteje';
$_['text_footer_center']   = 'Footer közepe';
$_['text_footer_bottom']   = 'Footer alja';
$_['text_default']         = 'Alapértelmezett';
$_['text_all_layout']      = 'Minden megjelenés';
$_['text_header_top']      = 'Header teteje';
$_['text_header_bottom']   = 'Header alja';
$_['text_ecslideshow_module']   = 'Slideshow Modul';
$_['text_ecslideshow_banner']   = 'Bannerek kezelése';
$_['text_ecslideshow_support']  = 'Támogatás';
$_['text_banner_image']         = 'Banner Képek';
$_['text_image_manager']        = 'Képek Kezelése';
$_['text_browse']               = 'Fájlok tallózása';
$_['text_clear']                = 'Kép törlése';
$_['text_input_product_name']   = 'Termék neve';
$_['text_select_banner']        = 'Banner kiválasztása';
$_['text_default']              = 'Alapértelmezett';
$_['text_white_space']          = 'Skálázás fehér területtel (Scale with white space)';
$_['text_resize_width']         = 'Kitöltés szélesség szerint';
$_['text_resize_height']        = 'Kitöltés magasság szerint';
$_['error_title']               = 'Hiba <strong>Banner %s</strong>: Banner kép cím hiányzik. Kérem adja meg minden nyelven a banner kép címét.';
$_['text_help_import']          = '';
$_['text_before']       = 'Elé';
$_['text_after']        = 'Mögé';

$_['all_page']                  = 'Minden oldal';
// Entry
$_['entry_caption_background_color'] = 'Felirat háttér szín: ';
$_['entry_caption_text_color']       = 'Felirat szín: ';
$_['entry_caption_link_color']       = 'Felirat link szín: ';
$_['entry_caption_class']            = 'Felirat Class: <br/><span class="help">CSS class név a slider felirathoz.</span>';
$_['entry_video_embed_code']         = 'Video beágyazás:<br/><span class="help">Slideren megjelenő beágyazott videó kód.</span>';

$_['entry_async_loading']       = 'Engedélyezés Async Betöltés:<br/><span class="help">Engedélyezés/Letiltás asynchronous scriptek használata azt jelenti hogy az oldalad gyorsabban betöltődik. A scriptet a háttérben tölti le, ezért az oldal megjelenik a script letöltése előtt.</span>';
$_['entry_show_slider_link']    = 'Engedélyezés slider link:<br><span class="help">Engedélyezés/Letiltás slide kép link bekapcsolása.</span>';
$_['entry_random_slider']       = 'Véletlenszerű slider mód:<br/><span class="help">Engedélyezés/Letiltás slideok megjelenítése véletlenszerű sorrendben.</span>';
$_['entry_strip_html_code']     = 'Szalag HTML kód:<br/><span class="help">Engedélyezés/Letiltás szalag HTML kód a leíráson</span>';
$_['entry_show_custom_code']    = 'Saját kód bekapcsolása';
$_['entry_custom_code']         = 'Saját kód: <br/><span class="help">Saját php kód beillleszthető (nem szükséges: <b>&lt;?php</b> és <b>?&gt;</b> ), ez hasznos lehet slider - modul integrációnál. Például: <br/><strong>echo $this->getChild("module/ecflashsale/ecflashsale", 31);</strong> </span>';
$_['entry_resize_type']         = 'Átméretezés Típusa: ';
$_['entry_support_rtl']         = 'Jobbról Balra Támogatása ?';
$_['entry_slider_caption']      = 'Slideshow Felirat';
$_['entry_show_caption']        = 'Felirat Megjelenítése:<br/><span class="help">Megjelenít/Rejt slider felirat.</span>';
$_['entry_caption_effect']      = 'Felirat Effect:';
$_['entry_store']               = 'Áruházak: ';
$_['entry_name']                = 'Név: ';
$_['entry_bar_position']        = 'Sáv Pozíció: ';
$_['entry_layout']              = 'Megjelenés:';
$_['entry_full_display']        = 'Slideshow áthelyetzése:<br/><span class="help">Áthelyezi egy tag elé vagy mögé <br>a slideshow-t.<br> Adja meg a tag <b style="color: #000;">Id</b> vagy <b style="color: #000">class</b> nevét<br> <b style="font-size: 13px; color: #000">#</b> vagy <b style="font-size: 15px; color:#000">.</b> -al kezdve. </span>';

$_['entry_position']            = 'Pozíció:';
$_['entry_status']              = 'Állapot:';
$_['entry_sort_order']          = 'Sorrend:';
$_['entry_banner']		        = '<font style="color:#E25203">Banner: <br><span class="help">Banner kép kiválasztása</span></font>';
$_['entry_module_width']        = 'Modul Szélesség: <br/><span class="help">Szám és típus megadása "px,%, auto", például: 980px, 100% vagy auto</span>';
$_['entry_module_height']       = 'Modul Magasság:  <br/><span class="help">Szám és típus megadása "px,%, auto", például: 780px, 60% vagy auto</span>';
$_['entry_main_width']          = 'Fő Kép Szélesség: <br><span class="help">Slideshow Fő kép szélességének beállítása (szám érték), például: 980</span>';
$_['entry_main_height']         = 'Fő Kép Magasság:<br><span class="help">Slideshow Fő kép magasságának beállítása (szám érték), például: 380</span>';
$_['entry_thumbnail_width']     = 'Indexkép Szélesség: ';
$_['entry_thumbnail_height']    = 'Indexkép Magasság: ';
$_['entry_slider_effect']       = 'Slideshow Effect';
$_['entry_skin']                ='Felület: <br><span class="help"></span>';
$_['entry_alignment']           ='Igazítás: <br><span class="help">Slide kép igazítása</span>';
$_['entry_auto_advance']        ='Automatikus slide váltás: <br><span class="help"></span>';
$_['entry_mobile_auto_advance'] ='Automatikus slide váltás (Mobilon): <br><span class="help"></span>';
$_['entry_bar_direction']       ='Sáv Irány: <br><span class="help"></span>';
$_['entry_cols']                ='Oszlopok: <br><span class="help"></span>';
$_['entry_rows']                ='Sorok: <br><span class="help"></span>';
$_['entry_easing']              ='Effect módosító: <br><span class="help">Slider effekteket módosítja</span>';
$_['entry_fx']                  ='Effect: <br><span class="help">Több is kiválasztható</span>';
$_['entry_hover']               ='Hover: <br><span class="help">Automatikus sliderváltás megállítása hoverrel</span>';
$_['entry_loader']              ='Töltő: <br><span class="help">Automatikus sliderváltásnál megjelő visszaszámláló</span>';
$_['entry_navigation']          ='Nevigáció: <br><span class="help">igen: prev, next és play/stop gombok megjelennek, nem: prev, next and play/stop gombok nem jelennek meg soha </span>';
$_['entry_navigation_hover']    ='Nevigáció Hover: <br><span class="help">igen: prev, next és play/stop gombok hoverre jelennek meg, nem: prev, next és play/stop gombok folyamatos megjelenítése</span>';
$_['entry_pagination']          ='Lapszámozás: <br><span class="help"></span>';
$_['entry_play_pause']          ='Play&Pause: <br><span class="help">play/pause gombok megjelenítése</span>';
$_['entry_thumbnails']          ='Indexképek: <br><span class="help">Indexképek megjelenítése</span>';
$_['entry_time']                ='Időtartam: <br><span class="help">milliszekundum a slide effect vége és a következő kezdete között</span>';
$_['entry_trans_period']        ='Átmeneti Idő: <br><span class="help">átváltás effect hossza milliszekundumban</span>';
$_['button_add_new_block']      = 'Új Slideshow';
$_['button_save_stay']          = 'Mentés & Maradás';
$_['tab_block']                 = 'Slideshow ';
$_['tab_banner']                = 'Banner ';
$_['entry_product']             = 'Termék kiválasztása: <br/><span class="help">Termék keresés név alapján, vagy beállítás ID alapján.</span>';
$_['entry_product_id']          = '&nbsp;product id: ';
$_['entry_slideshow_effect']    = '<h3>Effect Beállítások</h3>';
$_['entry_caption_alignment']   = 'Felirat Igazítás:<br><span class="help">Felülírja az alapértelmezett Felirat Igazítást.</span>';
$_['entry_image_alignment']     = 'Kép Igazítás:<br><span class="help">Felülírja az alapértelmezett Kép Igazítást.</span>';
$_['entry_effect']              = 'Banner Effect:<br><span class="help">Felülírja az alapértelmezett Banner Effect Igazítást.</span>';


$_['entry_title']                   = 'Cím:<br/><span class="help">Maximum 100 karakter</span> ';
$_['entry_description']             = 'Leírás: ';
$_['entry_link']                    = 'Link: ';
$_['entry_image']                   = 'Kép: ';
$_['entry_custom_position']         = 'Egyedi Pozíció: <br><span class="help">Template egyedi pozíciója, például: slideshow, content_left,...<br/>Az "Egyedi Pozíció" üresre állítva az Opencart Alapértelmezett pozíciókat használja.</span>';
$_['entry_limit']                   = 'Slider elem limit: ';
$_['entry_title_max_chars']         = 'Cím Maximális Karakterszám: ';
$_['entry_description_max_chars']   = 'Leírás Maximális Karakterszám: ';
$_['entry_slide_height']            = 'Slider Magasság:<br/><span class="help"></span> ';
$_['entry_min_height']              = 'Minimum Magasság:<br/><span class="help"></span> ';
$_['entry_show_product_info']       = 'Termék információk megjelenítése:<br/><span class="help">Mutat/Rejt termék ár, kritikák, kosárba gomb</span>';
$_['column_name']                   = 'Név';
$_['column_status']                 = 'Állapot';
$_['column_action']                 = 'Akció';

// Error
$_['error_permission']        = 'Figyelem: Önnek nincs jogosultsága módosítani a Responsive Slideshow modult!';
?>