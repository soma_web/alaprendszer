<?php
class ModelLocalisationCountry extends Model {
	public function getCountry($country_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "' AND status = '1'");
		
		return $query->row;
	}	
	
	public function getCountries() {
		$country_data = $this->cache->get('country.status');
		
		if (!$country_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE status = '1' ORDER BY name ASC");
	
			$country_data = $query->rows;
		
			$this->cache->set('country.status', $country_data);
		}

        foreach($country_data as $key=>$value){
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $country_data);

        return $country_data;
	}

    public function getEletkor() {
        $eletkors = $this->config->get('eletkor');

        if ($eletkors) {
            foreach($eletkors as $key=>$value){
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $eletkors);

            return $eletkors;
        } else {
            return false;
        }

    }
}
?>