$(document).ready(function(){
    /* Mindent kijelöl (Fenti) */
    $("input[name='check_all']").change(function(){
        if($(this).is(':checked')){
            $("input[type='checkbox']").prop("checked","checked");
        }else{
            $("input[type='checkbox']").removeAttr("checked");
            $("input[name='check_all']").removeAttr("checked");
        }
    });
    /*  */
    $("input[type='checkbox']:not(.check_all_box)").change(function(){
        if($(this).attr("name") != "check_all"){
            var inputs = $(this).parent("td").parent("tr").parent("tbody").find("input[type='checkbox']");
            if(!checked_all(inputs)){
                $(".check_all input[type='checkbox']").removeAttr("checked");
                $(this).parent("td").parent("tr").parent("tbody").parent("table").parent(".tablak").find(".check_all_box").removeAttr("checked");
            }
            else{
                $(this).parent("td").parent("tr").parent("tbody").parent("table").parent(".tablak").find(".check_all_box").prop("checked","checked");
                var all_inputs = $("input[type='checkbox']:not(.check_all_box):not([name='check_all'])");
                if(checked_all(all_inputs)){
                    $("input[name='check_all']").prop("checked","checked");
                }
            }
        }
    });
    /* Mindent kijelöl a dobozban vagy semmit */
    $(".check_all_box").bind("click",function(){
        //debugger;
       var inputs = $(this).parent("div").parent(".tabla_select").parent(".tablak").find("input[type='checkbox']");
        if($(this).is(":checked")){
            inputs.prop("checked","checked");
        }
        else{
            inputs.removeAttr("checked");
        }
    });
});
function checked_all(inputs){
    var mind_kijelolve = true;
    for(var i = 0; i < inputs.length; i++){
        if(!inputs.eq(i).is(":checked")){
            mind_kijelolve = false;
        }
    }
    return mind_kijelolve;
}
