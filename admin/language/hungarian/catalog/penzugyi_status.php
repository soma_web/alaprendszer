<?php

// Heading
$_['heading_title']       = 'Pénzügyi státusz';
$_['button_insert']       = 'Beszúrás';
$_['button_delete']       = 'Törlés';
$_['error_warning']       = '';
$_['success']             = '';
$_['column_megnevezes']   = 'Pénzügyi státusz';
$_['column_sort_order']   = 'Sorrend';
$_['column_action']       = 'Művelet';
$_['column_status']       = 'Státusz';


$_['entry_status']          = 'Státusz:';
$_['entry_sorrend']         = 'Sorrend:';
$_['entry_teljesitett']     = 'Fizetett állapot:';
$_['entry_teljesitett_lista'] = ' [Fizetett állapot]';

$_['entry_alapertelmezett_lista'] = ' [Alapértelmezett állapot]';
$_['entry_alapertelmezett'] = 'Alapértelmezett állapot:';

$_['text_engedelyezett']    = 'Engedélyezett';
$_['text_letiltott']        = 'Letiltott';

?>