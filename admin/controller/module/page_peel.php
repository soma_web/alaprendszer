<?php
class ControllerModulePagePeel extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/page_peel');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {			
			$this->model_setting_setting->editSetting('page_peel', $this->request->post);		
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		
		$this->data['entry_small_image']  = $this->language->get('entry_small_image');
		$this->data['entry_big_image']    = $this->language->get('entry_big_image');
		$this->data['entry_link']         = $this->language->get('entry_link');
		$this->data['entry_new_window']   = $this->language->get('entry_new_window');
		$this->data['entry_activate_link']= $this->language->get('entry_activate_link');
		$this->data['entry_layout']       = $this->language->get('entry_layout');
		$this->data['entry_position']     = $this->language->get('entry_position');
		$this->data['entry_status']       = $this->language->get('entry_status');
		$this->data['entry_sort_order']   = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
		$this->load->model('tool/image');
		
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['small_image'])) {
			$this->data['error_small_image'] = $this->error['small_image'];
		} else {
			$this->data['error_small_image'] = array();
		}
		
		if (isset($this->error['big_image'])) {
			$this->data['error_big_image'] = $this->error['big_image'];
		} else {
			$this->data['error_big_image'] = array();
		}
				
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/page_peel', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/page_peel', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];	
			
		$this->data['modules'] = array();
		
		if (isset($this->request->post['page_peel_module'])) {
			$modules = $this->request->post['page_peel_module'];
		} elseif ($this->config->get('page_peel_module')) {
			$modules = $this->config->get('page_peel_module');
		}  else {
			$modules = array();
		}

		foreach($modules as $module){
			if ($module['small_image']){
				$thumb_small_image = $this->model_tool_image->resize($module['small_image'],100,100);
			}else {
				$thumb_small_image = $this->model_tool_image->resize('no_image.jpg',100,100);
			}
			$module['thumb_small_image'] = $thumb_small_image;
			
			if ($module['big_image']){
				$thumb_big_image = $this->model_tool_image->resize($module['big_image'],100,100);
			}else {
				$thumb_big_image = $this->model_tool_image->resize('no_image.jpg',100,100);
			}
			$module['thumb_big_image'] = $thumb_big_image;
			
			$this->data['modules'][] = $module; 	
		}
				
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/page_peel.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/page_peel')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->request->post['page_peel_module'])) {
			foreach ($this->request->post['page_peel_module'] as $key => $value) {
				if (!utf8_strlen($value['small_image'])) {
					$this->error['small_image'][$key] = $this->language->get('error_small_image');
				}
				
				if (!utf8_strlen($value['big_image'])) {
					$this->error['big_image'][$key] = $this->language->get('error_big_image');
				}
			}
		}
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>