<?php
class ControllerModuleTgheliosHtml extends Controller {

	private $_name = 'tg_helios_html';


     protected function index($setting) {
		$this->load->model('localisation/language');
		$this->language->load('module/tg_helios_html');
		
		$languages = $this->model_localisation_language->getLanguages();
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['code1'] = html_entity_decode($this->config->get($this->_name . '_code1_' . $this->config->get('config_language_id')));
		$this->data['code2'] = html_entity_decode($this->config->get($this->_name . '_code2_' . $this->config->get('config_language_id')));
		$this->data['code3'] = html_entity_decode($this->config->get($this->_name . '_code3_' . $this->config->get('config_language_id')));
		$this->data['code4'] = html_entity_decode($this->config->get($this->_name . '_code4_' . $this->config->get('config_language_id')));


		
		
		$this->data['code'] = ''; $this->data['title'] = ''; $this->data['borderless'] = 0;



		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/'.$this->_name.'.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/'.$this->_name.'.tpl';
		} else {
			$this->template = 'default/template/module/'.$this->_name.'.tpl';
		}

		$this->render();
	}
}
?>
