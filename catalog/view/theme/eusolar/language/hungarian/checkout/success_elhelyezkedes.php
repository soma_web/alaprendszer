<?php

// Heading
$_['heading_title']	= 'Rendelése feldolgozva!';

// Text
$_['text_customer_payu']	= '<p>A kiemelés / elhelyezkedéssel kapcsolator rendelését sikeresen felvettük! Elbírálás után a kiemelést / elhelyezkedést teljesítjük.';
$_['text_customer']	= '<p>A kiemelés / elhelyezkedéssel kapcsolator rendelését sikeresen felvettük! Az összeg beérkezése után a kiemelést / elhelyezkedést teljesítjük.';
$_['text_termek']	= 'Termékeim';
$_['text_transaction_success_title']	= 'Sikeres rendelés';

?>