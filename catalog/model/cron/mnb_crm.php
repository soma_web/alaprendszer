<?php
class ModelCronMnbCrm extends Model {

    public function getMnb($data) {

        $vissza = array();

        $client = new SoapClient("http://www.mnb.hu/arfolyamok.asmx?wsdl");
        $response = $client->__soapCall("GetCurrentExchangeRates", array());

        $doc = new DOMDocument;
        $doc->loadXML($response->GetCurrentExchangeRatesResult);

        $xpath = new DOMXPath($doc);

        foreach($data as $currency) {
            $query = "//MNBCurrentExchangeRates/Day/Rate[@curr='$currency']";
            $entries = $xpath->query($query);
            if($entries->length){
                $vissza[$currency] = $entries->item(0)->nodeValue;
            }
        }
        return $vissza;
    }

    public function addCurrency($data) {
        foreach($data as $key=>$value) {
            $conversion = 1 / ($value);
            $sql = "UPDATE vtiger_currency_info SET conversion_rate = '".$conversion."' WHERE currency_code LIKE '".$key."'";
            $siker = $this->db_crm->query($sql);
        }
    }
}