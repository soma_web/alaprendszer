<?php if (count($currencies) > 1) { ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <div id="currency"><?php //echo $text_currency; ?>
     <?php foreach ($currencies as $currency) { ?>

     <?php /* Az aktuálisan kijelölt pénznem kiírása */ ?>

        <?php if ($currency['code'] == $currency_code) { ?>
           <?php if (isset($megjelenit_altalanos['penznem_hosszu_kiiras']) && ($megjelenit_altalanos['penznem_hosszu_kiiras']) && $currency['title']) { ?>
                 <a title="<?php echo $currency['title']; ?>"><b><?php echo $currency['title']; ?></b></a>
           <?php } else if ($currency['symbol_left']) { ?>
                 <a title="<?php echo $currency['title']; ?>"><b><?php echo $currency['symbol_left']; ?></b></a>
           <?php } else { ?>
                 <a title="<?php echo $currency['title']; ?>"><b><?php echo $currency['symbol_right']; ?></b></a>
           <?php } ?>

     <?php /* A nem kijelölt pénznemek kiírása linkként */ ?>

        <?php } else { ?>
           <?php if (isset($megjelenit_altalanos['penznem_hosszu_kiiras']) && ($megjelenit_altalanos['penznem_hosszu_kiiras']) && $currency['title']) { ?>
                  <a title="<?php echo $currency['title']; ?>" onClick="$('input[name=\'currency_code\']').attr('value', '<?php echo $currency['code']; ?>').submit(); $(this).parent().parent().submit();"><?php echo $currency['title']; ?></a>
           <?php } else if ($currency['symbol_left']) { ?>
                  <a title="<?php echo $currency['title']; ?>" onClick="$('input[name=\'currency_code\']').attr('value', '<?php echo $currency['code']; ?>').submit(); $(this).parent().parent().submit();"><?php echo $currency['symbol_left']; ?></a>
           <?php } else { ?>
                  <a title="<?php echo $currency['title']; ?>" onClick="$('input[name=\'currency_code\']').attr('value', '<?php echo $currency['code']; ?>').submit(); $(this).parent().parent().submit();"><?php echo $currency['symbol_right']; ?></a>
           <?php } ?>
        <?php } ?>
     <?php } ?>

     <input type="hidden" name="currency_code" value="" />
     <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
  </div>
</form>
<?php } ?>


