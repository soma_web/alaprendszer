<?php

$_['upper_div']          = array(
    'type'              => 'text',
    'name'              => 'upper_div',
    'default'           => "",
    'size'              => "10",
    'heading'           => 'Felső div class'
);

$_['fix_div']          = array(
    'type'              => 'text',
    'name'              => 'fix_div',
    'default'           => "",
    'size'              => "10",
    'heading'           => 'Fix div class'
);

?>