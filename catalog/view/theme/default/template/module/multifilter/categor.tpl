<?php if(isset($fo_categoria_marad) && $fo_categoria_marad){ ?>

    <div class="box" style="">

        <div class="box-heading reszek elso-filter"><?php echo $heading_title_category; ?>
            <?php if(isset($arrow_opened) && !empty($arrow_opened)) { ?>
                <img id="fokategoria_heading" src="<?php echo $arrow_opened; ?>">
            <?php } ?>
        </div>

        <div id="mf-category" class="box-content">
            <ul class="box-filter box-filter-fo_categorys">
                <?php foreach ($fo_categoria_marad as $category) { ?>
                <li  class="menu-text-image-2">
                    <?php if ($category['category_id'] == $category_id) { ?>
                        <a href="<?php echo $category['href']; ?>" class="active">  <?php echo $category['name']; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo $category['href']; ?>" >  <?php echo $category['name']; ?></a>
                    <?php }
                    category_children($category,$parts);
                    ?>
                <?php } ?>
            </ul>
        </div>
    </div>

    <script>
        $("#fokategoria_heading").bind("click",function(){
            if ( $("#mf-category").css("display") == "none") {
                $("#mf-category").slideDown(600);
            } else {
                $("#mf-category").slideUp(600);
            }
        });
    </script>

<?php } ?>


