<?php 
class ControllerProductMultifilter extends Controller {
	public function index() { 
		$this->language->load('product/multifilter');

        $this->load->model('catalog/multi_szurok');
        $this->load->model('catalog/multi_kozosites');
		$this->load->model('catalog/multifilter');
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');
        $megjelenit_product = $this->config->get('megjelenit_product');

        $this->load->model('tool/image');
        $this->data['module_name'] = "category";





        $config_sort_order = $this->config->get('config_sort_order');
        if(isset($config_sort_order) && !empty($config_sort_order)) {
            $config_sort_orders = explode('-', $config_sort_order);
        }

        if (isset($this->request->request['sort'])) {
            $sort = $this->request->request['sort'];
        } else {
            if(isset($config_sort_order) && !empty($config_sort_order) && ($megjelenit_product['product_cikkszam_termeknevben'] == 0)) {
                $sort = $config_sort_orders[0];
            } elseif (isset($megjelenit_product['product_cikkszam_termeknevben']) && $megjelenit_product['product_cikkszam_termeknevben']) {
                $sort = 'p.model';
            } else {
                $sort = 'p.sort_order';
            }
        }

        if (isset($this->request->request['order'])) {
            $order = $this->request->request['order'];
        } else {
            if(isset($config_sort_order) && !empty($config_sort_order) && !isset($megjelenit_product['product_cikkszam_termeknevben'])) {
                $order = $config_sort_orders[1];
            } else {
                $order = 'ASC';
            }
        }
		
		if (isset($this->request->request['page'])) {
			$page = $this->request->request['page'];
		} else { 
			$page = 1;
		}	
							
		if (isset($this->request->request['limit'])) {
			$limit = $this->request->request['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}


        $path                       = isset($this->request->request['path']) &&  !empty($this->request->request['path']) ? $this->request->request['path'] : "";
        $filter                     = isset($this->request->request['filter']) ? $this->request->request['filter'] : "";
        $filter_varos               = isset($this->request->request['filter_varos']) ? $this->request->request['filter_varos'] : "";
        $filter_artol               = isset($this->request->request['filter_artol']) ? $this->request->request['filter_artol'] : "";
        $filter_arig                = isset($this->request->request['filter_arig']) ? $this->request->request['filter_arig'] : "";
        $filter_ar_tol_ig           = isset($this->request->request['filter_ar_tol_ig']) ? $this->request->request['filter_ar_tol_ig'] : "";
        $filter_arszazalek          = isset($this->request->request['filter_arszazalek']) ? $this->request->request['filter_arszazalek'] : "";
        $filter_uzlet               = isset($this->request->request['filter_uzlet']) ? $this->request->request['filter_uzlet'] : "";
        $filter_tulajdonsagok       = isset($this->request->request['filter_tulajdonsagok']) ? $this->request->request['filter_tulajdonsagok'] : "";
        $filter_tulajdonsag_csuszka = isset($this->request->request['filter_tulajdonsag_csuszka']) ? $this->request->request['filter_tulajdonsag_csuszka'] : "";
        $filter_valasztek           = isset($this->request->request['filter_valasztek']) ? $this->request->request['filter_valasztek'] : "";
        $filter_gyarto              = isset($this->request->request['filter_gyarto']) ? $this->request->request['filter_gyarto'] : "";
        $filter_meret               = isset($this->request->request['filter_meret']) ? $this->request->request['filter_meret'] : "";
        $filter_szin                = isset($this->request->request['filter_szin']) ? $this->request->request['filter_szin'] : "";
        $filter_raktaron            = isset($this->request->request['filter_raktaron']) ? $this->request->request['filter_raktaron'] : "";

        if ( isset($this->request->request['esemeny_kivalto']) && $this->request->request['esemeny_kivalto'] == 'szurok_torlese') {

            if (isset($this->session->data['ar_atszamol'])) {
                unset ($this->session->data['ar_atszamol']);
            }
        }

            /*if ($path) $this->session->data['path'] = $path;
            elseif (isset($this->session->data['path'])) unset($this->session->data['path']);

            if ($filter) $this->session->data['filter'] = $filter;
            elseif (isset($this->session->data['filter'])) unset($this->session->data['filter']);

            if ($filter_varos)          $this->session->data['filter_varos']        = $filter_varos;
            elseif (isset($this->session->data['filter_varos'])) unset($this->session->data['filter_varos']);

            if ($filter_artol)          $this->session->data['filter_artol']        = $filter_artol;
            elseif (isset($this->session->data['filter_artol'])) unset($this->session->data['filter_artol']);

            if ($filter_arig)           $this->session->data['filter_arig']         = $filter_arig;
            elseif (isset($this->session->data['filter_arig'])) unset($this->session->data['filter_arig']);

            if ($filter_ar_tol_ig)      $this->session->data['filter_ar_tol_ig']    = $filter_ar_tol_ig;
            elseif (isset($this->session->data['filter_ar_tol_ig'])) unset($this->session->data['filter_ar_tol_ig']);

            if ($filter_arszazalek)     $this->session->data['filter_arszazalek']   = $filter_arszazalek;
            elseif (isset($this->session->data['filter_arszazalek'])) unset($this->session->data['filter_arszazalek']);

            if ($filter_uzlet)          $this->session->data['filter_uzlet']        = $filter_uzlet;
            elseif (isset($this->session->data['filter_uzlet'])) unset($this->session->data['filter_uzlet']);

            if ($filter_tulajdonsagok)  $this->session->data['filter_tulajdonsagok']= $filter_tulajdonsagok;
            elseif (isset($this->session->data['filter_tulajdonsagok'])) unset($this->session->data['filter_tulajdonsagok']);

            if ($filter_tulajdonsag_csuszka)  $this->session->data['filter_tulajdonsag_csuszka']= $filter_tulajdonsag_csuszka;
                elseif (isset($this->session->data['filter_tulajdonsag_csuszka'])) unset($this->session->data['filter_tulajdonsag_csuszka']);

            if ($filter_valasztek)      $this->session->data['filter_valasztek']    = $filter_valasztek;
            elseif (isset($this->session->data['filter_valasztek'])) unset($this->session->data['filter_valasztek']);

            if ($filter_gyarto)         $this->session->data['filter_gyarto']       = $filter_gyarto;
            elseif (isset($this->session->data['filter_gyarto'])) unset($this->session->data['filter_gyarto']);

            if ($filter_meret)         $this->session->data['filter_meret']       = $filter_meret;
            elseif (isset($this->session->data['filter_meret'])) unset($this->session->data['filter_meret']);


            if ($filter_raktaron)       $this->session->data['filter_raktaron']     = $filter_raktaron;
            elseif (isset($this->session->data['filter_raktaron'])) unset($this->session->data['filter_raktaron']);*/


        if ($path) {
            $kategoriak = explode(",",$this->request->request['path']);
            if ( in_array($this->request->request['path'],$kategoriak ) ) {
                $this->model_catalog_multifilter->setCategoryView($this->request->request['path']);
            }
        }

        $this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
       		'separator' => false
   		);

        $this->data['text_eredeti_ar']      = $this->language->get('text_eredeti_ar');
        $this->data['text_cikkszam']        = $this->language->get('text_cikkszam');
        $this->data['text_stock_in']        = $this->language->get('text_stock_in');
        $this->data['kaphato']              = $this->language->get('kaphato');
        $this->data['text_refine']          = $this->language->get('text_refine');
        $this->data['text_empty']           = $this->language->get('text_empty');
        $this->data['text_quantity']        = $this->language->get('text_quantity');
        $this->data['text_manufacturer']    = $this->language->get('text_manufacturer');
        $this->data['text_model']           = $this->language->get('text_model');
        $this->data['text_price']           = $this->language->get('text_price');
        $this->data['text_tax']             = $this->language->get('text_tax');
        $this->data['text_netto']           = $this->language->get('text_netto');
        $this->data['text_points']          = $this->language->get('text_points');
        $this->data['text_compare']         = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
        $this->data['text_display']         = $this->language->get('text_display');
        $this->data['text_list']            = $this->language->get('text_list');
        $this->data['text_grid']            = $this->language->get('text_grid');
        $this->data['text_sort']            = $this->language->get('text_sort');
        $this->data['text_limit']           = $this->language->get('text_limit');
        $this->data['text_raktar']          = $this->language->get('text_raktar');

        $this->data['text_nincs_raktaron']  = $this->language->get('text_nincs_raktaron');
        $this->data['text_raktaron']        = $this->language->get('text_raktaron');
        $this->data['text_compare']         = $this->language->get('text_compare');
        $this->data['text_wishlist']        = $this->language->get('text_wishlist');
        $this->data['text_kifuto_title']    = $this->language->get('text_kifuto_title');
        $this->data['text_mennyisegi_title']= $this->language->get('text_mennyisegi_title');

        $this->data['button_arajanlat']     = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem']     = $this->language->get('button_megrendelem');
        $this->data['button_arajanlat_keszites'] = $this->language->get('button_arajanlat_keszites');
        $this->data['button_cart']          = $this->language->get('button_cart');
        $this->data['button_set_valos_ar']  = $this->language->get('button_set_valos_ar');
        $this->data['button_bovebben']      = $this->language->get('button_bovebben');
        $this->data['button_wishlist']      = $this->language->get('button_wishlist');
        $this->data['button_compare']       = $this->language->get('button_compare');
        $this->data['button_continue']      = $this->language->get('button_continue');



        $this->data['text_akcio_title']     = $this->language->get('text_akcio_title');
        $this->data['text_pardarab_title']  = $this->language->get('text_pardarab_title');
        $this->data['text_uj_title']        = $this->language->get('text_uj_title');
        $this->data['text_ujdonsag_title']  = $this->language->get('text_ujdonsag_title');
        $this->data['text_ajandek_title']   = $this->language->get('text_ajandek_title');

        $this->data['thumb'] = false;
        $this->data['description'] = false;

        if ($this->config->get('megjelenit_category_description') == 1){
            $this->data['description_show'] = true;
        } else {
            $this->data['description_show'] = false;
        }
        if ($this->config->get('megjelenit_category_header_image') == 1 || $this->config->get('megjelenit_category_header_image_above') == 1){
            $this->data['image_show'] = true;
            $this->data['image_above_header'] = true;
            if ($this->config->get('megjelenit_category_header_image') == 1) {
                $this->data['image_above_header'] = false;
            }
        } else {
            $this->data['image_show'] = false;
        }



        $this->data['compare'] = $this->url->link('product/compare');
        $this->data['categories'] = array();



        if (isset($this->request->request['filter']) && $this->request->request['filter'] != null) {
            $kategoriak = explode(",",$this->request->request['filter']);

            foreach($kategoriak as $kategoria){
                $url1 = '&filter='.$kategoria;

                $url1 .= '&filter_varos=&filter_tulajdonsagok=&filter_valasztek=&filter_gyarto=&filter_meret=&filter_szin=&filter_raktaron=&filter_uzlet="';
                $url1 .= '&filter_arig=0&filter_artol=0&filter_ar_tol_ig=0&filter_arszazalek=0filter_tulajdonsag_csuszka=&path=&multiszuro=1&ajax_hivas=1';

                if (isset($_SESSION['categoryAll'])) {
                    $category_info = $this->config->in_array_category($_SESSION['categoryAll'],$kategoria);
                } else {
                    $category_info = $this->model_catalog_category->getCategory($kategoria);
                }

                $this->data['breadcrumbs'][] = array(
                    'text'      => $category_info['name'],
                    'href'        => $this->url->link('product/multifilter', $url1),
                    'separator' => $this->language->get('text_separator')
                );
            }
        }

        $beallitasok = $this->config->get('multifilter_beallitasok');
        $beallitasok = $this->config->rendezes($beallitasok, "sort_order", "ASC", false, "ASC", false, "ASC", false, "ASC", false);

        foreach($beallitasok as $key=>$beallitas) {

            if ($key == 'szuro' && $beallitas['value'] == 1) {
                $breadcrumbs_info = $this->model_catalog_multifilter->getBreadcrumbs('filter_uzlet');
                if ($breadcrumbs_info) {
                    foreach($breadcrumbs_info as $value) {
                        $this->data['breadcrumbs'][] = $value;
                    }
                }
            }
            if ($key == 'szuro_csoport' && $beallitas['value'] == 1) {
                $breadcrumbs_info = $this->model_catalog_multifilter->getBreadcrumbs('filter_varos');
                if ($breadcrumbs_info) {
                    foreach($breadcrumbs_info as $value) {
                        $this->data['breadcrumbs'][] = $value;
                    }
                }
            }
            if ($key == 'kategoria_checkbox' && $beallitas['value'] == 1) {
                true;
            }
            if ( ($key == 'kategoria_sub' && $beallitas['value'] == 1) || ($key == 'kategoria_normal' && $beallitas['value'] == 1) ) {

                if (isset($this->request->request['path']) && $this->request->request['path'] != null) {
                    $kategoriak = explode("_",$this->request->request['path']);

                    $url1 = "";
                    foreach($kategoriak as $keykategory=>$kategoria){
                        $url1 = '&path='.$kategoriak[0];

                        for ($i=1; $i<=$keykategory; $i++){
                            $url1 .= "_".$kategoriak[$i];
                        }

                        if (isset($_SESSION['categoryAll'])) {
                            $category_info = $this->config->in_array_category($_SESSION['categoryAll'],$kategoria);
                        } else {
                            $category_info = $this->model_catalog_category->getCategory($kategoria);
                        }
                        if($category_info) {
                            $this->data['breadcrumbs'][] = array(
                                'text'      => $category_info['name'],
                                'href'        => $this->url->link('product/multifilter', $url1),
                                'separator' => $this->language->get('text_separator')
                            );
                        }
                    }
                }
            }

            if ($key == 'gyarto' && $beallitas['value'] == 1) {
                $breadcrumbs_info = $this->model_catalog_multifilter->getBreadcrumbs('filter_gyarto');
                if ($breadcrumbs_info) {
                    foreach($breadcrumbs_info as $value) {
                        $this->data['breadcrumbs'][] = $value;
                    }
                }
            }

            if ($key == 'meret' && $beallitas['value'] == 1) {
                $breadcrumbs_info = $this->model_catalog_multifilter->getBreadcrumbs('filter_meret');
                if ($breadcrumbs_info) {
                    foreach($breadcrumbs_info as $value) {
                        $this->data['breadcrumbs'][] = $value;
                    }
                }
            }

            if ($key == 'szin' && $beallitas['value'] == 1) {
                $breadcrumbs_info = $this->model_catalog_multifilter->getBreadcrumbs('filter_szin');
                if ($breadcrumbs_info) {
                    foreach($breadcrumbs_info as $value) {
                        $this->data['breadcrumbs'][] = $value;
                    }
                }
            }

            if ($key == 'tulajdonsagok' && $beallitas['value'] == 1) {
                $breadcrumbs_info = $this->model_catalog_multifilter->getBreadcrumbs('filter_tulajdonsagok');
                if ($breadcrumbs_info) {
                    foreach($breadcrumbs_info as $value) {
                        $this->data['breadcrumbs'][] = $value;
                    }
                }
            }
            if ($key == 'valasztek' && $beallitas['value'] == 1) {
                $breadcrumbs_info = $this->model_catalog_multifilter->getBreadcrumbs('filter_valasztek');
                if ($breadcrumbs_info) {
                    foreach($breadcrumbs_info as $value) {
                        $this->data['breadcrumbs'][] = $value;
                    }
                }
            }
        }

        $this->data['mezok'] = $this->config->get("megjelenit_admin_product");
        $elhelyezkedes = $this->data['mezok']['elhelyezkedes'] == 1 ? true : false;

        if ($beallitasok['tulajdonsagok']['value'] == 1 ) {
            $szukit_bovit = !empty($filter_tulajdonsagok) ? $this->model_catalog_multifilter->tulajdonsagSzukitBovitLeosztas($filter_tulajdonsagok) : array();
            $vezerel_kategoriat_products  =   !empty($szukit_bovit['vezerel_kategoriat']) ? $this->model_catalog_multi_kozosites->getTulajdonsagokProducts($szukit_bovit['vezerel_kategoriat']) : false;
            $vezerel_kategoria = $vezerel_kategoriat_products ? $this->model_catalog_multi_szurok->veglegesAlKategoria(0,$vezerel_kategoriat_products) : false;
            if ($vezerel_kategoria && false) {
                $kategoria_kigyujtve = array();
                foreach($vezerel_kategoria as $kategoria) {
                    $kategoria_kigyujtve[] = $kategoria['category_id'];
                }
                $path = implode('_',$kategoria_kigyujtve);
                $this->request->request['path'] = $path;
                $this->request->get['path'] = $path;
                $_REQUEST['path'] = $path;
                $_GET['path'] = $path;

            }


            if (!empty($filter_tulajdonsagok) ) {
                $filter_tulajdonsagok_csoportositva = $this->model_catalog_multifilter->tulajdonsagCsoportok($filter_tulajdonsagok);
            }
        }


        $data = array(
            'filter_category'               => !empty($filter) ? $filter : implode(',',explode("_",$path)),
            'filter_varos'                  => $filter_varos,
            'filter_uzlet'                  => $filter_uzlet,
            'filter_artol'                  => $filter_artol,
            'filter_arig'                   => $filter_arig,
            'filter_ar_tol_ig'              => $filter_ar_tol_ig,
            'filter_arszazalek'             => $filter_arszazalek,
            'filter_tulajdonsagok'          => !empty($filter_tulajdonsagok_csoportositva) ? $filter_tulajdonsagok_csoportositva : $filter_tulajdonsagok,
            'filter_tulajdonsag_csuszka'    => $filter_tulajdonsag_csuszka,
            'filter_valasztek'              => $filter_valasztek,
            'filter_gyarto'                 => $filter_gyarto,
            'filter_meret'                  => $filter_meret,
            'filter_szin'                   => $filter_szin,
            'filter_raktaron'               => $filter_raktaron,
            'sort'                          => $sort,
            'order'                         => $order,
            'start'                         => ($page - 1) * $limit,
            'elhelyezkedes'                 => $elhelyezkedes,
            'limit'                         => $limit
        );

        $product_total = $this->model_catalog_multifilter->getMultiProducts($data,true);
        $results = $this->model_catalog_multifilter->getMultiProducts($data);


        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => 10
        );
        $results1 = $this->model_catalog_product->getProductsNew($data1);

        $setting['image_width']     = $this->config->get('config_image_product_width');
        $setting['image_height']    = $this->config->get('config_image_product_height');

        $this->data['height'] = $this->config->get('config_image_product_height')."px";
        $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
        $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');

        if ($results){
			foreach ($results as $result) {
                $special = $this->cart->productPrice($result['product_id']);
                $result['special'] = $special < $result['price'] ? $special : 0;

                $this->data['products'][] = $this->product->productPreparation($result,$setting,$results1);
			}



            $url = $this->urlSzuro("sort","order");

            $this->data['sorts'] = array();

            $this->data['sorts'][] = array(
                'text'  => $this->language->get('text_default'),
                'value' => 'p.sort_order-ASC',
                'href'  => $this->url->link('product/multifilter', 'path=' . $path . '&sort=p.sort_order&order=ASC&lista=1' . $url)
            );

            $this->data['sorts'][] = array(
                'text'  => $this->language->get('text_name_asc'),
                'value' => 'pd.name-ASC',
                'href'  => $this->url->link('product/multifilter', 'path=' . $path . '&sort=pd.name&order=ASC&lista=1' . $url)
            );

            $this->data['sorts'][] = array(
                'text'  => $this->language->get('text_name_desc'),
                'value' => 'pd.name-DESC',
                'href'  => $this->url->link('product/multifilter', 'path=' . $path . '&sort=pd.name&order=DESC&lista=1' . $url)
            );

            $this->data['sorts'][] = array(
                'text'  => $this->language->get('text_price_asc'),
                'value' => 'p.price-ASC',
                'href'  => $this->url->link('product/multifilter', 'path=' . $path . '&sort=p.price&order=ASC&lista=1' . $url)
            );

            $this->data['sorts'][] = array(
                'text'  => $this->language->get('text_price_desc'),
                'value' => 'p.price-DESC',
                'href'  => $this->url->link('product/multifilter', 'path=' . $path . '&sort=p.price&order=DESC&lista=1' . $url)
            );

            if ($this->config->get('config_review_status')) {
                $this->data['sorts'][] = array(
                    'text'  => $this->language->get('text_rating_desc'),
                    'value' => 'rating-DESC',
                    'href'  => $this->url->link('product/multifilter', 'path=' . $path . '&sort=rating&order=DESC&lista=1' . $url)
                );

                $this->data['sorts'][] = array(
                    'text'  => $this->language->get('text_rating_asc'),
                    'value' => 'rating-ASC',
                    'href'  => $this->url->link('product/multifilter', 'path=' . $path . '&sort=rating&order=ASC&lista=1' . $url)
                );
            }

            $this->data['sorts'][] = array(
                'text'  => $this->language->get('text_model_asc'),
                'value' => 'p.model-ASC',
                'href'  => $this->url->link('product/multifilter', 'path=' . $path . '&sort=p.model&order=ASC&lista=1' . $url)
            );

            $this->data['sorts'][] = array(
                'text'  => $this->language->get('text_model_desc'),
                'value' => 'p.model-DESC',
                'href'  => $this->url->link('product/multifilter', 'path=' . $path . '&sort=p.model&order=DESC&lista=1' . $url)
            );



            $url = $this->urlSzuro("limit");


            $this->data['limits'] = array();

            $this->data['limits'] = $this->product->limits('product/multifilter','path=' . $path,$url);



            $url = $this->urlSzuro();


            $pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('product/multifilter', 'path=' . $path . $url . '&limit='.$limit.'&page={page}');

			$this->data['pagination'] = $pagination->render();
		
			$this->data['sort'] = $sort;
			$this->data['order'] = $order;
			$this->data['limit'] = $limit;
		
			$this->data['continue'] = $this->url->link('common/home');
            $this->data['megjelenit_lista'] = $this->config->get('megjelenit_lista');

            if (isset($_REQUEST['piacter']) && $this->config->get("megjelenit_piacter") == 1){
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/piacter.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/product/piacter.tpl';
                } else {
                    $this->template = 'default/template/product/piacter.tpl';
                }
            } else {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/product/category.tpl';
                } else {
                    $this->template = 'default/template/product/category.tpl';
                }
            }

            if (isset($this->request->post['ajax_hivas']) && $this->request->post['ajax_hivas'] == 1) {
                $this->children = array(
                    'common/column_left',
                    'common/column_right',
                    'common/content_top',
                    'common/content_bottom',
                );
            } else {
                $this->children = array(
                    'common/column_left',
                    'common/column_right',
                    'common/content_top',
                    'common/content_bottom',
                    'common/content_header',
                    'common/footer',
                    'common/header'
                );
            }

				
			$this->response->setOutput($this->render());
        } else {
            $url = '';

            if (isset($this->request->request['path'])) {
                $url .= '&path=' . $this->request->request['path'];
            }

            if (isset($this->request->request['sort'])) {
                $url .= '&sort=' . $this->request->request['sort'];
            }

            if (isset($this->request->request['order'])) {
                $url .= '&order=' . $this->request->request['order'];
            }

            if (isset($this->request->request['page'])) {
                $url .= '&page=' . $this->request->request['page'];
            }

            if (isset($this->request->request['limit'])) {
                $url .= '&limit=' . $this->request->request['limit'];
            }

            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_error'),
                'href'      => $this->url->link('product/category', $url),
                'separator' => $this->language->get('text_separator')
            );

            $this->document->setTitle($this->language->get('text_error'));

            $this->data['heading_title'] = $this->language->get('text_error')."";

            $this->data['text_error'] = $this->language->get('text_error');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
            } else {
                $this->template = 'default/template/error/not_found.tpl';
            }

            if (isset($this->request->request['ajax_hivas']) && $this->request->request['ajax_hivas'] == 1) {
                $this->children = array(
                    'common/column_left',
                    'common/column_right',
                    'common/content_top',
                    'common/content_bottom',
                );
            } else {
                $this->children = array(
                    'common/column_left',
                    'common/column_right',
                    'common/content_top',
                    'common/content_bottom',
                    'common/content_header',
                    'common/footer',
                    'common/header'
                );
            }

            $this->response->setOutput($this->render());
        }

  	}


    public function urlSzuro($nemkell1="", $nemklell2="") {

        $url = "";
        $url .= isset($this->request->request['sort'])                  && $nemkell1 != "sort"                 && $nemklell2 != "sort"                   ? '&sort=' . $this->request->request['sort'] : "";
        $url .= isset($this->request->request['order'])                 && $nemkell1 != "order"                && $nemklell2 != "order"                  ? '&order=' . $this->request->request['order'] : "";
        $url .= isset($this->request->request['limit'])                 && $nemkell1 != "limit"                && $nemklell2 != "limit"                  ? '&limit=' . $this->request->request['limit'] : "";
        $url .= isset($this->request->request['filter'])                && $nemkell1 != "filter"               && $nemklell2 != "filter"                 ? '&filter=' . $this->request->request['filter'] : "";
        $url .= isset($this->request->request['filter_varos'])          && $nemkell1 != "filter_varos"         && $nemklell2 != "filter_varos"           ? '&filter_varos=' . $this->request->request['filter_varos'] : "";
        $url .= isset($this->request->request['filter_uzlet'])          && $nemkell1 != "filter_uzlet"         && $nemklell2 != "filter_uzlet"           ? '&filter_uzlet=' . $this->request->request['filter_uzlet'] : "";
        $url .= isset($this->request->request['filter_tulajdonsagok'])  && $nemkell1 != "filter_tulajdonsagok" && $nemklell2 != "filter_tulajdonsagok"   ? '&filter_tulajdonsagok=' . $this->request->request['filter_tulajdonsagok'] : "";
        $url .= isset($this->request->request['filter_tulajdonsag_csuszka'])  && $nemkell1 != "filter_tulajdonsag_csuszka" && $nemklell2 != "filter_tulajdonsag_csuszka"   ? '&filter_tulajdonsag_csuszka=' . $this->request->request['filter_tulajdonsag_csuszka'] : "";
        $url .= isset($this->request->request['filter_valasztek'])      && $nemkell1 != "filter_valasztek"     && $nemklell2 != "filter_valasztek"       ? '&filter_valasztek=' . $this->request->request['filter_valasztek'] : "";
        $url .= isset($this->request->request['filter_gyarto'])         && $nemkell1 != "filter_gyarto"        && $nemklell2 != "filter_gyarto"          ? '&filter_gyarto=' . $this->request->request['filter_gyarto'] : "";
        $url .= isset($this->request->request['filter_meret'])          && $nemkell1 != "filter_meret"         && $nemklell2 != "filter_meret"           ? '&filter_meret=' . $this->request->request['filter_meret'] : "";
        $url .= isset($this->request->request['filter_szin'])           && $nemkell1 != "filter_szin"          && $nemklell2 != "filter_szin"            ? '&filter_szin=' . $this->request->request['filter_szin'] : "";
        $url .= isset($this->request->request['filter_raktaron'])       && $nemkell1 != "filter_raktaron"      && $nemklell2 != "filter_raktaron"        ? '&filter_raktaron=' . $this->request->request['filter_raktaron'] : "";
        $url .= isset($this->request->request['filter_artol'])          && $nemkell1 != "filter_artol"         && $nemklell2 != "filter_artol"           ? '&filter_artol=' . $this->request->request['filter_artol'] : "";
        $url .= isset($this->request->request['filter_ar_tol_ig'])      && $nemkell1 != "filter_ar_tol_ig"     && $nemklell2 != "filter_ar_tol_ig"       ? '&filter_ar_tol_ig=' . $this->request->request['filter_ar_tol_ig'] : "";
        $url .= isset($this->request->request['filter_arig'])           && $nemkell1 != "filter_arig"          && $nemklell2 != "filter_arig"            ? '&filter_arig=' . $this->request->request['filter_arig'] : "";
        $url .= isset($this->request->request['filter_arszazalek'])     && $nemkell1 != "filter_arszazalek"    && $nemklell2 != "filter_arszazalek"      ? '&filter_arszazalek=' . $this->request->request['filter_arszazalek'] : "";

        return $url;
    }



/*
SELECT p.product_id
FROM hungaro_product p
LEFT JOIN hungaro_product_to_category p2c ON (p.product_id = p2c.product_id)

WHERE p2c.category_id IN(20,26,88)


SELECT p.product_id
FROM hungaro_product p
LEFT JOIN hungaro_product_to_category p2c ON (p.product_id = p2c.product_id)

WHERE  p2c.category_id = '88'

*/

}
?>