<?php
class ModelCatalogCategory extends Model {
	public function addCategory($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "category SET
		    parent_id       = '" . (int)$data['parent_id'] . "',
		    piacter         = '" . (isset($data['piacter']) ? (int)$data['piacter'] : 0) . "',
		    `top`           = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "',
		    `column`        = '" . (int)$data['column'] . "',
		    sort_order      = '" . (int)$data['sort_order'] . "',
		    status          = '" . (int)$data['status'] . "',
		    date_modified   = NOW(),
		    date_added      = NOW()");
	
		$category_id = $this->db->getLastId();
		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE category_id = '" . (int)$category_id . "'");
		}

        if (isset($data['image_icon'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET
                image_icon = '" . $this->db->escape(html_entity_decode($data['image_icon'], ENT_QUOTES, 'UTF-8')) . "'
                WHERE category_id = '" . (int)$category_id . "'");
        }
		
		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

        /*if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }*/

		if (isset($data['category_store'])) {
			foreach ($data['category_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['category_layout'])) {
			foreach ($data['category_layout'] as $store_id => $layout) {
				if ($layout['layout_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
				}
			}
		}
						
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
		
		$this->cache->delete('category');
	}



    public function editCategory($category_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "category SET
		    parent_id = '" . (int)$data['parent_id'] . "',
		    `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "',
		    `column` = '" . (int)$data['column'] . "',
		    piacter         = '" . (isset($data['piacter']) ? (int)$data['piacter'] : 0) . "',
		    sort_order = '" . (int)$data['sort_order'] . "',
		    status = '" . (int)$data['status'] . "',
		    date_modified = NOW()

		    WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE category_id = '" . (int)$category_id . "'");
        }

        if (isset($data['image_icon'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET image_icon = '" . $this->db->escape(html_entity_decode($data['image_icon'], ENT_QUOTES, 'UTF-8')) . "' WHERE category_id = '" . (int)$category_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $category_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_path['category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
        }

        /*$this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }*/


        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout) {
                if ($layout['layout_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id. "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('category');
    }

    public function deleteCategory($category_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int)$category_id . "'");

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path WHERE path_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $this->deleteCategory($result['category_id']);
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "'");

        $this->cache->delete('category');
    }

    public function getCategoryFilters($category_id) {
        $category_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_filter_data[] = $result['filter_id'];
        }

        return $category_filter_data;
    }

    // Function to repair any erroneous categories that are not in the category path table.
    public function repairCategories($parent_id = 0) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$parent_id . "'");

        foreach ($query->rows as $category) {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category['category_id'] . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$parent_id . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$category['category_id'] . "', level = '" . (int)$level . "'");

            $this->repairCategories($category['category_id']);
        }
    }

	public function getCategory($category_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR ' &gt; ') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "') AS keyword FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

       // $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "') AS keyword FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
		
		return $query->row;
	}


    public function getCategoriesAutoComp($data) {
        $sql = "SELECT c.category_id AS category_id, GROUP_CONCAT(cd1.name SEPARATOR ' &gt; ') AS name, c.parent_id, c.sort_order FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (c.category_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY c.category_id ORDER BY name";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public  function getCategories($parent_id = 0) {

        $category_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");

        foreach ($query->rows as $result) {
            $category_data[] = array(
                'category_id' => $result['category_id'],
                'name'        => $this->getPath($result['category_id'], $this->config->get('config_language_id')),
                'status'  	  => $result['status'],
                'sort_order'  => $result['sort_order'],
                'parent_id'  => $result['parent_id']
            );

            $category_data = array_merge($category_data, $this->getCategories($result['category_id']));
        }

        return $category_data;
    }
	
	public function getPath($category_id) {
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");
		
		if ($query->row['parent_id']) {
			return $this->getPath($query->row['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $query->row['name'];
		} else {
			return $query->row['name'];
		}
	}
	
	public function getCategoryDescriptions($category_id) {
		$category_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");
		
		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_keyword'     => $result['meta_keyword'],
				'meta_description' => $result['meta_description'],
				'description'      => $result['description']
			);
		}
		
		return $category_description_data;
	}	
	
	public function getCategoryStores($category_id) {
		$category_store_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_store_data[] = $result['store_id'];
		}
		
		return $category_store_data;
	}

	public function getCategoryLayouts($category_id) {
		$category_layout_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");
		
		foreach ($query->rows as $result) {
			$category_layout_data[$result['store_id']] = $result['layout_id'];
		}
		
		return $category_layout_data;
	}
		
	public function getTotalCategories() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category");
		
		return $query->row['total'];
	}	
		
	public function getTotalCategoriesByImageId($image_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category WHERE image_id = '" . (int)$image_id . "'");
		
		return $query->row['total'];
	}

	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}


    public function categoryPath() {
        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "category_path (
            `category_id`	            int(11),
	        `path_id`	                int(11),
	        `level`	                    int(11),
             PRIMARY KEY (`category_id`, `path_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "category_path");


        $sql = "SELECT * FROM " . DB_PREFIX . "category";

        $arr2 = $this->db->query($sql);

        $ketegoria_tree = array();
        foreach ( $arr2->rows as $value ){
            $ketegoria_tree[] = $this->buildTree($value);
        }

        foreach($ketegoria_tree as $value){
            $szint = 0;
            if (isset($value['categoria']['szulo'])) {
                $szint = count($value['categoria']['szulo']);
            }
            $sql = "INSERT INTO " .DB_PREFIX. "category_path set
                category_id = '" .$value['categoria']['kategoria']. "',
                path_id ='" .$value['categoria']['kategoria']. "',
                level ='" .$szint.  "'";
            $this->db->query($sql);

            if (isset($value['categoria']['szulo'])) {
                foreach($value['categoria']['szulo'] as $szulo){
                    $szint--;
                    $sql = "INSERT INTO " .DB_PREFIX. "category_path set
                    category_id = '" .$value['categoria']['kategoria']. "',
                    path_id = '".$szulo."',
                    level ='".$szint."'";
                    $this->db->query($sql);
                }
            }
        }

        //$this->category_to_product_magic_javitas();

    }



    public function buildTree($value) {
        $branch = array();

        $branch['categoria']['kategoria'] = $value['category_id'];
        if ($value['parent_id'] > 0 ) {
            $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$value['parent_id'] . "'";
            $uj_categori_id = $this->db->query($sql);
            $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];
            if ($uj_categori_id->row['parent_id'] > 0 ) {
                $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                $uj_categori_id = $this->db->query($sql);
                $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                if ($uj_categori_id->row['parent_id'] > 0 ) {
                    $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                    $uj_categori_id = $this->db->query($sql);
                    $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                    if ($uj_categori_id->row['parent_id'] > 0 ) {
                        $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                        $uj_categori_id = $this->db->query($sql);
                        $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                        if ($uj_categori_id->row['parent_id'] > 0 ) {
                            $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                            $uj_categori_id = $this->db->query($sql);
                            $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                            if ($uj_categori_id->row['parent_id'] > 0 ) {
                                $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                $uj_categori_id = $this->db->query($sql);
                                $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                if ($uj_categori_id->row['parent_id'] > 0 ) {
                                    $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                    $uj_categori_id = $this->db->query($sql);
                                    $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                    if ($uj_categori_id->row['parent_id'] > 0 ) {
                                        $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                        $uj_categori_id = $this->db->query($sql);
                                        $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                        if ($uj_categori_id->row['parent_id'] > 0 ) {
                                            $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                            $uj_categori_id = $this->db->query($sql);
                                            $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                            if ($uj_categori_id->row['parent_id'] > 0 ) {
                                                $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                                $uj_categori_id = $this->db->query($sql);
                                                $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                                if ($uj_categori_id->row['parent_id'] > 0 ) {
                                                    $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                                    $uj_categori_id = $this->db->query($sql);
                                                    $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                                    if ($uj_categori_id->row['parent_id'] > 0 ) {
                                                        $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                                        $uj_categori_id = $this->db->query($sql);
                                                        $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                                        if ($uj_categori_id->row['parent_id'] > 0 ) {
                                                            $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                                            $uj_categori_id = $this->db->query($sql);
                                                            $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        return $branch;
    }

    public function category_to_product_magic_javitas() {

        $mehet = true;
        $connection_integracio = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
        if (!mysql_select_db('trodimp_trodimp', $connection_integracio)) {
           $mehet = false;
        }

        if ($mehet) {
            $sql = "SELECT * FROM web_termek_kategoria";
            $resource = mysql_query($sql,$connection_integracio);

            if ($resource) {
                if (is_resource($resource)) {
                    $i = 0;
                    mysql_select_db(DB_DATABASE, $connection_integracio);

                    $data = array();
                    $sql = "TRUNCATE TABLE  " .DB_PREFIX. "product_to_category";
                    $this->db->query($sql);

                    while ($result = mysql_fetch_assoc($resource)) {
                        $sql = "SELECT product_id from " .DB_PREFIX. "product where cikkszam LIKE '" .$result['TermekKod'] ."'";
                        $product = $this->db->query($sql);

                        $sql = "SELECT category_id from " .DB_PREFIX. "category where kategoria_kod LIKE '" .$result['Kod'] ."'";
                        $category = $this->db->query($sql);


                        if ($product->num_rows > 0 && $category->num_rows > 0 ) {
                            $sql = "INSERT INTO " .DB_PREFIX. "product_to_category set
                             product_id  = '" . (int)$product->row['product_id'] . "',
                              category_id = '" . (int)$category->row['category_id'] . "'";
                            $this->db->query($sql);
                        }


                    }

                    mysql_free_result($resource);

                }
            }


        }
    }

    public function getCategoriesAll() {
        $sql = "SELECT * FROM " . DB_PREFIX . "category c
            LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
            LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)";

        $sql .= "WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
		        AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1'
		        ORDER BY c.category_id, LCASE(cd.name)";

        $query = $this->db->query($sql);

        if ($query->rows) {
            foreach($query->rows as $key=>$value) {
                $sql  = "SELECT COUNT(DISTINCT p2c.product_id) AS total FROM ".DB_PREFIX."product_to_category p2c ";
                $sql .= " INNER JOIN ".DB_PREFIX."product p ON (p2c.product_id=p.product_id AND p.status=1)";
                $sql .= " WHERE p2c.category_id='".$value['category_id']."'";
                $query_product = $this->db->query($sql);
                if ($query_product->row) {
                    $query->rows[$key]['product_total'] = $query_product->row['total'];
                } else {
                    $query->rows[$key]['product_total'] = 0;

                }
            }
        }
        return $query->rows;
    }

    public function buildTree2(array $elements, $parentId = 0, $path) {

        $this->load->model('tool/image');

        $branch = array();
        foreach ($elements as $element) {

            if ($element['parent_id'] == $parentId) {


                $element['href'] =  $this->url->link('product/category', 'path=' . $path.$element['category_id'] );
                $children = $this->buildTree2($elements, $element['category_id'], $path.$element['category_id']."_");
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    public function onlyFindProduct($categoryes) {

        $tomb = array();
        foreach($categoryes as $key=>$value) {
            if (empty($value['product_total']) && empty($value['product_id'])) {
                $tomb[$key] = 1;
            }

            if (!empty($value['children'])) {
                foreach($value['children'] as $key1=>$value1) {
                    if (empty($value1['product_total']) && empty($value1['product_id'])) {
                        $tomb[$key.'_'.$key1] = 1;

                    } else {
                        unset ($tomb[$key]);
                    }

                    if (!empty($value1['children'])) {
                        foreach($value1['children'] as $key2=>$value2) {
                            if (empty($value2['product_total']) && empty($value2['product_id'])) {
                                $tomb[$key.'_'.$key1.'_'.$key2] = 1;

                            } else {
                                unset ($tomb[$key]);
                                unset ($tomb[$key.'_'.$key1]);
                            }

                            if (!empty($value2['children'])) {
                                foreach($value2['children'] as $key3=>$value3) {
                                    if (empty($value3['product_total']) && empty($value3['product_id'])) {
                                        $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3] = 1;

                                    } else {
                                        unset ($tomb[$key]);
                                        unset ($tomb[$key.'_'.$key1]);
                                        unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                    }

                                    if (!empty($value3['children'])) {
                                        foreach($value3['children'] as $key4=>$value4) {
                                            if (empty($value4['product_total']) && empty($value4['product_id'])) {
                                                $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4] = 1;

                                            } else {
                                                unset ($tomb[$key]);
                                                unset ($tomb[$key.'_'.$key1]);
                                                unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                            }

                                            if (!empty($value4['children'])) {
                                                foreach($value4['children'] as $key5=>$value5) {
                                                    if (empty($value5['product_total']) && empty($value5['product_id'])) {
                                                        $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5] = 1;

                                                    } else {
                                                        unset ($tomb[$key]);
                                                        unset ($tomb[$key.'_'.$key1]);
                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4]);
                                                    }

                                                    if (!empty($value5['children'])) {
                                                        foreach($value5['children'] as $key6=>$value6) {
                                                            if (empty($value6['product_total']) && empty($value6['product_id'])) {
                                                                $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6] = 1;

                                                            } else {
                                                                unset ($tomb[$key]);
                                                                unset ($tomb[$key.'_'.$key1]);
                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4]);
                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5]);
                                                            }

                                                            if (!empty($value6['children'])) {
                                                                foreach($value6['children'] as $key7=>$value7) {
                                                                    if (empty($value7['product_total']) && empty($value7['product_id'])) {
                                                                        $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6.'_'.$key7] = 1;

                                                                    } else {
                                                                        unset ($tomb[$key]);
                                                                        unset ($tomb[$key.'_'.$key1]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5]);
                                                                        unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6]);
                                                                    }

                                                                    if (!empty($value7['children'])) {
                                                                        foreach($value7['children'] as $key8=>$value8) {
                                                                            if (empty($value8['product_total']) && empty($value8['product_id'])) {
                                                                                $tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6.'_'.$key7.'_'.$key8] = 1;

                                                                            } else {
                                                                                unset ($tomb[$key]);
                                                                                unset ($tomb[$key.'_'.$key1]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6]);
                                                                                unset ($tomb[$key.'_'.$key1.'_'.$key2.'_'.$key3.'_'.$key4.'_'.$key5.'_'.$key6.'_'.$key7]);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        foreach($tomb as $key=>$value) {
            $kulcsok = explode('_', $key);
            $kulcs_ossz = count($kulcsok);
            if ($kulcs_ossz == 1) {
                if (isset($categoryes[$kulcsok[0]])) {
                    unset ($categoryes[$kulcsok[0]]);
                }

            } elseif ($kulcs_ossz == 2) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]);
                }

            } elseif ($kulcs_ossz == 3) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]);
                }

            } elseif ($kulcs_ossz == 4) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]);
                }

            } elseif ($kulcs_ossz == 5) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]);
                }

            } elseif ($kulcs_ossz == 6) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]);
                }

            } elseif ($kulcs_ossz == 7) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]);
                }

            } elseif ($kulcs_ossz == 8) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]['children'][$kulcsok[7]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]['children'][$kulcsok[7]]);
                }

            } elseif ($kulcs_ossz == 9) {
                if (isset($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]['children'][$kulcsok[7]]['children'][$kulcsok[8]])) {
                    unset ($categoryes[$kulcsok[0]]['children'][$kulcsok[1]]['children'][$kulcsok[2]]['children'][$kulcsok[3]]['children'][$kulcsok[4]]['children'][$kulcsok[5]]['children'][$kulcsok[6]]['children'][$kulcsok[7]]['children'][$kulcsok[8]]);
                }
            }
        }

        return $categoryes;
    }

    public function categorySort($category=array()){
        $width  = $this->config->get('multi_category_image_width') ? $this->config->get('multi_category_image_width') : $this->config->get('config_image_thumb_width');
        $height = $this->config->get('multi_category_image_height') ? $this->config->get('multi_category_image_height') : $this->config->get('config_image_thumb_height');

        $width_full  = $this->config->get('multi_category_full_image_width') ? $this->config->get('multi_category_full_image_width') : $this->config->get('config_image_thumb_width');
        $height_full = $this->config->get('multi_category_full_image_height') ? $this->config->get('multi_category_full_image_height') : $this->config->get('config_image_thumb_height');

        $elements = $this->config->rendezes($category,"sort_order");

        $vissza = array();
        foreach($elements as $element) {
            if ($element['image_icon']) {
                $element['image_icon'] = $this->model_tool_image->resize($element['image_icon'],$width, $height);
            } else {
                $element['image_icon'] =  $this->model_tool_image->resize('no_image.jpg', $width, $height,false);
            }

            if ($element['image']) {
                $element['image'] = $this->model_tool_image->resize($element['image'],$width_full, $height_full);
            } else {
                $element['image'] =  $this->model_tool_image->resize('no_image.jpg', $width_full, $height_full,false);
            }

            if (isset($element['children'])) {
                $element['children'] = $this->categorySort($element['children']);
            }
            $vissza[] = $element;


        }

        return $vissza;

    }


}
?>