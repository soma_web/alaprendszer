<ul class="menu-three" >
    <?php if(!isset($template['hide_lastseen']) || !$template['hide_lastseen']) { ?>
        <?php if ($latottak){ ?>
            <li><a class="<?php echo $lastseen_active; ?>" href="<?php echo $lastseen; ?>"><?php echo $lastseen_heading_title; ?></a></li>
        <?php } ?>
    <?php } ?>

    <?php if(!isset($template['hide_featured']) || !$template['hide_featured']) { ?>
        <?php if ($akciosak){ ?>
            <li><a class="<?php echo $special_active; ?>" href="<?php echo $special_header; ?>"><?php echo $special_heading_title; ?></a></li>
        <?php } ?>
    <?php } ?>

    <?php if(!isset($template['hide_webshop']) || !$template['hide_webshop']) { ?>
        <li><a class="<?php echo  $home_active; ?>" href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
    <?php } ?>

    <?php if(isset($attributes)) {?>
        <?php foreach($attributes as $attribute) { ?>
            <li><a class="tulajdonsagok <?php echo $attribute['active']?>" attr_id="<?php echo $attribute['attribute_id']?>" href="<?php echo $attribute['href']; ?>"><?php echo $attribute['name']; ?></a></li>
        <?php }?>
    <?php }?>

</ul>