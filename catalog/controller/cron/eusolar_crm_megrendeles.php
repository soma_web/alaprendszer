<?php   
class ControllerCronEusolarCrmMegrendeles extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;

        $this->load->model("cron/altalanos");
        $this->load->model('cron/eusolar_crm_megrendeles');

        echo 'Inditás<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $orders = $this->model_cron_eusolar_crm_megrendeles->getOrders();

        echo '<br><br>Rendelések összesen: '.count($orders).' db<br>';


        if ($orders) {
            foreach($orders as $key=>$value) {

                $orders[$key]['product'] = $this->model_cron_eusolar_crm_megrendeles->getOrderProducts($value["order_id"]);
                $orders[$key]['fizetes'] = $this->model_cron_eusolar_crm_megrendeles->getOrderFizetes($value["payment_code"]);
                echo "Order_id:".$value["order_id"]."<br>";
                echo "<br>";
                if ($orders[$key]['product']) {
                    $this->model_cron_eusolar_crm_megrendeles->writeCrm($orders[$key]);
                }
            }
        }

        $this->log->write_crone('Rendelések össz.: ' .count($orders) );

        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}
?>