<?php
class Language {
	private $default = 'english';
	private $directory;
	private $template;
	private $data = array();
 
	public function __construct($directory,$template) {
		$this->directory = $directory;
		$this->template = $template;
	}

  	public function get($key) {
   		return (isset($this->data[$key]) ? $this->data[$key] : $key);
  	}
	
	public function load($filename) {

		$file = DIR_LANGUAGE . $this->directory . '/' . $filename . '.php';
    	
		if (file_exists($file)) {
			$_ = array();

			require($file);

			$this->data = array_merge($this->data, $_);

            $file= DIR_TEMPLATE.$this->template.'/language/'.$this->directory.'/'.$filename . '.php';

            if (file_exists($file)) {
                $_ = array();

                require($file);

                $this->data = array_merge($this->data, $_);
            }

            $konyvtar = explode('/',$filename);
            if (!empty($konyvtar[0]) && !empty($konyvtar[1]) && !empty($this->template)) {
                $file = DIR_LANGUAGE . $this->directory . '/' . $konyvtar[0].'/'.$this->template.'/'. $konyvtar[1] . '.php';

                if (file_exists($file)) {
                    $_ = array();

                    require($file);

                    $this->data = array_merge($this->data, $_);
                }
            }

			return $this->data;
		}



		$file = DIR_LANGUAGE . $this->default . '/' . $filename . '.php';

		if (file_exists($file)) {
			$_ = array();

			require($file);

			$this->data = array_merge($this->data, $_);


            $file= DIR_TEMPLATE.$this->template.'/language/'.$this->directory.'/'.$filename . '.php';

            if (file_exists($file)) {
                $_ = array();

                require($file);

                $this->data = array_merge($this->data, $_);
            }

			return $this->data;
		} else {
			trigger_error('Error: Could not load language ' . $filename . '!');
			exit();
		}
  	}
}
?>