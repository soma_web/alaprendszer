<?php
class ModelTotalShipping extends Model {
	public function getTotal(&$total_data, &$total, &$taxes,$kosar="cart") {
		if ($this->cart->hasShipping($kosar) && isset($this->session->data['shipping_method'])) {

            $shipping_taxes = 0;
			if ($this->session->data['shipping_method']['tax_class_id']) {
				$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);
				
				foreach ($tax_rates as $tax_rate) {
					if (!isset($taxes[$tax_rate['tax_rate_id']])) {
						$taxes[$tax_rate['tax_rate_id']] = $tax_rate['amount'];
					} else {
						$taxes[$tax_rate['tax_rate_id']] += $tax_rate['amount'];
					}
                    $shipping_taxes += $tax_rate['amount'];
				}
			}

            $total_data[] = array(
                'code'       => 'shipping',
                'title'      => $this->session->data['shipping_method']['title'],
                'text'       => $this->currency->format($this->session->data['shipping_method']['cost']),
                'value'      => $this->session->data['shipping_method']['cost'],
                'tax'        => $shipping_taxes,
                'sort_order' => $this->config->get('shipping_sort_order')
            );

			
			$total += $this->session->data['shipping_method']['cost'];
		}			
	}
}
?>