<?php
$_['korhinta_tart']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta_tart',
    'default'           => "1",
    'heading'           => 'Körhinta'
);

$_['korhinta_darab_tart']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_darab_tart',
    'size'              => "2",
    'default'           => "3",
    'heading'           => "Mejelenít"
);
$_['korhinta_width_tart']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_width_tart',
    'size'              => "2",
    'default'           => "260",
    'heading'           => "Termék szélesség"
);

$_['korhinta_margin_tart']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_margin_tart',
    'size'              => "2",
    'default'           => "20",
    'heading'           => "Dobozok közötti távolság"
);

$_['korhinta_random_tart']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta_random_tart',
    'default'           => "1",
    'heading'           => 'Véletlenszerű'
);

?>