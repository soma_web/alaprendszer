<?php 
class ControllerAccountAccount extends Controller { 
	public function index() {
		if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');
	  
	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	} 
	
		$this->language->load('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
        if (isset($this->session->data['error'])) {
    		$this->data['error'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}
		
    	$this->data['heading_title']                    = $this->language->get('heading_title');

    	$this->data['text_my_account']                  = $this->language->get('text_my_account');
		$this->data['text_my_orders']                   = $this->language->get('text_my_orders');
		$this->data['text_my_newsletter']               = $this->language->get('text_my_newsletter');
    	$this->data['text_edit']                        = $this->language->get('text_edit');
        $this->data['text_edit_paypal']                 = $this->language->get('text_edit_paypal');
        $this->data['text_myproduct']                   = $this->language->get('text_myproduct');
        $this->data['text_newproduct']                  = $this->language->get('text_newproduct');
        $this->data['text_salereport']                  = $this->language->get('text_salereport');
        $this->data['text_jelentesek']                  = $this->language->get('text_jelentesek');
        $this->data['text_productreport_megnezett']     = $this->language->get('text_productreport_megnezett');
        $this->data['text_productreport_rendelesek']    = $this->language->get('text_productreport_rendelesek');
        $this->data['text_kategoria_hatteradatok']      = $this->language->get('text_kategoria_hatteradatok');
        $this->data['text_productreport_vasarlok']      = $this->language->get('text_productreport_vasarlok');
        $this->data['text_productreport_megvasarolt']   = $this->language->get('text_productreport_megvasarolt');
        $this->data['error_nincs_bevaltva']             = $this->language->get('error_nincs_bevaltva');
        $this->data['text_password']                    = $this->language->get('text_password');
        $this->data['text_address']                     = $this->language->get('text_address');
        $this->data['text_wishlist']                    = $this->language->get('text_wishlist');
        $this->data['text_order']                       = $this->language->get('text_order');
        $this->data['text_download']                    = $this->language->get('text_download');
        $this->data['text_download_utalvany']           = $this->language->get('text_download_utalvany');
        $this->data['text_reward']                      = $this->language->get('text_reward');
        $this->data['text_return']                      = $this->language->get('text_return');
        $this->data['text_transaction']                 = $this->language->get('text_transaction');
        $this->data['text_newsletter']                  = $this->language->get('text_newsletter');
        $this->data['text_arajanlat']                  = $this->language->get('text_arajanlat');

        $this->data['text_utalvanyellenorzes']          = $this->language->get('text_utalvanyellenorzes');
        $this->data['myproduct']                        = $this->url->link('account/product', '', 'SSL');
        $this->data['newproduct']                       = $this->url->link('account/product/insert', '', 'SSL');
        $this->data['salereport']                       = $this->url->link('account/sale', '', 'SSL');
        $this->data['product_report_megnezett']         = $this->url->link('account/megnezett', '', 'SSL');
        $this->data['product_report_megvasarolt']       = $this->url->link('account/megvasarolt', '', 'SSL');
        $this->data['product_report_rendelesek']        = $this->url->link('account/rendelesek', '', 'SSL');
        $this->data['product_report_vasarlok']          = $this->url->link('account/vasarlok', '', 'SSL');
        $this->data['report_kategoria_hatteradatok']    = $this->url->link('account/kategoria_jelentes', '', 'SSL');
        $this->data['arajanlat']    					= $this->url->link('account/arajanlat', '', 'SSL');



    	$this->data['edit'] = $this->url->link('account/edit', '', 'SSL');
    	$this->data['password'] = $this->url->link('account/password', '', 'SSL');
    	$this->data['utalvanyellenorzes'] = $this->url->link('account/utalvanyellenorzes', '', 'SSL');
		$this->data['address'] = $this->url->link('account/address', '', 'SSL');
		$this->data['wishlist'] = $this->url->link('account/wishlist');
    	$this->data['order'] = $this->url->link('account/order', '', 'SSL');
    	$this->data['download'] = $this->url->link('account/download', '', 'SSL');
    	$this->data['utalvanyok'] = $this->url->link('account/utalvanyok', '', 'SSL');
		$this->data['return'] = $this->url->link('account/return', '', 'SSL');
		$this->data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		
		if ($this->config->get('reward_status')) {
			$this->data['reward'] = $this->url->link('account/reward', '', 'SSL');
		} else {
			$this->data['reward'] = '';
		}

        if( $this->config->get("megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben") != null) {
            $this->data['ceges_cimmodositas_a_fiok_szerkeztesben'] = $this->config->get("megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben");
        } else {
            $this->data['ceges_cimmodositas_a_fiok_szerkeztesben'] = 0;
        }

        if($this->config->get("megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben") != null) {
            $this->data['cimmodositas_a_fiok_szerkeztesben'] = $this->config->get("megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben");
        } else {
            $this->data['cimmodositas_a_fiok_szerkeztesben'] = 0;
        }

        $this->load->model('account/customer');
        $vevo=$this->model_account_customer->getCustomer($_SESSION['customer_id']);

        $this->data['feltolto'] = $vevo['feltolto'];

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/account.tpl';
		} else {
			$this->template = 'default/template/account/account.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'		
		);
				
		$this->response->setOutput($this->render());
  	}
}
?>