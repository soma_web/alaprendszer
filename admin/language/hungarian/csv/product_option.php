<?php

// Heading
$_['heading_title'] = 'Kapcsolódó választékok CSV Mentés/Visszaállítás';
$_['text_magyarazat'] = 'Visszatöltésnél a pirossal kiemelt oszlopok kitöltése kötelező. A többi oszlop megléte szükséges, de maradhat üresen. <br> A csillaggal jelölt oszlopokból elég az egyiket kitölteni.';
?>