<?php echo $header; ?>
<link rel="stylesheet" href="view/javascript/jquery/ui/themes/base/jquery.ui.all.css">
<style type="text/css">
	.breadcrumb {float:left;margin-top:15px;}
	#h1Title {float:none;}
	.success, .warning {display:none;position:absolute;top:142px;left:95px;font-weight:bold;font-style:italic;}
	#tabs, #titleFormQuestion, #titleFormCat {margin:0px;}
	.headerQuestion, .headerCategory {height:25px;padding:0px;margin:0px 0 15px 0;}
	.fLeft {float:left;}
	.deleteQuestion, .deleteCategory, #addQuestionButton, #cancelQuestionButton, #saveQuestionButton, #addCategoryButton, #cancelCategoryButton, #saveCategoryButton {color:#FFF !important;}
	#divSearch {position:absolute;top:58px;left:260px;}
	.fRight {float:right;}
	#searchWord{padding-left:15px;margin-top:15px;};
</style>

<script src="view/javascript/jquery/ui/minified/jquery.ui.core.min.js"></script>
<script src="view/javascript/jquery/ui/minified/jquery.ui.widget.min.js"></script>
<script src="view/javascript/jquery/ui/minified/jquery.ui.tabs.min.js"></script>
<script>
$(function() {
	$( "#tabs" ).tabs();
});
</script>
<div id="content">
<div style="height:40px;">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>

<input type="hidden" id="sortCategory" value="DESC">
<input type="hidden" id="sortQuestion" value="ASC">
<input type="hidden" id="currentPage" value="1">
</div>
  <div class="box">
    <div class="heading">
    	<h1 id="h1Title"><?php echo $heading_title;?></h1>
	
<div class="success"></div>
		<div class="warning"></div>

    </div>
	<div class="content">
    	<div id="tabs">
    		<ul>
    			<li id="#tab-questionsLi"><a id="#tab-questions" href="#tab-questions"><?php echo $text_tabQuestions;?></a></li>
  				<li id="#tab-CategoriesLi"><a id="#tab-Categories" href="#tab-categories"><?php echo $text_tabCategories;?></a></li>
    		</ul>	
			<div id="tab-questions">
				<div id="listQuestions">
					<div class="headerQuestion">
						<div class="fLeft">
							<a class="button deleteQuestion"><span><?php echo $text_deleteQuestions;?></span></a>
						</div>	
						<div id="divSearch">
							<strong id="searchWord"><?php echo $text_search;?></strong>
							<input id="searchQuestion" type="text"/>
							<span><a id="resetSearch"><?php echo $text_resetSearch;?></a></span>
						</div>	
						<div class="fRight">
							<a id="addQuestionButton" class="button"><span><?php echo $text_addQuestion;?></span></a>
						</div>	
					</div>	
					<form id="formQuestion" action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
						<div id="listQuestionsDiv">
							
						</div>	
					</form>
					<div class="pagination"></div>
				</div>	
				<div id="tabFormQuestion">
					<div class="headerQuestion">						
						<div class="fLeft">
							<h1 id="titleFormQuestion"></h1>
						</div>
						<div class="fRight">
							<a id="cancelQuestionButton" class="button" ><span><?php echo $text_questions;?></span></a>
							<a id="saveQuestionButton" class="button"><span><?php echo $text_save;?></span></a>
						</div>
					</div>
					<form id="addQuestionForm" action="" method="post" enctype="multipart/form-data">
						<input type="hidden" id="linkAddQuestion" size="100" value="<?php echo $actionAddQuestion; ?>">
						<input type="hidden" id="linkEditQuestion" size="100" value="<?php echo $actionEditQuestion; ?>">
						<input type="hidden" name="idFaq" id="faq_idFaq" size="100" value="">
						<table class="form">
						  <tr>
						    <td><?php echo $column_category;?></td>
						    <td id="selectCategories">
						    	<?php
						    		foreach ($faq_categories as $cat) { 
					        			echo '<option value="'.$cat['idCategory'].'">'.$cat['category'].'</option>';
						    		}
						    	?>
						   		</select>	-->
						   </td>
						  </tr>
						  <tr>
						    <td><?php echo $column_question;?></td>
						    <td>
						    	<?php if(isset($question)){?>
						    		<input name="question" id="faq_question" size="170" value="<?php echo $question; ?>">
						    	<?php }else{?>	
						    		<input name="question" id="faq_question" size="170" value="">
						    	<?php }?>	
						    </td>
						  </tr>
						  <tr>
						    <td><?php echo $column_response;?></td>
						    <td id="textAreaTd">
							    <textarea name="response" id="response1"></textarea>
						     </td>
						  </tr>
						</table>
					</form>
				</div>
			</div>
			<div id="tab-categories">
				<div id="listCategories">	
					<div class="headerCategory">
						<div class="fLeft">
							<a class="button deleteCategory"><span><?php echo $text_deleteCategories;?></span></a>
						</div>
						<div class="fRight">
							<a id="addCategoryButton" class="button"><span><?php echo $text_addCategory;?></span></a>
						</div>
					</div>
					<form id="formCat" action="<?php echo $deleteCategory; ?>" method="post" enctype="multipart/form-data">
						<div id="listCategoriesDiv">
									
						</div>	
					</form>
				</div>

				<div id="tabFormCategory">
					<div class="headerCategory">
						<div class="fLeft">
							<h1 id="titleFormCat"></h1>
						</div>
						<div class="fRight">
							<a id="cancelCategoryButton" class="button"><span><?php echo $text_categories;?></span></a>
							<a id="saveCategoryButton" class="button"><span><?php echo $text_save;?></span></a>
						</div>
					</div>
					<div>
						<form id="addCategoryForm" action="" method="post" enctype="multipart/form-data">
							<input type="hidden" id="linkAddCategory" size="100" value="<?php echo $actionAddCategory; ?>">
							<input type="hidden" id="linkEditCategory" size="100" value="<?php echo $actionEditCategory; ?>">
							<input type="hidden" name="idCategory" id="faqCategories_id" size="100" value="">
				            <table class="form">
					              <tr>
					                <td><?php echo $text_nameCategory;?></td>
					                <td>
					                	<input name="category" id="faqCategories_question" size="100" value="">
					               </td>
					              </tr>
				            </table>
				      	</form>
					</div>
				</div>
			</div>		
		</div>
	</div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 

<script type="text/javascript">
CKEDITOR.replace('response1', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});

<?php
echo "function refreshListQuestions(limitMin, limitMax, sort, column){
	$.ajax({
		type: 'post',
		url: '".$linkListQuestions."',
		dataType:'text',
		data:'limitMin='+limitMin+'&limitMax='+limitMax+'&sort='+sort+'&column='+column,
		success: function(text)
		{
			$('#listQuestionsDiv').html(text);
		},
		error: function()
		{
			alert('Error, cannot contact the server');
		}
	});
}

function searchQuestions(query){
	$.ajax({
		type: 'post',
		url: '".$linkListQuestions."',
		dataType:'text',
		data:'query='+escape(encodeURI(query)),
		success: function(text)
		{
			$('#listQuestionsDiv').html(text);
		},
		error: function()
		{
			alert('Error, cannot contact the server');
		}
	});
}

function refreshListCategories(){
	$.ajax({
		type: 'post',
		url: '".$linkListCategories."',
		dataType:'text',
		success: function(text)
		{
			$('#listCategoriesDiv').html(text);
		},
		error: function()
		{
			alert('Error, cannot contact the server');
		}
	});
}

function getSelectCategories(){
	$.ajax({
		type: 'post',
		url: '".$linkGetSelectCategories."',
		dataType:'text',
		success: function(text)
		{
			$('#selectCategories').html(text);
		},
		error: function()
		{
			alert('Error, cannot contact the server');
		}
	});
}

function pagination(currentPage){
	$.ajax({
		type: 'post',
		url: '".$pagination."',
		data: 'currentPage='+currentPage,
		dataType:'text',
		success: function(text)
		{
			$('.pagination').html(text);
		},
		error: function()
		{
			alert('Error, cannot contact the server');
		}
	});
}";?>

refreshListCategories();
refreshListQuestions(0, 15, 'ASC', 'category');
getSelectCategories();
pagination(1);


</script>

<script type=text/javascript>	
	$('#tabFormCategory').css('display', 'none');
	$('#tabFormQuestion').css('display', 'none');
	
	$('#addCategoryButton').click(function(){
		$('#titleFormCat').text("<?php echo $text_titleAddCategory; ?>");
		$('#faqCategories_id').val("");
		$('#faqCategories_question').val("");
		$('#addCategoryForm').attr('action',$('#linkAddCategory').val());
		$('#listCategories').css('display', 'none');
		$('#tabFormCategory').css('display', 'block');
		return false;
	});
	
	$('#cancelCategoryButton').click(function(){		
		$('#listCategories').css('display', 'block');
		$('#tabFormCategory').css('display', 'none');
		return false;
	});
	
	$('#addQuestionButton').click(function(){	
		$('#titleFormQuestion').text("<?php echo $text_titleAddQuestion; ?>");
		$('#faq_idFaq').val("");
		$('#faq_question').val("");
			CKEDITOR.instances.response1.destroy();	
			$('#textAreaTd').html('<textarea name="response" id="response1"></textarea>');
			//CKEDITOR.replace('response1');
			CKEDITOR.replace('response1', {
				filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
			});			
		$('#addQuestionForm').attr('action',$('#linkAddQuestion').val());
		$('#listQuestions').css('display', 'none');
		$('#tabFormQuestion').css('display', 'block');
		return false;
	});
	

	$('.deleteQuestion').click(function(){	
		$.ajax({
			type: 'post',
			url: $('#formQuestion').attr('action'),
			data: $('#formQuestion').serialize(),
			dataType:'xml',
			success: function(xml)
			{	
				if($(xml).find('status').text() == 'success'){
					$('.success').text($(xml).find('message').text());						
					$('.success').fadeIn(2000, function () {
	            	$(".success").fadeOut(1000);});
				}
				else{
					$('.warning').text($(xml).find('message').text());						
					$('.warning').fadeIn(2000, function () {
	            	$(".warning").fadeOut(1000);});
				}
				refreshListQuestions(0, 15, 'ASC', 'category');
				refreshListCategories();
			},
			error: function()
			{
				alert('Error, cannot contact the server');
			}
		});
	});
	
	$('.deleteCategory').click(function(){	
		$.ajax({
			type: 'post',
			url: $('#formCat').attr('action'),
			data: $('#formCat').serialize(),
			dataType:'xml',
			success: function(xml)
			{	
				if($(xml).find('status').text() == 'success'){
					$('.success').text($(xml).find('message').text());		
					$('.success').fadeIn(2000, function () {
        	    	$(".success").fadeOut(1000);});
        	    }
        	    else{
 					$('.warning').text($(xml).find('message').text());						
					$('.warning').fadeIn(2000, function () {
	            	$(".warning").fadeOut(1000);});
        	    }
				refreshListCategories();
				refreshListQuestions(0, 15, 'ASC', 'category');
				getSelectCategories();
			},
			error: function()
			{
				alert('Error, cannot contact the server');
			}
		});
	});

	
	$('#cancelQuestionButton').click(function(){		
		$('#listQuestions').css('display', 'block');
		$('#tabFormQuestion').css('display', 'none');
		return false;
	});
	
	$('#saveQuestionButton').click(function(){	
		$('#response1').val(escape(encodeURI(CKEDITOR.instances.response1.getData())));
		$('#faq_question').val(escape(encodeURI($('#faq_question').val())));
		$.ajax({
			type: 'post',
			url: $('#addQuestionForm').attr('action'),
			data: $('#addQuestionForm').serialize(),
			dataType:'xml',
			success: function(xml)
			{	
				if($(xml).find('status').text() == "success"){
					$('.success').text($(xml).find('message').text());		
					$('.success').fadeIn(2000, function () {
	            	$(".success").fadeOut(1000);});	
	            	
					//refreshListQuestions(0, 15, 'ASC', 'category');
					
					$('#searchQuestion').val('');
					var limit = 15;
					limitMin = ($('#currentPage').val()-1)*limit;
					refreshListQuestions(limitMin, limit, 'ASC', 'category');
					
					refreshListCategories();
					$('#listQuestions').css('display', 'block');
					$('#tabFormQuestion').css('display', 'none');
				}
				else{
					$('.warning').text($(xml).find('message').text());						
					$('.warning').fadeIn(2000, function () {
	            	$(".warning").fadeOut(1000);});
				}
			},
			error: function()
			{
				alert('Error, cannot contact the server');
			}
		});
		return false;
	});
	

	
	$('#saveCategoryButton').click(function(){		
		//$('#faqCategories_question').val(escape(encodeURI($('#faqCategories_question').val())));
		$.ajax({
			type: 'post',
			url: $('#addCategoryForm').attr('action'),
			data: $('#addCategoryForm').serialize(),
			dataType:'xml',
			success: function(xml)
			{
				if($(xml).find('status').text() == "success"){
					$('.success').text($(xml).find('message').text());		
					$('.success').fadeIn(2000, function () {
	            	$(".success").fadeOut(1000);});	
					refreshListCategories();
					refreshListQuestions(0, 15, 'ASC', 'category');
					getSelectCategories();
					$('#listCategories').css('display', 'block');
					$('#tabFormCategory').css('display', 'none');
				}
				else{
					$('.warning').text($(xml).find('message').text());						
					$('.warning').fadeIn(2000, function () {
	            	$(".warning").fadeOut(1000);});
				}
			},
			error: function()
			{
				alert('Error, cannot contact the server');
			}
		});
		return false;
	});
	
	$('#searchQuestion').keyup(function(){
		if($('#searchQuestion').val() != ""){
			searchQuestions($('#searchQuestion').val());
			$('.pagination').html("");
		}
		else{
			refreshListQuestions(0, 15, 'ASC', 'category');
			pagination(1);
		}	
	});
	
	$('#resetSearch').click(function(){
		$('#searchQuestion').val("");
		refreshListQuestions(0, 15, 'ASC', 'category');
		pagination(1);
	});
</script>
<?php echo $footer; ?>