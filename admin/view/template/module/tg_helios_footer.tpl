<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>

<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  
<div class="content">
    <div id="tabs" class="htabs">
        <a href="#tab-general"><?php echo $tab_gen; ?></a>
        <a href="#tab-footer_alap"><?php echo $tab_footer_alap; ?></a>
        <a href="#tab-footer_masodlagos"><?php echo $tab_footer_masodlagos; ?></a>
        <a href="#tab-sidebar"><?php echo $tab_sidebar; ?></a>
    </div>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">



<div id="tab-general">

    <div id="general-stores" class="htabs">
        <?php foreach ($stores as $store) { ?>
            <a href="#general-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
        <?php } ?>
    </div>


    <?php foreach ($stores as $store) { ?>
         <div id="general-store<?php echo $store['store_id']; ?>">
             <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
             <table class="form">
                 <tr>
                     <td><?php echo $entry_status; ?></td>
                     <td colspan=3>
                         <select name="tg_helios_footer_status<?php echo $aruhaz_id?>">
                             <?php $kepzett_valtozo = 'tg_helios_footer_status'.$aruhaz_id?>
                             <?php if ($$kepzett_valtozo) { ?>
                                 <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                 <option value="0"><?php echo $text_disabled; ?></option>
                             <?php } else { ?>
                                 <option value="1"><?php echo $text_enabled; ?></option>
                                 <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                             <?php } ?>
                       </select>
                     </td>
                 </tr>

                 <tr>
                     <td>Alap lábléc:</td>
                     <td style="width: 200px;" colspan="1">
                         <select name="tg_helios_footer_default_show<?php echo $aruhaz_id?>">

                             <?php $kepzett_valtozo = 'tg_helios_footer_default_show'.$aruhaz_id?>

                             <?php if ($$kepzett_valtozo) { ?>
                                 <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                 <option value="0">No</option>
                             <?php } else { ?>
                                 <option value="1">Yes</option>
                                 <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                             <?php } ?>
                         </select>
                     </td>

                     <td style="width: 140px;"><b>Felül</b> / <b>Alul</b> jelenjen meg:</td>
                     <td >
                         <select name="tg_helios_footer_default_show_alul<?php echo $aruhaz_id?>">

                             <?php $kepzett_valtozo = 'tg_helios_footer_default_show_alul'.$aruhaz_id?>

                             <?php if ($$kepzett_valtozo == 0) { ?>
                                 <option value="0" selected="selected">Felül</option>
                                 <option value="1" >Alul</option>
                             <?php } else { ?>
                                 <option value="0">Felül</option>
                                 <option value="1" selected="selected">Alul</option>
                             <?php } ?>
                         </select>
                     </td>
                </tr>
            </table>
        </div>
    <?php } ?>
</div>

<!-- tab_alap_beallitas (end) -->

<div id="tab-footer_alap">


    <div id="fttab" class="htabs">
        <a href="#fttab-informaciok"><?php echo $text_informacio; ?></a>
        <a href="#fttab-ugyfelszolgalat"><?php echo $text_ugyfelszolgalat; ?></a>
        <a href="#fttab-extrak"><?php echo $text_extrak; ?></a>
        <a href="#fttab-fiok"><?php echo $text_fiok; ?></a>
        <a href="#fttab-uzenet"><?php echo $text_uzenet; ?></a>
        <a href="#fttab-hirlevel"><?php echo $text_hirlevel; ?></a>
        <a href="#fttab-likebox"><?php echo $tab_likebox; ?></a>
        <a href="#fttab-elerhetoseg"><?php echo $text_elerhetoseg; ?></a>
        <a href="#fttab-kategoriak"><?php echo $text_kategoriak; ?></a>
    </div>





    <div id="fttab-informaciok">
        <div id="informaciok-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#informaciok-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>


        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>

            <div id="informaciok-store<?php echo $store['store_id']; ?>">
                <table class="form1">
                    <tr>
                        <td style="padding-right: 30px;"><?php echo $text_informacio; ?></td>
                        <td style="padding-right: 30px;">

                            <?php $kepzett_valtozo = 'tg_helios_footer_informaciok_status'.$aruhaz_id?>

                            <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo) { ?>
                                <input type="radio" name="tg_helios_footer_informaciok_status<?php echo $aruhaz_id?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_informaciok_status<?php echo $aruhaz_id?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="tg_helios_footer_informaciok_status<?php echo $aruhaz_id?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_informaciok_status<?php echo $aruhaz_id?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                        <td>Sorrend:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_informaciok_status_sorrend'.$aruhaz_id?>

                        <td><input type="text" name="tg_helios_footer_informaciok_status_sorrend<?php echo $aruhaz_id?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : 0?>" size=2 /></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>

    <div id="fttab-ugyfelszolgalat">

        <div id="ugyfelszolgalat-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#ugyfelszolgalat-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>


        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>

            <div id="ugyfelszolgalat-store<?php echo $store['store_id']; ?>">

                <table class="form1">
                    <tr>
                        <td style="padding-right: 30px;"><?php echo $text_ugyfelszolgalat; ?></td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_ugyfelszol_status'.$aruhaz_id?>

                            <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo) { ?>
                                <input type="radio" name="tg_helios_footer_ugyfelszol_status<?php echo $aruhaz_id?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_ugyfelszol_status<?php echo $aruhaz_id?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="tg_helios_footer_ugyfelszol_status<?php echo $aruhaz_id?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_ugyfelszol_status<?php echo $aruhaz_id?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                        <td>Sorrend:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_ugyfelszol_status_sorrend'.$aruhaz_id?>

                        <td><input type="text" name="tg_helios_footer_ugyfelszol_status_sorrend<?php echo $aruhaz_id?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : 0?>" size=2 /></td>
                    </tr>

                    <tr><td><br></td></tr>

                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td><?php echo $text_info_title; ?></td>
                            <td colspan=3>
                                <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                                <?php $kepzett_valtozo = 'ugyfelszol_title'.$language['language_id'].$aruhaz_id?>

                                <input type="text" name="ugyfelszol_title<?php echo $language['language_id'].$aruhaz_id; ?>"  size="30" value="<?php echo ${'ugyfelszol_title' . $language['language_id'] .$aruhaz_id}; ?>" />
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                            </td>
                        </tr>
                    <?php } ?>


                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                                <?php echo $entry_code; ?>
                            </td>
                            <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>

                            <?php $kepzett_valtozo = 'ugyfelszol_code'.$language['language_id'].$aruhaz_id?>

                            <td colspan=3><textarea name="ugyfelszol_code<?php echo $language['language_id'].$aruhaz_id; ?>" cols="40" rows="10"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>

                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php } ?>
    </div>

    <div id="fttab-extrak">
        <div id="extrak-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#extrak-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>


        <?php foreach ($stores as $store) { ?>
        <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>

            <div id="extrak-store<?php echo $store['store_id']; ?>">

                <table class="form1">
                    <tr>
                        <td style="padding-right: 30px;"><?php echo $text_extrak; ?></td>
                        <td style="padding-right: 30px;">
                            <?php $kepzett_valtozo = 'tg_helios_footer_extrak_status'.$aruhaz_id?>

                            <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo) { ?>
                                <input type="radio" name="tg_helios_footer_extrak_status<?php echo $aruhaz_id;?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_extrak_status<?php echo $aruhaz_id;?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="tg_helios_footer_extrak_status<?php echo $aruhaz_id;?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_extrak_status<?php echo $aruhaz_id;?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                        <td>Sorrend:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_extrak_status_sorrend'.$aruhaz_id?>

                        <td><input type="text" name="tg_helios_footer_extrak_status_sorrend<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : 0?>" size=2 /></td>
                    </tr>

                    <tr><td><br></td></tr>

                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td><?php echo $text_info_title; ?></td>
                            <td colspan=3>
                                <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                                <?php $kepzett_valtozo = 'extrak_title'.$language['language_id'].$aruhaz_id?>

                                <input type="text" name="extrak_title<?php echo $language['language_id'].$aruhaz_id; ?>"  size="30" value="<?php echo ${'extrak_title' . $language['language_id'].$aruhaz_id}; ?>" />
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                            </td>
                        </tr>
                    <?php } ?>


                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                                <?php echo $entry_code; ?>
                            </td>
                            <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                            <?php $kepzett_valtozo = 'extrak_code'.$language['language_id'].$aruhaz_id?>
                            <td colspan=3>
                                <textarea name="extrak_code<?php echo $language['language_id'].$aruhaz_id; ?>" cols="40" rows="10"><?php echo isset(${'extrak_code' . $language['language_id'].$aruhaz_id}) ? ${'extrak_code' . $language['language_id'].$aruhaz_id} : ''; ?></textarea>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php } ?>
    </div>


    <div id="fttab-fiok">
        <div id="fiok-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#fiok-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>


        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="fiok-store<?php echo $store['store_id']; ?>">

                <table class="form1">
                    <tr>
                        <td style="padding-right: 30px;"><?php echo $text_fiok; ?></td>
                        <td style="padding-right: 30px;">
                            <?php $kepzett_valtozo = 'tg_helios_footer_fiok_status'.$aruhaz_id?>

                            <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo) { ?>
                                <input type="radio" name="tg_helios_footer_fiok_status<?php echo $aruhaz_id;?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_fiok_status<?php echo $aruhaz_id;?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="tg_helios_footer_fiok_status<?php echo $aruhaz_id;?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_fiok_status<?php echo $aruhaz_id;?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                        <td>Sorrend:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_fiok_status_sorrend'.$aruhaz_id?>

                        <td><input type="text" name="tg_helios_footer_fiok_status_sorrend<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : 0?>" size=2 /></td>
                    </tr>

                    <tr><td><br></td></tr>

                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td>
                                <?php echo $text_info_title; ?>
                            </td>
                            <td colspan=3>
                                <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                                <?php $kepzett_valtozo = 'fiok_title'.$language['language_id'].$aruhaz_id?>

                                <input type="text" name="fiok_title<?php echo $language['language_id'].$aruhaz_id; ?>"  size="30" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" />
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                            </td>
                        </tr>
                    <?php } ?>


                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td ><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                                <?php echo $entry_code; ?>
                            </td>
                            <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                            <?php $kepzett_valtozo = 'fiok_code'.$language['language_id'].$aruhaz_id?>
                            <td colspan=3><textarea name="fiok_code<?php echo $language['language_id'].$aruhaz_id; ?>" cols="40" rows="10"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?></textarea>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php } ?>
    </div>


    <div id="fttab-uzenet">
        <div id="uzenet-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#uzenet-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="uzenet-store<?php echo $store['store_id']; ?>">

                <table class="form1">
                    <tr>
                        <td style="padding-right: 30px;"><?php echo $text_uzenet; ?></td>
                        <td style="padding-right: 30px;">

                            <?php $kepzett_valtozo = 'tg_helios_footer_uzenet_status'.$aruhaz_id?>

                            <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo) { ?>
                                <input type="radio" name="tg_helios_footer_uzenet_status<?php echo $aruhaz_id;?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_uzenet_status<?php echo $aruhaz_id;?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="tg_helios_footer_uzenet_status<?php echo $aruhaz_id;?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_uzenet_status<?php echo $aruhaz_id;?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                        <td>Sorrend:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_uzenet_status_sorrend'.$aruhaz_id?>

                        <td><input type="text" name="tg_helios_footer_uzenet_status_sorrend<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" size=2 /></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>

    <div id="fttab-hirlevel">
        <div id="hirlevel-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#hirlevel-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="hirlevel-store<?php echo $store['store_id']; ?>">
                <table class="form1">
                    <tr>
                        <td style="padding-right: 30px;"><?php echo $text_hirlevel; ?></td>
                        <td style="padding-right: 30px;">
                            <?php $kepzett_valtozo = 'tg_helios_footer_hirlevel_status'.$aruhaz_id?>

                            <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo) { ?>
                                <input type="radio" name="tg_helios_footer_hirlevel_status<?php echo $aruhaz_id;?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_hirlevel_status<?php echo $aruhaz_id;?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="tg_helios_footer_hirlevel_status<?php echo $aruhaz_id;?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_hirlevel_status<?php echo $aruhaz_id;?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                        <td>Sorrend:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_hirlevel_status_sorrend'.$aruhaz_id?>
                        <td><input type="text" name="tg_helios_footer_hirlevel_status_sorrend<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" size=2 /></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>

    <div id="fttab-likebox">
        <div id="likebox-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#likebox-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
        <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="likebox-store<?php echo $store['store_id']; ?>">
                <table class="form1">
                    <tr>
                        <td style="padding-right: 30px;"><?php echo $tab_likebox; ?></td>
                        <td style="padding-right: 30px;">

                            <?php $kepzett_valtozo = 'tg_helios_footer_likebox_status'.$aruhaz_id?>

                            <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo) { ?>
                                <input type="radio" name="tg_helios_footer_likebox_status<?php echo $aruhaz_id;?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_likebox_status<?php echo $aruhaz_id;?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="tg_helios_footer_likebox_status<?php echo $aruhaz_id;?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_likebox_status<?php echo $aruhaz_id;?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                        <td>Sorrend:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_likebox_status_sorrend'.$aruhaz_id?>

                        <td><input type="text" name="tg_helios_footer_likebox_status_sorrend<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" size=2 /></td>
                    </tr>

                    <tr>
                        <td colspan=3>
                            <?php $kepzett_valtozo = 'likebox_code'.$aruhaz_id?>

                            <textarea name="likebox_code<?php echo $aruhaz_id;?>" cols="40" rows="10"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>

    <div id="fttab-elerhetoseg">

        <div id="elerhetoseg-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#elerhetoseg-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="elerhetoseg-store<?php echo $store['store_id']; ?>">

                <table class="form1">
                    <tr>
                        <td style="padding-right: 30px;"><?php echo $text_elerhetoseg; ?></td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_elerhetoseg_status'.$aruhaz_id?>

                            <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo) { ?>
                                <input type="radio" name="tg_helios_footer_elerhetoseg_status<?php echo $aruhaz_id;?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_elerhetoseg_status<?php echo $aruhaz_id;?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="tg_helios_footer_elerhetoseg_status<?php echo $aruhaz_id;?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_elerhetoseg_status<?php echo $aruhaz_id;?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                        <td>Sorrend:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_elerhetoseg_status_sorrend'.$aruhaz_id?>

                        <td><input type="text" name="tg_helios_footer_elerhetoseg_status_sorrend<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" size=2 /></td>
                    </tr>

                    <tr><td><br></td></tr>

                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td><?php echo $text_info_title; ?></td>
                            <td colspan=3>
                                <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                                <?php $kepzett_valtozo = 'elerhetoseg_title'.$language['language_id'].$aruhaz_id?>

                                <input type="text" name="elerhetoseg_title<?php echo $language['language_id'].$aruhaz_id; ?>" id="elerhetoseg_title<?php echo $language['language_id']; ?>" size="30" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" />
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                            </td>
                        </tr>
                    <?php } ?>


                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td ><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br>
                                <?php echo $entry_code; ?>

                            </td>
                            <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                            <?php $kepzett_valtozo = 'elerhetoseg_code'.$language['language_id'].$aruhaz_id?>

                            <td colspan=3><textarea name="elerhetoseg_code<?php echo $language['language_id'].$aruhaz_id; ?>" cols="40" rows="10"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?></textarea>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php } ?>
    </div>


    <div id="fttab-kategoriak">
        <div id="kategoriak-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#kategoriak-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>


        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>

            <div id="kategoriak-store<?php echo $store['store_id']; ?>">
                <table class="form1">
                    <tr>
                        <td style="padding-right: 30px;"><?php echo $text_kategoriak; ?></td>
                        <td style="padding-right: 30px;">

                            <?php $kepzett_valtozo = 'tg_helios_footer_kategoriak_status'.$aruhaz_id?>

                            <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo) { ?>
                                <input type="radio" name="tg_helios_footer_kategoriak_status<?php echo $aruhaz_id?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_kategoriak_status<?php echo $aruhaz_id?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="tg_helios_footer_kategoriak_status<?php echo $aruhaz_id?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="tg_helios_footer_kategoriak_status<?php echo $aruhaz_id?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                        <td>Sorrend:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_kategoriak_status_sorrend'.$aruhaz_id?>

                        <td><input type="text" name="tg_helios_footer_kategoriak_status_sorrend<?php echo $aruhaz_id?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : 0?>" size=2 /></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>


</div>

<!-- tab_alap_footer (end) -->

<div id="tab-footer_masodlagos">

    <div id="fttab" class="htabs">
        <a href="#tab-info"><?php echo $tab_info; ?></a>
        <a href="#tab_contact"><?php echo $tab_contact; ?></a>
        <a href="#tab-terkep"><?php echo $tab_terkep; ?></a>
        <a href="#tab-twitter"><?php echo $tab_twitter; ?></a>
        <a href="#tab-default"><?php echo $tab_default; ?></a>
        <a href="#tab_facebook_like_button"><?php echo $tab_facebook_like_button; ?></a>
        <a href="#tab_egyeb"><?php echo $tab_egyeb; ?></a>
    </div>

    <div id="tab-info">
        <div id="info-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#info-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="info-store<?php echo $store['store_id']; ?>">

                <table class="form">
                    <tr>
                        <td>Engedélyezett</td>
                            <td>
                                <?php $kepzett_valtozo = 'tg_helios_footer_info_status'.$aruhaz_id?>
                                <select name="tg_helios_footer_info_status<?php echo $aruhaz_id;?>">
                                    <option value="1"<?php if($$kepzett_valtozo == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                    <option value="0"<?php if($$kepzett_valtozo != '1') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                                </select>
                            </td>
                    </tr>

                    <tr>
                        <td>Sorszám vízszintes:</td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_info_sort'.$aruhaz_id?>
                            <input type="text" name="tg_helios_footer_info_sort<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" size="5" />

                            <span class="fuggoleges">Sorszám függőleges:</span>
                            <?php $kepzett_valtozo = 'tg_helios_footer_info_fuggoleges'.$aruhaz_id?>
                            <input type="text" name="tg_helios_footer_info_fuggoleges<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" size="5" />
                        </td>
                    </tr>


                    <tr>
                        <td>Méretek:</td>
                        <td>Szélesség:
                            <?php $kepzett_valtozo = 'tg_helios_footer_info_szelesseg'.$aruhaz_id?>
                            <input type="text" name="tg_helios_footer_info_szelesseg<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" size="10" />

                            <span class="fuggoleges">Magasság:</span>
                            <?php $kepzett_valtozo = 'tg_helios_footer_info_magassag'.$aruhaz_id?>
                            <input type="text" name="tg_helios_footer_info_magassag<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" size="10" />
                        </td>
                    </tr>


                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td><?php echo $text_info_title; ?></td>
                                <td>
                                    <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                                    <?php $kepzett_valtozo = 'mymodule_title'.$language['language_id'].$aruhaz_id?>
                                    <input type="text" name="mymodule_title<?php echo $language['language_id'].$aruhaz_id; ?>"  size="30" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" />
                                    <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                                </td>
                        </tr>
                    <?php } ?>

                    <?php foreach ($languages as $language) { ?>
                        <tr>
                          <td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                              <?php echo $entry_code; ?>
                          </td>
                            <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                            <?php $kepzett_valtozo = 'mymodule_code'.$language['language_id'].$aruhaz_id?>
                          <td><textarea name="mymodule_code<?php echo $language['language_id'].$aruhaz_id; ?>" cols="40" rows="10"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?></textarea>

                          </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php } ?>
    </div> <!-- tab_info (end) -->



    <div id="tab_contact">
        <div id="contact-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#contact-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="contact-store<?php echo $store['store_id']; ?>">

                <table class="form">
                    <tr>
                        <td>Engedélyezett</td>
                            <td>
                                <?php $kepzett_valtozo = 'tg_helios_footer_contact_status'.$aruhaz_id?>

                                <select name="tg_helios_footer_contact_status<?php echo $aruhaz_id;?>">
                                    <option value="1"<?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                    <option value="0"<?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo != '1') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                                </select>
                            </td>
                    </tr>
                    <tr>
                        <td>Sorszám:</td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_contact_sort'.$aruhaz_id?>
                            <input type="text" name="tg_helios_footer_contact_sort<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />

                            <span class="fuggoleges">Sorszám függőleges:</span>
                            <?php $kepzett_valtozo = 'tg_helios_footer_contact_fuggoleges'.$aruhaz_id?>
                            <input type="text" name="tg_helios_footer_contact_fuggoleges<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />
                        </td>
                    </tr>

                    <tr>
                       <td>Méretek:</td>
                       <td>Szélesség:
                           <?php $kepzett_valtozo = 'tg_helios_footer_contact_szelesseg'.$aruhaz_id?>
                           <input type="text" name="tg_helios_footer_contact_szelesseg<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />

                           <span class="fuggoleges">Magasság:</span>
                           <?php $kepzett_valtozo = 'tg_helios_footer_contact_magassag'.$aruhaz_id?>
                           <input type="text" name="tg_helios_footer_contact_magassag<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />
                       </td>
                    </tr>

                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td><?php echo $text_info_title; ?></td>
                            <td>
                                <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                                <?php $kepzett_valtozo = 'mymodule_title2'.$language['language_id'].$aruhaz_id?>
                                <input type="text" name="mymodule_title2<?php echo $language['language_id'].$aruhaz_id; ?>"  size="30" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''?>" />
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                            </td>

                         </tr>
                    <?php } ?>



                    <tr>
                        <td>Vezetékes telefon</td>
                        <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>

                        <?php $kepzett_valtozo = 'tg_helios_footer_contact_phone'.$aruhaz_id?>
                        <td><input type="text" name="<?php echo $kepzett_valtozo?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="30" />
                            <?php $kepzett_valtozo = 'tg_helios_footer_contact_phone_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo?>" <?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' checked="checked"';?> /> Show</td>
                    </tr>

                    <tr>
                        <td>Mobil telefon</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_contact_mobile'.$aruhaz_id?>
                        <td><input type="text" name="tg_helios_footer_contact_mobile<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="30" />
                            <?php $kepzett_valtozo = 'tg_helios_footer_contact_mobile_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="tg_helios_footer_contact_mobile_show<?php echo $aruhaz_id;?>" <?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' checked="checked"';?> /> Show</td>
                    </tr>

                    <tr>
                        <td>Email</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_contact_email'.$aruhaz_id?>
                        <td><input type="text" name="tg_helios_footer_contact_email<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="30" />
                            <?php $kepzett_valtozo = 'tg_helios_footer_contact_email_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="tg_helios_footer_contact_email_show<?php echo $aruhaz_id;?>" <?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' checked="checked"';?> /> Show</td>
                    </tr>

                    <tr>
                        <td>Skype</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_contact_skype'.$aruhaz_id?>
                        <td><input type="text" name="tg_helios_footer_contact_skype<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="30" />
                            <?php $kepzett_valtozo = 'tg_helios_footer_contact_skype_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="tg_helios_footer_contact_skype_show<?php echo $aruhaz_id;?>" <?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' checked="checked"';?> /> Show</td>
                    </tr>

                    <tr>
                        <td>Cím</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_contact_address'.$aruhaz_id?>
                        <td><input type="text" name="tg_helios_footer_contact_address<?php echo $aruhaz_id;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="30" />
                            <?php $kepzett_valtozo = 'tg_helios_footer_contact_address_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="tg_helios_footer_contact_address_show<?php echo $aruhaz_id;?>" <?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' checked="checked"';?> /> Show</td>
                    </tr>

                </table>
            </div>
        <?php } ?>
    </div> <!-- tab-contact (end) -->


    <div id="tab-terkep">
        <div id="terkep-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#terkep-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="terkep-store<?php echo $store['store_id']; ?>">
                <table class="form">
                    <tr>
                        <td><?php echo $tab_terkep; ?></td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_terkep_status'.$aruhaz_id?>
                            <select name="<?php echo $kepzett_valtozo;?>">
                                <option value="1"<?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                <option value="0"<?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo != '1') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Sorszám:</td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_terkep_sort'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : '';; ?>" size="5" />

                            <span class="fuggoleges">Sorszám függőleges:</span>

                            <?php $kepzett_valtozo = 'tg_helios_footer_terkep_fuggoleges'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : '';; ?>" size="5" />
                        </td>
                    </tr>

                    <tr>
                        <td>Méretek:</td>
                        <td>Szélesség:
                            <?php $kepzett_valtozo = 'tg_helios_footer_terkep_szelesseg'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />

                            <span class="fuggoleges">Magasság:</span>

                            <?php $kepzett_valtozo = 'tg_helios_footer_terkep_magassag'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />
                        </td>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>


    <div id="tab-twitter">
        <div id="twitter-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#twitter-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
        <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="twitter-store<?php echo $store['store_id']; ?>">
                <table class="form">
                    <tr>
                        <td>Engedélyezett</td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_twitter_status'.$aruhaz_id?>
                            <select name="<?php echo $kepzett_valtozo;?>">
                                <option value="1"<?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                <option value="0"<?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo != '1') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Sorszám:</td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_twitter_sort'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />
                            <span class="fuggoleges">Sorszám függőleges:</span>
                            <?php $kepzett_valtozo = 'tg_helios_footer_twitter_fuggoleges'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />
                        </td>
                    </tr>

                    <tr>
                        <td>Méretek:</td>
                        <td>Szélesség:
                            <?php $kepzett_valtozo = 'tg_helios_footer_twitter_szelesseg'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />

                            <span class="fuggoleges">Magasság:</span>
                            <?php $kepzett_valtozo = 'tg_helios_footer_twitter_magassag'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />
                        </td>
                    </tr>


                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td><?php echo $text_info_title; ?></td>
                            <td>
                                <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                                <?php $kepzett_valtozo = 'mymodule_title3'.$language['language_id'].$aruhaz_id?>

                                <input type="text" name="<?php echo $kepzett_valtozo;?>"  size="30" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" />
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                            </td>
                        </tr>
                    <?php } ?>

                    <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
                    <tr>
                        <td> Tweets number: </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_twitter_tweets'.$aruhaz_id?>
                            <select name="<?php echo $kepzett_valtozo;?>">
                            <?php if (isset($tg_helios_footer_twitter_tweets)) { $selected = "selected"; ?>
                                <option value="1" <?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo == '1'){echo $selected;} ?>>1</option>
                                <option value="2" <?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo == '2'){echo $selected;} ?>>2</option>
                                <option value="3" <?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo == '3'){echo $selected;} ?>>3</option>
                            <?php } else { ?>
                                <option selected="selected"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>Felhasználó név:</td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_twitter_username'.$aruhaz_id?>
                        <td><input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="30" /></td>
                    </tr>

                </table>
            </div>
        <?php } ?>
    </div>


    <div id="tab-default">
        <div id="default-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#default-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
        <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="default-store<?php echo $store['store_id']; ?>">
                <table class="form">
                    <tr>
                        <td>Engedélyezett</td>
                            <td>
                                <?php $kepzett_valtozo = 'tg_helios_footer_facebook_fanpage_status'.$aruhaz_id?>
                                <select name="<?php echo $kepzett_valtozo;?>">
                                    <option value="1"<?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                    <option value="0"<?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo != '1') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                                </select>
                            </td>
                    </tr>
                    <tr>
                        <td>Sorszám:</td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_fanpage_sort'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />
                            <span class="fuggoleges">Sorszám függőleges:</span>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_fanpage_fuggoleges'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />
                        </td>
                    </tr>

                    <tr>
                        <td>Méretek:</td>
                        <td>Szélesség:
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_fanpage_szelesseg'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />

                            <span class="fuggoleges">Magasság:</span>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_fanpage_magassag'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />
                        </td>
                    </tr>

                    <?php foreach ($languages as $language) { ?>
                        <tr>
                            <td><?php echo $text_info_title; ?></td>
                            <td>
                                <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                                <?php $kepzett_valtozo = 'mymodule_title4'.$language['language_id'].$aruhaz_id?>
                                <input type="text" name="<?php echo $kepzett_valtozo;?>"  size="30" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" />
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                            </td>

                        </tr>
                    <?php } ?>

                    <tr>
                        <td>ID:</td>
                        <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
                        <?php $kepzett_valtozo = 'tg_helios_footer_facebook_fanpage_id'.$aruhaz_id?>
                        <td><input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="30" /></td>
                    </tr>

                </table>
            </div>
        <?php } ?>
    </div>


    <div id="tab_facebook_like_button">
        <div id="facebook_like_button-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#facebook_like_button-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="facebook_like_button-store<?php echo $store['store_id']; ?>">
                <table class="form">
                    <tr>
                        <td><?php echo $tab_facebook_like_button; ?></td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb_status'.$aruhaz_id?>
                        <select name="<?php echo $kepzett_valtozo;?>">
                            <option value="1"<?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                            <option value="0"<?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo != '1') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                        </select>


                        </td>
                    </tr>
                    <td>Sorszám:</td>
                    <td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb_sort'.$aruhaz_id?>
                        <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />
                        <span class="fuggoleges">Sorszám függőleges:</span>
                        <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb_fuggoleges'.$aruhaz_id?>
                        <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />
                    </td>

                    <tr>
                        <td>Méretek:</td>
                        <td>Szélesség:
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb_szelesseg'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />

                            <span class="fuggoleges">Magasság:</span>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb_magassag'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb0'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb0_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>

                    </tr>

                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb1'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb1_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb2'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb2_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb3'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb3_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb4'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb4_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb5'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb5_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb6'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb6_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb7'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb7_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb8'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb8_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb9'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb9_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb10'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>"  cols="40" rows="5"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_facebook_gomb10_show'.$aruhaz_id?>
                            <input type="checkbox" value="1" name="<?php echo $kepzett_valtozo;?>" <?php if(isset($$kepzett_valtozo) ? $$kepzett_valtozo : '' == '1') echo ' checked="checked"';?> /> Show
                        </td>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>

    <div id="tab_egyeb">
        <div id="egyeb-stores" class="htabs">
            <?php foreach ($stores as $store) { ?>
                <a href="#egyeb-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
            <?php } ?>
        </div>

        <?php foreach ($stores as $store) { ?>
            <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
            <div id="egyeb-store<?php echo $store['store_id']; ?>">

                <table class="form">
                    <tr>
                        <td><?php echo $tab_egyeb; ?></td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_egyeb_status'.$aruhaz_id?>
                            <select name="<?php echo $kepzett_valtozo;?>">
                                <option value="1"<?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                <option value="0"<?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo != '1') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                            </select>
                        </td>
                    </tr>
                    <td>Sorszám:</td>
                    <td>
                        <?php $kepzett_valtozo = 'tg_helios_footer_egyeb_sort'.$aruhaz_id?>
                        <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />
                        <span class="fuggoleges">Sorszám függőleges:</span>
                        <?php $kepzett_valtozo = 'tg_helios_footer_egyeb_fuggoleges'.$aruhaz_id?>
                        <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="5" />
                    </td>

                    <tr>
                        <td>Méretek:</td>
                        <td>Szélesség:
                            <?php $kepzett_valtozo = 'tg_helios_footer_egyeb_szelesseg'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />

                            <span class="fuggoleges">Magasság:</span>
                            <?php $kepzett_valtozo = 'tg_helios_footer_egyeb_magassag'.$aruhaz_id?>
                            <input type="text" name="<?php echo $kepzett_valtozo;?>" value="<?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?>" size="10" />
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <?php $kepzett_valtozo = 'tg_helios_footer_egyeb'.$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>" cols="40" rows="15"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>
</div>

<!-- tab_masodlagos_footer (end) -->


<div id="tab-sidebar">
    <div id="sidebar-stores" class="htabs">
        <?php foreach ($stores as $store) { ?>
            <a href="#sidebar-store<?php echo $store['store_id']; ?>"> <?php echo $store['name']; ?></a>
        <?php } ?>
    </div>

    <?php foreach ($stores as $store) { ?>
        <?php $aruhaz_id = $store['store_id'] ? $store['store_id'] : ''?>
        <div id="sidebar-store<?php echo $store['store_id']; ?>">

            <table class="form1">
                <tr>
                    <td style="padding-right: 30px;"><?php echo $tab_sidebar; ?></td>
                    <td style="padding-right: 30px;">
                        <?php $kepzett_valtozo = 'sidebar_code_status'.$aruhaz_id?>
                        <select name="<?php echo $kepzett_valtozo;?>">
                            <option value="1"<?php if (isset($$kepzett_valtozo) && $$kepzett_valtozo == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                            <option value="0"<?php if(isset($$kepzett_valtozo) && $$kepzett_valtozo != '1') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                        </select>
                    </td>
                </tr>

                <?php foreach ($languages as $language) { ?>
                    <tr>
                        <td>
                            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                            <?php echo $entry_code; ?></td>
                        <td>
                            <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>
                            <?php $kepzett_valtozo = 'sidebar_code'.$language['language_id'].$aruhaz_id?>
                            <textarea  name="<?php echo $kepzett_valtozo;?>" cols="40" rows="15"><?php echo isset($$kepzett_valtozo) ? $$kepzett_valtozo : ''; ?></textarea>
                        </td>

                    </tr>
                <?php } ?>

            </table>
        </div>
    <?php } ?>
</div>


</form> <!-- form action (end) -->
</div> <!-- content (end) -->
</div> <!-- box (end) -->


<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--

    <?php foreach ($stores as $store) { ?>
        <?php $aruhaz_id = $store['store_id'] ? '_'.$store['store_id'] : ''?>

        <?php foreach ($languages as $nyelv) { ?>
            CKEDITOR.replace('mymodule_code<?php echo $nyelv['language_id'].$aruhaz_id; ?>', {
                filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
            });

            CKEDITOR.replace('ugyfelszol_code<?php echo $nyelv['language_id'].$aruhaz_id; ?>', {
                filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
            });

            CKEDITOR.replace('extrak_code<?php echo $nyelv['language_id'].$aruhaz_id; ?>', {
                filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
            });
            CKEDITOR.replace('fiok_code<?php echo $nyelv['language_id'].$aruhaz_id; ?>', {
                filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
            });
            CKEDITOR.replace('elerhetoseg_code<?php echo $nyelv['language_id'].$aruhaz_id; ?>', {
                filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
            });
            CKEDITOR.replace('sidebar_code<?php echo $nyelv['language_id'].$aruhaz_id; ?>', {
                filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
            });


        <?php } ?>
    <?php } ?>

    CKEDITOR.replace('sidebar_code', {
        filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
    });

//--></script> 
<script type="text/javascript" src="view/javascript/jquery/ui/ui.draggable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.resizable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.dialog.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/jquery.bgiframe.js"></script>
<script type="text/javascript"><!--
function image_upload(field, preview) {
	$('#dialog').remove();

	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&field=' + encodeURIComponent(field) + '&token=<?php echo $this->session->data['token']; ?>" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $this->session->data['token']; ?>',
					type: 'POST',
					data: 'image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(data) {
						$('#' + preview).replaceWith('<img src="' + data + '" alt="" id="' + preview + '" style="border: 1px solid #EEEEEE;" onclick="image_upload(\'' + field + '\', \'' + preview + '\');" />');
					}
				});
			}
		},
		bgiframe: false,
		width: 800,
		height: 520,
		resizable: false,
		modal: false
	});
};
//--></script>

<script type="text/javascript"><!--
$('#tabs a').tabs(); 
$('#languages a').tabs(); 
$('#vtab-option a').tabs();
$('#fttab a').tabs();
$('#general-stores a').tabs();
$('#informaciok-stores a').tabs();
$('#kategoriak-stores a').tabs();
$('#ugyfelszolgalat-stores a').tabs();
$('#extrak-stores a').tabs();
$('#fiok-stores a').tabs();
$('#elerhetoseg-stores a').tabs();
$('#uzenet-stores a').tabs();
$('#hirlevel-stores a').tabs();
$('#likebox-stores a').tabs();
$('#info-stores a').tabs();
$('#contact-stores a').tabs();
$('#terkep-stores a').tabs();
$('#twitter-stores a').tabs();
$('#default-stores a').tabs();
$('#facebook_like_button-stores a').tabs();
$('#egyeb-stores a').tabs();
$('#sidebar-stores a').tabs();


//--></script>