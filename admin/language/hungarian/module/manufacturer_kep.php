<?php
// Heading
$_['heading_title']       = 'Gyártó képek';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeres módosítás';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal Oszlop';
$_['text_column_right']   = 'Jobb Oszlop';
$_['text_horizontal']     = 'Vízszintes';
$_['text_vertical']       = 'Függőleges';

// Entry
$_['entry_limit']        = 'Limit:';
$_['entry_scroll']       = 'Görgetés:';
$_['entry_dimension']    = 'Méret (SZ x M):';
$_['entry_image']        = 'Kép (SZ x M):';
$_['entry_axis']         = 'Tengely:';
$_['entry_layout']       = 'Kiosztás:';
$_['entry_position']     = 'Pozició:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';

// Error
$_['error_permission']   = 'Nincs jogosultsága a módosításra!';
$_['error_dimension']    = 'Méret szélesség &amp; magasság méretek szükségesek!';
$_['error_image']        = 'Kép Szélesség &amp; Magasság szükséges!';
?>