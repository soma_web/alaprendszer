<?php
class ModelPaymentKandH extends Model 
{
	public function getMethod($address, $total) 
  	{
		$this->load->language('payment/kandh');
		
		if (($total > 0 && $this->config->get('kandh_is_test'))
			|| ($total > 0 && $this->config->get('kandh_is_test'))
			|| ($total > 0 && !$this->config->get('kandh_is_test'))
		) 
		{
			$status = true;
		} 
		else 
		{
			$status = false;
		}
		
		$method_data = array();
			
		if ($status) 
		{  
			$method_data = array( 
				'code'       => 'kandh',
				'title'      => $this->language->get('text_title'),
				'sort_order' => $this->config->get('kandh_sort_order')
			);
		}
		
    	return $method_data;
  	}
	
}
?>