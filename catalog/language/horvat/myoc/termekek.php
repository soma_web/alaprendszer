<?php
// Heading
$_['heading_title']     	= 'Termékek';

// Header Bar Title
$_['text_all_products'] 	= 'Összes termék';

// Text
$_['text_pricelist']    	= 'Termékek';
$_['text_limit']        	= 'Termék / oldal:';
$_['text_empty']        	= 'Nem található termék';
$_['text_category']     	= 'Kategória';
$_['text_sku']          	= 'SKU:';
$_['text_upc']          	= 'UPC:';
$_['text_print']        	= 'Nyomtatás';
$_['text_notfound']     	= 'A termék nem található!';
$_['text_discount']     	= '%s vagy több: %s';

//Table Columns
$_['column_number']     	= 'No.';
$_['column_image']      	= 'Kép';
$_['column_name']       	= 'Név';
$_['column_model']      	= 'Model';
$_['column_rating']      	= 'Értékelés';
$_['column_price']      	= 'Ára';
$_['column_stock']      	= 'Törzs';
$_['column_qty']        	= 'Menny.';
$_['column_action']     	= 'Vásárlás';

//Errors
$_['error_customer_group'] 	= 'Your current customer account does not have the necessary privileges to view the price list.';
?>