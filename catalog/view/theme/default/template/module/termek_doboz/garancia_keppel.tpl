<?php if (isset($product['text_garancia']) && $product['text_garancia'] && isset($product['garancia_kep']) && $product['garancia_kep']) { ?>

    <?php
    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/'.$product['garancia_kep'])) {
        $garancia_kep = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/'.$product['garancia_kep'];
    } elseif(file_exists(DIR_TEMPLATE_IMAGE.'default/image/'.$product['garancia_kep'])) {
        $garancia_kep = DIR_TEMPLATE_IMAGE.'default/image/'.$product['garancia_kep'];
    }
    ?>

    <?php if ($product['garancia_description']) { ?>
        <div class="garancia_kep" style="cursor: pointer">
            <a class="various" href="#garancia_list<?php echo $product['product_id']?>" >
                <img style="width: 150px;" src="<?php echo $garancia_kep; ?>" title="<?php echo $product['text_garancia'] ?>" alt="<?php echo $product['text_garancia'] ?>" />
            </a>
        </div>

        <div id="garancia_list<?php echo $product['product_id']?>" class=""
            style="display: none; min-width: 320; min-height: 150px; background-color: white;">
            <?php echo $product['garancia_description']?>
        </div>

    <?php } else { ?>
        <div class="garancia_kep" >
            <a style="cursor: default">
                <img style="width: 150px;" src="<?php echo $garancia_kep; ?>" title="<?php echo $product['text_garancia'] ?>" alt="<?php echo $product['text_garancia'] ?>" />
            </a>
        </div>
    <?php } ?>

<?php } ?>