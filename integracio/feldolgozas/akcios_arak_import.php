<?php
class Akcios_Arak_Import {              /* SAP_vevok segédtábla feldolgozása a:
                                                product
                                                customer_group
                                                product_special        táblákba.
                                        */

    private $query;
    private $vevo_id;
    public $azonosito;
    public $csvadatok;
    public $sap_vevocsoport;

    public function __construct($query) {
        $this->query = $query;
        $this->csvadatok=$this->query->query("SELECT * FROM sap_akcios_arak");
    }

    public function Akcios_Arak($sorok) {
        $sap_cikkszam=$sorok['cikkszam'];

        $sql="SELECT * FROM ".DB_PREFIX."product WHERE cikkszam LIKE '$sap_cikkszam'";
        $query= $this->query->query($sql);

        if (isset($query->num_rows) && $query->num_rows > 0){
            $azonosito=$query->row["product_id"];
            $this->Akcios_Arak_Felir($sorok,$azonosito);

        } else {
            $m_datum = date('Y-m-d');
            $modosit="INSERT INTO ".DB_PREFIX."product SET
                                                product_id          = '',
                                                cikkszam            = '$sap_cikkszam',
                                                date_added          = '$m_datum',
                                                minimum             = '1',
                                                quantity            = '0',
                                                subtract            = '0',
                                                status              = '0' ";
            mysql_query($modosit) or Hibalista("Akciós árak", "product", "felvitel", $sap_cikkszam);

            $sql="SELECT * FROM ".DB_PREFIX."product WHERE cikkszam LIKE '$sap_cikkszam'";
            $query= $this->query->query($sql);
            $azonosito=$query->row['product_id'];
            $this->Akcios_Arak_Felir($sorok,$azonosito);
        }
    }

    public function Vevo_Csoport_Akcios($sap_vevocsoport){
        $sql="SELECT * FROM ".DB_PREFIX."customer_group WHERE csoportkod LIKE '$sap_vevocsoport'";
        $query= $this->query->query($sql);
        if (isset($query->num_rows) && $query->num_rows > 0){
            $this->sap_vevocsoport= $query->row['customer_group_id'];
        } else {
            $modosit="INSERT INTO ".DB_PREFIX."customer_group SET
                                                `customer_group_id`     =   '',
                                                `csoportkod`            =   '$sap_vevocsoport',
                                                `name`                  =   '$sap_vevocsoport'";

            //mysql_query($modosit) or die("Nem sikerült a customer_group táblában az új vevő csoport létrehozása");
            mysql_query($modosit) or Hibalista("Akciós árak", "customer_group", "felvitel", $sap_vevocsoport);

            $sql="SELECT * FROM ".DB_PREFIX."customer_group WHERE csoportkod LIKE '$sap_vevocsoport'";
            $query= $this->query->query($sql);
            $this->sap_vevocsoport= $query->row['customer_group_id'];
        }

    }
    public function Akcios_Arak_Felir($sorok,$azonosito){
        $sap_ar         =   $sorok['kedvezmenyes_ar'];
        $sap_datum_tol  =   datumVissza($sorok['kedvezmeny_kezdete']);
        $sap_datum_ig   =   datumVissza($sorok['kedvezmeny_vege']);
        $sap_vevokod    =   $sorok['vevokod'];
        $mehet          =   true;

        if (!empty($sorok['vevokod']) && $sorok['vevokod'] != "W00001"){
            $mehet=false;
            $this->VevokFelvisz($sap_vevokod);
            $sql="SELECT * FROM ".DB_PREFIX."product_customer_special WHERE product_id = $azonosito AND customer_id =$this->vevo_id AND quantity=0";
            $query= $this->query->query($sql);
            if (isset($query->num_rows) && $query->num_rows > 0){
                $modosit="UPDATE ".DB_PREFIX."product_customer_special SET
                                                `price`                 =   '$sap_ar',
                                                `date_start`	        =   '$sap_datum_tol',
                                                `date_end`              =   '$sap_datum_ig'
                           WHERE product_id = $azonosito AND customer_id =$this->vevo_id AND quantity=0";
                mysql_query($modosit) or Hibalista("Akciós árak", "product_customer_special","módosítás", $azonosito, $sap_vevokod);
            } else {
                $modosit="INSERT INTO ".DB_PREFIX."product_customer_special SET
                                                `product_id`            =   '$azonosito',
                                                `customer_id`	        =   '$this->vevo_id',
                                                `quantity`	            =   0,
                                                `price`                 =   '$sap_ar',
                                                `date_start`	        =   '$sap_datum_tol',
                                                `date_end`              =   '$sap_datum_ig' ";
                mysql_query($modosit) or Hibalista("Akciós árak", "product_customer_special","felvitel", $azonosito, $sap_vevokod);
            }

        } elseif (!empty($sorok['vevocsoport'])) {
            $this->Vevo_Csoport_Akcios($sorok['vevocsoport']);

        } else{
            $this->sap_vevocsoport=8;
        }
        if ($mehet){
            $sql="SELECT * FROM ".DB_PREFIX."product_special WHERE product_id = $azonosito AND customer_group_id=$this->sap_vevocsoport";
            $query= $this->query->query($sql);
            if (isset($query->num_rows) && $query->num_rows > 0){

                if (!empty($query->row['date_end']) && $query->row['date_end'] != "0000-00-00"){
                    if ($sap_datum_tol > $query->row['date_end']) $milesz="felvitel";
                    elseif ($sap_datum_ig < $query->row['date_start'])     $milesz="felvitel";
                    else $milesz="modositas";

                } else  $milesz="modositas";

                $milesz="modositas";
                if ($milesz=="modositas"){
                    $modosit="UPDATE ".DB_PREFIX."product_special SET
                                                `price`                 =   '$sap_ar',
                                                `date_start`	        =   '$sap_datum_tol',
                                                `date_end`              =   '$sap_datum_ig'
                           WHERE product_id = $azonosito AND customer_group_id=$this->sap_vevocsoport";
                    mysql_query($modosit) or Hibalista("Akciós árak", "product_special","módosítás", $azonosito, $this->sap_vevocsoport);

                } else {
                    $modosit="INSERT INTO ".DB_PREFIX."product_special SET
                                                `product_special_id`   =   '',
                                                `product_id`            =   '$azonosito',
                                                `customer_group_id`	    =   '$this->sap_vevocsoport',
                                                `priority`              =   '0',
                                                `price`                 =   '$sap_ar',
                                                `date_start`	        =   '$sap_datum_tol',
                                                `date_end`              =   '$sap_datum_ig' ";
                    mysql_query($modosit) or Hibalista("Akciós árak", "product_special", "felvitel", $azonosito, $this->sap_vevocsoport);

                }
            } else {
                $modosit="INSERT INTO ".DB_PREFIX."product_special SET
                                                `product_special_id`   =   '',
                                                `product_id`            =   '$azonosito',
                                                `customer_group_id`	    =   '$this->sap_vevocsoport',
                                                `priority`              =   '0',
                                                `price`                 =   '$sap_ar',
                                                `date_start`	        =   '$sap_datum_tol',
                                                `date_end`              =   '$sap_datum_ig' ";
                mysql_query($modosit) or Hibalista("Akciós árak", "product_special", "felvitel", $azonosito, $this->sap_vevocsoport);

            }
        }
        //mysql_query($modosit) or die("Nem sikerült a product_special tábla módosítása");
    }

    private function VevokFelvisz($sap_vevokod) {
        $sap_datum              = date('Y-m-d');

        $sql="SELECT * FROM ".DB_PREFIX."customer WHERE vevokod LIKE '$sap_vevokod'";
        $query= $this->query->query($sql);

        if (isset($query->num_rows) && $query->num_rows > 0){
            $this->vevo_id=$query->row['customer_id'];
        } else {
            $modosit="INSERT INTO ".DB_PREFIX."customer SET
                                                        customer_id	        = '',
                                                        vevokod             = '$sap_vevokod',
                                                        store_id	        = '0',
                                                        firstname	        = '',
                                                        lastname            = '',
                                                        email	            = '',
                                                        telephone	        = '',
                                                        password	        = '',
                                                        cart	            = '',
                                                        wishlist	        = '',
                                                        newsletter	        = 0,
                                                        customer_group_id	= '',
                                                        ip	                = '',
                                                        status	            = '0',
                                                        approved	        = '1',
                                                        adoszam	            = '',
                                                        date_added	        = '$sap_datum'";
            mysql_query($modosit) or   Hibalista("Akciós árak", "customer", "felvitel",  $sap_vevokod);


            $sql="SELECT * FROM ".DB_PREFIX."customer WHERE vevokod LIKE '$sap_vevokod'";
            $query= $this->query->query($sql);
            $this->vevo_id=$query->row['customer_id'];
        }
    }

}