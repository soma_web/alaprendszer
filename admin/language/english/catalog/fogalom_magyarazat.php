<?php
// Heading
$_['heading_title']     = 'Glossary';

// Text
$_['text_success']      = 'Success: You have modified glossary!';
$_['text_default']      = 'Default';

// Column
$_['column_title']      = 'Concept Title';
$_['column_action']     = 'Action';

// Entry
$_['entry_title']       = 'Concept:';
$_['entry_description'] = 'Description:';
$_['entry_status']      = 'Status:';

// Error 
$_['error_warning']     = 'Warning: Please check the form carefully for errors!';
$_['error_permission']  = 'Warning: You do not have permission to modify information!';
$_['error_title']       = 'Information Title must be between 3 and 64 characters!';
$_['error_description'] = 'Description must be between 3 characters!';
?>