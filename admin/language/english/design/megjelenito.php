<?php
$_['heading_title']                = 'Product display';
$_['tab_data']                     = 'Grid wiev';
$_['tab_view']                     = 'Wiev';
$_['tab_kosar']                    = 'Cart - Payment';
$_['tab_frontend_top']             = 'Frontend Settings';
$_['tab_backend_top']              = 'Backend Settings';
$_['tab_fektetett']                = 'Invested grid view';
$_['tab_allitott']                 = 'Set grid view';
$_['tab_lista']                    = 'List wiev';
$_['tab_altalanos']                = 'General settings';
$_['tab_altalanosadmin']           = 'Admin settings';
$_['tab_frontend']                 = 'Admin product list settings';
$_['tab_product']                  = 'Product subpage view';
$_['tab_nomodul']                  = 'Allow modules';
$_['tab_alapmodulok']              = 'Basic modules';
$_['tab_szallitas']                = 'Delivery';
$_['tab_fizetes']                  = 'Pay';
$_['tab_termekful']                = 'Product tab';
$_['tab_termektulajdonsag']        = 'Product properties';
$_['tab_rendeles']                 = 'Orders';
$_['tab_regisztracio']             = 'Registration';
$_['tab_regisztracio_ceges']       = 'Company Registration';
$_['tab_header']                   = 'Header';
$_['tab_regisztracio_elolvastam']  = 'Account terms';
$_['text_ujdonsag']                = 'Novelty input enable';
$_['tab_vevok']                    = 'Customers';
$_['tab_export']                   = 'Export/Import';
$_['text_csv_vevok_title']         = 'CSV Export';
$_['text_csv_vevok_setting']       = 'Customer CSV export enable';
$_['text_group_name']              = 'Group name:';


$_['entry_kiszereles']                 = 'Enable Packings:';
$_['entry_kiszerelesmertek']           = 'Enable Packaging Unit:';
$_['entry_nokep_megjelenites_tiltasa'] = 'No-image disable display:';
$_['entry_kep_megjelenites_tiltasa']   = 'Image disable display:';
$_['entry_admin_altalanos_tartozekok'] = 'Enable general accessories:';


$_['entry_emailreg_jelszo']                        = 'Disable password';

$_['entry_admin_csomag_ajanlat']                    = 'Enable package';
$_['entry_admin_rovid_leiras']                      = 'Enable short description';
$_['entry_admin_tesztek']                           = 'Enable tests';
$_['entry_admin_tartozekok']                        = 'Enable accessories';

$_['entry_admin_letoltheto']          = 'Related download image display:';
$_['entry_allitott']                = 'Established view';
$_['entry_kosarba']                 = 'Cart button';
$_['entry_mennyisegre_szur']        = 'Filtering queries quantity:';
$_['entry_ervenesseg_datumra_szur'] = 'Filtering queries the validity date:';
$_['entry_brutto_ar']               = 'Burtto price';
$_['entry_netto_ar']                = 'Netto price';
$_['entry_negativ_ar']              = 'Negative price display';
$_['entry_brutto_netto']            = 'Order: Brutto -> Netto';
$_['entry_description']             = 'Product description:';
$_['entry_ikonok']                  = 'Icons:';
$_['entry_ikon_helye']              = 'display In:';
$_['entry_uj']                      = 'New icon:';
$_['entry_pardarab']                = 'Few piece icon:';
$_['entry_szazalek']                = 'Percentage icon';
$_['entry_mennyiseg']               = 'Quantity select cart';
$_['entry_csomagolasi_mennyiseg']   = 'Packing quantity';
$_['entry_slideshow']               = 'Full width slideshow:';
$_['entry_kep_szoveg']              = 'Order: Image -> Product name:';
$_['entry_csomagolas_admin']        = 'Product admin packing enabled:';
$_['entry_szukitett_regisztracio']  = 'Enable Reduced registration:';
$_['entry_vonalkod_kuldes']         = 'Display: Send barcode:';
$_['entry_netto_ujsor']             = 'Separate netto price and price:';
$_['entry_jobb_egerbolt']           = 'Disable the right mouse button:';
$_['entry_masolhato']               = 'Disable extraction sites:';
$_['entry_kivansaglista']           = 'Wish list display:';
$_['entry_osszehasonlitas']         = 'Compare display:';
$_['entry_kiskepek_megjelenitese']  = 'During large image thumbnails:';
$_['entry_footer_kategoria']        = 'Footers bottom category list';
$_['entry_pagination']              = 'Show Page Numbering in the top row:';
$_['entry_termek_jobb_jobb_elso']       = 'Wish List, Compare:';
$_['entry_termek_jobb_jobb_masodik']    = 'Tweet, Facebook:';
$_['entry_termek_jobb_jobb_harmadik']   = 'Print Display:';
$_['entry_termek_neve_fejlec']          = 'Product Name header to display:';
$_['entry_product_model_termeknevben']  = 'Model before header to display:';
$_['entry_termek_neve_dobozban']        = 'Product name display:';
$_['entry_termek_kaphato']              = 'Available from how long to display:';
$_['entry_termek_filter_csoport']       = 'Filter group show:';
$_['entry_termek_filter']               = 'Filter show:';
$_['entry_frontend_input_neve']         = 'Field name:';
$_['entry_admin_eredeti_ar']            = 'Original price:';
$_['entry_form_eredeti_ar']             = 'Original price field display:';
$_['entry_form_utalvany']               = 'Voucher field display:';
$_['entry_utalvany_sorrendben']         = 'Utalvány mező szerint is rendez (0/1):';
$_['entry_utalvany_sorrendben']         = 'Voucher is organized according to field (0/1):';
$_['entry_utalvany_sorrendben_asc']     = 'DESC:';
$_['entry_fixmenu']                     = 'Fix Menu:';
$_['entry_meddig_kaphato']              = 'Admin display available to date:';
$_['entry_mikortol_kaphato']            = 'Admin display available since date:';
$_['entry_form_termek_elhelyezkedes']   = 'Enable Item Location ascending:';
$_['entry_termek_kiemeles_modositasa']  = 'Product highlight insertion, deletion Enable:';


$_['entry_product_leiras']                  = 'Product description next to image';

$_['entry_product_koztes']                  = 'Display between Pics - Tabs:';
$_['entry_product_garancia']                = 'Guarantee:';
$_['entry_product_szallitas']               = 'Shipping:';
$_['entry_product_velemeny']                = 'Review:';
$_['entry_product_osszehasonlit_kivansag']  = 'Compare, wishlist:';
$_['entry_product_addthis']                 = 'Addthis (social icons):';

$_['entry_product_mennyisegi_ikon']             = 'Quantity discount icon:';
$_['entry_product_velemeny_megosztas_doboz']    = 'Review Share box:';

$_['entry_termek_quantity']            = 'Quantity selecting cart:';
$_['entry_termek_social']              = 'Social ikonos display:';
$_['entry_termek_leiras']              = 'Show off tabs on Teme description:';
$_['entry_termek_kapcsolod_allitott']  = 'Related products set to show:';

$_['entry_kosarba_fk']                 = 'Cart button';
$_['entry_brutto_ar_fk']               = 'Brutto price';
$_['entry_netto_ar_fk']                = 'Netto price';
$_['entry_brutto_netto_fk']            = 'Order: Brutto -> Netto';
$_['entry_description_fk']             = 'Product description:';
$_['entry_ikonok_fk']                  = 'Icons:';
$_['entry_ikon_helye_fk']              = 'display In:';
$_['entry_uj_fk']                      = 'New icon:';
$_['entry_pardarab_fk']                = 'Few piece icon:';
$_['entry_szazalek_fk']                = 'Percentage icon';
$_['entry_mennyiseg_fk']               = 'Quantity select cart';
$_['entry_csomagolasi_mennyiseg_fk']   = 'Packing quantity';
$_['entry_slideshow_fk']               = 'Full width slideshow:';
$_['entry_kep_szoveg_fk']              = 'Order: Image -> Product name:';
$_['entry_csomagolas_admin_fk']        = 'Product admin packing enabled:';
$_['entry_jobb_egerbolt_fk']           = 'Disable the right mouse button:';
$_['entry_masolhato_fk']               = 'Disable extraction sites:';
$_['entry_kivansaglista_fk']           = 'Wish list display:';
$_['entry_osszehasonlitas_fk']         = 'Compare display:';
$_['entry_termek_kapcsolod_group_filter']= 'Filter group -> filter display:';
$_['entry_facebook_fk']                = 'Display facebook share button:';
$_['entry_email_fk']                   = 'Display email send button:';
$_['entry_minicart_fk']                = 'Small cart icon:';
$_['entry_megnezemgomb_fk']            = 'Show button';


$_['ikon_fent_vizszintes']  = "Top horizontal box layout";
$_['ikon_fent_fuggoleges']  = "Top box with vertical layout ";
$_['ikon_kep_felett']       = "Top horizontal image layout";
$_['ikon_kep_mellett']       = "In addition to vertical layout image";
$_['entry_admin_szazalek']               = 'Price show percentage Reply:';
$_['entry_elhelyezkedes']              = 'Location - Disable boost:';



$_['entry_lista_kosarba']                 = 'Cart button';
$_['entry_lista_brutto_ar']               = 'Burtto price';
$_['entry_lista_netto_ar']                = 'Netto price';
$_['entry_lista_brutto_netto']            = 'Order: brutto - netto';
$_['entry_lista_description']             = 'Product description:';
$_['entry_lista_uj']                      = 'Új icon:';
$_['entry_lista_pardarab']                = 'Few piece icon:';
$_['entry_lista_szazalek']                = 'Percentage icon';
$_['entry_lista_mennyiseg']               = 'Quantity select cart';
$_['entry_lista_csomagolasi_mennyiseg']   = 'Packing quantity';
$_['entry_lista_kivansaglista']           = 'Wish list display:';
$_['entry_lista_osszehasonlitas']         = 'Compare display:';
$_['entry_lista_mennyisegi']              = 'Quantity discount:';

$_['entry_product_jelentkezzen']         = 'Sign in seems to notice:';
$_['entry_product_elerhetoseg']          = 'Availability seems to notice:';
$_['entry_product_suly']                 = 'Weight display:';
$_['entry_product_mennyisegi_egyseg']    = 'Unit of Measure display:';
$_['entry_product_cikkszam']             = 'Product number:';
$_['entry_product_cikkszam2']             = 'Product number 2:';
$_['entry_product_model']                = 'Product model:';
$_['entry_product_gyarto']               = 'Show manufacturer:';
$_['entry_product_reward']               = 'Reward:';
$_['entry_product_sku']                  = 'SKU:';
$_['entry_product_meret']                = 'Size:';
$_['entry_vizjel']                       = 'Sign on Water:';
$_['entry_admin_nyelv_ellenorzes']       = 'Admins enable language control:';
$_['entry_tab_videok']                   = 'Disable video tab:';
$_['entry_tab_pdf']                      = 'Disable pdf upload tab:';


$_['entry_admin_ar_ellenorzes']              = 'Allow admins price control:';
$_['entry_admin_ketszintu_szuro_ellenorzes'] = 'Allow admins dual-stage filtration:';
$_['entry_admin_ervenyes_ig_ellenorzes']     = 'Allow admins Expiry Date field control:';

$_['entry_admin_sorrend']                = 'Order:';


$_['entry_product_sorrend']              = 'Order:';
$_['entry_kosar_kupon']          = 'Cart: Kupon, shipping calculations show:';
$_['entry_kosar_suly']           = 'Cart: Weight display header:';
$_['entry_penztar_fizetesi_mod']        = 'Checkout: Payment method show:';
$_['entry_model_adat']                  = 'Checkout: Confirm Order: model';
$_['entry_netto_adat']                  = 'Checkout: Confirm Order: netto';
$_['entry_admin_cart_shipping_region']      = 'Cart - Estimate Shipping & Taxes - display region';
$_['entry_admin_cart_shipping_postcode']    = 'Cart - Estimate Shipping & Taxes - display postcode';
$_['entry_penznem_kiiras']              = 'Display full name currency';
$_['entry_category_leiras']             = 'Category: Description show:';
$_['entry_category_kep_fejlecben']      = 'Category: Show header image:';
$_['entry_piacter']                     = 'Marketplace is On:';
$_['entry_mini_cart']                   = 'Mini cart button to display text:';
$_['entry_vatera']                      = 'Vatera module enabled';
$_['entry_description_karakter_szam']   = 'Number of characters to be displayed:';
$_['entry_gorgetosav']                   = 'Carousel and scrolling display on the modules admin:';
$_['entry_sap']                          = 'SAP module enable:';
$_['entry_gls']                          = 'GLS Package Point can be enabled';
$_['entry_ellenorzes']                  = 'Control:';

$_['entry_admin_model']                 = 'Admin product display model: list';
$_['entry_admin_cikkszam']              = 'Admin product display item number: list';
$_['entry_admin_cikkszam2']             = 'Admin product display item number II: list';

$_['entry_form_model']                  = 'Admin product display model: form';
$_['entry_form_cikkszam']               = 'Admin product display item number: form';
$_['entry_form_cikkszam2']              = 'Admin product display item number II: form';

$_['entry_admin_keres_model']            = 'Search by model';
$_['entry_admin_keres_cikkszam']         = 'Search by cikkszám:';
$_['entry_admin_keres_cikkszam2']        = 'Search by cikkszám2:';

$_['entry_frontend_feltoltes']           = 'Added frontend product:';
$_['entry_product_sorrend_fk']              = 'Order:';
$_['entry_kosar_kupon_fk']          = 'Cart: Kupon, shipping calculations show:';
$_['entry_kosar_suly_fk']           = 'Cart: Weight display header:';
$_['entry_penztar_fizetesi_mod_fk']        = 'Checkout: Payment method show:';
$_['entry_category_leiras_fk']             = 'Category: Description show:';
$_['entry_category_kep_fejlecben_fk']      = 'Category: Show header image:';
$_['entry_piacter_fk']                     = 'Marketplace is On:';
$_['entry_mini_cart_fk']                   = 'Mini cart button to display text:';
$_['entry_vatera_fk']                      = 'Vatera module enabled';
$_['entry_description_karakter_szam_fk']   = 'Number of characters to be displayed:';
$_['entry_gorgetosav_fk']                   = 'Carousel and scrolling display on the modules admin:';
$_['entry_sap_fk']                          = 'SAP module enable:';



$_['ikon_fent_vizszintes']  = "Top of the box horizontal";
$_['ikon_fent_fuggoleges']  = "Top of the box vertical";
$_['ikon_kep_felett']       = "Top of the picture vertical";
$_['ikon_kep_mellett']      = "Next to the picture vertical";




$_['entry_stock_status_id']             = 'Stock status id:';
$_['entry_manufacturer_id']             = 'Manufacturer id:';
$_['entry_tax_class_id']                = 'Tax class id:';
$_['entry_weight_class_id']             = 'Weight class id:';
$_['entry_width']                       = 'Width:';
$_['entry_height']                      = 'Height:';
$_['entry_length_class_id']             = 'Length class id:';
$_['entry_date_added']                  = 'Date added:';
$_['entry_date_modified']               = 'Date modified:';
$_['entry_viewed']                      = 'Viewed:';
$_['entry_megyseg']                     = 'Megyseg:';
$_['entry_csomagolasi_egyseg']          = 'Pack unit:';
$_['entry_ar_latszik']                  = 'Display price:';
$_['entry_date_ervenyes_ig']            = 'Date valid:';
$_['entry_darabaru_meter']              = 'Product length (meter):';
$_['entry_admin_elhelyezkedes']         = 'Location:';
$_['entry_ellenorzokod']                = 'Verification Code:';



$_['entry_iskolai_vegzettseg']          = 'Educational attainment:';

$_['entry_register_cegnev']                             = 'Show company name:';
$_['entry_register_adoszam']                            = 'Show tax number:';
$_['entry_register_vallalkozasi_forma']                 = 'Show company form:';
$_['entry_register_szekhely']                           = 'Show head office:';
$_['entry_register_ugyvezeto_neve']                     = 'Show manager name:';
$_['entry_register_ugyvezeto_telefonszama']             = 'Shwo manager phone:';

$_['entry_register_address_street']                     = 'Show street:';
$_['entry_register_address_city']                       = 'Show city:';
$_['entry_register_address_postcode']                   = 'Show postcode:';
$_['entry_register_address_country']                    = 'Show country:';
$_['entry_register_address_region']                     = 'Show region:';
$_['entry_register_address_cegnev']                     = '(Address) Show Company name:';
$_['entry_register_address_adoszam']                    = '(Address) Show tax number:';
$_['entry_register_address_vallalkozasi_forma']         = '(Address) Show company form:';
$_['entry_register_address_szekhely']                   = '(Address) Show head office:';
$_['entry_register_address_ugyvezeto_neve']             = '(Address) Show manager name:';
$_['entry_register_address_ugyvezeto_telefonszama']     = '(Address) Show manager phone:';

$_['entry_register_email']                              = 'Email address:';
$_['entry_register_email_megerosites']                  = 'Email address confirm:';
$_['entry_register_telefon']                            = 'Phone:';
$_['entry_register_fax']                                = 'Fax:';
$_['entry_register_paypal']                             = 'PayPal email ID:';
$_['entry_nem_regisztracio']                            = 'Allow Sex Registration:';
$_['entry_eletkor_regisztracio']                        = 'Allow Age Registration:';
$_['entry_register_vezeteknev']                         = 'First name:';
$_['entry_register_keresztnev']                         = 'Last name:';

$_['entry_registerblock_szemelyes']                     = 'Personal informations block:';
$_['entry_registration_default']                        = 'Default registration:';
$_['entry_registration_company']                        = 'Default company registration:';
$_['entry_registerblock_cim']                           = 'Address informations block:';
$_['entry_registerblock_ceges_cim']                     = 'Company informations block:';
$_['entry_registerblock_paypal']                        = 'Paypal block:';
$_['entry_registerblock_hirlevel']                      = 'Newsletter block:';

$_['entry_register_hirlevel_kategoriak']                = 'Newsletter categories';

$_['text_success']   = 'Successfully changed the settings!';

$_['entry_kepek_helyszinenkent']                        = 'Pictures with places:';

$_['entry_admin_biztonsagi_kod']                              = 'Security code to product:';

$_['entry_cimmodositas_a_szerkeztesben']                = 'Address edit in Account edit page:';

$_['entry_emlekezz_ram']                                = 'Remember me function in login:';
$_['entry_admin_nincs_negativ_ar']                      = 'Disable null/negative price in sellable product:';
$_['entry_admin_seourl_generalas']                      = 'Generate 301 URL:';

$_['entry_lejarat_nelkuli']                             = 'No expiry switch::';
$_['entry_default']                                     = 'Default:';


$_['entry_tabs_settings']                     = 'Tabs settings:';
$_['entry_product_tab_description']           = 'Display description tab:';
$_['entry_product_tab_meret']                 = 'Display size tab:';
$_['entry_product_tab_attribute_groups']      = 'Display attribute tab:';
$_['entry_product_tab_review_status']         = 'Display review tab:';
$_['entry_product_tab_products']              = 'Display products tab:';
$_['entry_product_tab_test_description']      = 'Display test description tab:';
$_['entry_product_tab_image']                 = 'Display image tab:';

?>
