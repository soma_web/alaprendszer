<div id="upsell">
    <div>

        <?php $megjelenit_upsell_csoportok = $this->config->get('upsell_beallitas_upsell_csoportok'); ?>

        <?php
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_upsell.php')) {
            $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/stylesheet/stylesheet_upsell.php';
            include_once($arkiir);

        } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_upsell.php')) {
            $arkiir = DIR_TEMPLATE.'default/stylesheet/stylesheet_upsell.php';
            include_once($arkiir);

        } ?>

        <?php   $elso       = true;
        $elozo      = 0;
        $divszamol_upsell  = 1;
        $div_nyit_upsell   = 0;
        $div_zar_upsel    = 0;
        ?>

        <?php foreach ($megjelenit_upsell_csoportok as $key=>$value) { ?>
            <?php
            if ($elso) {$elso = false;}
            else {
                $elozo = $divszamol_upsell;
            }
            $divszamol_upsell = count(explode('_',$key));

            if($divszamol_upsell <= $elozo) {
                for($i=$divszamol_upsell; $elozo >=$i; $i++) {
                    echo "</div>";
                    $div_zar_upsel ++;

                }
            }
            $elotag = ($divszamol_upsell%2 == 0) ? "upsell_oszlop_" : "upsell_sor_";
            ?>
            <div class="<?php echo $elotag.$key. ' ' . $value['default']['class']?> ">
                <?php $div_nyit_upsell++?>
                <?php if ($value['default']['status'] == 1) { ?>
                    <?php if(isset($value['templates']) && !empty($value['templates'])) { ?>
                        <?php foreach ($value['templates'] as $keytemplate=>$beallitas_upsell) { ?>
                            <?php if (isset($beallitas_upsell['status']) && $beallitas_upsell['status'] == 1) {?>
                                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/upsell/'. $keytemplate .'.tpl')) {
                                    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/upsell/'.$keytemplate . '.tpl';
                                } else {
                                    $arkiir = DIR_TEMPLATE.'default/template/module/upsell/'.$keytemplate.'.tpl';
                                } ?>
                                <?php include($arkiir); ?>
                            <?php } ?>

                        <?php } ?>
                    <?php } ?>
                <?php } ?>
        <?php }


        if ($div_nyit_upsell - $div_zar_upsel > 0) {
            $div_nyitva_upsell = $div_nyit_upsell - $div_zar_upsel;
            for ($i=0; $div_nyitva_upsell > $i; $i++) {
                echo "</div>";
            }
        }
        ?>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#upsell").css("height",$(document).height());

    });
</script>