<?php
// Heading
$_['heading_title']       = 'Specials in header';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module specials!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_ures']          = 'There is no animation';
$_['text_mcs5']          = 'Scroll animation';
$_['text_korhinta']      = 'Carousel animation';
// Entry
$_['entry_limit']         = 'Limit:';
$_['entry_kep_szelesseg'] = 'Image width:';
$_['entry_kep_magassag']  = 'Image height:';
$_['entry_status']        = 'Status:';
$_['entry_kiemelt']       = 'Only special offers:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module specials!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>