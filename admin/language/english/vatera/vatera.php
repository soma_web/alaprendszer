<?php
$_['tab_vatera']              = 'Vatera';

$_['vat_vazonosito']             = 'Vatera identifier';
$_['vat_termeknev']              = 'Product name';
$_['vat_alcim']                  = 'Subtitle';
$_['vat_darabszam']              = 'Piece';
$_['vat_kikialtasiar']           = 'Upset price';
$_['vat_minimalar']              = 'Minimum price';
$_['vat_villamar']               = 'Speed price';
$_['vat_licitlepcso']            = 'Licit';
$_['vat_aukcio_inditasa']        = 'Auction start';
$_['vat_aukcio_idotartam']       = 'Auction duration';
$_['vat_vatera_kategoria_kod']   = 'Vatera category code';
$_['vat_megye']                  = 'Shire';
$_['vat_orszag']                 = 'Clime';
$_['vat_sablon_azonosito']       = 'Shipping gage identifier';
$_['vat_ujrainditasok_szama']    = 'Restart number';
$_['vat_kiemelesek']             = 'Exaltation (Premeium service) ';
$_['vat_termek_jellemzok']       = 'Product attributes';
$_['vat_garancia']               = 'Warranty';
$_['vat_elfogadas_szazalek']     = 'My tender function is accepted  by the assumption';
$_['vat_kepek1']                 = 'Picture I.';
$_['vat_kepek2']                 = 'Picture II.';
$_['vat_kepek3']                 = 'Picture III.';
$_['vat_kepek4']                 = 'Picture IV.';
$_['vat_kinezeti_sablon']        = 'Product look gage identifier';
$_['vat_product_id']             = 'Product identifier code';
$_['vat_kategoria_specifikus']   = 'Category specific attribute';
$_['vat_kepek5']                 = 'Picture V.';
$_['vat_kepek6']                 = 'Picture VI.';
$_['vat_kepek7']                 = 'Picture VII.';
$_['vat_kepek8']                 = 'Picture VIII.';
$_['vat_leiras']                 = 'Description';
$_['vat_automatikus_kuldes']     = 'Automatic forwarding';

?>
