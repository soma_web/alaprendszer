<?php
// Heading
$_['heading_title']    = 'Kundengruppen';

// Text
$_['text_success']     = 'Erfolgreich: Kundengruppen erfolgreich geändert!';

// Column
$_['column_name']      = 'Bezeichnung';
$_['column_action']    = 'Aktion';

// Entry
$_['entry_name']       = 'Bezeichnung:';

// Error
$_['error_permission'] = 'Warnung: Sie haben keine Berechtigung, um Kundengruppen zu ändern!';
$_['error_name']       = 'Die Bezeichnung muss zwischen 3 und 64 Zeichen lang sein!';
$_['error_default']    = 'Warnung: Diese Kundengruppe kann nicht gelöscht werden, da sie als Standard definiert wurde!';
$_['error_store']      = 'Warnung: Diese Kundengruppe kann nicht gelöscht werden da sie %s Shops zugeordnet ist!';
$_['error_customer']   = 'Warnung: Diese Kundengruppe kann nicht gelöscht werden da ihr noch %s Kunden zugeordnet sind!';
?>
