<?php
class ModelSettingBiztositas extends Model {
    public function getInstalled($type) {
        $extension_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($type) . "'");

        foreach ($query->rows as $result) {
            $extension_data[] = $result['code'];
        }

        return $extension_data;
    }

    public function install($type, $code) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "extension SET `type` = '" . $this->db->escape($type) . "', `code` = '" . $this->db->escape($code) . "'");
    }

    public function uninstall($type, $code) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($type) . "' AND `code` = '" . $this->db->escape($code) . "'");
    }

    public function addBiztositas($data){

        $this->db->query("INSERT INTO " . DB_PREFIX . "biztositas SET `biztositas_tol` = '" . $data["biztositas_tol"] . "', `biztositas_ig` = '" . $data["biztositas_ig"] . "', `biztositas_ertek` = '" . $data["biztositas_ertek"] . "'");
    }

    public function getBiztositasok(){

        $biztositas_data = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "biztositas ");

        foreach ($query->rows as $result) {
            $biztositas_data[$result['id']]['biztositas_id'] = $result['id'];
            $biztositas_data[$result['id']]['biztositas_tol'] = $result['biztositas_tol'];
            $biztositas_data[$result['id']]['biztositas_ig'] = $result['biztositas_ig'];
            $biztositas_data[$result['id']]['biztositas_ertek'] = $result['biztositas_ertek'];
        }

        return $biztositas_data;

    }

}
?>