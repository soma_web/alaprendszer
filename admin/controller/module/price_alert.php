<?php
class ControllerModulePriceAlert extends Controller {
	private $version = '2.0'; 
	private $error = array(); 
	
	public function install(){		
        $this->load->model('module/price_alert');
		
		$this->model_module_price_alert->createTables();
	}
	
	public function uninstall(){		
        $this->load->model('module/price_alert');
		
		$this->model_module_price_alert->removeTables();
	}
	
	public function index() {   
		$this->load->language('module/price_alert');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->document->addStyle('view/stylesheet/price_alert.css');
		
		$this->load->model('setting/setting');
		$this->load->model('tool/image');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('price_alert', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title') . ' ' . $this->version;
		
		$this->data['tab_setting'] = $this->language->get('tab_setting');
		$this->data['tab_popup'] = $this->language->get('tab_popup');
		$this->data['tab_customer_email'] = $this->language->get('tab_customer_email');
		$this->data['tab_admin_email'] = $this->language->get('tab_admin_email');
		$this->data['tab_alert'] = $this->language->get('tab_alert');
		$this->data['tab_history'] = $this->language->get('tab_history');
		$this->data['tab_help']    = $this->language->get('tab_help');
		
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_status_attention'] = $this->language->get('text_status_attention');
		$this->data['text_check_send'] = $this->language->get('text_check_send');
		
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_secret_code'] = $this->language->get('entry_secret_code');
		$this->data['entry_admin_notification'] = $this->language->get('entry_admin_notification');
		$this->data['entry_use_html_email'] = $this->language->get('entry_use_html_email');
		
		$this->data['entry_popup_set_alert'] = $this->language->get('entry_popup_set_alert');
		$this->data['entry_popup_short_explanation'] = $this->language->get('entry_popup_short_explanation');
		$this->data['entry_popup_name'] = $this->language->get('entry_popup_name');
		$this->data['entry_popup_email'] = $this->language->get('entry_popup_email');
		$this->data['entry_popup_desired_price'] = $this->language->get('entry_popup_desired_price');
		$this->data['entry_popup_button_set_alert'] = $this->language->get('entry_popup_button_set_alert');
		$this->data['entry_popup_error_name'] = $this->language->get('entry_popup_error_name');
		$this->data['entry_popup_error_email'] = $this->language->get('entry_popup_error_email');
		$this->data['entry_popup_error_desired_price'] = $this->language->get('entry_popup_error_desired_price');		
		$this->data['entry_popup_error_price_already'] = $this->language->get('entry_popup_error_price_already');		
		$this->data['entry_popup_success'] = $this->language->get('entry_popup_success');		
		
		$this->data['entry_subject'] = $this->language->get('entry_subject');
		$this->data['entry_message'] = $this->language->get('entry_message');
		$this->data['entry_admin_message'] = $this->language->get('entry_admin_message');
		
		// -- price alert list
		$this->data['entry_customer'] = $this->language->get('entry_customer');	
		$this->data['entry_email'] = $this->language->get('entry_email');	
		$this->data['entry_product'] = $this->language->get('entry_product');
		$this->data['entry_date_added'] = $this->language->get('entry_date_added');		
		// -- stop price alert list
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_check_send'] = $this->language->get('button_check_send');
		$this->data['button_filter'] = $this->language->get('button_filter');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = array();
		}
				
		if (isset($this->error['secret_code'])) {
			$this->data['error_secret_code'] = $this->error['secret_code'];
		} else {
			$this->data['error_secret_code'] = '';
		}
		
		if (isset($this->error['use_html_email'])) {
			$this->data['error_use_html_email'] = $this->error['use_html_email'];
		} else {
			$this->data['error_use_html_email'] = '';
		}

		if (isset($this->error['set_alert'])) {
			$this->data['error_set_alert'] = $this->error['set_alert'];
		} else {
			$this->data['error_set_alert'] = array();
		}
		
		if (isset($this->error['short_explanation'])) {
			$this->data['error_short_explanation'] = $this->error['short_explanation'];
		} else {
			$this->data['error_short_explanation'] = array();
		}
		
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}

		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = array();
		}

		if (isset($this->error['button_set_alert'])) {
			$this->data['error_button_set_alert'] = $this->error['button_set_alert'];
		} else {
			$this->data['error_button_set_alert'] = array();
		}

		if (isset($this->error['desired_price'])) {
			$this->data['error_desired_price'] = $this->error['desired_price'];
		} else {
			$this->data['error_desired_price'] = array();
		}		
		
		if (isset($this->error['error_name'])) {
			$this->data['error_error_name'] = $this->error['error_name'];
		} else {
			$this->data['error_error_name'] = array();
		}

		if (isset($this->error['error_email'])) {
			$this->data['error_error_email'] = $this->error['error_email'];
		} else {
			$this->data['error_error_email'] = array();
		}

		if (isset($this->error['error_desired_price'])) {
			$this->data['error_error_desired_price'] = $this->error['error_desired_price'];
		} else {
			$this->data['error_error_desired_price'] = array();
		}	

		if (isset($this->error['error_price_already'])) {
			$this->data['error_error_price_already'] = $this->error['error_price_already'];
		} else {
			$this->data['error_error_price_already'] = array();
		}			

		if (isset($this->error['success'])) {
			$this->data['error_success'] = $this->error['success'];
		} else {
			$this->data['error_success'] = array();
		}		
		
		if (isset($this->error['customer_subject'])) {
			$this->data['error_customer_subject'] = $this->error['customer_subject'];
		} else {
			$this->data['error_customer_subject'] = array();
		}	

		if (isset($this->error['customer_message'])) {
			$this->data['error_customer_message'] = $this->error['customer_message'];
		} else {
			$this->data['error_customer_message'] = array();
		}	

		if (isset($this->error['admin_subject'])) {
			$this->data['error_admin_subject'] = $this->error['admin_subject'];
		} else {
			$this->data['error_admin_subject'] = array();
		}	

		if (isset($this->error['admin_message'])) {
			$this->data['error_admin_message'] = $this->error['admin_message'];
		} else {
			$this->data['error_admin_message'] = array();
		}		
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/price_alert', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/price_alert', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['price_alert_status'])) {
			$this->data['price_alert_status'] = $this->request->post['price_alert_status'];
		} elseif ($this->config->get('price_alert_status')) { 
			$this->data['price_alert_status'] = $this->config->get('price_alert_status');
		} else {
			$this->data['price_alert_status'] = '';
		}

		if (isset($this->request->post['price_alert_secret_code'])) {
			$this->data['price_alert_secret_code'] = $this->request->post['price_alert_secret_code'];
		} elseif ($this->config->get('price_alert_secret_code')) { 
			$this->data['price_alert_secret_code'] = $this->config->get('price_alert_secret_code');
		} else {
			$this->data['price_alert_secret_code'] = '';
		}
		
		if (isset($this->request->post['price_alert_admin_notification'])) {
			$this->data['price_alert_admin_notification'] = $this->request->post['price_alert_admin_notification'];
		} elseif ($this->config->get('price_alert_admin_notification')) { 
			$this->data['price_alert_admin_notification'] = $this->config->get('price_alert_admin_notification');
		} else {
			$this->data['price_alert_admin_notification'] = '';
		}		
		
		if (isset($this->request->post['price_alert_use_html_email'])) {
			$this->data['price_alert_use_html_email'] = $this->request->post['price_alert_use_html_email'];
		} elseif ($this->config->get('price_alert_use_html_email')) { 
			$this->data['price_alert_use_html_email'] = $this->config->get('price_alert_use_html_email');
		} else {
			$this->data['price_alert_use_html_email'] = '';
		}
		
		if (isset($this->request->post['price_alert_popup'])) {
			$this->data['price_alert_popup'] = $this->request->post['price_alert_popup'];
		} elseif ($this->config->get('price_alert_popup')) { 
			$this->data['price_alert_popup'] = $this->config->get('price_alert_popup');
		} else {
			$this->data['price_alert_popup'] = array();
		}

		if (isset($this->request->post['price_alert_customer_mail'])) {
			$this->data['price_alert_customer_mail'] = $this->request->post['price_alert_customer_mail'];
		} elseif ($this->config->get('price_alert_customer_mail')) { 
			$this->data['price_alert_customer_mail'] = $this->config->get('price_alert_customer_mail');
		} else {
			$this->data['price_alert_customer_mail'] = array();
		}
		
		if (isset($this->request->post['price_alert_admin_mail'])) {
			$this->data['price_alert_admin_mail'] = $this->request->post['price_alert_admin_mail'];
		} elseif ($this->config->get('price_alert_admin_mail')) { 
			$this->data['price_alert_admin_mail'] = $this->config->get('price_alert_admin_mail');
		} else {
			$this->data['price_alert_admin_mail'] = array();
		}		
		
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['front_base_url'] = defined('HTTPS_CATALOG')? HTTPS_CATALOG : HTTP_CATALOG;
		} else {
			$this->data['front_base_url'] = HTTP_CATALOG;
		}
		
		$this->data['token'] = $this->session->data['token'];
				
		$this->template = 'module/price_alert.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/price_alert')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$dinamic_strlen = 'utf8_strlen';
		 
		if ( !function_exists('utf8_strlen') ) {
			$dinamic_strlen = 'strlen';
		}

		if ($this->request->post['price_alert_admin_notification'] == 1) {
			foreach ($this->request->post['price_alert_admin_mail'] as $language_id => $value) {
				if ($dinamic_strlen($value['subject']) < 1) {
					$this->error['admin_subject'][$language_id] = $this->language->get('error_subject');
					$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_customer_email')); 
				}
				
				if ($dinamic_strlen($value['message']) < 1) {
					$this->error['admin_message'][$language_id] = $this->language->get('error_message');
					$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_customer_email')); 
				}
			}
		}
		
		foreach ($this->request->post['price_alert_customer_mail'] as $language_id => $value) {
			if ($dinamic_strlen($value['subject']) < 1) {
        		$this->error['customer_subject'][$language_id] = $this->language->get('error_subject');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_customer_email')); 
      		}
			
			if ($dinamic_strlen($value['message']) < 1) {
        		$this->error['customer_message'][$language_id] = $this->language->get('error_message');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_customer_email')); 
      		}
		}
		
		foreach ($this->request->post['price_alert_popup'] as $language_id => $value) {
			if ($dinamic_strlen($value['set_alert']) < 1) {
        		$this->error['set_alert'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}
			
			if ($dinamic_strlen($value['short_explanation']) < 1) {
        		$this->error['short_explanation'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}

			if ($dinamic_strlen($value['name']) < 1) {
        		$this->error['name'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}			
			
			if ($dinamic_strlen($value['email']) < 1) {
        		$this->error['email'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}	

			if ($dinamic_strlen($value['desired_price']) < 1) {
        		$this->error['desired_price'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}
			
			if ($dinamic_strlen($value['button_set_alert']) < 1) {
        		$this->error['button_set_alert'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}			

			if ($dinamic_strlen($value['error_name']) < 1) {
        		$this->error['error_name'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}

			if ($dinamic_strlen($value['error_email']) < 1) {
        		$this->error['error_email'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}	

			if ($dinamic_strlen($value['error_desired_price']) < 1) {
        		$this->error['error_desired_price'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}

			if ($dinamic_strlen($value['success']) < 1) {
        		$this->error['success'][$language_id] = $this->language->get('error_required');
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_popup')); 
      		}				
		}	
		
		if ($this->request->post['price_alert_use_html_email'] == 1 && !$this->isHTMLEmailExtensionInstalled() ) {
			$this->error['use_html_email'] = $this->language->get('error_html_email_not_installed');
			$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_setting')); 
		}	

		if ($dinamic_strlen($this->request->post['price_alert_secret_code']) < 5) {
			$this->error['secret_code'] = $this->language->get('error_secret_code');
			$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_setting')); 
		}		
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	private function isHTMLEmailExtensionInstalled() {
		$installed = false;
		
		if ($this->config->get('html_email_default_word') && file_exists(DIR_APPLICATION . 'model/tool/html_email.php') && file_exists(DIR_CATALOG . 'model/tool/html_email.php')) {
			$installed = true;	
		}
		
		return $installed;
	}
	
	public function getPriceAlerts() {
	    $this->load->language('module/price_alert');
		
		$this->load->model('module/price_alert');
		
		if (isset($this->request->get['pa_list_filter_customer'])) {
			$filter_customer = $this->request->get['pa_list_filter_customer'];
		} else {
			$filter_customer = null;
		}
		
		if (isset($this->request->get['pa_list_filter_email'])) {
			$filter_email = $this->request->get['pa_list_filter_email'];
		} else {
			$filter_email = null;
		}		
		
		if (isset($this->request->get['pa_list_filter_product'])) {
			$filter_product = $this->request->get['pa_list_filter_product'];
		} else {
			$filter_product = null;
		}

		if (isset($this->request->get['pa_list_filter_date_added'])) {
			$filter_date_added = $this->request->get['pa_list_filter_date_added'];
		} else {
			$filter_date_added = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'ppa.alert_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['pa_list_filter_customer'])) {
			$url .= '&pa_list_filter_customer=' . urlencode(html_entity_decode($this->request->get['pa_list_filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['pa_list_filter_email'])) {
			$url .= '&pa_list_filter_email=' . urlencode(html_entity_decode($this->request->get['pa_list_filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['pa_list_filter_product'])) {
			$url .= '&pa_list_filter_product=' . urlencode(html_entity_decode($this->request->get['pa_list_filter_product'], ENT_QUOTES, 'UTF-8'));
		}		
		
		if (isset($this->request->get['pa_list_filter_date_added'])) {
			$url .= '&pa_list_filter_date_added=' . $this->request->get['pa_list_filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['alerts'] = array();
		
		$data = array(
			'filter_customer'	     => $filter_customer,
			'filter_email'	         => $filter_email,
			'filter_product'	     => $filter_product,
			'filter_date_added'      => $filter_date_added,
			'sort'                   => $sort,
			'order'                  => $order,
			'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                  => $this->config->get('config_admin_limit')
		);
		
		$alert_total = $this->model_module_price_alert->getTotalPriceAlerts($data);
		
		$alerts = $this->model_module_price_alert->getPriceAlerts($data);
		
		if ($alerts) {
			foreach ($alerts as $alert) {
				$this->data['alerts'][] = array(
					'alert_id'      => $alert['alert_id'],
					'name' 			=> $alert['name'],
					'email' 		=> $alert['email'],
					'product_id'    => $alert['product_id'],
					'product_name'  => $alert['product_name'],
					'product_href'  => $this->url->link('catalog/product/update', 'product_id=' . $alert['product_id'].'&token='. $this->session->data['token'], 'SSL'),
					'product_price' => $this->currency->format($this->currency->convert($alert['product_price'], $alert['currency_code'], $this->config->get('config_currency'))),
					'desired_price' => $this->currency->format($this->currency->convert($alert['desired_price'], $alert['currency_code'], $this->config->get('config_currency'))),					
					'date_added'    => date($this->language->get('date_format_short') . ' ' . $this->language->get('time_format'), strtotime($alert['date_added'])),
				);
			}
		}
		
		$this->data['column_customer'] = $this->language->get('column_customer');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_product'] = $this->language->get('column_product');
		$this->data['column_price_seen'] = $this->language->get('column_price_seen');
		$this->data['column_price_desired'] = $this->language->get('column_price_desired');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_action']  = $this->language->get('column_action');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['pa_list_filter_customer'])) {
			$url .= '&pa_list_filter_customer=' . urlencode(html_entity_decode($this->request->get['pa_list_filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['pa_list_filter_email'])) {
			$url .= '&pa_list_filter_email=' . urlencode(html_entity_decode($this->request->get['pa_list_filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['pa_list_filter_product'])) {
			$url .= '&pa_list_filter_product=' . urlencode(html_entity_decode($this->request->get['pa_list_filter_product'], ENT_QUOTES, 'UTF-8'));
		}		
		
		if (isset($this->request->get['pa_list_filter_date_added'])) {
			$url .= '&pa_list_filter_date_added=' . $this->request->get['pa_list_filter_date_added'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$pagination = new Pagination();
		$pagination->total = $alert_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('module/price_alert/getPriceAlerts', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_customer'] = $filter_customer;
		$this->data['filter_email'] = $filter_email;
		$this->data['filter_product'] = $filter_product;
		$this->data['filter_date_added'] = $filter_date_added;
		
		$this->template = 'module/price_alert_list.tpl';		

		$this->response->setOutput($this->render());
	}
	
	public function getPriceAlertHistory() {
	    $this->load->language('module/price_alert');
		
		$this->load->model('module/price_alert');
		
		if (isset($this->request->get['pa_history_filter_customer'])) {
			$filter_customer = $this->request->get['pa_history_filter_customer'];
		} else {
			$filter_customer = null;
		}
		
		if (isset($this->request->get['pa_history_filter_email'])) {
			$filter_email = $this->request->get['pa_history_filter_email'];
		} else {
			$filter_email = null;
		}		

		if (isset($this->request->get['pa_history_filter_date_added'])) {
			$filter_date_added = $this->request->get['pa_history_filter_date_added'];
		} else {
			$filter_date_added = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'history_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['pa_history_filter_customer'])) {
			$url .= '&pa_history_filter_customer=' . urlencode(html_entity_decode($this->request->get['pa_history_filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['pa_history_filter_email'])) {
			$url .= '&pa_history_filter_email=' . urlencode(html_entity_decode($this->request->get['pa_history_filter_email'], ENT_QUOTES, 'UTF-8'));
		}		
		
		if (isset($this->request->get['pa_history_filter_date_added'])) {
			$url .= '&pa_history_filter_date_added=' . $this->request->get['pa_history_filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['alerts'] = array();
		
		$data = array(
			'filter_customer'	     => $filter_customer,
			'filter_email'	         => $filter_email,
			'filter_date_added'      => $filter_date_added,
			'sort'                   => $sort,
			'order'                  => $order,
			'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                  => $this->config->get('config_admin_limit')
		);
		
		$alert_total = $this->model_module_price_alert->getTotalPriceAlertHistory($data);
		
		$alerts = $this->model_module_price_alert->getPriceAlertHistory($data);
		
		if ($alerts) {
			foreach ($alerts as $alert) {
				$this->data['alerts'][] = array(
					'history_id'    => $alert['history_id'],
					'name' 			=> $alert['name'],
					'email' 		=> $alert['email'],
					'date_added'    => date($this->language->get('date_format_short') . ' ' . $this->language->get('time_format'), strtotime($alert['date_added'])),
				);
			}
		}
		
		$this->data['column_customer'] = $this->language->get('column_customer');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_sent'] = $this->language->get('column_sent');
		$this->data['column_action']  = $this->language->get('column_action');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['pa_history_filter_customer'])) {
			$url .= '&pa_history_filter_customer=' . urlencode(html_entity_decode($this->request->get['pa_history_filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['pa_history_filter_email'])) {
			$url .= '&pa_history_filter_email=' . urlencode(html_entity_decode($this->request->get['pa_history_filter_email'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['pa_history_filter_date_added'])) {
			$url .= '&pa_history_filter_date_added=' . $this->request->get['pa_history_filter_date_added'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$pagination = new Pagination();
		$pagination->total = $alert_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('module/price_alert/getPriceAlertHistory', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_customer'] = $filter_customer;
		$this->data['filter_email'] = $filter_email;
		$this->data['filter_date_added'] = $filter_date_added;
		
		$this->template = 'module/price_alert_history.tpl';		

		$this->response->setOutput($this->render());
	}
	
	public function getReminderEmail() {	
		$this->load->model('module/price_alert');
	
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
		
			$reminder_info = $this->model_module_price_alert->getHistory($this->request->post['acr_history_id']);
			
			if ($reminder_info) {
				$json['customer_name']     = $reminder_info['firstname'] . ' ' . $reminder_info['lastname'];
				$json['date_added']        = $reminder_info['date_added'];
				$json['email_description'] = html_entity_decode($reminder_info['email_description'], ENT_QUOTES, 'UTF-8'); 
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function getTotalRecovered() {	
		$this->load->language('module/price_alert');
		$this->load->model('module/price_alert');
	
		$json = array();
		
		$total_recovered = $this->model_module_price_alert->getTotalRecovered();
		
		if ((int)$total_recovered > 0) {
			$json['total_recovered'] = sprintf($this->language->get('text_total_recovered'), $this->currency->format($total_recovered));
		}	
		
		$this->response->setOutput(json_encode($json));
	}
	
	private function getCartProducts($customer_cart){
		$this->load->model('tool/image');
		
		$cart_products = array();
		
		if ($customer_cart && is_string($customer_cart)) {
			$cart = unserialize($customer_cart);
			
			foreach ($cart as $key => $value) {
				$product = explode(':', $key);
				$product_id = $product[0];
	
				// Options
				if (isset($product[1])) {
					$options = unserialize(base64_decode($product[1]));
				} else {
					$options = array();
				} 
				
				$product_info = $this->model_catalog_product->getProduct($product_id);
				
				if ($product_info){ 
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], 30,30);
					} else {
						$image = $this->model_tool_image->resize('no_image.jpg', 30,30);
					}
				
					$option_data = array();

					if ($options) {
						foreach ($options as $product_option_id => $option_value) {
							$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

							if ($option_query->num_rows) {
								if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
									$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$option_value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

									if ($option_value_query->num_rows) {

										$option_data[] = array(
											'name'                    => $option_query->row['name'],
											'option_value'            => $option_value_query->row['name']
										);								
									}
								} elseif ($option_query->row['type'] == 'checkbox' && is_array($option_value)) {
									foreach ($option_value as $product_option_value_id) {
										$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

										if ($option_value_query->num_rows) {

											$option_data[] = array(
												'name'                    => $option_query->row['name'],
												'option_value'            => $option_value_query->row['name']
											);								
										}
									}						
								} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
									$option_data[] = array(
										'name'                    => $option_query->row['name'],
										'option_value'            => $option_value
									);						
								}
							}
						}
					}	
				
					$cart_products[] = array( 
						'name'     => $product_info['name'],
						'image'    => $image,
						'quantity' => $value,
						'options'  => $option_data,
						'href'  => $this->url->link('product/product', 'product_id=' . $product_info['product_id'], 'SSL')
					);
				}
			}			
		}
		
		return $cart_products;
	}	
	
	private function hasActiveProducts($customer_info) {
		$active_products = 0;
		
		if ($customer_info['cart'] && is_string($customer_info['cart'])) {
			$cart = unserialize($customer_info['cart']);
			
			foreach ($cart as $key => $value) {
				$product = explode(':', $key);
				$product_id = $product[0];
				
				$product_info = $this->model_catalog_product->getProduct($product_id);
				
				if ($product_info){ 
					if ($this->config->get('price_alert_hide_out_stock')) {
						if ($product_info['quantity'] > 0) {
							$active_products++;
						}
					} else {
						$active_products++;
					}	
				}
			}			
		}
		
		return $active_products;
	}
	
	private function isPreviousOrder($customer_info) {
		$is_previous_order = false;
		
		$last_order_id = $this->model_module_price_alert->getLastOrderId($customer_info['customer_id']);
		
		if ($last_order_id) {
			$last_order_products = $this->model_module_price_alert->getLastOrderProducts($last_order_id);
		
			if ($customer_info['cart'] && is_string($customer_info['cart'])) {
				$cart = unserialize($customer_info['cart']);
				
				if ($this->hasSameProducts($cart, $last_order_products)) {
					$is_previous_order = true;
				}				
			}
		}
	
		return $is_previous_order;
	}
	
	private function hasSameProducts($cart, $last_order_products) {
		$same_products = true;  
		
		if (count($cart) != count($last_order_products)) {
			$same_products = false;
		} else {
		
			foreach ($cart as $key => $value) {
				$key_split = explode(":", $key);
				$product_id = $key_split[0];
				
				if (!in_array($product_id, $last_order_products)) {
					$same_products = false;
				}
			}	
		}
		
		return $same_products;
	}
	
	private function filterCustomers($customers) {
		$filtred_customers = array();
		
		if ($customers) {
			foreach($customers as $customer) {
				if ($this->hasActiveProducts($customer) && !$this->isPreviousOrder($customer)) {
					$filtred_customers[] = $customer; 
				}
			}			
		}
		
		return $filtred_customers;
	}
	
}
?>