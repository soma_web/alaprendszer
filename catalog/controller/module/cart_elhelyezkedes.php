<?php 
class ControllerModuleCartElhelyezkedes extends Controller {
	public function index() {
		$this->language->load('module/cart');

      	if (isset($this->request->get['remove'])) {
          	$this->cart_elhelyezkedes->remove($this->request->get['remove']);
      	}


        $this->load->model('checkout/order');

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['total_title'] = $this->language->get('total_title');
        $this->data['text_items'] = "(".$this->cart_elhelyezkedes->countProducts().")";
        $this->data['text_empty'] = $this->language->get('text_empty');
        $this->data['text_checkout'] = $this->language->get('text_checkout');
        $this->data['button_remove'] = $this->language->get('button_remove');
        $this->data['cart_elhelyezkedes'] = $this->url->link('checkout/cart_elhelyezkedes');
        $this->data['checkout'] = $this->url->link('checkout/checkout_elhelyezkedes', '', 'SSL');

        $this->load->model('tool/image');

        $this->data['penznem'] =  " ". $this->currency->getFizetoeszkoz();
        $this->data['products'] = array();
        $this->data['total'] = 0;

        $this->data['totals'] = $this->model_checkout_order->getTotalSElhelyezkedes();

        foreach ($this->cart_elhelyezkedes->getProducts() as $product) {
            //$this->data['total'] += $product['total'];
            if ($product['image']) {
                $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
            } else {
                $image = '';
            }

            $this->data['products'][] = array(
                'key'           => $product['key'],
                'thumb'         => $image,
                'name'          => $product['name'],
                'quantity'      => $product['quantity'],
                'total'         => $this->model_checkout_order->getProductTotalElhelyezkedes($product['key']),
                'kiemelesek'    => $product['kiemelesek'],
                'href'          => $this->url->link('account/product/kiemeles', 'product_id=' . $product['product_id'])
            );
        }
		



        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/cart_elhelyezkedes.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/cart_elhelyezkedes.tpl';
        } else {
            $this->template = 'default/template/module/cart_elhelyezkedes.tpl';
        }

        $this->response->setOutput($this->render());

    }
}
?>