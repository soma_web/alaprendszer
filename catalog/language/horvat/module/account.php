<?php

// Heading
$_['heading_title']    = 'Fiók';

// Text
$_['text_register']    = 'Regisztráció';
$_['text_login']       = 'Belépés';
$_['text_logout']      = 'Kilépés';
$_['text_forgotten']   = 'Elfelejtett jelszó';
$_['text_account']     = 'Fiókom';
$_['text_edit']        = 'Fiók módosítása';
$_['text_password']    = 'Jelszó';
$_['text_wishlist']    = 'Kívánság lista';
$_['text_order']       = 'Rendeléstörténet';
$_['text_download']    = 'Letöltések';
$_['text_return']      = 'Garancia';
$_['text_transaction'] = 'Tranzakciók';
$_['text_newsletter']  = 'Hírlevél';

$_['text_myproduct']          = 'Saját termékeim';
$_['text_newproduct']          = 'Úr termék felvitele';
$_['text_salesreport']          = 'Ertékesítési jelentés';
?>
