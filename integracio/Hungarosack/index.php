<?php
header('Content-type: text/html; charset=utf-8');
//header('Content-Type: text/html; charset=iso-8859-1');


global $query;
global $sorok;


require_once('config.php');
require_once('lekerdezes.php');
require_once('feldolgozas/termek_import.php');
require_once('feldolgozas/menny_kedv_import.php');
require_once('feldolgozas/vevok_import.php');
require_once('feldolgozas/akcios_arak_import.php');
require_once('feldolgozas/vasarlas.php');
require_once('feldolgozas/ujvevo.php');

require_once('csv_import/csv_termek.php');
require_once('csv_import/csv_menny_kedv.php');
require_once('csv_import/csv_vevok.php');
require_once('csv_import/csv_akcios_arak.php');


$query = new Mysql_Lekeres(DB_HOSTNAME, DB_USERNAME,DB_PASSWORD, DB_DATABASE);



$csv_files=scandir(UTVONAL_NEW);

$vasarlas_to_xml = new Vasarlas($query);
$vevo_to_xml = new Ujvevo($query);

$vevo_to_xml->Vevo_Kuldes();

$vasarlas_to_xml->Termek_Vasarlas_Vevo();
$vasarlas_to_xml->Termek_Vasarlas();


foreach($csv_files as $file_name){
    if (substr($file_name,0,9)=="termekek_" && substr($file_name,10,3) != "old"){
        TermekOlvas(UTVONAL_NEW."/".$file_name);
        $uj_file_name="termekek_old_".ubstr($file_name,9,4)."_".substr($file_name,13,2)."_".substr($file_name,15,2)."_".substr($file_name,17);
        unlink (UTVONAL_NEW."/".$file_name);
        //$result=rename(UTVONAL_NEW."/".$file_name, UTVONAL_OLD."/".$uj_file_name);

    } elseif (substr($file_name,0,9)=="kedvarak_" && substr($file_name,10,3) != "old"){
        AkciosArak(UTVONAL_NEW."/".$file_name);
        $uj_file_name="kedvarak_old_".substr($file_name,9,4)."_".substr($file_name,13,2)."_".substr($file_name,15,2)."_".substr($file_name,17);
        unlink (UTVONAL_NEW."/".$file_name);
//        $result=rename(UTVONAL_NEW."/".$file_name, UTVONAL_OLD."/".$uj_file_name);

    } elseif (substr($file_name,0,10)=="mkedvarak_" && substr($file_name,11,3) != "old"){
        MennyisegiKedvezmenyesArak(UTVONAL_NEW."/".$file_name);
        $uj_file_name="mkedvarak_old_".substr($file_name,10,4)."_".substr($file_name,14,2)."_".substr($file_name,16,2)."_".substr($file_name,18);
        unlink (UTVONAL_NEW."/".$file_name);
//        $result=rename(UTVONAL_NEW."/".$file_name, UTVONAL_OLD."/".$uj_file_name);

    }  elseif (substr($file_name,0,10)=="partnerek_" && substr($file_name,11,3) != "old"){
        Partnerek(UTVONAL_NEW."/".$file_name);
        $uj_file_name="partnerek_old_".substr($file_name,10,4)."_".substr($file_name,14,2)."_".substr($file_name,16,2)."_".substr($file_name,18);
        unlink (UTVONAL_NEW."/".$file_name);
//        $result=rename(UTVONAL_NEW."/".$file_name, UTVONAL_OLD."/".$uj_file_name);
    }
}

$lekerdezes = new Termek_Import($query);
if (isset($lekerdezes->csvadatok->num_rows) && $lekerdezes->csvadatok->num_rows > 0){
    foreach($lekerdezes->csvadatok->rows as $sorok){
        Hibaszures();
        $lekerdezes->product($sorok);
    }
    $letrehoz="drop table if exists sap_termekek";
    mysql_query($letrehoz)  or Hibalista("Termék import", "sap_termekek", "törlés", "Nem sikerült a tábla törlése");
    echo "A termékek integrálása befejeződött ...<br>";
}

$lekerdezes = new Menny_Kedv_Import($query);
if (isset($lekerdezes->csvadatok->num_rows) &&  $lekerdezes->csvadatok->num_rows > 0){
    foreach($lekerdezes->csvadatok->rows as $sorok ){
        Hibaszures();
        $lekerdezes->Mennyisegi_Kedvezmeny($sorok);
    }
    $letrehoz="drop table if exists sap_mennyisegi_kedvezmenyek";
    mysql_query($letrehoz) or Hibalista("Mennyiségi kedvezmény import", "sap_mennyisegi_kedvezmenyek", "törlés", "Nem sikerült a tábla törlése");
    echo "A mennyiségi ár kedvezmények bedolgozása befejeződött ...<br>";
}

$lekerdezes = new Vevok_Import($query);
if (isset($lekerdezes->csvadatok->num_rows) &&  $lekerdezes->csvadatok->num_rows > 0){
    foreach($lekerdezes->csvadatok->rows as $sorok ){
        Hibaszures();
        $lekerdezes->Vevok($sorok);
    }
    $letrehoz="drop table if exists sap_vevok";
    mysql_query($letrehoz) or Hibalista("Vevők import", "sap_vevok", "törlés", "Nem sikerült a tábla törlése");
    echo "A vevő tábla bedolgozása befejeződött ...<br>";
}

$lekerdezes = new Akcios_Arak_Import($query);
if (isset($lekerdezes->csvadatok->num_rows) &&  $lekerdezes->csvadatok->num_rows > 0){
    foreach($lekerdezes->csvadatok->rows as $sorok ){
        Hibaszures();
        $lekerdezes->Akcios_Arak($sorok);
    }
    $letrehoz="drop table if exists sap_akcios_arak";
    mysql_query($letrehoz) or  Hibalista("Akciós árak import", "sap_akcios_arak", "törlés", "Nem sikerült a tábla törlése");
    echo "Az akciós árak tábla bedolgozása befejeződött ...<br>";
}

echo "<br><br>Az integráció befejeződött ...";


function Hibaszures() {
    global $sorok;
    foreach($sorok as $key => $oszlop){
        if (strpos($oszlop,"'")){
           $oszlop=str_replace("'",'"',$oszlop);
           // echo "Idézöjel javítva";
        }
        if ( substr($oszlop,0,1) == '"' &&  substr($oszlop,strlen($oszlop)-1,1) == '"'){
            $sorok[$key] = substr($oszlop,1,strlen($oszlop)-2);
        }
        if (substr($oszlop,strlen($oszlop)-1,2) == "\r"){
            $sorok[$key] = substr($oszlop,0,strlen($oszlop)-1);
        }
    }
}

function Hibalista($adatTipus, $tablaNeve, $muvelet, $hibasRecord, $hibasRecord2=""){
    $letrehoz="CREATE TABLE if not exists sap_hibalista(
        `azonosito` INT(9) AUTO_INCREMENT DEFAULT NULL,
        `adatTipus`  text default null,
        `tablaNeve`  text default null,
        `muvelet`  text default null,
        `hibasRecord`  text default null,
        `hibasRecord2`  text default null,
        `datum`  datetime,
        PRIMARY KEY (`azonosito`))   engine=MyISAM default charset=UTF8";
    mysql_query($letrehoz) or die("Nem sikerült a hibalista tábla létrehozása");

    $beszur="INSERT INTO sap_hibalista SET
            `azonosito`     = '',
            `adatTipus`     = '".$adatTipus."',
            `tablaNeve`     = '".$tablaNeve."',
            `muvelet`       = '".$muvelet."',
            `hibasRecord`   = '".$hibasRecord."',
            `hibasRecord2`  = '".$hibasRecord2."',
            `datum`         = NOW() ";
    mysql_query($beszur) or die("Nem sikerült a hibalista elkészítése");
}

function datumVissza($para){
    if ($para=="") return "";
    else return substr($para,0,4)."-".substr($para,4,2)."-".substr($para,6,2);
}
?>