<?php
// Text
$_['text_login']       = 'Bejelentkezés';
$_['text_logout']      = 'Kijelentkezés';
$_['text_account']     = 'Fiók';
$_['text_categories']  = 'Kategóriák';


$_['text_all_products'] = 'Összes termék';




$_['text_home']     = 'Kezdőlap';
$_['text_wishlist'] = 'Kívánságlista (%s)';
$_['text_cart']     = 'Kosár';
$_['text_items']    = '%s tétel - %s';
$_['text_search']   = 'Keresés:';

$_['text_welcome']  = '<a style="color: #fff" href="%s">Bejelentkezés</a>';
$_['text_kijelentkezes']  = '<a style="color: #fff" href="%s">Kijelentkezés</a>';
$_['text_regisztral']  = '<a style="color: #999999" href="%s">Regisztráció</a>';
$_['text_logged']   = '<span style="font-size: 13px; position: relative; top: 4px">Bejelentkezve:</span> <b><a style="font-size: 17px" href="%s">%s</a></b>';

$_['text_account']  = 'Fiók';
$_['text_checkout'] = 'Pénztár';
$_['text_language'] = 'Nyelv';
$_['text_currency'] = 'Pénznem';
$_['text_pricelist']   = 'Árlista';

//$_['text_motto']    = 'Minőségbe csomagolva';
$_['text_motto']    = '&bdquo;Minőségbe csomagolva&rdquo;';
$_['text_reg']    = 'Regisztrációk';
$_['text_belep']    = 'Bejelentkezés';
$_['text_arlista']    = 'Árlista';
$_['text_kapcsolat']    = 'Kapcsolat';
$_['text_termekek']    = 'Termékek';
?> 