<?php

// Heading
$_['heading_title']          = 'Kategória';

// Text
$_['text_success']           = 'Sikeresen módosította a kategóriákat!';
$_['text_default']           = 'Alapértelmezett';
$_['text_image_manager']     = 'Képszerkesztő';
$_['text_browse']            = 'Fájlok tallózása';
$_['text_clear']             = 'Kép törlése';
$_['text_piacter']           = 'Piactér';
$_['text_piacter_select']    = 'Válasszon';
$_['piacter_rogzit']         = 'Rögzítés';
$_['piacter_torol']          = 'Törlés';
$_['entry_piacter']          = 'CSV file feltöltése:';
$_['entry_csvk_piacternek']  = 'Új CSV file piactér részére:';


// Column
$_['column_name']            = 'Kategória';
$_['column_sort_order']      = 'Sorrend';
$_['column_action']          = 'Művelet';

// Entry
$_['entry_name']             = 'Kategória neve:';
$_['entry_meta_keyword'] 	 = 'Meta Tag Kulcsszó:';
$_['entry_meta_description'] = 'Meta leírás:';
$_['entry_description']      = 'Leírás:';
$_['entry_parent']           = 'Szülőkategória:';
$_['entry_store']            = 'Áruházak:';
$_['entry_keyword']          = 'SEO Kulcsszó:<br/><span class="help">Globálisan egyedinek kell lennie!</span>';
$_['entry_image']            = 'Kép:';
$_['entry_image_icon']       = 'Kép ikon:';
$_['entry_top']              = 'Fent:<br/><span class="help">Kijelzi a felső menü sávban. Csak a legfelső szülőkatekoria esetén működik!</span>';
$_['entry_column']           = 'Oszlopok:<br/><span class="help">Az alsó 3 kategóriában használni kívánt oszlopok száma. Csak a legfelső szülőkatekoria esetén működik!</span>';
$_['entry_sort_order']       = 'Sorrend:';
$_['entry_status']           = 'Állapot:';
$_['entry_layout']           = 'Kiosztás Felülbírálása:';
$_['entry_filter']           = 'Szűrők:<br /><span class="help">(Autocomplete)</span>';


// Error
$_['error_warning']          = 'Figyelmesen ellenőrizze át az űrlapot!';
$_['error_permission']       = 'Figyelmeztetés: A kategóriák módosítása az Ön számára nem engedélyezett';
$_['error_name']             = 'Figyelmeztetés: A kategórianév legalább 2, és legfeljebb 32 karakterből álljon!';
?>
