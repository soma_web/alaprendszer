<?php
// Heading 
$_['heading_title']  = 'Change Password';

// Text
$_['text_account']   = 'Account';
$_['text_password']  = 'Your Password';
$_['text_success']   = 'Success: Your password has been successfully updated.';
$_['text_password_changed_subject'] = 'Password changed';
$_['text_password_changed_html'] = 'Password changed in %s site.';
$_['text_password_changed_text'] = 'Password changed in %s site.';

// Entry
$_['entry_password'] = 'Password:';
$_['entry_confirm']  = 'Password Confirm:';

// Error
$_['error_password'] = 'Password must be between 4 and 20 characters!';
$_['error_confirm']  = 'Password confirmation does not match password!';
?>