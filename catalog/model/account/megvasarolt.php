<?php
class ModelAccountMegvasarolt extends Model {

    public function getPurchased($data = array(),$cOrderID) {
        $sql = "SELECT op.name, op.model, SUM(op.quantity) AS quantity, SUM(op.total + op.total * op.tax / 100) AS total
		FROM " . DB_PREFIX . "order_product op
		LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)";

        if (!empty($data['filter_order_status_id'])) {
            $sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "' AND op.product_id IN (".$cOrderID.")";
        } else {
            $sql .= " WHERE o.order_status_id > '0' AND op.product_id IN (".$cOrderID.")";
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

        //$sql .= " GROUP BY op.model ORDER BY total DESC";
        $sql .= " GROUP BY op.product_id ORDER BY total DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalPurchased($data,$cOrderID) {
        //$sql = "SELECT COUNT(DISTINCT op.model) AS total FROM `" . DB_PREFIX . "order_product` op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)";
        $sql = "SELECT COUNT(DISTINCT op.product_id) AS total FROM `" . DB_PREFIX . "order_product` op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)";


        if (!empty($data['filter_order_status_id'])) {
            $sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "' AND op.product_id IN (".$cOrderID.")";
        } else {
            $sql .= " WHERE o.order_status_id > '0' AND op.product_id IN (".$cOrderID.")";
        }



        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    function getcustormProduct(){
        $proId = array();

        $query = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product_customer` WHERE customer_id =".(int)$this->customer->getId()) ;
        if ($query->num_rows > 0){
            foreach($query->rows as $value) {
                $proId[]= $value['product_id'];
            }
            $proids = implode(',',$proId);
        } else {
            $proids = 0;
        }

        return $proids;
    }
	
}
?>