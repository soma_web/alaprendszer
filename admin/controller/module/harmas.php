<?php
class ControllerModuleHarmas extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('module/harmas');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('harmas', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['entry_fejlec_latszik'] = $this->language->get('entry_fejlec_latszik');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_content_top'] = $this->language->get('text_content_top');
        $this->data['text_content_bottom'] = $this->language->get('text_content_bottom');
        $this->data['text_column_left'] = $this->language->get('text_column_left');
        $this->data['text_column_right'] = $this->language->get('text_column_right');

        $this->data['entry_product'] = $this->language->get('entry_product');
        $this->data['entry_limit'] = $this->language->get('entry_limit');
        $this->data['entry_image'] = $this->language->get('entry_image');
        $this->data['entry_layout'] = $this->language->get('entry_layout');
        $this->data['entry_position'] = $this->language->get('entry_position');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $this->data['entry_show'] = $this->language->get('entry_show');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['button_add_module'] = $this->language->get('button_add_module');
        $this->data['button_remove'] = $this->language->get('button_remove');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');

        $this->data['text_ures'] = $this->language->get('text_ures');
        $this->data['text_mcs5'] = $this->language->get('text_mcs5');
        $this->data['text_korhinta'] = $this->language->get('text_korhinta');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['image'])) {
            $this->data['error_image'] = $this->error['image'];
        } else {
            $this->data['error_image'] = array();
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/harmas', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('module/harmas', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->request->post['harmas_product'])) {
            $this->data['harmas_product'] = $this->request->post['harmas_product'];
        } else {
            $this->data['harmas_product'] = $this->config->get('harmas_product');
        }

        $this->load->model('catalog/product');

        if (isset($this->request->post['harmas_product'])) {
            $products = explode(',', $this->request->post['harmas_product']);
        } else {
            $products = explode(',', $this->config->get('harmas_product'));
        }

        $this->data['products'] = array();

        foreach ($products as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                $this->data['products'][] = array(
                    'product_id' => $product_info['product_id'],
                    'name'       => $product_info['name'],
                    'model'      => $product_info['model']
                );
            }
        }

        $this->data['modules'] = array();

        if (isset($this->request->post['harmas_module'])) {
            $this->data['modules'] = $this->request->post['harmas_module'];
        } elseif ($this->config->get('harmas_module')) {
            $this->data['modules'] = $this->config->get('harmas_module');
        }

        if (isset($this->request->post['harmas_module_fejlec'])) {
            $this->data['harmas_module_fejlec'] = $this->request->post['harmas_module_fejlec'];
        } elseif ($this->config->get('harmas_module')) {
            $this->data['harmas_module_fejlec'] = $this->config->get('harmas_module_fejlec');
        } else {
            $this->data['harmas_module_fejlec'] = '';
        }

        $this->load->model('design/layout');

        $this->data['layouts'] = $this->model_design_layout->getLayouts();

        $this->template = 'module/harmas.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'module/harmas')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (isset($this->request->post['harmas_module'])) {
            foreach ($this->request->post['harmas_module'] as $key => $value) {
                if (!$value['image_width'] || !$value['image_height']) {
                    $this->error['image'][$key] = $this->language->get('error_image');
                }
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}
?>