<?php
$_['ar_text']          = array(
    'type'              => 'checkbox',
    'name'              => 'ar_text',
    'default'           => "1",
    'heading'           => '"Ár" szöveg megjelenítés'
);

$_['netto_text']          = array(
    'type'              => 'checkbox',
    'name'              => 'netto_text',
    'default'           => "1",
    'heading'           => '"Nettó" szöveg megjelenítés'
);

$_['null_off']          = array(
    'type'              => 'checkbox',
    'name'              => 'null_off',
    'default'           => "1",
    'heading'           => 'Nulla ne jelenjen meg'
);

$_['netto_plus_afa']          = array(
    'type'              => 'checkbox',
    'name'              => 'netto_plus_afa',
    'default'           => "1",
    'heading'           => '+Áfa felirat:'
);
$_['eredeti_ar_tiltas']          = array(
    'type'              => 'checkbox',
    'name'              => 'eredeti_ar_tiltas',
    'default'           => "1",
    'heading'           => 'Eredeti ár tiltás'
);

?>