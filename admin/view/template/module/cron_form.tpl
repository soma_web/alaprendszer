<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                    <tr id='program'>
                        <td><?php echo $entry_program; ?></td>
                        <td>
                            <select name="program" onchange="inputElement()">
                                <?php foreach($cron_files as $cron_file) { ?>
                                    <?php $selected = $cron_file == $program ? "selected" : "";?>
                                    <option value="<?php echo $cron_file?>" <?php echo $selected?>><?php echo $cron_file?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <?php if (!empty($cron_input_elements)) { ?>
                        <?php foreach ($cron_input_elements as $input_element_key=>$input_elements) { ?>
                            <?php if ($input_element_key == $program) { ?>
                                <?php foreach ($input_elements as $input_element) { ?>
                                    <tr class="input_elements">
                                        <td><?php echo $input_element['heading']; ?></td>
                                        <td>
                                            <?php if ($input_element['type'] == 'input') { ?>
                                                <input type='text' name="<?php echo $input_element['name']?>" value="<?php echo array_key_exists($input_element['name'],$seria_input_elements) ? $seria_input_elements[$input_element['name']] : ''; ?>" size="<?php echo !empty($input_element['size']) ? $input_element['size'] : 8 ?>">

                                            <?php } elseif ($input_element['type'] == 'select') { ?>

                                            <?php } elseif ($input_element['type'] == 'checkbox') { ?>
                                                <input name="<?php echo $input_element['name']?>"
                                                       type="checkbox" <?php echo array_key_exists($input_element['name'],$seria_input_elements) ? 'checked' : ''; ?> >

                                            <?php } elseif ($input_element['type'] == 'textarea') { ?>
                                        <?php } ?>
                                        </td>
                                    </tr>

                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>





                    <tr>
                        <td style="vertical-align: top"><?php echo $entry_rendszeresseg; ?></td>
                        <td>
                            <select name="rendszeresseg" onchange="
                                $(this).val() == 1 ? $('#naponta_rendszeresen').slideDown('slow') : $('#naponta_rendszeresen').slideUp('slow');
                                $(this).val() == 1 ? $('#naponta_rendszeresen select').prop('disabled',false) : $('#naponta_rendszeresen select').prop('disabled',true);

                                $(this).val() == 2 ? $('#naponta_idonkent').slideDown('slow') : $('#naponta_idonkent').slideUp('slow');
                                $(this).val() == 2 ? $('#naponta_idonkent input').prop('disabled',false) : $('#naponta_idonkent input').prop('disabled',true);


                                $(this).val() == 3 ? $('#hetente').slideDown('slow') : $('#hetente').slideUp('slow');
                                $(this).val() == 3 ? $('#hetente select, #hetente input').prop('disabled',false) : $('#hetente select, #hetente input').prop('disabled',true);

                                $(this).val() == 4 ? $('#havonta').slideDown('slow') : $('#havonta').slideUp('slow');
                                $(this).val() == 4 ? $('#havonta select, #havonta input').prop('disabled',false) : $('#havonta select, #havonta input').prop('disabled',true);
                                ">
                                    <option value="1" <?php echo $rendszeresseg == 1 ? "selected" : ''?>><?php echo $text_naponta_rendszeresen?></option>
                                    <option value="2" <?php echo $rendszeresseg == 2 ? "selected" : ''?>><?php echo $text_naponta_idonkent?></option>
                                    <option value="3" <?php echo $rendszeresseg == 3 ? "selected" : ''?>><?php echo $text_hetente?></option>
                                    <!--<option value="4" <?php echo $rendszeresseg == 4 ? "selected" : ''?>><?php echo $text_havonta?></option>-->
                            </select>


                            <?php $display = $rendszeresseg == 1 ? "" : "display: none" ?>
                            <div id='naponta_rendszeresen' style="<?php echo $display?>; margin-top: 20px;">
                                <select name="idonkent">
                                    <?php foreach($napi_rendszeressegek as $key=>$napi_rendszeresseg) { ?>
                                        <?php $selected = $key == $idonkent ? "selected" : "";?>
                                        <option value="<?php echo $key?>" <?php echo $selected?>><?php echo $napi_rendszeresseg?></option>
                                    <?php } ?>
                                </select>
                            </div>


                            <?php $display = $rendszeresseg == 2 ? "" : "display: none" ?>
                            <?php $naponta_idonkent_row = 0; ?>
                            <div id='naponta_idonkent' style="<?php echo $display?>; margin-top: 20px;">
                                <?php foreach($naponta_idonkent as $naponta_idopont) { ?>

                                    <input type="text" class="naponta_idonkent_<?php echo $naponta_idonkent_row?> time" name="naponta_idonkent[<?php echo $naponta_idonkent_row; ?>]" size="3" value="<?php echo substr($naponta_idopont,0,-3)?>" readonly>
                                    <a onclick="$('.naponta_idonkent_<?php echo $naponta_idonkent_row; ?>').remove();" class="naponta_idonkent_<?php echo $naponta_idonkent_row; ?>"><?php echo $button_remove; ?></a>
                                    <br class="naponta_idonkent_<?php echo $naponta_idonkent_row?>">
                                    <br class="naponta_idonkent_<?php echo $naponta_idonkent_row?>">
                                    <?php $naponta_idonkent_row++; ?>
                                <?php } ?>
                                <a onclick="addNewIdonkent();" class="button"><?php echo $button_add_idonkent; ?></a>
                            </div>


                            <?php $display = $rendszeresseg == 3 ? "" : "display: none" ?>
                            <?php $hetente_row = 0; ?>
                            <?php $hetente_idonkent_row = 0; ?>

                            <div id='hetente' style="<?php echo $display?>; margin-top: 20px;">
                                <?php foreach($hetente_napok as $het_napja) { ?>
                                    <select name="hetente_nap[<?php echo $hetente_row; ?>][het_napja]" class="hetente_nap_<?php echo $hetente_row?> hetvalaszto">
                                        <option value="1" <?php echo $het_napja['het_napja'] == 1 ? 'selected' : ''?>><?php echo $text_hetfo?></option>
                                        <option value="2" <?php echo $het_napja['het_napja'] == 2 ? 'selected' : ''?>><?php echo $text_kedd?></option>
                                        <option value="3" <?php echo $het_napja['het_napja'] == 3 ? 'selected' : ''?>><?php echo $text_szerda?></option>
                                        <option value="4" <?php echo $het_napja['het_napja'] == 4 ? 'selected' : ''?>><?php echo $text_csutortok?></option>
                                        <option value="5" <?php echo $het_napja['het_napja'] == 5 ? 'selected' : ''?>><?php echo $text_pentek?></option>
                                        <option value="6" <?php echo $het_napja['het_napja'] == 6 ? 'selected' : ''?>><?php echo $text_szombat?></option>
                                        <option value="7" <?php echo $het_napja['het_napja'] == 7 ? 'selected' : ''?>><?php echo $text_vasarnap?></option>
                                    </select>
                                    <a onclick="$('.hetente_nap_<?php echo $hetente_row; ?>').remove();" class="hetente_nap_<?php echo $hetente_row; ?> hetnaptorol"><?php echo $button_remove; ?></a>

                                    <div id="hetente_idopont_<?php echo $hetente_row?>" class="hetente_nap_<?php echo $hetente_row; ?> het_idopontjai">
                                        <?php foreach($het_napja['idopontok'] as $idopont) { ?>
                                            <?php if ($idopont) { ?>
                                                <input type="text" class="hetente_idonkent_<?php echo $hetente_idonkent_row?> time hetente_nap_<?php echo $hetente_row?>"
                                                       name="hetente_nap[<?php echo $hetente_row; ?>][idopontok][]"
                                                       size="3" value="<?php echo substr($idopont,0,-3)?>" readonly>

                                                <a onclick="$('.hetente_idonkent_<?php echo $hetente_idonkent_row; ?>').remove();"
                                                        class="hetente_idonkent_<?php echo $hetente_idonkent_row; ?> hetente_nap_<?php echo $hetente_row?>"><?php echo $button_remove; ?></a>

                                                <br class="hetente_idonkent_<?php echo $hetente_idonkent_row?> hetente_nap_<?php echo $hetente_row?>">
                                                <br class="hetente_idonkent_<?php echo $hetente_idonkent_row?> hetente_nap_<?php echo $hetente_row?>">

                                                <?php $hetente_idonkent_row++; ?>
                                            <?php } ?>
                                        <?php } ?>

                                        <a onclick="addNewHetIdo('<?php echo $hetente_row?>');" class="hetente_nap_<?php echo $hetente_row?> addNewHetIdo"><?php echo $button_add_idonkent; ?></a>
                                    </div>
                                    <?php $hetente_row++; ?>

                                <?php } ?>
                                <a onclick="addNewHet();" class="button" id="addNewHet"><?php echo $button_add_nap; ?></a>
                            </div>



                            <?php $display = $rendszeresseg == 4 ? "" : "display: none" ?>
                            <?php $havonta_row = 0; ?>
                            <?php $havonta_idonkent_row = 0; ?>

                            <div id='havonta' style="<?php echo $display?>; margin-top: 20px;">
                                <?php foreach($hetente_napok as $honap_napja) { ?>
                                    <input name="havonta_nap[<?php echo $havonta_row; ?>][honap_napja]" class="havonta_nap_<?php echo $havonta_row?> honapvalaszto date" size="8">
                                    <!--<select name="havonta_nap[<?php echo $havonta_row; ?>][honap_napja]" class="havonta_nap_<?php echo $havonta_row?> honapvalaszto">
                                        <option value="1" <?php echo $honap_napja['honap_napja'] == 1 ? 'selected' : ''?>><?php echo $text_hetfo?></option>
                                        <option value="2" <?php echo $honap_napja['honap_napja'] == 2 ? 'selected' : ''?>><?php echo $text_kedd?></option>
                                        <option value="3" <?php echo $honap_napja['honap_napja'] == 3 ? 'selected' : ''?>><?php echo $text_szerda?></option>
                                        <option value="4" <?php echo $honap_napja['honap_napja'] == 4 ? 'selected' : ''?>><?php echo $text_csutortok?></option>
                                        <option value="5" <?php echo $honap_napja['honap_napja'] == 5 ? 'selected' : ''?>><?php echo $text_pentek?></option>
                                        <option value="6" <?php echo $honap_napja['honap_napja'] == 6 ? 'selected' : ''?>><?php echo $text_szombat?></option>
                                        <option value="7" <?php echo $honap_napja['honap_napja'] == 7 ? 'selected' : ''?>><?php echo $text_vasarnap?></option>
                                    </select>-->
                                    <a onclick="$('.havonta_nap_<?php echo $havonta_row; ?>').remove();" class="havonta_nap_<?php echo $havonta_row; ?> honapnaptorol"><?php echo $button_remove; ?></a>

                                    <div id="havonta_idopont_<?php echo $havonta_row?>" class="havonta_nap_<?php echo $havonta_row; ?> honap_idopontjai">
                                        <?php foreach($honap_napja['idopontok'] as $idopont) { ?>
                                            <?php if ($idopont) { ?>
                                                <input type="text" class="havonta_idonkent_<?php echo $havonta_idonkent_row?> time havonta_nap_<?php echo $havonta_row?>"
                                                       name="havonta_nap[<?php echo $havonta_row; ?>][idopontok][]"
                                                       size="3" value="<?php echo substr($idopont,0,-3)?>" readonly>

                                                <a onclick="$('.havonta_idonkent_<?php echo $havonta_idonkent_row; ?>').remove();"
                                                   class="havonta_idonkent_<?php echo $havonta_idonkent_row; ?> havonta_nap_<?php echo $havonta_row?>"><?php echo $button_remove; ?></a>

                                                <br class="havonta_idonkent_<?php echo $havonta_idonkent_row?> havonta_nap_<?php echo $havonta_row?>">
                                                <br class="havonta_idonkent_<?php echo $havonta_idonkent_row?> havonta_nap_<?php echo $havonta_row?>">

                                                <?php $havonta_idonkent_row++; ?>
                                            <?php } ?>
                                        <?php } ?>

                                        <a onclick="addNewHonapIdo('<?php echo $havonta_row?>');" class="havonta_nap_<?php echo $havonta_row?> addNewHonapIdo"><?php echo $button_add_idonkent; ?></a>
                                    </div>
                                    <?php $havonta_row++; ?>

                                <?php } ?>
                                <a onclick="addNewHonap();" class="button" id="addNewHonap"><?php echo $button_add_nap; ?></a>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_status; ?></td>
                        <td>
                            <select name="status">
                                <?php if ($status) { ?>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <?php } else { ?>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>

<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script>
    var naponta_idonkent_row = <?php echo isset($naponta_idonkent_row) ? $naponta_idonkent_row : 0; ?>;
    var hetente_row = <?php echo isset($hetente_row) ? $hetente_row : 0; ?>;
    var hetente_idonkent_row = <?php echo isset($hetente_idonkent_row) ? $hetente_idonkent_row : 0; ?>;
    var havonta_row = <?php echo isset($havonta_row) ? $havonta_row : 0; ?>;
    var havonta_idonkent_row = <?php echo isset($havonta_idonkent_row) ? $havonta_idonkent_row : 0; ?>;
    oraBeallitas();
    datumBeallitas();

    function oraBeallitas() {
        $('.time').timepicker({
            currentText:    '<?php echo $text_timepicker_currentText?>',
            closeText:      '<?php echo $text_timepicker_closeText?>',
            ampm: false,
            timeFormat: 'hh:mm tt',
            timeOnlyTitle:  '<?php echo $text_timepicker_timeOnlyTitle?>',
            timeText:       '<?php echo $text_timepicker_timeText?>',
            hourText:       '<?php echo $text_timepicker_hourText?>',
            minuteText:     '<?php echo $text_timepicker_minuteText?>',
            secondText:     '<?php echo $text_timepicker_secondText?>',
            timezoneText:   '<?php echo $text_timepicker_timezoneText?>',
            hourMax: 24,
            stepMinute: 5
        });
    }

    function datumBeallitas() {
        $('.date').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: false,
            changeYear: false,
            showButtonPanel: false,
            currentText: 'asdfjklhasdf',
            monthNames: [''],
            beforeShowDay: false,
            showMonthAfterYear: false,
            showWeek: false
        });


    }

    function addNewIdonkent() {
        var html  = '';

        html  = '<input type="text" class="naponta_idonkent_'+naponta_idonkent_row+' time" name="naponta_idonkent['+naponta_idonkent_row+']" size="3" value="00:00">';
        html += ' <a onclick="$(\'.naponta_idonkent_'+naponta_idonkent_row+'\').remove();" class="naponta_idonkent_'+naponta_idonkent_row+'"><?php echo $button_remove; ?></a>';
        html += '<br class="naponta_idonkent_'+naponta_idonkent_row+'">';
        html += '<br class="naponta_idonkent_'+naponta_idonkent_row+'">';


        if ($('#naponta_idonkent br').length == 0) {
            $('#naponta_idonkent').prepend(html);

        } else {
            $('#naponta_idonkent br:last').after(html);
        }
        oraBeallitas();

        naponta_idonkent_row++;
    }

    function addNewHet() {

        var html = '';
        html += '<select name="hetente_nap['+hetente_row+'][het_napja]" class="hetente_nap_'+hetente_row+' hetvalaszto">';
            html += '<option value="1"><?php echo $text_hetfo?></option>';
            html += '<option value="2"><?php echo $text_kedd?></option>';
            html += '<option value="3"><?php echo $text_szerda?></option>';
            html += '<option value="4"><?php echo $text_csutortok?></option>';
            html += '<option value="5"><?php echo $text_pentek?></option>';
            html += '<option value="6"><?php echo $text_szombat?></option>';
            html += '<option value="7"><?php echo $text_vasarnap?></option>';
        html += '</select>';

        html += ' <a onclick="$(\'.hetente_nap_'+hetente_row+'\').remove();" class="hetente_nap_'+hetente_row+' hetnaptorol"><?php echo $button_remove; ?></a>';



        html += '<div id="hetente_idopont_'+hetente_row+'" class="hetente_nap_'+hetente_row+' het_idopontjai">';

            html += '<a onclick="addNewHetIdo(\''+hetente_row+'\');" class="hetente_nap_'+hetente_row+' addNewHetIdo"><?php echo $button_add_idonkent; ?></a>';
        html += '</div>';

        hetente_row ++;
        $('#addNewHet').before(html);

    }

    function addNewHetIdo(hetente_sor) {
        var html  = '';

        html += '<input type="text" class="hetente_idonkent_'+hetente_idonkent_row+' time hetente_nap_'+hetente_sor+'" ';
        html += 'name="hetente_nap['+hetente_sor+'][idopontok][]" ';
            html += 'size="3" value="00:00" readonly> ';

        html += ' <a onclick="$(\'.hetente_idonkent_'+hetente_idonkent_row+'\').remove();" ';
            html += 'class="hetente_idonkent_'+hetente_idonkent_row+' hetente_nap_'+hetente_sor+'"><?php echo $button_remove; ?></a>';

        html += '<br class="hetente_idonkent_'+hetente_idonkent_row+' hetente_nap_'+hetente_sor+'">';
        html += '<br class="hetente_idonkent_'+hetente_idonkent_row+' hetente_nap_'+hetente_sor+'">';

        $('#hetente_idopont_'+hetente_sor+' a.addNewHetIdo').before(html);

        hetente_idonkent_row++;
        oraBeallitas();

    }


    function addNewHonap() {

        var html = '';
        html += '<input name="havonta_nap['+havonta_row+'][honap_napja]" class="havonta_nap_'+havonta_row+' honapvalaszto date" size="8">';


            /*html += '<select name="havonta_nap['+havonta_row+'][honap_napja]" class="havonta_nap_'+havonta_row+' honapvalaszto">';
            html += '<option value="1"><?php echo $text_hetfo?></option>';
        html += '<option value="2"><?php echo $text_kedd?></option>';
        html += '<option value="3"><?php echo $text_szerda?></option>';
        html += '<option value="4"><?php echo $text_csutortok?></option>';
        html += '<option value="5"><?php echo $text_pentek?></option>';
        html += '<option value="6"><?php echo $text_szombat?></option>';
        html += '<option value="7"><?php echo $text_vasarnap?></option>';
        html += '</select>';*/

        html += ' <a onclick="$(\'.havonta_nap_'+havonta_row+'\').remove();" class="havonta_nap_'+havonta_row+' honapnaptorol"><?php echo $button_remove; ?></a>';



        html += '<div id="havonta_idopont_'+havonta_row+'" class="havonta_nap_'+havonta_row+' honap_idopontjai">';

        html += '<a onclick="addNewHonapIdo(\''+havonta_row+'\');" class="havonta_nap_'+havonta_row+' addNewHonapIdo"><?php echo $button_add_idonkent; ?></a>';
        html += '</div>';

        havonta_row ++;
        $('#addNewHonap').before(html);
        datumBeallitas();
    }

    function addNewHonapIdo(havonta_sor) {
        var html  = '';

        html += '<input type="text" class="havonta_idonkent_'+havonta_idonkent_row+' time havonta_nap_'+havonta_sor+'" ';
        html += 'name="havonta_nap['+havonta_sor+'][idopontok][]" ';
        html += 'size="3" value="00:00" readonly> ';

        html += ' <a onclick="$(\'.havonta_idonkent_'+havonta_idonkent_row+'\').remove();" ';
        html += 'class="havonta_idonkent_'+havonta_idonkent_row+' havonta_nap_'+havonta_sor+'"><?php echo $button_remove; ?></a>';

        html += '<br class="havonta_idonkent_'+havonta_idonkent_row+' havonta_nap_'+havonta_sor+'">';
        html += '<br class="havonta_idonkent_'+havonta_idonkent_row+' havonta_nap_'+havonta_sor+'">';

        $('#havonta_idopont_'+havonta_sor+' a.addNewHonapIdo').before(html);

        havonta_idonkent_row++;
        oraBeallitas();

    }

    function inputElement() {
        var program = $("select[name='program']").val();
        var html = '';


        <?php if (!empty($cron_input_elements)) { ?>
            <?php foreach ($cron_input_elements as $input_element_key=>$input_elements) { ?>
                if (program == "<?php echo $input_element_key?>") {
                    <?php foreach ($input_elements as $input_element) { ?>
                        html += '<tr class="input_elements" style="display: none">';
                            html += '<td><?php echo $input_element['heading']; ?></td>';
                            html += '<td>';
                            <?php if ($input_element['type'] == 'input') { ?>
                                html += '<input name="<?php echo $input_element['name']?>" value="<?php echo array_key_exists($input_element['name'],$seria_input_elements) ? $seria_input_elements[$input_element['name']] : ''; ?>" size="<?php echo !empty($input_element['size']) ? $input_element['size'] : 8 ?>">';
                            <?php } elseif ($input_element['type'] == 'select') { ?>
                            <?php } elseif ($input_element['type'] == 'checkbox') { ?>
                            <?php } elseif ($input_element['type'] == 'textarea') { ?>
                            <?php } ?>
                            html += '</td>';
                        html += '</tr>';

                    <?php } ?>
                }
            <?php } ?>
        <?php } ?>

        if ($('.input_elements').length > 0 ) {
            $('.input_elements').fadeOut('slow',function(){
                $('.input_elements').remove();
                $('#program').after(html);
                $('.input_elements').fadeIn('slow')
            });
        } else {
            $('#program').after(html);
            $('.input_elements').fadeIn('slow')

        }
        debugger;


    }
</script>
