<?php

// Heading
$_['heading_title']      = 'PSIGate';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Siker: A PSIGate fiók részleteinek módosítása megtörtént!';

// Entry
$_['entry_merchant']     = 'Kereskedő ID:';
$_['entry_password']     = 'Jelszó:';
$_['entry_gateway']      = 'Gateway URL:';
$_['entry_test']         = 'Teszt Mód:';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';

// Error
$_['error_permission']   = 'Figyelmeztetés: Az PSIGate történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_merchant']     = 'Kereskedő ID szükséges!';
$_['error_password']     = 'Jelszó szükséges!';
$_['error_gateway']      = 'Gateway URL szükséges!';
?>
