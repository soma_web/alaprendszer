<?php  
class ControllerModuleManufacturerKep extends Controller {
	protected function index($setting) {

        $this->language->load('product/manufacturer');

        $this->load->model('catalog/manufacturer');

        $this->data['heading_title'] = $this->language->get('heading_title');

        $results = $this->model_catalog_manufacturer->getManufacturers();
        foreach($results as $key=>$result){
            if (empty($result['image'])) {
                unset ($results[$key]);
            } else {
			    $results[$key]['image'] = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_wishlist_width'), $this->config->get('config_image_wishlist_height'),false );

            }
        }

        $this->data['manufacturers'] = $this->config->rendezes($results,'sort_order');
        $this->data['fejlec'] = $this->config->get("manufacturer_module_fejlec") == 1 ? true : false;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer_kep.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/manufacturer_kep.tpl';
		} else {
			$this->template = 'default/template/module/manufacturer_kep.tpl';
		}
		
		$this->render();
	}
}
?>