<?php
class ControllerReportSaleUploaders extends Controller {
	public function index() {  
		$this->load->language('report/sale_uploaders');

		$this->document->setTitle($this->language->get('heading_title'));

        $url = '';

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

   		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/sale_uploaders', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->load->model('report/sale');

        $queryFilter = array(
            'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'                  => $this->config->get('config_admin_limit')
        );

        $uploaders = $this->model_report_sale->getUploaders($queryFilter);
        $vasaroltak = $this->model_report_sale->getSelledProductsByUploaders();

        $total = array_key_exists('total', $uploaders) ? $uploaders['total'] : 0;
        $feltoltok = array_key_exists('uploaders', $uploaders) ? $uploaders['uploaders'] : array();

        $sorok = array();
        foreach($feltoltok as $customer_id=>$customer) {
            $sor = $customer;
            $sor['name'] = $customer['firstname']." ".$customer['lastname'];
            if(array_key_exists($customer_id, $vasaroltak)) {
                $sor['vasaroltak'] = $vasaroltak[$customer_id];
            } else {
                $sor['vasaroltak'] = 0;
            }

            $sorok[] = $sor;
        }

        $sorted = $this->config->rendezes($sorok, 'feltoltott', 'DESC', 'vasaroltak', 'DESC');

        $this->data['sorok'] = $sorted;

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');
		
		$this->data['column_uploaders'] = $this->language->get('column_uploaders');
		$this->data['column_uploaded'] = $this->language->get('column_uploaded');
    	$this->data['column_selled'] = $this->language->get('column_selled');

		$this->data['button_nyomtatas'] = $this->language->get('button_nyomtatas');
		$this->data['button_exportalas'] = $this->language->get('button_exportalas');

		$this->data['token'] = $this->session->data['token'];

		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('report/sale_uploaders', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			
		$this->data['pagination'] = $pagination->render();

        $this->data['csv_be']  = $this->url->link('report/sale_uploaders_csvbe', 'token=' . $this->session->data['token']  .$url, 'SSL');


		$this->template = 'report/sale_uploaders.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
}
?>