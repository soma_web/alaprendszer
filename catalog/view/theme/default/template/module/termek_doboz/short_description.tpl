<?php if(isset($beallitas['lenyilo']) && $beallitas['lenyilo'] && isset($product['short_descr_lista']) && ($product['short_descr_lista']) != "") { ?>
    <style>
        #container .termek_tabban:hover {
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
            border-bottom: 0;
        }
    </style>
    <script type="text/javascript">
        /* .:Lenyíló div:. - pl.: Optiweb - termékdobozoknál
        *
        *   Működés: Mouseenter esetén meghatározza az adminban megadott
        *   div (class alapján) pozícióját (bottom, left) és a másik
        *   megadott divet a kiszámolt pozícióhoz igazítja.
        *
        * */
        /* CSS a stylesheet.css-ben, itt csak a működés zajlik */
        $('.<?php echo $beallitas['lenyilo']; ?>').mouseenter(function(){
            if($("div.sdesc").length == 0){
                var short = $(this).find('.<?php echo $beallitas['lenyilo_mit']; ?>').clone().html();
                var left = $(this).offset().left;
                var top = $(this).offset().top;
                var bottom = top+$(this).outerHeight();
                var width = $(this).outerWidth();
                $('body').append('<div class="sdesc" id="sdecs_div" style="width: '+width+'px; display: none;">'+short+'</div>');
                $('.sdesc').offset({top:bottom-2, left:left});
                $('.sdesc').stop().slideDown(300);
            }
        });

        /*
        * .:Lenyíló div:. - eltűntetés
        *
        *   Mouseleave esetén (kimegyünk az kurzorral a megadott divből)
        *   Eltűntetjük az előző pontban megjelenített divet.
        *
        * */

        $('.<?php echo $beallitas['lenyilo']; ?>').mouseleave(function(){
            $('div.sdesc').remove();
        });




    </script>
<?php } ?>


<div class="description">
    <?php //echo mb_substr($product['short_descr_lista'], 0, $beallitas['karakter'], "UTF-8");
    if(!empty($product['short_descr_lista']) && strlen($product['short_descr_lista']) > $beallitas['karakter'] ){
        //echo "...";
    } ?>
    <?php echo (isset($product['short_descr_lista']) ? $product['short_descr_lista'] : ""); ?>
</div>