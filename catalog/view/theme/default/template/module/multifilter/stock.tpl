<?php if(isset($stocks) && $stocks){ ?>


    <div class="box">
        <div class="box-heading reszek masodik-filter"><?php echo $heading_title_raktaron; ?>
            <img style="display: none"  src="catalog/view/theme/default/image/responsive/pos_r_opened.png">
        </div>

        <div class="box-content">
            <ul class="box-filter box-filter-raktar">
                <?php $raktaron_van     = ""?>
                <?php $raktaron_nincs   = ""?>

                <?php if (isset($filter_raktaron) && count($filter_raktaron) > 0 ) {?>
                    <?php if (in_array("2",$filter_raktaron)) { ?>
                        <?php $raktaron_van = "checked='checked'"?>
                    <?php } ?>

                    <?php if (in_array("1", $filter_raktaron)) { ?>
                        <?php $raktaron_nincs = "checked='checked'"?>
                    <?php } ?>
                <?php } ?>

                <?php if ($beallitasok['raktaron']['teljes'] != 1) { ?>

                    <?php if ($stocks['raktaron']) { ?>
                        <li><input class="css-checkbox raktaronvan" type="checkbox" value="2" id="raktaron" <?php echo $raktaron_van; ?> />
                            <label class="css-label mac-style raktaronvan" for="raktaron"><?php echo $entry_raktaron; ?></label>
                        </li>
                    <?php } ?>
                    <?php if ($stocks['nincs_raktaron']) { ?>
                        <li><input class="css-checkbox raktaronvan" type="checkbox" value="1" id="nincs_raktaron" <?php echo $raktaron_nincs; ?> />
                            <label class="css-label mac-style raktaronvan" for="nincs_raktaron"><?php echo $entry_nincs_raktaron; ?></label>
                        </li>
                    <?php } ?>

                <?php } else {?>
                    <?php if ($stocks['raktaron']) { ?>
                        <li><input class="css-checkbox raktaronvan" type="checkbox" value="2" id="raktaron" <?php echo $raktaron_van; ?> />
                            <label class="css-label mac-style raktaronvan" for="raktaron"><?php echo $entry_raktaron; ?></label>
                        </li>
                    <?php } else {?>
                        <li><input class="css-checkbox raktaronvan" type="checkbox" value="2" id="raktaron" <?php echo $raktaron_van; ?> disabled="disabled"/>
                            <label class="css-label mac-style raktaronvan halvany" for="raktaron"><?php echo $entry_raktaron; ?></label>
                        </li>
                    <?php } ?>

                    <?php if ($stocks['nincs_raktaron']) { ?>
                        <li><input class="css-checkbox raktaronvan" type="checkbox" value="1" id="nincs_raktaron" <?php echo $raktaron_nincs; ?> />
                            <label class="css-label mac-style raktaronvan" for="nincs_raktaron"><?php echo $entry_nincs_raktaron; ?></label>
                        </li>
                    <?php } else {?>
                        <li><input class="css-checkbox raktaronvan" type="checkbox" value="1" id="nincs_raktaron" <?php echo $raktaron_nincs; ?>  disabled="disabled"/>
                            <label class="css-label mac-style raktaronvan halvany" for="nincs_raktaron"><?php echo $entry_nincs_raktaron; ?></label>
                        </li>
                    <?php } ?>
                <?php } ?>

            </ul>
        </div>
    </div>


    <script>

        $("#raktaron").bind("click",function(){
            $("#nincs_raktaron").prop("checked",false);
        });

        $("#nincs_raktaron").bind("click",function(){
            $("#raktaron").prop("checked",false);
        });

    </script>



<?php } ?>