<?php
// Heading 
$_['heading_title'] = 'Cart';

// Text
$_['text_items']    = '%s item(s) - %s';
$_['text_empty']    = 'Your shopping cart is empty!';
$_['text_cart']     = 'View Cart';
$_['text_checkout'] = 'Checkout';
$_['text_ingyenes_fizetos'] = 'Checkout / Download';
$_['text_ingyenes']         = 'Download';
$_['total_title']           = 'All:';
$_['text_termek']           = 'Product';
?>