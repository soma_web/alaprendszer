<?php $display = '';?>
<?php if (!empty($csuszka_tol_ig[$value['attribute_id']][0]) && !empty($csuszka_tol_ig[$value['attribute_id']][1]) && $csuszka_tol_ig[$value['attribute_id']][0] == $csuszka_tol_ig[$value['attribute_id']][1]) { ?>
    <?php //$display = 'display:none';?>
<?php } ?>
    <div class="box tol-ig" style="min-height: 60px; max-width: 100%; font-size: 0; <?php echo $display;?> ">

        <div style="font-size: 12px"><?php echo $value['name']; ?></div>
        <div class="csuszka_tol_ig">



            <div id="attr-tolig-<?php echo $value['attribute_id']?>" class="box-content box-filter_artol box-filter" style="font-size: 12px; width: 94%; margin: auto; margin-top: 0;">


                <div id="slider-attribute-<? echo $value['attribute_id']?>" class="slider-csuszka slider"></div>


                <div class="attribute_also_felso_hatar" style="margin-top: 6px; width: 107%; position: relative; left: -7px; font-size: 0; height: auto">
                    <span style="font-size: 12px; display: inline-block; width: 50%;"
                            id="csuszka_tol-<?php echo $value['attribute_id']?>"
                            class="csuszka_tol"
                            minimum="<?php echo $value['csuszka_tol']?>"
                            attribute_id="<?php echo $value['attribute_id']?>"
                            value= "<?php echo ( !empty($csuszka_tol_ig[$value['attribute_id']][0]) ) ? $csuszka_tol_ig[$value['attribute_id']][0] : $value['csuszka_tol']?>" >

                        <span style="color: #fc0808;">
                            <?php echo ( !empty($csuszka_tol_ig[$value['attribute_id']][0]) ) ? $csuszka_tol_ig[$value['attribute_id']][0] : $value['csuszka_tol']; ?>
                        </span>
                    </span>

                     <span style="font-size: 12px;  display: inline-block; width: 50%; text-align: right;"
                           id="csuszka_ig-<?php echo $value['attribute_id']?>"
                           class="csuszka_ig"
                           attribute_id="<?php echo $value['attribute_id']?>"
                           maximum="<?php echo $value['csuszka_ig']?>"
                           attribute_id="<?php echo $value['attribute_id']?>"
                           value= "<?php echo ( !empty($csuszka_tol_ig[$value['attribute_id']][1]) ) ? $csuszka_tol_ig[$value['attribute_id']][1] : $value['csuszka_ig']?>" >


                          <span style="color: #fc0808;">
                            <?php echo ( !empty($csuszka_tol_ig[$value['attribute_id']][1]) ) ? $csuszka_tol_ig[$value['attribute_id']][1] : $value['csuszka_ig']; ?>
                          </span>
                    </span>
                </div>
            </div>
        </div>

    </div>

    <script>
        $( "#slider-attribute-<? echo $value['attribute_id']?>" ).slider({
            range: true,

            min: <?php echo  $value['csuszka_tol']?>,
            max: <?php echo  $value['csuszka_ig']?>,

            values: [ <?php echo !empty($csuszka_tol_ig[$value['attribute_id']][0]) ? $csuszka_tol_ig[$value['attribute_id']][0] : $value['csuszka_tol']?>,
                <?php echo !empty($csuszka_tol_ig[$value['attribute_id']][1]) ? $csuszka_tol_ig[$value['attribute_id']][1] : $value['csuszka_ig']?> ],



            slide: function( event, ui ) {
                $( "#csuszka_tol-<? echo $value['attribute_id']?>" ).html(ui.values[0]);
                $( "#csuszka_tol-<? echo $value['attribute_id']?>" ).attr('value',ui.values[0] );
                //$( "#csuszka_tol-<? echo $value['attribute_id']?>" ).val(ui.values[0] );

                $( "#csuszka_ig-<? echo $value['attribute_id']?>" ).html(ui.values[1]);
                $( "#csuszka_ig-<? echo $value['attribute_id']?>" ).attr('value',ui.values[1]);
            },
            change: function(event, ui) {
                $('#slider-attribute-<? echo $value['attribute_id']?>').trigger('click');
            }
        });
        //min: <?php echo  isset($csuszka_tol_ig[$value['attribute_id']][0]) ? $csuszka_tol_ig[$value['attribute_id']][0] : $value['csuszka_tol']?>,
        //max: <?php echo  isset($csuszka_tol_ig[$value['attribute_id']][1]) ? $csuszka_tol_ig[$value['attribute_id']][1] : $value['csuszka_ig']?>,

    </script>

    <style>
        .ui-widget-header {
            background: #df190a;
            background: -webkit-linear-gradient(top, #df190a, #ddd);
            background: -moz-linear-gradient(top, #df190a, #ddd);
            background: linear-gradient(top, #df190a, #ddd);
            -webkit-box-shadow: inset 0 2px 4px rgba(0,0,0,0.1);
            -moz-box-shadow: inset 0 2px 4px rgba(0,0,0,0.1);
            box-shadow: inset 0 2px 4px rgba(0,0,0,0.1);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            border: 1px solid #aaa;
            height: 4px;
            background: linear-gradient(#4366ce, #104f9c);
            background: #fff;

        }
    </style>



