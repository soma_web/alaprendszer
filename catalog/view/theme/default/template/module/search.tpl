
<div class="box">

    <?php if ($this->config->get('search_module_fejlec') ) { ?>
        <div class="box-heading"><?php echo $heading_title; ?></div>
    <?php } ?>
    <div class="box-content">
        <div class="search-content">
            <div id="search_module">
                <?php if ($filter_name) { ?>
                    <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
                <?php } else { ?>
                    <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '#ffffff';" />
                <?php } ?>
                <div id="button-search-module" class="button-search-module"></div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"><!--
$('#search_module input[name=\'filter_name\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-search-module').trigger('click');
	}
});

$('#button-search-module').bind('click', function() {

    url = 'index.php?route=product/search';
	var filter_name = $('#search_module input[name=\'filter_name\']').attr('value');
	var filter_model = $('#search_module input[name=\'filter_model\']').attr('value');
	var filter_cikkszam = $('#search_module input[name=\'filter_cikkszam\']').attr('value');
	var filter_cikkszam2 = $('#search_module input[name=\'filter_cikkszam2\']').attr('value');
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

    if (filter_model) {
        url += '&filter_model=' + encodeURIComponent(filter_model);
    }
    if (filter_cikkszam) {
        url += '&filter_cikkszam=' + encodeURIComponent(filter_cikkszam);
    }
    if (filter_cikkszam2) {
        url += '&filter_cikkszam2=' + encodeURIComponent(filter_cikkszam2);
    }

	var filter_category_id = $('#search_module select[name=\'filter_category_id\']').attr('value');
	
	if (filter_category_id > 0) {
		url += '&filter_category_id=' + encodeURIComponent(filter_category_id);
	}
	
	var filter_sub_category = $('#search_module input[name=\'filter_sub_category\']:checked').attr('value');
	
	if (filter_sub_category) {
		url += '&filter_sub_category=true';
	}
		
	var filter_description = $('#search_module input[name=\'filter_description\']:checked').attr('value');
	
	if (filter_description) {
		url += '&filter_description=true';
	}

	location = url;
});

//--></script>
