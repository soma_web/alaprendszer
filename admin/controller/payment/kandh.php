<?php 
class ControllerPaymentKandH extends Controller {
	private $error = array(); 
	 
	public function index() { 
		$this->load->language('payment/kandh');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			if(!empty($this->request->files['kandh_prod_key_file']['name']))
			{
				$this->request->post['kandh_prod_key_file'] = $this->request->files['kandh_prod_key_file']['name'];
				$this->saveFile($this->request->files['kandh_prod_key_file']['tmp_name'], $this->request->files['kandh_prod_key_file']['name']);	
			}
			else
			{
				$this->request->post['kandh_prod_key_file'] = $this->request->post['kandh_tmp_prod_key_file'];
			}
			
			$this->model_setting_setting->editSetting('kandh', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_bank_mode_block'] = $this->language->get('text_bank_mode_block');
		$this->data['text_bank_mode_load'] = $this->language->get('text_bank_mode_load');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');		
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_order_failed_status'] = $this->language->get('entry_order_failed_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_shop_code'] = $this->language->get('entry_shop_code');
		$this->data['entry_test_mode'] = $this->language->get('entry_test_mode');
		$this->data['entry_prod_key_file'] = $this->language->get('entry_prod_key_file');
		$this->data['entry_payment_language'] = $this->language->get('entry_payment_language');
		$this->data['entry_payment_currency'] = $this->language->get('entry_payment_currency');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      =>  $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),       		
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/kandh', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('payment/kandh', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');	
				
		if (isset($this->request->post['kandh_order_status_id'])) {
			$this->data['kandh_order_status_id'] = $this->request->post['kandh_order_status_id'];
		} else {
			$this->data['kandh_order_status_id'] = $this->config->get('kandh_order_status_id'); 
		} 
		
		$this->load->model('localisation/order_status');
		
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
				
		if (isset($this->request->post['kandh_status'])) {
			$this->data['kandh_status'] = $this->request->post['kandh_status'];
		} else {
			$this->data['kandh_status'] = $this->config->get('kandh_status');
		}
				
		if (isset($this->request->post['kandh_failed_status'])) {
			$this->data['kandh_failed_status'] = $this->request->post['kandh_failed_status'];
		} else {
			$this->data['kandh_failed_status'] = $this->config->get('kandh_failed_status');
		}
		if (isset($this->request->post['kandh_sort_order'])) {
			$this->data['kandh_sort_order'] = $this->request->post['kandh_sort_order'];
		} else {
			$this->data['kandh_sort_order'] = $this->config->get('kandh_sort_order');
		}
		
		if (isset($this->request->post['kandh_shop_code'])) 
		{
			$this->data['kandh_shop_code'] = $this->request->post['kandh_shop_code'];
		} 
		else 
		{
			$this->data['kandh_shop_code'] = $this->config->get('kandh_shop_code');
		}
		
		if (!empty($this->request->post['kandh_prod_key_file'])) 
		{
			$this->data['kandh_prod_key_file'] = $this->request->post['kandh_prod_key_file'];
		} 
		else 
		{
			$this->data['kandh_prod_key_file'] = $this->config->get('kandh_prod_key_file');
		}
		
		if (isset($this->request->post['kandh_payment_language'])) 
		{
			$this->data['kandh_payment_language'] = $this->request->post['kandh_payment_language'];
		} 
		else
		{
			$this->data['kandh_payment_language'] = $this->config->get('kandh_payment_language');
		}
		
		if (isset($this->request->post['kandh_is_test'])) 
		{
			$this->data['kandh_is_test'] = $this->request->post['kandh_is_test'];
		} 
		else
		{
			$this->data['kandh_is_test'] = $this->config->get('kandh_is_test');
		}
		
		if (isset($this->request->post['kandh_product_code'])) 
		{
			$this->data['kandh_product_code'] = $this->request->post['kandh_product_code'];
		} 
		else 
		{
			$this->data['kandh_product_code'] = $this->config->get('kandh_product_code');
		}
		
		if (isset($this->request->post['kandh_payment_currency'])) 
		{
			$this->data['kandh_payment_currency'] = $this->request->post['kandh_payment_currency'];
		} 
		else 
		{
			$this->data['kandh_payment_currency'] = $this->config->get('kandh_payment_currency');
		}
		
		
		$this->template = 'payment/kandh.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
			
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/kandh')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	private function saveFile($tmpName, $name)
	{
		move_uploaded_file($tmpName, DIR_SYSTEM . "config/" . $name);
	}
}
?>