<?php
class ModelAccountKategoriaJelentes extends Model {

    public function getCategory($data) {

        $is_customer_categories = $this->customerCategories();
        if ($is_customer_categories) {
            $sql = "SELECT cd.name, c.category_id FROM " . DB_PREFIX . "category c
                LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
                 WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                 AND c.date_added <= '" .$data['filter_date_end']. "'";

            $sql .= " AND c.category_id IN (" .$is_customer_categories. ")";
            $sql .= " GROUP BY c.category_id";

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query;
        } else {
            return false;
        }
    }

    public function customerCategories() {
        $cproArry = array();
        $cusProId = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product_customer` WHERE customer_id = " .(int)$this->customer->getId())->rows;
        if($cusProId){
            for($i=0;$i<count($cusProId);$i++){
                $cproArry[] = $cusProId[$i]['product_id'];
            }
            $custProID = implode(',',$cproArry);
        }else{
            $custProID = 0;
        }

        $sql = "SELECT p2c.category_id FROM " . DB_PREFIX . "product p
        INNER JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        $sql .= " WHERE p.product_id IN (".$custProID.")";
        $sql .= " AND  p.date_ervenyes_ig >= curdate()";


        $sql .= " GROUP BY p2c.category_id";
        $query = $this->db->query($sql);
        $vissza = false;

        if ($query->num_rows > 0) {
            $vissza = "";
            foreach($query->rows as $value) {
                $vissza .= $value['category_id'].",";
            }
            $vissza = substr($vissza,0,-1);

        }

        return $vissza;

    }


    public function getCategoryTotals($data) {

        $is_customer_categories = $this->customerCategories();

        $sql = "SELECT c.category_id FROM " . DB_PREFIX . "category c
            LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
             WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
             AND c.date_added <= '" .$data['filter_date_end']. "'";
        $sql .= " AND c.category_id IN (" .$is_customer_categories. ")";

        $sql .= " GROUP BY c.category_id";

        $query = $this->db->query($sql);

        return $query->num_rows;
    }

    public function getCategoryCustomer($category_id, $data) {

        $sql = "SELECT c.customer_id FROM " . DB_PREFIX . "product_to_category p2c

            LEFT JOIN " . DB_PREFIX . "product_customer pc ON (p2c.product_id = pc.product_id)
            LEFT JOIN " . DB_PREFIX . "customer c ON (pc.customer_id = c.customer_id)
            LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)

             WHERE p2c.category_id      = '" . $category_id . "'

             AND  ( (p.date_added       <= '" .$data['filter_date_start']. "' AND p.date_ervenyes_ig	>= '" .$data['filter_date_start']. "')
                    OR (p.date_added    >= '" .$data['filter_date_start']. "' AND p.date_added	<= '" .$data['filter_date_end']. "') )

             AND pc.customer_id IS NOT NULL";
        $sql .= " GROUP BY c.customer_id";

        $query = $this->db->query($sql);

        return $query->num_rows;
    }

    public function getCategoryPurchaseSum($category_id, $data) {
        /*

        1. vásárolt termék              - order_product
        2. vásárlás időpontja           - order
        3. kategoria id megszerzése     - product_to_category
        4. kategoria                    - category
        */

        //COUNT(*) AS total

        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order_product` op

            LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)
            LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (op.product_id = p2c.product_id)
            LEFT JOIN " . DB_PREFIX . "category c ON (c.category_id = p2c.category_id)

             WHERE c.category_id = '" . $category_id . "'
             AND  o.date_modified    >= '" .$data['filter_date_start']. "' AND o.date_modified	<= '" .$data['filter_date_end']. "'";

        $query = $this->db->query($sql);

        $vissza = 0;
        if ($query->num_rows > 0) {
            $vissza = $query->row['total'];
        }

        return $vissza;
    }

    public function getCategoryMyPurchaseSum($category_id, $data) {
        /*

        1. vásárolt termék              - order_product
        2. vásárlás időpontja           - order
        3. kategoria id megszerzése     - product_to_category
        4. kategoria                    - category
        5. nálam vásároltak             - product_customer
        */

        //COUNT(*) AS total

        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order_product` op

            LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)
            LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (op.product_id = p2c.product_id)
            LEFT JOIN " . DB_PREFIX . "category c ON (c.category_id = p2c.category_id)
            RIGHT JOIN " . DB_PREFIX . "product_customer pc ON (op.product_id = pc.product_id AND pc.customer_id = '" . $data['customer_id']. "')

             WHERE c.category_id = '" . $category_id . "'
             AND  o.date_modified    >= '" .$data['filter_date_start']. "' AND o.date_modified	<= '" .$data['filter_date_end']. "'";

        $query = $this->db->query($sql);
        $vissza = 0;
        if ($query->num_rows > 0) {
            $vissza = $query->row['total'];
        }

        return $vissza;
    }

    public function getCategoryAverage($category_id, $data) {
        /*

        1. vásárolt termék              - order_product
        2. vásárlás időpontja           - order
        3. kategoria id megszerzése     - product_to_category
        4. kategoria                    - category
        5. nálam vásároltak             - product_customer
        */

        //COUNT(*) AS total

        $sql = "SELECT p.szazalek, p.price FROM `" . DB_PREFIX . "order_product` op

            LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)
            LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (op.product_id = p2c.product_id)
            LEFT JOIN " . DB_PREFIX . "category c ON (c.category_id = p2c.category_id)
            LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = op.product_id)

             WHERE c.category_id = '" . $category_id . "'
             AND  o.date_modified    >= '" .$data['filter_date_start']. "' AND o.date_modified	<= '" .$data['filter_date_end']. "'";
        $query = $this->db->query($sql);

        $a = 0;
        $b = 0;
        $price_szazalek  = 0;
        $price           = 0;
        if ($query->num_rows > 0) {
            foreach($query->rows as $value) {
                if ($value['szazalek'] == 1) {
                    $price_szazalek += abs($value['price']);
                    $a++;
                } else {
                    $price += abs($value['price']);
                    $b++;
                }
            }
        }

        $a = $a == 0 ? 1 :$a;
        $b = $b == 0 ? 1 :$b;

        $vissza = array(
            'price_szazalek'    => round($price_szazalek / $a),
            'price'             => round($price / $b)
        );

        return $vissza;
    }

    public function getCategoryViewed($category_id, $data) {
        $sql = "SELECT SUM(counter) AS total FROM `" . DB_PREFIX . "category_view`
             WHERE category_id = '".$category_id."'
              AND  datum    >= '" .$data['filter_date_start']. "'
              AND datum	<= '" .$data['filter_date_end']. "'";

        $query = $this->db->query($sql);

        $sum = 0;
        if ($query->num_rows > 0) {
            $sum = $query->row['total'] == null ? 0 : $query->row['total'];

        }
        return $sum;
    }

}
?>