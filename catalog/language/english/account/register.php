<?php
// Heading 
$_['heading_title']        = 'Register Account';

// Text
$_['text_account']         = 'Account';
$_['text_register']        = 'Register';
$_['text_account_already'] = 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']    = 'Your Personal Details';
$_['text_your_address']    = 'Your Address';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a class="colorbox" href="%s" alt="%s"><b>%s</b></a>';
$_['error_fb_email_error']   = 'Please enable to enter the e-mail address to log on facebook or create a new account!';
// Entry
$_['entry_adoszam']        = 'Tax number:';
$_['entry_firstname']      = 'First Name:';
$_['entry_lastname']       = 'Last Name:';
$_['entry_email']          = 'E-Mail:';
$_['entry_email_again']    = 'E-Mail Confirm:';
$_['entry_telephone']      = 'Telephone:';
$_['entry_fax']            = 'Fax:';
$_['entry_company']        = 'Company:';
$_['entry_adoszam']        = 'tax ID:';
$_['entry_address_1']      = 'Address 1:';
$_['entry_address_2']      = 'Address 2:';
$_['entry_postcode']       = 'Post Code:';
$_['entry_city']           = 'City:';
$_['entry_country']        = 'Country:';
$_['entry_zone']           = 'Region / State:';
$_['entry_newsletter']     = 'Subscribe:';
$_['entry_password']       = 'Password:';
$_['entry_confirm']        = 'Password Confirm:';
$_['entry_nem']            = 'Sex:';
$_['entry_eletkor']        = 'Age:';

// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_address_1']      = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_agree']          = 'Warning: You must agree to the %s!';
$_['error_eletkor']   = 'Please select the age of';
$_['error_nem']   = 'Please select the gender of';

$_['text_paypaltitle']         = 'Paypal Account Details';
$_['text_paypallable']         = 'Paypal Account Email Id';
$_['select_ferfi']         = 'Men';
$_['select_no']            = 'woman';

$_['text_your_cegadatok']           = 'Company Informations';
$_['entry_iskolai_vegzettseg']      = 'Educational attainment';
$_['entry_vallalkozasi_forma']      = 'Business form:';
$_['entry_szekhely']                = 'Head office:';
$_['entry_ugyvezeto_neve']          = 'Manager name:';
$_['entry_ugyvezeto_telefonszama']  = 'Manager phone:';

?>