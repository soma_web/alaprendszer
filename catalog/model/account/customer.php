<?php
class ModelAccountCustomer extends Model {
	public function addCustomer($data) {
        $ceges          = isset($data['feltolto']) ? (int)$data['feltolto'] : 0;
        $firstname      = isset($data['firstname']) ? $this->db->escape($data['firstname']) : "";
        $lastname       = isset($data['lastname']) ? $this->db->escape($data['lastname']) : "";
        $email          = isset($data['email']) ? $this->db->escape($data['email']) : "";
        $telephone      = isset($data['telephone']) ? $this->db->escape($data['telephone']) : "";
        $fax            = isset($data['fax']) ? $this->db->escape($data['fax']) : "";
        $origPassword   = isset($data['password']) ? $data['password'] : "";
        $password       = isset($data['password']) ? $this->db->escape(md5($data['password'])) : "";
        $newsletter     = isset($data['newsletter']) ? (int)$data['newsletter'] : 0;
        $adoszam        = isset($data['adoszam']) ? $this->db->escape($data['adoszam']) : "";
        $company        = isset($data['company']) ? $this->db->escape($data['company']) : "";

        $nem            = isset($data['nem']) ? $data['nem'] : '';
        $eletkor        = isset($data['eletkor']) ? $data['eletkor'] : '';
        $paypal         = isset($data['paypalemail']) ? $this->db->escape($data['paypalemail']) : "";
        $iskolai_vegzettseg = isset($data['iskolai_vegzettseg']) ? $this->db->escape($data['iskolai_vegzettseg']) : '';

        $vallalkozasi_forma = isset($data['vallalkozasi_forma']) ? $this->db->escape($data['vallalkozasi_forma']) : '';
        $szekhely       = isset($data['szekhely']) ? $this->db->escape($data['szekhely']) : '';
        $ugyvezeto_neve = isset($data['ugyvezeto_neve']) ? $this->db->escape($data['ugyvezeto_neve']) : '';
        $ugyvezeto_telefonszama = isset($data['ugyvezeto_telefonszama']) ? $this->db->escape($data['ugyvezeto_telefonszama']) : '';
        $weblap         = isset($data['weblap']) ? $this->db->escape($data['weblap']) : '';

      	$sql = "INSERT INTO " . DB_PREFIX . "customer SET store_id = '" . (int)$this->config->get('config_store_id') . "',
      	firstname               = '" . $firstname . "',
      	lastname                = '" . $lastname . "',
      	email                   = '" . $email . "',
      	telephone               = '" . $telephone . "',
      	fax                     = '" . $fax . "',
      	password                = '" . $password . "',
      	newsletter              = '" . $newsletter . "',
      	customer_group_id       = '" . (int)$this->config->get('config_customer_group_id') . "',
      	status                  = '1',
      	adoszam                 =  '" . $adoszam . "',
      	feltolto                =  '" . $ceges . "',
      	nem                     =  '" . $nem . "',
      	eletkor                 =  '" . $eletkor . "',
      	iskolai_vegzettseg      =  '" . $iskolai_vegzettseg. "',
      	vallalkozasi_forma      =  '" . $vallalkozasi_forma. "',
      	szekhely                =  '" . $szekhely. "',
      	ugyvezeto_neve          =  '" . $ugyvezeto_neve. "',
      	ugyvezeto_telefonszama  =  '" . $ugyvezeto_telefonszama. "',
      	company                 =  '" . $company. "',
      	Paypal_email            =  '" . $paypal. "',
      	weblap                  =  '" . $weblap. "',
      	date_added = NOW()";
        $siker = $this->db->query($sql);
      	
		$customer_id = $this->db->getLastId();

        $sql = "UPDATE " . DB_PREFIX . "customer SET
            vevokod = 'W" .$customer_id. "'
		    WHERE customer_id = '" . (int)$customer_id . "'";
        $siker = $this->db->query($sql);


        //$this->addSap($data,$customer_id);

        $address_1      = isset($data['address_1']) ? $this->db->escape($data['address_1']) : '';
        $address_2      = isset($data['address_2']) ? $this->db->escape($data['address_2']) : '';
        $city           = isset($data['city']) ? $this->db->escape($data['city']) : '';
        $postcode       = isset($data['postcode']) ? $this->db->escape($data['postcode']) : '';
        $country_id     = isset($data['country_id']) ? (int)$data['country_id'] : '';
        $zone_id        = isset($data['zone_id']) ? (int)$data['zone_id'] : '';
        $company_cim    = isset($data['company_cim']) ? $this->db->escape($data['company_cim']) : "";

      	$sql = "INSERT INTO " . DB_PREFIX . "address SET
      	        customer_id                 = '" . (int)$customer_id . "',
      	        firstname                   = '" . $firstname . "',
      	        lastname                    = '" . $lastname . "',
      	        company                     = '" . $company_cim . "',
      	        adoszam                     = '" . $adoszam . "',
      	        vallalkozasi_forma          = '" . $vallalkozasi_forma. "',
      	        szekhely                    = '" . $szekhely. "',
      	        ugyvezeto_neve              = '" . $ugyvezeto_neve. "',
      	        ugyvezeto_telefonszama      = '" . $ugyvezeto_telefonszama. "',
      	        address_1                   = '" . $address_1 . "',
      	        address_2                   = '" . $address_2 . "',
      	        city                        = '" . $city . "',
      	        postcode                    = '" . $postcode . "',
      	        country_id                  = '" . $country_id . "',
      	        zone_id                     = '" . $zone_id . "'";

        $siker = $this->db->query($sql);

        $address_id = $this->db->getLastId();

      	$sql = "UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'";
        $siker = $this->db->query($sql);

        if (!$this->config->get('config_customer_approval')) {
			$sql = "UPDATE " . DB_PREFIX . "customer SET approved = '1' WHERE customer_id = '" . (int)$customer_id . "'";
            $siker = $this->db->query($sql);

        }

        if(isset($data['newsletter']) && $data['newsletter'] == '1' && isset($data['newslettercategories']) && is_array($data['newslettercategories'])) {
            $this->load->model('account/newslettercategories');
            $this->model_account_newslettercategories->setCategoriesToCustomer($customer_id, $data['newslettercategories']);
        }

		$this->language->load('mail/customer');
		
		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

        # Email header
		$message = sprintf($this->language->get('text_user'), $data['firstname']." ".$data['lastname']) . "\n\n";
		$message .= sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";
		if (!$this->config->get('config_customer_approval')) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}
        $message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
        $message .= sprintf($this->language->get('text_adatai')) . "\n";

        # Email body (fields)
        $regemail = $this->config->get('megjelenit_regisztracios_email');
        if($ceges == 1) {
            ### Személyes adatok ###
            # Vezetéknév
            if($this->config->get('megjelenit_regisztracio_ceges_vezeteknev')) {
                $message .= sprintf($this->language->get('text_data_vezeteknev'), $firstname) . "\n";
            }
            # Keresztnév
            if($this->config->get('megjelenit_regisztracio_ceges_keresztnev')) {
                $message .= sprintf($this->language->get('text_data_keresztnev'), $lastname) . "\n";
            }
            # Email
            if($this->config->get('megjelenit_regisztracio_ceges_email')) {
                $message .= sprintf($this->language->get('text_data_email'), $email) . "\n";
            }
            # Telefon
            if($this->config->get('megjelenit_regisztracio_ceges_telefon')) {
                $message .= sprintf($this->language->get('text_data_telefon'), $telephone) . "\n";
            }
            # Fax
            if($this->config->get('megjelenit_regisztracio_ceges_fax')) {
                $message .= sprintf($this->language->get('text_data_fax'), $fax) . "\n";
            }
            # Cégnév
            if($this->config->get('megjelenit_regisztracio_ceges_cegnev')) {
                $message .= sprintf($this->language->get('text_data_ceg'), $company) . "\n";
            }
            # Adószám
            if($this->config->get('megjelenit_regisztracio_ceges_adoszam')) {
                $message .= sprintf($this->language->get('text_data_ado'), $adoszam) . "\n";
            }

            ### Címadatok ###
            # Utca, házszám
            if($this->config->get('megjelenit_regisztracio_ceges_cimadat_utca')) {
                $message .= sprintf($this->language->get('text_data_utca'), $address_1) . "\n";
            }
            # Kiegészítő címadatok
            if($this->config->get('megjelenit_regisztracio_ceges_cimadat_kiegeszito')) {
                $message .= sprintf($this->language->get('text_data_cimadat_kiegeszito'), $address_2) . "\n";
            }
            # Város
            if($this->config->get('megjelenit_regisztracio_ceges_cimadat_varos')) {
                $message .= sprintf($this->language->get('text_data_varos'), $city) . "\n";
            }
            # Irányítószám
            if($this->config->get('megjelenit_regisztracio_ceges_cimadat_iranyitoszam')) {
                $message .= sprintf($this->language->get('text_data_iranyito'), $postcode) . "\n";
            }
            # Ország
            if($this->config->get('megjelenit_regisztracio_ceges_cimadat_orszag')) {
                $this->load->model('localisation/country');
                $result = $this->model_localisation_country->getCountry($country_id);
                $message .= sprintf($this->language->get('text_data_orszag'), $result['name']) . "\n";
            }
            # Állam/Megye
            if($this->config->get('megjelenit_regisztracio_ceges_cimadat_megye')) {
                $this->load->model('localisation/zone');
                $result = $this->model_localisation_zone->getZone($zone_id);
                $message .= sprintf($this->language->get('text_data_megye'), $result['name']) . "\n";
            }

            ### Jelszó ###
            # Password
            $needPassword = true;
            if(is_array($regemail) && array_key_exists('ceges_jelszo', $regemail) && $regemail['ceges_jelszo'] == 1) {
                $needPassword = true;
            } elseif(is_array($regemail) && array_key_exists('ceges_jelszo', $regemail) && $regemail['ceges_jelszo'] == 0) {
                $needPassword = false;
            }
            if($needPassword) {
                $message .= sprintf($this->language->get('text_data_jelszo'), $origPassword) . "\n";
            }

            # Hírlevél
            if($this->config->get('megjelenit_regisztracioblokk_ceges_hirlevel')) {
                if($newsletter) {
                    $message .= sprintf($this->language->get('text_data_hirlevel'), $this->language->get('text_data_igen')) . "\n";
                } else {
                    $message .= sprintf($this->language->get('text_data_hirlevel'), $this->language->get('text_data_nem')) . "\n";
                }

            }
        } else {
            ### Személyes adatok ###
            # Vezetéknév
            if($this->config->get('megjelenit_regisztracio_vezeteknev')) {
                $message .= sprintf($this->language->get('text_data_vezeteknev'), $firstname) . "\n";
            }
            # Keresztnév
            if($this->config->get('megjelenit_regisztracio_keresztnev')) {
                $message .= sprintf($this->language->get('text_data_keresztnev'), $lastname) . "\n";
            }
            # Email
            if($this->config->get('megjelenit_regisztracio_email')) {
                $message .= sprintf($this->language->get('text_data_email'), $email) . "\n";
            }
            # Telefon
            if($this->config->get('megjelenit_regisztracio_telefon')) {
                $message .= sprintf($this->language->get('text_data_telefon'), $telephone) . "\n";
            }
            # Fax
            if($this->config->get('megjelenit_regisztracio_fax')) {
                $message .= sprintf($this->language->get('text_data_fax'), $fax) . "\n";
            }
            # Cégnév
            if($this->config->get('megjelenit_regisztracio_cegnev')) {
                $message .= sprintf($this->language->get('text_data_ceg'), $company) . "\n";
            }
            # Adószám
            if($this->config->get('megjelenit_regisztracio_adoszam')) {
                $message .= sprintf($this->language->get('text_data_ado'), $adoszam) . "\n";
            }

            ### Címadatok ###
            # Utca, házszám
            if($this->config->get('megjelenit_regisztracio_utca')) {
                $message .= sprintf($this->language->get('text_data_utca'), $address_1) . "\n";
            }
            # Kiegészítő címadatok
            if($this->config->get('megjelenit_regisztracio_cimadat_kiegeszito')) {
                $message .= sprintf($this->language->get('text_data_cimadat_kiegeszito'), $address_2) . "\n";
            }
            # Város
            if($this->config->get('megjelenit_regisztracio_varos')) {
                $message .= sprintf($this->language->get('text_data_varos'), $city) . "\n";
            }
            # Irányítószám
            if($this->config->get('megjelenit_regisztracio_iranyitoszam')) {
                $message .= sprintf($this->language->get('text_data_iranyito'), $postcode) . "\n";
            }
            # Ország
            if($this->config->get('megjelenit_regisztracio_orszag')) {
                $this->load->model('localisation/country');
                $result = $this->model_localisation_country->getCountry($country_id);
                if(isset($result['name'])) {
                    $message .= sprintf($this->language->get('text_data_orszag'), $result['name']) . "\n";
                }
            }
            # Állam/Megye
            if($this->config->get('megjelenit_regisztracio_megye')) {
                $this->load->model('localisation/zone');
                $result = $this->model_localisation_zone->getZone($zone_id);
                if(isset($result['name'])) {
                    $message .= sprintf($this->language->get('text_data_megye'), $result['name']) . "\n";
                }
            }

            ### Jelszó ###
            # Password
            $needPassword = true;
            if(is_array($regemail) && array_key_exists('jelszo', $regemail) && $regemail['jelszo'] == 1) {
                $needPassword = true;
            } elseif(is_array($regemail) && array_key_exists('jelszo', $regemail) && $regemail['jelszo'] == 0) {
                $needPassword = false;
            }
            if($needPassword) {
                $message .= sprintf($this->language->get('text_data_jelszo'), $origPassword) . "\n";
            }

            # Hírlevél
            if($this->config->get('megjelenit_regisztracioblokk_hirlevel')) {
                if($newsletter) {
                    $message .= sprintf($this->language->get('text_data_hirlevel'), $this->language->get('text_data_igen')) . "\n";
                } else {
                    $message .= sprintf($this->language->get('text_data_hirlevel'), $this->language->get('text_data_nem')) . "\n";
                }
            }
        }

        # Email footer
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= $this->config->get('config_name');
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
        $mail->setSubject($subject);

        $mail->setText($message);
		//$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();
		
		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$mail->setTo($this->config->get('config_email'));
			$mail->send();
			
			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_emails'));
			
			foreach ($emails as $email) {
				if (strlen($email) > 0 && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
	}
	
	public function addSap($data,$customer_id) {
       /* $letrehoz="CREATE TABLE if not exists sapnak_vevok
       ( `vevokod`  text default null,
        `vevo_neve`  text default null,
        `vevo_idegen_neve`  text default null,
        `vevo_sap_csoportkod`  text default null,
        `fizetesi_mod`  text default null,
        `telefonszam`  text default null,
        `mail`  text default null,
        `adoszam`  text default null,
        `szall_iranyitoszam`  text default null,
        `szall_varos`  text default null,
        `szall_utca`  text default null,
        `szall_orszagkod`  text default null,
        `szaml_iranyitoszam`  text default null,
        `szaml_varos`  text default null,
        `szaml_utca`  text default null,
        `szaml_orszagkod`  text default null,
        `vevo_csoport`  text default null)   engine=MyISAM default charset=UTF8";
        $this->db->query($letrehoz);

        /* "', password = '" . $this->db->escape(md5($data['password'])) . "',
        newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "',
        customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "',
        status = '1', date_added = NOW()*/

     /*   $szallitasi_orszag_kod      = $data['country_id'];
        $query_country=$this->db->query( "SELECT * FROM `".DB_PREFIX."country` WHERE `country_id` = ".(int)$szallitasi_orszag_kod);

        $this->db->query("INSERT INTO sapnak_vevok SET

                        `vevokod`               =
                        `vevo_neve`             =   '".$data['firstname']." ".$data['lastname']."',
                        `vevo_idegen_neve`      =
                        `vevo_sap_csoportkod`   =
                        `fizetesi_mod`          =
                        `telefonszam`           =   '".$data['telephone'].",
                        `mail`                  =   '".$data['email'].",
                        `adoszam`               =
                        `szall_iranyitoszam`    =
                        `szall_varos`           =
                        `szall_utca`            =   '".$data['email'].",
                        `szall_orszagkod`       =   '".$query_country->row['iso_code_2']."',
                        `szaml_iranyitoszam`    =
                        `szaml_varos`           =
                        `szaml_utca`            =
                        `szaml_orszagkod`       =
                        `vevo_csoport`          =*/






    }

	public function editCustomer($data) {
        $firstname = isset($data['firstname']) ? $this->db->escape($data['firstname']) : '';
        $lastname = isset($data['lastname']) ? $this->db->escape($data['lastname']) : '';
        $email = isset($data['email']) ? $this->db->escape($data['email']) : '';
        $telephone = isset($data['telephone']) ? $this->db->escape($data['telephone']) : '';
        $fax = isset($data['fax']) ? $this->db->escape($data['fax']) : '';
        $nem = isset($data['nem']) ? $data['nem'] : '';
        $company = isset($data['company']) ? $data['company'] : '';
        $eletkor = isset($data['eletkor']) ? $data['eletkor'] : '';
        $adoszam = isset($data['adoszam']) ? $this->db->escape($data['adoszam']) : '';
        $Paypal_email = isset($data['paypalemail']) ? $this->db->escape($data['paypalemail']) : '';
        $iskolai_vegzettseg = isset($data['iskolai_vegzettseg']) ? $this->db->escape($data['iskolai_vegzettseg']) : '';

        $vallalkozasi_forma = isset($data['vallalkozasi_forma']) ? $this->db->escape($data['vallalkozasi_forma']) : '';
        $szekhely = isset($data['szekhely']) ? $this->db->escape($data['szekhely']) : '';
        $ugyvezeto_neve = isset($data['ugyvezeto_neve']) ? $this->db->escape($data['ugyvezeto_neve']) : '';
        $ugyvezeto_telefonszama = isset($data['ugyvezeto_telefonszama']) ? $this->db->escape($data['ugyvezeto_telefonszama']) : '';
        $weblap = isset($data['weblap']) ? $this->db->escape($data['weblap']) : '';

        $this->db->query("UPDATE " . DB_PREFIX . "customer SET
		 firstname              = '" . $firstname . "',
		 lastname               = '" . $lastname . "',
		 email                  = '" . $email . "',
		 telephone              = '" . $telephone . "',
		 fax                    = '" . $fax . "',
		 nem                    = '" . $nem . "',
      	 eletkor                = '" . $eletkor . "',
      	 company                = '" . $company . "',
      	 Paypal_email           = '" . $Paypal_email . "',
		 adoszam                = '" . $adoszam . "',
		 iskolai_vegzettseg     = '".$iskolai_vegzettseg."',
		 vallalkozasi_forma     = '".$vallalkozasi_forma."',
		 szekhely               = '".$szekhely."',
		 ugyvezeto_neve         = '".$ugyvezeto_neve."',
      	 weblap                 =  '" . $weblap. "',
		 ugyvezeto_telefonszama = '".$ugyvezeto_telefonszama."'
		 WHERE customer_id      = '" . (int)$this->customer->getId() . "'");
	}

	public function editPassword($email, $password) {
      	$this->db->query("UPDATE " . DB_PREFIX . "customer SET password = '" . $this->db->escape(md5($password)) . "' WHERE email = '" . $this->db->escape($email) . "'");
	}

	public function editNewsletter($newsletter, $newslettercategories) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");

        if(isset($newslettercategories) && is_array($newslettercategories)) {
            $this->load->model('account/newslettercategories');
            $this->model_account_newslettercategories->setCategoriesToCustomer((int)$this->customer->getId(), $newslettercategories);
        }
	}
					
	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		
		return $query->row;
	}

	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");
		
		return $query->row;
	}
		
	public function getCustomers($data = array()) {
		$sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cg.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group cg ON (c.customer_group_id = cg.customer_group_id) ";

		$implode = array();
		
		if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
			$implode[] = "LCASE(CONCAT(c.firstname, ' ', c.lastname)) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}
		
		if (isset($data['filter_email']) && !is_null($data['filter_email'])) {
			$implode[] = "c.email = '" . $this->db->escape($data['filter_email']) . "'";
		}
		
		if (isset($data['filter_customer_group_id']) && !is_null($data['filter_customer_group_id'])) {
			$implode[] = "cg.customer_group_id = '" . $this->db->escape($data['filter_customer_group_id']) . "'";
		}	
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
		}	
		
		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$implode[] = "c.approved = '" . (int)$data['filter_approved'] . "'";
		}	
			
		if (isset($data['filter_ip']) && !is_null($data['filter_ip'])) {
			$implode[] = "c.customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
		}	
				
		if (isset($data['filter_date_added']) && !is_null($data['filter_date_added'])) {
			$implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
		
		$sort_data = array(
			'name',
			'c.email',
			'customer_group',
			'c.status',
			'c.ip',
			'c.date_added'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);
		
		return $query->rows;	
	}
		
	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE email = '" . $this->db->escape($email) . "'");
		
		return $query->row['total'];
	}
	
	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");
		
		return $query->rows;
	}	
	
	public function isBlacklisted($ip) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip_blacklist` WHERE ip = '" . $this->db->escape($ip) . "'");
		
		return $query->num_rows;
	}	
}
?>