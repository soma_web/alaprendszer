<?php
class ModelShippingWeight extends Model {
    function getQuote($address=0, $fizetesek="") {
		$this->load->language('shipping/weight');
		
		$quote_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");
	
		foreach ($query->rows as $result) {
			if ($this->config->get('weight_' . $result['geo_zone_id'] . '_status')) {
				$sql = "SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "'
				AND country_id = '" . (int)$address['country_id'] . "'
				AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')";

                $query = $this->db->query($sql);
                if ($query->num_rows) {
					$status = true;
				} else {
					$status = false;
				}
			} else {
				$status = false;
			}
		
			if ($status) {

                $fizetesi_mod = array();

                if ($fizetesek){
                    foreach($fizetesek as $value) {
                        if ( $this->config->get("weight_payment_".$value['code']) &&  $this->config->get("weight_payment_".$value['code']) == 1){
                            $fizetesi_mod[] = array(
                                'fizetesi_mod' => $value['code']
                            );
                        }
                    }
                }
				$cost = '';
				$weight = $this->cart->getWeight();
				
				$rates = explode(',', $this->config->get('weight_' . $result['geo_zone_id'] . '_rate'));
				
				foreach ($rates as $rate) {
					$data = explode(':', $rate);
				
					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$cost = $data[1];
						}
				
						break;
					}
				}
				
				if ((string)$cost != '') {
                    if ($this->config->get('weight_tax_class_id') > 0) {
                        $query_tax = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$this->config->get('weight_tax_class_id') . "'");
                    }
                    $ado_title = isset($query_tax->row['title']) ? $query_tax->row['title'] :"";

					$quote_data['weight_' . $result['geo_zone_id']] = array(
						'code'         => 'weight.weight_' . $result['geo_zone_id'],
                        'fizetesi_mod'  => $fizetesi_mod,
						'title'        => $this->config->get('weight_text') != null ? $this->config->get('weight_text') :  $this->language->get('text_description'), //$this->language->get('text_weight_szoveg'), // . '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')',
						'cost'         => $cost,
						'tax_class_id' => $this->config->get('weight_tax_class_id'),
						'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('weight_tax_class_id'), $this->config->get('config_tax'))),
                        'netto_text'   => $this->currency->format($cost),
                        'ado_title'    => $ado_title,
                        'biztositas'   => $this->config->get('weight_biztositas') != null ? $this->config->get('weight_biztositas') : 0,
						'mail'         => $this->config->get('weight_mail')

					);
				}
			}
		}
		
		$method_data = array();
	
		if ($quote_data) {
      		$method_data = array(
        		'code'       => 'weight',
        		'title'      => $this->config->get('weight_header') != null ? $this->config->get('weight_header'). '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')' : $this->language->get('text_description'). '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')',
                'fizetesi_mod'  => $fizetesi_mod,
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get('weight_sort_order'),
        		'error'      => false
      		);
		}
	
		return $method_data;
  	}
}
?>