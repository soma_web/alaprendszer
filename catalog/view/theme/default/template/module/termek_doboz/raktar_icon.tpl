<?php if (isset($product['raktaron'])) {

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/raktaron.png')) {
        $raktaron = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/raktaron.png';
    } else {
        $raktaron = DIR_TEMPLATE_IMAGE.'default/image/raktaron.png';
    }

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/nincs_raktaron.png')) {
        $nincs_raktaron = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/nincs_raktaron.png';
    } else {
        $nincs_raktaron = DIR_TEMPLATE_IMAGE.'default/image/nincs_raktaron.png';
    }

?>


    <?php if($product['raktaron']) { ?>
        <img class="icon_raktaron" title="<? if (isset($text_raktaron)) { echo $text_raktaron; }?>"  src="<? echo $raktaron?>" >
    <?php  } else { ?>
        <img class="icon_raktaron" title="<? if (isset($text_nincs_raktaron)) { echo $text_nincs_raktaron; }?>"  src="<? echo $nincs_raktaron?>" >
    <?php } ?>

<?php } ?>

