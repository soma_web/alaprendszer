<?php echo $header; ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
    <h1 style="display: none"><?php echo $heading_title; ?></h1>
    <div class="content">
        <div class="felso-sor-fejlec"><b><?php echo $text_critea; ?></b></div>
            <div class="felso-sor">
                <div class="felso-sor-cim"><?php echo $entry_search_name; ?></div>

                <div style="display: inline-table; position: relative">
                    <?php if ($filter_name && $filter_name != $this->language->get('text_search')) { ?>
                        <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
                    <?php } else { ?>
                        <input type="text" name="filter_name"/>
                    <!--<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" style="color: #999;" />-->
                    <?php } ?>
                    <div style="display: none" class="search-checkbox">
                        <?php if ($filter_description) { ?>
                            <input type="checkbox" name="filter_description" value="1" id="description" checked="checked" />
                        <?php } else { ?>
                            <input type="checkbox" name="filter_description" value="1" id="description" />
                        <?php } ?>
                        <label for="description"><?php echo $entry_description; ?></label>
                    </div>
                </div>

            </div>

            <?php if ($this->config->get("megjelenit_admin_keres_model") == 1 && false) {?>
            <div  class="felso-sor"><div class="felso-sor-cim"><?php echo $entry_search_model; ?></div>
            <div style="height: 15px; vertical-align: middle;display: inline-table">
                <?php if ($filter_model) { ?>
                    <input type="text" name="filter_model" value="<?php echo str_replace("%"," ",$filter_model); ?>" />
                <?php } else { ?>
                    <input type="text" name="filter_model" value="" />
                <?php } ?>
            </div>
            </div>
            <?php } ?>

            <div  class="felso-sor-le">
                <div>
                    <select style="height: 29px;" name="filter_category_id">
                        <option value="0"><?php echo $text_category; ?></option>
                        <?php foreach ($categories as $category_1) { ?>
                            <?php if ($category_1['category_id'] == $filter_category_id) { ?>
                                <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
                            <?php } ?>

                            <?php foreach ($category_1['children'] as $category_2) { ?>
                                <?php if ($category_2['category_id'] == $filter_category_id) { ?>
                                    <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                                <?php } ?>

                                <?php foreach ($category_2['children'] as $category_3) { ?>
                                    <?php if ($category_3['category_id'] == $filter_category_id) { ?>
                                        <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>

                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="search-checkbox">
                    <?php if ($filter_sub_category) { ?>
                        <input type="checkbox" name="filter_sub_category" value="1" id="sub_category" checked="checked" />
                    <?php } else { ?>
                        <input type="checkbox" name="filter_sub_category" value="1" id="sub_category" />
                    <?php } ?>
                    <label for="sub_category"><?php echo $text_sub_category; ?></label>
                </div>
            </div>

            <?php if ($this->config->get("megjelenit_admin_keres_cikkszam") == 1) {?>
            <div  class="felso-sor">
                <div>
                    <?php echo $entry_search_cikkszam; ?>
                </div>
                <div>
                    <?php if ($filter_cikkszam) { ?>
                        <input type="text" name="filter_cikkszam" value="<?php echo str_replace("%"," ",$filter_cikkszam); ?>" />
                    <?php } else { ?>
                        <input type="text" name="filter_cikkszam" value="" />
                    <?php } ?>
                </div>
            </div>
            <?php } ?>

            <?php if ($this->config->get("megjelenit_admin_keres_cikkszam2") == 1) {?>
            <div  class="felso-sor">
                <div><?php echo $entry_search_cikkszam2; ?></div>
                <div>
                    <?php if ($filter_cikkszam2) { ?>
                        <input type="text" name="filter_cikkszam2" value="<?php echo str_replace("%"," ",$filter_cikkszam2); ?>" />
                    <?php } else { ?>
                        <input type="text" name="filter_cikkszam2" value="" />
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
            <div  class="felso-sor-gomb">
                <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="button" />
                <input type="hidden" name="keres_autocomplate" value="<?php echo $this->config->get('megjelenit_keres_autocomplete')?>">

            </div>

    </div>



  <h2><?php echo $text_search; ?></h2>
  <?php if ($products) { ?>
      <?php if ($this->config->get('megjelenit_felul_lapszamozas') == 1) {?>
        <div class="product-filter">
        <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
        <div class="limit"><?php echo $text_limit; ?>
          <select onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="sort"><?php echo $text_sort; ?>
          <select onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      </div>


      <div class="pagination"><?php echo $pagination; ?></div>
      <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>

    <?php } ?>

    <div class="box-product">
        <?php $bal = true; ?>

        <?php  foreach ($products as $product) { ?>

            <?php if ($product['utalvany'] != 0) { ?>
                <?php $class="utalvany";?>
            <?php } else {?>
                <?php $class="";?>
            <?php } ?>

            <div style="position: relative;" class="product-search <?php echo $class?>" >
                <?php if (isset($product['kiemelt_tpl']) && $product['kiemelt_tpl']) { ?>
                    <?php include($product['kiemelt_tpl']); ?>
                <?php } else {?>
                    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                    } else {
                        $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                    } ?>
                    <?php include($arkiir); ?>
                <?php } ?>

            </div>
        <?php  } ?>

    </div>


    <div class="product-list">
        <?php foreach ($products as $product) { ?>
            <div class="lista-elem">
                <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/listanezet.tpl')) {
                    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/listanezet.tpl';
                } else {
                    $arkiir = DIR_TEMPLATE.'default/template/module/listanezet.tpl';
                } ?>
                <?php include($arkiir); ?>
            </div>

        <?php } ?>
    </div>





  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php }?>
</div>

  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('#content input[name=\'filter_name\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('#button-search').bind('click', function() {

    url = 'index.php?route=product/search';
	var filter_name = $('#content input[name=\'filter_name\']').attr('value');
	var filter_model = $('#content input[name=\'filter_model\']').attr('value');
	var filter_cikkszam = $('#content input[name=\'filter_cikkszam\']').attr('value');
	var filter_cikkszam2 = $('#content input[name=\'filter_cikkszam2\']').attr('value');
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

    if (filter_model) {
        url += '&filter_model=' + encodeURIComponent(filter_model);
    }
    if (filter_cikkszam) {
        url += '&filter_cikkszam=' + encodeURIComponent(filter_cikkszam);
    }
    if (filter_cikkszam2) {
        url += '&filter_cikkszam2=' + encodeURIComponent(filter_cikkszam2);
    }

	var filter_category_id = $('#content select[name=\'filter_category_id\']').attr('value');
	
	if (filter_category_id > 0) {
		url += '&filter_category_id=' + encodeURIComponent(filter_category_id);
	}
	
	var filter_sub_category = $('#content input[name=\'filter_sub_category\']:checked').attr('value');
	
	if (filter_sub_category) {
		url += '&filter_sub_category=true';
	}
		
	var filter_description = $('#content input[name=\'filter_description\']:checked').attr('value');
	
	if (filter_description) {
		url += '&filter_description=true';
	}

	location = url;
});


  /*  $('.cart .search_button_cart').bind('click', function () {

        var str=$(this).attr('id');
        var osztva=str.split("_");
        var pid=osztva[1];
        var dbstr="search-quantity_"+pid;
        var db=document.getElementById(dbstr).value;

        var csomagolas="#search_csomagolas_"+pid;
        var csomagolasi_mennyiseg=$(csomagolas).val();
        var data0={product_id:pid, quantity:db};
        if (typeof(csomagolasi_mennyiseg) != "undefined" ){
            if (csomagolasi_mennyiseg > 1){
                var mennyiseg=db*csomagolasi_mennyiseg;
                var data0={product_id:pid, quantity:db, quantity2:mennyiseg};
            }
        }

        $.ajax({
            url:'index.php?route=checkout/cart/add',
            type:'post',
            data:data0,
            dataType:'json',
            success:function (json) {
                $('.success, .warning, .attention, .error').remove();

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
                        }
                    }
                }

                if (json['success']) {
                    //$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img style="" src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('#notification').after('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                    $('.success').fadeIn('slow');

                    $('#cart-total').html(json['total']);

                    $('html, body').animate({ scrollTop:0 }, 'slow');

                    $("#slidecontent").load("index.php #slidecontent");
                }
            }
        });
    });*/

   /* $('.search_list_button_cart').bind('click', function () {
        var str=$(this).attr('id');
        var osztva=str.split("_");
        var pid=osztva[1];
        var dbstr="search-list-quantity_"+pid;
        var db=document.getElementById(dbstr).value;

        var csomagolas="#search_list_csomagolas_"+pid;
        var csomagolasi_mennyiseg=$(csomagolas).val();
        var data0={product_id:pid, quantity:db};
        if (typeof(csomagolasi_mennyiseg) != "undefined" ){
            if (csomagolasi_mennyiseg > 1){
                var mennyiseg=db*csomagolasi_mennyiseg;
                var data0={product_id:pid, quantity:db, quantity2:mennyiseg};
            }
        }
        $.ajax({
            url:'index.php?route=checkout/cart/add',
            type:'post',
            data:data0,
            dataType:'json',
            success:function (json) {
                $('.success, .warning, .attention, .error').remove();

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
                        }
                    }
                }

                if (json['success']) {
                    $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                    $('.success').fadeIn('slow');

                    $('#cart-total').html(json['total']);

                    $('html, body').animate({ scrollTop:0 }, 'slow');

                    $("#slidecontent").load("index.php #slidecontent");
                }
            }
        });
    });*/

//--></script>
<?php echo $footer; ?>