<?php
$_['boughts']          = array(
    'type'              => 'checkbox',
    'name'              => 'boughts',
    'default'           => "1",
    'heading'           => 'Szintén vásárolt'
);

$_['kapcsolodo']          = array(
    'type'              => 'checkbox',
    'name'              => 'kapcsolodo',
    'default'           => "1",
    'heading'           => 'Kapcsolódó'
);

$_['korhinta']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta',
    'default'           => "1",
    'heading'           => 'Körhinta'
);

$_['korhinta_darab']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_darab',
    'size'              => "2",
    'default'           => "3",
    'heading'           => "Mejelenít"
);

$_['korhinta_width']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_width',
    'size'              => "2",
    'default'           => "260",
    'heading'           => "Termék szélesség"
);

$_['korhinta_margin']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_margin',
    'size'              => "2",
    'default'           => "20",
    'heading'           => "Dobozok közötti távolság"
);

$_['korhinta_random']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta_random',
    'default'           => "1",
    'heading'           => 'Véletlenszerű'
);

?>