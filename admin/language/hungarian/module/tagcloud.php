<?php

// Heading
$_['heading_title']    			= 'Cimke Felhő';

// Text
$_['text_module']      			= 'Modulok';
$_['text_success']     			= 'Siker: sikeresen módosította a Cimke Felhő modult!';
$_['text_content_top']    	    = 'Tartalom teteje';
$_['text_content_bottom'] 	    = 'Tartalom alja';
$_['text_column_left']    		= 'Bal oszlop';
$_['text_column_right']   	    = 'Jobb oszlop';
$_['text_normal']       		= 'Normál';
$_['text_bold']       			= 'Félkövér';
$_['text_limit']       			= 'Cimkék száma:';
$_['text_module_settings']      = 'A fenti beaállítások érvényesek az összes modul elhelyezésre.';

// Entry
$_['entry_title']      			= 'Egyéni fejléc:<br /><span class="help">If empty and "Show Header" is set to yes, variable from the language will be shown.</span>';
$_['entry_header'] 			    = 'Fejléc mutatása:<br /><span class="help">Doboz mutatásának bekapcsolva kell lennie hozzá.</span>';
$_['entry_icon']   				= 'Ikon mutatása:<br /><span class="help">Doboz mutatásának bekapcsolva kell lennie hozzá.</span>';
$_['entry_box']   				= 'Doboz mutatása:';
$_['entry_yes']	       			= 'Igen';
$_['entry_no']	       			= 'Nem';

$_['entry_template']			= 'Active Template:';

$_['entry_layout']        		= 'Elrendezés:';
$_['entry_limit']				= 'Korlát:';
$_['entry_min_font_size']	    = 'Min Méret:';
$_['entry_max_font_size']	    = 'Max Méret:';
$_['entry_font_weight'] 		= 'Betű Stílus:';
$_['entry_position']      		= 'Pozíció:';
$_['entry_status']        		= 'Státusz:';
$_['entry_sort_order']    	    = 'Sorrend:';

// Error
$_['error_permission']		= 'Hiba: Nincs jogosultsága módosítani a Cimke Felhő modult!';
?>