<?php

// Heading
$_['heading_title']      = 'Paymate';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Siker: A PayMate fiók részleteinek módosítása megtörtént!';

// Entry
$_['entry_username']     = 'Paymate felhasználónév:';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';

// Error
$_['error_permission']   = 'Figyelmeztetés: Az Paymate történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_username']     = 'Felhasználónév szükséges!';
?>
