<?php
class ModelModuleArajanlat extends Model {


    public function add($product_id, $qty = 1, $option = array()) {
        if (!$option) {
            $key = (int)$product_id;
        } else {
            $key = (int)$product_id . ':' . base64_encode(serialize($option));
        }

        if ((int)$qty && ((int)$qty > 0)) {
            if (!isset($this->session->data['arajanlat'][$key])) {
                $this->session->data['arajanlat'][$key] = (int)$qty;
            } else {
                $valasztekok = explode(':',$key);
                if (isset($valasztekok[1])) {
                    $session = unserialize(base64_decode($valasztekok[1]));

                    foreach($session as $session_key=>$option) {
                        if (is_array($option)) {
                            foreach($option as $option_key=>$ertek) {
                                $mennyiseg = explode('|',$ertek);
                                if (isset($mennyiseg[1])) {
                                    $session[$session_key][$option_key] = $mennyiseg[0].'|'.((int)$mennyiseg[1]*2);
                                }
                            }
                        }
                    }
                    if (isset($session)) {
                        $qty = $this->session->data['arajanlat'][$key]+$qty;
                        unset ($this->session->data['arajanlat'][$key]);
                        $this->add($product_id,$qty,$session);
                    }

                } else {
                    $this->session->data['arajanlat'][$key] += (int)$qty;
                }
            }
        }

        $this->data = array();
    }

    public function deleteOption($product_key,$product_option_id,$product_option_value_id) {
        $valasztekok = explode(':',$product_key);
        if (isset($valasztekok[1])) {
            $session = unserialize(base64_decode($valasztekok[1]));

            $mennyiseg_levon = 0;
            if (is_array($session[$product_option_id])) {
                foreach($session[$product_option_id] as $key=>$value) {
                    $ertek = explode('|',$value);
                    if ($ertek[0] == $product_option_value_id) {
                        if (!empty($ertek[1]) ) {
                            $mennyiseg_levon = $ertek[1];
                        }
                        unset ($session[$product_option_id][$key]);
                        break;
                    }
                }

            }   else {
                unset ($session[$product_option_id]);
            }

            $mennyiseg = (int)$this->session->data['arajanlat'][$product_key]-(int)$mennyiseg_levon;
            $mennyiseg = $mennyiseg < 1 ? 1 : $mennyiseg;
            unset ($this->session->data['arajanlat'][$product_key]);
            $this->add($valasztekok[0],$mennyiseg,$session);
        }

        echo '';

    }




    public function countProduct($product_id) {

        $quantity = 0;
        foreach($this->session->data['arajanlat'] as $key=>$cart) {
            $tartalom = explode(':',$key);
            if ($tartalom[0] == $product_id) {
                $quantity += $cart;
            }
        }
        return $quantity;
    }

    public function countProducts() {

        $quantity = 0;
        if (isset($this->session->data['arajanlat']) && $this->session->data['arajanlat']) {
            foreach($this->session->data['arajanlat'] as $key=>$mennyiseg) {
                $tartalom = explode(':',$key);
                if (!isset($product[$tartalom[0]])) {
                    $product[$tartalom[0]] = 0;
                }
                $product[$tartalom[0]] += $mennyiseg;
            }
            foreach ($product as $mennyiseg) {
                $quantity += $mennyiseg;
            }
        }
        return $quantity;
    }

    public function remove($key) {
        if (isset($this->session->data['arajanlat'][$key])) {
            unset($this->session->data['arajanlat'][$key]);
        }
    }

    public function writeDatabase($data=array()) {

        if ($data) {

            $gyujto = array();
            foreach($data as $key=>$value) {
                $product = explode(':',$key);
                if (isset($product[1])) {
                    $options = unserialize(base64_decode($product[1]));
                    foreach($options as $po_key=>$option_values) {
                        if (is_array($option_values)) {
                            foreach($option_values as $option) {
                                $po_value = explode('|',$option);
                                if (isset($po_value[1]) && $po_value[1]) {
                                    if (isset($gyujto[$product[0]][$po_key][$po_value[0]])) {
                                        $gyujto[$product[0]][$po_key][$po_value[0]] += $po_value[1];
                                    } else {
                                        $gyujto[$product[0]][$po_key][$po_value[0]] = $po_value[1];
                                    }
                                }
                            }
                        } else {
                            $gyujto[$product[0]][$po_key] = $option_values;
                        }
                    }
                }

                if (isset($gyujto[$product[0]]['total_mennyiseg'])) {
                    $gyujto[$product[0]]['total_mennyiseg'] += $value;
                } else {
                    $gyujto[$product[0]]['total_mennyiseg'] = $value;
                }
                $gyujto[$product[0]]['product_id'] = $product[0];

            }
        }


        if ($gyujto) {

            if (isset($this->session->data['arajanlat_id']) && $this->session->data['arajanlat_id']) {
                $this->updateDatabase($gyujto);

                return true;
            }

            $ajanlat_neve = isset($this->session->data['arajanlat_neve']) ? $this->session->data['arajanlat_neve'] : '';
            $ajanlat_neve = isset($this->request->request['arajanlat_neve']) ? $this->request->request['arajanlat_neve'] : $ajanlat_neve;

            $this->language->load('module/arajanlat');
            $vevo_neve = $this->customer->getFirstName('vevo_neve').' '.$this->customer->getLastName('vevo_neve');
            $sql = "SELECT a.*, c.name AS orszag FROM ".DB_PREFIX."address a
                    LEFT JOIN ".DB_PREFIX."country c ON (c.country_id=a.country_id)
            WHERE address_id='".$this->customer->getAddressId()."'";
            $query = $this->db->query($sql);



            $sql = "INSERT INTO ".DB_PREFIX."arajanlat SET
                    customer_id = '".($this->customer->getId() ? $this->customer->getId() : '')."',
                    `name`      = '".$ajanlat_neve."',

                    szallito_neve              = '".$this->config->get('config_owner')."',
                    szallito_varos             = '".$this->language->get('szallito_varos')."',
                    szallito_utca              = '".$this->language->get('szallito_utca')."',
                    szallito_orszag            = '".$this->language->get('szallito_orszag')."',
                    szallito_adoszam           = '".$this->language->get('szallito_adoszam')."',
                    szallito_bank              = '".$this->language->get('szallito_bank')."',
                    szallito_bankszamla_szam   = '".$this->language->get('szallito_bankszamla_szam')."',
                    szallito_bankszamla_swift  = '".$this->language->get('szallito_bankszamla_swift')."',
                    szallito_bankszamla_iban   = '".$this->language->get('szallito_bankszamla_iban')."',

                    vevo_neve                  = '".$vevo_neve."',
                    vevo_varos                 = '".(isset($query->row['city']) ? $query->row['city'] : '')."',
                    vevo_utca                  = '".(isset($query->row['address_1']) ? $query->row['address_1'] : '')."',
                    vevo_orszag                = '".(isset($query->row['orszag']) ? $query->row['orszag'] : '')."',
                    vevo_adoszam               = '".$this->customer->getAdoszam()."',
                    vevo_bank                  = '',
                    vevo_bankszamla_szam       = '',
                    ajanlat_kelte              = curdate(),
                    ajanlat_szallitasi_hatarido= '',

                    ajanlat_fizetesi_hatarido  = '".$this->language->get('ajanlat_fizetesi_hatarido')."',
                    ajanlat_fizetesi_mod       = '".$this->language->get('ajanlat_fizetesi_mod')."',

                    ajanlat_kuldo_neve         = '".$vevo_neve."',
                    ajanlat_kuldo_telefon      = '".$this->customer->getTelephone()."',
                    ajanlat_kuldo_email        = '".$this->customer->getEmail()."',

                    ajanlat_megszolitas        = '".sprintf($this->language->get('ajanlat_megszolitas'),$vevo_neve)."',
                    ajanlat_altalanos_szoveg_felul= '".$this->language->get('ajanlat_altalanos_szoveg_felul')."',
                    ajanlat_altalanos_szoveg_alul= '".sprintf($this->language->get('ajanlat_altalanos_szoveg_alul'),$vevo_neve,$this->config->get('config_owner'))."',

                    `date_added`= NOW()";

            $query=$this->db->query($sql);
            $arajanlat_id = $this->db->getLastId();

            $sql = "INSERT INTO ".DB_PREFIX."arajanlat_to_store SET
                    arajanlat_id    = '".$arajanlat_id."',
                    `store_id`      = '" .(int)$this->config->get('config_store_id'). "'";
            $query=$this->db->query($sql);

            foreach($gyujto as $product) {
                $sql = "INSERT INTO ".DB_PREFIX."arajanlat_product SET
                    arajanlat_id    = '".$arajanlat_id."',
                    product_id      = '".$product['product_id']."',
                    mennyiseg       = '".$product['total_mennyiseg']."'";

                $query=$this->db->query($sql);
                $arajanlat_product_id = $this->db->getLastId();

                foreach($product as $product_option_id=>$product_option) {
                    if ( is_array($product_option) ) {
                        $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option SET
                            arajanlat_product_id    = '".$arajanlat_product_id."',
                            product_option_id       = '".$product_option_id."',
                            arajanlat_id            = '".$arajanlat_id."',
                            option_id               = (SELECT option_id FROM ".DB_PREFIX."product_option WHERE product_option_id = '".$product_option_id."')";

                        $query=$this->db->query($sql);
                        $arajanlat_option_id = $this->db->getLastId($sql);

                        foreach ($product_option as $product_option_value_id=>$mennyiseg) {
                            $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option_value SET
                            arajanlat_option_id     = '".$arajanlat_option_id."',
                            arajanlat_product_id    = '".$arajanlat_product_id."',
                            product_option_value_id = '".$product_option_value_id."',
                            arajanlat_id            = '".$arajanlat_id."',
                            option_value_id         = (SELECT option_value_id FROM ".DB_PREFIX."product_option_value WHERE product_option_value_id = '".$product_option_value_id."'),
                            mennyiseg               = $mennyiseg";

                            $this->db->query($sql);

                        }
                        $product_option_value_id = '';

                    } elseif  ($product_option_id != 'total_mennyiseg' && $product_option_id != 'product_id') {
                        $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option SET
                            arajanlat_product_id    = '".$arajanlat_product_id."',
                            arajanlat_id            = '".$arajanlat_id."',
                            product_option_id       = '".$product_option_id."',
                            option_id               = (SELECT option_id FROM ".DB_PREFIX."product_option WHERE product_option_id = '".$product_option_id."')";

                        $this->db->query($sql);
                        $arajanlat_option_id = $this->db->getLastId($sql);

                        $sql = "SELECT o.type FROM ".DB_PREFIX."product_option po ";
                        $sql .= " LEFT JOIN ".DB_PREFIX."option o ON (o.option_id=po.option_id)";
                        $sql .= " WHERE po.product_option_id='".$product_option_id."'";

                        $query = $this->db->query($sql);
                        if ($query->num_rows > 0 ){

                            if ($query->row['type'] == "select" || $query->row['type'] == "radio" || $query->row['type'] == "checkbox"  || $query->row['type'] == "checkbox_qty"  || $query->row['type'] == "image" ) {

                                $sql = "SELECT option_value_id FROM ".DB_PREFIX."product_option_value WHERE product_option_value_id = '".$product_option."'";
                                $query_pov = $this->db->query($sql);

                                if ($query_pov->num_rows > 0) {
                                    $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option_value SET
                                                product_option_value_id = '".$product_option."',
                                                arajanlat_product_id    = '".$arajanlat_product_id."',
                                                arajanlat_option_id     = '".$arajanlat_option_id."',
                                                arajanlat_id            = '".$arajanlat_id."',
                                                option_value_id         = '".$query_pov->row['option_value_id']."'";

                                    $query = $this->db->query($sql);
                                }
                            } else {
                                $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option_value SET
                                    arajanlat_product_id    = '".$arajanlat_product_id."',
                                    product_option_value_ertek  = '".$product_option."',
                                    arajanlat_id                = '".$arajanlat_id."',
                                    arajanlat_option_id         = '".$arajanlat_option_id."'";

                                $query = $this->db->query($sql);
                            }
                        }
                    }
                }
            }
        }
        if (isset($this->session->data['arajanlat']) ) {
            unset ($this->session->data['arajanlat']);
        }

        if ( isset($this->session->data['arajanlat_neve']) ) {
            unset ($this->session->data['arajanlat_neve']);
        }

        if ( isset($this->session->data['arajanlat_id']) ) {
            unset ($this->session->data['arajanlat_id']);
        }

    }

    public function updateDatabase($data) {

        $ajanlat_neve = isset($this->session->data['arajanlat_neve']) ? $this->session->data['arajanlat_neve'] : '';
        $ajanlat_neve = isset($this->request->request['arajanlat_neve']) ? $this->request->request['arajanlat_neve'] : $ajanlat_neve;

        $arajanlat_id = $this->session->data['arajanlat_id'];

        $sql = "UPDATE ".DB_PREFIX."arajanlat SET
                `name`      = '".$ajanlat_neve."',
                `date_modified`= NOW()
                WHERE arajanlat_id='".$arajanlat_id."'";

        $query = $this->db->query($sql);

        foreach($data as $product) {
            $sql = "SELECT * FROM ".DB_PREFIX."arajanlat_product
                    WHERE   arajanlat_id='".$arajanlat_id."'
                        AND product_id  ='".$product['product_id']."'";
            $query=$this->db->query($sql);

            if ($query->num_rows > 0) {
                $arajanlat_product_id = $query->row['arajanlat_product_id'];

                $sql = "UPDATE ".DB_PREFIX."arajanlat_product SET mennyiseg       = '".$product['total_mennyiseg']."'
                            WHERE   arajanlat_id='".$arajanlat_id."'
                                AND product_id  ='".$product['product_id']."'";
                $query = $this->db->query($sql);

            } else {
                $sql = "INSERT INTO ".DB_PREFIX."arajanlat_product SET
                arajanlat_id    = '".$arajanlat_id."',
                product_id      = '".$product['product_id']."',
                mennyiseg       = '".$product['total_mennyiseg']."'";

                $this->db->query($sql);
                $arajanlat_product_id = $this->db->getLastId();
            }

            foreach($product as $product_option_id=>$product_option) {
                if ( is_array($product_option) ) {

                    $sql = "SELECT * FROM ".DB_PREFIX."arajanlat_option
                                WHERE   arajanlat_product_id='".$arajanlat_product_id."'
                                    AND product_option_id  ='".$product_option_id."'";
                    $query=$this->db->query($sql);

                    if ($query->num_rows > 0) {
                        $arajanlat_option_id = $query->row['arajanlat_option_id'];

                    } else {

                        $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option SET
                            arajanlat_product_id    = '".$arajanlat_product_id."',
                            arajanlat_id            = '".$arajanlat_id."',
                            product_option_id       = '".$product_option_id."',
                            option_id       = (SELECT option_id FROM ".DB_PREFIX."product_option WHERE product_option_id = '".$product_option_id."')";

                        $this->db->query($sql);
                        $arajanlat_option_id = $this->db->getLastId($sql);
                    }

                    foreach ($product_option as $product_option_value_id=>$mennyiseg) {

                        $sql = "SELECT * FROM ".DB_PREFIX."arajanlat_option_value
                                WHERE   arajanlat_option_id='".$arajanlat_option_id."'
                                    AND product_option_value_id  ='".$product_option_value_id."'";
                        $query=$this->db->query($sql);

                        if ($query->num_rows > 0) {
                            $sql = "UPDATE ".DB_PREFIX."arajanlat_option_value SET
                                        mennyiseg               = $mennyiseg
                                   WHERE  arajanlat_option_id='".$arajanlat_option_id."'
                                    AND product_option_value_id  ='".$product_option_value_id."'";

                            $query=$this->db->query($sql);

                        } else {
                            $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option_value SET
                                        arajanlat_option_id     = '".$arajanlat_option_id."',
                                        arajanlat_id            = '".$arajanlat_id."',
                                        arajanlat_product_id    = '".$arajanlat_product_id."',
                                        product_option_value_id = '".$product_option_value_id."',
                                        option_value_id         = (SELECT option_value_id FROM ".DB_PREFIX."product_option_value WHERE product_option_value_id = '".$product_option_value_id."'),
                                        mennyiseg               = $mennyiseg";

                            $query = $this->db->query($sql);
                        }

                    }
                    $product_option_value_id = '';

                } elseif  ($product_option_id != 'total_mennyiseg' && $product_option_id != 'product_id') {
                    $sql = "SELECT * FROM ".DB_PREFIX."arajanlat_option
                                WHERE   arajanlat_product_id= '".$arajanlat_product_id."'
                                    AND arajanlat_id        = '".$arajanlat_id."'
                                    AND product_option_id   = '".$product_option_id."'";
                    $query=$this->db->query($sql);

                    if ($query->num_rows > 0) {
                        $arajanlat_option_id = $query->row['arajanlat_option_id'];

                    } else {

                        $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option SET
                                    arajanlat_product_id    = '".$arajanlat_product_id."',
                                    arajanlat_id            = '".$arajanlat_id."',
                                    product_option_id       = '".$product_option_id."',
                                    option_id               = (SELECT option_id FROM ".DB_PREFIX."product_option WHERE product_option_id = '".$product_option_id."')";

                        $this->db->query($sql);
                        $arajanlat_option_id = $this->db->getLastId($sql);
                    }

                    $sql = "SELECT o.type FROM ".DB_PREFIX."product_option po ";
                    $sql .= " LEFT JOIN ".DB_PREFIX."option o ON (o.option_id=po.option_id)";
                    $sql .= " WHERE po.product_option_id='".$product_option_id."'";

                    $query = $this->db->query($sql);
                    if ($query->num_rows > 0 ){

                        if ($query->row['type'] == "select" || $query->row['type'] == "radio" || $query->row['type'] == "checkbox"  || $query->row['type'] == "checkbox_qty"  || $query->row['type'] == "image" ) {

                            $sql = "SELECT option_value_id FROM ".DB_PREFIX."product_option_value WHERE product_option_value_id = '".$product_option."'";
                            $query_pov = $this->db->query($sql);

                            if ($query_pov->num_rows > 0) {

                                $sql = "SELECT * FROM ".DB_PREFIX."arajanlat_option_value
                                WHERE   arajanlat_option_id='".$arajanlat_option_id."'
                                    AND product_option_value_id  ='".$product_option_value_id."'";
                                $query=$this->db->query($sql);

                                if ($query->num_rows > 0) {
                                        $sql = "UPDATE ".DB_PREFIX."arajanlat_option_value SET
                                                        product_option_value_id = '".$product_option."',
                                                        option_value_id         = '".$query_pov->row['option_value_id']."'
                                                    WHERE   arajanlat_option_id='".$arajanlat_option_id."'
                                                        AND product_option_value_id  ='".$product_option_value_id."'";

                                        $query=$this->db->query($sql);


                                } else {
                                        $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option_value SET
                                                product_option_value_id = '".$product_option."',
                                                arajanlat_id            = '".$arajanlat_id."',
                                                arajanlat_product_id    = '".$arajanlat_product_id."',
                                                arajanlat_option_id     = '".$arajanlat_option_id."',
                                                option_value_id         = '".$query_pov->row['option_value_id']."'";

                                    $query = $this->db->query($sql);
                                }
                            }
                        } else {
                            $sql = "SELECT * FROM ".DB_PREFIX."arajanlat_option_value
                                WHERE   arajanlat_option_id='".$arajanlat_option_id."'";
                            $query=$this->db->query($sql);

                            if ($query->num_rows > 0) {
                                $sql = "UPDATE ".DB_PREFIX."arajanlat_option_value SET
                                                product_option_value_ertek = '".$product_option."',
                                            WHERE   arajanlat_option_id='".$arajanlat_option_id."'";
                            } else {
                                $sql = "INSERT INTO ".DB_PREFIX."arajanlat_option_value SET
                                            product_option_value_ertek = '".$product_option."',
                                            arajanlat_product_id    = '".$arajanlat_product_id."',
                                            arajanlat_id            = '".$arajanlat_id."',
                                            arajanlat_option_id     = '".$arajanlat_option_id."'";

                                $query = $this->db->query($sql);
                            }
                        }
                    }
                }
            }
        }

        $this->deleteMissingItem($data,$arajanlat_id);

        if (isset($this->session->data['arajanlat']) ) {
            unset ($this->session->data['arajanlat']);
        }

        if ( isset($this->session->data['arajanlat_neve']) ) {
            unset ($this->session->data['arajanlat_neve']);
        }

        if ( isset($this->session->data['arajanlat_id']) ) {
            unset ($this->session->data['arajanlat_id']);
        }
    }

    public function deleteMissingItem($data,$arajanlat_id) {
        $this->load->model('account/arajanlat');
        $arajanlat_info = $this->model_account_arajanlat->getArajanlat($arajanlat_id);

        foreach($arajanlat_info['products'] as $arajanlat_product) {
            if (!array_key_exists($arajanlat_product['product_id'],$data)) {
                $this->deleteArajanlatProduct($arajanlat_product['arajanlat_product_id']);
            } else {
                foreach ($arajanlat_product['options'] as $options) {
                    if (!array_key_exists($options['product_option_id'],$data[$arajanlat_product['product_id']]) ) {
                        $this->deleteArajanlatOption($options['arajanlat_option_id']);
                    }
                    if (is_array($options['value'])) {
                        foreach($options['value'] as $value) {
                            if (isset($value['product_option_value_id']) && isset($data[$arajanlat_product['product_id']][$options['product_option_id']]) && is_array($data[$arajanlat_product['product_id']][$options['product_option_id']])) {
                                if (!array_key_exists($value['product_option_value_id'], $data[$arajanlat_product['product_id']][$options['product_option_id']])) {
                                    $this->deleteArajanlatValue($value['product_option_value_id']);

                                }
                            }
                        }
                    }
                }
            }
        }


    }


    public function deleteArajanlatProduct($arajanlat_product_id) {
        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_product      WHERE arajanlat_product_id = '" .$arajanlat_product_id. "'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_option       WHERE arajanlat_product_id = '" .$arajanlat_product_id. "'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_option_value WHERE arajanlat_product_id = '" .$arajanlat_product_id. "'";
        $query = $this->db->query($sql);

    }

    public function deleteArajanlatOption($arajanlat_option_id) {

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_option       WHERE arajanlat_option_id = '" .$arajanlat_option_id. "'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_option_value WHERE arajanlat_option_id = '" .$arajanlat_option_id. "'";
        $query = $this->db->query($sql);

    }

    public function deleteArajanlatValue($arajanlat_option_value_id) {

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_option_value WHERE arajanlat_option_value_id = '" .$arajanlat_option_value_id. "'";
        $query = $this->db->query($sql);

    }
}