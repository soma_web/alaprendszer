<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
  
  <div id="tabs" class="htabs"><a href="#tab_general">General Options</a><a href="#tab_social">Social Network</a></div>
  
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
	
	<div id="tab_general">
      <table id="module" class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $entry_layout; ?></td>
            <td class="left"><?php echo $entry_position; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
            <td class="right"><?php echo $entry_sort_order; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
            <td class="left"><select name="tg_helios_social_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
            <td class="left"><select name="tg_helios_social_module[<?php echo $module_row; ?>][position]">
                <?php if ($module['position'] == 'content_top') { ?>
                <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                <?php } else { ?>
                <option value="content_top"><?php echo $text_content_top; ?></option>
                <?php } ?>  
                <?php if ($module['position'] == 'content_bottom') { ?>
                <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                <?php } else { ?>
                <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                <?php } ?> 
                <?php if ($module['position'] == 'column_left') { ?>
                <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                <?php } else { ?>
                <option value="column_left"><?php echo $text_column_left; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_right') { ?>
                <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                <?php } else { ?>
                <option value="column_right"><?php echo $text_column_right; ?></option>
                <?php } ?>
              </select></td>
            <td class="left"><select name="tg_helios_social_module[<?php echo $module_row; ?>][status]">
                <?php if ($module['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
            <td class="right"><input type="text" name="tg_helios_social_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="4"></td>
            <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
          </tr>
        </tfoot>
      </table>
	    </div>
		
		
		<div id="tab_social">
				<table class="form">
				
				
					<tr>
						<td><img src="view/image/social_icons/facebook.png" alt="" /> Facebook</td>
						<td><input type="text" name="facebook_icon" value="<?php echo $facebook_icon; ?>" size="60" />
						<input type="checkbox" value="1" name="facebook_show"<?php if($facebook_show == '1') echo ' checked="checked"';?> /> Show</td>
					</tr> 
		
					<tr>
						<td><img src="view/image/social_icons/twitter.png" alt="" /> Twitter</td>
						<td><input type="text" name="twitter_icon" value="<?php echo $twitter_icon; ?>" size="60" />
						<input type="checkbox" value="1" name="twitter_show"<?php if($twitter_show == '1') echo ' checked="checked"';?> /> Show</td>
					</tr> 
					
					
					<tr>
						<td><img src="view/image/social_icons/yahoomessenger.png" alt="" /> Yahoo Messenger</td>
						<td><input type="text" name="yahoomessenger_icon" value="<?php echo $yahoomessenger_icon; ?>" size="60" />
						<input type="checkbox" value="1" name="yahoomessenger_show"<?php if($yahoomessenger_show == '1') echo ' checked="checked"';?> /> Show</td>
					</tr> 
					
				
					<tr>
						<td><img src="view/image/social_icons/skype.png" alt="" /> Skype</td>
						<td><input type="text" name="skype_icon" value="<?php echo $skype_icon; ?>" size="60" />
						<input type="checkbox" value="1" name="skype_show"<?php if($skype_show == '1') echo ' checked="checked"';?> /> Show</td>
					</tr> 
					
					
					<tr>
						<td><img src="view/image/social_icons/flickr.png" alt="" /> Flickr</td>
						<td><input type="text" name="flickr_icon" value="<?php echo $flickr_icon; ?>" size="60" />
						<input type="checkbox" value="1" name="flickr_show"<?php if($flickr_show == '1') echo ' checked="checked"';?> /> Show</td>
					</tr> 
					
					
					<tr>
						<td><img src="view/image/social_icons/vimeo.png" alt="" /> Vimeo</td>
						<td><input type="text" name="vimeo_icon" value="<?php echo $vimeo_icon; ?>" size="60" />
						<input type="checkbox" value="1" name="vimeo_show"<?php if($vimeo_show == '1') echo ' checked="checked"';?> /> Show</td>
					</tr> 
					
					
				</table>
			</div> <!-- #tab_social (end) -->
		
		
    </form>
  </div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><select name="tg_helios_social_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="tg_helios_social_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';
	html += '    <td class="left"><select name="tg_helios_social_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="tg_helios_social_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
$('#languages a').tabs(); 
$('#vtab-option a').tabs();
//--></script> 


