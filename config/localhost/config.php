<?php
// HTTP
define('HTTP_SERVER', '');
define('HTTP_IMAGE', 'image/');
define('HTTP_ADMIN', 'admin/');
define('HTTP_CATALOG', 'alaprendszer/');

// HTTPS
define('HTTPS_SERVER', '');
define('HTTPS_IMAGE', '/alaprendszer/image/');
define('HTTP_18', 'felnott/');
define('HTTPS_CATALOG', 'alaprendszer/');

// DIR
define('DIR_ARUHAZ', '/alaprendszer/');
define('DIR_APPLICATION', 'catalog/');
define('DIR_SYSTEM', 'system/');
define('DIR_DATABASE', 'system/database/');
define('DIR_LANGUAGE', 'catalog/language/');
define('DIR_TEMPLATE', 'catalog/view/theme/');
define('DIR_TEMPLATE_IMAGE', '/alaprendszer/catalog/view/theme/');
define('DIR_CONFIG', 'system/config/');
define('DIR_IMAGE', 'image/');
define('DIR_CACHE', 'system/cache/');
define('DIR_DOWNLOAD', 'download/');
define('DIR_LOGS', 'system/logs/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'dell');
define('DB_PREFIX', 'hungaro_');

define('DB_DATABASE', 'videop13');

?>