<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top;
    switch ($answer_page) {
        case "success": ?>
                <br/>
                <h2 class="siker"><?php echo $text_thank; ?></h2>
                <div>
                   <h3><?php echo $text_dear_customer; ?></h3>
                    <?php echo $text_details; ?>
                    <?php echo $text_see_you_soon; ?>
                </div>
                <table class="eredmenytabla1">
                    <tr>
                        <th><?php echo $text_transaction_id; ?></td>
                            <td><?php echo  $tranzakcioAzonosito; ?></th>
                    </tr>
                </table>
            <?php break;
        case "cancel": ?>
                <h2 class="elutasitott"><?php echo $text_cancelled; ?></h2>
            <?php  break;
        case "fail": ?>
            <h2><?php echo $text_store_error; ?></h2>
            <table class="eredmenytabla">
                <tr>
                    <th><?php echo $text_transaction_id; ?></th>
                    <td><?php echo $tranzakcioAzonosito ?></td>
                </tr>
                <tr>
                    <th><?php echo $text_error; ?></td>
                        <td><?php echo  $error_msg; ?></th>
                </tr>
            </table>
            <?php break;
        case "unknown": ?>
            <h2 class="hiba"><?php echo $text_bank_error; ?></h2>
            <table class="eredmenytabla">
                <tr>
                    <th><?php echo $text_error; ?></td>
                        <td><?php if(!empty($error_msg)) echo  $error_msg; ?></th>
                </tr>
            </table>
            <?php break;
        case "default":  ?>
            <h2 class="hiba"><?php echo $text_no_purchase; ?></h2>
                <?php break;
    }
    echo $content_bottom; ?></div>
<?php echo $footer; ?>