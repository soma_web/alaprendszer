<?php
class ControllerAccountUtalvanyellenorzes extends Controller {
	private $error = array();
	     
  	public function index() {	
    	if (!$this->customer->isLogged()) {
      		$this->session->data['redirect'] = $this->url->link('account/password', '', 'SSL');

      		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	}

		$this->language->load('account/utalvanyellenorzes');

    	$this->document->setTitle($this->language->get('heading_title'));
			  
    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('account/utalvanyok');

            if (isset($_REQUEST['vonalkod_bavaltasa']) && $_REQUEST['vonalkod_bavaltasa'] == 1 ) {
                $this->model_account_utalvanyok->vonalkodBevaltasa();
                $this->session->data['success'] = $this->language->get('text_success_bevaltva');
                $this->redirect($this->url->link('account/account', '', 'SSL'));
            } elseif (isset($_REQUEST['vonalkod_bavaltasa'])) {
                $this->session->data['error'] = $this->language->get('error_nincs_bevaltva');
                $this->redirect($this->url->link('account/account', '', 'SSL'));
            }


			$utalvany = $this->model_account_utalvanyok->vonalkodEllenorzes();
            if ($utalvany) {
                if (!$this->model_account_utalvanyok->jogosultsagEllenorzes($utalvany['product_id']) ) {
                    $this->session->data['error'] = $this->language->get('error_nemjogosult');
                    $this->redirect($this->url->link('account/utalvanyellenorzes', '', 'SSL'));

                }
            }
            if ($utalvany && $utalvany['status'] == 1){
                if ($utalvany['felhasznalva'] == 1) {
                    $this->session->data['error'] = $this->language->get('error_felhasznalva');
                    $this->redirect($this->url->link('account/utalvanyellenorzes', '', 'SSL'));
                } else {
                    $this->session->data['success'] = $this->language->get('text_success');
                    $this->session->data['vonalkod_bavaltva'] = 1;
                    $this->session->data['vonalkod'] = $utalvany['vonalkod'];
                    $this->redirect($this->url->link('account/utalvanyellenorzes', '', 'SSL'));
                }
            } elseif ($utalvany) {
                $this->session->data['error'] = $this->language->get('error_nem_engedelyezett');
                $this->redirect($this->url->link('account/utalvanyellenorzes', '', 'SSL'));
            } else {
                $this->session->data['error'] = $this->language->get('error_nemletezo');
                $this->redirect($this->url->link('account/utalvanyellenorzes', '', 'SSL'));
            }
    	}

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $this->data['error'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $this->data['error'] = '';
        }



        if (isset($this->session->data['vonalkod_bavaltva'])) {
            $this->data['vonalkod_bavaltva'] = $this->session->data['vonalkod_bavaltva'];
            unset($this->session->data['vonalkod_bavaltva']);
        } else {
            $this->data['vonalkod_bavaltva'] = '';
        }

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),       	
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/utalvanyellenorzes', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
			
    	$this->data['heading_title'] = $this->language->get('heading_title');



    	$this->data['button_continue'] = $this->language->get('button_continue');
    	$this->data['button_back'] = $this->language->get('button_back');
    	$this->data['text_vonalkod'] = $this->language->get('text_vonalkod');
    	$this->data['entry_vonalkod'] = $this->language->get('entry_vonalkod');
    	$this->data['entry_vonalkod_bevaltasa'] = $this->language->get('entry_vonalkod_bevaltasa');
    	$this->data['text_yes'] = $this->language->get('text_yes');
    	$this->data['text_no'] = $this->language->get('text_no');

		if (isset($this->error['vonalkod'])) {
			$this->data['error_vonalkod'] = $this->error['vonalkod'];
		} else {
			$this->data['error_vonalkod'] = '';
		}

    	$this->data['action'] = $this->url->link('account/utalvanyellenorzes', '', 'SSL');
		
		if (isset($this->request->post['vonalkod'])) {
    		$this->data['vonalkod'] = $this->request->post['vonalkod'];
		} else {
			$this->data['vonalkod'] = '';
		}

        if (isset($this->session->data['vonalkod'])) {
            $this->data['vonalkod'] = $this->session->data['vonalkod'];
            unset($this->session->data['vonalkod']);
        } elseif (isset($this->request->post['vonalkod'])) {
            $this->data['vonalkod'] = $this->request->post['vonalkod'];
        } else {
            $this->data['vonalkod'] = '';
        }


    	$this->data['back'] = $this->url->link('account/account', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/utalvanyellenorzes.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/utalvanyellenorzes.tpl';
		} else {
			$this->template = 'default/template/account/utalvanyellenorzes.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
						
		$this->response->setOutput($this->render());			
  	}
  
  	private function validate() {
    	if ((utf8_strlen($this->request->post['vonalkod']) != 13) ) {
      		$this->error['vonalkod'] = $this->language->get('error_vonalkod');
    	}

	
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}
}
?>
