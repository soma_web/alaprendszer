<?php

class ModelAccountRememberMe extends Model {
    public function addRememberedLogin($customer_id, $token) {
        $sql = "INSERT INTO ".DB_PREFIX."remembered_logins(customer_id, token) VALUES(".$customer_id.", '".$token."');";
        $this->db->query($sql);
    }

    public function getRememberedLogins($customer_id) {
        $sql = "SELECT * FROM ".DB_PREFIX."remembered_logins WHERE customer_id=".$customer_id.";";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function checkRememberedLogin($customer_id, $token) {
        $sql = "SELECT * FROM ".DB_PREFIX."remembered_logins WHERE customer_id=".$customer_id." AND token='".$token."';";
        $query = $this->db->query($sql);

        if($query->num_rows == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function removeRememberedLogin($customer_id, $token) {
        $sql = "DELETE FROM ".DB_PREFIX."remembered_logins WHERE customer_id=".$customer_id." AND token='".$token."';";
        $this->db->query($sql);
    }

    public function removeAllRememberedToUser($customer_id) {
        $sql = "DELETE FROM ".DB_PREFIX."remembered_logins WHERE customer_id=".$customer_id.";";
        $this->db->query($sql);
    }

    public function autoLogin() {
        $userid = $_COOKIE['userid'];
        if($this->checkRememberedLogin($userid, $_COOKIE['token'])) {
            # login
            $this->load->model('account/customer');
            $customer = $this->model_account_customer->getCustomer($userid);
            $email = $customer['email'];
            $this->customer->login($email, '', true);
        } else {
            $this->removeAllRememberedToUser($userid);
        }
    }
}