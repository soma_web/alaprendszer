<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <div  style="float: left">
        <table class="form">
            <tr>
                <td><?php echo $entry_magas_alacsony; ?></td>
                <td><select name="item_product_gls_magas_alacsony">
                    <?php if ($item_product_gls_magas_alacsony == 1) { ?>
                        <option value="1" selected="selected"><?php echo $text_magas; ?></option>
                        <option value="2"><?php echo $text_alacsony ?></option>
                    <?php } else { ?>
                        <option value="1"><?php echo $text_magas; ?></option>
                        <option value="2"  selected="selected"><?php echo $text_alacsony ?></option>
                    <?php } ?>
                    </select>
                </td>
            </tr>


            <?php $elso = true;?>
            <?php foreach ($languages as $language) { ?>
                <tr>
                    <?php if ($elso) { ?>
                        <td><?php echo $entry_title_heading; ?></td>
                        <?php $elso = false; ?>
                    <?php } else { ?>
                        <td></td>
                    <?php } ?>
                    <td><input type="text" name="item_product_gls_header_<?php echo $language['language_id']; ?>" value="<?php echo ${'item_product_gls_header_' .$language['language_id'] }; ?>" />
                        <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                    </td>
                </tr>
            <?php } ?>


            <?php $elso = true;?>
            <?php foreach ($languages as $language) { ?>
                <tr>
                    <?php if ($elso) { ?>
                        <td><?php echo $entry_title_text; ?></td>
                        <?php $elso = false; ?>
                    <?php } else { ?>
                        <td></td>
                    <?php } ?>
                    <td><input type="text" name="item_product_gls_text_<?php echo $language['language_id']; ?>" value="<?php echo ${'item_product_gls_text_' .$language['language_id'] }; ?>" />
                        <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                    </td>
                </tr>
            <?php } ?>


          <tr>
            <td><?php echo $entry_tax_class; ?></td>
            <td><select name="item_product_gls_tax_class_id">
                  <option value="0"><?php echo $text_none; ?></option>
                  <?php foreach ($tax_classes as $tax_class) { ?>
                  <?php if ($tax_class['tax_class_id'] == $item_product_gls_tax_class_id) { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_geo_zone; ?></td>
            <td><select name="item_product_gls_geo_zone_id">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $item_product_gls_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="item_product_gls_status">
                <?php if ($item_product_gls_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>

            <tr>
                <td><?php echo $entry_mail; ?></td>
                <td><textarea name="item_product_gls_mail" style="width: 100%; height: 70px;"><?php echo $item_product_gls_mail; ?></textarea></td>
            </tr>

            <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="item_product_gls_sort_order" value="<?php echo $item_product_gls_sort_order; ?>" size="1" /></td>
          </tr>
            <?php if(isset($megjelenit_gls) && $megjelenit_gls == 1) { ?>
            <tr>
                <td><?php echo $text_gls_csomagpont; ?></td>
                <td>
                    <?php if ($item_product_gls_gls_csomagpont == 1) { ?>
                        <input type="checkbox" name="item_product_gls_gls_csomagpont" value="<?php echo $item_product_gls_gls_csomagpont; ?>"
                               checked="checked"/>
                    <?php } else { ?>
                        <input type="checkbox" name="item_product_gls_gls_csomagpont" value="<?php echo $item_product_gls_gls_csomagpont; ?>"/>
                    <?php } ?>
                </td>
            </tr>
            <?php } else { $item_product_gls_gls_csomagpont = 0; } ?>
        </table>
    </div>
      <div  style="float: left; margin-left: 60px;"><h2>Fizetési módok</h2>
          <table class="form">
              <?php foreach($payment as $key =>$value) {?>
                  <tr>
                      <td><?php echo $value['name']; ?></td>
                      <td><select name="item_product_gls_<?php echo $key?>">
                              <?php if ($value['valasztva']) { ?>
                                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                  <option value="0"><?php echo $text_disabled; ?></option>
                              <?php } else { ?>
                                  <option value="1"><?php echo $text_enabled; ?></option>
                                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                              <?php } ?>
                          </select></td>
                  </tr>
              <?php } ?>
          </table>
      </div>

          <div  style="float: left; margin-left: 60px;"><h2>Engedélyezett szállítási módok</h2>
              <?php unset($shipping['shipping_item_product_gls']);?>

              <table class="form">
                  <?php foreach($shipping as $key =>$value) {?>
                      <tr>
                          <td><?php echo $value['name']; ?></td>
                          <td><select name="item_product_gls_<?php echo $key?>">
                                  <?php if ($value['valasztva']) { ?>
                                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                      <option value="0"><?php echo $text_disabled; ?></option>
                                  <?php } else { ?>
                                      <option value="1"><?php echo $text_enabled; ?></option>
                                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                  <?php } ?>
                              </select></td>
                      </tr>
                  <?php }   ?>
              </table>
          </div>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?> 