<?php if ($addresses) { ?>
    <input type="radio" name="payment_address" value="existing" id="payment-address-existing" checked="checked" />

    <label for="payment-address-existing"><?php echo $text_address_existing; ?></label>
    <div id="payment-existing">
        <select name="address_id" style="width: 100%; margin-bottom: 15px;" size="5">
            <?php $i=0; foreach ($addresses as $address) { ?>
                <?php if ($address['address_id'] == $address_id) { ?>
                    <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['company']; ?> <?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php //echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                <?php } elseif (count($addresses) == 1) { ?>
                    <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['company']; ?> <?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php //echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $address['address_id']; ?>"><?php echo $address['company']; ?> <?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php //echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                <?php }
                $i=1;  ?>
            <?php } ?>
        </select>
    </div>
<?php } ?>

<?php if ($addresses) { ?>
<p>
    <input type="radio" name="payment_address" value="new" id="payment-address-new" />
    <label for="payment-address-new"><?php echo $text_address_new; ?></label>
</p>
<div id="payment-new" style="display: none;">
    <?php } else { ?>
    <p>
        <input type="radio" name="payment_address" value="new" id="payment-address-new" checked="checked" />
        <label for="payment-address-new"><?php echo $text_address_new; ?></label>
    </p>
    <div id="payment-new" style="display: block;">
        <?php } ?>
        <table class="form">
            <?php if (($this->config->get('megjelenit_regisztracio_vezeteknev') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_vezeteknev') == 1 )) {?>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
                    <td><input type="text" name="firstname" value="" class="large-field" /></td>
                </tr>
            <?php } ?>
            <?php if (($this->config->get('megjelenit_regisztracio_keresztnev') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_keresztnev') == 1  )) {?>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
                    <td><input type="text" name="lastname" value="" class="large-field" /></td>
                </tr>
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_cegnev') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_cegnev') == 1 ) ) {?>
                <tr>
                    <td><?php echo $entry_company; ?></td>
                    <td><input type="text" name="company" value="" class="large-field" /></td>
                </tr>
            <?php } else { ?>
                <input type="hidden" name="company" value=""  />
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_adoszam') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_adoszam') == 1 ) ) {?>
                <tr>
                    <td><?php echo $entry_adoszam; ?></td>
                    <td><input type="text" name="adoszam" value="" class="large-field" /></td>
                </tr>
            <?php } else { ?>
                <input type="hidden" name="adoszam" value=""  />
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_vallalkozasi_forma') == 1 )  || ($this->config->get('megjelenit_regisztracio_ceges_vallalkozasi_forma') == 1 ) ) {?>
                <tr>
                    <td><?php echo $entry_vallalkozasi_forma; ?></td>
                    <td><input type="text" name="vallalkozasi_forma" class="large-field" value="" /></td>
                </tr>
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_szekhely') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_szekhely') == 1 ) ) {?>
                <tr>
                    <td><?php echo $entry_szekhely; ?></td>
                    <td><input type="text" name="szekhely" class="large-field" value="" /></td>
                </tr>
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_ugyvezeto_neve') == 1  ) || ($this->config->get('megjelenit_regisztracio_ceges_ugyvezeto_neve') == 1 ) ) {?>
                <tr>
                    <td><?php echo $entry_ugyvezeto_neve; ?></td>
                    <td><input type="text" name="ugyvezeto_neve" class="large-field" value="" /></td>
                </tr>
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_ugyvezeto_telefonszama') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_ugyvezeto_telefonszama') == 1 ) ) {?>
                <tr>
                    <td><?php echo $entry_ugyvezeto_telefonszama; ?></td>
                    <td><input type="text" name="ugyvezeto_telefonszama" class="large-field" value="" /></td>
                </tr>
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_utca') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_utca') == 1 ) ) {?>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
                    <td><input type="text" name="address_1" value="" class="large-field" /></td>
                </tr>
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_cimadat_kiegeszito') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_cimadat_kiegeszito') == 1 ) ) {?>
                <tr>
                    <td><?php echo $entry_address_2; ?></td>
                    <td><input type="text" name="address_2" value="" class="large-field" /></td>
                </tr>
            <?php } else { ?>
                <input type="hidden" name="address_2" value=""  />
            <?php } ?>

            <?php if ( ($this->config->get('megjelenit_regisztracio_varos') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_varos') == 1 ) ) {?>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_city; ?></td>
                    <td><input type="text" name="city" value="" class="large-field" /></td>
                </tr>
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_iranyitoszam') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_iranyitoszam') == 1 ) ) {?>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
                    <td><input type="text" name="postcode" value="" class="large-field" /></td>
                </tr>
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_orszag') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_orszag') == 1 ) ) {?>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_country; ?></td>
                    <td><select name="country_id" class="large-field" onchange="$('#payment-address select[name=\'zone_id\']').load('index.php?route=checkout/payment_address/zone&country_id=' + this.value);">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($countries as $country) { ?>
                                <?php if ($country['country_id'] == $country_id) { ?>
                                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select></td>
                </tr>
            <?php } ?>
            <?php if ( ($this->config->get('megjelenit_regisztracio_megye') == 1 ) || ($this->config->get('megjelenit_regisztracio_ceges_megye') == 1 ) ) {?>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
                    <td><select name="zone_id" class="large-field">
                        </select></td>
                </tr>
            <?php } ?>
        </table>
    </div>
    <br />
    <div class="buttons">
        <div class="right"><input type="button" value="<?php echo $button_continue; ?>" id="button-payment-address" class="button" /></div>
    </div>
    <script type="text/javascript"><!--
        $('#payment-address select[name=\'zone_id\']').load('index.php?route=checkout/payment_address/zone&country_id=<?php echo $country_id; ?>');
        $('#payment-address input[name=\'payment_address\']').on('change', function() {
            debugger;
            if (this.value == 'new') {
                $('#payment-existing').hide();
                $('#payment-new').show();
            } else {
                $('#payment-existing').show();
                $('#payment-new').hide();
            }
        });
        //--></script>