<?php
class ModelCatalogElhelyezkedes extends Model {

    public function getElhelyezkedesek() {

        $vissza = array();
        $elhelyezkedesek = $this->config->get("elhelyezkedes");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['status'] == 1) {
                    $vissza[] = $value;
                }
            }
        }
        $vissza = $this->config->rendezes($vissza,"sort_order");

        return $vissza;
    }

    public function kellAlcsoport () {

        if ($_REQUEST['elhelyezkedes_id'] == 0) {
            return false;
        }

        $vissza = false;
        $elhelyezkedes = $this->config->get('elhelyezkedes');
        foreach($elhelyezkedes as $value) {
            if ($value['elhelyezkedes_id'] == $_REQUEST['elhelyezkedes_id']) {
                if ($value['alcsoport_status'] == 1) {
                    $vissza = true;
                }
            }
        }

        return $vissza;

    }


    public function getPenzugyAllapotKituntetett() {

        $elhelyezkedesek = $this->config->get("fizetes_elbiralas");

        $fizetes_elbiralas_status_id = "";
        $penzugyi_status_id          = "";

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['status'] == 1 && $value['teljesitett_sor'] == 1) {
                    $fizetes_elbiralas_status_id = $value['fizetes_elbiralas_status_id'];
                    break;
                }
            }
        }

        $elhelyezkedesek = $this->config->get("penzugyi_status");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['status'] == 1 && $value['teljesitett_sor'] == 1) {
                    $penzugyi_status_id = $value['penzugyi_status_id'];
                    break;
                }
            }
        }

        $vissza = array(
            'fizetes_elbiralas_status_id' => $fizetes_elbiralas_status_id,
            'penzugyi_status_id'          => $penzugyi_status_id
        );

        return $vissza;
    }

    public function getSzabadHelyek() {

        $vissza = false;
        $elhelyezkedesek = $this->config->get("elhelyezkedes");
        $elhelyezkedes_alcsoport = $this->config->get("elhelyezkedes_alcsoport");
        $kiemelesek = $this->config->get("kiemelesek");

        $biralat = $this->getPenzugyAllapotKituntetett();

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['elhelyezkedes_id'] == $_REQUEST['elhelyezkedes_id']) {
                    $elhelyezkedesek_max = $value['maximum'];
                }
            }
        }

        if ($elhelyezkedes_alcsoport) {
            foreach ($elhelyezkedes_alcsoport as $value) {
                if ($value['elhelyezkedes_alcsoport_id'] == $_REQUEST['elhelyezkedes_alcsoport_id']) {
                    $elhelyezkedes_alcsoport_max = $value['maximum'];
                }
            }
        }

        if ($kiemelesek) {
            foreach ($kiemelesek as $value) {
                if ($value['kiemelesek_id'] == $_REQUEST['kiemelesek_id']) {
                    $kiemelesek_max = $value['maximum'];
                }
            }
        }

        if (empty($_REQUEST['datum_kezdo']) || empty($_REQUEST['datum_vege']) || $_REQUEST['idoszak'] < 1 ) {
            return true;
        }

        $sql = "SELECT elhelyezkedes_id,  elhelyezkedes_alcsoport_id, kiemelesek_id, datum_tol, datum_ig, elhelyezes_id FROM " . DB_PREFIX . "product_elhelyezes WHERE
            (datum_tol >= '" . $_REQUEST['datum_kezdo'] . "' AND datum_tol <= '" . $_REQUEST['datum_vege'] . "') OR
            (datum_ig >= '" . $_REQUEST['datum_kezdo'] . "' AND datum_ig <= '" . $_REQUEST['datum_vege'] . "') OR
            (datum_tol <= '" . $_REQUEST['datum_kezdo'] . "' AND datum_ig >= '" . $_REQUEST['datum_vege'] . "')
            AND (fizetes_elbiralas_status_id != '" .$biralat['fizetes_elbiralas_status_id'] . "' OR penzugyi_status_id != '" . $biralat['penzugyi_status_id'] ."')
            AND elhelyezes_id != '" .$_REQUEST['elhelyezes_id'] . "'";

        $query = $this->db->query($sql);
        $elhelyezkedesek_ossz           = 0;
        $elhelyezkedes_alcsoport_ossz   = 0;
        $kiemelesek_ossz                = 0;

        foreach ($query->rows as $value) {
            if ($value['elhelyezkedes_id'] == $_REQUEST['elhelyezkedes_id'] && $value['elhelyezes_id'] != $_REQUEST['elhelyezes_id']) {
                $elhelyezkedesek_ossz ++;
            }
            if ($value['elhelyezkedes_id'] == $_REQUEST['elhelyezkedes_id'] &&  $value['elhelyezkedes_alcsoport_id'] == $_REQUEST['elhelyezkedes_alcsoport_id'] && $value['elhelyezes_id'] != $_REQUEST['elhelyezes_id']) {
                $elhelyezkedes_alcsoport_ossz ++;
            }
            if ($value['kiemelesek_id'] == $_REQUEST['kiemelesek_id'] && $value['elhelyezes_id'] != $_REQUEST['elhelyezes_id']) {
                $kiemelesek_ossz ++;
            }
        }

        $elhelyezkedesek_kulonbseg = 1;
        $elhelyezkedes_alcsoport_kulonbseg = 1;
        $kiemelesek_kulonbseg = 1;

        if (isset($elhelyezkedesek_max)) {
            $elhelyezkedesek_kulonbseg = $elhelyezkedesek_max -$elhelyezkedesek_ossz;
        }

        if (isset($elhelyezkedes_alcsoport_max)) {
            $elhelyezkedes_alcsoport_kulonbseg = $elhelyezkedes_alcsoport_max - $elhelyezkedes_alcsoport_ossz;
        }
        if (isset($kiemelesek_max)) {
            $kiemelesek_kulonbseg = $kiemelesek_max - $kiemelesek_ossz;
        }

        $vissza = array(
            'elhelyezkedesek_kulonbseg'         => $elhelyezkedesek_kulonbseg,
            'elhelyezkedes_alcsoport_kulonbseg' => $elhelyezkedes_alcsoport_kulonbseg,
            'kiemelesek_kulonbseg'              => $kiemelesek_kulonbseg
        );

        return $vissza;
    }


}
?>