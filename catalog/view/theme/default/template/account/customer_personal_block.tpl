<div class="content">
    <table class="form">
        <?php if (($this->config->get('megjelenit_regisztracio_cegnev') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_cegnev') == 1 && $ceges == 1 )) {?>
            <tr>
                <td><?php if ($this->config->get('megjelenit_regisztracio_cegnev_kotelezo') == 1) { ?><span class="required">*</span> <?php } ?><?php echo $entry_company; ?></td>
                <td><input type="text" name="company" value="<?php echo $company; ?>" />
                    <?php if ($error_company) { ?>
                        <span class="error"><?php echo $error_company; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <?php if (($this->config->get('megjelenit_regisztracio_adoszam') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_adoszam') == 1 && $ceges == 1 )) {?>
            <tr>
                <td><?php if ($this->config->get('megjelenit_regisztracio_adoszam_kotelezo') == 1) { ?><span class="required">*</span> <?php } ?><?php echo $entry_adoszam; ?></td>
                <td><input type="text" name="adoszam" value="<?php echo $adoszam; ?>" />
                    <?php if ($error_adoszam) { ?>
                        <span class="error"><?php echo $error_adoszam; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>








        <?php if ( ($this->config->get('megjelenit_regisztracio_vallalkozasi_forma') == 1 && $ceges == 0)  || ($this->config->get('megjelenit_regisztracio_ceges_vallalkozasi_forma') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><?php if ($this->config->get('megjelenit_regisztracio_vallalkozasi_forma_kotelezo') == 1) { ?><span class="required">*</span> <?php } ?><?php echo $entry_vallalkozasi_forma; ?></td>
                <td><input type="text" name="vallalkozasi_forma" value="<?php echo $vallalkozasi_forma; ?>" />
                    <?php if ($error_vallalkozasi_forma) { ?>
                        <span class="error"><?php echo $error_vallalkozasi_forma; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_szekhely') == 1 && $ceges == 0 ) || ($this->config->get('megjelenit_regisztracio_ceges_szekhely') == 1 && $ceges == 1 ) ) {?>
            <tr>
                <td><?php if ($this->config->get('megjelenit_regisztracio_szekhely_kotelezo') == 1) { ?><span class="required">*</span> <?php } ?><?php echo $entry_szekhely; ?></td>
                <td><input type="text" name="szekhely" value="<?php echo $szekhely; ?>" />
                    <?php if ($error_szekhely) { ?>
                        <span class="error"><?php echo $error_szekhely; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_ugyvezeto_neve') == 1 && $ceges == 0 ) || ($this->config->get('megjelenit_regisztracio_ceges_ugyvezeto_neve') == 1 && $ceges == 1 ) ) {?>
            <tr>
                <td><?php if ($this->config->get('megjelenit_regisztracio_ugyvezeto_neve_kotelezo') == 1) { ?><span class="required">*</span> <?php } ?><?php echo $entry_ugyvezeto_neve; ?></td>
                <td><input type="text" name="ugyvezeto_neve" value="<?php echo $ugyvezeto_neve; ?>" />
                    <?php if ($error_ugyvezeto_neve) { ?>
                        <span class="error"><?php echo $error_ugyvezeto_neve; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <?php if ( ($this->config->get('megjelenit_regisztracio_ugyvezeto_telefonszama') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_ugyvezeto_telefonszama') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><?php if ($this->config->get('megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo') == 1) { ?><span class="required">*</span> <?php } ?><?php echo $entry_ugyvezeto_telefonszama; ?></td>
                <td><input type="text" name="ugyvezeto_telefonszama" value="<?php echo $ugyvezeto_telefonszama; ?>" />
                    <?php if ($error_ugyvezeto_telefonszama) { ?>
                        <span class="error"><?php echo $error_ugyvezeto_telefonszama; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>










        <?php if (($this->config->get('megjelenit_regisztracio_vezeteknev') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_vezeteknev') == 1 && $ceges == 1 )) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
                <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
                    <?php if ($error_firstname) { ?>
                        <span class="error"><?php echo $error_firstname; ?></span>
                    <?php } ?></td>
            </tr>
        <?php } ?>
        <?php if (($this->config->get('megjelenit_regisztracio_keresztnev') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_keresztnev') == 1 && $ceges == 1 )) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
                <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
                    <?php if ($error_lastname) { ?>
                        <span class="error"><?php echo $error_lastname; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <?php if (($this->config->get('megjelenit_regisztracio_email') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_email') == 1 && $ceges == 1 )) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_email; ?></td>
                <td><input type="text" name="email" value="<?php echo $email; ?>" />
                    <?php if ($error_email) { ?>
                        <span class="error"><?php echo $error_email; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <?php if (($this->config->get('megjelenit_regisztracio_email_megerosites') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_email_megerosites') == 1 && $ceges == 1 )) {?>
            <?php if(isset($regisztracio) && $regisztracio == 1) { ?>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_email_again; ?></td>
                    <td><input type="text" name="email_again" value="<?php echo $email_again; ?>" />
                        <?php if ($error_email_again) { ?>
                            <span class="error"><?php echo $error_email_again; ?></span>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        <?php if (($this->config->get('megjelenit_regisztracio_telefon') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_telefon') == 1 && $ceges == 1 )) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
                <td><input type="text" name="telephone" value="<?php echo $telephone; ?>" />
                    <?php if ($error_telephone) { ?>
                        <span class="error"><?php echo $error_telephone; ?></span>
                    <?php } ?></td>
            </tr>
        <?php } ?>
        <?php if (($this->config->get('megjelenit_regisztracio_fax') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_fax') == 1 && $ceges == 1 )) {?>
            <tr>
                <td><?php echo $entry_fax; ?></td>
                <td><input type="text" name="fax" value="<?php echo $fax; ?>" /></td>
            </tr>
        <?php } ?>

        <?php if ( ($this->config->get('megjelenit_eletkor_regisztracio') == 1 && $ceges == 0) || ($this->config->get('megjelenit_eletkor_regisztracio_ceges') == 1 && $ceges == 1) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_eletkor; ?></td>
                <td><select name="eletkor" >
                        <option value=""><?php echo $text_select; ?></option>
                        <?php foreach ($eletkors as $eletkora) { ?>
                            <?php if ($eletkora['megnevezes'] == $eletkor) { ?>
                                <option value="<?php echo $eletkora['megnevezes']; ?>" selected="selected"><?php echo $eletkora['megnevezes']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $eletkora['megnevezes']; ?>"><?php echo $eletkora['megnevezes']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    <?php if ($error_eletkor) { ?>
                        <span class="error"><?php echo $error_eletkor; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>

        <?php if ( ($this->config->get('megjelenit_nem_regisztracio') == 1 && $ceges == 0 ) || ($this->config->get('megjelenit_nem_regisztracio_ceges') == 1 && $ceges == 1 ) ) {?>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_nem; ?></td>
                <td><select name="nem" >
                        <option value=""><?php echo $text_select; ?></option>
                        <?php foreach ($nems as $value) { ?>
                            <?php if ($value['ertek'] == $nem) { ?>
                                <option value="<?php echo $value['ertek']; ?>" selected="selected"><?php echo $value['nev']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $value['ertek']; ?>"><?php echo $value['nev']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    <?php if ($error_nem) { ?>
                        <span class="error"><?php echo $error_nem; ?></span>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <?php if (($this->config->get('megjelenit_regisztracio_iskolai_vegzettseg') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_iskolai_vegzettseg') == 1 && $ceges == 1 )) {?>
            <tr>
                <td><?php echo $entry_iskolai_vegzettseg; ?></td>
                <td><select name="iskolai_vegzettseg" >
                        <option value=""><?php echo $text_select; ?></option>
                        <?php foreach ($iskolai_vegzettsegek as $cur_iskolai_vegzettseg) { ?>
                            <?php if ($cur_iskolai_vegzettseg['megnevezes'] == $iskolai_vegzettseg) { ?>
                                <option value="<?php echo $cur_iskolai_vegzettseg['megnevezes']; ?>" selected="selected"><?php echo $cur_iskolai_vegzettseg['megnevezes']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $cur_iskolai_vegzettseg['megnevezes']; ?>"><?php echo $cur_iskolai_vegzettseg['megnevezes']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if ($this->config->get('megjelenit_regisztracio_weblap') == 1) {?>
            <tr>
                <td><?php echo $entry_weblap; ?></td>

                <td>
                    <input type="text" name="weblap" value="<?php echo !empty($weblap) ? $weblap : ''; ?>" />
                </td>
            </tr>
        <?php } ?>
    </table>
</div>