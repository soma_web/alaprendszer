<?php
// Heading
$_['heading_title']      = 'Általános termék integráció';

// Text
$_['text_module']        = 'Modulok';
$_['text_success']       = 'Sikeresen módosította az Általános termék integráció modult!';
$_['text_all']           = 'Összes';
$_['text_all_product']   = 'Összes termék engedélyezése';
$_['text_all_select_product'] = 'Összesen:';
$_['text_darab']         = 'db';
$_['text_magyarazat']    = 'Ha a "Leválogatott termék listája" <b style="font-size: 15px; padding-left: 5px ">üres</b>, akkor az <b style="font-size: 15px"> összes engedélyezett termék</b> feldolgozásra kerül!';
$_['text_description']   = 'Teljes leírás';
$_['text_short_description']    = 'Rövid leírás';
$_['text_url_elerhetosegek'] = '<b>Integrációs file-ok elérhetőségei:</b>';
$_['text_url_magyarazat']    = 'csv/arukeresok/';

$_['column_termek_integracio_id']   = 'Id';
$_['column_name']                   = 'Leválogatás negnevezése';
$_['column_status']                 = 'Státusz';
$_['column_action']                 = 'Művelet';
$_['column_connects']               = 'Hozzárendelve';


// Entry
$_['entry_status']          = '<b>Státusz:</b>';
$_['entry_product_status']  = 'Termék státusza:';
$_['entry_ar_tol']          = 'Ár tól-ig:';
$_['entry_levalogatott']    = 'Leválogatott termékek listája:';
$_['entry_hozzaad_neve']    = '+ Neve';
$_['entry_hozzaad_model']   = '+ Model';
$_['entry_levelogatott']    = '<b>Engedélyezett termékek</b>';
$_['entry_category']        = 'Kategóriák';
$_['entry_name']            = '<b>Leválogatás neve:</b>';
$_['entry_kapcsolodo']      = '<b>Kapcsolódó integrációk:</b>';
$_['entry_shipping']        = '<b>Kiszállítás:</b>';
$_['entry_shipping_time_in_stock']  = '<b>Szállítási idő nap:<span class="help">(ha van raktáron)</span></b>';
$_['entry_shipping_time_not_stock'] = '<b>Szállítási idő nap: <span class="help">(ha nincs raktáron)</span></b>';
$_['entry_warranty']        = '<b>Garancia hónap: <span class="help">(ha a terméknél nincs megadva)</span></b>';
$_['entry_text_pieces']     = '<b>Leírásban megjelenítő max. szavak száma:</b>';
$_['entry_description']     = '<b>Termékismertető típusa:</b>';
$_['entry_pickpack']        = '<b>A termékek "Pick Pack Pont"-on átvehetők</b>';
$_['entry_shipping_price']  = 'Szállítási díj: <span class="help">(ha a terméknél nincs megadva)</span>';

$_['button_levalogat']    = 'Leválogat';
$_['button_legyartas']    = 'Integrációs file-ok létrehozása';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az Általános termék integráció modul módosítása az Ön számára nem engedélyezett!';
?>