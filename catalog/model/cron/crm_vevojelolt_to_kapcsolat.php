<?php
class ModelCronCrmVevojeloltToKapcsolat extends Model {

    private $crm_id;
    private $contact_no;
    private $account_crm_id = 0;
    private $account_no     = '';
    private $potential_id = 0;
    private $potential_no = '';
    private $lead_id;
    private $quoteid = 0;
    private $quote_no = 0;

	public function getLeads() {
        $vissza = array();
        $sql = "CREATE TABLE if not exists vtiger_lead_to_contact (
        `leadid`	            int(19),
        `leadstatus`	        varchar(50) )
         engine=MyISAM default charset=UTF8";
        $query = $this->db_crm->query($sql);


        $sql = "SELECT * FROM vtiger_lead_to_contact";
        $query = $this->db_crm->query($sql);
        $vissza = $query->rows;

        return $vissza;
	}

    public function getMap() {
        $vissza = array();
        $sql = "SELECT * FROM vtiger_convertleadmapping ";
        $query = $this->db_crm->query($sql);
        foreach($query->rows as $key=>$value) {
            $sql = "SELECT * FROM vtiger_field WHERE fieldid='".$value['leadfid']."'";
            $field = $this->db_crm->query($sql);
            $vissza[$key]['lead'] = $field->row;

            $sql = "SELECT * FROM vtiger_field WHERE fieldid='".$value['contactfid']."'";
            $contact = $this->db_crm->query($sql);
            $vissza[$key]['contact'] = $contact->row;

            $sql = "SELECT * FROM vtiger_field WHERE fieldid='".$value['accountfid']."'";
            $account = $this->db_crm->query($sql);
            $vissza[$key]['account'] = $account->row;

            $sql = "SELECT * FROM vtiger_field WHERE fieldid='".$value['potentialfid']."'";
            $account = $this->db_crm->query($sql);
            $vissza[$key]['potential'] = $account->row;
        }

        return $vissza;
	}


    public function addContact($lead,$convert_map) {
        $lead_tables = array();
        $this->load->model('cron/altalanos');
        $this->lead_id = $lead['leadid'];


        $sql = "SELECT * FROM vtiger_leaddetails WHERE leadid ='" . $lead['leadid'] . "'";
        $query = $this->db_crm->query($sql);
        $lead_tables['vtiger_leaddetails'] = $query->row;

        if ($query->row) {

            $this->crm_id = $this->model_cron_altalanos->getCrmId();
            $this->contact_no = $this->model_cron_altalanos->getCrmNo('Contacts');


            $sql_entity     = " SELECT * FROM vtiger_crmentity WHERE crmid='".$lead['leadid']."'";
            $query_entity   = $this->db_crm->query($sql_entity);

            $sql_modbasic   = " SELECT * FROM vtiger_modtracker_basic WHERE crmid='".$lead['leadid']."'";
            $query_modbasic = $this->db_crm->query($sql_modbasic);



            // Cég létrehozása, ha még nincs - ha van, akkor account_crm_id beállítása
            if (!empty($lead_tables['vtiger_leaddetails']['company'])) {

                $con_sql = "SELECT a.accountid, a.account_no FROM vtiger_account a ";
                $con_sql .= " INNER JOIN vtiger_crmentity c ON (c.crmid=a.accountid) ";
                $con_sql .= " WHERE a.accountname LIKE '" .$lead_tables['vtiger_leaddetails']['company']. "'";
                $con_sql .= " AND c.deleted = 0";
                $con_query = $this->db_crm->query($con_sql);
                if ($con_query->row) {
                    $this->account_crm_id = $con_query->row['accountid'];
                    $this->account_no     = $con_query->row['account_no'];
                } else {
                    $this->account_crm_id = $this->model_cron_altalanos->getCrmId();
                    $this->account_no = $this->model_cron_altalanos->getCrmNo('Accounts');
                    $this->addNewAccount($query_entity->row, $query_modbasic->row);

                }
            }
            // Cég létrehozása, ha még nincs - ha van, akkor account_crm_id beállítása V É G E


            // új contact létrehozása
            $this->addNewContact($query_entity->row, $query_modbasic->row);
            // új contact létrehozása V É G E


            // Új Lehetőségek - árajánlat
            $issetPotential = $this->issetPotential($convert_map);
            if ($issetPotential) {
                $this->potential_id = $this->model_cron_altalanos->getCrmId();
                $this->potential_no = $this->model_cron_altalanos->getCrmNo('Potentials');

                $this->addNewPotential($query_entity->row, $query_modbasic->row,$lead_tables['vtiger_leaddetails']);

                $ajanlatok = $this->getAjanlatokFromWebshop();
                if ($ajanlatok) {
                    foreach ($ajanlatok as $ajanlat) {
                        $this->quoteid = $this->model_cron_altalanos->getCrmId();
                        $this->quote_no = $this->model_cron_altalanos->getCrmNo('Potentials');
                        $this->addPotentialArajanlat($query_entity->row, $query_modbasic->row, $ajanlat);
                    }
                }
            }
            // Lehetőségek - árajánlat V É G E


            foreach($convert_map as $map) {
                if ($map['lead']) {

                    $leadId_name        = $this->idName($map['lead']['tablename']);
                    $accountId_name     = !empty($map['account']['tablename']) ? $this->idName($map['account']['tablename']) : '';
                    $contactId_name     = !empty($map['contact']['tablename']) ? $this->idName($map['contact']['tablename']) : '';
                    $potentialId_name   = !empty($map['potential']['tablename']) ? $this->idName($map['potential']['tablename']) : '';

                    if (!isset($lead_tables[$map['lead']['tablename']])) {
                        $sql = "SELECT * FROM " . $map['lead']['tablename'] . " WHERE " . $leadId_name . "='" . $lead['leadid'] . "'";
                        $query = $this->db_crm->query($sql);
                        $lead_tables[$map['lead']['tablename']] = $query->row;
                    }

                    // Contact
                    if ($map['contact'] && $contactId_name) {
                        $sql = "SELECT " . $contactId_name . " FROM " . $map['contact']['tablename'] . " WHERE " . $contactId_name . "='" . $this->crm_id . "'";
                        $query = $this->db_crm->query($sql);
                        if ($query->row) {
                            $sql = "UPDATE " . $map['contact']['tablename'] . " SET " . $map['contact']['columnname'] . "= '" . $lead_tables[$map['lead']['tablename']][$map['lead']['columnname']] . "'";
                            $sql .= " WHERE " . $contactId_name . "='" . $this->crm_id . "'";
                            $siker = $this->db_crm->query($sql);

                        } else {
                            $sql = "INSERT INTO " . $map['contact']['tablename'] . " SET ";
                            $sql .= $contactId_name . "='" . $this->crm_id . "', ";
                            $sql .= $map['contact']['columnname'] . "= '" . $lead_tables[$map['lead']['tablename']][$map['lead']['columnname']] . "'";
                            $siker = $this->db_crm->query($sql);
                        }
                    }


                    // Account
                    if ($this->account_crm_id && !empty($map['account']['columnname']) && $accountId_name) {
                        $sql = "SELECT " . $accountId_name . " FROM " . $map['account']['tablename'] . " WHERE " . $accountId_name . "='" . $this->account_crm_id . "'";
                        $query = $this->db_crm->query($sql);

                        if ($query->row) {
                            $sql = "UPDATE " . $map['account']['tablename'] . " SET " . $map['account']['columnname'] . "= '" . $lead_tables[$map['lead']['tablename']][$map['lead']['columnname']] . "'";
                            $sql .= " WHERE " . $accountId_name . "='" . $this->account_crm_id . "'";
                            $sql .= " AND ".$map['account']['columnname']." IS NULL OR LENGTH(".$map['account']['columnname'].")=0;";
                            $siker = $this->db_crm->query($sql);


                        } else {
                            $sql = "INSERT INTO " . $map['account']['tablename'] . " SET ";
                            $sql .= $accountId_name . "='" . $this->account_crm_id . "', ";
                            $sql .= $map['account']['columnname'] . "= '" . $lead_tables[$map['lead']['tablename']][$map['lead']['columnname']] . "'";
                            $siker = $this->db_crm->query($sql);
                        }
                    }

                    // Potential
                    if ($issetPotential && $potentialId_name) {
                        $sql = "SELECT " . $potentialId_name . " FROM " . $map['potential']['tablename'] . " WHERE " . $potentialId_name . "='" . $this->potential_id . "'";
                        $query = $this->db_crm->query($sql);
                        if ($query->row) {
                            if (!empty($lead_tables[$map['lead']['tablename']][$map['lead']['columnname']])) {
                                $sql = "UPDATE " . $map['potential']['tablename'] . " SET " . $map['potential']['columnname'] . "= '" . $lead_tables[$map['lead']['tablename']][$map['lead']['columnname']] . "'";
                                $sql .= " WHERE " . $potentialId_name . "='" . $this->potential_id . "'";
                                $siker = $this->db_crm->query($sql);
                            }

                        } else {
                            $sql = "INSERT INTO " . $map['potential']['tablename'] . " SET ";
                            $sql .= $potentialId_name . "='" . $this->potential_id . "', ";
                            $sql .= $map['potential']['columnname'] . "= '" . $lead_tables[$map['lead']['tablename']][$map['lead']['columnname']] . "'";
                            $siker = $this->db_crm->query($sql);
                        }
                    }


                }
            }
        }


        return true;
    }

    private function addNewContact($entity,$modbasic) {

        $sql = "INSERT INTO vtiger_contactdetails SET
                    contactid = '" . $this->crm_id . "',
                    contact_no='".$this->contact_no."',
                    accountid='".$this->account_crm_id."'";
        $siker = $this->db_crm->query($sql);


        $data['crmid']      = $this->crm_id;
        $data['module']     = 'Contacts';
        $data['crmentity']  = $entity;
        $data['modbasic']   = $modbasic;

        $this->model_cron_altalanos->addEntity($data);
        $this->addKerdoiv();
        $this->addEmails();

    }

    private function addNewAccount($entity,$modbasic) {

        $sql = "INSERT INTO vtiger_account SET
                    accountid   = '" . $this->account_crm_id . "',
                    account_no  = '".$this->account_no."'";
        $siker = $this->db_crm->query($sql);

        $data['crmid']      = $this->account_crm_id;
        $data['module']     = 'Accounts';
        $data['crmentity']  = $entity;
        $data['modbasic']   = $modbasic;

        $this->model_cron_altalanos->addEntity($data);

    }



    private function addNewPotential($entity,$modbasic,$leaddetails)
    {

        $sql = "INSERT INTO vtiger_potential SET
                    potentialid     = '" . $this->potential_id . "',
                    potential_no    = '" . $this->potential_no . "',
                    contact_id      = '" . $this->crm_id . "',
                    sales_stage     = 'Létrehozva',
                    potentialname   = '" . (trim($leaddetails['lastname'] . ' ' . $leaddetails['firstname'])) . "',
                    related_to      = '" . $this->account_crm_id . "'";
        $siker = $this->db_crm->query($sql);





        $data['crmid']      = $this->potential_id;
        $data['module']     = 'Potentials';
        $data['crmentity']  = $entity;
        $data['modbasic']   = $modbasic;

        $this->model_cron_altalanos->addEntity($data);

        $sql = "SELECT * FROM vtiger_contpotentialrel WHERE
                contactid   = '" . $this->crm_id . "' AND
                potentialid = '" . $this->potential_id . "'";
        $query = $this->db_crm->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO vtiger_contpotentialrel SET
                    contactid   = '" . $this->crm_id . "',
                    potentialid = '" . $this->potential_id . "'";
            $siker = $this->db_crm->query($sql);
        }


        $this->addDocuments();
        $this->addCampaign();
        $this->addComments();
        $this->addCalendar();
    }

    private function getAjanlatokFromWebshop() {
        $vissza = array();

        $sql = "SELECT cf_759 AS kilowatt FROM vtiger_leadscf WHERE leadid = '".$this->lead_id."'";
        $query = $this->db_crm->query($sql);
        if ($query->row) {

            $sql = "SELECT pricebookid FROM vtiger_pricebookcf p ";
            $sql .=" INNER JOIN vtiger_crmentity c ON (p.pricebookid = c.crmid) ";
            $sql .=" WHERE cf_2445 <= '".$query->row['kilowatt']."' ";
            $sql .=" AND cf_2447 >= '".$query->row['kilowatt']."' ";
            $sql .=" AND c.deleted=0 ";
            $sql .=" LIMIT 0,3 ";
            $query = $this->db_crm->query($sql);
            if ($query->rows) {
                $this->load->model("cron/altalanos");
                foreach($query->rows as $value) {
                    $this->model_cron_altalanos->addFuttatasEredmeny('Ajálott csomag crmid: ' . $value['pricebookid']);
                }

                foreach($query->rows as $csomag) {
                    $sql = "SELECT p.*, pd.name FROM ".DB_PREFIX."product p ";
                    $sql .= " LEFT JOIN ".DB_PREFIX."product_description pd ON (p.product_id=pd.product_id) ";
                    $sql .= "  WHERE   p.crm_product_id = '".$csomag['pricebookid']."'";
                    $sql .= "  AND   pd.language_id='".(int)$this->config->get('config_language_id')."'";

                    $query_webshop_csomag = $this->db->query($sql);
                    echo '';
                    if ($query_webshop_csomag->row) {
                        $this->model_cron_altalanos->addFuttatasEredmeny('Csomag neve: ' . $query_webshop_csomag->row['name']);
                    } else {
                        $this->model_cron_altalanos->addFuttatasEredmeny('Lekérési hiba (318.sor): ' . $sql);

                    }

                    if ($query_webshop_csomag->row) {
                        $adokulcs = 0;
                        $tax_rates = $this->tax->getRates(0, $query_webshop_csomag->row['tax_class_id']);
                        foreach($tax_rates as $tax_rate) {
                            $adokulcs = (float)$tax_rate['rate'];
                            break;
                        }

                        $vissza[$query_webshop_csomag->row['product_id']]['csomag']['csomag_neve'] = $query_webshop_csomag->row['name'];
                        $vissza[$query_webshop_csomag->row['product_id']]['csomag']['adokulcs'] = $adokulcs;



                        $sql = "SELECT * FROM ".DB_PREFIX."product_package WHERE product_id = '".$query_webshop_csomag->row['product_id']."'";
                        $query_webshop_csomag_products = $this->db->query($sql);
                        if ($query_webshop_csomag_products->rows) {

                            $vissza[$query_webshop_csomag->row['product_id']]['csomag']['subtotal'] = 0;;
                            $vissza[$query_webshop_csomag->row['product_id']]['csomag']['total'] = 0;;

                            foreach($query_webshop_csomag_products->rows as $product) {

                                $sql = "SELECT p.*, pd.name FROM ".DB_PREFIX."product p
                                        LEFT JOIN ".DB_PREFIX."product_description pd ON (p.product_id=pd.product_id)
                                        WHERE   p.product_id = '".$product['package_id']."'
                                            AND pd.language_id='".(int)$this->config->get('config_language_id')."'";
                                $query_product = $this->db->query($sql);
                                if ($query_product->row) {
                                    $this->model_cron_altalanos->addFuttatasEredmeny('Csomag tartalma (product_id-termék neve): ' . $product['package_id'].' <b>'.$query_product->row['name'].'</b>');

                                    $query_product->row['quantity'] = $product['quantity'];
                                    $query_product->row['price'] = $product['price'];

                                    $query_product->row['tax_rate'] = 0;
                                    $query_product->row['tax_amount'] = 0;

                                    if ($query_webshop_csomag->row['tax_class_id']) {
                                        $tax_rates = $this->tax->getRates((float)$product['price']*(float)$product['quantity'], $query_webshop_csomag->row['tax_class_id']);
                                        foreach($tax_rates as $tax_rate) {
                                            $query_product->row['tax_rate'] = (float)$tax_rate['rate'];
                                            $query_product->row['tax_amount'] = $tax_rate['amount'];
                                            break;
                                        }
                                    }
                                    $vissza[$query_webshop_csomag->row['product_id']]['products'][] = $query_product->row;
                                    $vissza[$query_webshop_csomag->row['product_id']]['csomag']['subtotal'] += (int)$product['price']*(int)$product['quantity'];
                                    $vissza[$query_webshop_csomag->row['product_id']]['csomag']['total'] += (int)$product['price']*(int)$product['quantity']+$query_product->row['tax_amount'];
                                    echo '';



                                }

                            }
                        }
                    }
                }
            }
        }

        return $vissza;
    }

    private function addPotentialArajanlat($entity, $modbasic, $ajanlat) {

        $data['crmid']      = $this->quoteid;
        $data['module']     = 'Quotes';
        $data['crmentity']  = $entity;
        $data['crmentity']['label']  = $ajanlat['csomag']['csomag_neve'];

        $data['modbasic']   = $modbasic;

        $this->model_cron_altalanos->addEntity($data);

        $sql = "INSERT INTO vtiger_quotes SET
                    quoteid             = '" . $this->quoteid . "',
                    subject             = '".$ajanlat['csomag']['csomag_neve']."',
                    potentialid         = '".$this->potential_id."',
                    quotestage          = 'Created',
                    validtill           = CURDATE() + INTERVAL 60 DAY,
                    contactid           = '".$this->crm_id."',
                    quote_no            = '".$this->quote_no."',
                    subtotal            = '".$ajanlat['csomag']['subtotal']."',
                    carrier             = '',
                    shipping            = '',
                    inventorymanager    = 0,
                    `type`              = '',
                    adjustment          = 0,
                    total               = '".$ajanlat['csomag']['total']."',
                    taxtype             = 'group',
                    discount_percent    = 0,
                    discount_amount     = 0,
                    s_h_amount          = 0,
                    accountid           = '".$this->account_crm_id."',
                    terms_conditions    = '',
                    currency_id         = 1,
                    conversion_rate     = 1,
                    pre_tax_total       = '".$ajanlat['csomag']['subtotal']."',
                    s_h_percent         = 0";

        $siker = $this->db_crm->query($sql);

        $bill_city      = '';
        $bill_code      = '';
        $bill_country   = '';
        $bill_state     = '';
        $bill_street    = '';
        $bill_pobox     = '';

        $sql = "SELECT * FROM vtiger_accountbillads WHERE accountaddressid = '".$this->account_crm_id."'";
        $query = $this->db_crm->query($sql);
        if ($query->row) {
            $bill_city      = $query->row['bill_city'];
            $bill_code      = $query->row['bill_code'];
            $bill_country   = $query->row['bill_country'];
            $bill_state     = $query->row['bill_state'];
            $bill_street    = $query->row['bill_street'];
            $bill_pobox     = $query->row['bill_pobox'];
        }

        $sql = "SELECT * FROM vtiger_leadaddress WHERE leadaddressid = '".$this->lead_id."'";
        $query = $this->db_crm->query($sql);
        if ($query->row) {
            $bill_city      = empty($bill_city) ? $query->row['city'] : $bill_city;
            $bill_code      = empty($bill_code) ? $query->row['code'] : $bill_code;
            $bill_country   = empty($bill_country) ? $query->row['country'] : $bill_country;
            $bill_state     = empty($bill_state) ? $query->row['state'] : $bill_state;
            $bill_street    = empty($bill_street) ? $query->row['lane'] : $bill_street;
            $bill_pobox     = empty($bill_pobox) ? $query->row['pobox'] : $bill_pobox;
        }

        $sql = "INSERT INTO vtiger_quotesbillads SET
                    quotebilladdressid  = '" . $this->quoteid . "',
                    bill_city           = '" .$bill_city. "',
                    bill_code           = '" .$bill_code. "',
                    bill_country        = '" .$bill_country. "',
                    bill_state          = '" .$bill_state. "',
                    bill_street         = '" .$bill_street. "',
                    bill_pobox          = '" .$bill_pobox. "'";
        $siker = $this->db_crm->query($sql);


        $sql = "INSERT INTO vtiger_quotescf SET
                    quoteid             = '" . $this->quoteid . "'";
        $siker = $this->db_crm->query($sql);

        $sql = "INSERT INTO vtiger_quotesshipads SET
                    quoteshipaddressid  = '" . $this->quoteid . "',
                    ship_city           = '',
                    ship_code           = '',
                    ship_country        = '',
                    ship_state          = '',
                    ship_street         = '',
                    ship_pobox          = ''";
        $siker = $this->db_crm->query($sql);

        $this->addProducts($ajanlat);


    }

    private function addProducts($ajanlat) {
        if ($ajanlat['products']) {
            foreach($ajanlat['products'] as $key=>$product) {
                    $lineitem_id = $this->model_cron_altalanos->getLineItemId();
                    $sql = "INSERT INTO vtiger_inventoryproductrel SET
                        id                = '".$this->quoteid."',
                        productid         = '".$product['crm_product_id']."',
                        sequence_no       = '".($key+1)."',
                        quantity          = '".$product['quantity']."',
                        listprice         = '".$product['price']."',
                        discount_percent  = '',
                        discount_amount   = '',
                        comment           = '',
                        description	      = '',
                        incrementondel    = '0',
                        lineitem_id       = '".$lineitem_id."',
                        tax1              = '',
                        tax2              = '',
                        tax3              = '',
                        tax4              = '',
                        tax5              = '".$ajanlat['csomag']['adokulcs']."'";
                    $siker = $this->db_crm->query($sql);

            }
        }

    }


    private function issetPotential($table=array()) {
        $vissza = false;
        foreach($table as $value) {
            if ($value['potential']) {
                $vissza = true;
                break;
            }
        }
        return $vissza;
    }

    private function idName($tablename) {

        if ($tablename == "vtiger_leaddetails") {
            $id_name = 'leadid';

        } elseif ($tablename == "vtiger_leadscf") {
            $id_name = 'leadid';

        } elseif ($tablename == "vtiger_contactdetails") {
            $id_name = 'contactid';

        } elseif ($tablename == "vtiger_contactsubdetails") {
            $id_name = 'contactsubscriptionid';

        } elseif ($tablename == "vtiger_contactscf") {
            $id_name = 'contactid';

        } elseif ($tablename == "vtiger_accountbillads") {
            $id_name = 'accountaddressid';

        } elseif ($tablename == "vtiger_accountshipads") {
            $id_name = 'accountaddressid';

        } elseif ($tablename == "vtiger_accountscf") {
            $id_name = 'accountid';

        } elseif ($tablename == "vtiger_potential") {
            $id_name = 'potentialid';

        } elseif ($tablename == "vtiger_potentialscf") {
                $id_name = 'potentialid';

        } elseif ($tablename == "vtiger_crmentity") {
            $id_name = 'crmid';

        } else {
            $id_name = str_replace('vtiger_', '', $tablename) . 'id';
        }

        return $id_name;

    }



    private function addEmails() {
        $sql = "SELECT emailid, idlists FROM `vtiger_emaildetails` WHERE `idlists` LIKE '%".$this->lead_id."@%'";
        $query = $this->db_crm->query($sql);
        if ($query->rows) {
            foreach($query->rows as $value) {
                $uj_ertek = str_replace($this->lead_id,$this->crm_id,$value['idlists']);
                $sql = "UPDATE vtiger_emaildetails SET idlists='".$uj_ertek."' WHERE emailid='".$value['emailid']."'";
                $query = $this->db_crm->query($sql);

                $sql = "UPDATE vtiger_seactivityrel SET crmid='".$this->crm_id."' WHERE crmid='".$this->lead_id."' AND activityid='".$value['emailid']."'";
                $siker = $this->db_crm->query($sql);
            }
        }
    }

    private function addKerdoiv() {
        $sql = "UPDATE vtiger_kerdoivek SET kerdoivek_tks_kapcsolodik = '".$this->crm_id."' WHERE kerdoivek_tks_kapcsolodik = '".$this->lead_id."'";
        $siker = $this->db_crm->query($sql);

        $sql = "UPDATE vtiger_crmentityrel
                      SET
                        `module`  = 'Contacts',
                        `crmid`   = '".$this->crm_id."'
                      WHERE
                        crmid     ='".$this->lead_id."' AND
                        relmodule = 'Kerdoivek'";
        $siker = $this->db_crm->query($sql);
    }

    private function addDocuments() {
        $sql = "UPDATE vtiger_senotesrel SET crmid = '".$this->potential_id."' WHERE  crmid = '".$this->lead_id."'";
        $siker = $this->db_crm->query($sql);

    }

    private function addCampaign() {
        $sql = "SELECT campaignid FROM vtiger_campaignleadrel WHERE  leadid = '".$this->lead_id."' LIMIT 0,1";
        $query = $this->db_crm->query($sql);
        if ($query->row) {
            $sql = "UPDATE vtiger_potential SET campaignid='".$query->row['campaignid']."' WHERE potentialid = '".$this->potential_id."'";
            $siker = $this->db_crm->query($sql);
        }
    }


    private function addComments() {
        $sql = "UPDATE vtiger_modcomments SET related_to = '".$this->potential_id."' WHERE  related_to = '".$this->lead_id."'";
        $siker = $this->db_crm->query($sql);
    }

    private function addCalendar() {
        $sql = "UPDATE vtiger_crmentityrel SET
                      crmid     = '".$this->potential_id."',
                        `module`  = 'Potentials'
                    WHERE  crmid = '".$this->lead_id."'";
        $siker = $this->db_crm->query($sql);

        $sql = "UPDATE vtiger_seactivityrel SET
                      crmid     = '".$this->potential_id."'
                    WHERE  crmid = '".$this->lead_id."'";
        $siker = $this->db_crm->query($sql);

    }

    public function deleteTriggerLead($data) {
        $sql = "DELETE FROM vtiger_lead_to_contact
            WHERE
                leadid      = '".$data['leadid']."' AND
                leadstatus LIKE '".$data['leadstatus']."'";
        $query = $this->db_crm->query($sql);
    }



}
?>