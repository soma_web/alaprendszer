<?php
$_['width']          = array(
    'type'              => 'text',
    'name'              => 'width',
    'default'           => "10",
    'size'              => "4",
    'heading'           => 'Width:'
);

$_['height']          = array(
    'type'              => 'text',
    'name'              => 'height',
    'default'           => "14",
    'size'              => "4",
    'heading'           => 'Height:'
);
?>