<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Text
$_['text_subscribe_success']	= 'Sikeres feliratkozás!!';
$_['text_unsubscribe_success']	= 'Sikeres leiratkozás!';
$_['text_subscribe_confirmation'] = 'Küldtünk Önnek egy megerősítő email-t a megrendelésről.';

?>
