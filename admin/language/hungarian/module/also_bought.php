<?php
/*
+--------------------------------------------------------+
|                   ALSO BOUGHT MODULE					 |
|            Copyright to: www.oc-extensions.com 		 |
|														 |		
|     Become a premium member and you can download all   |
|				the modules with only $99           	 |
+--------------------------------------------------------+
*/

// Heading
$_['heading_title']       = 'Mit vettek még, akik ezt vették';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeresen módosította a Mit vettek még, akik ezt vették modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_sale']           = 'Eladás';

$_['text_heading_title']  = 'Mit vettek még, akik ezt vették';
$_['text_delete_cache']   = 'Törlés \'Mit vettek még, akik ezt vették cache\' ';
$_['text_hours']          = 'óra múlva';


// Entry
$_['entry_limit']         = 'Korlát:';
$_['entry_image']         = 'Kép (Sz x M):';
$_['entry_sort']          = 'Sorrend';
$_['entry_layout']        = 'Megjelenítés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_order_status']  = 'Rendelés állapota';
$_['entry_order_status_operand'] = 'Típus';
$_['entry_show_add_to_cart_button'] = 'Kosárba gomb <br /> megjelenítése';

$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';

// Error
$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága módosítani a Mit vettek még, akik ezt vették modult!';
$_['error_image']         = 'Meg kell adni a kép magasságát és szélességét!';
$_['error_dc_period']     = 'Kérem állítsa be törlésre \'Mit vettek még, akik ezt vették\' cache-t.';
?>