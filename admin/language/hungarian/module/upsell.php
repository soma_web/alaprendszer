<?php

// Heading
$_['heading_title']    = 'Vásárlás ösztönző (Upsell) modul';

// Text
$_['text_module']                       = 'Modulok';
$_['text_success']                      = 'Sikeresen módosította a vásárlás ösztönző modult!';
$_['text_szinten_vasarolt']             = 'Szintén vásárolt termékek';
$_['text_kapcsolodo']                   = 'Kapcsolódó termékek';
$_['text_tartozekok']                   = 'Általános tartozékok';
$_['text_kiemelt']                      = 'Kiemelt termékek';
$_['text_bestseller']                   = 'Leggyakrabban vásárolt';
$_['text_latest']                       = 'Legújabb termékek';
$_['text_auto']                         = 'Automatikus';
$_['text_kosar_nem_latszik']            = 'Kosárból semmi nem látszik:';
$_['text_kosarba_tett_termek_latszik']  = 'Aktuális kosárba  tett termék látszik:';
$_['text_kosar_tartalma_latszik']       = 'Teljes kosár tartalma látszik:';


// Entry
$_['entry_status']              = 'Állapot:';
$_['entry_fejlec_latszik']      = 'Fejléc megjelenítése:';
$_['entry_megfelelo_termekek']  = 'Termék csoportok kiválasztása:';
$_['entry_automata_bellitas']   = 'Automatikus kiválasztás sorrend:';
$_['entry_max_termek']          = 'Maximum megjeleníthető termék:';
$_['entry_design_beallitas']    = 'Design beállítások:';
$_['entry_bought_relacio']      = 'Relációs jel:';
$_['entry_bought_order_status'] = 'Rendelés státusza:';

// Error
$_['error_permission'] = 'Figyelmeztetés: A vásárlás ösztönző modul módosítása az Ön számára nem engedélyezett!';
?>
