<div class="osszehasonlit_kivansag">
    <span class="compare">
        <a class="hasonlit" onclick="addToCompare('<?php echo $product_id; ?>');">
            <?php if(isset($beallitas_termek_aloldal['compare_icon']) && $beallitas_termek_aloldal['compare_icon']) { ?>
                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/compare_icon.png')) {
                    $icon = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/compare_icon.png';
                } else {
                    $icon = DIR_TEMPLATE_IMAGE.'default/image/compare_icon.png';
                } ?>
                <img class="product-icon-compare" title="<? if (isset($button_compare)) { echo $button_compare; }?>"  src="<? echo $icon?>" >
            <?php } ?>
            <?php if(isset($beallitas_termek_aloldal['compare_text']) && $beallitas_termek_aloldal['compare_text']) { ?>
                <span><?php echo $button_compare; ?></span>
            <?php } ?>
        </a>
    </span>
</div>