<?php
class ControllerAccountKuponfile extends Controller {
    public function index() {

        $json = array();

        header("Content-Type: image/jpg");

        if ($_POST['szazalek'] = 1) {
            $penznem = "%";
        } else {
            $penznem = $this->currency->getFizetoeszkoz();
        }


        $this->language->load('product/product');
        $kaphato = $this->language->get('text_kaphato');

        $src = imagecreatefromjpeg($_REQUEST['image']);

        $src_width  = $this->config->get('config_image_product_width');
        $src_height = $this->config->get('config_image_product_height');

        $szoveg = str_replace("&lt;","",$_POST['szoveg']);
        $szoveg = str_replace("/p&gt;","",$szoveg);
        $szoveg = str_replace("p&gt;","",$szoveg);
        $szovegek = explode(" ",$szoveg);

        $sorkiir = "";
        $i = 1;
        foreach ($szovegek as $szoveg) {
            $szoveg = substr($szoveg,0,50);
            if ( (strlen($szoveg)+strlen($sorkiir)) > 60 ) {
                $sorkiir = "";
                $i++;
            }
            $sorkiir .= $szoveg . " ";
        }



        $im = @imagecreate(500, $src_height+(30*$i)+20);
        $background_color = imagecolorallocate($im, 250, 250, 250);
        $text_color = imagecolorallocate($im, 0, 0, 0);
        $text_color1 = imagecolorallocate($im, 90, 90, 90);


        $font = DIR_TEMPLATE.'default/stylesheet/impact.ttf';
        imagettftext($im, 20, 0, $src_width+10, 30, $text_color1, $font, $_POST['name']);
        imagettftext($im, 20, 0, $src_width+40, 70, $text_color, $font, "-". $_POST['price'].$penznem);
        if (!empty($_POST['date_ervenyes_ig'])) {
            imagettftext($im, 12, 0, $src_width+10, 100, $text_color1, $font, $kaphato." " .$_POST['date_ervenyes_ig']."-ig");
        }
        imagettftext($im, 13, 0, $src_width+10, 140, $text_color1, $font, $_POST['upc']);


        $szoveg = str_replace("&lt;","",$_POST['szoveg']);
        $szoveg = str_replace("/p&gt;","",$szoveg);
        $szoveg = str_replace("p&gt;","",$szoveg);

        $szovegek = explode(" ",$szoveg);

        $sorkiir = "";
        $i = 1;
        foreach ($szovegek as $szoveg) {
            $szoveg = substr($szoveg,0,50);
            if ( (strlen($szoveg)+strlen($sorkiir)) > 62 ) {
                imagettftext($im, 12, 0, 0, $src_height+(30*$i), $text_color1, $font,$sorkiir);
                $sorkiir = "";
                $i++;
            }
            $sorkiir .= $szoveg . " ";
        }
        imagettftext($im, 12, 0, 0, $src_height+(30*$i), $text_color1, $font,$sorkiir);


        while (true) {
            $kimehet = true;
            $name=mt_rand(3000,300000000);
            $utvonal= DIR_IMAGE."data/".$this->session->data['customer_id']."/a".$name.".jpg";
            $utvonal_mentes= HTTPS_IMAGE."data/".$this->session->data['customer_id']."/a".$name.".jpg";
            $utvonal_mentes2= "data/".$this->session->data['customer_id']."/a".$name.".jpg";

            $files = glob(DIR_IMAGE."data/".$this->session->data['customer_id']."/*.jpg");

            foreach ($files as $file) {
                if ($file == $utvonal) {
                    $kimehet = false;
                    break;
                }
            }
            if ($kimehet) {
                break;
            }
        }

        $no_kep = false;
        $kep_name = basename($_REQUEST['image']);
        if (strpos($kep_name,"no_image") !== false ) {
            $no_kep = true;
        }

       if ($_REQUEST['kep_letiltva'] != 1) {
           if ($no_kep) {
               $megjelenit_altalanos = $this->config->get('megjelenit_altalanos');
               if ($megjelenit_altalanos['no_kep_tiltas'] != 1) {
                   imagecopymerge($im, $src, 0, 0, 0, 0, $src_width, $src_height, 100);
               }
           } else {
               imagecopymerge($im, $src, 0, 0, 0, 0, $src_width, $src_height, 100);
           }
       }



        imagejpeg($im,$utvonal);

        $this->load->model('tool/image');
        $json['utvonal_cache'] = $this->model_tool_image->resize($utvonal_mentes2, $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));

        $json['utvonal'] = $utvonal;
        $json['utvonal_mentes'] = $utvonal_mentes;
        $json['utvonal_mentes2'] = $utvonal_mentes2;

        $this->response->setOutput(json_encode($json));

        }
    }
?>