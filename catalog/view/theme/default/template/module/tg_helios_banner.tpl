<div class="banner-name"><?php echo $heading_title; ?></div>

<div id="carousel-banner">
			
<ul class="jcarousel33-skin-opencart">
	
  <?php foreach ($banners as $banner) { ?>
  
  <?php if ($banner['link']) { ?>
  
  <li><a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /><h3><?php echo $banner['title']; ?></h3></a></li>
  
  <?php } else { ?>
					<li><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /><h3><?php echo $banner['title']; ?></h3></li>
				<?php } ?>
  <?php } ?>
 
  </ul>
				</div>




<script type="text/javascript"><!--
$('#carousel-banner ul').jcarousel33({
	vertical: false,
	visible: 4,
	scroll: 3
});
//--></script>
