<?php

// Text
$_['text_title']   	= 'Postai (rózsaszín) pénzesutalvány';
$_['text_instruction'] 	= 'Csekk rendelés';
$_['text_payable'] 	= 'Kedvezményezett: ';
$_['text_address'] 	= 'Cím: ';
$_['text_payment'] 	= 'Rendelését az összeg jóváírása után teljesítjük.';
?>
