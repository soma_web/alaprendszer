<?php
// Text
$_['text_refine']       = 'Refine Search';
$_['text_product']      = 'Products';
$_['text_error']        = 'No results found!';
$_['text_empty']        = 'There are no products in this category';
$_['text_quantity']     = 'Quant.:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Product code:';
$_['text_points']       = 'Reward points:';
$_['text_price']        = 'Price:';
$_['text_tax']          = 'Netto:';
$_['text_reviews']      = '%s based on reviews.';
$_['text_compare']      = 'Compare this product (%s)';
$_['text_display']      = 'View:';
$_['text_list']         = 'List';
$_['text_grid']         = 'Grid';
$_['text_sort']         = 'Order:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Price (Low &gt; High)';
$_['text_price_desc']   = 'Price (High &gt; Low)';
$_['text_rating_asc']   = 'Evaluation (worst)';
$_['text_rating_desc']  = 'Evaluation (best)';
$_['text_model_asc']    = 'Modell (A - Z)';
$_['text_model_desc']   = 'Modell (Z - A)';
$_['text_limit']        = 'Show:';
$_['text_stock_in']     = 'Estimated arrival:';

$_['text_raktar']       = 'Per-store inventory:';
$_['text_garancia']= 'warranty';
$_['text_szallitas']= 'Shipping cost:';
$_['entry_garancia_honap']      = 'months';
$_['entry_garancia_ev']         = 'year';
$_['text_kifuto_title']       = 'End-product';
$_['text_mennyisegi_title']   = 'Discount';

$_['text_akcio_title']        = 'Special product';
$_['text_pardarab_title']     = 'Few pieces';
$_['text_uj_title']           = 'New product';
$_['text_ujdonsag_title']     = 'Newness';

// Ikonok szöveges (iconok_szoveges.tpl)
$_['text_uj']                = 'New';
$_['text_akcios']            = 'On Sale';
$_['text_pardarab']          = 'Few Pieces';
$_['text_mennyisegi']        = 'Volume Discount';
// Ikonok szöveges vége

?>
