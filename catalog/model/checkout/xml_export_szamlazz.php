<?php
class ModelCheckoutXmlExportSzamlazz extends Model {

    public function szamlazzellenor($order_id,$settings){
        $sql = "SELECT * FROM ".DB_PREFIX."szamlazzhu WHERE order_id='".$order_id."'";
        $query = $this->db->query($sql);

        if($query->num_rows > 0){
            $this->update_szamlazz($order_id,$settings) ;
        }
        else{
            $this->insert_szamlazz($order_id,$settings);
        }
    }

    public function update_szamlazz($order_id,$settings){
        $sql = "UPDATE ".DB_PREFIX."szamlazzhu SET ";
        $sql .="order_id                ='".$order_id."',
                eloleg_szamla           = '".$settings['eloleg_szamla']."',
                vegszamla               = '".$settings['vegszamla']."',
                dijbekero               = '".$settings['dijbekero']."',
                fizetve                 = '".$settings['fizetve']."',

                kelt_datum              = CURDATE(),
                teljesites_datum        = CURDATE(),
                fizetesi_hatarido_datum = CURDATE() + INTERVAL 8 DAY,

                fizetesi_mod            = '".$_SESSION['payment_method']['title']."',
                penznem                 = '".$_SESSION['currency']."',
                peldany_szam            = '".$settings['peldany_szam']."',
                szamla_nyelve           = '".$this->config->get('config_admin_language')."',
                arfolyam_bank           = 'Magyar Nemzeti Bank(MNB)',
                arfolyam                = '1',
                szamlaszam_elotag       = '".$settings['szamlazz_hu_elotag']."',
                megjegyzes              = '".$settings['megjegyzes']."'";


        $sql .= " WHERE order_id='".$order_id."'";
        $siker = $this->db->query($sql);

        return true;
    }

    public function insert_szamlazz($order_id,$settings){
        $sql = "INSERT INTO ".DB_PREFIX."szamlazzhu SET ";
        $sql .= "order_id               = '".$order_id."',
                eloleg_szamla           = '".$settings['eloleg_szamla']."',
                vegszamla               = '".$settings['vegszamla']."',
                dijbekero               = '".$settings['dijbekero']."',
                fizetve                 = '".$settings['fizetve']."',
                kelt_datum              = CURDATE(),
                teljesites_datum        = CURDATE(),
                fizetesi_hatarido_datum = CURDATE() + INTERVAL 8 DAY,
                fizetesi_mod            = '".$_SESSION['payment_method']['title']."',
                penznem                 = '".$_SESSION['currency']."',
                peldany_szam            = '".$settings['peldany_szam']."',
                szamla_nyelve           = '".$this->config->get('config_admin_language')."',
                arfolyam_bank           = 'Magyar Nemzeti Bank(MNB)',
                arfolyam                = '1',
                szamlaszam_elotag       = '".$settings['szamlazz_hu_elotag']."',
                megjegyzes              = '".$settings['megjegyzes']."'";


        $siker = $this->db->query($sql);

        return true;
    }

    public function getOrderProducts($order_id) {
        $query = $this->db->query("SELECT op.*, p.utalvany AS ingyenes, p.megyseg FROM " . DB_PREFIX . "order_product op
            LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = op.product_id)
		    WHERE order_id = '" . (int)$order_id . "'");
        return $query->rows;
    }


}
?>