<?php
// Heading
$_['heading_title']   = 'Visited Products';


// Text
$_['text_account']    = 'Account';
$_['text_home']       = 'Shop';
$_['button_reset']    = 'Restore';
$_['column_name']     = 'Product name';
$_['column_model']    = 'Modell';
$_['column_viewed']   = 'Visited';
$_['column_percent']  = 'Percent';
$_['text_no_results']  = 'No viewed products';


?>