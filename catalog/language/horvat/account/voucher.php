<?php
// Heading
$_['heading_title']    = 'Vásároljon Ajándékutalványt';

// Text
$_['text_account']     = 'Fiók';
$_['text_voucher']     = 'Ajándékutalvány';
$_['text_description'] = 'Ez az ajándékutalvány el lesz küldve e-mailben a címzettnek miután a rendelése ki lett fizetve.';
$_['text_agree']       = 'Elfogadom, hogy az ajándékutalványok nem visszatéríthetőek.';
$_['text_message']     = '<p>Köszönöm, hogy ajándékutalványt vásárolt! Miután befejezte rendelését, az Ön ajándékutalvány címzettjének egy e-mail lesz küldve, annak részleteivel, hogy hogyan tudja beváltani az ajándékutalványt.</p>';
$_['text_for']         = '%s ajándékutalvány neki: %s';

// Entry
$_['entry_to_name']    = 'Címztett neve:';
$_['entry_to_email']   = 'Címzett e-mail címe:';
$_['entry_from_name']  = 'Az Ön neve:';
$_['entry_from_email'] = 'Az Ön e-mail címe:';
$_['entry_theme']      = 'Ajándékutalvány témája:';
$_['entry_message']    = 'Üzenet:<br /><span class="help">(Opcionális)</span>';
$_['entry_amount']     = 'Érték:<br /><span class="help">(Az értéknek %s és %s között kell lennie)</span>';

// Error
$_['error_to_name']    = 'A címzett nevének 1-64 karakter között kell lennie!';
$_['error_from_name']  = 'Az ön nevének 1-64 karakter között kell lennie!';
$_['error_email']      = 'Az e-mail cím nem tűnik érvényesnek!';
$_['error_theme']      = 'Válasszon témát!';
$_['error_amount']     = 'Az értéknek %s és %s között kell lennie!';
$_['error_agree']      = 'Figyelem: El kell fogadnia, hogy az ajándékutalványok nem visszatéríthetőek!';
?>
