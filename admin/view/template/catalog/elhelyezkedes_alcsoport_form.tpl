<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/customer.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <table class="form">
              <tr>
                  <td><input type="text" name="megnevezes" value="<?php echo $megnevezes; ?>" /></td>
              </tr>
              <tr>
                  <td>
                      <span class="elhelyezkedes"><?php echo $entry_sorrend; ?></span>
                      <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="3"/>
                  </td>
              </tr>

              <tr>
                  <td>
                      <span class="elhelyezkedes"><?php echo $entry_darabszam; ?></span>
                      <input type="text" name="maximum" value="<?php echo $maximum; ?>" size="3"/>
                  </td>
              </tr>

              <tr>
                  <td>
                      <span class="elhelyezkedes"><?php echo $entry_ara; ?></span>
                      <input type="text" name="ara" value="<?php echo $ara; ?>" size="3"/>
                  </td>
              </tr>

              <tr>
                  <td>
                      <span class="elhelyezkedes"><?php echo $entry_tax_class; ?></span>
                      <select name="tax_class_id">
                          <option value="0"><?php echo $text_select; ?></option>
                          <?php foreach ($tax_classes as $tax_class) { ?>
                              <?php if ($tax_class['tax_class_id'] == $tax_class_id) { ?>
                                  <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                              <?php } ?>
                          <?php } ?>
                      </select>
                  </td>
              </tr>

              <tr>
                  <td>
                      <span class="elhelyezkedes"><?php echo $entry_status; ?></span>
                      <select name="status">
                          <?php if ($status == 0) { ?>
                              <option value="0" selected="selected"><?php echo $text_letiltott; ?></option>
                              <option value="1"><?php echo $text_engedelyezett; ?></option>

                          <?php } else { ?>
                              <option value="0"><?php echo $text_letiltott; ?></option>
                              <option value="1" selected="selected"><?php echo $text_engedelyezett; ?></option>

                          <?php } ?>
                      </select>
                  </td>
              </tr>

          </table>

      </form>
    </div>
  </div>
</div>


<?php echo $footer; ?>