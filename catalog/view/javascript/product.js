/* Termék aloldalon lévő jobb oldali box margóját számolja */
/* Start */
$(window).bind("load", function() {
    var kepernyoszeles = $(window).width();
    var kepszeles = $("#product_left").width()+"px";
    if(kepernyoszeles >= 877){
        $(".product-info .left + .right").css("margin-left",kepszeles);

    }
    else{
        $(".product-info .left + .right").css("margin-left","0px");
    }
});
$(window).resize(function(){
    var kepernyoszeles = $(window).width();
    if(kepernyoszeles >= 877){
        var kepszeles = $("#product_left").width()+"px";
        $(".product-info .left + .right").css("margin-left",kepszeles);
    }
    else{
        $(".product-info .left + .right").css("margin-left","0px");
    }
});
/* Vége */