<?php
class ControllerModuleSzamlazzHu extends Controller {
    private $error = array();
    private $_name = 'szamlazzhu';


    public function index() {
        $this->load->language('module/' . $this->_name);
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
            $szamlazz_hu_module['szamlazz_hu_module'] = $this->request->post['szamlazz_hu_module'];

            $this->model_setting_setting->editSetting($this->_name, $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');

            $this->load->model('localisation/order_status');
            $this->model_localisation_order_status->editOrderStatuses($this->request->post['szamla_tipusa']);


            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->data['heading_title']            = $this->language->get('heading_title');

        $this->data['text_enabled']             = $this->language->get('text_enabled');
        $this->data['text_disabled']            = $this->language->get('text_disabled');
        $this->data['text_content_top']         = $this->language->get('text_content_top');
        $this->data['text_content_bottom']      = $this->language->get('text_content_bottom');

        $this->data['text_settings']            = $this->language->get('text_settings');
        $this->data['text_password']            = $this->language->get('text_password');
        $this->data['text_username']            = $this->language->get('text_username');
        $this->data['text_yes']                 = $this->language->get('text_yes');
        $this->data['text_no']                  = $this->language->get('text_no');
        $this->data['text_eszamla']             = $this->language->get('text_eszamla');
        $this->data['text_szamla_download']     = $this->language->get('text_szamla_download');
        $this->data['text_kulcstartojelszo']    = $this->language->get('text_kulcstartojelszo');
        $this->data['text_peldanyszam']         = $this->language->get('text_peldanyszam');
        $this->data['text_valaszverzio']         = $this->language->get('text_valaszverzio');
        $this->data['text_aggregator']         = $this->language->get('text_aggregator');

        $this->data['text_select']         = $this->language->get('text_select');


        $this->data['entry_product']            = $this->language->get('entry_product');
        $this->data['entry_area_id']            = $this->language->get('entry_area_id');
        $this->data['entry_layout']             = $this->language->get('entry_layout');
        $this->data['entry_position']           = $this->language->get('entry_position');
        $this->data['entry_status']             = $this->language->get('entry_status');
        $this->data['entry_sort_order']         = $this->language->get('entry_sort_order');
        $this->data['entry_order_status'] = $this->language->get('entry_order_status');


        $this->data['button_save']              = $this->language->get('button_save');
        $this->data['button_cancel']            = $this->language->get('button_cancel');
        $this->data['button_add_module']        = $this->language->get('button_add_module');
        $this->data['button_remove']            = $this->language->get('button_remove');
        $this->data['entry_yes']                = $this->language->get( 'entry_yes' );
        $this->data['entry_no']	                = $this->language->get( 'entry_no' );

        $this->data['button_save']              = $this->language->get('button_save');
        $this->data['button_cancel']            = $this->language->get('button_cancel');

        $this->data['entry_area']               = $this->language->get('entry_area');
        $this->data['entry_code']               = $this->language->get('entry_code');
        $this->data['entry_header']             = $this->language->get( 'entry_header' );

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $this->load->model('localisation/language');


        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/'.$this->_name, 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('module/'.$this->_name, 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['token'] = $this->session->data['token'];


        $this->load->model('design/layout');

        $this->data['layouts'] = $this->model_design_layout->getLayouts();

        $this->data['modules'] = array();

        if (isset($this->request->post['szamlazz_hu_module'])) {
            $this->data['szamlazz_hu_module'] = $this->request->post['szamlazz_hu_module'];
        } elseif ($this->config->get('szamlazz_hu_module')) {
            $this->data['szamlazz_hu_module'] = $this->config->get('szamlazz_hu_module');
        }
        if (isset($this->request->post['szamlazz_order_status_id'])) {
            $this->data['szamlazz_order_status_id'] = $this->request->post['szamlazz_order_status_id'];
        } else {
            $this->data['szamlazz_order_status_id'] = $this->config->get('szamlazz_order_status_id');
        }

        $data = array(
            'sort'  => 'name',
            'order' => 'ASC',
            'start' => 0,
            'limit' => 100
        );

        $this->load->model('localisation/order_status');
        $results = $this->model_localisation_order_status->getOrderStatuses($data);

        foreach ($results as $result) {

            $this->data['order_statuses'][] = array(
                'order_status_id' => $result['order_status_id'],
                'name'            => $result['name'] . (($result['order_status_id'] == $this->config->get('config_order_status_id')) ? $this->language->get('text_default') : null),
                'szamla_tipusa'   => $this->model_localisation_order_status->getOrderStatusSzamla($result['order_status_id'])
            );
        }

        $this->template = 'module/' . $this->_name . '.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    private function validate() {

        if (!$this->user->hasPermission('modify', 'module/' . $this->_name)) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>
