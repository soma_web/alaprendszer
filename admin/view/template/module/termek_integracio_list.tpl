<?php echo $header; ?>

<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="createFiles()" class="button"><?php echo $button_legyartas; ?></a>
                <a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
                <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
            </div>
        </div>
        <div class="send">
            <div class="heading"><?php echo $text_url_elerhetosegek?></div>
            <?php echo $text_url_magyarazat?>
                <?php foreach($engedelyezett_integraciok as $value) { ?>
                    <?php echo ' - '.$value; ?>
                <?php } ?>
        </div>

        <div class="content">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="list">
                    <thead>
                    <tr>
                        <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>


                        <td class="left">
                            <?php if ($sort == 'termek_integracio_id') { ?>
                                <a href="<?php echo $sort_termek_integracio_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_termek_integracio_id; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_termek_integracio_id; ?>"><?php echo $column_termek_integracio_id; ?></a>
                            <?php } ?>
                        </td>

                        <td class="left">
                            <?php if ($sort == 'name') { ?>
                                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                            <?php } ?>
                        </td>


                        <td class="left">
                            <?php if ($sort == 'status') { ?>
                                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                            <?php } ?>
                        </td>

                        <td class="left">
                             <a href=""><?php echo $column_connects; ?></a>
                        </td>

                        <td class="right"><?php echo $column_action; ?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if ($termek_integraciok) { ?>
                        <?php foreach ($termek_integraciok as $termek_integracio) { ?>
                            <tr>
                                <td style="text-align: center;">
                                    <?php if ($termek_integracio['selected']) { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $termek_integracio['termek_integracio_id']; ?>" checked="checked" />
                                    <?php } else { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $termek_integracio['termek_integracio_id']; ?>" />
                                    <?php } ?>
                                </td>
                                <td class="left"><?php echo $termek_integracio['termek_integracio_id']; ?></td>
                                <td class="left">
                                    <span class="list_ertek" onclick="listElemValueModify(this)">
                                        <?php echo $termek_integracio['name']; ?>
                                    </span>
                                    <span class="list_modify" style="display: none">
                                        <input type="text" name="sort_order" value="<?php echo $termek_integracio['name']; ?>" modositas="<?php echo $termek_integracio['name_event']?>">
                                    </span>
                                </td>

                                <td class="left">
                                    <span class="list_ertek" onclick="listElemValueModify(this)"><?php echo $termek_integracio['status']; ?></span>

                                    <span class="list_modify" style="display: none">
                                        <select name="" modositas="<?php echo $termek_integracio['status_event']?>" onchange="$(this).blur();">
                                            <?php if (!empty($termek_integracio['status_value'])) { ?>
                                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                                <option value="0"><?php echo $text_disabled; ?></option>
                                            <?php } else { ?>
                                                <option value="1"><?php echo $text_enabled; ?></option>
                                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                            <?php } ?>
                                        </select>
                                    </span>
                                </td>

                                <td class="left"><?php echo $termek_integracio['connects']; ?></td>
                                <td class="right">
                                    <?php foreach ($termek_integracio['action'] as $action) { ?>
                                        [ <a href="<?php echo $action['href']; ?>" <?php echo !empty($action['target']) ? 'target='.$action['target'] : ''?>><?php echo $action['text']; ?></a> ]
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
    </div>
</div>

<script>
    function createFiles() {
        $.ajax({
            url: '<?php echo HTTP_CATALOG; ?>index.php?route=cron/altalanos_termek_integracio&token=<?php echo $token; ?>',
            data: '',
            type: 'post',
            dataType: 'json',
            beforeSend: function() {
                var height = $("body").height();
                var html = '';
                html += '<div class="takaro" style="height: '+height+'px">';
                html += '<img class="wait" src="view/image/loading_tappancs.gif" alt="" />';
                html += '</div>';
                $("body").append(html);
                $(".takaro").slideDown("slow");

            },
            complete: function() {
                $(".takaro").slideUp("slow",function(){
                    $(".takaro").remove();
                });
            },
            success: function(json) {
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
</script>
<?php require_once('view/javascript/common.php')?>

<?php echo $footer; ?> 