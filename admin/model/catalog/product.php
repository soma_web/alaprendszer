<?php
class ModelCatalogProduct extends Model {
    public function addProduct($data) {

        if (!isset($data['sku']) ){
            $data['sku'] = "";
        }

        if (!isset($data['upc']) ){
            $data['upc']= "";
        }

        if (!isset($data['location']) ){
            $data['location'] = "";
        }


        if (!isset($data['quantity']) ){
            $data['quantity'] = "";
        }

        if (!isset($data['csom_egyseg']) ){
            $data['csom_egyseg'] = "";

        }
        if (!isset($data['csom_mennyiseg']) ){
            $data['csom_mennyiseg'] = "";

        }
        if (!isset($data['minimum']) ){
            $data['minimum'] = "";

        }
        if (!isset($data['subtract']) ){
            $data['subtract'] = "";

        }
        if (!isset($data['checked']) ){
            $data['checked'] = "";

        }
        if (!isset($data['stock_status_id']) ){
            $data['stock_status_id'] = "";

        }
        if (!isset($data['date_available']) ){
            $data['date_available'] = "";

        }
        if (!isset($data['date_ervenyes_ig']) ){
            $data['date_ervenyes_ig'] = "";

        }
        if (!isset($data['manufacturer_id']) ){
            $data['manufacturer_id'] = "";

        }
        if (!isset($data['shipping']) ){
            $data['shipping'] = "";

        }
        if (!isset($data['stock_in_date']) ){
            $data['stock_in_date'] = "";

        }
        if (!isset($data['lizing']) ){
            $data['lizing'] = "";

        }
        if (!isset($data['price']) ){
            $data['price'] = "";

        }
        if (!isset($data['eredeti_ar']) ){
            $data['eredeti_ar'] = "";

        }
        if (!isset($data['szazalek']) ){
            $data['szazalek'] = "";

        }
        if (!isset($data['points']) ){
            $data['points'] = "";

        }

        $data['egyedi_szallitas']       = isset($data['egyedi_szallitas']) ? $data['egyedi_szallitas'] : 0;
        $data['egyedi_szallitasi_dij']  = isset($data['egyedi_szallitasi_dij']) ? $data['egyedi_szallitasi_dij'] : 0;
        $data['garancia_ertek']         = isset($data['garancia_ertek']) ? $data['garancia_ertek'] : 0;
        $data['garancia_egyseg']        = isset($data['garancia_egyseg']) ? $data['garancia_egyseg'] : 0;
        $data['ujdonsag']               = isset($data['ujdonsag']) ? $data['ujdonsag'] : 0;
        $data['kifuto']                 = isset($data['kifuto']) ? $data['kifuto'] : 0;
        $data['imagedesabled']          = isset($data['imagedesabled']) ? $data['imagedesabled'] : 0;
        $data['location']               = isset($data['location']) ? $data['location'] : 0;
        $data['megyseg']                = isset($data['megyseg']) ? $data['megyseg'] : 0;
        $data['cikkszam2']              = isset($data['cikkszam2']) ? $data['cikkszam2'] : 0;
        $data['cikkszam']               = isset($data['cikkszam']) ? $data['cikkszam'] : 0;
        $data['model']                  = isset($data['model']) ? $data['model'] : 0;
        $data['sort_order']             = isset($data['sort_order']) ? $data['sort_order'] : 0;
        $data['tax_class_id']           = isset($data['tax_class_id']) ? $data['tax_class_id'] : 0;
        $data['utalvany']               = isset($data['utalvany']) ? $data['utalvany'] : 0;
        $data['status']                 = isset($data['status']) ? $data['status'] : 0;
        $data['length_class_id']        = isset($data['length_class_id']) ? $data['length_class_id'] : 0;
        $data['elorendeles']            = isset($data['elorendeles']) ? $data['elorendeles'] : 0;
        $data['megrendelem']            = isset($data['megrendelem']) ? $data['megrendelem'] : 0;
        $data['ar_tiltasa']             = isset($data['ar_tiltasa']) ? $data['ar_tiltasa'] : 0;
        $data['height']                 = isset($data['height']) ? $data['height'] : 0;
        $data['width']                  = isset($data['width']) ? $data['width'] : 0;
        $data['length']                 = isset($data['length']) ? $data['length'] : 0;
        $data['weight_class_id']        = isset($data['weight_class_id']) ? $data['weight_class_id'] : 0;
        $data['packaging_class_id']     = isset($data['packaging_class_id']) ? $data['packaging_class_id'] : 0;
        $data['product_video']          = isset($data['product_video']) ? $data['product_video'] : '';
        $data['video']                  = isset($data['video']) ? $data['video'] : '';
        $data['weight']                 = isset($data['weight']) ? $data['weight'] : '';
        $data['packaging']              = isset($data['packaging']) ? $data['packaging'] : '';
        $data['max_szazalek_kedvezmeny']= isset($data['max_szazalek_kedvezmeny']) ? $data['max_szazalek_kedvezmeny'] : 0;
        $data['szin_meret_szukseges']   = isset($data['szin_meret_szukseges']) ? $data['szin_meret_szukseges'] : 0;
        $data['egyszer_kosarba']        = isset($data['egyszer_kosarba']) ? $data['egyszer_kosarba'] : 0;
        $data['crm_tipus']              = isset($data['crm_tipus']) ? $data['crm_tipus'] : 0;
        $data['crm_category_id']        = isset($data['crm_category_id']) ? $data['crm_category_id'] : 0;
        $data['crm_product_id']         = isset($data['crm_product_id']) ? $data['crm_product_id'] : 0;


        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET
            model                   = '" . $this->db->escape($data['model']) . "',
            cikkszam                = '" . $this->db->escape($data['cikkszam']) . "',
            cikkszam2               = '" . $this->db->escape($data['cikkszam2']) . "',
            megyseg                 = '" . $this->db->escape($data['megyseg']) . "',
            sku                     = '" . $this->db->escape($data['sku']) . "',
            upc                     = '" . $this->db->escape($data['upc']) . "',
            location                = '" . $this->db->escape($data['location']) . "',
            quantity                = '" . (int)$data['quantity'] . "',
            csomagolasi_egyseg      = '" . (int)$data['csom_egyseg'] . "' ,
            csomagolasi_mennyiseg   = '" . (int)$data['csom_mennyiseg'] . "' ,
            minimum                 = '" . (float)$data['minimum'] . "',
            subtract                = '" . (int)$data['subtract'] . "',
            checked                 = '" . (int)$data['checked'] . "',
            stock_status_id         = '" . (int)$data['stock_status_id'] . "',
            date_available          = '" . $this->db->escape($data['date_available']) . "',
            date_ervenyes_ig        = '" . $data['date_ervenyes_ig'] . "',
            stock_in_date           = '" . $data['stock_in_date'] . "',
            manufacturer_id         = '" . (int)$data['manufacturer_id'] . "',
            shipping                = '" . (int)$data['shipping'] . "',
            lizing                  = '" . (int)$data['lizing'] . "',
            price                   = '" . (float)$data['price'] . "',
            eredeti_ar              = '" . (float)$data['eredeti_ar'] . "',
            szazalek                = '" . (int)$data['szazalek'] . "',
            points                  = '" . (int)$data['points'] . "',
            kifuto                  = '" . (int)$data['kifuto'] . "',
            ujdonsag                = '" . (int)$data['ujdonsag'] . "',
            elorendeles             = '" . (int)$data['elorendeles'] . "',
            megrendelem             = '" . (int)$data['megrendelem'] . "',
            ar_tiltasa              = '" . (int)$data['ar_tiltasa'] . "',
            video                   = '" . $data['video'] . "',
            weight                  = '" . (float)$data['weight'] . "',
            weight_class_id         = '" . (int)$data['weight_class_id'] . "',
            packaging               = '" . (float)$data['packaging'] . "',
            packaging_class_id      = '" . (int)$data['packaging_class_id'] . "',
            `length`                = '" . (float)$data['length'] . "',
            width                   = '" . (float)$data['width'] . "',
            height                  = '" . (float)$data['height'] . "',
            length_class_id         = '" . (int)$data['length_class_id'] . "',
            status                  = '" . (int)$data['status'] . "',
            crm_tipus               = '" . (int)$data['crm_tipus'] . "',
            crm_category_id         = '" . (int)$data['crm_category_id'] . "',
            crm_product_id          = '" . (int)$data['crm_product_id'] . "',
            utalvany                = '" . (int)$data['utalvany'] . "',
            tax_class_id            = '" . $this->db->escape($data['tax_class_id']) . "',
            sort_order              = '" . (int)$data['sort_order'] . "',
            egyedi_szallitas        = '" . (int)$data['egyedi_szallitas'] . "',
            egyedi_szallitasi_dij   = '" . (float)$data['egyedi_szallitasi_dij'] . "',
            garancia_ertek          = '" . ($data['garancia_egyseg'] == 2 ? $data['garancia_ertek']*12 : $data['garancia_ertek']) . "',
            garancia_egyseg         = '" . (int)$data['garancia_egyseg'] . "',
            imagedesabled           = '" . $data['imagedesabled'] . "',
            max_szazalek_kedvezmeny = '" . $data['max_szazalek_kedvezmeny'] . "',
            szin_meret_szukseges    = '" . $data['szin_meret_szukseges'] . "',
            egyszer_kosarba         = '" . $data['egyszer_kosarba'] . "',
            date_added              = NOW()");

        $product_id = $this->db->getLastId();

        if ($this->config->get('megjelenit_vatera') == 1) {
            if (file_exists(DIR_TEMPLATE."catalog/vatera.tpl")){
                require_once(DIR_APPLICATION."model/catalog/vateraup.php");}
        }


        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "'
			    WHERE product_id = '" . (int)$product_id . "'");
        }
        if (isset($data['letoltheto_kep'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET letoltheto = '" . $this->db->escape(html_entity_decode($data['letoltheto_kep'], ENT_QUOTES, 'UTF-8')) . "'
			    WHERE product_id = '" . (int)$product_id . "'");
        }

        foreach ($data['product_description'] as $language_id => $value) {
            $value['short_description']     = isset($value['short_description']) ? $value['short_description'] : "";
            $value['test_description']      = isset($value['test_description'])  ? $value['test_description'] : "";
            $value['garancia_description']  = isset($value['garancia_description'])  ? $value['garancia_description'] : "";

            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET
			    product_id              = '" . (int)$product_id . "',
			    language_id             = '" . (int)$language_id . "',
			    name                    = '" . $this->db->escape(html_entity_decode($value['name'], ENT_QUOTES, 'UTF-8')) . "',
			    meta_keyword            = '" . $this->db->escape($value['meta_keyword']) . "',
			    meta_description        = '" . $this->db->escape($value['meta_description']) . "',
			    short_description       = '" . $this->db->escape($value['short_description']) . "',
			    test_description        = '" . $this->db->escape($value['test_description']) . "',
			    garancia_description    = '" . $this->db->escape($value['garancia_description']) . "',
			    description             = '" . $this->db->escape($value['description']) . "'");
        }

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['product_elhelyezkedes'])) {
            foreach ($data['product_elhelyezkedes'] as $key=>$elhelyezkedes) {
                if ($key !== "NULL") {

                    $elhelyezkedes_csoport = $this->config->get('elhelyezkedes');
                    $elhelyezkedes_alcsoport = $this->config->get('elhelyezkedes_alcsoport');
                    $kell_alcsoport = 0;
                    $elhelyezkedes_sort_order           = null;
                    $elhelyezkedes_alcsoport_sort_order = null;

                    foreach($elhelyezkedes_csoport as $value) {

                        if ($value['elhelyezkedes_id'] == $elhelyezkedes['elhelyezkedes_id']) {
                            $elhelyezkedes_sort_order = $value['sort_order'];
                            if ($value['alcsoport_status'] == 1) {
                                $kell_alcsoport = 1;
                            } else {
                                $kell_alcsoport = 0;
                            }
                            break;
                        }
                    }

                    foreach($elhelyezkedes_alcsoport as $value) {

                        if ($value['elhelyezkedes_alcsoport_id'] == $elhelyezkedes['elhelyezkedes_alcsoport_id']) {
                            $elhelyezkedes_alcsoport_sort_order = $value['sort_order'];
                            break;
                        }
                    }

                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_elhelyezes SET
                     `product_id`                            = '" . (int)$product_id . "',
	                `elhelyezkedes_id`	                    = '" . (int)$elhelyezkedes['elhelyezkedes_id'] . "',
	                `elhelyezkedes_neve`	                = '" . $elhelyezkedes['elhelyezkedes_neve'] . "',
	                `elhelyezkedes_netto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_netto_ara'] . "',
	                `elhelyezkedes_brutto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_brutto_ara'] . "',
	                `elhelyezkedes_sort_order`	            = '" . $elhelyezkedes_sort_order . "',

	                `elhelyezkedes_alcsoport_id`	        = '" . (int)$elhelyezkedes['elhelyezkedes_alcsoport_id'] . "',
	                `elhelyezkedes_alcsoport_neve`	        = '" . $elhelyezkedes['elhelyezkedes_alcsoport_neve'] . "',
	                `elhelyezkedes_alcsoport_netto_ara` 	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_netto_ara'] . "',
	                `elhelyezkedes_alcsoport_brutto_ara`	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'] . "',
	                `elhelyezkedes_alcsoport_sort_order`	= '" . $elhelyezkedes_alcsoport_sort_order . "',
	                `kell_alcsoport`	                    = '" . $kell_alcsoport. "',

	                `kiemelesek_id`                         = '" . (int)$elhelyezkedes['kiemelesek_id'] . "',
	                `kiemelesek_neve`                       = '" . $elhelyezkedes['kiemelesek_neve'] . "',
	                `kiemelesek_netto_ara`                  = '" . $elhelyezkedes['kiemelesek_netto_ara'] . "',
	                `kiemelesek_brutto_ara`                 = '" . $elhelyezkedes['kiemelesek_brutto_ara'] . "',
	                `sor`                                   = '" . $elhelyezkedes['sor'] . "',
	                `netto_ar`                              = '" . $elhelyezkedes['netto_ar'] . "',
	                `brutto_ar`                             = '" . $elhelyezkedes['brutto_ar'] . "',

	                `total_netto_ar`                        = '" . $elhelyezkedes['total_netto_ar'] . "',
	                `total_brutto_ar`                       = '" . $elhelyezkedes['total_brutto_ar'] . "',

	                `idoszak`                   	        = '" . (int)$elhelyezkedes['idoszak'] . "',
	                `idoszak_id`                   	        = '" . (int)$elhelyezkedes['idoszak_id'] . "',
	                `priority`                   	        = '" . (int)$elhelyezkedes['priority'] . "',
	                `datum_tol`                 	        = '" . $elhelyezkedes['datum_tol'] . "',
	                `datum_ig`          	                = '" . $elhelyezkedes['datum_ig'] . "',
	                `fizetes_elbiralas_status_id`           = '" . (int)$elhelyezkedes['fizetes_elbiralas_status_id'] . "',
	                `penzugyi_status_id`                    = '" . (int)$elhelyezkedes['penzugyi_status_id'] . "'");
                }
            }
        }


        if (isset($data['product_customer'])) {
            foreach ($data['product_customer'] as $customer_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_customer SET product_id = '" . (int)$product_id . "', customer_id = '" . (int)$customer_id['customer_id'] . "'");
            }
        }



        if (isset($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox'
                    || $product_option['type'] == 'image' || $product_option['type'] == 'checkbox_qty') {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET
                        product_id = '" . (int)$product_id . "',
                        option_id = '" . (int)$product_option['option_id'] . "',
                        required = '" . (isset($product_option['required']) ? (int)$product_option['required'] :0) . "'");

                    $product_option_id = $this->db->getLastId();

                    if (isset($product_option['product_option_value'])) {
                        foreach ($product_option['product_option_value'] as $product_option_value) {

                            if ($product_option['type'] == 'checkbox_qty') {
                                $sql = "SELECT option_szin_id, azonosito FROM " . DB_PREFIX . "option_value WHERE option_value_id = '" .(int)$product_option_value['option_value_id']. "'";
                                $query = $this->db->query($sql);
                            }

                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET
                                product_option_id   = '" . (int)$product_option_id . "',
                                product_id          = '" . (int)$product_id . "',
                                option_id           = '" . (int)$product_option['option_id'] . "',
                                option_value_id     = '" . (int)$product_option_value['option_value_id'] . "',
                                quantity            = '" . (int)$product_option_value['quantity'] . "',
                                subtract            = '" . (int)$product_option_value['subtract'] . "',
                                checked             = '" . (isset($product_option_value['checked']) ? (int)$product_option_value['checked'] : 0) . "',
                                price               = '" . (float)$product_option_value['price'] . "',
                                option_szin_id      = '" . (isset($query->row['option_szin_id']) ? (int)$query->row['option_szin_id'] : ''). "',
                                azonosito           = '" . (isset($query->row['azonosito']) ? (int)$query->row['azonosito'] : ''). "',
                                price_prefix        = '" . $this->db->escape($product_option_value['price_prefix']) . "',
                                points              = '" . (int)$product_option_value['points'] . "',
                                points_prefix       = '" . $this->db->escape($product_option_value['points_prefix']) . "',
                                weight              = '" . (float)$product_option_value['weight'] . "',
                                weight_prefix       = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value = '" . $this->db->escape($product_option['option_value']) . "', required = '" . (int)$product_option['required'] . "'");
                }
            }
        }

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        if (isset($data['product_vevo'])) {
            foreach ($data['product_vevo'] as $product_vevo) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_customer_special SET product_id = '" . (int)$product_id . "', customer_id = '" . (int)$product_vevo['customer_id'] . "', quantity = '" . (int)$product_vevo['quantity'] . "', price = '" . (float)$product_vevo['price'] . "', date_start = '" . $this->db->escape($product_vevo['date_start']) . "', date_end = '" . $this->db->escape($product_vevo['date_end']) . "'");
            }
        }

        if (isset($data['product_customer_utalvany'])) {
            foreach ($data['product_customer_utalvany'] as $product_vevo) {
                if (is_array($product_vevo)) {
                    $product_vevo = $product_vevo['customer_id'];
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_customer_utalvany SET
                product_id = '" . (int)$product_id . "',
                customer_id = '" . (int)$product_vevo . "'");
            }
        }

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $kiemelt = isset($product_special['kiemelt']) ? $product_special['kiemelt'] : 0;


                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET
				    product_id = '" . (int)$product_id . "',
				    customer_group_id = '" . (int)$product_special['customer_group_id'] . "',
				    priority = '" . (int)$product_special['priority'] . "',
				    price = '" . (float)$product_special['price'] . "',
				    kiemelt = '" . $kiemelt . "',
				    date_start = '" . $this->db->escape($product_special['date_start']) . "',
				    date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET
				    product_id = '" . (int)$product_id . "',
				    image = '" . $this->db->escape(html_entity_decode($product_image['image'], ENT_QUOTES, 'UTF-8')) . "',
				    sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        if (isset($data['product_video']) && !empty($data['product_video'])) {
            foreach ($data['product_video'] as $product_video) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_video SET
                product_id = '" . (int)$product_id . "',
                video_url = '" . $product_video['video_url'] . "',
                sort_order = '" . (int)$product_video['sort_order'] . "',
                video_title = '" . $product_video['video_title'] . "'");
            }
        }

        if (isset($data['product_pdf']) && !empty($data['product_pdf'])) {
            foreach ($data['product_pdf'] as $product_pdf) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_pdf SET
                product_id  = '" . (int)$product_id . "',
                `path`      = '" . $this->db->escape(html_entity_decode($product_pdf['image'], ENT_QUOTES, 'UTF-8')) . "',
                sort_order  = '" . (int)$product_pdf['sort_order'] . "',
                pdf_title   = '" . $product_pdf['pdf_title'] . "'");
            }
        }

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $sql = "SELECT * FROM " . DB_PREFIX . "filter WHERE  filter_id = '" . (int)$filter_id . "'";

                $query = $this->db->query($sql);
                $fi_s_id = $query->row['filter_select_id'];

                $csoport_id = $query->row['filter_group_id'];

                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET
                    product_id = '" . (int)$product_id . "',
                    filter_group_id = '" . (int)$csoport_id . "',
                    filter_select_id = '" . (int)$fi_s_id . "',
                    filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");

                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }

        if (isset($data['product_package'])) {
            foreach ($data['product_package'] as $key=>$package) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_package SET
                    product_id      = '" . (int)$product_id . "',
                    quantity        = '" . (int)$package['quantity'] . "',
                    package_id      = '" . (int)$package['package_id'] . "',
                    price           = '" . (int)$package['price'] . "'");
            }
        }

        if (isset($data['product_accessory'])) {
            foreach ($data['product_accessory'] as $accessory_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_accessory SET
                    product_id      = '" . (int)$product_id . "',
                    accessory_id      = '" . (int)$accessory_id . "'");
            }
        }

        if (isset($data['product_ajandek'])) {
            foreach ($data['product_ajandek'] as $ajandek_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_ajandek SET
                    product_id      = '" . (int)$product_id . "',
                    ajandek_id      = '" . (int)$ajandek_id . "'");
            }
        }

        if (isset($data['product_helyettesito'])) {
            foreach ($data['product_helyettesito'] as $helyettesito_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_helyettesito SET
                    product_id      = '" . (int)$product_id . "',
                    helyettesito_id      = '" . (int)$helyettesito_id . "'");
            }
        }

        if (isset($data['product_general_accessory'])) {
            foreach ($data['product_general_accessory'] as $general_accessory_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_general_accessory SET
                    product_id      = '" . (int)$product_id . "',
                    general_accessory_id      = '" . (int)$general_accessory_id . "'");
            }
        }

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
                if ($product_reward['points'] > 0) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
                }
            }
        }

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout) {
                if ($layout['layout_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
                }
            }
        }

        if (isset($data['product_tag'])) {

            foreach ($data['product_tag'] as $language_id => $value) {
                if ($value) {
                    $tags = explode(',', $value);

                    foreach ($tags as $tag) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_tag SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', tag = '" . $this->db->escape(trim($tag)) . "'");
                    }
                }
            }
        }

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }



        $this->cache->delete('product');
    }

    public function editProduct($product_id, $data) {
        if ($this->config->get('megjelenit_vatera') == 1) {
            if (file_exists(DIR_APPLICATION."model/catalog/vatera.php")){
                require_once(DIR_APPLICATION."model/catalog/vatera.php");
            }
        }

        $query =  $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" .$product_id . "'");

        if (isset($data['model']) ) {
            $model = $this->db->escape($data['model']);
        } else {
            $model = $query->row['model'];
        }

        if (isset($data['cikkszam']) ) {
            $cikkszam = $this->db->escape($data['cikkszam']);
        } else {
            $cikkszam = $query->row['cikkszam'];
        }

        if (isset($data['cikkszam2']) ) {
            $cikkszam2 = $this->db->escape($data['cikkszam2']);
        } else {
            $cikkszam2 = $query->row['cikkszam2'];
        }

        if (isset($data['eredeti_ar']) ) {
            $eredeti_ar = $data['eredeti_ar'];
        } else {
            $eredeti_ar = $query->row['eredeti_ar'];
        }

        if (isset($data['utalvany']) ) {
            $utalvany = $data['utalvany'];
        } else {
            $utalvany = $query->row['utalvany'];
        }

        if (isset($data['date_ervenyes_ig']) ) {
            $date_ervenyes_ig = $data['date_ervenyes_ig'];
        } else {
            $date_ervenyes_ig = $query->row['date_ervenyes_ig'];
        }

        if (isset($data['date_available']) ) {
            $date_available = $data['date_available'];
        } else {
            $date_available = $query->row['date_available'];
        }

        if (isset($data['szazalek']) ) {
            $szazalek = $data['szazalek'];
        } else {
            $szazalek = $query->row['szazalek'];
        }
        if (isset($data['points']) ) {
            $points = $data['points'];
        } else {
            $points = $query->row['points'];
        }
        if (isset($data['weight']) ) {
            $weight = $data['weight'];
        } else {
            $weight = $query->row['weight'];
        }

        if (isset($data['packaging']) ) {
            $packaging = $data['packaging'];
        } else {
            $packaging = $query->row['packaging'];
        }

        if (isset($data['checked']) ) {
            $checked = $data['checked'];
        } else {
            $checked = $query->row['checked'];
        }

        if (isset($data['video']) ) {
            $video = $data['video'];
        } else {
            $video = $query->row['video'];
        }

        if (isset($data['weight_class_id']) ) {
            $weight_class_id = $data['weight_class_id'];
        } else {
            $weight_class_id = $query->row['weight_class_id'];
        }

        if (isset($data['packaging_class_id']) ) {
            $packaging_class_id = $data['packaging_class_id'];
        } else {
            $packaging_class_id = $query->row['packaging_class_id'];
        }

        if (isset($data['length']) ) {
            $length = $data['length'];
        } else {
            $length = $query->row['length'];
        }
        if (isset($data['width']) ) {
            $width = $data['width'];
        } else {
            $width = $query->row['width'];
        }
        if (isset($data['height']) ) {
            $height = $data['height'];
        } else {
            $height = $query->row['height'];
        }
        if (isset($data['length_class_id']) ) {
            $length_class_id = $data['length_class_id'];
        } else {
            $length_class_id = $query->row['length_class_id'];
        }
        if (isset($data['status']) ) {
            $status = $data['status'];
        } else {
            $status = $query->row['status'];
        }
        if (isset($data['tax_class_id']) ) {
            $tax_class_id = $data['tax_class_id'];
        } else {
            $tax_class_id = $query->row['tax_class_id'];
        }
        if (isset($data['sort_order']) ) {
            $sort_order = $data['sort_order'];
        } else {
            $sort_order = $query->row['sort_order'];
        }
        if (isset($data['minimum']) ) {
            $minimum = $data['minimum'];
        } else {
            $minimum = $query->row['minimum'];
        }
        if (isset($data['shipping']) ) {
            $shipping = $data['shipping'];
        } else {
            $shipping = $query->row['shipping'];
        }

        if (isset($data['stock_in_date']) ) {
            $stock_in_date = $data['stock_in_date'];
        } else {
            $stock_in_date = $query->row['stock_in_date'];
        }

        if (isset($data['lizing']) ) {
            $lizing = $data['lizing'];
        } else {
            $lizing = $query->row['lizing'];
        }

        if (isset($data['sku']) ) {
            $sku = $data['sku'];
        } else {
            $sku = $query->row['sku'];
        }
        if (isset($data['upc']) ) {
            $upc = $data['upc'];
        } else {
            $upc = $query->row['upc'];
        }
        if (isset($data['megyseg']) ) {
            $megyseg = $data['megyseg'];
        } else {
            $megyseg = $query->row['megyseg'];
        }
        if (isset($data['csom_egyseg']) ) {
            $csom_egyseg = $data['csom_egyseg'];
        } else {
            $csom_egyseg = $query->row['csomagolasi_egyseg'];
        }
        if (isset($data['csom_mennyiseg']) ) {
            $csom_mennyiseg = $data['csom_mennyiseg'];
        } else {
            $csom_mennyiseg = $query->row['csomagolasi_mennyiseg'];
        }

        $data['egyedi_szallitas']       = isset($data['egyedi_szallitas']) ? $data['egyedi_szallitas']              : $query->row['egyedi_szallitas'];
        $data['egyedi_szallitasi_dij']  = isset($data['egyedi_szallitasi_dij']) ? $data['egyedi_szallitasi_dij']    : $query->row['egyedi_szallitasi_dij'];
        $data['imagedesabled']          = isset($data['imagedesabled']) ? $data['imagedesabled']                    : $query->row['imagedesabled'];
        $data['garancia_ertek']         = isset($data['garancia_ertek']) ? $data['garancia_ertek']                  : $query->row['garancia_ertek'];
        $data['garancia_egyseg']        = isset($data['garancia_egyseg']) ? $data['garancia_egyseg']                : $query->row['garancia_egyseg'];
        $data['correctsecuritycode']    = isset($data['correctsecuritycode']) ? $data['correctsecuritycode']        : $query->row['correctsecuritycode'];
        $data['location']               = isset($data['location']) ? $data['location']                              : $query->row['location'];
        $data['max_szazalek_kedvezmeny']= isset($data['max_szazalek_kedvezmeny']) ? $data['max_szazalek_kedvezmeny']: $query->row['max_szazalek_kedvezmeny'];
        $data['szin_meret_szukseges']   = isset($data['szin_meret_szukseges']) ? $data['szin_meret_szukseges']      : $query->row['szin_meret_szukseges'];
        $data['egyszer_kosarba']        = isset($data['egyszer_kosarba']) ? $data['egyszer_kosarba']                : $query->row['egyszer_kosarba'];
        $crm_tipus                      = isset($data['crm_tipus']) ? $data['crm_tipus']                            : $query->row['crm_tipus'];
        $crm_category_id                = isset($data['crm_category_id']) ? $data['crm_category_id']                : $query->row['crm_category_id'];
        $crm_product_id                 = isset($data['crm_product_id']) ? $data['crm_product_id']                  : $query->row['crm_product_id'];


        $sql = "UPDATE " . DB_PREFIX . "product SET
		                    model                   = '" . $model . "',
		                    cikkszam                = '" . $cikkszam . "',
		                    cikkszam2               = '" . $cikkszam2. "',
		                    megyseg                 = '" . $this->db->escape($megyseg) . "',
		                    csomagolasi_egyseg      = '" . $this->db->escape($csom_egyseg) . "',
		                    csomagolasi_mennyiseg   = '" . $this->db->escape($csom_mennyiseg) . "',
		                    sku                     = '" . $this->db->escape($sku) . "',
		                    upc                     = '" . $this->db->escape($upc) . "',
		                    location                = '" . $this->db->escape($data['location']) . "',
		                    quantity                = '" . (int)$data['quantity'] . "',
		                    minimum                 = '" . (float)$minimum . "',
		                    subtract                = '" . (int)$data['subtract'] . "',
		                    checked                 = '" . (int)$checked . "',
		                    stock_status_id         = '" . (int)$data['stock_status_id'] . "',
		                    date_available          = '" . $this->db->escape($date_available) . "',
		                    date_ervenyes_ig        = '" . $date_ervenyes_ig . "',
		                    stock_in_date           = '" . $stock_in_date . "',
		                    manufacturer_id         = '" . (int)$data['manufacturer_id'] . "',
		                    shipping                = '" . (int)$shipping . "',
		                    lizing                  = '" . (int)$lizing . "',
		                    price                   = '" . (float)$data['price'] . "',
		                    eredeti_ar              = '" . (float)$eredeti_ar . "',
		                    utalvany                = '" . (int)$utalvany . "',
		                    szazalek                = '" . (int)$szazalek . "',
		                    points                  = '" . (int)$points . "',
		                    kifuto                  = '" . (isset($data['kifuto']) ? $data['kifuto'] : $query->row['kifuto']) . "',
		                    ujdonsag                = '" . (isset($data['ujdonsag']) ? $data['ujdonsag'] : $query->row['ujdonsag']) . "',
		                    elorendeles             = '" . (isset($data['elorendeles']) ? $data['elorendeles'] : $query->row['elorendeles']) . "',
		                    megrendelem             = '" . (isset($data['megrendelem']) ? $data['megrendelem'] : $query->row['megrendelem']) . "',
		                    ar_tiltasa              = '" . (isset($data['ar_tiltasa']) ? $data['ar_tiltasa'] : $query->row['ar_tiltasa']) . "',
		                    video                   = '" . $video . "',
		                    weight                  = '" . (float)$weight . "',
		                    weight_class_id         = '" . (int)$weight_class_id . "',
		                    packaging               = '" . (float)$packaging . "',
		                    packaging_class_id      = '" . (int)$packaging_class_id . "',
		                    length                  = '" . (float)$length . "',
		                    width                   = '" . (float)$width . "',
		                    height                  = '" . (float)$height . "',
		                    length_class_id         = '" . (int)$length_class_id . "',
		                    status                  = '" . (int)$status . "',
		                    crm_tipus               = '" . (int)$crm_tipus . "',
		                    crm_category_id         = '" . (int)$crm_category_id . "',
		                    crm_product_id          = '" . (int)$crm_product_id . "',
		                    egyszer_kosarba         = '" . $data['egyszer_kosarba'] . "',
		                    tax_class_id            = '" . $this->db->escape($tax_class_id) . "',
		                    sort_order              = '" . (int)$sort_order . "',
		                    egyedi_szallitas        = '" . (int)$data['egyedi_szallitas'] . "',
                            egyedi_szallitasi_dij   = '" . (float)$data['egyedi_szallitasi_dij'] . "',
                            garancia_ertek          = '" . ($data['garancia_egyseg'] == 2 ? $data['garancia_ertek']*12 : $data['garancia_ertek']) . "',
                            garancia_egyseg         = '" . (int)$data['garancia_egyseg'] . "',
		                    imagedesabled           = '" . $data['imagedesabled'] . "',
		                    correctsecuritycode     = '".  $data['correctsecuritycode']. "',
                            max_szazalek_kedvezmeny = '" . (float)$data['max_szazalek_kedvezmeny'] . "',
                            szin_meret_szukseges = '" . $data['szin_meret_szukseges'] . "',
		                    date_modified           = NOW()
		                        WHERE product_id = '" . (int)$product_id . "'";

        $this->db->query($sql);

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "'
			 WHERE product_id = '" . (int)$product_id . "'");
        }

        $data['letoltheto_kep'] = isset($data['letoltheto_kep']) ? $data['letoltheto_kep'] : "";

        $this->db->query("UPDATE " . DB_PREFIX . "product SET letoltheto = '" . $this->db->escape(html_entity_decode($data['letoltheto_kep'], ENT_QUOTES, 'UTF-8')) . "'
			 WHERE product_id = '" . (int)$product_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($data['product_description'] as $language_id => $value) {
            $value['short_description']     = isset($value['short_description'])    ? $value['short_description']   : "";
            $value['test_description']      = isset($value['test_description'])     ? $value['test_description']    : "";
            $value['garancia_description']  = isset($value['garancia_description'])  ? $value['garancia_description'] : "";

            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET
			    product_id              = '" . (int)$product_id . "',
			    language_id             = '" . (int)$language_id . "',
			    name                    = '" . $this->db->escape(html_entity_decode($value['name'], ENT_QUOTES, 'UTF-8')) . "',
			    meta_keyword            = '" . $this->db->escape($value['meta_keyword']) . "',
			    meta_description        = '" . $this->db->escape($value['meta_description']) . "',
			    short_description       = '" . $this->db->escape($value['short_description']) . "',
			    test_description        = '" . $this->db->escape($value['test_description']) . "',
			    garancia_description    = '" . $this->db->escape($value['garancia_description']) . "',
			    description             = '" . $this->db->escape($value['description']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "product_elhelyezes WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_elhelyezkedes'])) {
            foreach ($data['product_elhelyezkedes'] as $key=>$elhelyezkedes) {
                if ($key !== "NULL") {

                    $elhelyezkedes_csoport = $this->config->get('elhelyezkedes');
                    $elhelyezkedes_alcsoport = $this->config->get('elhelyezkedes_alcsoport');
                    $kell_alcsoport = 0;
                    $elhelyezkedes_sort_order           = 999;
                    $elhelyezkedes_alcsoport_sort_order = 999;

                    foreach($elhelyezkedes_csoport as $value) {

                        if ($value['elhelyezkedes_id'] == $elhelyezkedes['elhelyezkedes_id']) {
                            $elhelyezkedes_sort_order = $value['sort_order'];
                            if ($value['alcsoport_status'] == 1) {
                                $kell_alcsoport = 1;
                            } else {
                                $kell_alcsoport = 0;
                            }
                            break;
                        }
                    }

                    foreach($elhelyezkedes_alcsoport as $value) {

                        if ($value['elhelyezkedes_alcsoport_id'] == $elhelyezkedes['elhelyezkedes_alcsoport_id']) {
                            $elhelyezkedes_alcsoport_sort_order = $value['sort_order'];
                            break;
                        }
                    }

                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_elhelyezes SET
                     `product_id`                            = '" . (int)$product_id . "',
	                `elhelyezkedes_id`	                    = '" . (int)$elhelyezkedes['elhelyezkedes_id'] . "',
	                `elhelyezkedes_neve`	                = '" . $elhelyezkedes['elhelyezkedes_neve'] . "',
	                `elhelyezkedes_netto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_netto_ara'] . "',
	                `elhelyezkedes_brutto_ara`	            = '" . $elhelyezkedes['elhelyezkedes_brutto_ara'] . "',
	                `elhelyezkedes_sort_order`	            = '" . $elhelyezkedes_sort_order . "',

	                `elhelyezkedes_alcsoport_id`	        = '" . (int)$elhelyezkedes['elhelyezkedes_alcsoport_id'] . "',
	                `elhelyezkedes_alcsoport_neve`	        = '" . $elhelyezkedes['elhelyezkedes_alcsoport_neve'] . "',
	                `elhelyezkedes_alcsoport_netto_ara` 	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_netto_ara'] . "',
	                `elhelyezkedes_alcsoport_brutto_ara`	= '" . $elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'] . "',
	                `elhelyezkedes_alcsoport_sort_order`	= '" . $elhelyezkedes_alcsoport_sort_order . "',
	                `kell_alcsoport`	                    = '" . $kell_alcsoport. "',

	                `kiemelesek_id`                         = '" . (int)$elhelyezkedes['kiemelesek_id'] . "',
	                `kiemelesek_neve`                       = '" . $elhelyezkedes['kiemelesek_neve'] . "',
	                `kiemelesek_netto_ara`                  = '" . $elhelyezkedes['kiemelesek_netto_ara'] . "',
	                `kiemelesek_brutto_ara`                 = '" . $elhelyezkedes['kiemelesek_brutto_ara'] . "',
	                `sor`                                   = '" . $elhelyezkedes['sor'] . "',
	                `netto_ar`                              = '" . $elhelyezkedes['netto_ar'] . "',
	                `brutto_ar`                             = '" . $elhelyezkedes['brutto_ar'] . "',

	                `total_netto_ar`                        = '" . $elhelyezkedes['total_netto_ar'] . "',
	                `total_brutto_ar`                       = '" . $elhelyezkedes['total_brutto_ar'] . "',

	                `idoszak`                   	        = '" . (int)$elhelyezkedes['idoszak'] . "',
	                `idoszak_id`                   	        = '" . (int)$elhelyezkedes['idoszak_id'] . "',
	                `priority`                   	        = '" . (int)$elhelyezkedes['priority'] . "',
	                `datum_tol`                 	        = '" . $elhelyezkedes['datum_tol'] . "',
	                `datum_ig`          	                = '" . $elhelyezkedes['datum_ig'] . "',
	                `fizetes_elbiralas_status_id`           = '" . (int)$elhelyezkedes['fizetes_elbiralas_status_id'] . "',
	                `penzugyi_status_id`                    = '" . (int)$elhelyezkedes['penzugyi_status_id'] . "'");
                }
            }
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");

        if (!empty($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute
						    SET product_id = '" . (int)$product_id . "',
						    attribute_id = '" . (int)$product_attribute['attribute_id'] . "',
						    language_id = '" . (int)$language_id . "',
						    text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox'
                    || $product_option['type'] == 'image' || $product_option['type'] == 'checkbox_qty') {
                    $sql = "INSERT INTO " . DB_PREFIX . "product_option SET
                        product_option_id = '" . (int)$product_option['product_option_id'] . "',
                        product_id = '" . (int)$product_id . "',
                        option_id = '" . (isset($product_option['option_id']) ? (int)$product_option['option_id'] : 0) . "',
                        required = '" . (isset($product_option['required']) ? (int)$product_option['required'] : 0) . "'";
                    $query = $this->db->query($sql);

                    $product_option_id = $this->db->getLastId();

                    if (isset($product_option['product_option_value'])) {
                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            if ($product_option['type'] == 'checkbox_qty') {
                                $sql = "SELECT option_szin_id, azonosito FROM " . DB_PREFIX . "option_value WHERE option_value_id = '" .(int)$product_option_value['option_value_id']. "'";
                                $query = $this->db->query($sql);
                            }

                            $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET
                                product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "',
                                product_option_id       = '" . (int)$product_option_id . "',
                                product_id              = '" . (int)$product_id . "',
                                option_id               = '" . (int)$product_option['option_id'] . "',
                                option_value_id         = '" . (int)$product_option_value['option_value_id'] . "',

                                option_szin_id          = '" . (isset($query->row['option_szin_id']) ? (int)$query->row['option_szin_id'] : ''). "',
                                azonosito               = '" . (isset($query->row['azonosito']) ? (int)$query->row['azonosito'] : ''). "',

                                quantity                = '" . (int)$product_option_value['quantity'] . "',
                                subtract                = '" . (int)$product_option_value['subtract'] . "',
                                checked             = '" . (isset($product_option_value['checked']) ? (int)$product_option_value['checked'] : 0) . "',
                                price                   = '" . (float)$product_option_value['price'] . "',
                                price_prefix            = '" . $this->db->escape($product_option_value['price_prefix']) . "',
                                points                  = '" . (int)$product_option_value['points'] . "',
                                points_prefix           = '" . $this->db->escape($product_option_value['points_prefix']) . "',
                                weight                  = '" . (isset($product_option_value['weight']) ? (float)$product_option_value['weight'] : 0.00) . "',
                                weight_prefix           = '" . $this->db->escape($product_option_value['weight_prefix']) . "'";

                            $query = $this->db->query($sql);
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value = '" . $this->db->escape($product_option['option_value']) . "', required = '" . (int)$product_option['required'] . "'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_customer_special WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_vevo'])) {
            foreach ($data['product_vevo'] as $product_vevo) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_customer_special SET
                product_id = '" . (int)$product_id . "',
                customer_id = '" . (int)$product_vevo['customer_id'] . "',
                quantity = '" . (int)$product_vevo['quantity'] . "',
                price = '" . (float)$product_vevo['price'] . "',
                date_start = '" . $this->db->escape($product_vevo['date_start']) . "',
                date_end = '" . $this->db->escape($product_vevo['date_end']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_customer_utalvany WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['product_customer_utalvany'])) {
            foreach ($data['product_customer_utalvany'] as $product_vevo) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_customer_utalvany SET
                product_id = '" . (int)$product_id . "',
                customer_id = '" . (int)$product_vevo . "'");
            }
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $kiemelt = isset($product_special['kiemelt']) ? $product_special['kiemelt'] : 0;

                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET
				    product_id = '" . (int)$product_id . "',
				    customer_group_id = '" . (int)$product_special['customer_group_id'] . "',
				    priority = '" . (int)$product_special['priority'] . "',
				    price = '" . (float)$product_special['price'] . "',
				    kiemelt = '" . $kiemelt . "',
				    date_start = '" . $this->db->escape($product_special['date_start']) . "',
				    date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape(html_entity_decode($product_image['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_video WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_video'])) {
            foreach ($data['product_video'] as $product_video) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_video SET product_id = '" . (int)$product_id . "', video_url = '" . $product_video['video_url'] . "', sort_order = '" . (int)$product_video['sort_order'] . "', video_title = '" . $product_video['video_title'] . "'");
            }
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "product_pdf WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_pdf'])) {
            foreach ($data['product_pdf'] as $product_pdf) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_pdf SET
                    product_id  = '" . (int)$product_id . "',
                    `path`        = '" . $this->db->escape(html_entity_decode($product_pdf['image'], ENT_QUOTES, 'UTF-8')) . "',
                    sort_order  = '" . (int)$product_pdf['sort_order'] . "',
                    pdf_title   = '" . $product_pdf['pdf_title'] . "'");
            }
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $sql = "SELECT * FROM " . DB_PREFIX . "filter WHERE  filter_id = '" . (int)$filter_id . "'";

                $query = $this->db->query($sql);
                $fi_s_id = $query->row['filter_select_id'];

                $csoport_id = $query->row['filter_group_id'];

                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET
                    product_id = '" . (int)$product_id . "',
                    filter_group_id = '" . (int)$csoport_id . "',
                    filter_select_id = '" . (int)$fi_s_id . "',
                    filter_id = '" . (int)$filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "' OR product_id = '" .(int)$product_id. "'");
        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "product_package WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['product_package'])) {
            foreach ($data['product_package'] as $key=>$package) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_package SET
                    product_id      = '" . (int)$product_id . "',
                    quantity        = '" . (int)$package['quantity'] . "',
                    package_id      = '" . (int)$package['package_id'] . "',
                    price           = '" . (int)$package['price'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_accessory WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['product_accessory'])) {
            foreach ($data['product_accessory'] as $accessory_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_accessory SET
                    product_id      = '" . (int)$product_id . "',
                    accessory_id      = '" . (int)$accessory_id. "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_ajandek WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['product_ajandek'])) {
            foreach ($data['product_ajandek'] as $ajandek_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_ajandek SET
                    product_id      = '" . (int)$product_id . "',
                    ajandek_id      = '" . (int)$ajandek_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_helyettesito WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['product_helyettesito'])) {
            foreach ($data['product_helyettesito'] as $helyettesito_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_helyettesito SET
                    product_id      = '" . (int)$product_id . "',
                    helyettesito_id = '" . (int)$helyettesito_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_general_accessory WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['product_general_accessory'])) {
            foreach ($data['product_general_accessory'] as $general_accessory_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_general_accessory SET
                    product_id      = '" . (int)$product_id . "',
                    general_accessory_id      = '" . (int)$general_accessory_id. "'");
            }
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $value) {
                if ($value['points'] > 0) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$value['points'] . "'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout) {
                if ($layout['layout_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
                }
            }
        }


        if (isset($data['product_tag'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_tag WHERE product_id = '" . (int)$product_id. "'");

            foreach ($data['product_tag'] as $language_id => $value) {
                if ($value) {
                    $tags = explode(',', $value);

                    foreach ($tags as $tag) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_tag SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', tag = '" . $this->db->escape(trim($tag)) . "'");
                    }
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id. "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        if(isset($data['product_image_in_places'])) {
            $this->load->model('catalog/productimageplaces');
            $this->model_catalog_productimageplaces->setDatas($product_id, $data['product_image_in_places']);
        }

        $this->cache->delete('product');
    }

    public function editProductOptionAjax($data,$product_id){

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox'
                    || $product_option['type'] == 'image' || $product_option['type'] == 'checkbox_qty') {
                    $sql = "INSERT INTO " . DB_PREFIX . "product_option SET
                        product_option_id = '" . (int)$product_option['product_option_id'] . "',
                        product_id = '" . (int)$product_id . "',
                        option_id = '" . (isset($product_option['option_id']) ? (int)$product_option['option_id'] : 0) . "',
                        required = '" . (isset($product_option['required']) ? (int)$product_option['required'] : 0) . "'";
                    $query = $this->db->query($sql);

                    $product_option_id = $this->db->getLastId();

                    if (isset($product_option['product_option_value'])) {
                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            if ($product_option['type'] == 'checkbox_qty') {
                                $sql = "SELECT option_szin_id, azonosito FROM " . DB_PREFIX . "option_value WHERE option_value_id = '" .(int)$product_option_value['option_value_id']. "'";
                                $query = $this->db->query($sql);
                            }

                            $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET
                                product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "',
                                product_option_id       = '" . (int)$product_option_id . "',
                                product_id              = '" . (int)$product_id . "',
                                option_id               = '" . (int)$product_option['option_id'] . "',
                                option_value_id         = '" . (int)$product_option_value['option_value_id'] . "',

                                option_szin_id          = '" . (isset($query->row['option_szin_id']) ? (int)$query->row['option_szin_id'] : ''). "',
                                azonosito               = '" . (isset($query->row['azonosito']) ? (int)$query->row['azonosito'] : ''). "',

                                quantity                = '" . (int)$product_option_value['quantity'] . "',
                                subtract                = '" . (int)$product_option_value['subtract'] . "',
                                checked             = '" . (isset($product_option_value['checked']) ? (int)$product_option_value['checked'] : 0) . "',
                                price                   = '" . (float)$product_option_value['price'] . "',
                                price_prefix            = '" . $this->db->escape($product_option_value['price_prefix']) . "',
                                points                  = '" . (int)$product_option_value['points'] . "',
                                points_prefix           = '" . $this->db->escape($product_option_value['points_prefix']) . "',
                                weight                  = '" . (isset($product_option_value['weight']) ? (float)$product_option_value['weight'] : 0.00) . "',
                                weight_prefix           = '" . $this->db->escape($product_option_value['weight_prefix']) . "'";

                            $query = $this->db->query($sql);
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value = '" . $this->db->escape($product_option['option_value']) . "', required = '" . (int)$product_option['required'] . "'");
                }
            }
        }
    }

    public function copyProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        if ($query->num_rows) {
            $data = array();

            $data = $query->row;

            $data['keyword'] = '';

            $data['status'] = '0';

            $data = array_merge($data, array('product_attribute'        => $this->getProductAttributes($product_id)));
            $data = array_merge($data, array('product_description'      => $this->getProductDescriptions($product_id)));
            $data = array_merge($data, array('product_discount'         => $this->getProductDiscounts($product_id)));
            $data = array_merge($data, array('product_image'            => $this->getProductImages($product_id)));
            $data = array_merge($data, array('product_pdf'              => $this->getProductPdfs($product_id)));
            $data = array_merge($data, array('product_video'            => $this->getProductVideos($product_id)));
            $data = array_merge($data, array('product_filter'           => $this->getProductFilters($product_id)));
            $data = array_merge($data, array('product_option'           => $this->getProductOptions($product_id)));
            $data = array_merge($data, array('product_related'          => $this->getProductRelated($product_id)));
            $data = array_merge($data, array('product_package'          => $this->getProductPackage($product_id)));
            $data = array_merge($data, array('product_accessory'        => $this->getProductAccessory($product_id)));
            $data = array_merge($data, array('product_general_accessory' => $this->getProductGeneralAccessory($product_id)));
            $data = array_merge($data, array('product_helyettesito'     => $this->getProductHelyettesito($product_id)));
            $data = array_merge($data, array('product_reward'           => $this->getProductRewards($product_id)));
            $data = array_merge($data, array('product_special'          => $this->getProductSpecials($product_id)));
            $data = array_merge($data, array('product_tag'              => $this->getProductTags($product_id)));
            $data = array_merge($data, array('product_category'         => $this->getProductCategories($product_id)));
            $data = array_merge($data, array('product_download'         => $this->getProductDownloads($product_id)));
            $data = array_merge($data, array('product_layout'           => $this->getProductLayouts($product_id)));
            $data = array_merge($data, array('product_store'            => $this->getProductStores($product_id)));
            $data = array_merge($data, array('vatera'                   => $this->getProductVatera($product_id)));
            $data = array_merge($data, array('product_customer'         => $this->getProductCustomer($product_id)));
            $data = array_merge($data, array('product_customer_utalvany' => $this->getProductVevoUtalvany($product_id)));
            $data = array_merge($data, array('product_customer_special' => $this->getProductVevo($product_id)));
            $data = array_merge($data, array('product_elhelyezkedes'    => $this->getProductElhelyezkedes($product_id)));

            $this->addProduct($data);
        }
    }

    public function deleteProduct($product_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_pdf WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_video WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_package WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_package WHERE package_id = '" . (int)$product_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_accessory WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_accessory WHERE accessory_id = '" . (int)$product_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_general_accessory WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_general_accessory WHERE general_accessory_id = '" . (int)$product_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_helyettesito WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_helyettesito WHERE helyettesito_id = '" . (int)$product_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_tag WHERE product_id='" . (int)$product_id. "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_customer WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_customer_special WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_customer_utalvany WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_elhelyezes WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id. "'");
        if (file_exists(DIR_TEMPLATE."catalog/vatera.tpl")){
            $this->db->query("DELETE FROM vatera WHERE product_id = '" . (int)$product_id . "'");}

        $this->cache->delete('product');
    }

    public function getProduct($product_id) {

        $sql = "SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "') AS keyword FROM " . DB_PREFIX . "product p
		    LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
		    WHERE p.product_id = '" . (int)$product_id . "'
		    AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $query = $this->db->query($sql);

        return $query->row;
    }


    public function getCustomerProducts($data = array()) {
        $cproArry = array();
        $cusProId = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product_customer`")->rows;
        if($cusProId){
            for($i=0;$i<count($cusProId);$i++){
                $cproArry[] = $cusProId[$i]['product_id'];
            }
            $custProID = implode(',',$cproArry);
        }else{
            $custProID = 0;
        }


        $sql = "SELECT * , c.firstname, c.lastname, p.status FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        if (!empty($data['filter_category_id'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        $sql .= "LEFT JOIN " .DB_PREFIX. "product_customer pc ON (p.product_id = pc.product_id)";
        $sql .= "LEFT JOIN " .DB_PREFIX. "customer c ON (pc.customer_id = c.customer_id)";

        $sql .= " WHERE p.product_id IN (".$custProID.") AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND LCASE(pd.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_customer'])) {
            $sql .= " AND LCASE( concat(c.firstname, ' ',c.lastname))  LIKE '" . $this->db->escape(utf8_strtolower($data['filter_customer'])) . "%'";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND LCASE(p.model) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'";
        }

        if (!empty($data['filter_price'])) {
            $sql .= " AND p.price >='" . $data['filter_price'] . "'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }

        $sql .= " GROUP BY p.product_id";

        $sort_data = array(
            'pd.name',
            'p.model',
            'p.price',
            'c.firstname',
            'p.quantity',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }
    public function getTotalCustomerProducts($data = array()) {

        $cproArry = array();
        $cusProId = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product_customer`")->rows;
        if($cusProId){
            for($i=0;$i<count($cusProId);$i++){
                $cproArry[] = $cusProId[$i]['product_id'];
            }
            $custProID = implode(',',$cproArry);
        }else{
            $custProID = 0;
        }

        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        if (!empty($data['filter_category_id'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        $sql .= "LEFT JOIN " .DB_PREFIX. "product_customer pc ON (p.product_id = pc.product_id)";
        $sql .= "LEFT JOIN " .DB_PREFIX. "customer c ON (pc.customer_id = c.customer_id)";

        $sql .= " WHERE p.product_id IN (".$custProID.") AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND LCASE(pd.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_customer'])) {
            $sql .= " AND LCASE( concat(c.firstname, ' ',c.lastname))  LIKE '" . $this->db->escape(utf8_strtolower($data['filter_customer'])) . "%'";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND LCASE(p.model) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'";
        }

        if (!empty($data['filter_price'])) {
            $sql .= " AND p.price >='" . $data['filter_price'] . "'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getProducts($data = array()) {
        if ($data) {

            $sql = "SELECT  *, p.product_id as termek_id FROM " . DB_PREFIX . "product p
			    LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			    LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";

            if (isset($data['filter_category']) && !empty($data['filter_category']) ){
                $data['filter_category_id'] = $data['filter_category'];
            }

            $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

            if (!empty($data['filter_product_id'])) {
                $sql .= " AND p.product_id = '".$data['filter_product_id']."'";
            }

            if (!empty($data['filter_package_products'])) {
                $sql .= " AND p.product_id NOT IN (".$data['filter_package_products'].")";
            }

            if (!empty($data['filter_name'])) {
                $sql .= " AND LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
            }

            if (!empty($data['filter_model'])) {
                $sql .= " AND LCASE(p.model) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'";
            }

            if (!empty($data['filter_cikkszam'])) {
                $sql .= " AND LCASE(p.cikkszam) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_cikkszam'])) . "%'";
            }

            if (!empty($data['filter_cikkszam2'])) {
                $sql .= " AND LCASE(p.cikkszam2) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_cikkszam2'])) . "%'";
            }

            if (!empty($data['filter_price'])) {
                $sql .= " AND p.price >='" . $data['filter_price'] . "'";
            }

            if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
                $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
            }

            if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
                $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
            }

            if (!empty($data['filter_modify_date'])) {
                $sql .= " AND p.date_modified LIKE '" . $data['filter_modify_date'] . "%'";
            }

            if (!empty($data['filter_date_added'])) {
                $sql .= " AND p.date_added LIKE '" . $data['filter_date_added'] . "%'";
            }

            if (isset($data['filter_kivetel']) && $data['filter_kivetel'] ) {
                $sql .= " AND p.product_id NOT IN(".$data['filter_kivetel'].")";
            }



            if (!empty($data['filter_category_id'])) {
                if (!empty($data['filter_sub_category'])) {
                    $implode_data = array();

                    $implode_data[] = "category_id = '" . (int)$data['filter_category_id'] . "'";

                    $this->load->model('catalog/category');

                    $categories = $this->model_catalog_category->getCategories($data['filter_category_id']);

                    foreach ($categories as $category) {
                        $implode_data[] = "p2c.category_id = '" . (int)$category['category_id'] . "'";
                    }

                    $sql .= " AND (" . implode(' OR ', $implode_data) . ")";
                } else {
                    $sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
                }
            }

            $sql .= " GROUP BY p.product_id";

            $sort_data = array(
                'p.product_id',
                'pd.name',
                'p.model',
                'p.date_added',
                'p.date_modified',
                'p.cikkszam',
                'p.cikkszam2',
                'p.kifuto',
                'p.ujdonsag',
                'p.elorendeles',
                'p.megrendelem',
                'p.ar_tiltasa',
                'p.garancia_ertek',
                'p.price',
                'p.quantity',
                'p.status',
                'p.sort_order',
                'p2c.category_id'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY pd.name";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }


            //$valami = mysql_query($sql);

            //$i = 1;

            $vissza = array();
            //while ($result = mysql_fetch_assoc($valami)) {
            //  $vissza[$i] = $result;
//                $vissza[$i]['product_id'] = $result['termek_id'];
//                $i++;
//            }

            $query = $this->db->query($sql);
            foreach ($query->rows as $key=>$result) {
                $vissza[$key] = $result;
                $vissza[$key]['product_id'] = $result['termek_id'];
            }

            //return $query->rows;

            return $vissza;
        } else {
            $product_data = $this->cache->get('product.' . (int)$this->config->get('config_language_id'));

            if (!$product_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p
				    LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				    WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY pd.name ASC");

                $product_data = $query->rows;

                $this->cache->set('product.' . (int)$this->config->get('config_language_id'), $product_data);
            }

            return $product_data;
        }
    }

    public function getProductsByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
		    LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)
		     WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");

        return $query->rows;
    }

    public function getProductDescriptions($product_id) {
        $product_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_description_data[$result['language_id']] = array(
                'name'                  => $result['name'],
                'description'           => $result['description'],
                'short_description'     => $result['short_description'],
                'meta_keyword'          => $result['meta_keyword'],
                'test_description'      => $result['test_description'],
                'meta_description'      => $result['meta_description'],
                'garancia_description'  => $result['garancia_description']
            );
        }

        return $product_description_data;
    }

    public function getProductAttributes($product_id) {
        $product_attribute_data = array();

        $product_attribute_query = $this->db->query("SELECT pa.attribute_id, ad.name FROM " . DB_PREFIX . "product_attribute pa
		    LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id)
		    LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id)
		    WHERE pa.product_id = '" . (int)$product_id . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY pa.attribute_id");

        foreach ($product_attribute_query->rows as $product_attribute) {
            $product_attribute_description_data = array();

            $product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

            foreach ($product_attribute_description_query->rows as $product_attribute_description) {
                $nev = str_replace('&quot;','"',$product_attribute_description['text']);
                $nev = str_replace(';',' ',$nev);
                $product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $nev);
            }

            $product_attribute_data[] = array(
                'attribute_id'                  => $product_attribute['attribute_id'],
                'name'                          => $product_attribute['name'],
                'product_attribute_description' => $product_attribute_description_data
            );
        }

        return $product_attribute_data;
    }

    public function getProductAttributes_Group($product_id) {
        $attribute = $this->getProductAttributes($product_id);

        return $attribute;
    }

    public function getProductOptions($product_id) {
        $product_option_data = array();

        $sql = "SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order";
        $product_option_query = $this->db->query($sql);

        foreach ($product_option_query->rows as $product_option) {
            if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox'
                || $product_option['type'] == 'image' || $product_option['type'] == 'checkbox_qty') {
                $product_option_value_data = array();

                $product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov
                        LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id)
                        LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id)
                        WHERE   pov.product_option_id = '" . (int)$product_option['product_option_id'] . "'
                            AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                            ORDER BY LCASE(ovd.name)");
                            //ORDER BY ov.sort_order");

                foreach ($product_option_value_query->rows as $product_option_value) {
                    $product_option_value_data[] = array(
                        'product_option_value_id' => $product_option_value['product_option_value_id'],
                        'option_value_id'         => $product_option_value['option_value_id'],
                        'name'                    => $product_option_value['name'],
                        'image'                   => $product_option_value['image'],
                        'quantity'                => $product_option_value['quantity'],
                        'subtract'                => $product_option_value['subtract'],
                        'checked'                 => $product_option_value['checked'],
                        'price'                   => $product_option_value['price'],
                        'price_prefix'            => $product_option_value['price_prefix'],
                        'points'                  => $product_option_value['points'],
                        'points_prefix'           => $product_option_value['points_prefix'],
                        'weight'                  => $product_option_value['weight'],
                        'weight_prefix'           => $product_option_value['weight_prefix'],
                        'option_szin_id'          => $product_option_value['option_szin_id'],
                        'azonosito'               => $product_option_value['azonosito']
                    );
                }

                $product_option_data[] = array(
                    'product_option_id'    => $product_option['product_option_id'],
                    'option_id'            => $product_option['option_id'],
                    'name'                 => $product_option['name'],
                    'type'                 => $product_option['type'],
                    'product_option_value' => $product_option_value_data,
                    'required'             => $product_option['required']
                );
            } else {
                $product_option_data[] = array(
                    'product_option_id' => $product_option['product_option_id'],
                    'option_id'         => $product_option['option_id'],
                    'name'              => $product_option['name'],
                    'type'              => $product_option['type'],
                    'option_value'      => $product_option['option_value'],
                    'required'          => $product_option['required']
                );
            }
        }

        return $product_option_data;
    }

    public function getProductImages($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order");

        return $query->rows;
    }

    public function getProductVideos($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_video WHERE product_id = '" . (int)$product_id . "'");

        return $query->rows;
    }

    public function getProductPdfs($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_pdf WHERE product_id = '" . (int)$product_id . "'");

        return $query->rows;
    }

    public function getProductCustomer($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_customer WHERE product_id = '" . (int)$product_id . "'");

        return $query->rows;
    }

    public function getProductDiscounts($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, priority, price");

        return $query->rows;
    }

    public function getProductVevo($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_customer_special WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, price");

        return $query->rows;
    }

    public function getProductVevoUtalvany($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_customer_utalvany WHERE product_id = '" . (int)$product_id. "'");

        return $query->rows;
    }



    public function getProductSpecials($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");
        return $query->rows;
    }

    public function getProductElhelyezkedes($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_elhelyezes WHERE product_id = '" . (int)$product_id . "' ORDER BY datum_tol");

        return $query->rows;
    }

    public function getProductRewards($product_id) {
        $product_reward_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
        }

        return $product_reward_data;
    }

    public function getProductVatera($product_id) {
        $query = $this->db->query("SELECT * FROM vatera WHERE product_id = '" . (int)$product_id . "'" );
        return $query->rows;
    }

    public function getProductDownloads($product_id) {
        $product_download_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_download_data[] = $result['download_id'];
        }

        return $product_download_data;
    }

    public function getProductStores($product_id) {
        $product_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_store_data[] = $result['store_id'];
        }

        return $product_store_data;
    }

    public function getProductLayouts($product_id) {
        $product_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $product_layout_data;
    }

    public function getProductFilters($product_id) {
        $product_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_filter_data[] = $result['filter_id'];
        }

        return $product_filter_data;
    }

    public function getProductCategories($product_id) {
        $product_category_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_category_data[] = $result['category_id'];
        }

        return $product_category_data;
    }

    public function getProductRelated($product_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = $result['related_id'];
        }

        return $product_related_data;
    }
    public function getProductAjanlottTartozekok($product_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_accessory WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = $result['accessory_id'];
        }

        return $product_related_data;
    }
    public function getProductAltalanosTartozekok($product_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_general_accessory WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = $result['general_accessory_id'];
        }

        return $product_related_data;
    }

    public function getProductAjandek($product_id) {
        $product_ajandek_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_ajandek WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_ajandek_data[] = $result['ajandek_id'];
        }

        return $product_ajandek_data;
    }

    public function getProductHelyettesito($product_id) {
        $product_helyettesito_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_helyettesito WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_helyettesito_data[] = $result['helyettesito_id'];
        }

        return $product_helyettesito_data;
    }


    public function getProductAccessory($product_id) {
        $product_accessory_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_accessory WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_accessory_data[] = $result['accessory_id'];
        }
        return $product_accessory_data;
    }

    public function getProductGeneralAccessory($product_id) {
        $product_general_accessory_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_general_accessory WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_general_accessory_data[] = $result['general_accessory_id'];
        }
        return $product_general_accessory_data;
    }

    public function getProductPackage($product_id) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_package WHERE product_id = '" . (int)$product_id . "' ORDER BY product_id");

        return $query->rows;
    }

    public function getProductTags($product_id) {
        $product_tag_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_tag WHERE product_id = '" . (int)$product_id . "'");

        $tag_data = array();

        foreach ($query->rows as $result) {
            $tag_data[$result['language_id']][] = $result['tag'];
        }

        foreach ($tag_data as $language => $tags) {
            $product_tag_data[$language] = implode(',', $tags);
        }

        return $product_tag_data;
    }

    public function getTotalProducts($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p
		    LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        if (isset($data['filter_category']) && !empty($data['filter_category']) ){
            $data['filter_category_id'] = $data['filter_category'];
        }

        if (!empty($data['filter_category_id'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_product_id'])) {
            $sql .= " AND p.product_id = '".$data['filter_product_id']."'";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND LCASE(pd.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND LCASE(p.model) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'";
        }
        if (!empty($data['filter_cikkszam'])) {
            $sql .= " AND LCASE(p.cikkszam) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_cikkszam'])) . "%'";
        }
        if (!empty($data['filter_cikkszam2'])) {
            $sql .= " AND LCASE(p.cikkszam2) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_cikkszam2'])) . "%'";
        }

        if (!empty($data['filter_price'])) {
            $sql .= " AND p.price >='" . $data['filter_price'] . "'";
        }

        if (!empty($data['filter_modify_date'])) {
            $sql .= " AND p.date_modified LIKE '" . $data['filter_modify_date'] . "%'";
        }
        if (!empty($data['filter_date_added'])) {
            $sql .= " AND p.date_added LIKE '" . $data['filter_date_added'] . "%'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }



        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_sub_category'])) {
                $implode_data = array();

                $implode_data[] = "p2c.category_id = '" . (int)$data['filter_category_id'] . "'";

                $this->load->model('catalog/category');

                $categories = $this->model_catalog_category->getCategories($data['filter_category_id']);

                foreach ($categories as $category) {
                    $implode_data[] = "p2c.category_id = '" . (int)$category['category_id'] . "'";
                }

                $sql .= " AND (" . implode(' OR ', $implode_data) . ")";
            } else {
                $sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
            }
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalProductsByTaxClassId($tax_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByStockStatusId($stock_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByWeightClassId($weight_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByPackagingClassId($packaging_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE packaging_class_id = '" . (int)$packaging_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByLengthClassId($length_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int)$length_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByDownloadId($download_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByManufacturerId($manufacturer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByAttributeId($attribute_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByOptionId($option_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$option_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

        return $query->row['total'];
    }

    public function getProductsInAttributes($data) {

        $query = $this->getProductsInCsv($data,"sor");


        $vissza = array();
        if ($query->num_rows > 0) {
            foreach($query->rows as $value)  {
                $nev = str_replace('&quot;','"',$value['name']);
                $vissza[] = array(
                    'product_id'    => $value['product_id'],
                    'model'         => $value['model'],
                    'name'          => strlen($nev) > 20 ? mb_substr($nev,0,20, "UTF-8")."..." : $nev,
                    'attributes'    => $this->getProductAttributes_Group($value['product_id'])
                );

            }
        } else $vissza = false;

        return $vissza;
    }

    public function getProductsInRelateds($data) {

        $query = $this->getProductsInCsv($data,"sor");
        $query_termekek = $this->getProductsInCsv($data,"oszlop");


        $vissza['termekek'] = array();

        if ($query->num_rows > 0) {
            foreach($query->rows as $value)  {
                $vissza['termekek'][] = array(
                    'product_id'    => $value['product_id'],
                    'model'         => $value['model'],
                    'name'          => strlen($value['name']) > 20 ? mb_substr($value['name'],0,20, "UTF-8")."..." : $value['name'],
                    'kapcsolodo'    => $this->getProductRelated($value['product_id'])
                );
            }
        } else $vissza['termekek'] = false;

        $vissza['kapcsolodo'] = array();
        if ($query_termekek->num_rows > 0) {
            foreach($query_termekek->rows as $value)  {
                $vissza['kapcsolodo'][] = array(
                    'product_id'    => $value['product_id'],
                    'model'         => $value['model'],
                    'name'          => strlen($value['name']) > 20 ? mb_substr($value['name'],0,20, "UTF-8")."..." : $value['name']
                );
            }
        } else $vissza['kapcsolodo'] = false;

        if (! $vissza['kapcsolodo'] &&  $vissza['termekek']) $vissza = false;

        return $vissza;
    }


    public function getProductsInAjanlott($data) {

        $query = $this->getProductsInCsv($data,"sor");
        $query_termekek = $this->getProductsInCsv($data,"oszlop");

        $vissza['termekek'] = array();

        if ($query->num_rows > 0) {
            foreach($query->rows as $value)  {
                $vissza['termekek'][] = array(
                    'product_id'    => $value['product_id'],
                    'model'         => $value['model'],
                    'name'          => strlen($value['name']) > 20 ? mb_substr($value['name'],0,20, "UTF-8")."..." : $value['name'],
                    'kapcsolodo'    => $this->getProductAjanlottTartozekok($value['product_id'])
                );
            }
        } else $vissza['termekek'] = false;

        $vissza['kapcsolodo'] = array();
        if ($query_termekek->num_rows > 0) {
            foreach($query_termekek->rows as $value)  {
                $vissza['kapcsolodo'][] = array(
                    'product_id'    => $value['product_id'],
                    'model'         => $value['model'],
                    'name'          => strlen($value['name']) > 20 ? mb_substr($value['name'],0,20, "UTF-8")."..." : $value['name']
                );
            }
        } else $vissza['kapcsolodo'] = false;

        if (! $vissza['kapcsolodo'] &&  $vissza['termekek']) $vissza = false;

        return $vissza;
    }


    public function getProductsInAltalanos($data) {

        $query = $this->getProductsInCsv($data,"sor");
        $query_termekek = $this->getProductsInCsv($data,"oszlop");

        $vissza['termekek'] = array();

        if ($query->num_rows > 0) {
            foreach($query->rows as $value)  {
                $vissza['termekek'][] = array(
                    'product_id'    => $value['product_id'],
                    'model'         => $value['model'],
                    'name'          => strlen($value['name']) > 20 ? mb_substr($value['name'],0,20, "UTF-8")."..." : $value['name'],
                    'kapcsolodo'    => $this->getProductAltalanosTartozekok($value['product_id'])
                );
            }
        } else $vissza['termekek'] = false;

        $vissza['kapcsolodo'] = array();
        if ($query_termekek->num_rows > 0) {
            foreach($query_termekek->rows as $value)  {
                $vissza['kapcsolodo'][] = array(
                    'product_id'    => $value['product_id'],
                    'model'         => $value['model'],
                    'name'          => strlen($value['name']) > 20 ? mb_substr($value['name'],0,20, "UTF-8")."..." : $value['name']
                );
            }
        } else $vissza['kapcsolodo'] = false;

        if (! $vissza['kapcsolodo'] &&  $vissza['termekek']) $vissza = false;

        return $vissza;
    }

    public function getProductsInCsv($data,$termek_sor) {
        $sql = "SELECT  p.product_id, p.model, pd.name FROM " . DB_PREFIX . "product p
			    LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_model'])) {
            $sql .= " AND LCASE(p.model) LIKE '" . $this->db->escape(utf8_strtolower( ($termek_sor == "sor" ? $data['filter_model'] : $data['filter_model_termekek']) )) . "%'";
        }
        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . ($termek_sor == "sor" ? (int)$data['filter_status'] : (int)$data['filter_status']) . "'";
        }

        $sql .= " GROUP BY p.product_id";
        $sort_data = array(
            'pd.name',
            'p.model',
            'p.cikkszam',
            'p.cikkszam2',
            'p.price',
            'p.quantity',
            'p.status',
            'p.sort_order',
            'p2c.category_id'
        );
        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }
        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }
            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        return $this->db->query($sql);
    }

    public function addCsvProductsAttributes($products,$attributes){
        $output = fopen(DIR_ARUHAZ."csv/jellemzok.csv", "w");
        if (!$output) {
            $output = fopen("./../csv/jellemzok.csv", "w");
        }

        if (!$output) {
            $output = fopen("../csv/jellemzok.csv", "w");
        }

        if (!$output) {
            $output = fopen("./csv/jellemzok.csv", "w");
        }

        if (!$output) {
            $output = fopen("/csv/jellemzok.csv", "w");
        }


        $fejlec = array();
        $fejlec[] = "Azonosító";
        $fejlec[] = "Modell";
        $fejlec[] = "Név";
        foreach ($attributes as $value) {
            $fejlec[] = strlen($value['attribute_group']) > 20 ? mb_substr($value['attribute_group'],0,20, "UTF-8")."..." : $value['attribute_group'];
        }
        $fejlec =  implode("@&@@&@", $fejlec);
        $fejlec = str_replace(";"," ",$fejlec);
        $fejlec = str_replace("@&@@&@",";",$fejlec);
        fputs($output, $fejlec."\n", strlen($fejlec)+1);


        $fejlec = array();
        $fejlec[] = "";
        $fejlec[] = "";
        $fejlec[] = "";
        foreach ($attributes as $value) {
            $atalakit =  strlen($value['name']) > 20 ? mb_substr($value['name'],0,20, "UTF-8")."..." : $value['name'];
            $atalakit = str_replace("'"," ",$atalakit);
            //$fejlec[] = str_replace('"'," ",$atalakit);
            $fejlec[] = $atalakit;
        }
        $fejlec =  implode("@&@@&@", $fejlec);
        $fejlec = str_replace(";"," ",$fejlec);
        $fejlec = str_replace("@&@@&@",";",$fejlec);
        fputs($output, $fejlec."\n", strlen($fejlec)+1);


        $fejlec = array();
        $fejlec[] = "";
        $fejlec[] = "";
        $fejlec[] = "";
        foreach ($attributes as $value) {
            $fejlec[] = $value['attribute_id'];
        }
        $fejlec =  implode("@&@@&@", $fejlec);
        $fejlec = str_replace(";"," ",$fejlec);
        $fejlec = str_replace("@&@@&@",";",$fejlec);
        fputs($output, $fejlec."\n", strlen($fejlec)+1);

        foreach($products as $product) {
            $newLine = $product['product_id'];
            $newLine .= ";";
            $newLine .= str_replace(";"," ",$product['model']);
            $newLine .= ";";
            $newLine .= str_replace(";"," ",$product['name']);
            foreach($attributes as $attribute) {
                $talalt = false;
                $newLine .= ";";
                if ($product['attributes']) {
                    foreach($product['attributes'] as $p_attr) {
                        if ($p_attr['attribute_id'] == $attribute['attribute_id']){
                            $newLine .= !empty($p_attr['product_attribute_description'][2]['text']) ? $p_attr['product_attribute_description'][2]['text'] : "Igen";
                            $talalt = true;
                            break;
                        }
                    }
                    if (!$talalt) {
                        $newLine .= "";
                    }
                }
            }
            fputs($output, $newLine."\n", strlen($newLine)+1);

        }

        fclose($output);

        $file = DIR_ARUHAZ."csv/jellemzok.csv";
        if (!file_exists($file)) {
            $file = "./../csv/jellemzok.csv";
        }
        if (!file_exists($file)) {
            $file = "../csv/jellemzok.csv";
        }
        if (!file_exists($file)) {
            $file = "/csv/jellemzok.csv";
        }

        if (!file_exists($file)) {
            $file = "./csv/jellemzok.csv";
        }

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            unlink($file);
            exit;
        }
    }


    public function addCsvProductsReleted($products,$kapcsolodo,$file_name){
        $output = fopen(DIR_ARUHAZ."csv/".$file_name.".csv", "w");
        if (!$output) {
            $output = fopen("./../csv/".$file_name.".csv", "w");
        }

        if (!$output) {
            $output = fopen("../csv/".$file_name.".csv", "w");
        }

        if (!$output) {
            $output = fopen("./csv/".$file_name.".csv", "w");
        }

        if (!$output) {
            $output = fopen("/csv/".$file_name.".csv", "w");
        }


        $fejlec = array();
        $fejlec[] = "Azonosító";
        $fejlec[] = "Modell";
        $fejlec[] = "Név";
        foreach ($kapcsolodo as $value) {
            $fejlec[] = $value['product_id'];
        }

        $fejlec =  implode("@&@@&@", $fejlec);
        $fejlec = str_replace(";"," ",$fejlec);
        $fejlec = str_replace("@&@@&@",";",$fejlec);
        fputs($output, $fejlec."\n", strlen($fejlec)+1);

        $fejlec = array();
        $fejlec[] = "";
        $fejlec[] = "";
        $fejlec[] = "";
        foreach ($kapcsolodo as $value) {
            $fejlec[] = $value['model'];
        }
        $fejlec =  implode("@&@@&@", $fejlec);
        $fejlec = str_replace(";"," ",$fejlec);
        $fejlec = str_replace("@&@@&@",";",$fejlec);
        fputs($output, $fejlec."\n", strlen($fejlec)+1);

        $fejlec = array();
        $fejlec[] = "";
        $fejlec[] = "";
        $fejlec[] = "";
        foreach ($kapcsolodo as $value) {
            $atalakit = strlen($value['name']) > 20 ? mb_substr($value['name'],0,20, "UTF-8")."..." : $value['name'];
            $atalakit = str_replace("'"," ",$atalakit);
            $fejlec[] = str_replace('"'," ",$atalakit);
        }

        $fejlec =  implode("@&@@&@", $fejlec);
        $fejlec = str_replace(";"," ",$fejlec);
        $fejlec = str_replace("@&@@&@",";",$fejlec);
        fputs($output, $fejlec."\n", strlen($fejlec)+1);
        fputs($output, "\n", strlen($fejlec)+1);

        foreach($products as $product) {
            $newLine = $product['product_id'];
            $newLine .= ";";
            $newLine .= str_replace(";"," ",$product['model']);
            $newLine .= ";";
            $atalakit = str_replace(";"," ",$product['name']);
            $atalakit = str_replace('"'," ",$atalakit);
            $atalakit = str_replace("'"," ",$atalakit);
            $newLine .= $atalakit;

            $tarolt_id = array();
            foreach($kapcsolodo as $kapcs) {
                $talalt = false;
                $newLine .= ";";
                if ($product['kapcsolodo']) {
                    foreach($product['kapcsolodo'] as $p_kapcs) {
                        if ($p_kapcs == $kapcs['product_id']){
                            $tarolt_id[] = $p_kapcs;
                            $newLine .= "X";
                            $talalt = true;
                            break;
                        }
                    }
                    if (!$talalt) {
                        $newLine .= "";
                    }
                }
            }

            foreach($product['kapcsolodo'] as $related) {
                if ( !in_array($related, $tarolt_id) ) {
                    $newLine .= ";";
                    $newLine .=$related;
                }

            }
            fputs($output, $newLine."\n", strlen($newLine)+1);

        }

        fclose($output);

        $file = DIR_ARUHAZ."csv/".$file_name.".csv";
        if (!file_exists($file)) {
            $file = "./../csv/".$file_name.".csv";
        }
        if (!file_exists($file)) {
            $file = "../csv/".$file_name.".csv";
        }
        if (!file_exists($file)) {
            $file = "/csv/".$file_name.".csv";
        }

        if (!file_exists($file)) {
            $file = "./csv/".$file_name.".csv";
        }

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            unlink($file);

            exit;
        }
    }

    public function csvImportAttribute($data) {
        foreach($data as $key=>$value) {
            $sql = "DELETE FROM ".DB_PREFIX."product_attribute WHERE product_id='".$value['product_id']."'";
            $this->db->query($sql);
            foreach($value as $attr_key=>$attribute) {
                if ($attr_key != 'product_id' && $attr_key != 'model' && $attr_key != 'name') {
                    if (!empty($attribute)){
                        $sql = "INSERT INTO ".DB_PREFIX."product_attribute
                            SET product_id='".$value['product_id']."',
                                attribute_id='".$attr_key."',
                                text='".$attribute."',
                                language_id='".$this->config->get('config_language_id')."'";
                        $this->db->query($sql);
                    }
                }
            }
        }
    }

    public function csvImportRelated($data) {
        foreach($data as $key=>$value) {
            if ($key > 3) {
                $sql = "DELETE FROM ".DB_PREFIX."product_related WHERE
                    product_id='".$value['product_id']."' OR
                    related_id='".$value['product_id']."'";
                $this->db->query($sql);


                foreach($value as $attr_key=>$attribute) {
                    if ($attr_key != 'product_id' && $attr_key != 'model' && $attr_key != 'name') {
                        if (!empty($attribute)){
                            $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_related
                                SET product_id='".$value['product_id']."',
                                    related_id='".( (isset($data[0][$attr_key]) && !empty($data[0][$attr_key])) ? $data[0][$attr_key] : $attribute)."'";
                            $this->db->query($sql);

                            $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_related
                                SET product_id='".( (isset($data[0][$attr_key]) && !empty($data[0][$attr_key])) ? $data[0][$attr_key] : $attribute)."',
                                    related_id='".$value['product_id']."'";
                            $this->db->query($sql);
                        }
                    }
                }
            }
        }
        return true;
    }

    public function csvImportAjanlott($data) {
        foreach($data as $key=>$value) {
            if ($key > 3) {
                $sql = "DELETE FROM ".DB_PREFIX."product_accessory WHERE
                    product_id='".$value['product_id']."'";
                $this->db->query($sql);


                foreach($value as $attr_key=>$attribute) {
                    if ($attr_key != 'product_id' && $attr_key != 'model' && $attr_key != 'name') {
                        if (!empty($attribute)){
                            $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_accessory
                                SET product_id='".$value['product_id']."',
                                    accessory_id='".( (isset($data[0][$attr_key]) && !empty($data[0][$attr_key])) ? $data[0][$attr_key] : $attribute)."'";
                            $this->db->query($sql);
                        }
                    }
                }
            }
        }
        return true;
    }

    public function csvImportAltalanos($data) {
        foreach($data as $key=>$value) {
            if ($key > 3) {
                $sql = "DELETE FROM ".DB_PREFIX."product_general_accessory WHERE
                    product_id='".$value['product_id']."'";
                $this->db->query($sql);


                foreach($value as $attr_key=>$attribute) {
                    if ($attr_key != 'product_id' && $attr_key != 'model' && $attr_key != 'name') {
                        if (!empty($attribute)){
                            $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_general_accessory
                                SET product_id='".$value['product_id']."',
                                    general_accessory_id='".( (isset($data[0][$attr_key]) && !empty($data[0][$attr_key])) ? $data[0][$attr_key] : $attribute)."'";
                            $this->db->query($sql);
                        }
                    }
                }
            }
        }
        return true;
    }

}
?>