<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
            <tr>
                <td><?php echo $entry_fejlec_latszik; ?></td>
                <td colspan=3><?php if ($splash_page_product_fejlec) { ?>
                        <input type="radio" name="splash_page_product_fejlec" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="splash_page_product_fejlec" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="splash_page_product_fejlec" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="splash_page_product_fejlec" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

        </table>

      <table id="module" class="list">
        <thead>
          <tr>
		    <td class="left"><?php echo $entry_image; ?></td>
			<td class="left"><?php echo $entry_pattern; ?></td>
			<td class="left"><?php echo $entry_bgcolor; ?></td>
            <td class="left"><?php echo $entry_timer; ?></td>
			<td class="left"><?php echo $entry_url; ?></td>
            <td class="left"><?php echo $entry_layout; ?></td>
            <td class="left"><?php echo $entry_position; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
            <td class="right"><?php echo $entry_sort_order; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
		    
			<td class="left">
				<div class="image"><img src="<?php echo $module['thumb']; ?>" alt="" id="thumb_<?php echo $module_row; ?>" /><br />
                  <input type="hidden" name="splash_page_module[<?php echo $module_row; ?>][image]" value="<?php echo $module['image']; ?>" id="image_<?php echo $module_row; ?>" />
                  <a onclick="image_upload('image_<?php echo $module_row; ?>', 'thumb_<?php echo $module_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb_<?php echo $module_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image_<?php echo $module_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
				</div>
				<?php if (isset($error_image[$module_row])) { ?>
					<span class="error"><?php echo $error_image[$module_row]; ?></span>
				<?php } ?>
			</td>
			<td class="left">
				<div class="image"><img src="<?php echo $module['pattern_thumb']; ?>" alt="" id="pattern_thumb_<?php echo $module_row; ?>" /><br />
                  <input type="hidden" name="splash_page_module[<?php echo $module_row; ?>][pattern]" value="<?php echo $module['pattern']; ?>" id="pattern_<?php echo $module_row; ?>" />
                  <a onclick="image_upload('pattern_<?php echo $module_row; ?>', 'pattern_thumb_<?php echo $module_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#pattern_thumb_<?php echo $module_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#pattern_<?php echo $module_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
				</div>
			</td>
			<td>#<input type="text" name="splash_page_module[<?php echo $module_row; ?>][color]" class="cp" value="<?php echo $module['color']; ?>" size="3" /></td>
            <td class="left"><input type="text" name="splash_page_module[<?php echo $module_row; ?>][timer]" value="<?php echo $module['timer']; ?>" size="3" /></td>
            
			<td class="left"><input type="text" name="splash_page_module[<?php echo $module_row; ?>][url]" value="<?php echo $module['url']; ?>" />
			<?php if (isset($error_url[$module_row])) { ?>
				<span class="error"><?php echo $error_url[$module_row]; ?></span>
			<?php } ?></td>
			<td class="left"><select name="splash_page_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
            <td class="left"><select name="splash_page_module[<?php echo $module_row; ?>][position]">
                <?php if ($module['position'] == 'content_top') { ?>
                <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                <?php } else { ?>
                <option value="content_top"><?php echo $text_content_top; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'content_bottom') { ?>
                <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                <?php } else { ?>
                <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_left') { ?>
                <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                <?php } else { ?>
                <option value="column_left"><?php echo $text_column_left; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_right') { ?>
                <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                <?php } else { ?>
                <option value="column_right"><?php echo $text_column_right; ?></option>
                <?php } ?>
              </select></td>
            <td class="left"><select name="splash_page_module[<?php echo $module_row; ?>][status]">
                <?php if ($module['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
            <td class="right"><input type="text" name="splash_page_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="9"></td>
            <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
</div>

<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';	
	html += '    <td class="left">';
	html +=	'		<div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb_' + module_row + '" /><br />';
    html += '           <input type="hidden" name="splash_page_module[' + module_row + '][image]" value="" id="image_' + module_row + '" /> ';
    html += '           <a onclick="image_upload(\'image_' + module_row + '\', \'thumb_' + module_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb_' + module_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image_' + module_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a> ';
	html += '		</div></td> ';
	html += '    <td class="left">';
	html +=	'		<div class="image"><img src="<?php echo $no_image; ?>" alt="" id="pattern_thumb_' + module_row + '" /><br />';
    html += '           <input type="hidden" name="splash_page_module[' + module_row + '][pattern]" value="" id="pattern_' + module_row + '" /> ';
    html += '           <a onclick="image_upload(\'pattern_' + module_row + '\', \'pattern_thumb_' + module_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#pattern_thumb_' + module_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#pattern_' + module_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a> ';
	html += '		</div></td> ';
	html += '	 <td>#<input type="text" name="splash_page_module[' + module_row + '][color]" class="cp" size="3" /></td> ';	
	html += '    <td class="left"><input type="text" name="splash_page_module[' + module_row + '][timer]" size="3" /></td>';
	html += '    <td class="left"><input type="text" name="splash_page_module[' + module_row + '][url]" /></td>';
	html += '    <td class="left"><select name="splash_page_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="splash_page_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';
	html += '    <td class="left"><select name="splash_page_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="splash_page_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script>

<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 520,
		resizable: false,
		modal: false
	});
};
//--></script>  
 
<?php echo $footer; ?>