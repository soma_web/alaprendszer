<?php
class ControllerReportProductViewedCsvbe extends Controller {
	public function index() {     
		$this->load->language('report/product_viewed');

		$this->document->setTitle($this->language->get('heading_title'));
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
				
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
						
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/product_viewed', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);		
		
		$this->load->model('report/product');
		
		$data = array(
		);
				
		$product_viewed_total = $this->model_report_product->getTotalProductsViewed($data); 
		
		$product_views_total = $this->model_report_product->getTotalProductViews(); 
		
		$this->data['products'] = array();
		
		$results = $this->model_report_product->getProductsViewed($data);
		
		foreach ($results as $result) {
			if ($result['viewed']) {
				$percent = round($result['viewed'] / $product_views_total * 100, 2);
			} else {
				$percent = 0;
			}
            if ($this->config->get('megjelenit_form_admin_model') == 1) {
                $this->data['products'][] = array(
                    'name'    => $result['name'],
                    'model'   => $result['model'],
                    'viewed'  => $result['viewed'],
                    'percent' => $percent . '%'
                );
            } else {
                $this->data['products'][] = array(
                    'name'    => $result['name'],
                    'viewed'  => $result['viewed'],
                    'percent' => $percent . '%'
                );
            }
		}
 		
		$this->data['heading_title'] = $this->language->get('heading_title');
		 
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_model'] = $this->language->get('column_model');
		$this->data['column_viewed'] = $this->language->get('column_viewed');
		$this->data['column_percent'] = $this->language->get('column_percent');
        $this->data['button_nyomtatas'] = $this->language->get('button_nyomtatas');
        $this->data['button_exportalas'] = $this->language->get('button_exportalas');
		$this->data['button_reset'] = $this->language->get('button_reset');

		$url = '';		
				
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				
		$this->data['reset'] = $this->url->link('report/product_viewed/reset', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

        $this->load->model('tool/ExportCSV');

        if ($this->config->get('megjelenit_form_admin_model') == 1) {
            $this->model_tool_ExportCSV->setHeader(array(
                'name'          => $this->data['column_name'],
                'model'         => $this->data['column_model'],
                'viewed'        => $this->data['column_viewed']
            ));
        } else {
            $this->model_tool_ExportCSV->setHeader(array(
                'name'          => $this->data['column_name'],
                'viewed'        => $this->data['column_viewed']
            ));
        }

        if(!empty($results)) {
            $this->model_tool_ExportCSV->setDatas($results);
        } else {
            $this->model_tool_ExportCSV->setDatas(array());
        }
        $this->model_tool_ExportCSV->sendToOutput();
	}
	
	public function reset() {
		$this->load->language('report/product_viewed');
		
		$this->load->model('report/product');
		
		$this->model_report_product->reset();
		
		$this->session->data['success'] = $this->language->get('text_success');
		
		$this->redirect($this->url->link('report/product_viewed', 'token=' . $this->session->data['token'], 'SSL'));
	}
}
?>