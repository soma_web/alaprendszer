<?php  echo $header; ?>


<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
       <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div id="header_altalanos">
                    <a onclick="addCsoport();" class="button">Csoport hozzáadása</a>

                    <?php if (isset($csv_beallitasok) && $csv_beallitasok) {?>
                        <?php $elemszam = 0?>
                        <?php foreach($csv_beallitasok as $key_aktualis=>$value) {?>
                            <div class="csoportok" id="csoport_<?php echo $key_aktualis?>" >

                                <div class="csoportok_nyito" style="padding: 5px 0 30px 20px;" onclick="csikiCsuki(this,<?php echo $key_aktualis;?>)">
                                    <a onclick="$('#csoport<?php echo "_".$key_aktualis;?>').remove();" class="button"><?php echo $button_remove; ?></a>
                                    <a style="text-decoration: none; margin-right: 40px; float: left">
                                        Csoport neve: <input type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][export_name]" value="<?php echo $value['export_name'];?>" size="25">
                                    </a>
                                </div>

                                <div class="csoport_beallitasok">
                                    <div class="csoport_<?php echo $key_aktualis;?>">

                                    Küldés <b>CSV-be:</b> <input type="radio" name="csv_beallitasok[<?php echo $key_aktualis?>][kuldes]" value="1"
                                                          <?php echo isset($value['kuldes']) && $value['kuldes'] == 1 ? 'checked="checked"' : '' ?>
                                                          onclick="kuldesNyitZar(this, <?php echo $key_aktualis?>)" >

                                           <b>Formba:</b> <input type="radio" name="csv_beallitasok[<?php echo $key_aktualis?>][kuldes]" value="2"
                                                          <?php echo isset($value['kuldes']) && $value['kuldes'] == 2 ? 'checked="checked"' : '' ?>
                                                          onclick="kuldesNyitZar(this,<?php echo $key_aktualis?>)">
                                    </br>
                                    </br>

                                        <div class="csv_be" style="margin-bottom: 20px;<?php echo isset($value['kuldes']) && $value['kuldes'] == 2 ? 'display:none' : ''?>">
                                            <?php $tarolas_display = "display: none"?>
                                            <?php $kuldes_display  = "display: none"?>
                                            <?php $letolt_cimke  = ""?>
                                            <?php $tarol_cimke  = ""?>
                                            <?php $kuldes_cimke  = ""?>

                                            <?php if (isset($value['tarolas']) && $value['tarolas'] == 1) { ?>
                                                <?php $letolt_cimke  = "font-weight: bold"?>

                                            <?php } elseif (isset($value['tarolas']) && $value['tarolas'] == 2) { ?>
                                                <?php $tarolas_display = "display: inline"?>
                                                <?php $kuldes_display  = "display: none"?>
                                                <?php $tarol_cimke  = "font-weight: bold"?>

                                            <?php } elseif (isset($value['tarolas']) && $value['tarolas'] == 3) { ?>
                                                <?php $tarolas_display = "display: none"?>
                                                <?php $kuldes_display  = "display: inline"?>
                                                <?php $kuldes_cimke  = "font-weight: bold"?>

                                            <?php } ?>

                                            <span id="letolt_cimke_<?php echo $key_aktualis?>" class="cimke" style="<?php echo $letolt_cimke?>">Letöltés</span>
                                            <input type="radio" name="csv_beallitasok[<?php echo $key_aktualis?>][tarolas]" value="1"
                                                    <?php echo isset($value['tarolas']) && $value['tarolas'] == 1 ? 'checked="checked"' : '' ?>
                                                    onclick="tarolasNyitZar(this,<?php echo $key_aktualis?>)" >

                                            <span id="tarol_cimke_<?php echo $key_aktualis?>" class="cimke" style="<?php echo $tarol_cimke?>">Tárolás</span>
                                            <input type="radio" name="csv_beallitasok[<?php echo $key_aktualis?>][tarolas]" value="2"
                                                    <?php echo isset($value['tarolas']) && $value['tarolas'] == 2 ? 'checked="checked"' : '' ?>
                                                   onclick="tarolasNyitZar(this,<?php echo $key_aktualis?>)">

                                            <span id="tarolas_helye_<?php echo $key_aktualis?>" style="padding-right: 30px; <?php echo $tarolas_display?>">
                                                Tárolás helye: <input  type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][tarolas_helye]" value="<?php echo isset($value['tarolas_helye']) ? $value['tarolas_helye'] : ""?>">
                                            </span>

                                            <span id="kuldes_cimke_<?php echo $key_aktualis?>" class="cimke" style="<?php echo $kuldes_cimke?>">Küldés külső címre</span>
                                            <input type="radio" name="csv_beallitasok[<?php echo $key_aktualis?>][tarolas]" value="3"
                                                    <?php echo isset($value['tarolas']) && $value['tarolas'] == 3 ? 'checked="checked"' : '' ?>
                                                    onclick="tarolasNyitZar(this,<?php echo $key_aktualis?>)">
                                            <span id="kuldes_helye_<?php echo $key_aktualis?>" style="padding-right: 30px; <?php echo $kuldes_display?>">
                                                FTP utvonal: <input  type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][tarolas_helye]" value="<?php echo isset($value['tarolas_helye']) ? $value['tarolas_helye'] : ""?>">
                                            </span>

                                        </div>

                                    <div class="formb_action" style="margin-bottom: 20px;<?php echo isset($value['kuldes']) && $value['kuldes'] == 1 ? 'display:none' : ''?>">
                                        <div style="display: inline-block">
                                            Form elérési útja:
                                            <span class="help">pl. http://oldal.hu/fogado_form</span>
                                        </div>
                                        <div style="display: inline-block">
                                            <input type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][from_action]"
                                                   value="<?php echo !empty($value['from_action']) ? $value['from_action'] : ''?>" style="width: 300px">
                                        </div>
                                    </div>


                                    <?php $tablasor = isset($value['tabla']) && !empty($value['tabla']) ? count($value['tabla']) : 0;?>
                                        <table class="fotabla">
                                            <tr>
                                                <td style="min-width: 452px"><a onclick="addTabla('<?php echo $key_aktualis?>');" class="button top">Új tábla</a></td>
                                                <td><a onclick="addOszlopMezo('<?php echo $key_aktualis?>');" class="button top">Új mező</a>
                                                    <a onclick="addOszlopAllando('<?php echo $key_aktualis?>');" class="button top">Új állando</a>
                                                </td>
                                            </tr>
                                            <tr><td><br></td></tr>
                                            <tr>
                                                <td  class="datbazis_tablak">
                                                    <?php if(isset($value['tabla']) && !empty($value['tabla']) ) { ?>
                                                        <table style="margin-right: 60px;">
                                                            <?php foreach($value['tabla'] as $table_key=>$value_tabla) {?>
                                                                <tr style="margin-bottom: 20px" class="add_<?php echo $key_aktualis?>_<?php echo $table_key; ?> tabla_nev">
                                                                    <td>Tábla:</td>
                                                                    <td>
                                                                        <select name="csv_beallitasok[<?php echo $key_aktualis?>][tabla][<?php echo $table_key?>][name]" class="tablak_<?php echo $key_aktualis?>" onchange="tablaValaszt(<?php echo$key_aktualis?>)">
                                                                            <option>--- Válasszon ---</option>

                                                                            <?php foreach($szerkezet as $szerkezet_key=>$szerkezet_tabla) {?>
                                                                                <?php if($value_tabla['name'] == $szerkezet_key){ ?>
                                                                                    <option selected="selected" value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>
                                                                                <?php } else { ?>
                                                                                    <option value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>
                                                                                <?php } ?>
                                                                            <?php }?>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <span class="top" style="display: inline-block; width: 65px;">Főtábla: </span>
                                                                        <?php if(isset($value_tabla['fotabla'])) { ?>
                                                                            <input type="checkbox" name="csv_beallitasok[<?php echo $key_aktualis?>][tabla][<?php echo $table_key?>][fotabla]" value="1" checked="checked" onclick="checkEllenoriz('<?php echo $key_aktualis;?>','<?php echo $table_key?>',this)"/>
                                                                        <?php } else {?>
                                                                            <input type="checkbox" name="csv_beallitasok[<?php echo $key_aktualis?>][tabla][<?php echo $table_key?>][fotabla]" value="1" onclick="checkEllenoriz('<?php echo $key_aktualis;?>','<?php echo $table_key?>',this)"/>
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td>
                                                                        <img src="view/image/delete.png" alt="" onclick="$('.add_<?php echo $key_aktualis?>_<?php echo $table_key; ?>' ).remove(); tablaValaszt(<?php echo$key_aktualis?>); return false;" ><br>
                                                                    </td>
                                                                </tr>
                                                                <tr class="add_<?php echo $key_aktualis?>_<?php echo $table_key; ?>">
                                                                    <td>Kapcsolat:</td>
                                                                    <td>
                                                                        <?php if(isset($value_tabla['fotabla'])) { ?>
                                                                            <select disabled style="opacity: 0.4" id="select_<?php echo $key_aktualis?>_<?php echo $table_key?>" class="select_<?php echo $key_aktualis?>" name="csv_beallitasok[<?php echo $key_aktualis?>][tabla][<?php echo $table_key?>][kapcsolodas]"/>
                                                                                <option value=""  selected="selected">-- Válasszon --</option>
                                                                                <option value="LEFT">LEFT JOIN</option>
                                                                                <option value="RIGHT">RIGHT JOIN</option>
                                                                                <option value="INNER">INNER JOIN</option>
                                                                            </select>
                                                                        <?php } else { ?>
                                                                            <select id="select_<?php echo $key_aktualis?>_<?php echo $table_key?>" class="select_<?php echo $key_aktualis?>" name="csv_beallitasok[<?php echo $key_aktualis?>][tabla][<?php echo $table_key?>][kapcsolodas]"/>
                                                                                <option value="">-- Válasszon --</option>
                                                                                <?php if($value_tabla['kapcsolodas'] == "LEFT") { ?>
                                                                                    <option value="LEFT"  selected="selected">LEFT JOIN</option>
                                                                                    <option value="RIGHT">RIGHT JOIN</option>
                                                                                    <option value="INNER">INNER JOIN</option>

                                                                                <?php } elseif($value_tabla['kapcsolodas'] == "RIGHT"){ ?>
                                                                                    <option value="LEFT">LEFT JOIN</option>
                                                                                    <option value="RIGHT"  selected="selected">RIGHT JOIN</option>
                                                                                    <option value="INNER">INNER JOIN</option>

                                                                                <?php } elseif($value_tabla['kapcsolodas'] == "INNER"){ ?>
                                                                                    <option value="LEFT" >LEFT JOIN</option>
                                                                                    <option value="RIGHT">RIGHT JOIN</option>
                                                                                    <option value="INNER"  selected="selected">INNER JOIN</option>

                                                                                <?php } else { ?>
                                                                                    <option value="LEFT">LEFT JOIN</option>
                                                                                    <option value="RIGHT">RIGHT JOIN</option>
                                                                                    <option value="INNER">INNER JOIN</option>
                                                                                <?php } ?>
                                                                            </select>
                                                                         <?php } ?>
                                                                    </td>
                                                                    <td></td><td></td>
                                                                </tr>
                                                                <tr class="add_<?php echo $key_aktualis?>_<?php echo $table_key; ?>">

                                                                    <td>On kapcsolat:</td>
                                                                    <td>
                                                                        <textarea name="csv_beallitasok[<?php echo $key_aktualis?>][tabla][<?php echo $table_key?>][on_kapcsolat]"><?php echo $value_tabla['on_kapcsolat'];?></textarea>
                                                                    </td>
                                                                    <td>Feltétel:</td>
                                                                    <td>
                                                                        <textarea name="csv_beallitasok[<?php echo $key_aktualis?>][tabla][<?php echo $table_key?>][where_feltetel]"><?php echo $value_tabla['where_feltetel'];?></textarea>
                                                                    </td>
                                                                    <td></td><td></td>

                                                                </tr>

                                                                <tr class="add_<?php echo $key_aktualis?>_<?php echo $table_key; ?> tabla_vege">
                                                                    <td><br><br></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </table>
                                                    <?php } ?>
                                                </td>
                                                <td  class="datbazis_mezok">
                                                    <table class="mezo_oszlopok">
                                                        <tr>
                                                            <td></td><td style="text-align: left; padding-left: 83px;">Kerekítés</td>
                                                            <td class="form_mezo_neve">Form mező neve<span class="help">(ha a küldés formba érkezik)</span></td>
                                                        </tr>
                                                        <?php if (isset($value['mezok']) &&  !empty($value['mezok']) ) {?>
                                                            <?php foreach($value['mezok'] as $mezo_key=>$value_mezo) {?>
                                                                <tr class="oszlop_<?php echo $key_aktualis?>_<?php echo $mezo_key; ?> oszlopok">
                                                                    <td>
                                                                        <?php if (isset($value_mezo['name']) && !empty($value_mezo['name'])) {?>

                                                                        <select name="csv_beallitasok[<?php echo $key_aktualis?>][mezok][<?php echo $mezo_key?>][name]" style="width: 250px">
                                                                            <option>--- Válasszon ---</option>
                                                                            <?php foreach($szerkezet as $szerkezet_key=>$szerkezet_mezok) {?>
                                                                                <?php
                                                                                $mehet = false;
                                                                                    foreach ($value['tabla'] as $ervenyes_tabla) {
                                                                                        if ($ervenyes_tabla['name'] == $szerkezet_key) {
                                                                                            $mehet = true;
                                                                                        }
                                                                                    }
                                                                                ?>

                                                                                <?php if ($mehet) { ?>
                                                                                    <?php foreach($szerkezet_mezok as $szerkezet_mezok_tartalom) { ?>
                                                                                        <?php if($szerkezet_key.'.'.$szerkezet_mezok_tartalom['Field'] == $value_mezo['name']){ ?>

                                                                                            <option selected="selected" value="<?php echo $szerkezet_key.".".$szerkezet_mezok_tartalom['Field']?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key).".".$szerkezet_mezok_tartalom['Field']?></option>
                                                                                        <?php } else { ?>
                                                                                            <option value="<?php echo $szerkezet_key.".".$szerkezet_mezok_tartalom['Field']?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key).".".$szerkezet_mezok_tartalom['Field']?></option>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                <?php } ?>

                                                                            <?php }?>
                                                                        </select>
                                                                        <?php } elseif(isset($value_mezo['allando'])) {?>
                                                                            <input  style="width: 250px" type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][mezok][<?php echo $mezo_key?>][allando]" value="<?php echo $csv_beallitasok[$key_aktualis]['mezok'][$mezo_key]['allando'];?>" />
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td>
                                                                        <input  type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][mezok][<?php echo $mezo_key?>][sort_order]" value="<?php echo $csv_beallitasok[$key_aktualis]['mezok'][$mezo_key]['sort_order'];?>" style="width: 30px"/>
                                                                        <input  type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][mezok][<?php echo $mezo_key?>][sort]"
                                                                                value="<?php echo isset($csv_beallitasok[$key_aktualis]['mezok'][$mezo_key]['sort']) ? $csv_beallitasok[$key_aktualis]['mezok'][$mezo_key]['sort'] : '0';?>" style="width: 30px"/>


                                                                        <?php if (!isset($value_mezo['allando'])) { ?>
                                                                        <?php $tizedes = isset($csv_beallitasok[$key_aktualis]['mezok'][$mezo_key]['tizedes']) ? $csv_beallitasok[$key_aktualis]['mezok'][$mezo_key]['tizedes'] : 0 ?>
                                                                            <select name="csv_beallitasok[<?php echo $key_aktualis?>][mezok][<?php echo $mezo_key?>][tizedes]" style="width: 110px">
                                                                                <option>--- Válasszon ---</option>
                                                                                <option value="1" <?php echo $tizedes == 1 ? "selected='selected'" : ""; ?> >Egészre</option>
                                                                                <option value="2" <?php echo $tizedes == 2 ? "selected='selected'" : ""; ?> >1 tizedesre</option>
                                                                                <option value="3" <?php echo $tizedes == 3 ? "selected='selected'" : ""; ?> >2 tizedesre</option>
                                                                                <option value="4" <?php echo $tizedes == 4 ? "selected='selected'" : ""; ?> >3 tizedesre</option>
                                                                            </select>
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td class="form_mezo_neve">
                                                                        <input  type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][mezok][<?php echo $mezo_key?>][form_mezo_neve]"
                                                                                value="<?php echo !empty($csv_beallitasok[$key_aktualis]['mezok'][$mezo_key]['form_mezo_neve']) ? $csv_beallitasok[$key_aktualis]['mezok'][$mezo_key]['form_mezo_neve'] : '';?>" style="width: 130px"/>
                                                                    </td>

                                                                    <td>
                                                                        <img src="view/image/delete.png" alt="" onclick="$('.oszlop_<?php echo $key_aktualis?>_<?php echo $mezo_key; ?>').remove(); return false;" />
                                                                    </td>

                                                                </tr>

                                                            <?php } ?>
                                                        <?php } ?>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="top" style="width: 105px; display: inline-block;">Order By: </span>
                                                    <input class="allando_name" type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][order_by]" value="<?php echo isset($value['order_by']) ? $value['order_by'] : "";?>" size="25"/>
                                                </td>
                                            </tr>
                                            <tr><td>
                                                    <span class="top" style="width: 105px; display: inline-block;">Karakter kódolás</span>
                                                    <input class="allando_name" type="text" name="csv_beallitasok[<?php echo $key_aktualis?>][karakter_kodolas]" value="<?php echo isset($value['karakter_kodolas']) ? $value['karakter_kodolas'] : "";?>" size="25"/>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>


                            </div>
                        <?php }?>
                    <?php }?>
                </div>
                <input type="hidden" name="nyitott_sor" value="" id="nyitott_sor">
            </form>
    </div>
</div>


<script>
    var csv_row = <?php echo isset($key_aktualis) ? $key_aktualis+1 : 0?>;

    <?php if ($nyitott_sor !== "") { ?>
        $('#csoport_<?php echo $nyitott_sor;?> .csoport_beallitasok').css('display','block');
    <?php } ?>

    function addCsoport(){
        $('.csoport_beallitasok').slideUp(500);
        $('.csoportok_nyito').removeClass("active");
        var html = "";

        html += '<div class="csoportok" id="csoport_'+csv_row+'" >';

            html += '<div class="csoportok_nyito" style="padding: 5px 0 30px 20px;" onclick="csikiCsuki(this,'+csv_row+')">';
                html += '<a onclick="$(\'#csoport_'+csv_row+'\').remove();" class="button"><?php echo $button_remove; ?></a>';
                html += '<a style="text-decoration: none; margin-right: 40px; float: left">';
                    html +='Csoport neve: <input type="text" name="csv_beallitasok['+csv_row+'][export_name]" value="" size="25">';
                html +='</a>';
            html += '</div>';

            html += '<div class="csoport_beallitasok">';
                html += '<div class="csoport_'+csv_row+'">';

                    html += 'Futás után: <input type="radio" name="csv_beallitasok['+csv_row+'][tarolas]" value="1" checked="checked" onclick="tarolasNyitZar(this, '+csv_row+')" >Letöltés';
                    html += '<input type="radio" name="csv_beallitasok['+csv_row+'][tarolas]" value="2" onclick="tarolasNyitZar(this,'+csv_row+')">Tárolás';
                    html += '<span id="tarolas_helye_'+csv_row+'" style="padding-left: 50px; display: none">';
                        html += 'Tárolás helye: <input type="text" name="csv_beallitasok['+csv_row+'][tarolas_helye]" value="">';
                    html += '</span>';

                    html += '<table class="fotabla">';
                        html += '<tr>';
                            html += '<td style="min-width: 452px"><a onclick="addTabla('+csv_row+')" class="button top">Új tábla</a></td>';
                            html += '<td><a onclick="addOszlopMezo('+csv_row+')" class="button top">Új mező</a>';
                                html += '<a onclick="addOszlopAllando('+csv_row+');" class="button top">Új állando</a>';
                            html += '</td>';
                        html += '</tr>';
                        html += '<tr><td><br></td></tr>';

                        html += '<tr>';
                            html +='<td  class="datbazis_tablak"><table></table></td>';
                            html +='<td  class="datbazis_mezok"><table class="mezo_oszlopok"></table></td>';
                        html += '</tr>';
                    html += '</table>';
                    html +='<span class="top" style="width: 105px; display: inline-block;">Order By: </span>';
                    html +='<input class="allando_name" type="text" name="csv_beallitasok['+csv_row+'][order_by]" value="" size="25" ><br>';

                    html +='<span class="top" style="width: 105px; display: inline-block;">Karakter kódolás: </span>';
                    html +='<input class="allando_name" type="text" name="csv_beallitasok['+csv_row+'][karakter_kodolas]" value="" size="25" >';
            html += '</div>';
            html += '</div>';
        html += '</div>';
        csv_row++;
        $("#header_altalanos").append(html);
        $('#csoport_'+(csv_row-1)+' .csoport_beallitasok').slideDown(500);
        $('#csoport_'+(csv_row-1)+' .csoportok_nyito').addClass("active");
    }



    function addTabla(sor){
        var tablasor = $(".csoport_"+sor+" .tabla_nev").length;
        var utolso_tablasor;
        var maximum_valtozo = "";
        if (tablasor > 0) {
            for(var i=0; tablasor > i; i++) {
                var a = $(".csoport_"+sor+" .tabla_nev")[i].attributes.class.value;
                if (a > maximum_valtozo) {
                    maximum_valtozo = a;
                }
                utolso_tablasor = a.split(" ")[0];
            }

            var levagott_valtozo = maximum_valtozo.substr(0,maximum_valtozo.indexOf(" "));
            levagott_valtozo = levagott_valtozo.split("_");
            tablasor = levagott_valtozo[2]*1+1;
        }




        var html = "";
        html += '<tr style="margin-bottom: 20px" class="add_'+sor+'_'+tablasor+' tabla_nev">';
            html += '<td>Tábla:</td>';
            html += '<td>';
                html += '<select name="csv_beallitasok['+sor+'][tabla]['+tablasor+'][name]" onchange="tablaValaszt('+sor+')">';
                html += '<option>--- Válasszon ---</option>';

                <?php foreach($szerkezet as $szerkezet_key=>$szerkezet_tabla) {?>
                    html += '<option value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>';
                <?php }?>
                html += '</select>';
            html += '</td>';
            html += '<td>';
                html += '<span class="top" style="display: inline-block; width: 65px;">Főtábla: </span>';
                html += '<input type="checkbox" name="csv_beallitasok['+sor+'][tabla]['+tablasor+'][fotabla]" value="1" onclick="checkEllenoriz('+sor+','+tablasor+',this)"/>';
            html += '</td>';
            html += '<td>';
                html += '<img src="view/image/delete.png" alt="" onclick="$(\'.add_'+sor+'_'+tablasor+'\' ).remove(); return false;" onchange="tablaValaszt('+sor+')"><br>';
            html += '</td>';
        html += '</tr>';
        html += '<tr class="add_'+sor+'_'+tablasor+'">';
            html += '<td>Kapcsolat:</td>';
            html += '<td>';
                html += '<select id="select_'+sor+'_'+tablasor+'" class="select_'+sor+'" name="csv_beallitasok['+sor+'][tabla]['+tablasor+'][kapcsolodas]" >';
                    html += '<option value=""  selected="selected">-- Válasszon --</option>';
                    html += '<option value="LEFT">LEFT JOIN</option>';
                    html += '<option value="RIGHT">RIGHT JOIN</option>';
                    html += '<option value="INNER">INNER JOIN</option>';
                html += '</select>';
            html += '</td>';
        html += '</tr>';


        html += '<tr class="add_'+sor+'_'+tablasor+'">';
            html += '<td>On kapcsolat:</td>';
            html += '<td>';
                html += '<textarea name="csv_beallitasok['+sor+'][tabla]['+tablasor+'][on_kapcsolat]"></textarea>';
            html += '</td>';

            html += '<td>Feltétel:</td>';
            html += '<td>';
                html += '<textarea name="csv_beallitasok['+sor+'][tabla]['+tablasor+'][where_feltetel]"></textarea>';
            html += '</td>';
        html += '</tr>';

        html += '<tr class="add_'+sor+'_'+tablasor+' tabla_vege">';
            html += '<td><br><br></td>';
        html += '</tr>';

        if (tablasor >= 1) {
            $("."+utolso_tablasor+' + .tabla_vege').after(html);
        } else {
            //var html_elo = '<tr><td><table>';
            //html += '</table></td><td><table></table></td></tr>';

            //html_elo += html;

            $('.csoport_'+sor+' .datbazis_tablak table').html(html);

        }


    }


    function addOszlopMezo(sor){
        var oszlopsor = $(".csoport_"+sor+" .oszlopok").length;
        var classtomb = new Array();
        var utolso_oszlop;
        var sor_str = sor+"";

        for(var i=0; oszlopsor > i; i++) {
            var a = $(".csoport_"+sor+" .oszlopok")[i].attributes.class.value;
            var b = a.substr(0,a.indexOf(" "));
            var teljes_hossz    = b.length;
            var levagott_hossz  = b.substr(0,(7+sor_str.length+1)).length;
            var maradek = teljes_hossz-levagott_hossz;
            classtomb[i] = a.substr(levagott_hossz,maradek);
            utolso_oszlop = a.substr(levagott_hossz,maradek);
        }

        var legnagyobb_oszlopsor = 0;
        for(var i=0; classtomb.length > i; i++) {
            if (classtomb[i]*1 > legnagyobb_oszlopsor) {
                legnagyobb_oszlopsor = classtomb[i]*1;
            }
        }
        oszlopsor = legnagyobb_oszlopsor*1+1;


        var nyitott_tablak_ossz = $('.tablak_'+sor).length;
        var nyitott_tablak = new Array();
        for(var i=0; nyitott_tablak_ossz > i; i++) {
            nyitott_tablak[i] = $('.tablak_'+sor)[i].value;
        }

        var html = "";
        html += '<tr class="oszlop_'+sor+'_'+oszlopsor+' oszlopok">';
            html += '<td>';
                html += '<select name="csv_beallitasok['+sor+'][mezok]['+oszlopsor+'][name]" style="width: 250px">';
                    html += '<option>--- Válasszon ---</option>';
                    <?php foreach($szerkezet as $szerkezet_key=>$szerkezet_mezok) {?>

                        var mehet = false;
                        for (i in nyitott_tablak) {
                             if (nyitott_tablak[i] == "<?php echo $szerkezet_key?>") {
                                 mehet = true;
                             }
                        }

                        if (mehet) {
                            <?php foreach($szerkezet_mezok as $szerkezet_mezok_tartalom) { ?>
                                html += '<option value="<?php echo $szerkezet_key.".".$szerkezet_mezok_tartalom['Field']?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key).".".$szerkezet_mezok_tartalom["Field"]?></option>';
                            <?php } ?>
                        }
                    <?php } ?>
                html += '</select>';
            html += '</td>';

            html += '<td>';
                html += '<input  type="text" name="csv_beallitasok['+sor+'][mezok]['+oszlopsor+'][sort_order]" value="" style="width: 30px" >';
                html += '<input  type="text" name="csv_beallitasok['+sor+'][mezok]['+oszlopsor+'][sort]" value="" style="width: 30px" >';

                html += '<select name="csv_beallitasok['+sor+'][mezok]['+oszlopsor+'][tizedes]" style="width: 110px">';
                    html += '<option>--- Válasszon ---</option>';
                    html += '<option value="1">Egészre</option>';
                    html += '<option value="2">1 tizedesre</option>';
                    html += '<option value="3">2 tizedesre</option>';
                    html += '<option value="4">3 tizedesre</option>';
                html += '</select>';
            html += '</td>';

            html += '<td class="form_mezo_neve">';
                html += '<input  type="text" name="csv_beallitasok['+sor+'][mezok]['+oszlopsor+'][form_mezo_neve]" value="" style="width: 130px"/>';
            html += '</td>';

            html += '<td>';
                html +='<img src="view/image/delete.png" alt="" onclick="$(\'.oszlop_'+sor+'_'+oszlopsor+'\').remove(); return false;" >';
            html += '</td>';

        html += '</tr>';

        if ($(".oszlop_"+sor+"_"+utolso_oszlop).length > 0) {
            $(".oszlop_"+sor+"_"+utolso_oszlop).after(html);
        } else {
            $(".csoport_"+sor+" .mezo_oszlopok").html(html);
        }
    }

    function addOszlopAllando(sor){

        var oszlopsor = $(".csoport_"+sor+" .oszlopok").length;
        var classtomb = new Array();
        var utolso_oszlop;
        var sor_str = sor+"";

        for(var i=0; oszlopsor > i; i++) {
            var a = $(".csoport_"+sor+" .oszlopok")[i].attributes.class.value;
            var b = a.substr(0,a.indexOf(" "));
            var teljes_hossz    = b.length;
            var levagott_hossz  = b.substr(0,(7+sor_str.length+1)).length;
            var maradek = teljes_hossz-levagott_hossz;
            classtomb[i] = a.substr(levagott_hossz,maradek);
            utolso_oszlop = a.substr(levagott_hossz,maradek);
        }

        var legnagyobb_oszlopsor = 0;
        for(var i=0; classtomb.length > i; i++) {
            if (classtomb[i]*1 > legnagyobb_oszlopsor) {
                legnagyobb_oszlopsor = classtomb[i]*1;
            }
        }
        oszlopsor = legnagyobb_oszlopsor*1+1;

        var html = "";
        html += '<tr class="oszlop_'+sor+'_'+oszlopsor+' oszlopok">';
            html += '<td>';
                html +='<input style="width: 250px" type="text" name="csv_beallitasok['+sor+'][mezok]['+oszlopsor+'][allando]" value="" >';
            html += '</td>';

            html += '<td>';
                html +='<input type="text" name="csv_beallitasok['+sor+'][mezok]['+oszlopsor+'][sort_order]"    value="" style="width: 30px" >';
                html +='<input type="text" name="csv_beallitasok['+sor+'][mezok]['+oszlopsor+'][sort]"          value="" style="width: 30px" >';
        html += '</td>';

            html += '<td>';
                html +='<img src="view/image/delete.png" alt="" onclick="$(\'.oszlop_'+sor+'_'+oszlopsor+'\').remove(); return false;" >';
            html += '</td>';
        html += '</tr>';

        if ($(".oszlop_"+sor+"_"+utolso_oszlop).length > 0) {
            $(".oszlop_"+sor+"_"+utolso_oszlop).after(html);
        } else {
            $(".csoport_"+sor+" .mezo_oszlopok").html(html);
        }

    }


    function csikiCsuki(para,sor){
        if (this.event.target == para || this.event.target.localName == "span") {
            if ($(para).next().css("display") != "none") {
                $(para).next().slideUp(500);
                $(para).removeClass("active");

            } else {
                $('.csoportok_nyito').removeClass("active");
                $('.csoport_beallitasok').hide();
                $(para).next().slideDown(500);
                $(para).addClass("active");

            }
        }
    }


    function checkEllenoriz(aktualis_csv,aktualis_tabla,aktualis_elem) {
        $("#csoport_"+aktualis_csv+" input[type=checkbox]").prop("checked",false);
        $(aktualis_elem).prop("checked",true);


        $(".select_"+aktualis_csv).attr("disabled",false).css("opacity","1");
        $("#select_"+aktualis_csv+"_"+aktualis_tabla).attr("disabled",true).css("opacity","0.4");

    }

    function addAllando(sor){
        var allandosor = $(".allando_"+sor+" .allando_name").length;

        var html = "";
        html += '<span id="allando_'+sor+'_'+allandosor+'"  class="input_mezo_sorok">';
            html += '<input class="allando_name" type="text" name="csv_beallitasok['+sor+'][allando]['+allandosor+'][allando_name]" value="" size="25" style="margin-right: 5px">';
            html += '<input type="text" name="csv_beallitasok['+sor+'][allando]['+allandosor+'][sort_order]" value="" size="2" style="margin-right: 5px">';
            html += '<img src="view/image/delete.png" alt="" onclick="$(\'#allando_'+sor+'_'+allandosor+'\').remove(); return false;" />';

        html += '</span>';
        $(".allando_"+sor+" > a").before(html);
    }

    function tablaValaszt(sor){
        $('#nyitott_sor').val(sor);
        $('#form').submit();
    }

    function tarolasNyitZar(para,sor) {
        debugger;

        $(".cimke").css("font-weight","normal");

        if (para.value == 1) {
            debugger;
            $('#letolt_cimke_'+ sor).css("font-weight","bold");
            $('#tarolas_helye_'+ sor).fadeOut(600);
            $('#kuldes_helye_'+ sor).fadeOut(600);


        } else if (para.value == 2) {
            $('#tarol_cimke_'+ sor).css("font-weight","bold");
            $('#tarolas_helye_'+ sor).fadeIn(600);
            $('#kuldes_helye_'+ sor).fadeOut(600);

        } else if (para.value == 3) {
            $('#kuldes_cimke_'+ sor).css("font-weight","bold");
            $('#tarolas_helye_'+ sor).fadeOut(600);
            $('#kuldes_helye_'+ sor).fadeIn(600);
        }
    }

    function kuldesNyitZar(para,sor) {
        if (para.value == 2) {
            $('.csoport_'+sor+' .csv_be').fadeOut(600);
            $('.csoport_'+sor+' .formb_action').fadeIn(600);
            $('.csoport_'+sor+' .form_mezo_neve').fadeIn(600);
        } else {
            $('.csoport_'+sor+' .csv_be').fadeIn(600);
            $('.csoport_'+sor+' .formb_action').fadeOut(600);
            $('.csoport_'+sor+' .form_mezo_neve').fadeOut(600);

        }
    }



</script>







<?php echo $footer; ?>