<?php
define('EXTENSION_NAME',            'Product Questions &amp; Answers');
define('EXTENSION_VERSION',         '1.7.5');
define('EXTENSION_ID',              '1328');
define('EXTENSION_COMPATIBILITY',   'OpenCart 1.5.1.x, 1.5.2.x, 1.5.3.x, 1.5.4.x, 1.5.5.x and 1.5.6.x');
define('EXTENSION_STORE_URL',       'http://www.opencart.com/index.php?route=extension/extension/info&extension_id=1328');
define('EXTENSION_SUPPORT_EMAIL',   'support@opencart.ee');
define('EXTENSION_SUPPORT_FORUM',   'http://forum.opencart.com/viewtopic.php?f=123&t=25969');
define('OTHER_EXTENSIONS',          'http://www.opencart.com/index.php?route=extension/extension&filter_username=bull5-i');

class ControllerModuleQA extends Controller {
    private $error = array();
    protected $alert = array(
        'error'     => array(),
        'warning'   => array(),
        'success'   => array(),
        'info'      => array()
    );
    private $defaults = array(
        // (General)
        'qa_installed'                      => 1,
        'qa_installed_version'              => EXTENSION_VERSION,
        'qa_status'                         => 0,
        'qa_new_question_notification'      => 1,
        'qa_question_reply_notification'    => 1,
        'qa_notification_emails'            => array(),
        'qa_notification_from'              => 0,
        'qa_remove_sql_changes'             => 0,
        // Question form (Form)
        'qa_form_display_name'              => 1,
        'qa_form_require_name'              => 1,
        'qa_form_display_email'             => 1,
        'qa_form_require_email'             => 0,
        'qa_form_display_phone'             => 0,
        'qa_form_require_phone'             => 0,
        'qa_form_display_custom_field'      => 0,
        'qa_form_require_custom_field'      => 0,
        'qa_form_custom_field_name'         => array(),
        'qa_form_display_captcha'           => 1,
        'qa_form_require_captcha'           => 1,
        // Questions & answers (Q&A)
        'qa_display_questions'              => 1,
        'qa_display_all_languages'          => 1,
        'qa_new_question_status'            => 0,
        'qa_items_per_page'                 => 5,
        'qa_preload'                        => 0,
        'qa_display_question_author'        => 1,
        'qa_display_question_date'          => 1,
        'qa_display_answer_author'          => 0,
        'qa_display_answer_date'            => 0,
    );

    public function __construct($registry) {
        parent::__construct($registry);
        $this->load->helper('qa');

        $this->language->load('module/qa');
        $this->load->model('module/qa');
    }

    public function index() {
        $this->document->addStyle('view/stylesheet/qa/css/custom.min.css');

        $this->document->addScript('view/javascript/qa/custom.min.js');
        $this->document->addScript('view/javascript/jquery/superfish/js/superfish.js');

        $this->document->setTitle($this->language->get('extension_name'));

        $this->load->model('setting/setting');

        $ajax_request = isset($this->request->server['HTTP_X_REQUESTED_WITH']) && !empty($this->request->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && !$ajax_request && $this->validateForm($this->request->post)) {
            $original_settings = $this->model_setting_setting->getSetting('qa');

            $settings = array_merge($original_settings, $this->request->post);
            $settings['qa_installed_version'] = $original_settings['qa_installed_version'];

            $this->model_setting_setting->editSetting('qa', $settings);

            $this->session->data['success'] = $this->language->get('text_success_update');

            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        } else if ($this->request->server['REQUEST_METHOD'] == 'POST' && $ajax_request) {
            $response = array('success' => false);

            if ($this->validateForm($this->request->post)) {
                $original_settings = $this->model_setting_setting->getSetting('qa');

                if ((int)$original_settings['qa_status'] != (int)$this->request->post['qa_status']) {
                    $response['reload'] = true;
                    $this->session->data['success'] = $this->language->get('text_success_update');
                }

                $settings = array_merge($original_settings, $this->request->post);
                $settings['qa_installed_version'] = $original_settings['qa_installed_version'];

                $this->model_setting_setting->editSetting('qa', $settings);

                $response['success'] = true;
                $response['msg'] = $this->language->get("text_success_update");
            } else {
                if (!$this->checkVersion()) {
                    $response['reload'] = true;
                }
                $response = array_merge($response, array("error" => true), array("errors" => $this->error), array("alerts" => $this->alert));
            }

            $this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
            return;
        }

        $this->checkVersion() && $this->model_module_qa->checkDatabaseStructure($this->alert);



        $this->checkVersion();

        $this->data['heading_title'] = $this->language->get('extension_name');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_toggle_navigation'] = $this->language->get('text_toggle_navigation');
        $this->data['text_legal_notice'] = $this->language->get('text_legal_notice');
        $this->data['text_license'] = $this->language->get('text_license');
        $this->data['text_extension_information'] = $this->language->get('text_extension_information');
        $this->data['text_terms'] = $this->language->get('text_terms');
        $this->data['text_license_text'] = $this->language->get('text_license_text');
        $this->data['text_other_extensions'] = sprintf($this->language->get('text_other_extensions'), OTHER_EXTENSIONS);
        $this->data['text_support_subject'] = $this->language->get('text_support_subject');
        $this->data['text_faq'] = $this->language->get('text_faq');
        $this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_first_page'] = $this->language->get('text_first_page');
        $this->data['text_all'] = $this->language->get('text_all');
        $this->data['text_store_email_address'] = $this->language->get('text_store_email_address');
        $this->data['text_customer_email_address'] = $this->language->get('text_customer_email_address');

        $this->data['tab_settings'] = $this->language->get('tab_settings');
        $this->data['tab_form'] = $this->language->get('tab_form');
        $this->data['tab_qa'] = $this->language->get('tab_qa');
        $this->data['tab_support'] = $this->language->get('tab_support');
        $this->data['tab_about'] = $this->language->get('tab_about');
        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_faq'] = $this->language->get('tab_faq');
        $this->data['tab_services'] = $this->language->get('tab_services');
        $this->data['tab_changelog'] = $this->language->get('tab_changelog');
        $this->data['tab_extension'] = $this->language->get('tab_extension');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_apply'] = $this->language->get('button_apply');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['button_close'] = $this->language->get('button_close');
        $this->data['button_upgrade'] = $this->language->get('button_upgrade');

        $this->data['help_new_question_notification'] = $this->language->get('help_new_question_notification');
        $this->data['help_question_reply_notification'] = $this->language->get('help_question_reply_notification');
        $this->data['help_notificatin_emails'] = $this->language->get('help_notificatin_emails');
        $this->data['help_notificatin_email_from'] = $this->language->get('help_notificatin_email_from');
        $this->data['help_display_questions'] = $this->language->get('help_display_questions');
        $this->data['help_items_per_page'] = $this->language->get('help_items_per_page');
        $this->data['help_preload'] = $this->language->get('help_preload');
        $this->data['help_display_all_languages'] = $this->language->get('help_display_all_languages');
        $this->data['help_remove_sql_changes'] = $this->language->get('help_remove_sql_changes');

        $this->data['entry_installed_version'] = $this->language->get('entry_installed_version');
        $this->data['entry_extension_status'] = $this->language->get('entry_extension_status');
        $this->data['entry_new_question_notification'] = $this->language->get('entry_new_question_notification');
        $this->data['entry_notificatin_emails'] = $this->language->get('entry_notificatin_emails');
        $this->data['entry_notificatin_email_from'] = $this->language->get('entry_notificatin_email_from');
        $this->data['entry_question_reply_notification'] = $this->language->get('entry_question_reply_notification');
        $this->data['entry_display_all_languages'] = $this->language->get('entry_display_all_languages');
        $this->data['entry_remove_sql_changes'] = $this->language->get('entry_remove_sql_changes');
        $this->data['entry_form_display_name'] = $this->language->get('entry_form_display_name');
        $this->data['entry_form_require_name'] = $this->language->get('entry_form_require_name');
        $this->data['entry_form_display_email'] = $this->language->get('entry_form_display_email');
        $this->data['entry_form_require_email'] = $this->language->get('entry_form_require_email');
        $this->data['entry_form_display_phone'] = $this->language->get('entry_form_display_phone');
        $this->data['entry_form_require_phone'] = $this->language->get('entry_form_require_phone');
        $this->data['entry_form_display_custom'] = $this->language->get('entry_form_display_custom');
        $this->data['entry_form_require_custom'] = $this->language->get('entry_form_require_custom');
        $this->data['entry_form_custom_field_name'] = $this->language->get('entry_form_custom_field_name');
        $this->data['entry_form_display_captcha'] = $this->language->get('entry_form_display_captcha');
        $this->data['entry_form_require_captcha'] = $this->language->get('entry_form_require_captcha');
        $this->data['entry_display_questions'] = $this->language->get('entry_display_questions');
        $this->data['entry_new_questions_status'] = $this->language->get('entry_new_questions_status');
        $this->data['entry_items_per_page'] = $this->language->get('entry_items_per_page');
        $this->data['entry_preload'] = $this->language->get('entry_preload');
        $this->data['entry_display_question_author'] = $this->language->get('entry_display_question_author');
        $this->data['entry_display_question_date'] = $this->language->get('entry_display_question_date');
        $this->data['entry_display_answer_author'] = $this->language->get('entry_display_answer_author');
        $this->data['entry_display_answer_date'] = $this->language->get('entry_display_answer_date');
        $this->data['entry_extension_name'] = $this->language->get('entry_extension_name');
        $this->data['entry_extension_compatibility'] = $this->language->get('entry_extension_compatibility');
        $this->data['entry_extension_store_url'] = $this->language->get('entry_extension_store_url');
        $this->data['entry_copyright_notice'] = $this->language->get('entry_copyright_notice');

        $this->data['error_ajax_request'] = $this->language->get('error_ajax_request');

        $this->data['ext_name'] = EXTENSION_NAME;
        $this->data['ext_version'] = EXTENSION_VERSION;
        $this->data['ext_id'] = EXTENSION_ID;
        $this->data['ext_compatibility'] = EXTENSION_COMPATIBILITY;
        $this->data['ext_store_url'] = EXTENSION_STORE_URL;
        $this->data['ext_support_email'] = EXTENSION_SUPPORT_EMAIL;
        $this->data['ext_support_forum'] = EXTENSION_SUPPORT_FORUM;
        $this->data['other_extensions_url'] = OTHER_EXTENSIONS;

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'active'    => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'active'    => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('extension_name'),
            'href'      => $this->url->link('module/qa', 'token=' . $this->session->data['token'], 'SSL'),
            'active'    => true
        );

        $this->data['save'] = $this->url->link('module/qa', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['upgrade'] = $this->url->link('module/qa/upgrade', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['update_pending'] = !$this->checkVersion();

        $this->data['ssl'] = (int)$this->config->get('config_use_ssl') ? 's' : '';

        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        $this->data['multilingual'] = count($languages) > 1;
        $this->data['languages'] = arrayRemapKeysToIds('language_id', $languages);

        $this->data['installed_version'] = $this->installedVersion();

        # Loop through all settings for the post/config values
        foreach (array_keys($this->defaults) as $setting) {
            if (isset($this->request->post[$setting])) {
                $this->data[$setting] = $this->request->post[$setting];
            } else {
                $this->data[$setting] = $this->config->get($setting);
                if ($this->data[$setting] === null) {
                    if (!isset($this->alert['warning']['unsaved']) && $this->checkVersion())  {
                        $this->alert['warning']['unsaved'] = $this->language->get('error_unsaved_settings');
                    }
                    if (isset($this->defaults[$setting])) {
                        $this->data[$setting] = $this->defaults[$setting];
                    }
                }
            }
        }

        if (isset($this->session->data['error'])) {
            $this->error = $this->session->data['error'];

            unset($this->session->data['error']);
        }

        if (isset($this->error['warning'])) {
            $this->alert['warning']['warning'] = $this->error['warning'];
        }

        if (isset($this->error['error'])) {
            $this->alert['error']['error'] = $this->error['error'];
        }

        if (isset($this->session->data['success'])) {
            $this->alert['success']['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        }

        $this->data['errors'] = $this->error;

        $this->data['token'] = $this->session->data['token'];

        $this->data['alerts'] = $this->alert;

        $this->template = 'module/qa.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    public function install() {
       // $this->model_module_qa->applyDatabaseChanges();

        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();
        $languages = arrayRemapKeysToIds('language_id', $languages);
        foreach ($languages as $language_id => $language) {
            $this->defaults['qa_notification_emails'][$language_id] = $this->config->get('config_email');
        }

        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('qa', $this->defaults);
    }

    public function uninstall() {
        if ($this->config->get("qa_remove_sql_changes")) {
         //   $this->model_module_qa->revertDatabaseChanges();
        }

        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('qa');
    }

    public function upgrade() {
        $ajax_request = isset($this->request->server['HTTP_X_REQUESTED_WITH']) && !empty($this->request->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

        $response = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateUpgrade()) {
            $this->load->model('setting/setting');

            if ($this->model_module_qa->upgradeDatabaseStructure($this->installedVersion(), $this->alert)) {
                $settings = array();

                // Go over all settings, add new values and remove old ones
                foreach ($this->defaults as $setting => $default) {
                    $value = $this->config->get($setting);
                    if ($value === null) {
                        $settings[$setting] = $default;
                    } else {
                        $settings[$setting] = $value;
                    }
                }

                if (!is_array($settings['qa_notification_emails'])) {
                    $email = $settings['qa_notification_emails'];
                    $settings['qa_notification_emails'] = array();

                    $this->load->model('localisation/language');

                    $languages = $this->model_localisation_language->getLanguages();
                    $languages = arrayRemapKeysToIds('language_id', $languages);

                    foreach ($languages as $language_id => $language) {
                        $settings['qa_notification_emails'][$language_id] = $email ? $email : $this->config->get('config_email');
                    }
                }

                $settings['qa_installed_version'] = EXTENSION_VERSION;

                $this->model_setting_setting->editSetting('qa', $settings);

                $this->session->data['success'] = sprintf($this->language->get('text_success_upgrade'), EXTENSION_VERSION);
                $response['success'] = true;
                $response['msg'] = sprintf($this->language->get('text_success_upgrade'), EXTENSION_VERSION);
            } else {
                $this->alert['error']['database_upgrade'] = $this->language->get('error_upgrade_database');
                $response = array_merge(array("error" => true), array("errors" => $this->error), array("alerts" => $this->alert));
            }
        } else {
            $response = array_merge(array("error" => true), array("errors" => $this->error), array("alerts" => $this->alert));
        }

        if (!$ajax_request) {
            $this->redirect($this->url->link('module/qa', 'token=' . $this->session->data['token'], 'SSL'));
        } else {
            $this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
            return;
        }
    }

    private function checkVersion() {
        $errors = false;

        $installed_version = $this->installedVersion();

        if ($installed_version != EXTENSION_VERSION) {
            $errors = true;
            $this->alert['info']['version'] = sprintf($this->language->get('error_version'), EXTENSION_VERSION);
        }

        return !$errors;
    }

    private function validate() {
        $errors = false;

        if (!$this->user->hasPermission('modify', 'module/qa')) {
            $errors = true;
            $this->alert['error']['permission'] = $this->language->get('error_permission');
        }

        if (!$errors) {
            return $this->checkVersion() && $this->model_module_qa->checkDatabaseStructure($this->alert);
        } else {
            return false;
        }
    }

    private function validateForm($data) {
        $errors = false;

        if ((int)$data['qa_new_question_notification']) {
            foreach ((array)$data['qa_notification_emails'] as $language_id => $value) {
                if (empty($value)) {
                    $errors = true;
                    $this->error["qa_notification_emails[{$language_id}]"] = $this->language->get('error_missing_email');
                } else {
                    $emails = explode(',', $value);

                    foreach ($emails as $email) {
                        if (!validate_email($email)) {
                            $errors = true;
                            $this->error["qa_notification_emails[{$language_id}]"] = $this->language->get('error_email');
                            $this->error["qa_notification_emails"][$language_id] = $this->language->get('error_email');
                        }
                    }
                }
            }
        }

        if ((int)$data['qa_form_display_custom_field']) {
            foreach ((array)$data['qa_form_custom_field_name'] as $language_id => $value) {
                if (utf8_strlen(trim($value)) == 0) {
                    $errors = true;
                    $this->error["qa_form_custom_field_name[{$language_id}]"] = $this->language->get('error_custom_field_name');
                    $this->error["qa_form_custom_field_name"][$language_id] = $this->language->get('error_custom_field_name');
                }
            }
        }

        if (!is_numeric($data['qa_items_per_page']) || (int)$data['qa_items_per_page'] != $data['qa_items_per_page'] || (int)$data['qa_items_per_page'] < 0) {
            $errors = true;
            $this->error['qa_items_per_page'] = $this->language->get('error_items_per_page');
        }

        if ($errors) {
            $errors = true;
            $this->alert['warning']['warning'] = $this->language->get('error_warning');
        }

        if (!$errors) {
            return $this->validate();
        } else {
            return false;
        }
    }

    private function validateUpgrade() {
        $errors = false;

        if (!$this->user->hasPermission('modify', 'module/qa')) {
            $errors = true;
            $this->alert['error']['permission'] = $this->language->get('error_permission');
        }

        return !$errors;
    }

    private function installedVersion() {
        $installed_version = $this->config->get('qa_installed_version');
        return $installed_version ? $installed_version : '1.6.0';
    }
}
?>
