<?php

// Heading
$_['heading_title']        = 'PayU';
$_['text_payu']            = '<a onclick="window.open(\'http://www.payu.hu\');"><img src="view/image/payment/payu.png" alt="PayU" title="PayU" style="border: 1px solid #EEEEEE;" /></a>';

// Text
$_['text_payment']         = 'Fizetés';
$_['text_success']         = 'Siker: A PayU fiók részleteinek módosítása megtörtént!';
$_['text_language_hu']     = 'Magyar';
$_['text_language_en']     = 'Angol';

// Entry
$_['entry_merchant_huf']   = 'Eladó szállító kódja <strong>(HUF)</strong>:<span class="help">A <a href="https://secure.payu.hu/cpanel/" target="_blank" style="text-decoration: none;">PayU vezérlőpulton</a> keresztül a Fiókkezelő/Fiókbeállítások menüben érhető el</span>';
$_['entry_secret_key_huf'] = 'IPN titkos kulcs <strong>(HUF)</strong>:';

$_['entry_merchant_eur']   = 'Eladó szállító kódja <strong>(EUR)</strong>:<span class="help">A <a href="https://secure.payu.hu/cpanel/" target="_blank" style="text-decoration: none;">PayU vezérlőpulton</a> keresztül a Fiókkezelő/Fiókbeállítások menüben érhető el</span>';
$_['entry_secret_key_eur'] = 'IPN titkos kulcs <strong>(EUR)</strong>:';

$_['entry_language']       = 'Fizetési felület nyelve:';
$_['entry_testorder']      = 'Teszt üzemmód:';
$_['entry_debug']          = 'Hibakeresés kérése:<span class="help">A PayU csapat beavatkozásának kérése</span>';
$_['entry_order_timeout']  = 'Rendelés időkerete (másodperc):<span class="help">Meghatározza az időkeretet, amíg a rendelés megadható (opcionális)</span>';

$_['entry_order_status']   = 'Rendelés állapota:';
$_['entry_status']         = 'Állapot:';
$_['entry_sort_order']     = 'Sorrend:';

// Error
$_['error_permission']     = 'Figyelmeztetés: Az PayU-val történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_merchant']       = 'Eladó szállító kód szükséges!';
?>