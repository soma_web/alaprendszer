<?php
class ModelShippingPickup extends Model {
    function getQuote($address, $fizetesek="") {
		$this->load->language('shipping/pickup');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('pickup_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if (!$this->config->get('pickup_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		$method_data = array();
	
		if ($status) {
            $fizetesi_mod = array();

            if ($fizetesek){
                foreach($fizetesek as $value) {
                    if ( $this->config->get("pickup_payment_".$value['code']) &&  $this->config->get("pickup_payment_".$value['code']) == 1){
                        $fizetesi_mod[] = array(
                            'fizetesi_mod' => $value['code']
                        );
                    }
                }
            }


            $warehouse_quote = array();
            $pickup_warehouse = $this->config->get("pickup_warehouse");
            if ($pickup_warehouse == 1) {
                $this->load->model("warehouse/warehouse");
                $warehouses = $this->model_warehouse_warehouse->getWarehouses();

                /*foreach($warehouses as $warehouse) {
                    $carts = $this->session->data['cart'];
                    $termek = array();
                    foreach($carts as $key=>$cart) {
                        $termek[$key] = $this->model_warehouse_warehouse->getTotalWarehouseQuantity($warehouse['warehouse_id'],$key);
                    }
                    $warehouse_quote[] = array(
                        'warehouse' =>  $warehouse['city'].$warehouse['postcode'].$warehouse['address'],
                        'termek'    =>  $termek
                    );

                }*/


                $carts = $this->session->data['cart'];
                $this->load->model("catalog/product");
                foreach($carts as $key=>$cart) {
                        $uzlet = $this->model_warehouse_warehouse->getWarehousesProduct($key);
                    $warehouse_quote[] = array(
                        'termek'    =>  $this->getProductName($key),
                        'warehouse' =>  $uzlet
                    );

                }
            }

            $quote_data = array();

            $quote_data['pickup'] = array(
                'code'         => 'pickup.pickup',
                'title'        => $this->config->get('pickup_text') != null ? $this->config->get('pickup_text') :  $this->language->get('text_description'),
                'fizetesi_mod'  => $fizetesi_mod,
                'cost'         => 0.00,
                'tax_class_id' => 0,
                'text'         => $this->currency->format(0.00),
                'netto_text'   => $this->currency->format(0.00),
                'warehouse_quote'   => $warehouse_quote,
                'ado_title'    => "",
                'mail'         => $this->config->get('item_mail')
            );

      		$method_data = array(
        		'code'              => 'pickup',
        		'title'             => $this->config->get('pickup_header') != null ? $this->config->get('pickup_header') :  $this->language->get('text_description'),
        		'quote'             => $quote_data,
                'fizetesi_mod'      => $fizetesi_mod,
				'sort_order'        => $this->config->get('pickup_sort_order'),
				'warehouse_quote'   => $warehouse_quote,
        		'error'             => false
      		);
		}
	
		return $method_data;
	}

    function getProductName($product_id) {
        $sql="SELECT pd.name, p.cikkszam
        FROM " . DB_PREFIX . "product p
            LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
            LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)

            WHERE p.product_id = '" . (int)$product_id . "'
                AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

        $query = $this->db->query($sql);
        $vissza = "";

        if ($query->num_rows > 0) {
            $vissza = $query->row['cikkszam']. " - " .$query->row['name'];
        }
        return $vissza;

    }
}
?>