<?php
class ModelSettingSetting {

    private $db;

    public function __construct($query) {
        $this->db = $query;
    }

    public function getSettingOtpWebshopClient() {
        $data = array();

        $sql = "SELECT * FROM " . DB_PREFIX . "setting
            WHERE store_id = '0'
            AND `group` = 'otp'
            AND `key`   = 'otp_webshop_client'";
        $query = $this->db->query($sql);


        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                $data[$result['key']] = unserialize($result['value']);
            }
        }

        return $data['otp_webshop_client'];
    }

    public function getSettingOtpHaromszereplos() {
        $data = array();

        $sql = "SELECT * FROM " . DB_PREFIX . "setting
            WHERE store_id = '0'
            AND `group` = 'otp'
            AND `key`   = 'otp_haromszereplos'";
        $query = $this->db->query($sql);


        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                $data[$result['key']] = unserialize($result['value']);
            }
        }

        return $data['otp_haromszereplos'];
    }


}
?>