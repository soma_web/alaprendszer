<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Hírlevél Marketing Feliratkozók';

// Entry
$_['entry_yes']								= 'Igen';
$_['entry_no']								= 'Nem';
$_['entry_store']							= 'Áruház: ';

// Text
$_['text_success']           				= 'Siker: Hozzáadtad a(z) %s emaileket!';
$_['text_success_delete']           		= 'Siker: A kijelölt elemek törölve';
$_['text_add_info']							= 'Emailek vagy Név|Email vagy Név|Keresztnév|Email vesszővel vagy új sorral tagolva:';
$_['text_default']							= 'Alap';

// Button 
$_['button_subscribe']						= 'Feliratkozás';
$_['button_unsubscribe']					= 'Leiratkozás';
$_['button_csv']							= 'Export CSV';

// Column 
$_['column_actions']						= 'Akciók';
$_['column_name']							= 'Név';
$_['column_email']							= 'Email';
$_['column_subscribed']						= 'Előfizetés';
$_['column_list']							= 'Marketing lists';
$_['column_store']							= 'Áruház';
$_['column_language']						= 'Nyelv';

?>
