<?php
class ControllerModuleCategoryCenter extends Controller {
    protected function index($setting) {
        $this->language->load('module/category',$this->config->get('config_template'));

        $this->data['heading_title'] = $this->language->get('heading_title');
        if ($this->config->get('category_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }
        $this->load->model('tool/image');

        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string)$this->request->get['path']);
        } else {
            $parts = array();
        }

        if (isset($parts[0])) {
            $this->data['category_id'] = $parts[0];
        } else {
            $this->data['category_id'] = 0;
        }

        if (isset($parts[1])) {
            $this->data['child_id'] = $parts[1];
        } else {
            $this->data['child_id'] = 0;
        }

        if ($this->config->get('category_center_module_fejlec') == 1){
            $this->data['description_show'] = true;
        } else {
            $this->data['description_show'] = false;
        }

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $this->data['categories'] = array();
        $box_magassag = $this->config->get('category_center_box_height');

        $this->data['box_magassag'] = $box_magassag ?  "height: " .$box_magassag. "px" : "";

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            $children_data = array();

            $children = $this->model_catalog_category->getCategories($category['category_id']);

            foreach ($children as $child) {
                $data = array(
                    'filter_category_id'  => $child['category_id'],
                    'filter_sub_category' => true
                );

                $ha_piacter=$this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']);

                if ($category['piacter'] == 1){
                    $ha_piacter.="&piacter=piacter";
                }

                if ($setting['count']) {
                    $product_total = $this->model_catalog_product->getTotalProducts($data);

                    $children_data[] = array(
                        'category_id'   => $child['category_id'],
                        'name'          => $child['name'] . ' (' . $product_total . ')',
                        'description'   => $child['description'],
                        'thumb'         => $child['image'] ? $this->model_tool_image->resize($child['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false) : $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false),
                        'href'          => $ha_piacter
                    );
                } else {
                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name'        => $child['name'],
                        'description'        => $child['description'],
                        'thumb' => $child['image'] ? $this->model_tool_image->resize($child['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false) : $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false),
                        'href'        => $ha_piacter
                    );
                }
            }

            $data = array(
                'filter_category_id'  => $category['category_id'],
                'filter_sub_category' => true
            );

            $ha_piacter=$this->url->link('product/category', 'path=' . $category['category_id']);
            if ($category['piacter'] == 1){
                $ha_piacter.="&piacter=piacter";
            }

            if ($category['image'] && file_exists(DIR_IMAGE . $category['image'])) {
                $image = $category['image'];
            } else {
                $image = 'no_image.jpg';
            }

            if ($setting['count']) {
                $product_total = $this->model_catalog_product->getTotalProducts($data);


                $this->data['categories'][] = array(
                    'category_id'       => $category['category_id'],
                    'name'              => $category['name'],
                    'product-count'     => $this->model_catalog_product->getTotalProducts($data),
                    'description'       => $category['description'],
                    'children'          => $children_data,
                    'thumb_image_width' => $this->config->get('config_image_category_width'),
                    'thumb'             => $image ? $this->model_tool_image->resize($image, $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false) : $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false),
                    'href'              => $ha_piacter
                );
            } else {
                $this->data['categories'][] = array(
                    'category_id'       => $category['category_id'],
                    'name'              => $category['name'],
                    'description'       => $category['description'],
                    'children'          => $children_data,
                    'thumb_image_width' => $this->config->get('config_image_category_width'),
                    'thumb'             => $image ? $this->model_tool_image->resize($image, $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false) : $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false),
                    'href'              => $ha_piacter
                );
            }
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category_center.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/category_center.tpl';
        } else {
            $this->template = 'default/template/module/category_center.tpl';
        }

        $this->render();
    }
}
?>