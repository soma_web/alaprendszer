<?php
// Heading
$_['heading_title']       = 'Adult Page';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module adult page!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_or']             = 'OR';
$_['text_image']          = 'Image';
$_['text_color']          = 'Color';
$_['text_browse']         = 'Browse';
$_['text_clear']          = 'Clear';
$_['text_image_manager']  = 'Image Manager';




// Entry
$_['entry_image']         	   = 'Image:<br /><span class="help">(no resize)</span>';
$_['entry_pattern']       	   = 'Pattern (no resize)<br /><span class="help">show if image doesn\'t cover all screen</span>';
$_['entry_bgcolor']            = 'Background color<br /><span class="help">appear if image is smaller than screen and pattern is not defined</span>';  
$_['entry_timer']              = 'Timer<br /><span class="help">seconds after adult page will hide (default 2 sec)</span>';
$_['entry_url']                = 'URL<br /><span class="help">Show only if this URL is requested<br /> E.g: product/product&product_id=50</span>';
$_['entry_layout']             = 'Layout:';
$_['entry_position']           = 'Position:';
$_['entry_status']             = 'Status:';
$_['entry_sort_order']         = 'Sort Order:';

// Error 
$_['error_permission']         = 'Warning: You do not have permission to modify module adult page!';
$_['error_image']              = 'No image uploaded for adult screen!';
$_['error_url']                = 'URL specified is from other layout!';

?>