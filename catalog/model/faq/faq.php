<?php
class ModelFaqFaq extends Model {
	public function getFaqByCategory($idCategory) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq f WHERE f.idCategory = '" . (int)$idCategory . "' ORDER BY f.question");
		return $query->rows;
	}
	
	public function getFaqCategories() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq_categories fc ORDER BY fc.category");
		return $query->rows;
	}
	
	public function getFaqsQuery($query) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq f, " . DB_PREFIX . "faq_categories fc WHERE f.idCategory = fc.idCategory AND (f.question LIKE '%".$query."%' OR f.response LIKE '%".$query."%')");
		
		return $query->rows;
	}	
}
?>