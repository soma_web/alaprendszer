<?php
// Heading
$_['heading_title']       = 'Footer beállítások';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified module TG Helios Footer Module!';
$_['text_left']        = 'Left';
$_['text_right']       = 'Right';
$_['text_footer']       = 'Footer';
$_['text_image_manager'] = 'Image Manager';
$_['text_description'] = 'Description';
$_['text_info_title'] = 'Title:';
$_['text_social_title'] = 'Follow Us Title:';

$_['text_informacio']        = 'Információk';
$_['text_ugyfelszolgalat']   = 'Ügyfélszolgálat';
$_['text_extrak']            = 'Extrák';
$_['text_fiok']              = 'Fiók';
$_['text_uzenet']            = 'Üzenjen nekünk';
$_['text_hirlevel']          = 'Hírlevél';
$_['text_elerhetoseg']       = 'Egyéb';
$_['text_kategoriak']        = 'Kategória lista';
$_['tab_likebox']            = 'Facebook doboz';



// Tab
$_['tab_gen']           = 'Alap beállítás';
$_['tab_info']          = 'Információk';
$_['tab_twitter']       = 'Twitter';
$_['tab_facebook']      = 'Facebook doboz';
$_['tab_contact']       = 'Kapcsolatok';
$_['tab_payment']       = 'Payment Images';
$_['tab_default']       = 'Facebook doboz';

$_['tab_terkep']                  = 'Google térkép';
$_['tab_facebook_like_button']    = 'Social ikonok';
$_['tab_egyeb']                   = 'Egyéb';

$_['tab_footer_alap']             = 'Footer Alap';
$_['tab_footer_masodlagos']       = 'Footer Másodlagos';
$_['tab_sidebar']                 = 'Sidebar';


// Entry
$_['entry_limit']      = 'Limit:';
$_['entry_code']       = 'Text';
$_['entry_position']   = 'Position:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';
$_['entry_url'] = 'URL:';
$_['entry_storetitle'] = 'Áruházak:';
$_['entry_unsubscribe']  = 'Unsubscribe:';
$_['entry_mail']   		 = 'Send Email';
$_['entry_registered']   = 'Registered Users:';
$_['entry_informationurl'] = 'URL:';
$_['entry_informationtitle'] = 'Information Title:';
$_['entry_categoryurl'] = 'URL:';
$_['entry_categorytitle'] = 'Category Title:';
$_['entry_supporttitle'] = 'Support Title:';
$_['entry_image']        = 'Image: <br /><span class="help">Click on the image to change it.</span>';
$_['entry_url']        = 'Link Address: ';

// Buttons
$_['button_addslide']  = 'Add New Slide';
$_['button_remove']    = 'Remove';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module TG Prada Footer Module!';

?>