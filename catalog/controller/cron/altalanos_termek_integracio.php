<?php   
class ControllerCronAltalanosTermekIntegracio extends Controller
{

    private $products = array();




    public function index() {
        $this->load->model('cron/altalanos_termek_integracio');
        $this->load->model('catalog/product');

        $integraciok = $this->model_cron_altalanos_termek_integracio->getIntegracions();

        foreach ($integraciok as $integracio) {
            $data['products'] = $integracio['products'];
            $products = $this->model_catalog_product->getProducts($data);
            $this->products = array();
            foreach($products as $product) {
                if (!$product) {
                    continue;
                }
                $termekem = $this->product->productPreparation($product);
                $termekem['href']   = str_replace('amp;','',$termekem['href']);
                $termekem['category'] = $this->getCategoryes($product['product_id']);
                $termekem['pickpack'] = $integracio['pickpack'];

                /* ------- Szállítás --------- */
                $this->load->model('shipping/'.$integracio['shipping']);
                $address=array(
                    'country_id'    => 97,
                    'zone_id'       => 0
                );

                if ($integracio['shipping'] == 'item_product' || $integracio['shipping'] == 'item_product_gls' ) {
                    $termekem['shipping_price'] = !empty($product['egyedi_szallitas']) ? $product['egyedi_szallitasi_dij'] : $integracio['shipping_price'];
                } else {
                    $shipping = $this->{'model_shipping_' . $integracio['shipping']}->getQuote($address);

                    $sh_price = 0;
                    foreach($shipping['quote'] as $value) {
                        $sh_price = $value['text'];
                        break;
                    }
                    $sh_price = str_replace('.', '', $sh_price);
                    $sh_price = str_replace(',', '', $sh_price);
                    $sh_price = str_replace(' ', '', $sh_price);
                    $termekem['shipping_price'] = (int)$sh_price;
                }
                $termekem['shipping_price'] = round($termekem['shipping_price']);
                /* ------- Szállítás vége --------- */

                if ($termekem['raktaron']) {
                    $termekem['delivery_time'] = $integracio['shipping_time_in_stock'];
                } else {
                    $termekem['delivery_time'] = $integracio['shipping_time_not_stock'];
                }



                /* ------- Garancia --------- */
                if (!empty($product['garancia_egyseg']) ) {
                    $termekem['garancia_ertek'] =  $product['garancia_egyseg'] == 1 ? (int)$product['garancia_ertek'] * 12: $product['garancia_ertek'];
                } else {
                    $termekem['garancia_ertek'] = $integracio['warranty'];
                }
                /* ------- Garancia vége --------- */



                /* ------- Description --------- */
                if (!empty($product['short_description']) && $integracio['description'] == "short_description") {
                    $termekem['description'] = $product['short_description'];
                }
                $termekem['description'] = $this->stripDescription($termekem['description'],$integracio['pieces']);
                $termekem['name'] = $this->stripDescription($termekem['name'],$integracio['pieces']);
                /* ------- Description vége --------- */


                /* ------- Simple price --------- */
                if ($termekem['special']) {
                    $special = str_replace('.', '', $termekem['special']);
                    $special = str_replace(',', '', $special);
                    $special = str_replace(' ', '', $special);
                    $termekem['simple_special'] = (int)$special;
                }
                $price = str_replace('.', '', $termekem['price']);
                $price = str_replace(',', '', $price);
                $price = str_replace(' ', '', $price);
                $termekem['simple_price'] = (int)$price;
                /* ------- Simple price vége --------- */


                $this->products[] = $termekem;

            }



            if ($this->products) {
                $pages = $integracio['connects'];
                if (!empty($pages)) {
                    foreach ($pages as $key => $page) {
                        echo '';
                        if ($key == 'argep') {
                            $this->argep('argep');

                        } elseif ($key == 'olcso') {
                            $this->olcso('olcso');

                        } elseif ($key == 'kirakat') {
                            $this->kirakat('kirakat');

                        } elseif ($key == 'olcsobbat' ) {
                            $this->kirakat('olcsobbat');

                        } elseif ($key == 'arkozpont') {
                            $this->kirakat('arkozpont');

                        } elseif ($key == 'joaron') {
                            $this->joaron('joaron');

                        } elseif ($key == 'arukereso') {
                            $this->arukereso('arukereso');

                        } elseif ($key == 'osszehasonlitom') {
                            $this->arukereso('osszehasonlitom');


                        }
                    }
                }
            }
        }
    }

    public function stripDescription($description,$length=20) {

        $hun_html = array("&ouml;","&Ouml;","&uuml;","&Uuml;","&eacute;","&Eacute;","&iacute;","&Iacute;","&Aacute;","&aacute;","&oacute;","&Oacute;","&uacute;","&Uacute;","\r","\n","&nbsp","\t");
        $hun_real = array("ö","Ö","ü","Ü","é","É","i","Í","Á","á","ó","Ó","ú","Ú"," "," "," "," ");

        $description        = strip_tags(html_entity_decode($description, ENT_QUOTES, 'UTF-8'));
        $description        = str_replace($hun_html,$hun_real,$description);
        $description_prices = explode(" ", $description);
        foreach($description_prices as $key=>$value) {
            if (empty($value)) {
                unset ($description_prices[$key]);
            }
        }
        $count_pieces       = count($description_prices);

        $darabolt = false;
        $product_description    = "";
        for ($i = 0; $i <= $length; $i++) {
            if (!empty($description_prices[$i])) {
                $darabolt = true;
                $product_description .= trim($description_prices[$i]) . " ";
            }
        }


        if ($darabolt && !empty($description_prices[$i])) {
            $product_description .= "...";
        }
        return $product_description;
    }

    private function getCategoryes($product_id) {
        $hun_html = array("&ouml;","&Ouml;","&uuml;","&Uuml;","&eacute;","&Eacute;","&iacute;","&Iacute;","&Aacute;","&aacute;","&oacute;","&Oacute;","&uacute;","&Uacute;");
        $hun_real = array("ö","Ö","ü","Ü","é","É","i","Í","Á","á","ó","Ó","ú","Ú");

        $category_hierarchy = $this->db->query("SELECT d.name
							FROM " . DB_PREFIX . "product_to_category pc
							LEFT JOIN " . DB_PREFIX . "category_description d ON d.category_id = pc.category_id
							LEFT JOIN " . DB_PREFIX . "category_path p ON p.category_id = d.category_id
							WHERE pc.product_id = '$product_id'
							ORDER BY p.level ASC");


        $category_names = '';
        $category_names_joaron = '';
        if ($category_hierarchy->num_rows > 0) {

            foreach($category_hierarchy->rows as $key=>$value) {
                $category_hierarchy->rows[$key] = $value['name'];
            }


            $category_hierarchy = array_unique($category_hierarchy->rows);
            $to_sring_check = implode(" / ", $category_hierarchy);
            if (!empty($to_sring_check)) {
                $category_names = str_replace($hun_html, $hun_real,
                    iconv("UTF-8", "UTF-8//IGNORE",
                        preg_replace('/[\x00-\x1f]/', ' ',
                            strip_tags(
                                html_entity_decode(
                                    $to_sring_check
                                )
                            )

                        )
                    )
                );
                $category_names_joaron = str_replace(" / ", " > ", $category_names);
            }
        }

        return array(
            'category_names'        => $category_names,
            'category_names_joaron' => $category_names_joaron
        );


    }

    private function argep($filename) {
        $xml_argep = new DOMDocument("1.0","UTF-8");
        $root_argep = $xml_argep ->createElement("termeklista");
        $xml_argep ->appendChild($root_argep);

        foreach($this->products as $product) {


            $id = $xml_argep->createElement("cikkszam");
            $idText = $xml_argep->createCDATASection($product['cikkszam']);
            $id->appendChild($idText);

            $title = $xml_argep->createElement("nev");
            $titleText = $xml_argep->createCDATASection($product['name']);
            $title->appendChild($titleText);

            $desc = $xml_argep->createElement("leiras");
            $desc_text = $xml_argep->createCDATASection($product['description']);
            $desc->appendChild($desc_text);

            $price = $xml_argep->createElement("ar");
            $price_text = $xml_argep->createTextNode(!empty($product['simple_special']) ? $product['simple_special'] : $product['simple_price']);
            $price->appendChild($price_text);

            $picture = $xml_argep->createElement("fotolink");
            $picture_text = $xml_argep->createCDATASection($product['thumb_product']);
            $picture->appendChild($picture_text);

            $url = $xml_argep->createElement("termeklink");
            $url_text = $xml_argep->createCDATASection($product['href']);
            $url->appendChild($url_text);

            $ido = $xml_argep->createElement("ido");
            $ido_text = $xml_argep->createCDATASection($product['delivery_time']);
            $ido->appendChild($ido_text);

            $szallitas = $xml_argep->createElement("szallitas");
            $szallitas_text = $xml_argep->createTextNode($product['shipping_price']);
            $szallitas->appendChild($szallitas_text);

            $book = $xml_argep->createElement("termek");
            $book->appendChild($id);
            $book->appendChild($title);
            $book->appendChild($desc);
            $book->appendChild($price);
            $book->appendChild($picture);
            $book->appendChild($url);
            $book->appendChild($ido);
            $book->appendChild($szallitas);

            $root_argep->appendChild($book);
        }

        $xml_argep ->formatOutput = true;
        $xml_argep ->save(DIR_ARUHAZ."/csv/arukeresok/".$filename.".xml") or die("HIBA AZ Árgép XML FILE ÍRÁSA SORÁN!!!");

    }

    private function arukereso($filename) {
        $xml_arukereso = new DOMDocument("1.0","UTF-8");
        $root_arukereso = $xml_arukereso ->createElement("products");
        $xml_arukereso ->appendChild($root_arukereso);

        foreach($this->products as $product) {

            $indentifier_arukereso = $xml_arukereso->createElement("identifier");
            $indentifierText = $xml_arukereso->createTextNode($product['product_id']);
            $indentifier_arukereso->appendChild($indentifierText);

            $manufacturer_arukereso = $xml_arukereso->createElement("manufacturer");
            $manufacturerText = $xml_arukereso->createTextNode($product['manufacturers']['name']);
            $manufacturer_arukereso->appendChild($manufacturerText);

            $name_arukereso = $xml_arukereso->createElement("name");
            $osszefuzott_nev = !empty($product['cikkszam']) ? $product['cikkszam'] : $product['model'];
            $osszefuzott_nev .= ' '.$product['name'];
            $osszefuzott_nev = trim($osszefuzott_nev);


            $nameText = $xml_arukereso->createTextNode($osszefuzott_nev);
            $name_arukereso->appendChild($nameText);

            $category_arukereso = $xml_arukereso->createElement("category");
            $category_text = $xml_arukereso->createTextNode($product['category']['category_names']);
            $category_arukereso->appendChild($category_text);

            $url_arukereso = $xml_arukereso->createElement("product_url");
            $url_text = $xml_arukereso->createTextNode($product['href']);
            $url_arukereso->appendChild($url_text);

            $netprice_arukereso = $xml_arukereso->createElement("net_price");
            $netpricearukereso = $xml_arukereso->createTextNode(!empty($product['special_netto']) ? $product['special_netto'] : $product['price_netto']);
            $netprice_arukereso->appendChild($netpricearukereso);

            $grossprice_arukereso = $xml_arukereso->createElement("price");
            $grosspriceText = $xml_arukereso->createTextNode(!empty($product['special']) ? $product['special'] : $product['price']);
            $grossprice_arukereso->appendChild($grosspriceText);

            $picture_arukereso = $xml_arukereso->createElement("image_url");
            $picture_text = $xml_arukereso->createTextNode($product['thumb_product']);
            $picture_arukereso->appendChild($picture_text);

            $desc_arukereso = $xml_arukereso->createElement("description");
            $desc_text = $xml_arukereso->createTextNode($product['description']);
            $desc_arukereso->appendChild($desc_text);

            $deliverytime_arukereso = $xml_arukereso->createElement("delivery_time");
            $deliverytimeText = $xml_arukereso->createTextNode($product['delivery_time']);
            $deliverytime_arukereso->appendChild($deliverytimeText);


            $deliveryprice_arukereso = $xml_arukereso->createElement("delivery_cost");
            $deliverypriceText = $xml_arukereso->createTextNode(!empty($product['shipping_price']) ? $product['shipping_price'] : "FREE");
            $deliveryprice_arukereso->appendChild($deliverypriceText);

            $book2 = $xml_arukereso->createElement("product");

            if (!empty($products['ean'])) {
                $ean_arukereso = $xml_arukereso->createElement("ean_code");
                $eanText = $xml_arukereso->createTextNode($products['ean']);
                $ean_arukereso->appendChild($eanText);
                $book2->appendChild($ean_arukereso);
            }

            $book2->appendChild($indentifier_arukereso);
            $book2->appendChild($manufacturer_arukereso);
            $book2->appendChild($name_arukereso);
            $book2->appendChild($category_arukereso);
            $book2->appendChild($url_arukereso);
            $book2->appendChild($netprice_arukereso);
            $book2->appendChild($grossprice_arukereso);
            $book2->appendChild($picture_arukereso);
            $book2->appendChild($desc_arukereso);
            $book2->appendChild($deliverytime_arukereso);
            $book2->appendChild($deliveryprice_arukereso);

            $root_arukereso->appendChild($book2);
        }

        $xml_arukereso ->formatOutput = true;
        $xml_arukereso ->save(DIR_ARUHAZ."/csv/arukeresok/".$filename.".xml") or die("HIBA AZ Árukereső XML FILE ÍRÁSA SORÁN!!!");

    }

    private function kirakat($filename) {
        $xml_kirakat = new DOMDocument("1.0","UTF-8");
        $root_kirakat = $xml_kirakat ->createElement("catalog");
        $xml_kirakat ->appendChild($root_kirakat);

        foreach($this->products as $product) {
            $id_kirakat   = $xml_kirakat ->createElement("id");
            $idText = $xml_kirakat ->createTextNode($product['product_id']);
            $id_kirakat->appendChild($idText);

            $manufacturer_kirakat   = $xml_kirakat ->createElement("manufacturer");
            $manufacturerText = $xml_kirakat ->createCDATASection($product['manufacturers']['name']);
            $manufacturer_kirakat->appendChild($manufacturerText);

            $name_kirakat   = $xml_kirakat ->createElement("name");
            $nameText = $xml_kirakat ->createCDATASection($product['name']);
            $name_kirakat->appendChild($nameText);

            $netprice_kirakat   = $xml_kirakat ->createElement("netprice");
            $netpriceText = $xml_kirakat ->createTextNode(!empty($product['special_netto']) ? $product['special_netto'] : $product['price_netto']);
            $netprice_kirakat->appendChild($netpriceText);

            $grossprice_kirakat   = $xml_kirakat ->createElement("grossprice");
            $grosspriceText = $xml_kirakat ->createTextNode(!empty($product['special']) ? $product['special'] : $product['price']);
            $grossprice_kirakat->appendChild($grosspriceText);

            $grosspricecarriage_kirakat   = $xml_kirakat ->createElement("grosspricecarriage");
            $grosspricecarriageText = $xml_kirakat ->createTextNode((!empty($product['simple_special']) ? $product['simple_special'] : $product['simple_price'])+($product['shipping_price']));
            $grosspricecarriage_kirakat->appendChild($grosspricecarriageText);

            $deliveryprice_kirakat   = $xml_kirakat ->createElement("deliveryprice");
            $deliverypriceText = $xml_kirakat ->createTextNode($product['shipping_price']);
            $deliveryprice_kirakat->appendChild($deliverypriceText);

            $deliverytime_kirakat   = $xml_kirakat ->createElement("deliverytime");
            $deliverytimeText = $xml_kirakat ->createTextNode($product['delivery_time']);
            $deliverytime_kirakat->appendChild($deliverytimeText);

            $pickpackpoint_kirakat   = $xml_kirakat ->createElement("pickpackpoint");
            $pickpackpointText = $xml_kirakat ->createTextNode($product['pickpack']);
            $pickpackpoint_kirakat->appendChild($pickpackpointText);

            $warranty_kirakat   = $xml_kirakat ->createElement("warranty");
            $warrantyText = $xml_kirakat ->createTextNode($product['garancia_ertek']);
            $warranty_kirakat->appendChild($warrantyText);

            $stock_kirakat   = $xml_kirakat ->createElement("stock");
            $stockText = $xml_kirakat ->createTextNode($product['stock']);
            $stock_kirakat->appendChild($stockText);

            $itemid_kirakat  = $xml_kirakat ->createElement("itemid");
            $itemidText = $xml_kirakat ->createTextNode($product['model']);
            $itemid_kirakat->appendChild($itemidText);

            $url_kirakat   = $xml_kirakat ->createElement("urlsite");
            $url_text = $xml_kirakat ->createCDATASection($product['href']);
            $url_kirakat->appendChild($url_text);

            $picture_kirakat   = $xml_kirakat ->createElement("urlpicture");
            $picture_text = $xml_kirakat ->createCDATASection($product['thumb_product']);
            $picture_kirakat->appendChild($picture_text);

            $desc_kirakat   = $xml_kirakat ->createElement("describe");
            $desc_text = $xml_kirakat ->createCDATASection($product['description']);
            $desc_kirakat->appendChild($desc_text);

            $category_kirakat   = $xml_kirakat ->createElement("category");
            $category_text = $xml_kirakat ->createCDATASection($product['category']['category_names']);
            $category_kirakat->appendChild($category_text);

            $book1 = $xml_kirakat ->createElement("product");
            $book1->appendChild($id_kirakat);
            $book1->appendChild($manufacturer_kirakat);


            if (!empty($products['isbn']) OR !empty($products['ean'])){

                $barcodes = $xml_kirakat ->createElement("barcodes");

                if(!empty($products['isbn'])){
                    $isbn_kirakat   = $xml_kirakat ->createElement("barcode");
                    $isbnText = $xml_kirakat ->createCDATASection($products['isbn']);
                    $isbn_kirakat->appendChild($isbnText);
                    $isbn_kirakat -> setAttribute('type', 'isbn');
                    $barcodes->appendChild($isbn_kirakat);
                }

                if(!empty($products['ean'])){
                    $ean_kirakat   = $xml_kirakat ->createElement("barcode");
                    $eanText = $xml_kirakat ->createCDATASection($products['ean']);
                    $ean_kirakat->appendChild($eanText);
                    $ean_kirakat -> setAttribute('type', 'ean-13');
                    $barcodes->appendChild($ean_kirakat);
                }

                $book1->appendChild($barcodes);
            }

            $book1->appendChild($name_kirakat);
            $book1->appendChild($netprice_kirakat);
            $book1->appendChild($grossprice_kirakat);
            $book1->appendChild($grosspricecarriage_kirakat);
            $book1->appendChild($deliveryprice_kirakat);
            $book1->appendChild($deliverytime_kirakat);
            $book1->appendChild($pickpackpoint_kirakat);
            $book1->appendChild($warranty_kirakat);
            $book1->appendChild($stock_kirakat);
            $book1->appendChild($itemid_kirakat);
            $book1->appendChild($url_kirakat);
            $book1->appendChild($picture_kirakat);
            $book1->appendChild($desc_kirakat);
            $book1->appendChild($category_kirakat);

            $root_kirakat->appendChild($book1);

        }

        $xml_kirakat ->formatOutput = true;
        $xml_kirakat ->save(DIR_ARUHAZ."/csv/arukeresok/".$filename.".xml") or die("HIBA AZ Olcsóbbat.hu/Kirakat.hu XML FILE ÍRÁSA SORÁN!!!");

    }

    private function olcso() {
        $xml_olcso = new DOMDocument("1.0","UTF-8");
        $root_olcso = $xml_olcso ->createElement("products");
        $xml_olcso ->appendChild($root_olcso);

        foreach($this->products as $product) {
            $id_olcso = $xml_olcso->createElement("id");
            $id_Text = $xml_olcso->createTextNode($product['model']);
            $id_olcso->appendChild($id_Text);

            $name_olcso = $xml_olcso->createElement("name");
            $name_Text = $xml_olcso->createTextNode($product['name']);
            $name_olcso->appendChild($name_Text);

            $price_olcso = $xml_olcso->createElement("price");
            $price_Text = $xml_olcso->createTextNode(!empty($product['special']) ? $product['special'] : $product['price']);
            $price_olcso->appendChild($price_Text);


            $url_olcso = $xml_olcso->createElement("url");
            $url_Text = $xml_olcso->createTextNode($product['href']);
            $url_olcso->appendChild($url_Text);

            $description_olcso = $xml_olcso->createElement("description");
            $description_Text = $xml_olcso->createTextNode($product['description']);
            $description_olcso->appendChild($description_Text);

            $picture_olcso = $xml_olcso->createElement("picture");
            $picture_Text = $xml_olcso->createTextNode($product['thumb_product']);
            $picture_olcso->appendChild($picture_Text);

            $category_olcso = $xml_olcso->createElement("category");
            $category_Text = $xml_olcso->createTextNode($product['category']['category_names']);
            $category_olcso->appendChild($category_Text);

            $manufacturer_olcso = $xml_olcso->createElement("manufacturer");
            $manufacturer_Text = $xml_olcso->createTextNode($product['manufacturers']['name']);
            $manufacturer_olcso->appendChild($manufacturer_Text);




            $originalPrice_olcso = $xml_olcso->createElement("originalPrice");
            $originalPrice_Text = $xml_olcso->createTextNode($product['price']);
            $originalPrice_olcso->appendChild($originalPrice_Text);

            $deliveryTime_olcso = $xml_olcso->createElement("deliveryTime");
            $deliveryTime_Text = $xml_olcso->createTextNode($product['delivery_time']);
            $deliveryTime_olcso->appendChild($deliveryTime_Text);

            $deliveryPrice_olcso = $xml_olcso->createElement("deliveryPrice");
            $deliveryPrice_Text = $xml_olcso->createTextNode($product['shipping_price']);
            $deliveryPrice_olcso->appendChild($deliveryPrice_Text);

            $gar_olcso = $xml_olcso->createElement("gar");
            $garPrice_Text = $xml_olcso->createTextNode($product['garancia_ertek']);
            $gar_olcso->appendChild($garPrice_Text);

            $book3 = $xml_olcso->createElement("product");
            $book3->appendChild($id_olcso);
            $book3->appendChild($name_olcso);
            $book3->appendChild($price_olcso);
            $book3->appendChild($url_olcso);
            $book3->appendChild($description_olcso);
            $book3->appendChild($picture_olcso);
            $book3->appendChild($category_olcso);
            $book3->appendChild($manufacturer_olcso);
            $book3->appendChild($originalPrice_olcso);
            $book3->appendChild($deliveryTime_olcso);
            $book3->appendChild($deliveryPrice_olcso);
            $book3->appendChild($gar_olcso);

            $root_olcso->appendChild($book3);
        }
        $xml_olcso ->formatOutput = true;
        $xml_olcso ->save(DIR_ARUHAZ."/csv/arukeresok/olcso.xml") or die("HIBA AZ Olcso XML FILE ÍRÁSA SORÁN!!!");

    }

    private function joaron() {
        $filename = DIR_ARUHAZ.'/csv/arukeresok/joaron.csv';

        if (!$handle = fopen($filename, 'w')) {
            echo "Cannot open file ($filename)";
            exit;
        }
        $list[] = array('id','manufacturer', 'name', 'category', 'description', 'price' , 'product_url', 'image_url', 'delivery_cost', 'delivery_time');

        foreach($this->products as $product) {

            $gyarto = mb_convert_encoding($product['manufacturers']['name'], "ISO-8859-2", "UTF-8");
            $nev = mb_convert_encoding($product['name'], "ISO-8859-2", "UTF-8");
            $ketegoria = mb_convert_encoding($product['category']['category_names_joaron'], "ISO-8859-2", "UTF-8");
            $leiras = mb_convert_encoding($product['description'], "ISO-8859-2", "UTF-8");
            $ara = !empty($product['special']) ? $product['special'] : $product['price'];
            $termeklink = mb_convert_encoding($product['href'], "ISO-8859-2", "UTF-8");
            $keplink = mb_convert_encoding($product['thumb_product'], "ISO-8859-2", "UTF-8");
            $szallitasi_dij = $product['shipping_price'];
            $szallitasi_ido = $product['delivery_time'];

            $list[] = array($product['product_id'],$gyarto, $nev, $ketegoria, $leiras, $ara, $termeklink, $keplink, $szallitasi_dij, $szallitasi_ido);

        }
        foreach ($list as $fields) {
            fputcsv($handle, $fields, ";");
        }

        fclose($handle);
    }

}
?>