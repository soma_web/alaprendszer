<?php
// Heading
$_['heading_title']    = 'Bevásárló kosár';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Sikeres módosítás!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

// Entry
$_['entry_ajax']       = 'AJAX kosárba helyezés:';
$_['entry_position']   = 'Pozició:';
$_['entry_layout']        = 'Kiosztás:';
$_['entry_status']     = 'Állapot:';
$_['entry_sort_order'] = 'Sorrend:';

// Error
$_['error_permission'] = 'Nincs módosítási engedélye!';
?>