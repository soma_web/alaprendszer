<?php if (isset($termekadatok['megjelenit_product_meret']) && !empty($termekadatok['megjelenit_product_meret'])) { ?>
    <div class="termekadatok termekadatok-meret" >
        <span class="termekadatok-meret-szoveg"><?php echo $termekadatok['megjelenit_product_meret']['text_name']; ?></span>
        <span class="termekadatok-meret-ertek"><?php echo $termekadatok['megjelenit_product_meret']['value']; ?></span>
    </div>
<?php } ?>