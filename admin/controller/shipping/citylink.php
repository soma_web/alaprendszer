<?php
class ControllerShippingCitylink extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('shipping/citylink');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!isset($this->request->post['gls_csomagpont_cl'])){
                $this->request->post['gls_csomagpont_cl'] = 0;
            } else {
                $this->request->post['gls_csomagpont_cl'] = 1;
            }

			$this->model_setting_setting->editSetting('citylink', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_gls_csomagpont']  = $this->language->get('text_gls_csomagpont');

        $this->data['entry_title_heading'] = $this->language->get('entry_title_heading');
        $this->data['entry_title_text'] = $this->language->get('entry_title_text');

		$this->data['entry_rate'] = $this->language->get('entry_rate');
		$this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_mail'] = $this->language->get('entry_mail');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/citylink', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('shipping/citylink', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['megjelenit_gls'] = $this->config->get('megjelenit_gls');
		
		if (isset($this->request->post['citylink_rate'])) {
			$this->data['citylink_rate'] = $this->request->post['citylink_rate'];
		} elseif ($this->config->get('citylink_rate')) {
			$this->data['citylink_rate'] = $this->config->get('citylink_rate');
		} else {
			$this->data['citylink_rate'] = '10:11.6,15:14.1,20:16.60,25:19.1,30:21.6,35:24.1,40:26.6,45:29.1,50:31.6,55:34.1,60:36.6,65:39.1,70:41.6,75:44.1,80:46.6,100:56.6,125:69.1,150:81.6,200:106.6';	
		}

		if (isset($this->request->post['citylink_tax_class_id'])) {
			$this->data['citylink_tax_class_id'] = $this->request->post['citylink_tax_class_id'];
		} else {
			$this->data['citylink_tax_class_id'] = $this->config->get('citylink_tax_class_id');
		}

		if (isset($this->request->post['citylink_mail'])) {
			$this->data['citylink_mail'] = $this->request->post['citylink_mail'];
		} else {
			$this->data['citylink_mail'] = $this->config->get('citylink_mail');
		}

        if (isset($this->request->post['gls_csomagpont_cl'])) {
            $this->data['gls_csomagpont_cl'] = $this->request->post['gls_csomagpont_cl'];
        } else {
            $this->data['gls_csomagpont_cl'] = $this->config->get('gls_csomagpont_cl');
        }


		if (isset($this->request->post['citylink_geo_zone_id'])) {
			$this->data['citylink_geo_zone_id'] = $this->request->post['citylink_geo_zone_id'];
		} else {
			$this->data['citylink_geo_zone_id'] = $this->config->get('citylink_geo_zone_id');
		}
		
		if (isset($this->request->post['citylink_status'])) {
			$this->data['citylink_status'] = $this->request->post['citylink_status'];
		} else {
			$this->data['citylink_status'] = $this->config->get('citylink_status');
		}
		
		if (isset($this->request->post['citylink_sort_order'])) {
			$this->data['citylink_sort_order'] = $this->request->post['citylink_sort_order'];
		} else {
			$this->data['citylink_sort_order'] = $this->config->get('citylink_sort_order');
		}

        if (isset($this->request->post['citylink_header'])) {
            $this->data['citylink_header'] = $this->request->post['citylink_header'];
        } else {
            $this->data['citylink_header'] = $this->config->get('citylink_header');
        }
        if (isset($this->request->post['citylink_text'])) {
            $this->data['citylink_text'] = $this->request->post['citylink_text'];
        } else {
            $this->data['citylink_text'] = $this->config->get('citylink_text');
        }

		$this->load->model('localisation/tax_class');
		
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		$this->load->model('localisation/geo_zone');
		
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();


        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getInstalled('payment');
        $no_module = $this->model_setting_extension->getNoModule();


        foreach ($extensions as $key => $value) {
            if (!file_exists(DIR_APPLICATION . 'controller/payment/' . $value . '.php')) {
                $this->model_setting_extension->uninstall('payment', $value);

                unset($extensions[$key]);
            } elseif (in_array($value, $no_module)){
                unset($extensions[$key]);
            } else {
                $this->load->language('payment/' . $value);

                if (isset($this->request->post['citylink_payment_'.$value])) {
                    $this->data['citylink_payment_'.$value] = $this->request->post['citylink_payment_'.$value];
                } elseif ( $this->config->get('citylink_payment_'.$value)) {
                    $this->data['citylink_payment_'.$value] = $this->config->get('citylink_payment_'.$value);
                } else {
                    $this->data['citylink_payment_'.$value] = 0;
                }

                $this->data['payment']['payment_' . $value] = array(
                    'name'      => $this->language->get('heading_title'),
                    'valasztva' => $this->data['citylink_payment_'.$value]
                );


            }
        }


		$this->template = 'shipping/citylink.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/citylink')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>