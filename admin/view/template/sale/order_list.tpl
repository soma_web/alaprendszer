<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <?php if((isset($megjelenit_rendeles['csv_letolt']) &&  $megjelenit_rendeles['csv_letolt'] == 1 && !empty($megjelenit_rendeles['csv_letolt_csoportnev'])) || (isset($megjelenit_rendeles['csv_gls_pont']) && $megjelenit_rendeles['csv_gls_pont'] == 1 && !empty($megjelenit_rendeles['csv_gls_pont_csoportnev']))) {?>
              <a onclick="csvSzures()" class="button" style="position: relative;"><?php echo $button_csv_export; ?></a>
              <span id="csv_beallitas" style="display: none; position: absolute; border-radius: 4px 4px 6px 6px; background: #efefef; border: 1px solid #bbbbbb; padding: 20px">
                  <?php if((isset($megjelenit_rendeles['csv_letolt']) &&  $megjelenit_rendeles['csv_letolt'] == 1 && !empty($megjelenit_rendeles['csv_letolt_csoportnev'])) && (isset($megjelenit_rendeles['csv_gls_pont']) && $megjelenit_rendeles['csv_gls_pont'] == 1 && !empty($megjelenit_rendeles['csv_gls_pont_csoportnev']))) {?>
                      <select id="csv_export_select">
                          <option name="empty" value="empty" ><?php echo $text_choose; ?></option>
                          <option name="gls" value="gls" ><?php echo $text_gls_pont; ?></option>
                          <option name="tranzo" value="tranzo" ><?php echo $text_tranzoflex; ?></option>
                      </select>
                  <?php } ?>
                  <table id="csv_export_table" class="<?php if(isset($megjelenit_rendeles['csv_letolt']) && $megjelenit_rendeles['csv_letolt'] == 1) { echo "tranzo";} else if (isset($megjelenit_rendeles['csv_gls_pont']) && $megjelenit_rendeles['csv_gls_pont']) { echo 'gls'; }?>" >
                      <tr>
                          <td style="min-width: 140px; display: table"><?php echo $text_rendeles_szam;?></td>
                          <td>
                              <input type="text"    name="data[]" class = "datarendelestol" value="" size="4">
                          </td>
                          <td>
                              <input type="text"    name="data[]" class = "datarendelesig" value="" size="4">

                          </td>
                      </tr>

                      <tr>
                          <td><?php echo $text_rendeles_datum;?></td>
                          <td>
                              <input type="text"    name="data[]" class = "datadatumtol date" value="" size="8">
                          </td>
                          <td>
                              <input type="text"    name="data[]" class = "datadatumig date" value="" size="8">
                          </td>
                      </tr>
                      <tr>
                          <td colspan="3" style="text-align: right; padding-top: 20px">
                              <a onclick="csvIndit()" class="button" style="position: relative;"><?php echo $text_letoltes; ?></a>
                          </td>
                      </tr>
                  </table>

              </span>
          <?php } ?>
          <a onclick="$('#form').attr('action', '<?php echo $invoice; ?>'); $('#form').attr('target', '_blank'); $('#form').submit();" class="button"><?php echo $button_invoice; ?></a>
          <a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
          <a onclick="$('#form').attr('action', '<?php echo $delete; ?>'); $('#form').attr('target', '_self'); $('#form').submit();" class="button"><?php echo $button_delete; ?></a>
      </div>
    </div>
    <div class="content">
      <form action="" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="right"><?php if ($sort == 'o.order_id') { ?>
                <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_order; ?>"><?php echo $column_order_id; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'customer') { ?>
                <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'status') { ?>
                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                <?php } ?></td>
              <td class="right"><?php if ($sort == 'o.total') { ?>
                <a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'o.date_added') { ?>
                <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'o.date_modified') { ?>
                <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                <?php } ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td></td>
              <td align="right"><input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" size="4" style="text-align: right;" /></td>
              <td><input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" /></td>
              <td><select name="filter_order_status_id">
                  <option value="*"></option>
                  <?php if ($filter_order_status_id == '0') { ?>
                  <option value="0" selected="selected"><?php echo $text_missing; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_missing; ?></option>
                  <?php } ?>
                  <?php foreach ($order_statuses as $order_status) { ?>
                  <?php if ($order_status['order_status_id'] == $filter_order_status_id) { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
              <td align="right"><input type="text" name="filter_total" value="<?php echo $filter_total; ?>" size="4" style="text-align: right;" /></td>
              <td><input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" class="date" /></td>
              <td><input type="text" name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" size="12" class="date" /></td>
              <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
            </tr>
            <?php if ($orders) { ?>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($order['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" />
                <?php } ?></td>
              <td class="right"><?php echo $order['order_id']; ?></td>
              <td class="left"><?php echo $order['customer']; ?></td>

              <td class="left">

                <span class="list_ertek" onclick="listElemValueModify(this)"><?php echo $order['status']; ?></span>

                <span class="list_modify" style="display: none">
                <select name="order_status_id" modositas="<?php echo $order['status_event']?>" onchange="$(this).blur();">
                    <?php foreach ($order_statuses as $order_status) { ?>
                        <?php if ($order_status['order_status_id'] == $order['status_value']) { ?>
                            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>

              </td>
              <td class="right"><?php echo $order['total']; ?></td>
              <td class="left"><?php echo $order['date_added']; ?></td>
              <td class="left"><?php echo $order['date_modified']; ?></td>
              <td class="right"><?php foreach ($order['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>


      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--

    var csv_export_select = "";
    $('#csv_export_select').on('change', function() {
        csv_export_select = this.value;

    })

    function csvSzures() {
        if ($('#csv_beallitas').css("display") == "none") {
            $('#csv_beallitas').slideDown(500);
        } else {
            $('#csv_beallitas').slideUp(500);
        }
    }

    function csvIndit() {
        if(csv_export_select == '') {
            csv_export_select = $('#csv_export_select').val();
        }
        if(csv_export_select != 'empty') {
            url = 'index.php?route=tool/csv_export/csvFeltetel&token=<?php echo $token; ?>';

            var datarendelestol = $('.datarendelestol').val();
            var datarendelesig  = $('.datarendelesig').val()
            var datadatumtol    = $('.datadatumtol').val();
            var datadatumig     = $('.datadatumig').val();

            var feltetel = "";
            var szuro = "";
            var van = false;

            debugger;
            $('#csv_beallitas').slideUp(500);
            if(typeof csv_export_select === 'undefined') {
                if($('#csv_export_table').hasClass('gls')) {
                    csv_export_select = 'gls';
                }
                if($('#csv_export_table').hasClass('tranzo')) {
                    csv_export_select = 'tranzo';
                }
            }

            if (datarendelestol.length > 0 || datarendelesig.length > 0 || datadatumtol.length > 0 || datadatumig.length > 0) {
                var feltetel = " WHERE ";
                if(datarendelestol.length > 0) {
                    van_rendeles = true;
                    feltetel += "<?php echo DB_PREFIX?>order.order_id >= "+datarendelestol;
                }
                if(datarendelesig.length > 0) {
                    if (van_rendeles) {
                        feltetel += " AND ";
                    }
                    feltetel += "<?php echo DB_PREFIX?>order.order_id <= "+datarendelesig;
                }
                if(datadatumtol.length > 0) {
                    van = true;
                    if(van_rendeles) {
                        feltetel += " AND ";
                    }
                    feltetel += "<?php echo DB_PREFIX?>order.date_added >= '"+datadatumtol+"'";
                }
                if(datadatumig.length > 0) {
                    if (van) {
                        feltetel += " AND ";
                    }
                    feltetel += "<?php echo DB_PREFIX?>order.date_added <= '"+datadatumig+"'";
                }

                if(csv_export_select == 'gls') {
                    feltetel += " AND ";
                    feltetel += " <?php echo DB_PREFIX?>order.shipping_gls_pont_shopid <> ''";
                }
            } else if(csv_export_select == 'gls') {
                feltetel += " WHERE ";
                feltetel += " <?php echo DB_PREFIX?>order.shipping_gls_pont_shopid <> ''";
            }

            debugger;
            if(csv_export_select == 'tranzo') {
                //alert(csv_export_select);
            location = url+'&feltetel='+feltetel+'&export_csoport=<?php echo $megjelenit_rendeles['csv_letolt_csoportnev']?>';
            }

            if(csv_export_select == 'gls') {
               // alert(csv_export_select);
            location = url+'&feltetel='+feltetel+'&export_csoport=<?php echo $megjelenit_rendeles['csv_gls_pont_csoportnev']?>';
            }

        }
    }

    function filter() {
        url = 'index.php?route=sale/order&token=<?php echo $token; ?>';

        var filter_order_id = $('input[name=\'filter_order_id\']').attr('value');

        if (filter_order_id) {
            url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
        }

        var filter_customer = $('input[name=\'filter_customer\']').attr('value');

        if (filter_customer) {
            url += '&filter_customer=' + encodeURIComponent(filter_customer);
        }

        var filter_order_status_id = $('select[name=\'filter_order_status_id\']').attr('value');

        if (filter_order_status_id != '*') {
            url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
        }

        var filter_total = $('input[name=\'filter_total\']').attr('value');

        if (filter_total) {
            url += '&filter_total=' + encodeURIComponent(filter_total);
        }

        var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');

        if (filter_date_added) {
            url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
        }

        var filter_date_modified = $('input[name=\'filter_date_modified\']').attr('value');

        if (filter_date_modified) {
            url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
        }

        location = url;
    }
//--></script>  
<script type="text/javascript"><!--
    $(document).ready(function() {
        $('.date').datepicker({dateFormat: 'yy-mm-dd'});
    });
//--></script> 
<script type="text/javascript"><!--
    $('#form input').keydown(function(e) {
        if (e.keyCode == 13) {
            filter();
        }
    });
//--></script> 
<script type="text/javascript"><!--
    $.widget('custom.catcomplete', $.ui.autocomplete, {
        _renderMenu: function(ul, items) {
            var self = this, currentCategory = '';

            $.each(items, function(index, item) {
                if (item.category != currentCategory) {
                    ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');

                    currentCategory = item.category;
                }

                self._renderItem(ul, item);
            });
        }
    });

    $('input[name=\'filter_customer\']').catcomplete({
        delay: 0,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            category: item.customer_group,
                            label: item.name,
                            value: item.customer_id
                        }
                    }));
                }
            });
        },
        select: function(event, ui) {
            $('input[name=\'filter_customer\']').val(ui.item.label);

            return false;
        }
    });
//--></script>
<?php require_once('view/javascript/common.php')?>

<?php echo $footer; ?>