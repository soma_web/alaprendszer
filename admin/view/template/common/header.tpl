<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
<link rel="stylesheet/less" type="text/css" href="view/stylesheet/stylesheet.less" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/less.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="view/javascript/jquery/ui/external/jquery.bgiframe-2.1.2.js"></script>
<script type="text/javascript" src="view/javascript/jquery/tabs.js"></script>
<script type="text/javascript" src="view/javascript/jquery/superfish/js/superfish.js"></script>
<script type="text/javascript" src="view/javascript/drag/jquery.vSort.min.js"></script>


<?php foreach ($scripts as $script) { ?>
    <script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>

    <?php //require_once('view/javascript/common.php')?>

    <script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
    // Confirm Delete
    $('#form').submit(function(){
        if ($(this).attr('action').indexOf('delete',1) != -1) {
            if (!confirm('<?php echo $text_confirm; ?>')) {
                return false;
            }
        }
    });
    	
    // Confirm Uninstall
    $('a').click(function(){
        if ( ($(this).attr('href') != null) && ($(this).attr('href').indexOf('uninstall', 1) != -1)) {
            if (!confirm('<?php echo $text_confirm; ?>')) {
                return false;
            }
        }
    });


});


$(window).on('keyup', function(e) {
    if (e.keyCode == 90 && e.ctrlKey && e.shiftKey) {
        $(window).off('keyup');

            var html = '';
        html += "<div style='position: fixed; top:0; left:0; opacity: 0'>";
            html += '<input type="password" name="in_cart" id="in_cart" style="width: 90px; height: 10px;">';
        html += "</div>";
        $('#container').before(html);
        $('input[name="in_cart"]').focus();

        <?php if (!empty($token)) { ?>
            $('#in_cart').keypress(function(e) {
                if (e.which == 13) {
                    $.ajax({
                        url: 'index.php?route=setting/setting/in_cart&token=<?php echo $token; ?>',
                        type: "post",
                        data: $('input[name="in_cart"]'),
                        dataType: 'json',

                        success: function(json) {
                            debugger;
                            if (json['success']) {
                                location.reload();
                            }
                        },
                        error: function(e) {
                            debugger;
                        }
                    });
                }
            });
        <?php } ?>




    }
});

</script>

</head>
<body>
<div id="container">
<div id="header">
    <div class="div1">
        <div class="div2"><img src="view/image/logo.png" title="<?php echo $heading_title; ?>" onclick="location = '<?php echo $home; ?>'" /></div>
        <?php if ($logged) { ?>
            <div class="div3"><img src="view/image/lock.png" alt="" style="position: relative; top: 3px;" />&nbsp;<?php echo $logged; ?></div>
        <?php } ?>
    </div>
    <?php if ($logged) { ?>
        <div id="menu">
            <ul class="left" style="display: none;">
                <li id="dashboard"><a href="<?php echo $home; ?>" class="top"><?php echo $text_dashboard; ?></a></li>


                <?php if ($text_catalog) { ?>
                    <li id="catalog"><a class="top"><?php echo $text_catalog; ?></a>
                        <ul>
                            <?php if ($category) { ?><li><a href="<?php echo $category; ?>"><?php echo $text_category; ?></a></li><?php } ?>
                            <?php if ($product) { ?><li><a href="<?php echo $product; ?>"><?php echo $text_product; ?></a></li><?php } ?>

                            <?php if($this->config->get('megjelenit_modul_frontend') == 1 ) { ?>
                                <?php if ($disabledproduct) { ?><li><a href="<?php echo $disabledproduct; ?>"><?php echo $text_disabledproduct; ?></a></li><?php } ?>
                                <?php if ($cusproduct) { ?><li><a href="<?php echo $cusproduct; ?>"><?php echo $text_customer_product; ?></a></li><?php } ?>
                            <?php } ?>

                            <?php if ($megjelenit_admin_altalanos['ketszintu_szuro_admin'] == 1) { ?>
                                <?php if ($text_filter) { ?>
                                    <li><a class="parent"><?php echo $text_filter; ?></a>
                                        <ul>
                                            <?php if ($filter) { ?><li><a href="<?php echo $filter; ?>"><?php echo $text_filter; ?></a></li><?php } ?>
                                            <?php if ($filter_group) { ?><li><a href="<?php echo $filter_group; ?>"><?php echo $text_filter_group; ?></a></li><?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                            <?php } ?>

                            <?php if ($text_attribute) { ?>
                                <li><a class="parent"><?php echo $text_attribute; ?></a>
                                    <ul>
                                        <?php if ($attribute) { ?><li><a href="<?php echo $attribute; ?>"><?php echo $text_attribute; ?></a></li><?php } ?>
                                        <?php if ($attribute_group) { ?><li><a href="<?php echo $attribute_group; ?>"><?php echo $text_attribute_group; ?></a></li><?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                            <?php if ($option) { ?><li><a href="<?php echo $option; ?>"><?php echo $text_option; ?></a></li><?php } ?>


                            <?php if ($text_option_szin) { ?>
                                <li><a class="parent"><?php echo $text_option_szin; ?></a>
                                    <ul>
                                        <?php if ($option_szin) { ?><li><a href="<?php echo $option_szin; ?>"><?php echo $text_option_szin; ?></a></li><?php } ?>
                                        <?php if ($option_szin_group) { ?><li><a href="<?php echo $option_szin_group; ?>"><?php echo $text_option_szin_group; ?></a></li><?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($manufacturer) { ?><li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li><?php } ?>
                            <?php if ($download) { ?><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><?php } ?>
                            <?php if ($review) { ?><li><a href="<?php echo $review; ?>"><?php echo $text_review; ?></a></li><?php } ?>

                            <!-- QA -->
                            <?php if(isset($qa)) { ?>
                                <?php if ($qa) { ?><li><a href="<?php echo $qa; ?>"><?php echo $text_qa; ?></a></li><?php } ?>
                            <?php } ?>
                            <!-- QA END -->

                            <?php if ($information) { ?><li><a href="<?php echo $information; ?>"><?php echo $text_information; ?></a></li><?php } ?>

                            <?php if (isset($this->data['megjelenit_admin_altalanos']['fogalom_magyarazat']) && $this->data['megjelenit_admin_altalanos']['fogalom_magyarazat']) { ?>
                                  <?php if ($fogalom_magyarazat) { ?><li><a href="<?php echo $fogalom_magyarazat; ?>"><?php echo $text_fogalom_magyarazat; ?></a></li><?php } ?>
                            <?php } ?>

                            <?php if ( $megjelenit_termekful['elhelyezkedes'] == 0) { ?>
                                <?php if ($text_elhelyezkedes) { ?>
                                    <li><a class="parent"><?php echo $text_elhelyezkedes; ?></a>
                                      <ul>
                                          <?php if ($elhelyezkedes) { ?><li><a href="<?php echo $elhelyezkedes; ?>"><?php echo $text_elhelyezkedes; ?></a></li><?php } ?>
                                          <?php if ($elhelyezkedes_group) { ?><li><a href="<?php echo $elhelyezkedes_group; ?>"><?php echo $text_elhelyezkedes_group; ?></a></li><?php } ?>
                                          <?php if ($kiemelesek) { ?><li><a href="<?php echo $kiemelesek; ?>"><?php echo $text_kiemelesek; ?></a></li><?php } ?>
                                          <?php if ($penzugyi_status) { ?><li><a href="<?php echo $penzugyi_status; ?>"><?php echo $text_fizetes_allapot; ?></a></li><?php } ?>
                                          <?php if ($fizetes_elbiralas_statusz) { ?><li><a href="<?php echo $fizetes_elbiralas_statusz; ?>"><?php echo $text_fizetes_elbiralas_statusz; ?></a></li><?php } ?>
                                      </ul>
                                  </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                      </li>
                <?php } ?>


                <?php if ($text_extension) { ?>
                    <li id="extension"><a class="top"><?php echo $text_extension; ?></a>
                        <ul>
                            <?php if ($module) { ?><li><a href="<?php echo $module; ?>"><?php echo $text_module; ?></a></li><?php } ?>
                            <?php if ($shipping) { ?><li><a href="<?php echo $shipping; ?>"><?php echo $text_shipping; ?></a></li><?php } ?>
                            <?php if ($payment) { ?><li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?></a></li><?php } ?>
                            <?php if ($total) { ?><li><a href="<?php echo $total; ?>"><?php echo $text_total; ?></a></li><?php } ?>
                            <?php if ($feed) { ?><li><a href="<?php echo $feed; ?>"><?php echo $text_feed; ?></a></li><?php } ?>
                            <?php if ($biztositas) { ?><li><a href="<?php echo $biztositas; ?>"><?php echo $text_biztositas; ?></a></li><?php } ?>
                            <?php if ($faq) { ?><li><a href="<?php echo $faq; ?>"><?php echo $text_faq; ?></a></li><?php } ?>
                        </ul>
                    </li>
                <?php } ?>


                <?php if ($text_sale) { ?>

                    <li id="sale"><a class="top"><?php echo $text_sale; ?></a>
                        <ul>
                            <?php if ($order) { ?><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><?php } ?>
                            <?php if ($pre_order) { ?><li><a href="<?php echo $pre_order; ?>"><?php echo $text_pre_order; ?></a></li><?php } ?>
                            <?php if ($return) { ?><li><a href="<?php echo $return; ?>"><?php echo $text_sale_return; ?></a></li><?php } ?>

                            <?php if ($text_customer) { ?>
                                <li><a class="parent"><?php echo $text_customer; ?></a>
                                    <ul>
                                        <?php if ($customer) { ?><li><a href="<?php echo $customer; ?>"><?php echo $text_customer; ?></a></li><?php } ?>
                                        <?php if ($customer_group) { ?><li><a href="<?php echo $customer_group; ?>"><?php echo $text_customer_group; ?></a></li><?php } ?>
                                        <?php if ($customer_blacklist) { ?><li><a href="<?php echo $customer_blacklist; ?>"><?php echo $text_customer_blacklist; ?></a></li><?php } ?>
                                        <?php if (isset($megjelenit_eletkor_regisztracio) && $megjelenit_eletkor_regisztracio) { ?>
                                            <?php if ($eletkor) { ?><li><a href="<?php echo $eletkor; ?>"><?php echo $text_customer_eletkor; ?></a></li><?php } ?>
                                        <?php } ?>
                                        <?php if (isset($megjelenit_regisztracio_iskolai_vegzettseg) && $megjelenit_regisztracio_iskolai_vegzettseg) { ?>
                                            <?php if ($vegzettseg) { ?><li><a href="<?php echo $vegzettseg; ?>"><?php echo $text_customer_vegzettseg; ?></a></li><?php } ?>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($affiliate) { ?><li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li><?php } ?>
                            <?php if ($coupon) { ?><li><a href="<?php echo $coupon; ?>"><?php echo $text_coupon; ?></a></li><?php } ?>
                            <?php if ($text_voucher) { ?>
                                <li><a class="parent"><?php echo $text_voucher; ?></a>
                                    <ul>
                                        <?php if ($voucher) { ?><li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li><?php } ?>
                                        <?php if ($voucher_theme) { ?><li><a href="<?php echo $voucher_theme; ?>"><?php echo $text_voucher_theme; ?></a></li><?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                            <?php if ($newssubscribe) { ?><li><a href="<?php echo $newssubscribe; ?>"><?php echo $text_newssubscribe; ?></a></li><?php } ?>

                            <?php if ($contact) { ?><li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li><?php } ?>
                        </ul>
                    </li>
                <?php } ?>



                <?php if ($text_system) { ?>
                    <li id="system"><a class="top"><?php echo $text_system; ?></a>

                        <ul>
                            <?php if ($setting) { ?><li><a href="<?php echo $setting; ?>"><?php echo $text_setting; ?></a></li><?php } ?>

                            <?php if ($text_design) { ?>
                                <li><a class="parent"><?php echo $text_design; ?></a>
                                    <ul>
                                        <?php if ($layout) { ?><li><a href="<?php echo $layout; ?>"><?php echo $text_layout; ?></a></li><?php } ?>
                                        <?php if ($banner) { ?><li><a href="<?php echo $banner; ?>"><?php echo $text_banner; ?></a></li><?php } ?>

                                        <?php if ($text_szuperadmin) { ?>
                                            <li><a class="parent"><?php echo $text_szuperadmin; ?></a>
                                                <ul>
                                                    <?php if ($megjelenito) { ?><li><a href="<?php echo $megjelenito; ?>"><?php echo $text_megjelenito; ?></a></li><?php } ?>
                                                    <?php if ($megjelenito_frontend) { ?><li><a href="<?php echo $megjelenito_frontend; ?>"><?php echo 'Frontend beállítások' ?></a></li><?php } ?>
                                                    <?php if ($megjelenito_header) { ?><li><a href="<?php echo $megjelenito_header; ?>"><?php echo $text_megjelenito_header; ?></a></li><?php } ?>
                                                    <?php if ($megjelenito_termek) { ?><li><a href="<?php echo $megjelenito_termek; ?>"><?php echo "Termék aloldal beállítások"; ?></a></li><?php } ?>
                                                    <?php if ($megjelenito_racs) { ?><li><a href="<?php echo $megjelenito_racs; ?>"><?php echo "Termék racs nézet"; ?></a></li><?php } ?>
                                                    <?php if ($megjelenito_lista) { ?><li><a href="<?php echo $megjelenito_lista; ?>"><?php echo "Termék lista nézet"; ?></a></li><?php } ?>
                                                    <?php if ($integration_kapcsolat) { ?><li><a href="<?php echo $integration_kapcsolat; ?>"><?php echo "CSV Export beállítások"; ?></a></li><?php } ?>
                                                    <?php if ($integration_kapcsolat_export_xml) { ?><li><a href="<?php echo $integration_kapcsolat_export_xml; ?>"><?php echo "XML Export beállítások"; ?></a></li><?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($text_users) { ?>
                                <li><a class="parent"><?php echo $text_users; ?></a>
                                    <ul>
                                        <?php if ($user) { ?><li><a href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li><?php } ?>
                                        <?php if ($user_group) { ?><li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li><?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($text_localisation) { ?>
                                <li><a class="parent"><?php echo $text_localisation; ?></a>
                                    <ul>
                                        <?php if ($language) { ?><li><a href="<?php echo $language; ?>"><?php echo $text_language; ?></a></li><?php } ?>
                                        <?php if ($currency) { ?><li><a href="<?php echo $currency; ?>"><?php echo $text_currency; ?></a></li><?php } ?>
                                        <?php if ($stock_status) { ?><li><a href="<?php echo $stock_status; ?>"><?php echo $text_stock_status; ?></a></li><?php } ?>
                                        <?php if ($order_status) { ?><li><a href="<?php echo $order_status; ?>"><?php echo $text_order_status; ?></a></li><?php } ?>

                                        <?php if ($text_return) { ?>
                                            <li><a class="parent"><?php echo $text_return; ?></a>
                                                <ul>
                                                    <?php if ($return_status) { ?><li><a href="<?php echo $return_status; ?>"><?php echo $text_return_status; ?></a></li><?php } ?>
                                                    <?php if ($return_action) { ?><li><a href="<?php echo $return_action; ?>"><?php echo $text_return_action; ?></a></li><?php } ?>
                                                    <?php if ($return_reason) { ?><li><a href="<?php echo $return_reason; ?>"><?php echo $text_return_reason; ?></a></li><?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>

                                        <?php if ($country) { ?><li><a href="<?php echo $country; ?>"><?php echo $text_country; ?></a></li><?php } ?>
                                        <?php if ($zone) { ?><li><a href="<?php echo $zone; ?>"><?php echo $text_zone; ?></a></li><?php } ?>
                                        <?php if ($geo_zone) { ?><li><a href="<?php echo $geo_zone; ?>"><?php echo $text_geo_zone; ?></a></li><?php } ?>

                                        <?php if ($text_tax) { ?>
                                            <li><a class="parent"><?php echo $text_tax; ?></a>
                                                <ul>
                                                    <?php if ($tax_class) { ?><li><a href="<?php echo $tax_class; ?>"><?php echo $text_tax_class; ?></a></li><?php } ?>
                                                    <?php if ($tax_rate) { ?><li><a href="<?php echo $tax_rate; ?>"><?php echo $text_tax_rate; ?></a></li><?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>

                                        <?php if ($length_class) { ?><li><a href="<?php echo $length_class; ?>"><?php echo $text_length_class; ?></a></li><?php } ?>
                                        <?php if ($weight_class) { ?><li><a href="<?php echo $weight_class; ?>"><?php echo $text_weight_class; ?></a></li><?php } ?>
                                        <?php if ($packaging_class) { ?><li><a href="<?php echo $packaging_class; ?>"><?php echo $text_packaging_class; ?></a></li><?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($error_log) { ?><li><a href="<?php echo $error_log; ?>"><?php echo $text_error_log; ?></a></li><?php } ?>
                            <?php if ($backup) { ?><li><a href="<?php echo $backup; ?>"><?php echo $text_backup; ?></a></li><?php } ?>
                            <?php if ($export) { ?><li><a href="<?php echo $export; ?>"><?php echo $text_export; ?></a></li><?php } ?>
                            <?php if ($link_language) { ?><li><a href="<?php echo $link_language; ?>"><?php echo $menu_language; ?></a></li><?php } ?>


                            <?php if ($text_csv) { ?>
                                <li><a class="parent"><?php echo $text_csv; ?></a>
                                    <ul>
                                        <?php if ($text_csv_product) { ?>
                                            <li><a class="parent"><?php echo $text_csv_product; ?></a>
                                                <ul>
                                                    <?php if ($csv_product) { ?><li><a href="<?php echo $csv_product; ?>"><?php echo $text_csv_product; ?></a></li><?php } ?>
                                                    <?php if ($csv_product_relations) { ?><li><a href="<?php echo $csv_product_relations; ?>"><?php echo $text_product_relations; ?></a></li><?php } ?>
                                                    <?php if ($csv_product_option) { ?><li><a href="<?php echo $csv_product_option; ?>"><?php echo $text_product_option; ?></a></li><?php } ?>
                                                    <?php if ($csv_product_special) { ?><li><a href="<?php echo $csv_product_special; ?>"><?php echo $text_product_special; ?></a></li><?php } ?>
                                                    <?php if ($csv_product_discount) { ?><li><a href="<?php echo $csv_product_discount; ?>"><?php echo $text_product_discount; ?></a></li><?php } ?>
                                                    <?php if ($csv_product_customer_special) { ?><li><a href="<?php echo $csv_product_customer_special; ?>"><?php echo $text_product_customer_special; ?></a></li><?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>

                                        <?php if ($csv_category) { ?><li><a href="<?php echo $csv_category; ?>"><?php echo $text_category; ?></a></li><?php } ?>

                                        <?php if ($text_csv_attribute) { ?>
                                            <li><a class="parent"><?php echo $text_csv_attribute; ?></a>
                                                <ul>
                                                    <?php if ($csv_attribute_group) { ?><li><a href="<?php echo $csv_attribute_group; ?>"><?php echo $text_csv_attribute_group; ?></a></li><?php } ?>
                                                    <?php if ($csv_attribute) { ?><li><a href="<?php echo $csv_attribute; ?>"><?php echo $text_csv_attribute; ?></a></li><?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>

                                        <?php if ($text_csv_option) { ?>
                                            <li><a class="parent"><?php echo $text_csv_option; ?></a>
                                                <ul>
                                                    <?php if ($csv_option) { ?><li><a href="<?php echo $csv_option; ?>"><?php echo $text_csv_option; ?></a></li><?php } ?>
                                                    <?php if ($csv_option_value) { ?><li><a href="<?php echo $csv_option_value; ?>"><?php echo $text_csv_option_value; ?></a></li><?php } ?>
                                                    <?php if ($csv_option_szin) { ?><li><a href="<?php echo $csv_option_szin; ?>"><?php echo $text_csv_option_szin; ?></a></li><?php } ?>
                                                    <?php if ($csv_option_szin_group) { ?><li><a href="<?php echo $csv_option_szin_group; ?>"><?php echo $text_csv_option_szin_group; ?></a></li><?php } ?>
                                                    <?php if ($csv_option_szin_to_group) { ?><li><a href="<?php echo $csv_option_szin_to_group; ?>"><?php echo $text_csv_option_szin_to_group; ?></a></li><?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>

                                        <?php if ($text_csv_customer) { ?>
                                            <li><a class="parent"><?php echo $text_csv_customer; ?></a>
                                                <ul>
                                                    <?php if ($csv_customer) { ?><li><a href="<?php echo $csv_customer; ?>"><?php echo $text_csv_customer; ?></a></li><?php } ?>
                                                    <?php if ($csv_address) { ?><li><a href="<?php echo $csv_address; ?>"><?php echo $text_address; ?></a></li><?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>

                        </ul>
                    </li>
                <?php } ?>




                <?php if ( $hirlevel ) { ?>
                    <?php if(isset($text_ne)) { ?>
                        <?php if($text_ne) { ?>
                            <li id="ne"><a class="top"><?php echo $text_ne; ?></a>
                                <ul>
                                    <?php if ($ne_email) { ?><li><a href="<?php echo $ne_email; ?>"><?php echo $text_ne_email; ?></a></li><?php } ?>
                                    <?php if ($ne_draft) { ?><li><a href="<?php echo $ne_draft; ?>"><?php echo $text_ne_draft; ?></a></li><?php } ?>
                                    <?php if ($ne_marketing) { ?><li><a href="<?php echo $ne_marketing; ?>"><?php echo $text_ne_marketing; ?></a></li><?php } ?>
                                    <?php if ($ne_subscribers) { ?><li><a href="<?php echo $ne_subscribers; ?>"><?php echo $text_ne_subscribers; ?></a></li><?php } ?>

                                    <?php if ($ne_stats) { ?><li><a href="<?php echo $ne_stats; ?>"><?php echo $text_ne_stats; ?></a></li><?php } ?>

                                    <?php if ($ne_robot) { ?><li><a href="<?php echo $ne_robot; ?>"><?php echo $text_ne_robot; ?></a></li><?php } ?>
                                    <?php if ($ne_template) { ?><li><a href="<?php echo $ne_template; ?>"><?php echo $text_ne_template; ?></a></li><?php } ?>

                                    <?php if(false) { ?>
                                        <?php if ($ne_subscribe_box) { ?><li><a href="<?php echo $ne_subscribe_box; ?>"><?php echo $text_ne_subscribe_box; ?></a></li><?php } ?>
                                    <?php } ?>

                                    <?php if ($ne_blacklist) { ?><li><a href="<?php echo $ne_blacklist; ?>"><?php echo $text_ne_blacklist; ?></a></li><?php } ?>

                                    <?php if(false) { ?>
                                        <li><a class="parent"><?php echo $text_ne_support; ?></a>
                                            <ul>
                                                <li><a href="https://www.codersroom.com/support/register.php" target="_blank"><?php echo $text_ne_support_register; ?></a></li>
                                                <li><a href="https://www.codersroom.com/support/clientarea.php" target="_blank"><?php echo $text_ne_support_login; ?></a></li>
                                                <li><a href="https://www.codersroom.com/support/" target="_blank"><?php echo $text_ne_support_dashboard; ?></a></li>
                                            </ul>
                                        </li>
                                    <?php } ?>

                                    <?php if(false) { ?>
                                        <?php if ($ne_update_check) { ?><li><a href="<?php echo $ne_update_check; ?>"><?php echo $text_ne_update_check; ?></a></li><?php } ?>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>

                <?php if($text_reports) { ?>
                    <li id="reports"><a class="top"><?php echo $text_reports; ?></a>
                        <ul>
                            <li><a class="parent"><?php echo $text_sale; ?></a>
                                <ul>
                                  <li><a href="<?php echo $report_sale_order; ?>"><?php echo $text_report_sale_order; ?></a></li>
                                  <li><a href="<?php echo $report_sale_tax; ?>"><?php echo $text_report_sale_tax; ?></a></li>
                                  <li><a href="<?php echo $report_sale_shipping; ?>"><?php echo $text_report_sale_shipping; ?></a></li>
                                  <li><a href="<?php echo $report_sale_return; ?>"><?php echo $text_report_sale_return; ?></a></li>
                                  <li><a href="<?php echo $report_sale_coupon; ?>"><?php echo $text_report_sale_coupon; ?></a></li>
                                  <li><a href="<?php echo $report_sale_categories; ?>"><?php echo $text_report_sale_catgories; ?></a></li>
                                  <li><a href="<?php echo $report_sale_uploaders; ?>"><?php echo $text_report_sale_uploaders; ?></a></li>
                                </ul>
                            </li>

                            <?php if($text_product_report) { ?>
                                <li><a class="parent"><?php echo $text_product_report; ?></a>
                                    <ul>
                                        <li><a href="<?php echo $report_product_viewed; ?>"><?php echo $text_report_product_viewed; ?></a></li>
                                        <li><a href="<?php echo $report_product_purchased; ?>"><?php echo $text_report_product_purchased; ?></a></li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if($text_customer_report) { ?>
                                <li><a class="parent"><?php echo $text_customer_report; ?></a>
                                    <ul>
                                        <li><a href="<?php echo $report_customer_order; ?>"><?php echo $text_report_customer_order; ?></a></li>
                                        <li><a href="<?php echo $report_customer_reward; ?>"><?php echo $text_report_customer_reward; ?></a></li>
                                        <li><a href="<?php echo $report_customer_credit; ?>"><?php echo $text_report_customer_credit; ?></a></li>
                                        <li><a href="<?php echo $report_customer_wishlist; ?>"><?php echo $text_report_customer_wishlist; ?></a></li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if($text_affiliate) { ?>
                                <li><a class="parent"><?php echo $text_affiliate; ?></a>
                                    <ul>
                                        <li><a href="<?php echo $report_affiliate_commission; ?>"><?php echo $text_report_affiliate_commission; ?></a></li>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>

                <?php if($pavblog_installed) { ?>
                    <?php if($text_pavblog_blog) { ?>
                        <li id="pavblogs"><a class="top"><?php echo $text_pavblog_blog; ?></a>
                            <ul>
                                <li><a href="<?php echo $pavblogs_category; ?>"><?php echo $text_pavblog_manage_cate; ?></a></li>
                                <li><a href="<?php echo $pavblogs_blogs; ?>"><?php echo $text_pavblog_manage_blog; ?></a></li>
                                <li><a href="<?php echo $pavblogs_add_blog; ?>"><?php echo $text_pavblog_add_blog; ?></a></li>
                                <li><a href="<?php echo $pavblogs_comments; ?>"><?php echo $text_pavblog_manage_comment; ?></a></li>
                                <li><a href="<?php echo $pavblogs_general; ?>"><?php echo $text_pavblog_general_setting; ?></a></li>

                                <?php if($text_pavblog_front_mods) { ?>
                                    <li><a class="parent"><?php echo $text_pavblog_front_mods; ?></a>
                                        <ul>
                                            <li><a href="<?php echo $pavblogs_category_mod; ?>"><?php echo $text_pavblog_category; ?></a></li>
                                            <li><a href="<?php echo $pavblogs_latest_comment_mod; ?>"><?php echo $text_pavblog_comment; ?></a></li>
                                            <li><a href="<?php echo $pavblogs_latest_mod; ?>"><?php echo $text_pavblog_latest; ?></a></li>
                                        </ul>
                                    </li>
                                <?php } ?>

                            </ul>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
            <ul class="right">
                <li id="store"><a onClick="window.open('<?php echo $store; ?>');" class="top"><?php echo $text_front; ?></a>
                    <ul>
                        <?php foreach ($stores as $stores) { ?>
                            <li><a onClick="window.open('<?php echo $stores['href']; ?>');"><?php echo $stores['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li id="store"><a class="top" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            </ul>
        </div>

        <script type="text/javascript"><!--
            $(document).ready(function() {
	            $('#menu > ul').superfish({
		            hoverClass	 : 'sfHover',
		            pathClass	 : 'overideThisToUse',
		            delay		 : 0,
		            animation	 : {height: 'show'},
		            speed		 : 'normal',
		            autoArrows   : false,
		            dropShadows  : false,
		            disableHI	 : false, /* set to true to disable hoverIntent detection */
		            onInit		 : function(){},
		            onBeforeShow : function(){},
		            onShow       : function(){
                        $(this).css("overflow", "visible");
                    },
		            onHide		 : function(){}
	            });
	
	            $('#menu > ul').css('display', 'block');


                route = getURLVar('route');
                if (!route) {
                    $('#dashboard').addClass('selected');
                } else {
                    part = route.split('/');

                    url = part[0];

                    if (part[1]) {
                        url += '/' + part[1];
                    }
                    $('a[href*=\'' + url + '\']').parents('li[id]').addClass('selected');
                }
            });
 
            function getURLVar(urlVarName) {
                var urlHalves = String(document.location).toLowerCase().split('?');
                var urlVarValue = '';

                if (urlHalves[1]) {
                    var urlVars = urlHalves[1].split('&');

                    for (var i = 0; i <= (urlVars.length); i++) {
                        if (urlVars[i]) {
                            var urlVarPair = urlVars[i].split('=');

                            if (urlVarPair[0] && urlVarPair[0] == urlVarName.toLowerCase()) {
                                urlVarValue = urlVarPair[1];
                            }
                        }
                    }
                }

                return urlVarValue;
            }



        //--></script>
    <?php } ?>
</div>
