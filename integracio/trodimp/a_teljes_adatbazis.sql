BEGIN
declare vege int default false;
declare uj_kod varchar(30) default "";
declare uj_nev varchar(255) default "";
declare uj_nyelv varchar(3) default "";

declare uj_termekkod  varchar(15) default 0;
declare uj_mennyiseg  float       default 0;
declare uj_raktar     int(11)     default 0;


declare uj_PartnerKod int(11)     default 0;
declare uj_Engedmeny  double      default 0;
declare uj_SpeciAr    double      default 0;
declare uj_Datumtol   date        default "";
declare uj_Datumig    date        default "";
declare uj_Engedmeny2 double      default 0;

declare uj_Arkategoria int(11)    default 0;
declare uj_FizetesiMod varchar(1) default "";

declare uj_ListaAr    double      default 0;
declare uj_Ar1        double      default 0;
declare uj_Ar2        double      default 0;
declare uj_Ar3        double      default 0;
declare uj_Ar4        double      default 0;
declare uj_Ar5        double      default 0;
declare uj_Ar6        double      default 0;
declare uj_Ar7        double      default 0;
declare uj_Ar8        double      default 0;
declare uj_Ar9        double      default 0;
declare uj_Ar10       double      default 0;

declare uj_HasonloTerm varchar(15)     default "";

declare uj_TermekNev   varchar(160)    default "";
declare uj_NevMasodik  varchar(40)     default "";
declare uj_NevHarmadik varchar(40)     default "";
declare uj_Megjegyzes  varchar(4096)   default "";


declare uj_Torolve          int(1)        default 0;
declare uj_Statusza         varchar(1)    default "";
declare uj_MennyisegiEgyseg varchar(25)   default "";
declare uj_RendelesiEgyseg  int(11)       default 0;
declare uj_TermekTipus      varchar(5)    default "";
declare uj_Suly             float         default 0;
declare uj_Vonalkod         varchar(15)   default "";
declare uj_Afa              int(2)        default 0;

declare uj_Megnevezes       varchar(30)   default "";

declare lepteto   cursor for select Kod, Nev from  trodimp_trodimp.web_kategoriak order by Kod;
declare lepteto1  cursor for select Kod, Nev , Nyelv from  trodimp_trodimp.web_kategoriak_szovegek order by Kod;

declare lepteto2  cursor for select TermekKod, Mennyiseg, Raktar from  trodimp_trodimp.web_keszlet;
declare lepteto3  cursor for select TermekKod, PartnerKod, Engedmeny, SpeciAr, Datumtol , Datumig, Engedmeny2 from  trodimp_trodimp.mx_partner_engedmeny;
declare lepteto4  cursor for select Arkategoria, PartnerKod, FizetesiMod   from  trodimp_trodimp.mx_partner_torzs;
declare lepteto5  cursor for select TermekKod, ListaAr, Ar1, Ar2, Ar3, Ar4, Ar5, Ar6, Ar7, Ar8, Ar9, Ar10 from  trodimp_trodimp.mx_tizfele_ar;
declare lepteto6  cursor for select TermekKod, Kod from  trodimp_trodimp.web_termek_kategoria;
declare lepteto7  cursor for select TermekKod, HasonloTerm from  trodimp_trodimp.web_termek_ref;
declare lepteto8  cursor for select TermekKod, Nyelv, TermekNev, NevMasodik, NevHarmadik, Megjegyzes from  trodimp_trodimp.web_termek_szovegek;
declare lepteto9  cursor for select Torolve, Statusza, TermekKod, MennyisegiEgyseg, RendelesiEgyseg, TermekTipus, ListaAr, Suly, Vonalkod, Afa from  trodimp_trodimp.web_termek_torzs;
declare lepteto10 cursor for select TermekTipus, Megnevezes from  trodimp_trodimp.web_termek_tipus;
declare lepteto11 cursor for select TermekTipus2, Megnevezes from  trodimp_trodimp.web_termek_tipus2;

declare continue handler for not found set vege = true;

open lepteto11;
ciklus: loop
fetch lepteto11 into uj_kod, uj_nev;
if vege then
leave ciklus;
else
call termek_tipus2_felvisz(uj_kod, uj_nev);
end if;
end loop ciklus;
close lepteto11;






end