<?php
class ModelShippingParcelforce48 extends Model {
    function getQuote($address, $fizetesek="") {
		$this->load->language('shipping/parcelforce_48');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('parcelforce_48_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if (!$this->config->get('parcelforce_48_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		$method_data = array();
	
		if ($status) {

            $fizetesi_mod = array();

            if ($fizetesek){
                foreach($fizetesek as $value) {
                    if ( $this->config->get("parcelforce_48_payment_".$value['code']) &&  $this->config->get("parcelforce_48_payment_".$value['code']) == 1){
                        $fizetesi_mod[] = array(
                            'fizetesi_mod' => $value['code']
                        );
                    }
                }
            }
			$cost = 0;
			$weight = $this->cart->getWeight();
			$sub_total = $this->cart->getSubTotal();
			
			$rates = explode(',', $this->config->get('parcelforce_48_rate'));
			
			foreach ($rates as $rate) {
  				$data = explode(':', $rate);
  					
				if ($data[0] >= $weight) {
					if (isset($data[1])) {
    					$cost = $data[1];
					}
					
   					break;
  				}
			}

			$rates = explode(',', $this->config->get('parcelforce_48_insurance'));
			
			foreach ($rates as $rate) {
  				$data = explode(':', $rate);
  				
				if ($data[0] >= $sub_total) {
					if (isset($data[1])) {
    					$insurance = $data[1];
					}
					
   					break; 
  				}
			}
			
			$quote_data = array();
			
			if ((float)$cost) {
				$text = $this->language->get('text_description');
			
				if ($this->config->get('parcelforce_48_display_weight')) {
					$text .= ' (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')';
				}
			
				if ($this->config->get('parcelforce_48_display_insurance') && (float)$insurance) {
					$text .= ' (' . $this->language->get('text_insurance') . ' ' . $this->currency->format($insurance) . ')';
				}		

				if ($this->config->get('parcelforce_48_display_time')) {
					$text .= ' (' . $this->language->get('text_time') . ')';
				}

                if ($this->config->get('parcelforce_48_tax_class_id') > 0) {
                    $query_tax = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$this->config->get('parcelforce_48_tax_class_id') . "'");
                }
                $ado_title = isset($query_tax->row['title']) ? $query_tax->row['title'] :"";

      			$quote_data['parcelforce_48'] = array(
        			'code'         => 'parcelforce_48.parcelforce_48',
        			'title'        => $this->config->get('parcelforce_48_text') != null ? $this->config->get('parcelforce_48_text') :  $this->language->get('text_description'),
                    'fizetesi_mod'  => $fizetesi_mod,
        			'cost'         => $cost,
        			'tax_class_id' => $this->config->get('parcelforce_48_tax_class_id'),
					'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('parcelforce_48_tax_class_id'), $this->config->get('config_tax'))),
                    'netto_text'   => $this->currency->format($cost),
                    'ado_title'    => $ado_title

                );

      			$method_data = array(
        			'code'       => 'parcelforce_48',
        			'title'      => $this->config->get('parcelforce_48_header') != null ? $this->config->get('parcelforce_48_header') :  $this->language->get('text_description'),
        			'quote'      => $quote_data,
                    'fizetesi_mod'  => $fizetesi_mod,
					'sort_order' => $this->config->get('parcelforce_48_sort_order'),
        			'error'      => false
      			);
			}
		}
	
		return $method_data;
	}
}
?>