<?php
class ControllerModuleSplashPage extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/splash_page');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {			
			$this->model_setting_setting->editSetting('splash_page', $this->request->post);		
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['entry_fejlec_latszik'] = $this->language->get('entry_fejlec_latszik');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_image'] = $this->language->get('text_image');
		$this->data['text_or'] = $this->language->get('text_or');
		$this->data['text_color'] = $this->language->get('text_color');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		
		
		$this->data['entry_image']      = $this->language->get('entry_image');
		$this->data['entry_pattern']    = $this->language->get('entry_pattern');
		$this->data['entry_bgcolor']    = $this->language->get('entry_bgcolor');
		$this->data['entry_url']        = $this->language->get('entry_url');
		$this->data['entry_timer']      = $this->language->get('entry_timer');
		$this->data['entry_layout']     = $this->language->get('entry_layout');
		$this->data['entry_position']   = $this->language->get('entry_position');
		$this->data['entry_status']     = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
		
		$this->load->model('tool/image');
		
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}
		
		if (isset($this->error['url'])) {
			$this->data['error_url'] = $this->error['url'];
		} else {
			$this->data['error_url'] = array();
		}
				
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/splash_page', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/splash_page', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['splash_page_product'])) {
			$this->data['splash_page_product'] = $this->request->post['splash_page_product'];
		} else {
			$this->data['splash_page_product'] = $this->config->get('splash_page_product');
		}

        if (isset($this->request->post['splash_page_product_fejlec'])) {
            $this->data['splash_page_product_fejlec'] = $this->request->post['splash_page_product_fejlec'];
        } else {
            $this->data['splash_page_product_fejlec'] = $this->config->get('splash_page_product_fejlec');
        }


        $this->data['modules'] = array();
		
		if (isset($this->request->post['splash_page_module'])) {
			$modules = $this->request->post['splash_page_module'];
		} elseif ($this->config->get('splash_page_module')) {
			$modules = $this->config->get('splash_page_module');
		}  else {
			$modules = array();
		}

		foreach($modules as $module){
			if ($module['image']){
				$thumb = $this->model_tool_image->resize($module['image'],100,100);
			}else {
				$thumb = $this->model_tool_image->resize('no_image.jpg',100,100);
			}
			$module['thumb'] = $thumb;
			
			if ($module['pattern']){
				$pattern_thumb = $this->model_tool_image->resize($module['pattern'],100,100);
			}else {
				$pattern_thumb = $this->model_tool_image->resize('no_image.jpg',100,100);
			}
			$module['pattern_thumb'] = $pattern_thumb;
			
			$this->data['modules'][] = $module; 	
		}
				
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/splash_page.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/splash_page')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$this->load->model('module/splash_page');
		
		if (isset($this->request->post['splash_page_module'])) {
			foreach ($this->request->post['splash_page_module'] as $key => $value) {
				if (!utf8_strlen($value['image'])) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
				
				if (utf8_strlen($value['url']) > 0 ){
					$parts = explode("&", $value['url']);
					$lroute = $parts[0];

					if ($this->model_module_splash_page->getLayoutIdFromRoute($lroute) != $value['layout_id']){
						$this->error['url'][$key] = $this->language->get('error_url');
					}
				}
			}
		}
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>