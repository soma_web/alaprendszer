<?php if(!(isset($beallitas_termek_aloldal['null_off']) && $beallitas_termek_aloldal['null_off'])) { ?>
    <?php $not_null = true; ?>
<?php } ?>

<?php if($not_null && empty($ar_tiltasa)) { ?>
    <?php if (!$special) { ?>
        <?php if (isset($beallitas_termek_aloldal['ar_text']) && $beallitas_termek_aloldal['ar_text']) { ?>
            <div class="price_egyedul price_netto_ar <?php echo !$mutat_arat ? 'price_nemkell' : ''?>">
                <span class="price_text"><?php echo $text_price; ?></span>
                <span class="price-new"> <?php echo $price_netto; ?>
                    <?php echo (!empty($beallitas_termek_aloldal['netto_plus_afa']) && $beallitas_termek_aloldal['netto_plus_afa']) ? $text_plus_afa : '' ; ?>
                </span>
            </div>

        <?php } else if (isset($beallitas_termek_aloldal['netto_text']) && $beallitas_termek_aloldal['netto_text']) { ?>
            <div class="price_egyedul price_netto_ar <?php echo !$mutat_arat ? 'price_nemkell' : ''?>">
                <span class="price_text"><?php echo $text_tax; ?> </span>
                <span class="price-new"> <?php echo $price_netto; ?>
                    <?php echo (!empty($beallitas_termek_aloldal['netto_plus_afa']) && $beallitas_termek_aloldal['netto_plus_afa']) ? $text_plus_afa : '' ; ?>
                </span>
            </div>

        <?php } else { ?>
            <div class="price_egyedul price_netto_ar <?php echo !$mutat_arat ? 'price_nemkell' : ''?>">
                <?php echo $price; ?><?php echo (!empty($beallitas_termek_aloldal['netto_plus_afa']) && $beallitas_termek_aloldal['netto_plus_afa']) ? $text_plus_afa : '' ; ?>
            </div>
        <?php } ?>

    <?php } else {?>
        <div class="price_ujarral price_netto_ar <?php echo !$mutat_arat ? 'price_nemkell' : ''?>">
            <?php if (isset($beallitas_termek_aloldal['ar_text']) && $beallitas_termek_aloldal['ar_text']) { ?>
                <span class="price_text"><?php echo $text_price; ?></span>
                <span class="price-old"> <?php echo $price_netto; ?></span>
                <span class="price-new"><?php echo $special_netto; ?>
                    <?php echo (!empty($beallitas_termek_aloldal['netto_plus_afa']) && $beallitas_termek_aloldal['netto_plus_afa']) ? $text_plus_afa : '' ; ?>
                </span>

            <?php } else if (isset($beallitas_termek_aloldal['netto_text']) && $beallitas_termek_aloldal['netto_text']) { ?>
                <div class="price_egyedul price_netto_ar">
                    <span class="price_text price_text_netto"><?php echo $text_tax; ?> </span>
                    <?php if (empty($beallitas_termek_aloldal['eredeti_ar_tiltas'])) { ?>
                        <span class="price-old"> <?php echo $price_netto; ?></span>
                    <?php } ?>
                    <span class="price-new"><?php echo $special_netto; ?>
                        <?php echo (!empty($beallitas_termek_aloldal['netto_plus_afa']) && $beallitas_termek_aloldal['netto_plus_afa']) ? $text_plus_afa : '' ; ?>
                    </span>
                </div>

            <?php } else { ?>
                <?php if (empty($beallitas_termek_aloldal['eredeti_ar_tiltas'])) { ?>
                    <span class="price-old"> <?php echo $price_netto; ?></span>
                <?php } ?>

                <span class="price-new">
                    <?php echo $special_netto; ?><?php echo (!empty($beallitas_termek_aloldal['netto_plus_afa']) && $beallitas_termek_aloldal['netto_plus_afa']) ? $text_plus_afa : '' ; ?>
                </span>
            <?php } ?>
        </div>
    <?php }?>
<?php } ?>