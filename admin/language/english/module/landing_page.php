<?php
// Heading
$_['heading_title']    = 'Landing Page';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have saved settings for TG Helios Html Module!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';

// Entry
$_['entry_product']       = 'Products:';
$_['entry_area_id']       = 'HTML Area:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_classname']     = 'CSS Class:';
$_['entry_area']          = 'HTML Area';
$_['entry_code']          = 'HTML Content:';
$_['entry_yes']	      = 'Yes';
$_['entry_no']	           = 'No';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify this module!';
$_['error_code']          = 'Code Required';
?>
