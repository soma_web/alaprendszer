<?php
class ControllerPaymentPaymentElhelyezkedesCod1 extends Controller {
	protected function index() {
    	$this->data['button_confirm'] = $this->language->get('button_confirm');

		$this->data['continue'] = $this->url->link('checkout/success_elhelyezkedes');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/payment_elhelyezkedes/cod1.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/payment_elhelyezkedes/cod1.tpl';
        } elseif ( file_exists("catalog/view/theme/default/template/payment/payment_elhelyezkedes/cod1.tpl") ) {
            $this->template = 'default/template/payment/payment_elhelyezkedes/cod1.tpl';
        } else {
            $this->template = 'default/template/payment/cod1.tpl';
        }

        $this->render();
    }

    public function confirm() {
        $this->load->model('checkout/order');

        $this->model_checkout_order->updateElhelyezkedes();

    }
}
?>