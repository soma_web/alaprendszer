<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style>
        * { font-family: arial, Sans-Serif, DejaVu Sans; font-size:10px } /* Fontos ezt használni, másképp nem működik rendesen az UTF-8-as kódolás */
        #content > form td {
            min-width: inherit;
        }

        #content > form table.termek {
            width: 100%;
            border-bottom: 1px solid #ccc;
        }

        #content > form table.termek thead td {
            padding: 7px;
        }

        #content > form table.termek tr td {
            text-align: right;
        }



        #content > form {
            padding: 0;
        }

        .fejlec {
            display: table;
            width: 100%;
            background: #eee;
            padding: 10px;
            margin: 0 0 10px 0;
        }
       /* .fejlec_oszlop {
            display: table-cell;
        }*/

        .cimke {
           /* font-size: 12px;*/
            color: #555;
        }
        .ertek {
           /* font-size: 12px;*/
            color: #e60000;
        }
        .right {
            text-align: right;
        }
        .odd {
            background: #E2E9E6;
        }

        .arajanlat_modosit span  {
            height: 20px;
            background: #f7f7f7;
            margin: 0 0 3px 0;

        }

        .arajanlat_modosit td img {
            margin: 10px 0 0 0;
        }

        .arajanlat_modosit td.listaar {
            font-weight: bold;
        }
        .small {
          /*  font-size: 10px;*/
        }
        .arajanlat_modosit td span .product_price_color {

        }
        #customer_name {
            text-decoration: underline;
            cursor: pointer;
        }


        #kozepe_hatternek .kozepe #content a:hover {
            color: #010;
        }

        table.list .right {
            padding: 7px 0;
        }

        .ertek.ajanlat-adatok {
            color: #333;
            text-decoration: underline;
            cursor: pointer;
            font-weight: bold;
           /* font-size: 12px;*/
        }
        .ertek.ajanlat-adatok:hover {
            color: #010;
        }
        .ajanlat_fejlec_adatai{
            display: table;
            width: 100%;
        }

        .arajanlat_szallito {
            border: 1px solid #ddd;
            padding: 10px;
        }

        .arajanlat_szallito span  {
            width: 250px;

        }


        .arajanlat_szallito.szallito {
            margin-right: 20px;
        }


        .tabla_sor {
            display: table-row;
        }
        .tabla_oszlop {
            display: table-cell;
            border-top: 1px solid #ddd;
        }
        .szam_kelt_mod {
            margin: 15px 0;
        }

        .bekezdes {
            padding-left: 15px;
        }

        .szelesseg {
            width: 25%;
            display: inline-table;
        }
        .ceg_neve {
            font-weight: bold;
        }

        .ajanlat_fejlec_adatai.felso tr td {
            border: 2px solid #ddd;
            padding: 3px;
        }

        .termek_fejlec td {
            border-bottom: 2px solid #000;
            border-top: 2px solid #000;
            padding: 10px;
        }

        .termek_body td {
            border-bottom: 1px solid #ddd;
            padding: 5px;
        }
        .termek_body_osszesen td {
            border-bottom: 1px solid #888;
            padding: 5px;
        }

        .termek_foot td {
            border-bottom: 3px solid #000;
            padding: 5px;
            padding-bottom: 15px;
            padding-top: 15px;
        }



    </style>

    <table style="width: 100%">
        <tr style="width: 100%">
            <td style="width: 50%"><img src="http://textil.nmsnet.hu/image/data/logo/textilpng_small.png" alt="kep" title="kep"/></td>
            <td style="width: 50%; text-align: center"><div style="font-size:20px; text-align: center;" class="title"><?php echo $heading_arajanlat; ?></div></td>
        </tr>
    </table>

    <div id="ajanlat_fejlec" >
        <table class="ajanlat_fejlec_adatai"><tr><td style="vertical-align: top; min-height: 800px">
            <div class="fejlec_oszlop">
                <div><b><?php echo $fejlec_szallito?></b></div>
                <div style="height: 180px;"  class="arajanlat_szallito szallito">
                    <div class="ceg_neve"><span><?php echo $arajanlat_info['szallito_neve'];?></span></div>
                    <div><?php echo $fejlec_szekhely?></div>
                    <div class="bekezdes"><span><?php echo $arajanlat_info['szallito_varos'];?></span></div>
                    <div class="bekezdes"><span><?php echo $arajanlat_info['szallito_utca'];?></span></div>
                    <div class="bekezdes"><span><?php echo $arajanlat_info['szallito_orszag'];?></span></div>
                    <div style="padding-top: 15px">
                        <div>
                            <span class="szelesseg"><?php echo $fejlec_adoszam?></span>
                            <span><?php echo $arajanlat_info['szallito_adoszam'];?></span>
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_bank?></span>
                            <span><?php echo $arajanlat_info['szallito_bank'];?></span>
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_bank_szamla?></span>
                            <span><?php echo $arajanlat_info['szallito_bankszamla_szam'];?></span>
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_swift_kod?></span>
                            <span><?php echo $arajanlat_info['szallito_bankszamla_swift'];?></span>
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_iban_kod?></span>
                            <span><?php echo $arajanlat_info['szallito_bankszamla_iban'];?></span>
                        </div>
                    </div>
                </div>
            </div>
            </td>
            <td style="vertical-align: top;">
            <div class="fejlec_oszlop">
                <div><b><?php echo $fejlec_vevo?></b></div>
                <div style="height: 180px;" class="arajanlat_szallito">
                    <div  class="ceg_neve"><span><?php echo $arajanlat_info['vevo_neve'];?></span></div>
                    <div><?php echo $fejlec_szekhely?></div>
                    <div class="bekezdes"><span><?php echo $arajanlat_info['vevo_varos'];?></span></div>
                    <div class="bekezdes"><span><?php echo $arajanlat_info['vevo_utca'];?></span></div>
                    <div class="bekezdes"><span><?php echo $arajanlat_info['vevo_orszag'];?></span></div>
                    <div style="padding-top: 15px">
                        <div>
                            <span class="szelesseg"><?php echo $fejlec_adoszam?></span>
                            <span><?php echo $arajanlat_info['vevo_adoszam'];?></span>
                        </div>
                        <div>
                            <span class="szelesseg"><?php echo $fejlec_bank?></span>
                            <span><?php echo $arajanlat_info['vevo_bank'];?></span>
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_bank_szamla?></span>
                            <span><?php echo $arajanlat_info['vevo_bankszamla_szam'];?></span>
                        </div>
                    </div>
                </div>
                </td>
                </tr>
            </table>

        </div>

        <table cellspacing="0" class="ajanlat_fejlec_adatai felso szam_kelt_mod">
            <tr class="tabla_sor">
                <td class="tabla_oszlop"><?php echo $fejlec_ajanlat_szam?>

                </td>
                <td class="tabla_oszlop"><?php echo $fejlec_ajanlat_kelt?>

                </td>
                <td class="tabla_oszlop"><?php echo $fejlec_szallitasi_hatarido?>

                </td>
                <td class="tabla_oszlop"><?php echo $fejlec_fizetesi_hatarido?>

                </td>
                <td class="tabla_oszlop"><?php echo $fejlec_fizetesi_mod?>

                </td>
            </tr>
            <tr class="tabla_sor">
                <td class="tabla_oszlop" style="font-weight: bold">
                    <?php echo 'AJ'.$arajanlat_info['arajanlat_id'];?>
                </td>
                <td class="tabla_oszlop">
                    <span><?php echo $arajanlat_info['ajanlat_kelte'];?></span>
                </td>
                <td class="tabla_oszlop">
                    <span><?php echo $arajanlat_info['ajanlat_szallitasi_hatarido'];?></span>
                </td>
                <td class="tabla_oszlop">
                    <span><?php echo $arajanlat_info['ajanlat_fizetesi_hatarido'];?></span>
                </td>
                <td class="tabla_oszlop">
                    <span><?php echo $arajanlat_info['ajanlat_fizetesi_mod'];?></span>
                </td>
            </tr>
        </table>

        <table  class="ajanlat_fejlec_adatai also szam_kelt_mod">
            <tr class="tabla_sor">
                <td class="tabla_oszlop"><?php echo $fejlec_kuldo?>

                </td>
                <td class="tabla_oszlop"><?php echo $fejlec_telefon?>

                </td>
                <td class="tabla_oszlop"><?php echo $fejlec_email?>

                </td>
            </tr>
            <tr class="tabla_sor">
                <td>
                    <span><?php echo $arajanlat_info['ajanlat_kuldo_neve'];?></span>
                </td>
                <td>
                    <span><?php echo $arajanlat_info['ajanlat_kuldo_telefon'];?></span>
                </td>
                <td>
                    <span><?php echo $arajanlat_info['ajanlat_kuldo_email'];?></span>
                </td>
            </tr>
        </table>

        <div style="margin-bottom:3px;">
            <span style="width:100%; margin-top: 10px ; height: 28px" ><b><?php echo $arajanlat_info['ajanlat_megszolitas'];?></b></span>
        </div>
        <div style="margin-bottom: 15px;">
            <span style="width:100%; margin: 5px 0 0px 0; height: 28px" ><?php echo $arajanlat_info['ajanlat_altalanos_szoveg_felul'];?></span>
        </div>

    <?php if (isset($arajanlat_info['products']) && $arajanlat_info['products']) { ?>

        <table style="width: 100%; margin-bottom: 10px;" cellspacing="0" class="termek list">
            <thead>
                <tr class="termek_fejlec">
                    <td><b><?php echo $column_kep?></b></td>
                    <td><b><?php echo $column_nev?></b></td>
                    <td><b><?php echo $column_model?></b></td>
                    <td><b><?php echo $column_listaar?></b></td>
                    <td><b><?php echo $column_engedmeny_szazalek?></b></td>
                    <td><b><?php echo $column_engedmeny_ertek?></b></td>
                    <td><b><?php echo $column_mennyiseg?></b></td>
                    <td style="text-align: right;"><b><?php echo $column_ertek?></b></td>
                </tr>
            </thead>
        <tbody>
        <?php $class = 'even'; ?>

        <?php foreach($arajanlat_info['products'] as $product) { ?>
            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>

            <tr class="<?php echo $class?> termek_body">
                <td><img src="<?php echo $product['image']?>" ></td>

                <td><b><?php echo $product['name']?></b>
                    <?php $van_option = false;?>
                    <?php $van_valamilyen_option = false;?>

                    <?php if (isset($product['options']) && $product['options'] ) { ?>
                        <?php foreach($product['options'] as $option) { ?>

                            <?php if (isset($option['value']) && $option['value']) { ?>
                                <?php $van_valamilyen_option = true;; ?>

                                <?php foreach($option['value'] as $value) { ?>
                                    <?php if (!$value['mennyiseg']) { ?>
                                        <?php $van_option = true;?>

                                        <br>
                                        <?php echo $option['option_name'].': '.$value['option_value_name']?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($van_option) { ?>
                        <br>
                        <br>
                        <br>
                    <?php } ?>
                </td>

                <td><?php echo $product['model']?></td>

                <td><b><?php echo $this->currency->format($product['price'])?></b>
                    <?php $ar=(int)$product['price']?>

                    <?php if (isset($product['options']) && $product['options'] ) { ?>
                        <?php foreach($product['options'] as $option) { ?>

                            <?php if (isset($option['value']) && $option['value']) { ?>
                                <?php foreach($option['value'] as $value) { ?>
                                    <?php if (!$value['mennyiseg']) { ?>
                                        <br>
                                        <?php echo $value['price'] ? $value['price_prefix'].' '.$this->currency->format($value['price']) : ''?>
                                        <?php $ar +=$value['price']?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($van_option) { ?>
                        <br>
                        <br>
                        <br>
                    <?php } ?>

                </td>

                <td>
                    <?php echo $product['szazalek']?>
                </td>
                <td>
                    <?php echo round($product['egysegar'],1)?>"
                </td>
                <td>
                    <?php echo $product['mennyiseg']?>
                </td>

                <td style="text-align: right;">
                    <?php echo $this->currency->format($product['ertek'])?>
                </td>
            </tr>

            <?php $van_option = false;?>

            <?php if (isset($product['options']) && $product['options'] ) { ?>
                <?php foreach($product['options'] as $option) { ?>

                    <?php if (isset($option['value']) && $option['value']) { ?>
                        <?php foreach($option['value'] as $value) { ?>
                            <?php if ($value['mennyiseg']) { ?>
                                <?php $van_option = true;?>

                                <tr class="<?php echo $class?> termek_body">
                                    <td></td>
                                    <td><?php echo $option['option_name'].': '.$value['option_value_name']?></td>
                                    <td></td>

                                    <?php if ($value['price'] > 0) { ?>
                                        <td>
                                            <?php echo $value['price_prefix'].' '.$this->currency->format($value['price'])?>
                                        </td>
                                        <td>
                                            <?php echo $value['szazalek']?>
                                        </td>
                                        <td>
                                            <?php echo round($value['egysegar'],1)?>
                                        </td>
                                    <?php } else { ?>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    <?php } ?>

                                    <td>
                                        <span class="small"><?php echo $value['mennyiseg']?>
                                            <span><?php echo ' '.$product['megyseg']; ?></span>
                                        </span>
                                    </td>
                                    <?php if ($value['price'] > 0) { ?>
                                        <td style="text-align: right;" >
                                            <?php echo $this->currency->format($value['ertek'])?>
                                        </td>
                                    <?php } else { ?>
                                        <td></td>
                                    <?php } ?>
                                </tr>

                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>

            <?php if ($van_option) { ?>
                <tr class="<?php echo $class?> termek_body_osszesen">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <div style="text-align: right"><?php echo $column_ar_osszesen?>:</div>
                        <div style="margin-bottom: 5px; text-align: right">
                           <b><?php echo $this->currency->format($product['product_ertek_osszesen'])?></b>
                        </div>
                    </td>
                </tr>
            <?php } ?>


        <?php } ?>
        </tbody>
        <tfoot>
        <tr class="termek_foot">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <div style="text-align: right;"><b><?php echo $column_total_osszesen?>:</b></div>
                <div style="font-size: 12px;text-align: right;"><b><?php echo $this->currency->format($arajanlat_info['total'])?></b></div>
            </td>
        </tr>
        </table>
    <?php } ?>

    <div style="margin-top: 20px; width: 100%">
        <?php echo html_entity_decode($arajanlat_info['ajanlat_altalanos_szoveg_alul'], ENT_QUOTES, 'UTF-8');?>
    </div>

