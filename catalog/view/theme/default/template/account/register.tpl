<?php $registracio = 1 ?>
<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <h1><?php echo $heading_title; ?></h1>
  <p><?php echo $text_account_already; ?></p>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <?php if (($this->config->get('megjelenit_regisztracioblokk_szemelyes') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracioblokk_ceges_szemelyes') == 1 && $ceges == 1 )) {?>
      <h2><?php echo $text_your_details; ?></h2>
      <?php
      include('catalog/view/theme/default/template/account/customer_personal_block.tpl');
      ?>
  <?php } ?>

  <?php if (($this->config->get('megjelenit_regisztracioblokk_cim') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracioblokk_ceges_cim') == 1 && $ceges == 1 )) { ?>
      <?php if ( $ceges == 0 ) { ?>
          <h2><?php echo $text_your_address; ?> </h2>
      <?php } else { ?>
          <h2><?php echo $text_your_cegadatok; ?> </h2>
      <?php } ?>
      <?php
      include('catalog/view/theme/default/template/account/customer_address_block.tpl');
      ?>
  <?php } ?>
  <h2><?php echo $text_your_password; ?></h2>
  <div class="content">
      <table class="form">
          <tr>
              <td><span class="required">*</span> <?php echo $entry_password; ?></td>
              <td><input type="password" name="password" value="<?php echo $password; ?>" />
                  <?php if ($error_password) { ?>
                      <span class="error"><?php echo $error_password; ?></span>
                  <?php } ?></td>
          </tr>
          <tr>
              <td><span class="required">*</span> <?php echo $entry_confirm; ?></td>
              <td><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
                  <?php if ($error_confirm) { ?>
                      <span class="error"><?php echo $error_confirm; ?></span>
                  <?php } ?></td>
          </tr>
      </table>
  </div>
  <?php if (($this->config->get('megjelenit_regisztracioblokk_paypal') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracioblokk_ceges_paypal') == 1 && $ceges == 1 )) {?>
      <h2><?php echo $text_paypaltitle; ?></h2>
      <div class="content">
          <table class="form">
              <?php if ( ($this->config->get('megjelenit_regisztracio_paypal') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_paypal') == 1 && $ceges == 1) ) {?>
              <tr>
                  <td><?php echo $text_paypallable; ?></td>
                  <td><input type="text" name="paypalemail" value="<?php echo $paypalemail; ?>"  /></td>
              </tr>
              <?php } ?>
          </table>
      </div>
  <?php } ?>
  <?php if (($this->config->get('megjelenit_regisztracioblokk_hirlevel') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracioblokk_ceges_hirlevel') == 1 && $ceges == 1 )) {?>
      <h2><?php echo $text_newsletter; ?></h2>
      <div class="content">
          <table class="form">
              <tr >
                  <td><?php echo $entry_newsletter; ?>
                      <?php if($newsletter == '1') { ?>
                          <input id="newsletter" type="checkbox" class="css-checkbox" name="newsletter" checked="checked" value="1" />
                      <?php } else { ?>
                          <input id="newsletter" type="checkbox" class="css-checkbox" name="newsletter" value="1" />
                      <?php } ?>
                  </td>
                  <?php if ( ($this->config->get('megjelenit_regisztracio_hirlevel_kategoriak') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_hirlevel_kategoriak') == 1 && $ceges == 1) ) {?>
                      <?php if($newsletter == '1') { ?>
                          <td id="newslettercategories" style="">
                      <?php } else { ?>
                          <td id="newslettercategories" style="display: none">
                      <?php } ?>
                      <div style="overflow: auto; height: 100px">
                      <?php foreach($categories as $category) { ?>
                          <?php if(in_array($category['category_id'], $newslettercategories)) { ?>
                              <input class="css-checkbox" type="checkbox" checked="checked" name="newslettercategories[]" value="<?php echo $category['category_id']; ?>" /><?php echo $category['name']; ?><br />
                          <?php } else { ?>
                              <input class="css-checkbox" type="checkbox" name="newslettercategories[]" value="<?php echo $category['category_id']; ?>" /><?php echo $category['name']; ?><br />
                          <?php } ?>
                      <?php } ?>
                      </div>
                  </td>
                  <?php } ?>
              </tr>
          </table>
      </div>
  <?php } ?>
    <?php if ($text_agree) { ?>
    <div class="buttons">
      <div class="right"><?php echo $text_agree; ?>
        <?php if ($agree) { ?>
            <input type="checkbox" class="agree" name="agree" value="1" checked="checked" />
        <?php } else { ?>
            <input type="checkbox"  class="agree" name="agree" value="1" />
        <?php } ?>
        <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
      </div>
    </div>
    <?php } else { ?>
    <div class="buttons">
      <div class="right">
        <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
      </div>
    </div>
    <?php } ?>
    <input type="hidden" name="feltolto" value="<?php echo $ceges ?>" />

  </form>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('select[name=\'zone_id\']').load('index.php?route=account/register/zone&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
//--></script> 
<script type="text/javascript"><!--
/*$('.colorbox').colorbox({
	width: 560,
	height: 560
});*/
    <?php if ( ($this->config->get('megjelenit_regisztracio_hirlevel_kategoriak') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_hirlevel_kategoriak') == 1 && $ceges == 1) ) {?>
    $('#newsletter').on('change', function() {
        if (this.checked) {
            $('#newslettercategories').show();
        } else {
            $('#newslettercategories').hide();
        }
    });
    <?php } ?>

//--></script>
    <script type="text/javascript">
        $("[name=postcode]").autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: 'index.php?route=account/address/getpostcode&part=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item.text,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            search: function(event) {
            },
            focus: function() {
            },
            select: function( event, ui ) {
                $.ajax({
                    url: 'index.php?route=account/address/getcity&postcodeid=' +  encodeURIComponent(ui.item.value),
                    dataType: 'json',
                    success: function(json) {
                        debugger;
                        if(! json instanceof Object) {
                            return false;
                        }
                        var cityInput = $('[name=city]');
                        var postcode = $("[name=postcode]")
                        //var zoneInput = $('[name=zone_id]');
                        //var countryInput = $('[name=country_id]');
                        if(cityInput != undefined && cityInput.length != 0) {
                            cityInput.val(json.city);
                        }
                        if(postcode != undefined && postcode.length != 0) {
                            postcode.val(json.postcode);
                        }
                        /*
                        if(zoneInput != undefined && zoneInput.length != 0) {
                            var zoneSelect = zoneInput.find('option:contains("'+json['country']+'")');
                            if(zoneSelect != undefined && zoneSelect.length != 0) {
                                zoneSelect[0].selected = 'selected'
                            }
                        }
                        if(countryInput != undefined) {
                            countryInput.find('option:contains("Hungary")')[0].selected = 'selected';
                        }
                        */
                    }
                });
                ui.item.value = $('[name=postcode]').val();
            }
        });
    </script>
<?php echo $footer; ?>