<?php

// Heading
$_['heading_title']    = 'Kuponok';

// Text
$_['text_total']       = 'Összes rendelés';
$_['text_success']     = 'Sikeresen módosította az ajándékutalványt!';

// Entry
$_['entry_status']     = 'Állapot:';
$_['entry_sort_order'] = 'Sorrend:';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az ajándékutalvány módosítása az Ön számára nem engedélyezett!';
?>
