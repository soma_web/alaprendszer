<?php

// Heading
$_['heading_title']     = 'Címjegyzék';

// Text
$_['text_account']      = 'Fiók';
$_['text_address_book'] = 'Címjegyzék tételek';
$_['text_edit_address'] = 'Cím módosítás';
$_['text_insert']       = 'A cím hozzáadása sikerült';
$_['text_update']       = 'A cím módosítása sikerült';
$_['text_delete']       = 'A cím törlése sikerült';

// Entry
$_['entry_firstname']   = 'Vezetéknév:';
$_['entry_lastname']    = 'Keresztnév:';
$_['entry_company']     = 'Cég:';
$_['entry_adoszam']     = 'Adószám:';

$_['entry_address_1']   = 'Utca, házszám:';
$_['entry_address_2']   = 'Kiegészítő címadatok:';
$_['entry_postcode']    = 'Irányítószám:';
$_['entry_city']        = 'Város:';
$_['entry_country']     = 'Ország:';
$_['entry_zone']        = 'Állam / Megye:';
$_['entry_default']     = 'Új alapértelmezett cím:';
$_['entry_vallalkozasi_forma']      = 'Vállalkozási forma:';
$_['entry_szekhely']                = 'Székhely:';
$_['entry_ugyvezeto_neve']          = 'Ügyvezető neve:';
$_['entry_ugyvezeto_telefonszama']  = 'Ügyvezető telefonszáma:';

// Error
$_['error_delete']      = 'Hiba: Legalább egy címnek lennie kell!';
$_['error_default']     = 'Hiba: Nem törölheti az alapértelmezett címet!';
$_['error_firstname']   = 'Az keresztnév legalább 3 és legfeljebb 32 karakterből állhat!';
$_['error_lastname']    = 'A vezetéknév legalább 3 és legfeljebb 32 karakterből állhat!';
$_['error_address_1']   = 'Az utca, házszám legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_city']        = 'A város legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_postcode']    = 'Az irányítószám legalább 2 és legfeljebb 10 karakterből állhat!';
$_['error_country']     = 'Válasszon országot!';
$_['error_zone']        = 'Válasszon megyét!';
?>
