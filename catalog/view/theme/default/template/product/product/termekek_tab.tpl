<?php
$vantab             = false;
$also_bought_tab    = false;
$kapcsolodo_tab     = false;

    if (isset($also_boughts) && !empty($also_boughts) && isset($beallitas_termek_aloldal['boughts'])) {
        $vantab = true;
        $tab = "also_bought";

    } elseif ($products && isset($beallitas_termek_aloldal['kapcsolodo'])) {
        $vantab = true;
        $tab = "products";
    }
    $language_id = $this->config->get('config_language_id');
    $also_bought_title= $this->config->get('also_bought_heading_title_'.$language_id);

?>
<?php if ($vantab) { ?>
    <div id="tabs_products" class="htabs" >
        <div class="inner-htabs">
            <?php if (isset($also_boughts) && !empty($also_boughts) && isset($beallitas_termek_aloldal['boughts'])) { ?>
                <?php $also_bought_tab = true ?>
                <a href="#tab-bought"><?php echo $also_bought_title; ?> (<?php echo count($also_boughts); ?>) </a>
            <?php } ?>

            <?php if ($products  && isset($beallitas_termek_aloldal['kapcsolodo'])) { ?>
                <?php $kapcsolodo_tab = true;?>
                 <a href="#tab-kapcsolodo"><?php echo $tab_related; ?> (<?php echo count($products); ?>)</a>
            <?php } ?>
        </div>
    </div>
<?php } ?>


<?php if ($also_bought_tab) { ?>
    <div id="tab-bought" class="tab-content" style="padding-right: 0">
        <div class="box-product tab_kornyezet tab-bought" style="width: 100%">
                <?php foreach ($also_boughts as $product) { ?>
                        <div class="termek">
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>
                <?php } ?>
        </div>
    </div>
<?php } ?>


<?php if ($kapcsolodo_tab) { ?>
    <div id="tab-kapcsolodo" class="tab-content">
       <div class="box-product tab_kornyezet tab-helyettesito tab-kapcsolodo" style="width: 100%">
           <?php if (!empty($products)) { ?>
               <input type="hidden" name="kapcsolodo_csoport" value="<?php echo !empty($product_id) ? $product_id : ''?>">
                <?php foreach ($products as $product) { ?>
                        <div class="termek_tabban" >
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>
                <?php } ?>
             <?php } ?>
        </div>
    </div>
<?php } ?>

<script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/bxslider/jquery.bxslider.css" />
<script>
    $('#tabs_products a').tabs({
        bxSlidesOn: <?php echo isset($beallitas_termek_aloldal['korhinta']) ? 1 : 0?>,
        //minSlides:  <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 2?>,
        minSlides:  2,
        maxSlides:  <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 3?>,
        slideWidth: <?php echo isset($beallitas_termek_aloldal['korhinta_width']) ? $beallitas_termek_aloldal['korhinta_width'] : 260?>,
        slideMargin: <?php echo isset($beallitas_termek_aloldal['korhinta_margin']) ? $beallitas_termek_aloldal['korhinta_margin'] : 20?>,
        randomStart: <?php echo isset($beallitas_termek_aloldal['korhinta_random']) ? "true" : "false" ?>,
        autoHidePager: true /*  Eltűnteti a nyilakat, ha minSlides értékénél kevesebb kép jelenik meg. */
    });
</script>