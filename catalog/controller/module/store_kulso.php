<?php   
class ControllerModuleStoreKulso extends Controller {
	protected function index($setting) {
		$status = true;
		$this->load->model('setting/store');
		$results = $this->model_setting_store->getStores();
		if (!$results) {
			$status = false;
		}

		if (isset($this->session->data['sessionba']) && $this->session->data['sessionba'] == 'valasztott') {
			$status= false;
		}

		if ($this->config->get('store_kulso_admin')) {
			$this->load->library('user');
		
			$this->user = new User($this->registry);
			
			$status = $this->user->isLogged();
		}
		
		if ($status) {
			$this->language->load('module/store_kulso');
			
			$this->data['heading_title'] = $this->language->get('heading_title');
			
			$this->data['text_store'] = $this->language->get('text_store');

            $this->data['text_enter'] = $this->language->get('text_enter');
			
			$this->data['store_id'] = $this->config->get('config_store_id');

            $this->data['store_text_status'] = $this->config->get('store_kulso_text');

			$this->load->model('tool/image');

            $this->data['slider']['slider'] = $this->config->get('store_kulso_slider')  ? $this->config->get('store_kulso_slider') : false ;
            $this->data['slider']['around'] = $this->config->get('store_kulso_around')  ? $this->config->get('store_kulso_around') : false ;
            $this->data['slider']['title'] = $this->config->get('store_kulso_title')  ? $this->config->get('store_kulso_title') : false ;
            $this->data['slider']['speed'] = ($this->config->get('store_kulso_speed') && $this->config->get('store_kulso_speed') <= 1 && $this->config->get('store_kulso_speed') >= 0)  ? $this->config->get('store_kulso_speed') : false ;
            $this->data['slider']['mirror'] = ($this->config->get('store_kulso_mirror'))  ? $this->config->get('store_kulso_mirror') : false ;
            $this->data['slider']['opacity'] = ($this->config->get('store_kulso_opacity') && $this->config->get('store_kulso_opacity') <= 1 && $this->config->get('store_kulso_opacity') >= 0)  ? $this->config->get('store_kulso_opacity') : false ;

			$store_images = $this->model_setting_store->storeImages($results);


			$image_width  = $this->config->get('store_kulso_image_width') ? $this->config->get('store_kulso_image_width') : $this->config->get('config_image_product_width') ;
			$image_height = $this->config->get('store_kulso_image_height') ? $this->config->get('store_kulso_image_height') : $this->config->get('config_image_product_height') ;

            if(isset($image_height) && $image_height) {
                $this->data['image_height'] = $image_height;
            }

            if(isset($image_width) && $image_width) {
                $this->data['image_width'] = $image_width;
            }

			$alap_image = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);

			foreach($store_images as $images) {
				if ($images['store_id'] == 0) {
					if ($images['image'] && file_exists(DIR_IMAGE . $images['image'])) {
						$alap_image = $this->model_tool_image->resize($images['image'], $image_width, $image_height);
					}
					break;
				}
			}



			$this->data['stores'] = array();
			
			$this->data['stores'][] = array(
				'store_id' 	=> 0,
				'name'     	=> $this->config->get('config_store_name'),
				'url'      	=> HTTP_SERVER . 'index.php?route=common/home&sessionba=valasztott',
				'image'		=> $alap_image
			);
			


			foreach ($results as $result) {
				$alap_image = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);

				foreach($store_images as $images) {
					if ($result['store_id'] == $images['store_id']) {
						$alap_image = $images['image'];
						if ($alap_image && file_exists(DIR_IMAGE . $alap_image)) {
							$alap_image = $this->model_tool_image->resize($alap_image, $image_width, $image_height);
						} else {
							$alap_image = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);
						}
						break;
					}
				}
				$this->data['stores'][] = array(
					'store_id' => $result['store_id'],
					'name'     => $result['name'],
					'url'      => $result['url'] . 'index.php?route=common/home&sessionba=valasztott',
					'image'		=> $alap_image
				);
			}
	
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/store_kulso.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/module/store_kulso.tpl';
			} else {
				$this->template = 'default/template/module/store_kulso.tpl';
			}
			
			$this->render();
		}
	}
}
?>