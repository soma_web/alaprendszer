<?php
$_['korhinta']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta',
    'default'           => "1",
    'heading'           => 'Körhinta'
);

$_['korhinta_darab']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_darab',
    'size'              => "2",
    'default'           => "3",
    'heading'           => "Mejelenít"
);
$_['korhinta_width']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_width',
    'size'              => "2",
    'default'           => "260",
    'heading'           => "Termék szélesség"
);
?>