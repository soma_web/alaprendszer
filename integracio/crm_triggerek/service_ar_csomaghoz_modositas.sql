begin
    UPDATE vtiger_pricebookproductrel SET listprice = NEW.unit_price WHERE productid=NEW.serviceid;

    IF NEW.discontinued = 1 THEN
      insert into product_to_webshop set
            hivo_tabla = "vtiger_service",
            productid = NEW.serviceid,
            hivo_esemeny = "modositas";
    END IF;
END