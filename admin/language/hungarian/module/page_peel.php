<?php

// Heading
$_['heading_title']       = 'Szamárfül';

// Text
$_['text_module']         = 'Modulok';
$_['text_heading_title']  = 'Szamárfül';
$_['text_success']        = 'Sikeresen módosította a Szamárfül modult!';;
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_browse']         = 'Fájlok tallózása';
$_['text_clear']          = 'Kép törlése';
$_['text_image_manager']  = 'Képkezelő';

// Entry
$_['entry_small_image']   = 'Kis kép:<br />75 X 75';
$_['entry_big_image']     = '<span class="required">*</span> Nagy kép:<br />500 X 500';
$_['entry_link']          = 'Link / Új Ablak';
$_['entry_new_window']    = 'Megnyitás<br /> Új ablakban?';
$_['entry_activate_link'] = 'Aktiválás csak<br /> erre a linkre:';
$_['entry_layout']        = 'Megjelenés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';

// Error
$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága módosítani a Szamárfül modult!';
$_['error_small_image']   = 'Válasszon kis képet';
$_['error_big_image']     = 'Válasszon nagy képet';
?>