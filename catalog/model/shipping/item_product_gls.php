<?php
class ModelShippingItemProductGls extends Model {
    function getQuote($address, $fizetesek="", $szallitasok=false) {
		$this->load->language('shipping/item_product_gls');
        $this->load->model('setting/setting');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('item_product_gls_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
		
		if (!$this->config->get('item_product_gls_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		$method_data = array();
	
		if ($status) {

            $fizetesi_mod = array();

            if ($fizetesek){
                foreach($fizetesek as $value) {
                    if ( $this->config->get("item_product_gls_payment_".$value['code']) &&  $this->config->get("item_product_gls_payment_".$value['code']) == 1){
                        $fizetesi_mod[] = array(
                            'fizetesi_mod' => $value['code']
                        );
                    }
                }
            }

            $this->load->model('setting/extension');
            $szallitasok = $this->model_setting_extension->getExtensions('shipping');
            $szallitasi_mod = array();

            if ($szallitasok){
                foreach($szallitasok as $value) {
                    if ( $this->config->get("item_product_gls_shipping_".$value['code']) &&  $this->config->get("item_product_gls_shipping_".$value['code']) == 1){
                        $szallitasi_mod[] = array(
                            'szallitasi_mod' => $value['code']
                        );
                    }
                }
            }


			$quote_data = array();
            if ($this->config->get('item_product_gls_tax_class_id') > 0) {
                $query_tax = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$this->config->get('item_product_gls_tax_class_id') . "'");
            }
            $ado_title = isset($query_tax->row['title']) ? $query_tax->row['title'] :"";
            $language_id = $this->config->get('config_language_id');

            $megjelenit_gls = $this->config->get('megjelenit_gls') ? $this->config->get('megjelenit_gls') : 0;
            if(isset($megjelenit_gls) && $megjelenit_gls == 1) {
                $gls['item_product_gls_status'] = $this->config->get('item_product_gls_gls_csomagpont') ? $this->config->get('item_product_gls_gls_csomagpont') : 0;
            } else {
                $gls['item_product_gls_status'] = "0";
            }

            $szallitasi_dij = $this->getProductShippingPrice();

            if (!$szallitasi_dij) return false;


      		$quote_data['item_product_gls'] = array(
        		'code'              => 'item_product_gls.item_product_gls',
        		'title'             => $this->config->get('item_product_gls_text_'.$language_id) != null ? $this->config->get('item_product_gls_text_'.$language_id) :  $this->language->get('text_description'),
                'gls'               => $gls,
                'fizetesi_mod'      => $fizetesi_mod,
                'szallitasi_mod'    => $szallitasi_mod,
        		'cost'              => $szallitasi_dij,
         		'tax_class_id'      => $this->config->get('item_product_gls_tax_class_id'),
				'text'              => $this->currency->format($this->tax->calculate($szallitasi_dij, $this->config->get('item_product_gls_tax_class_id'), $this->config->get('config_tax'))),
                'netto_text'        => $this->currency->format($szallitasi_dij),
                'ado_title'         => $ado_title,
                'mail'              => $this->config->get('item_product_gls_mail')

            );

      		$method_data = array(
        		'code'              => 'item_product_gls',
        		'title'             => $this->config->get('item_product_gls_header_'.$language_id) != null ? $this->config->get('item_product_gls_header_'.$language_id) :  $this->language->get('text_title'),
                'fizetesi_mod'      => $fizetesi_mod,
                'szallitasi_mod'    => $szallitasi_mod,
        		'quote'             => $quote_data,
				'sort_order'        => $this->config->get('item_product_gls_sort_order'),
        		'error'             => false
      		);
		}
	
		return $method_data;
	}


    public function getProductShippingPrice() {
        $products = $this->cart->getProducts();
        $egyedi_szallitasi_dij = false;
        foreach ($products as $product) {
            if ($product['egyedi_szallitas'] == 1 ) {
                if (!$egyedi_szallitasi_dij) {
                    $egyedi_szallitasi_dij = $product['egyedi_szallitasi_dij'];
                } else {
                    if ($this->config->get('item_product_gls_magas_alacsony') == 1) {
                        if ( $product['egyedi_szallitasi_dij'] > $egyedi_szallitasi_dij) {
                            $egyedi_szallitasi_dij = $product['egyedi_szallitasi_dij'];
                        }
                    } else {
                        if ( $product['egyedi_szallitasi_dij'] < $egyedi_szallitasi_dij) {
                            $egyedi_szallitasi_dij = $product['egyedi_szallitasi_dij'];
                        }
                    }
                }
            }
        }
        return $egyedi_szallitasi_dij;
    }
}
?>