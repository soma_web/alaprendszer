<?php

// Heading
$_['heading_title']      = 'PayPal Website Payment Pro';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Siker: A PayPal Website Payment Pro fiók részleteinek módosítása megtörtént!';
$_['text_pp_pro']        = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Engedély';
$_['text_sale']          = 'Eladás';

// Entry
$_['entry_username']     = 'API Felhasználónév:';
$_['entry_password']     = 'API Jelszó:';
$_['entry_signature']    = 'API Aláírás:';
$_['entry_test']         = 'Teszt Mód:<br /><span class="help">Használja az élő vagy a tesztelő (sandbox) átjáró szervert a tranzakció feldolgozásához?</span>';
$_['entry_transaction']  = 'Tranzakció Módszer:';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';
$_['entry_total']        = 'Összeg (minimum összeg a tranzakcióhoz):';

// Error
$_['error_permission']   = 'Figyelmeztetés: Az PayPal Website Payment Pro történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_username']     = 'API Felhasználónév szükséges!';
$_['error_password']     = 'API Jelszó szükséges!';
$_['error_signature']    = 'API Aláírás szükséges!';
?>