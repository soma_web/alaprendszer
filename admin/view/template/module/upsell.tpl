<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
   <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
   <?php } ?>
</div>
<?php if ($error_warning) { ?>
   <div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>

<div class="box">
   <div class="heading">
       <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
       <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
   </div>


   <div class="content">
       <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
           <table class="form">
               <tr>
                   <td><?php echo $entry_status; ?></td>
                   <td>
                       <select name="upsell[status]">
                           <?php if (isset($upsell['status']) &&  $upsell['status']) { ?>
                               <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                               <option value="0"><?php echo $text_disabled; ?></option>
                           <?php } else { ?>
                               <option value="1"><?php echo $text_enabled; ?></option>
                               <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                           <?php } ?>
                       </select>
                   </td>
               </tr>

               <tr>
                   <td><?php echo $entry_fejlec_latszik; ?></td>
                   <td>
                       <?php if (isset($upsell['fejlec']) && $upsell['fejlec']) { ?>
                           <input type="radio" name="upsell[fejlec]" value="1" checked="checked" />
                           <?php echo $text_yes; ?>
                           <input type="radio" name="upsell[fejlec]" value="0" />
                           <?php echo $text_no; ?>
                       <?php } else { ?>
                           <input type="radio" name="upsell[fejlec]" value="1" />
                           <?php echo $text_yes; ?>
                           <input type="radio" name="upsell[fejlec]" value="0" checked="checked" />
                           <?php echo $text_no; ?>
                       <?php } ?>
                   </td>
               </tr>

               <tr>
                   <td><?php echo $entry_megfelelo_termekek; ?></td>
                   <td>
                       <select name="upsell[termekek]" onchange="automatic(this)">
                           <?php for($i=0; $i<=7; $i++) { ?>
                               <?php $selected = isset($upsell['termekek']) && $upsell['termekek'] == $i ? 'selected="selected"' : "" ?>
                               <option value="<?php echo $i;?>" <?php echo $selected?>><?php echo $termek_csoportok[$i]; ?></option>
                           <?php } ?>
                       </select>
                  </td>
               </tr>

               <tr id="automatikus">
                   <td><?php echo $entry_automata_bellitas; ?></td>
                   <td>
                       <table>
                           <?php for($i=2; $i<=7; $i++) { ?>
                               <tr>
                                   <td>
                                       <?php echo $termek_csoportok[$i]; ?>:
                                   </td>
                                   <td>
                                       <input type="text" name="upsell[csoport_sorrend][<?php echo $i?>]"
                                              value="<?php echo (isset($upsell['csoport_sorrend'][$i]) ? $upsell['csoport_sorrend'][$i] : 0);?>" size="2">
                                   </td>
                               </tr>
                           <?php } ?>
                       </table>
                   </td>
               </tr>

               <?php $display = isset($upsell['termekek']) && $upsell['termekek'] != 2 ? "display: none" : "" ?>
               <tr class="also_bought" style="<?php echo $display;?>">
                   <td><?php echo $entry_bought_relacio; ?></td>
                   <td>
                       <select name="upsell[also_bought]">
                           <?php if (isset($upsell['also_bought']) &&  $upsell['also_bought'] == "&gt;=") { ?>
                               <option value=">=" selected="selected"> >= </option>
                               <option value="="> = </option>
                               <option value="<="> <= </option>

                           <?php } elseif (isset($upsell['also_bought']) &&  $upsell['also_bought'] == "=") { ?>
                               <option value=">="> >= </option>
                               <option value="="selected="selected"> = </option>
                               <option value="<="> <= </option>

                           <?php } elseif (isset($upsell['also_bought']) &&  $upsell['also_bought'] == "&lt;=") { ?>
                               <option value=">="> >= </option>
                               <option value="="> = </option>
                               <option value="<="selected="selected" > <= </option>

                           <?php } else { ?>
                               <option value=">="> >= </option>
                               <option value="="> = </option>
                               <option value="<="> <= </option>
                           <?php } ?>
                       </select>
                   </td>
               </tr>

               <tr class="also_bought" style="<?php echo $display;?>">
                   <td><?php echo $entry_bought_order_status; ?></td>

                   <td>
                       <select name="upsell[order_status_id]">
                           <?php foreach($orders_statuses as $order_status){?>
                               <?php if ( $upsell['order_status_id'] == $order_status['order_status_id']) { ?>
                                   <option value="<?php echo $order_status['order_status_id']; ?>" selected><?php echo $order_status['name']; ?></option>
                               <?php } else { ?>
                                   <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                               <?php } ?>
                           <?php } ?>
                       </select>
                   </td>
               </tr>


               <tr>
                   <td><?php echo $entry_max_termek; ?></td>
                   <td>
                       <input type="text" name="upsell[limit]" value="<?php echo isset($upsell['limit']) ? $upsell['limit'] : 0?>" size="2">
                   </td>
               </tr>
           </table>



           <div id="racs_altalanos">
               <a onclick="addCsoport();" class="button">Csoport hozzáadása</a>

               <?php if (isset($upsell_beallitas_upsell_csoportok) && $upsell_beallitas_upsell_csoportok) {?>
               <?php $elemszam = 0?>
               <?php for($i=0; count($upsell_beallitas_upsell_csoportok_kiegeszito) > $i; $i++) {?>
               <?php $key_aktualis = $upsell_beallitas_upsell_csoportok_kiegeszito[$i];?>
               <?php $key_kovetkezo = isset($upsell_beallitas_upsell_csoportok_kiegeszito[$i+1]) ? $upsell_beallitas_upsell_csoportok_kiegeszito[$i+1] : $upsell_beallitas_upsell_csoportok_kiegeszito[$i];?>
               <?php $csoportok = $upsell_beallitas_upsell_csoportok[$key_aktualis]?>

               <?php $tomb_mostani = explode("_",$key_aktualis);?>
               <?php $tomb_kovetkezo = explode("_",$key_kovetkezo);?>
               <?php $kovetkezo_elemszam = count($tomb_kovetkezo);?>

               <?php if ($elemszam < count($tomb_mostani) || true) {?>
                   <?php $elemszam = count($tomb_mostani);?>
               <?php } ?>

               <div class="csoportok sortitem" id="csoport_<?php echo $key_aktualis?>" style="padding-left: <?php echo (strlen($key_aktualis)+8)?>px" >
                   <span class="sorthandle"><img src="view/image/icon-drag-drop.png"></span>
                   <div class="csoportok_nyito"  onclick="csikiCsuki(this)">
                       <?php $sor = true?>
                       <?php foreach($tomb_mostani as $value) {?>
                           <?php if ($sor){?>
                               <span>Sor: <b><?php echo $value?></b>  </span>
                               <?php $sor = false;?>
                           <?php } else {?>
                               <span>Oszl.: <b><?php echo $value?></b>  </span>
                               <?php $sor = true;?>
                           <?php }?>
                       <?php } ?>

                       <?php
                       $inttomb = array();
                       foreach($tomb_mostani as $value){
                           $inttomb[] = (int)$value;
                       }
                       ?>
                       <a onclick="$('#csoport<?php echo "_".$key_aktualis;?>').remove();" class="button"><?php echo $button_remove; ?></a>
                       <a onclick="addCsoport(<?php echo json_encode($inttomb)?>);" class="button">Új csoport</a>
                       <a onclick="addPaste('<?php echo $key_aktualis;?>');" class="button beillesztes">Beillesztés</a>
                       <a onclick="copyRemove('<?php echo $key_aktualis;?>');" id="copyremove_<?php echo $key_aktualis;?>" class="button copyremove" style="display: none">Másolás törlés</a>

                       <a style="text-decoration: none; margin-right: 40px;">
                           <?php echo $entry_status?>
                           <?php if (isset($upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['status']) && $upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['status'] == 1) {?>
                               <input type="radio" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][default][status]" value="1" checked="checked" />
                               <input type="radio" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][default][status]" value="0"  />
                           <?php } else {?>
                               <input type="radio" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][default][status]" value="1"  />
                               <input type="radio" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][default][status]" value="0" checked="checked" />
                           <?php } ?>
                       </a>
                       <a class="input_tol_ig">Felbontás (tól -ig):
                           <input type="text" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][default][width_tol]" size="3"
                                  value="<?php echo isset($upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['width_tol']) ? $upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['width_tol'] : 0; ?>"/>-
                           <input type="text" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][default][width_ig]" size="3"
                                  value="<?php echo isset($upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['width_ig']) ? $upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['width_ig'] : 3000; ?>" />
                       </a>
                       <a class="tartalom" style="margin-right: 30px;">
                           <?php if(isset($upsell_beallitas_upsell_csoportok[$key_aktualis]['templates'])) { ?>
                               <img src="view/image/tartalom1.png">
                           <?php } ?>
                       </a>
                   </div>

                   <div class="csoport_beallitasok">
                       <div>
                           <div class="scrollbox">
                               <?php $class = 'odd'; ?>
                               <?php foreach ($upsell_beallitas_upsell_csoportok_files as $fileskey=>$beallitdoboz) { ?>
                                   <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                   <div class="<?php echo $class; ?>">
                                       <!--<span class="template_neve">-->
                                           <?php if (isset($upsell_beallitas_upsell_csoportok[$key_aktualis]['templates'][$fileskey]['status']) && $upsell_beallitas_upsell_csoportok[$key_aktualis]['templates'][$fileskey]['status'] == 1) { ?>
                                               <?php $disabled = ""?>
                                               <input class="valaszto" onclick="elrejt(this);" type="checkbox" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][status]" value="1" checked="checked" />
                                               <span class='szoveg'><?php echo $fileskey; ?></span>
                                           <?php } else { ?>
                                               <input class="valaszto" onclick="elrejt(this);" type="checkbox" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][status]" value="1" />
                                               <span class='szoveg'><?php echo $fileskey; ?></span>
                                               <?php $disabled = "disabled"?>
                                           <?php } ?>
                                       <!--</span>-->

                                       <?php if (count($beallitdoboz) > 0) {?>
                                           <?php
                                           $key = $fileskey;
                                           $melyik_doboz = "upsell_beallitas_upsell_csoportok[$key_aktualis][templates]"
                                           ?>
                                           <span>
                                                <?php $doboz = isset($upsell_beallitas_upsell_csoportok[$key_aktualis]['templates'][$fileskey]) ? $upsell_beallitas_upsell_csoportok[$key_aktualis]['templates'][$fileskey] : ""; ?>
                                               <?php foreach ($beallitdoboz as $keydoboz=>$tomb) { ?>
                                                   <?php if(count($tomb) > 1) { ?>
                                                       <?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>
                                                   <?php } ?>
                                               <?php } ?>
                                                </span>
                                       <?php } ?>

                                       <?php if (isset($upsell_beallitas_upsell_csoportok[$key_aktualis]['templates'][$fileskey]['sort_order'])) { ?>
                                           <input <?php echo $disabled;?> class="sorrend" type="text" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][sort_order]" value="<?php echo $upsell_beallitas_upsell_csoportok[$key_aktualis]['templates'][$fileskey]['sort_order']?>" size="2"/>
                                       <?php } else { ?>
                                           <input <?php echo $disabled;?> class="sorrend" type="text" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][templates][<?php echo $fileskey?>][sort_order]" value="0" size="2"/>
                                       <?php } ?>

                                   </div>
                               <?php } ?>
                           </div>
                           <br>
                           <a onclick="$(this).parent().find(':checkbox').attr('checked', true);
                                            $(this).parent().find('input, select').attr('disabled',false);"><?php echo $text_select_all; ?>
                           </a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);
                                                   $(this).parent().find('input, select').attr('disabled',true);
                                                   $('.valaszto').attr('disabled',false)"><?php echo $text_unselect_all; ?>
                           </a>
                           <br>
                       </div>
                       <div class="kijeloles">
                           <a onclick="addCopy('<?php echo $key_aktualis;?>');" class="button masolas">Kijelölés másolásra</a>
                                <span> <b>CSS display érték:</b> (pl.: inline-block: )
                                    <input type="text" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][default][display]" size="8"
                                           value="<?php echo isset($upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['display']) ? $upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['display'] : "block"; ?>" />
                                </span>
                                <span> <b>CSS class neve:</b>
                                    <input type="text" name="upsell_beallitas_upsell_csoportok[<?php echo $key_aktualis?>][default][class]" size="18"
                                           value="<?php echo isset($upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['class']) ? $upsell_beallitas_upsell_csoportok[$key_aktualis]['default']['class'] : ""; ?>" />
                                </span>
                       </div>
                   </div>
                   <div class="div_csoport" style="position: relative">

                   <?php if ($kovetkezo_elemszam <= $elemszam ) {
                       for($j=$kovetkezo_elemszam; $j<= $elemszam; $j++){
                           echo "</div>";
                       }
                   } ?>
                   <?php if ($kovetkezo_elemszam <= $elemszam ) {
                       for($j=$kovetkezo_elemszam; $j<= $elemszam; $j++){
                           echo "</div>";
                       }
                   } ?>
                   <?php } ?>
                   <?php for($j=1; $j< $elemszam; $j++){
                       echo "</div>";
                   } ?>
                   <?php } ?>
               </div>

       </form>
   </div>

</div>
</div>

<?php $modul_name = "upsell_beallitas_upsell_csoportok"?>
<?php $csoportok_files = &$upsell_beallitas_upsell_csoportok_files?>
<?php //$hozzafuz = "upsell_altalanos"?>
<?php $hozzafuz = "racs_altalanos"?>

<?php include_once(DIR_TEMPLATE. "design/modularis_js.php")?>

<script>
   function automatic(para) {
        if ($(para).val() == 2) {
            $(".also_bought").fadeIn(1000);
        } else {
            $(".also_bought").fadeOut(1000);
        }
    }
</script>



<script>
/*function addCsoport(tombatad){

    var html = '<div class="csoportok" id="';
    var new_div_id = "csoport";
    var tomb = "[";
    for(i in tombatad){
        tomb += tombatad[i]+",";
        new_div_id += '_'+tombatad[i];
    }
    for (var kii=0; $("#"+new_div_id+"_"+kii).length > 0; kii++){
    }
    var old_div_id = new_div_id;
    new_div_id += '_'+kii;
    tomb += kii+"]";

    html += new_div_id+'"';

    html += ' style="padding-left: '+new_div_id.length+'px">';

    html += '<div class="csoportok_nyito"  onclick="csikiCsuki(this)">';
    var paratlan = true;
    for(i in tombatad){
        html += "<span>";
        if (paratlan) {
            html += "Sor: ";
            paratlan = false;
        } else {
            html += "Oszl.: ";
            paratlan = true;
        }
        html += '<b>'+tombatad[i]+'</b>';
        html += '</span>';
    }
    html += '<span>';
    if (paratlan) {
        html += "Sor: ";
    } else {
        html += "Oszl.: ";
    }
    html += '<b>'+kii+'</b>';
    html += "</span>";

    var new_kulcs = new_div_id.substr(8);
    var new_kulcs_past = "\'"+new_div_id.substr(8)+"\'";
    new_div_id = "#"+new_div_id;

    html += '<a onclick="$(\''+new_div_id+'\').remove();" class="button"><?php echo $button_remove ?></a>';
    html += '<a onclick="addCsoport('+tomb+')" class="button">Új csoport</a>';
    html += '<a onclick="addPaste('+new_kulcs_past+')" class="button beillesztes">Beillesztés</a>';

    html += '<a style="text-decoration: none; margin-right: 40px;">';
    html += 'Állapot: ';
    html += '<input type="radio" name="upsell_beallitas_upsell_csoportok['+new_kulcs+'][default][status]" value="1" />';
    html += '<input type="radio" name="upsell_beallitas_upsell_csoportok['+new_kulcs+'][default][status]" value="0" checked="checked"/>';
    html += '</a>';
    html += '<a class="input_tol_ig">Felbontás (tól -ig):';
    html += '<input type="text" name="upsell_beallitas_upsell_csoportok['+new_kulcs+'][default][width_tol]" size="3" value="0"/>- ';
    html += '<input type="text" name="upsell_beallitas_upsell_csoportok['+new_kulcs+'][default][width_ig]" size="3" value="3000" />';
    html += '</a>';
    html += '</div>';


    html += '<div class="csoport_beallitasok">';
    html += '<div>';
    html += '<div class="scrollbox">';
    <?php $class = 'odd'; ?>
    <?php foreach ($upsell_beallitas_upsell_csoportok_files as $fileskey=>$beallitdoboz) { ?>
    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
    html += '<div class="<?php echo $class; ?>">';
    html += '<input class="valaszto" onclick="elrejt(this);" type="checkbox" name="upsell_beallitas_upsell_csoportok['+new_kulcs+'][templates][<?php echo $fileskey?>][status]" value="1" />';
    html += '<?php echo $fileskey; ?>';

    <?php if (count($beallitdoboz) > 0) {?>
    <?php
    $key = $fileskey;
    $melyik_doboz = "upsell_beallitas_upsell_csoportok[NULL_119][templates]"
    ?>
    html += '<span>';
    <?php $disabled = "disabled"?>

    <?php foreach ($beallitdoboz as $keydoboz=>$tomb) { ?>
    <?php if(count($tomb) > 1) { ?>
    html += '<?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>';
    <?php } ?>
    <?php } ?>
    html += '</span>';
    <?php } ?>
    html += '<input disabled class="sorrend" type="text" name="upsell_beallitas_upsell_csoportok['+new_kulcs+'][templates][<?php echo $fileskey?>][sort_order]" value="0" size="2"/>';
    html += '</div>';
    <?php } ?>
    html += '</div>';

    html += '<br>';
    html += '<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);$(this).parent().find(\'input, select\').attr(\'disabled\',false)"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false); $(this).parent().find(\'input, select\').attr(\'disabled\',true); $(\'.valaszto\').attr(\'disabled\',false)"><?php echo $text_unselect_all; ?></a>';
    html += '<br>';
    html += '</div>';

    html += '<div class="kijeloles">';
    html += '<a onclick="addCopy('+new_kulcs_past+');" class="button masolas">Kijelölés másolásra</a>';
    html += '<span> <b>CSS display érték:</b> (pl.: inline-block: )';
    html += '<input type="text" name="upsell_beallitas_upsell_csoportok['+new_kulcs+'][default][display]" size="8" value="block"/>';
    html += '</span>';
    html += '<span> <b>CSS class neve:</b>';
    html += '<input type="text" name="upsell_beallitas_upsell_csoportok['+new_kulcs+'][default][class]" size="18" value=""/>';
    html += '</span>';
    html += '</div>';
    html += '</div>';
    html += '</div>';



    html = html.replace(/NULL_119/g, new_kulcs);
    if ( $("#"+old_div_id).length > 0) {
        $("#"+old_div_id).append(html);
    } else {
        $("#upsell_altalanos").append(html);
    }
}

function addCopy(para){
    var eredmeny = $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[checked=checked],[type=text]');
    tarolo = new Array();

    for (var i=0; eredmeny.length > i; i++){
        tarolo[i] = eredmeny[i].name;
    }
    tarolo[i]   = $("#csoport_"+para+ " > .csoport_beallitasok .kijeloles > span:nth(0) > input").val();
    tarolo[i+1] = $("#csoport_"+para+ " > .csoport_beallitasok .kijeloles > span:nth(1) > input").val();

    $(".masolas").css("display","none");
    $("#csoport_"+para+ " > .csoportok_nyito").css("color","blue");
    $(".beillesztes").fadeIn(300);
    $("#csoport_"+para+ " > .csoportok_nyito > .beillesztes").css("display","none");
    $('.csoport_beallitasok').slideUp(300);
}

function addPaste(para){
    var eredmeny = $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input");
    for (var i=0; eredmeny.length > i; i++){
        for (var j=0; tarolo.length-1 > j; j++) {
            var type = $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("type");
            if (eredmeny[i].name.substr(eredmeny[i].name.indexOf("]")+1) == tarolo[j].substr(tarolo[j].indexOf("]")+1)) {

                if (type == "checkbox"){
                    $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("checked",true);
                } else if(type == "text") {

                    var tarolt_kulcs    = tarolo[j].substring(tarolo[j].indexOf("[")+1,tarolo[j].indexOf("]"));

                    var ertek = $("#csoport_"+tarolt_kulcs+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+tarolo[j]+'"]').val();
                    $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("value",ertek);
                }
                break;

            } else {
                if (type == "checkbox"){
                    $("#csoport_"+para+ " > .csoport_beallitasok .scrollbox input").filter('[name="'+eredmeny[i].name+'"]').attr("checked",false);
                }
            }
        }
    }

    $("#csoport_"+para+ " > .csoport_beallitasok .kijeloles > span:nth(0) > input").val(tarolo[tarolo.length-2]);
    $("#csoport_"+para+ " > .csoport_beallitasok .kijeloles > span:nth(1) > input").val(tarolo[tarolo.length-1]);

    $(".beillesztes").css("display","none");
    $(".masolas").fadeIn(300);
    $(".csoportok_nyito").css("color","#000");

    if ($("#csoport_"+para+ " > .csoport_beallitasok").css("display") == "none") {
        $('.csoport_beallitasok').css("display","none");
        $("#csoport_"+para+ " > .csoport_beallitasok").slideDown(500);
    }
}

function csikiCsuki(para){

    if (this.event.target == para || this.event.target.localName == "span") {
        if ($(para).next().css("display") != "none") {
            $(para).next().slideUp(500);
            $(para).removeClass("active");

        } else {
            $('.csoportok_nyito').removeClass("active");
            $('.csoport_beallitasok').hide();
            $(para).next().slideDown(500);
            $(para).addClass("active");

        }
    }
}

function elrejt(para) {
    var sort_name = para.name.substr(0,para.name.length-8)+'[sort_order]';

    if ($(para).prop("checked")) {
        $(".scrollbox input[name='"+sort_name+"']").parent().find("input, select").attr("disabled",false)

    } else {
        $(".scrollbox input[name='"+sort_name+"']").parent().find("input, select").attr("disabled",true)
    }
    $(para).attr("disabled",false)
}*/



</script>

<?php echo $footer; ?>