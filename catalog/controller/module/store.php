<?php   
class ControllerModuleStore extends Controller {
	protected function index($setting) {
		$status = true;
		
		if ($this->config->get('store_admin')) {
			$this->load->library('user');
		
			$this->user = new User($this->registry);
			
			$status = $this->user->isLogged();
		}
		
		if ($status) {
			$this->language->load('module/store');
			
			$this->data['heading_title'] = $this->language->get('heading_title');
			
			$this->data['text_store'] = $this->language->get('text_store');
			
			$this->data['store_id'] = $this->config->get('config_store_id');

			$this->load->model('setting/store');
			$this->load->model('tool/image');

			$results = $this->model_setting_store->getStores();

			$store_images = $this->model_setting_store->storeImages($results);



			foreach($store_images as $images) {
				if ($images['store_id'] == 0) {
					if ($images['image'] && file_exists(DIR_IMAGE . $images['image'])) {
						$alap_image = $this->model_tool_image->resize($images['image'], $this->config->get('store_image_width'), $this->config->get('store_image_height'));
					} else {
						$alap_image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('store_image_width'), $this->config->get('store_image_height'));
					}
					break;
				}
			}


			$this->data['stores'] = array();
			
			$this->data['stores'][] = array(
				'store_id' 	=> 0,
				'name'     	=> $this->language->get('text_default'),
				'url'      	=> HTTP_SERVER . 'index.php?route=common/home',
				'image'		=> $alap_image
			);
			


			foreach ($results as $result) {
				foreach($store_images as $images) {
					if ($result['store_id'] == $images['store_id']) {
						$alap_image = $images['image'];
						break;
					}
				}
				$this->data['stores'][] = array(
					'store_id' => $result['store_id'],
					'name'     => $result['name'],
					'url'      => $result['url'] . 'index.php?route=common/home',
					'image'		=> $alap_image
				);
			}
	
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/store.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/module/store.tpl';
			} else {
				$this->template = 'default/template/module/store.tpl';
			}
			
			$this->render();
		}
	}
}
?>