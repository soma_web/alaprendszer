<?php
class ModelAccountElhelyezkedesAlcsoport extends Model {

    public function getAlcsoportElhelyezkedesek() {

        $vissza = array();
        $elhelyezkedesek = $this->config->get("elhelyezkedes_alcsoport");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['status'] == 1) {
                    $vissza[] = $value;
                }
            }
        }

        return $vissza;
    }
}
?>