<?php
// Text
$_['text_subject']          = '%s - Új kérdés';
$_['text_new_question']     = 'Új kérdése érkezett erről a termékről: <a href="%s" style="color:#069;font-weight:bold;">%s</a>.';
$_['text_question_detail']  = 'Kérdés részletei';
$_['text_question']         = 'Kérdés:';
$_['text_question_author']  = 'Kérdező:';
$_['text_email']            = 'E-mail címe:';
$_['text_phone']            = 'Telefonszáma:';
$_['text_custom']           = 'Egyéb:';
$_['text_customer_name']    = 'Vásárló:';
$_['text_anonymous']        = 'Névtelen';
$_['text_ip']               = 'IP cím:';
//$_['text_powered_by']       = 'powered by';
?>
