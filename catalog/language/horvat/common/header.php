<?php

// Text
$_['text_home']     = 'Početna stranica';
$_['text_wishlist'] = 'Kívánságlista (%s)';
$_['text_wishlist_short'] = 'Kívánságlista';
$_['text_cart']     = 'Kosár';
$_['text_items']    = '%s tétel - %s';
$_['text_search']   = 'Keresés:';
$_['text_blogs']           = 'Blogs';
$_['text_compare']  = 'Összehasonlítás (%s)';

/*$_['text_welcome']  = '<a style="color: #fff" href="%s">Bejelentkezés</a>';
$_['text_kijelentkezes']  = '<a style="color: #fff" href="%s">Kijelentkezés</a>';
$_['text_regisztral']  = '<a style="color: #999999" href="%s">Regisztráció</a>';
$_['text_logged']   = '<span style="font-size: 13px; position: relative; top: 4px">Bejelentkezve:</span> <b><a style="font-size: 19px" href="%s">%s</a></b>';
$_['text_honlapom']  = '<a style="color: #fff" href="%s" title="Honlapunk">További információk</a>';*/


$_['text_maganszemelyknek']  = 'Magánszemélyeknek';
$_['text_welcome']  = '<a href="%s">Bejelentkezés</a>';
$_['text_welcome_belep']  = '<a href="%s">Belépés</a>';
$_['text_kijelentkezes']  = '<a href="%s">Kijelentkezés</a>';
$_['text_regisztral']  = '<a href="%s">Regisztráció</a>';
$_['text_regisztral_csalogat']  = '<a href="%s">Regisztráció most!</a>';

$_['text_regisztral_smallest']  = '<a href="%s"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_mreg.png" /></div><div class="jobb_kicsi"><span>Regisztráció</span></div></a>';
$_['text_welcome_smallest']  = '<a href="%s"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_login.png" /></div><div class="jobb_kicsi"><span>Bejelentkezés</span></div></a>';
$_['text_kijelentkezes_smallest']  = '<a href="%s"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_logout.png" /></div><div class="jobb_kicsi"><span>Kijelentkezés</span></div></a>';
$_['text_information_kicsi_smallest']  = '<div class="information_kicsi_smallest"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_info.png" /></div><div class="jobb_kicsi"><span>Információ</span></div></div>';
$_['text_szuro_kicsi_smallest']  = '<div class="information_kicsi_smallest"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_filter.png" /></div><div class="jobb_kicsi"><span>Szűrő</span></div></div>';
$_['text_ajanlat_kicsi_smallest']  = '<a href="%s"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_creg.png" /></div><div class="jobb_kicsi"><span>Ajánlat</span></div></a>';
$_['text_cart_kicsi_smallest']  = '<div class="cart_kicsi_smallest"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_cart.png" /></div><div class="jobb_kicsi"><span>Kosár</span></div></div>';

$_['text_welcome_kicsi']  = '<a href="%s"><img src="catalog/view/theme/kupon/image/responsive/pos_r_login.png" /></a>';
$_['text_kijelentkezes_kicsi']  = '<a href="%s"><img src="catalog/view/theme/kupon/image/responsive/pos_r_logout.png" /></a>';
$_['text_regisztral_kicsi']  = '<a href="%s"><img src="catalog/view/theme/kupon/image/responsive/pos_r_mreg.png" /></a>';
$_['text_information_kicsi']  = '<img src="catalog/view/theme/kupon/image/responsive/pos_r_info.png" />';

$_['text_logged']   = '<span style="display: inline">Bejelentkezve:</span> <b><a style="font-size: 17px" href="%s">%s</a></b>';



$_['text_ajanlat_keres']  = 'Árajánlat kérésem';



$_['text_logged_minosegbe']   = '<span style="display: inline">Bejelentkezve:</span> <b><a style="font-size: 17px" href="%s">%s</a></b>';
$_['text_honlapom']  = '<a href="%s" target="_blank" title="Honlapunk">Tovább a weboldalra</a>';

$_['text_account']  = 'Fiók';
$_['text_checkout'] = 'Pénztár';
$_['text_language'] = 'Nyelv';
$_['text_currency'] = 'Pénznem';
$_['text_pricelist']   = 'Árlista';
$_['text_partner']   = 'Partnereinknek';
$_['text_ajanlat']   = '<a href="%s">Töltsd fel ajánlatod</a>';

//$_['text_motto']    = 'Minőségbe csomagolva';
$_['text_motto']    = '&bdquo;Minőségbe csomagolva&rdquo;';
$_['text_reg']    = 'Regisztrációk';
$_['text_belep']    = 'Bejelentkezés';
$_['text_facebook_belep']    = 'Facebook Belépés';
$_['text_meg_nem_reg']  = 'Még nem regisztráltál?';
$_['text_kilep']    = 'Kijelentkezés';
$_['text_arlista']    = 'Árlista';
$_['text_kapcsolat']    = 'Kontakt';
$_['text_termekek']    = 'Termékek';
$_['text_termek']    = 'Termék';
$_['text_oldalterkep']    = 'Oldaltérkép';

$_['text_news']    = 'Hírek';
$_['text_services']    = 'Szolgáltatások';
$_['text_gyik']           = 'Gyakori kérdések';

$_['menu_introduction']      = 'Bemutatkozás';
$_['menu_services']          = 'Szolgáltatások';
$_['menu_applications']      = 'Alkalmazások';
$_['menu_laser_knowledge']   = 'Lézer ismeretek';
$_['menu_contact']           = 'Kontakt';

$_['text_welcome_helios']  = 'Üdvözlöm! Kérem <a href="%s">lépjen be</a> vagy <a href="%s">regisztráljon</a> új fiókot!';
$_['text_logged_helios']   = 'Bejelentkezve <b><a href="%s">%s</a></b> néven. <a href="%s">Kijelentkezés</a>';

$_['text_hirlevel']     = 'Hírlevél';
$_['text_markak']       = 'Forgalmazott márkák';
$_['text_szerviz']      = 'Szervízközpont';
$_['text_uzlet']        = 'Üzleteink';
$_['text_blogreceptek']  = 'Blog-Receptek';

$_['text_webshop']      = 'Webshop';
$_['text_hasznalt']     = 'text_hasznalt';
$_['text_berelheto']    = 'text_berelheto';
$_['text_hirek']        = 'Hírek/Info';

$_['action']            = 'Akciós termékek';

?>
