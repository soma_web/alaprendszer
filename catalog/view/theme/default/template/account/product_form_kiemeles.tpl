<?php echo $header; ?>
    <script>

    function Osszesit(sor) {

        var elhelyezkedes    = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_id]\']");
        var elhelyezkedes_id = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_id]\']");
        var elhelyezkedes_kivalasztva = elhelyezkedes[0].selectedIndex;
        var elhelyezkedes_ar=  elhelyezkedes[0][elhelyezkedes_kivalasztva].getAttribute("simple_ar");
        var elhelyezkedes_netto_ar=  elhelyezkedes[0][elhelyezkedes_kivalasztva].getAttribute("simple_ar_netto");
        var elhelyezkedes_neve=  elhelyezkedes[0][elhelyezkedes_kivalasztva].getAttribute("neve");


        var elhelyezkedes_alcsoport = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_id]\']");
        var elhelyezkedes_alcsoport_kivalasztva = elhelyezkedes_alcsoport[0].selectedIndex;
        var elhelyezkedes_alcsoport_ar=  elhelyezkedes_alcsoport[0][elhelyezkedes_alcsoport_kivalasztva].getAttribute("simple_ar");
        var elhelyezkedes_alcsoport_netto_ar=  elhelyezkedes_alcsoport[0][elhelyezkedes_alcsoport_kivalasztva].getAttribute("simple_ar_netto");
        var elhelyezkedes_alcsoport_neve=  elhelyezkedes_alcsoport[0][elhelyezkedes_alcsoport_kivalasztva].getAttribute("neve");

        var kiemelesek = $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_id]\']");
        var kiemelesek_kivalasztva = kiemelesek[0].selectedIndex;
        var kiemelesek_ar=  kiemelesek[0][kiemelesek_kivalasztva].getAttribute("simple_ar");
        var kiemelesek_netto_ar=  kiemelesek[0][kiemelesek_kivalasztva].getAttribute("simple_ar_netto");
        var kiemelesek_neve=  kiemelesek[0][kiemelesek_kivalasztva].getAttribute("neve");

        var ar_osszesen       = Number(elhelyezkedes_ar)+Number(elhelyezkedes_alcsoport_ar)+Number(kiemelesek_ar);
        var ar_osszesen_netto = Number(elhelyezkedes_netto_ar)+Number(elhelyezkedes_alcsoport_netto_ar)+Number(kiemelesek_netto_ar);


        $("#ar_osszesen"+sor).html("<?php echo $text_osszesen;?>  " +number_format(ar_osszesen)+"<?php echo $penznem?>");

        var datum_tol = $("[name=\'product_elhelyezkedes["+sor+"][datum_tol]\']").val();
        var datum_ig = $("[name=\'product_elhelyezkedes["+sor+"][datum_ig]\']").val();

        var datum_kezdo = new Date(datum_tol);
        var datum_vege  = new Date(datum_ig);

        var timeDiff = Math.abs(datum_vege.getTime() - datum_kezdo.getTime()+1);
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays > 0 ){
        } else {
            diffDays = 0;
        }

        var ar_osszesen_netto_darab = diffDays * ar_osszesen_netto;
        var ar_osszesen_darab       = diffDays * ar_osszesen;


        $("#ar_sor_total"+sor).html("Fizetendő :"+number_format(ar_osszesen_darab)+"<?php echo $penznem?>");

        $("[name=\'product_elhelyezkedes["+sor+"][sor]\']").val(sor);

        $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_neve]\']").val(elhelyezkedes_neve);
        $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_netto_ara]\']").val(elhelyezkedes_netto_ar);
        $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_brutto_ara]\']").val(elhelyezkedes_ar);

        $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_neve]\']").val(elhelyezkedes_alcsoport_neve);
        $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_netto_ara]\']").val(elhelyezkedes_alcsoport_netto_ar);
        $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_brutto_ara]\']").val(elhelyezkedes_alcsoport_ar);

        $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_neve]\']").val(kiemelesek_neve);
        $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_netto_ara]\']").val(kiemelesek_netto_ar);
        $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_brutto_ara]\']").val(kiemelesek_ar);

        $("[name=\'product_elhelyezkedes["+sor+"][netto_ar]\']").val(ar_osszesen_netto);
        $("[name=\'product_elhelyezkedes["+sor+"][brutto_ar]\']").val(ar_osszesen);

        $("[name=\'product_elhelyezkedes["+sor+"][total_netto_ar]\']").val(ar_osszesen_netto_darab);
        $("[name=\'product_elhelyezkedes["+sor+"][total_brutto_ar]\']").val(ar_osszesen_darab);

        var elhelyezes_id=   $("[name=\'product_elhelyezkedes["+sor+"][elhelyezes_id]\']").val();
        var idoszak =   $("[name=\'product_elhelyezkedes["+sor+"][idoszak]\']").val();

        if (elhelyezkedes.val() > 0 || kiemelesek.val() > 0) {
            $('#ures_id'+sor).slideUp(500);
            $('#elhelyezkedes-row'+sor+' .szelesseg').css("border","none");

        }
        if (idoszak > 0) {
            $('#ures_idoszak'+sor).slideUp(500);
            $("[name=\'product_elhelyezkedes["+sor+"][idoszak]\']").css("border","2px inset");

        }
        if (datum_tol > "0000-00-00") {
            $('#ures_datum_kezdo'+sor).slideUp(500);
            $("[name=\'product_elhelyezkedes["+sor+"][datum_tol]\']").css("border","2px inset");

        }

        $.ajax({
            url: 'index.php?route=account/product/elhelyezkedesSzabadHely',
            type: "post",
            data: 'datum_kezdo='                 + datum_tol
                + '&datum_vege='                 + datum_ig
                + '&idoszak='                    + idoszak
                + '&elhelyezes_id='              + elhelyezes_id
                + '&elhelyezkedes_id='           + elhelyezkedes.val()
                + '&elhelyezkedes_alcsoport_id=' + elhelyezkedes_alcsoport.val()
                + '&sor=' + sor
                + '&kiemelesek_id='              + kiemelesek.val(),

            dataType: 'json',

            success: function(json) {
                $('.success, .warning, .error').remove();

                $('#elhelyezkedes-row'+json['sor']+" select" ).css("border","none");

                if (json['error']) {
                    if ( json['error']['elhelyezkedes']) {
                        $('#elhelyezkedes-row'+json['sor'] ).before('<div class="warning" style="display: none;">' + json['error']['elhelyezkedes'] + '</div>');

                        $('.warning').slideDown(500);
                        $('#elhelyezkedes-row'+json['sor']+' .kosar').fadeOut("slow");
                        $("[name=\'product_elhelyezkedes["+json['sor']+"][elhelyezkedes_id]\']").css("border","2px solid #df190a");

                    }
                    if ( json['error']['elhelyezkedes_alcsoport']) {
                        $('#elhelyezkedes-row'+json['sor'] ).before('<div class="warning" style="display: none;">' + json['error']['elhelyezkedes_alcsoport'] + '</div>');

                        $('.warning').slideDown(500);
                        $('#elhelyezkedes-row'+json['sor']+' .kosar').fadeOut("slow");
                        $("[name=\'product_elhelyezkedes["+json['sor']+"][elhelyezkedes_alcsoport_id]\']").css("border","2px solid #df190a");

                    }
                    if( json['error']['kiemelesek']) {
                        $('#elhelyezkedes-row'+json['sor'] ).before('<div class="warning" style="display: none;">' + json['error']['kiemelesek'] + '</div>');

                        $('.warning').slideDown(500);
                        $('#elhelyezkedes-row'+json['sor']+' .kosar').fadeOut('slow');
                        $("[name=\'product_elhelyezkedes["+json['sor']+"][kiemelesek_id]\']").css("border","2px solid #df190a");

                    }
                } else {
                    $('#elhelyezkedes-row'+json['sor']+' .kosar').fadeIn('slow');
                    $('#elhelyezkedes-row'+json['sor']+" select" ).css("border","none");
                }
            },
            error: function(e) {
            }


        });

    }

</script>

<?php if (isset($error_warning) && $error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>

    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <h1><img src="catalog/view/theme/default/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
  <div id="kiemelesek_kosar">
          <?php echo $cart_elhelyezkedes; ?>
  </div>
  <div style="margin-bottom:10px; display: table; width: 100% ">
      <div style="display: table-cell; text-align: left">
          <span style="font-size: 18px; font-weight: bold; color: #636E75; background-color: transparent; padding: 2px 20px 2px 50px; border: 1px solid #ccc; border-radius: 5px;">
              <?php echo $name;?>
          </span>
      </div>
      <div style="display: table-cell; text-align: right">
          <span style="">
              <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>&nbsp;
              <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
          </span>
      </div>


  </div>
  <div class="wishlist-info">



  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

          <table id="elhelyezkedes" class="list">
              <thead>
                  <tr>
                     <td class="left" colspan="9"><span>Rendelhető elhelyezkedés, kiemelés, keret stb. </span></td>
                  </tr>
              </thead>



              <tbody id="elhelyezkedes-rowNULL" style="margin-top: 10px; display: none">
              <tr>
                  <td class="left szelesseg">

                      <div  class="select_block" >
                          <select name="product_elhelyezkedes[NULL][elhelyezkedes_id]" class="elhelyezkedes"
                                  onchange="AratKiikr(this,'elhelyezkedesNULL','NULL')" <?php #echo $disabled; ?> >
                              <option value="0"><?php echo $text_select; ?></option>
                              <?php foreach ($elhelyezkedesek_selects as $elhelyezkedesek_select) { ?>
                                      <option value             = "<?php echo $elhelyezkedesek_select['elhelyezkedes_id']; ?>"
                                              ara               = "<?php echo number_format($this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                              neve              = "<?php echo $elhelyezkedesek_select['megnevezes']; ?>"
                                              simple_ar_netto   = "<?php echo $elhelyezkedesek_select['ara']; ?>"
                                              simple_ar         = "<?php echo $this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                          <?php echo $elhelyezkedesek_select['megnevezes']." - ".number_format($this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                                      </option>

                              <?php } ?>
                          </select>
                          <span id="elhelyezkedesNULL"  class="ar_megjelenit"></span>
                      </div>



                      <div  class="select_block" style="display: none" id="alcsoportNULL" >
                          <select name="product_elhelyezkedes[NULL][elhelyezkedes_alcsoport_id]"  class="elhelyezkedes_alcsoport"
                                  onchange="AratKiikr(this,'elhelyezkedes_alcsoportNULL','NULL' )" <?php #echo $disabled; ?> >
                              <option value="0"><?php echo $text_select; ?></option>
                              <?php foreach ($elhelyezkedesek_alcsoport_selects as $elhelyezkedesek_alcsoport_select) { ?>
                                      <option value             = "<?php echo $elhelyezkedesek_alcsoport_select['elhelyezkedes_alcsoport_id']; ?>"
                                              ara               = "<?php echo number_format($this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                              neve              = "<?php echo $elhelyezkedesek_alcsoport_select['megnevezes']; ?>"
                                              simple_ar_netto   = "<?php echo $elhelyezkedesek_alcsoport_select['ara']; ?>"
                                              simple_ar         = "<?php  echo $this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_alcsoport_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                          <?php echo $elhelyezkedesek_alcsoport_select['megnevezes']." - ".number_format($this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_alcsoport_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                                      </option>

                              <?php } ?>
                          </select>
                          <span id="elhelyezkedes_alcsoportNULL"  class="ar_megjelenit"></span>
                      </div>



                      <div  class="select_block" >
                          <select name="product_elhelyezkedes[NULL][kiemelesek_id]" class="kiemelesek"
                                  onchange="AratKiikr(this,'kiemelesekNULL','NULL')"  <?php #echo $disabled; ?> >

                              <option value="0"><?php echo $text_select; ?></option>
                              <?php foreach ($kiemelesek_selects as $kiemelesek_select) { ?>
                                      <option value             = "<?php echo $kiemelesek_select['kiemelesek_id']; ?>"
                                              ara               = "<?php echo number_format($this->tax->calculate($kiemelesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                              neve              = "<?php echo $kiemelesek_select['megnevezes']; ?>"
                                              simple_ar_netto   = "<?php echo $kiemelesek_select['ara']; ?>"
                                              simple_ar         = "<?php  echo $this->tax->calculate($kiemelesek_select['ara'], $kiemelesek_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                          <?php echo substr($kiemelesek_select['megnevezes'],0,70)." - ".number_format($this->tax->calculate($kiemelesek_select['ara'], $kiemelesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                                      </option>


                              <?php } ?>
                          </select>
                          <span id="kiemelesekNULL" class="ar_megjelenit"></span>
                      </div>
                  </td>



                  <td class="left">
                      <div>
                          <input type="text" name="product_elhelyezkedes[NULL][idoszak]"  sor="NULL" neve="mennyi"
                                 value="" size="2" onchange="datumig_atszamol(this)"/>


                          <select name="product_elhelyezkedes[NULL][idoszak_id]" sor="NULL" neve="idoszak_id"
                                  onchange="datumig_atszamol(this)" onKeyUp="datumig_atszamol(this)"  >
                              <?php foreach($idoszakok as $ido) { ?>
                                  <option value="<?php echo $ido['id']?>"><?php echo $ido['name']; ?></option>
                              <?php } ?>
                          </select>


                          <input  type="text" name="product_elhelyezkedes[NULL][datum_tol]" sor="NULL"  neve="datum_tol"  value=""  class="elhelyezkedes_datum" />
                          <input  type="text" name="product_elhelyezkedes[NULL][datum_ig]"  sor="NULL"  neve="datum_ig" readonly="readonly" value="" class="" />
                      </div>
                      <table class="form" style="border: none">
                          <tr>
                              <td class="prioritas"><?php echo $text_prioritas; ?>
                                  <input type="text" name="product_elhelyezkedes[NULL][priority]"   value="" size="2" />
                              </td>

                          </tr>
                      </table>

                  </td>


                  <td class="left" style="width: 13%;">
                      <div id="torles_kosarbaNULL">
                          <div class="kiemeles_kosarba"><a onclick="$('#elhelyezkedes-rowNULL').remove();" class="button"><?php echo $button_remove; ?></a></div>
                          <input type="button" class="kosar button" value="<?php echo $button_cart; ?>"
                                 onclick="addToCartKiemeles('<?php echo $_REQUEST['product_id']; ?>',NULL)" />
                      </div>


                  </td>

              </tr>

              <input type="hidden" name="product_elhelyezkedes[NULL][fizetes_elbiralas_status_id]"         value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][penzugyi_status_id]"                  value="" >

              <input type="hidden" name="product_elhelyezkedes[NULL][total_netto_ar]"                      value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][total_brutto_ar]"                     value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][netto_ar]"                            value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][brutto_ar]"                           value="" >

              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_neve]"                  value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_netto_ara]"             value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_brutto_ara]"            value="" >

              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_alcsoport_neve]"        value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_alcsoport_netto_ara]"   value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_alcsoport_brutto_ara]"  value="" >

              <input type="hidden" name="product_elhelyezkedes[NULL][kiemelesek_neve]"                     value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][kiemelesek_netto_ara]"                value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][kiemelesek_brutto_ara]"               value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][sor]"                                 value="NULL" >


              <tr>
                  <td >
                      <span id="ar_osszesenNULL"  class="ar_megjelenit_ossesen"></span>

                  </td>
                  <td >
                              <span id="ar_sor_totalNULL"  class="ar_megjelenit_ossesen">

                              </span>

                  </td>
              </tr>

              <tr>
                  <td colspan="9" class="csik"></td>
              </tr>
              </tbody>




              <?php foreach ($product_elhelyezkedess as $product_elhelyezkedes) { ?>
                  <?php $elhelyezkedes_ar = false;?>
                  <?php $elhelyezkedes_alcsoport_ar = false;?>
                  <?php $kiemelesek_ar = false;?>
                  <?php $readonly = ''; ?>
                  <?php $disabled = ''; ?>
                  <?php if ($product_elhelyezkedes['penzugyi_status'] || $product_elhelyezkedes['fizetesi_status'] || $product_elhelyezkedes['kosarban_van']) { ?>
                      <?php $readonly = 'readonly="readonly"'; ?>
                      <?php $disabled = 'disabled="disabled""'; ?>
                  <?php } ?>
                  <tbody id="elhelyezkedes-row<?php echo $product_elhelyezkedes['sor']; ?>" style="margin-top: 10px;">
                      <tr>
                          <td class="left szelesseg">

                              <div  class="select_block" >
                                  <select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_id]" class="elhelyezkedes"
                                          onchange="AratKiikr(this,'elhelyezkedes<?php echo $product_elhelyezkedes['sor']?>','<?php echo $product_elhelyezkedes['sor']?>','<?php echo $product_elhelyezkedes['product_id']; ?>')" <?php echo $disabled; ?> >
                                      <?php $megtalalva = false; ?>
                                      <option value="0"><?php echo $text_select; ?></option>
                                      <?php foreach ($elhelyezkedesek_selects as $elhelyezkedesek_select) { ?>

                                          <?php if ($elhelyezkedesek_select['elhelyezkedes_id'] == $product_elhelyezkedes['elhelyezkedes_id']) { ?>
                                              <?php $megtalalva=true; ?>
                                              <option value           = "<?php echo $product_elhelyezkedes['elhelyezkedes_id']; ?>" selected="selected"
                                                      ara             = "<?php echo number_format($product_elhelyezkedes['elhelyezkedes_brutto_ara'],0,'','.').$penznem ; ?>"
                                                      neve            = "<?php echo $product_elhelyezkedes['elhelyezkedes_neve']; ?>"
                                                      simple_ar_netto = "<?php echo $product_elhelyezkedes['elhelyezkedes_netto_ara']; ?>"
                                                      simple_ar       = "<?php echo $product_elhelyezkedes['elhelyezkedes_brutto_ara'] ?>" >

                                                  <?php echo $product_elhelyezkedes['elhelyezkedes_neve']." - ".number_format($product_elhelyezkedes['elhelyezkedes_brutto_ara'],0,'','.').$penznem ; ?>
                                              </option>

                                              <?php $elhelyezkedes_ar = $this->tax->calculate($product_elhelyezkedes['elhelyezkedes_netto_ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')); ?>

                                          <?php } else { ?>

                                              <option value             = "<?php echo $elhelyezkedesek_select['elhelyezkedes_id']; ?>"
                                                      ara               = "<?php echo number_format($this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                                      neve              = "<?php echo $elhelyezkedesek_select['megnevezes']; ?>"
                                                      simple_ar_netto   = "<?php echo $elhelyezkedesek_select['ara']; ?>"
                                                      simple_ar         = "<?php echo $this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                                  <?php echo $elhelyezkedesek_select['megnevezes']." - ".number_format($this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                                              </option>
                                          <?php } ?>
                                      <?php } ?>

                                      <?php if (!$megtalalva && $product_elhelyezkedes['elhelyezkedes_id'] > 0) { ?>
                                          <option value           = "<?php echo $product_elhelyezkedes['elhelyezkedes_id']; ?>" selected="selected"
                                                  ara             = "<?php echo number_format($product_elhelyezkedes['elhelyezkedes_brutto_ara'],0,'','.').$penznem ; ?>"
                                                  neve            = "<?php echo $product_elhelyezkedes['elhelyezkedes_neve']; ?>"
                                                  simple_ar_netto = "<?php echo $product_elhelyezkedes['elhelyezkedes_netto_ara']; ?>"
                                                  simple_ar       = "<?php echo $product_elhelyezkedes['elhelyezkedes_brutto_ara']; ?>" >

                                              <?php echo $product_elhelyezkedes['elhelyezkedes_neve']." - ".number_format($product_elhelyezkedes['elhelyezkedes_brutto_ara'],0,'','.').$penznem ; ?>
                                          </option>

                                          <?php $elhelyezkedes_ar = $product_elhelyezkedes['elhelyezkedes_brutto_ara']; ?>
                                      <?php } ?>

                                  </select>
                                  <span id="elhelyezkedes<?php echo $product_elhelyezkedes['sor']?>"  class="ar_megjelenit">
                                      <?php echo $elhelyezkedes_ar ? number_format($elhelyezkedes_ar,0,"",".").$penznem : "";?>
                                  </span>
                              </div>


                              <?php if ($product_elhelyezkedes['kell_alcsoport'] != 1) { ?>
                                    <?php $latszik = "display:none" ?>
                              <?php } else {?>
                                  <?php $latszik = "display:block" ?>
                              <?php } ?>
                              <div  class="select_block" style="<?php echo $latszik; ?>" id="alcsoport<?php echo $product_elhelyezkedes['sor'];?>">
                                  <select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_id]" class="elhelyezkedes_alcsoport"
                                          onchange="AratKiikr(this,'elhelyezkedes_alcsoport<?php echo $product_elhelyezkedes['sor']?>','<?php echo $product_elhelyezkedes['sor']?>' )" <?php echo $disabled; ?> >
                                      <?php $megtalalva = false; ?>

                                      <option value="0"><?php echo $text_select; ?></option>
                                      <?php foreach ($elhelyezkedesek_alcsoport_selects as $elhelyezkedesek_alcsoport_select) { ?>
                                          <?php if ($elhelyezkedesek_alcsoport_select['elhelyezkedes_alcsoport_id'] == $product_elhelyezkedes['elhelyezkedes_alcsoport_id']) { ?>
                                              <?php $megtalalva=true; ?>

                                              <option value             = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_id']; ?>" selected="selected"
                                                      ara               = "<?php echo number_format($product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'],0,'','.').$penznem ; ?>"
                                                      neve              = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']; ?>"
                                                      simple_ar_netto   = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_netto_ara']; ?>"
                                                      simple_ar         = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'] ?>">
                                                  <?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']." - ".number_format($product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'],0,'','.').$penznem ; ?>
                                              </option>
                                              <?php $elhelyezkedes_alcsoport_ar = $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'];?>

                                          <?php } else { ?>

                                              <option value             = "<?php echo $elhelyezkedesek_alcsoport_select['elhelyezkedes_alcsoport_id']; ?>"
                                                      ara               = "<?php echo number_format($this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                                      neve              = "<?php echo $elhelyezkedesek_alcsoport_select['megnevezes']; ?>"
                                                      simple_ar_netto   = "<?php echo $elhelyezkedesek_alcsoport_select['ara']; ?>"
                                                      simple_ar         = "<?php  echo $this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_alcsoport_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                                  <?php echo $elhelyezkedesek_alcsoport_select['megnevezes']." - ".number_format($this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_alcsoport_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                                              </option>
                                          <?php } ?>

                                      <?php } ?>

                                      <?php if (!$megtalalva && $product_elhelyezkedes['elhelyezkedes_alcsoport_id'] > 0) { ?>

                                          <option value             = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_id']; ?>" selected="selected"
                                                  ara               = "<?php echo number_format($product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'],0,'','.').$penznem ; ?>"
                                                  neve              = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']; ?>"
                                                  simple_ar_netto   = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_netto_ara']; ?>"
                                                  simple_ar         = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara']; ?>">
                                              <?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']." - ".number_format($product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'],0,'','.').$penznem ; ?>
                                          </option>
                                          <?php $elhelyezkedes_alcsoport_ar = $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'];?>

                                      <?php } ?>
                                  </select>

                                  <span id="elhelyezkedes_alcsoport<?php echo $product_elhelyezkedes['sor']?>"  class="ar_megjelenit">
                                     <?php echo $elhelyezkedes_alcsoport_ar ? number_format($elhelyezkedes_alcsoport_ar,0,"",".").$penznem : "" ;?>
                                  </span>
                              </div>
                              <?php if ($product_elhelyezkedes['kell_alcsoport'] != 1) { ?>
                                  <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_id]" value="0" >
                              <?php } ?>

                              <div  class="select_block" >
                                  <select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kiemelesek_id]" class="kiemelesek"
                                          onchange="AratKiikr(this,'kiemelesek<?php echo $product_elhelyezkedes['sor']?>','<?php echo $product_elhelyezkedes['sor']?>')"  <?php echo $disabled; ?> >
                                      <?php $megtalalva = false; ?>

                                      <option value="0"><?php echo $text_select; ?></option>
                                      <?php foreach ($kiemelesek_selects as $kiemelesek_select) { ?>
                                          <?php if ($kiemelesek_select['kiemelesek_id'] == $product_elhelyezkedes['kiemelesek_id']) { ?>
                                              <?php $megtalalva=true; ?>

                                              <option value             = "<?php echo $product_elhelyezkedes['kiemelesek_id']; ?>" selected="selected"
                                                      ara               = "<?php echo number_format($product_elhelyezkedes['kiemelesek_brutto_ara'],0,'','.').$penznem ; ?>"
                                                      neve              = "<?php echo $product_elhelyezkedes['kiemelesek_neve']; ?>"
                                                      simple_ar_netto   = "<?php echo $product_elhelyezkedes['kiemelesek_netto_ara']; ?>"
                                                      simple_ar         = "<?php echo $product_elhelyezkedes['kiemelesek_brutto_ara']; ?>" >
                                                  <?php echo $product_elhelyezkedes['kiemelesek_neve']." - ".number_format($product_elhelyezkedes['kiemelesek_brutto_ara'],0,'','.').$penznem ; ?>
                                              </option>
                                              <?php $kiemelesek_ar = $product_elhelyezkedes['kiemelesek_brutto_ara'];?>

                                          <?php } else { ?>
                                              <option value             = "<?php echo $kiemelesek_select['kiemelesek_id']; ?>"
                                                      ara               = "<?php echo number_format($this->tax->calculate($kiemelesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                                      neve              = "<?php echo $kiemelesek_select['megnevezes']; ?>"
                                                      simple_ar_netto   = "<?php echo $kiemelesek_select['ara']; ?>"
                                                      simple_ar         = "<?php  echo $this->tax->calculate($kiemelesek_select['ara'], $kiemelesek_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                                   <?php echo substr($kiemelesek_select['megnevezes'],0,70)." - ".number_format($this->tax->calculate($kiemelesek_select['ara'], $kiemelesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                                              </option>
                                          <?php } ?>
                                      <?php } ?>

                                      <?php if (!$megtalalva && $product_elhelyezkedes['kiemelesek_id'] > 0) { ?>
                                          <option value             = "<?php echo $product_elhelyezkedes['kiemelesek_id']; ?>" selected="selected"
                                                  ara               = "<?php echo number_format($product_elhelyezkedes['kiemelesek_brutto_ara'],0,'','.').$penznem ; ?>"
                                                  neve              = "<?php echo $product_elhelyezkedes['kiemelesek_neve']; ?>"
                                                  simple_ar_netto   = "<?php echo $product_elhelyezkedes['kiemelesek_netto_ara']; ?>"
                                                  simple_ar         = "<?php echo $product_elhelyezkedes['kiemelesek_brutto_ara'] ?>" >
                                              <?php echo $product_elhelyezkedes['kiemelesek_neve']." - ".number_format($product_elhelyezkedes['kiemelesek_brutto_ara'],0,'','.').$penznem ; ?>
                                          </option>
                                          <?php $kiemelesek_ar = $product_elhelyezkedes['kiemelesek_brutto_ara'];?>
                                      <?php } ?>
                                  </select>

                                  <span id="kiemelesek<?php echo $product_elhelyezkedes['sor']?>" class="ar_megjelenit">
                                     <?php echo  $kiemelesek_ar ? number_format($kiemelesek_ar,0,"",".").$penznem : "" ;?>
                                  </span>
                              </div>
                          </td>



                          <td class="left">
                              <div>
                                  <input type="text" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][idoszak]"  sor="<?php echo $product_elhelyezkedes['sor']; ?>" neve="mennyi" <?php echo $disabled; ?>
                                         value="<?php echo $product_elhelyezkedes['idoszak']?>" size="2" onchange="datumig_atszamol(this)"/>



                                  <select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][idoszak_id]" sor="<?php echo $product_elhelyezkedes['sor']; ?>" neve="idoszak_id"
                                                       onchange="datumig_atszamol(this)" onKeyUp="datumig_atszamol(this)" <?php echo $disabled; ?>  >
                                      <?php foreach($idoszakok as $ido) { ?>
                                          <?php if ($ido['id'] == $product_elhelyezkedes['idoszak_id']) { ?>
                                              <option value="<?php echo $ido['id']?>"  selected="selected"><?php echo $ido['name']; ?></option>
                                          <?php } else { ?>
                                              <option value="<?php echo $ido['id']?>"><?php echo $ido['name']; ?></option>
                                          <?php } ?>
                                      <?php } ?>
                                  </select>




                                  <input  type="text" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][datum_tol]" sor="<?php echo $product_elhelyezkedes['sor']; ?>" <?php echo $disabled; ?> neve="datum_tol"  value="<?php  echo $product_elhelyezkedes['datum_tol']?>"  class="elhelyezkedes_datum" />
                                  <input  type="text" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][datum_ig]"  sor="<?php echo $product_elhelyezkedes['sor']; ?>"   neve="datum_ig" readonly="readonly" value="<?php  echo $product_elhelyezkedes['datum_ig']?>" class="" />
                              </div>
                              <table class="form" style="border: none">
                                  <tr>
                                      <td class="prioritas"><?php echo $text_prioritas; ?>
                                            <input type="text" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][priority]" <?php echo $disabled; ?>   value="<?php  echo $product_elhelyezkedes['priority']?>" size="2" />
                                      </td>


                                      <?php if ($product_elhelyezkedes['penzugyi_status'] || $product_elhelyezkedes['fizetesi_status'] ) { ?>
                                          <td class="penzugyi_status"><?php echo $entry_status; ?></td>
                                          <td style="font-weight: bold">
                                              <?php if ($product_elhelyezkedes['penzugyi_status']) { ?>
                                                  <?php echo $product_elhelyezkedes['penzugyi_status']['megnevezes']; ?>
                                                  <br>
                                              <?php }  ?>
                                              <?php if ($product_elhelyezkedes['fizetesi_status']) { ?>
                                                  <?php echo $product_elhelyezkedes['fizetesi_status']['megnevezes']; ?>
                                              <?php }  ?>
                                          </td>
                                      <?php } ?>
                                  </tr>
                              </table>

                          </td>


                          <?php if ( !$product_elhelyezkedes['penzugyi_status'] && !$product_elhelyezkedes['fizetesi_status'] && !$product_elhelyezkedes['kosarban_van'] ) { ?>
                              <td class="left" style="width: 13%;">
                                  <div id="torles_kosarba<?php echo $product_elhelyezkedes['sor'];?>">
                                      <div class="kiemeles_kosarba"><a onclick="$('#elhelyezkedes-row<?php echo $product_elhelyezkedes['sor']; ?>').remove();  $('.success, .warning, .error').remove();" class="button"><?php echo $button_remove; ?></a></div>
                                      <input type="button" class="kosar button" value="<?php echo $button_cart; ?>"
                                             onclick="addToCartKiemeles('<?php echo $product_elhelyezkedes['product_id']; ?>',<? echo $product_elhelyezkedes['sor']?>)" />
                                  </div>
                              </td>
                          <?php } else {?>
                              <td style="width: 13%;"></td>
                          <?php } ?>
                      </tr>


                      <?php if ( !empty($disabled) ) { ?>
                          <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_id]"        value="<?php echo $product_elhelyezkedes['elhelyezkedes_id']; ?>" >
                          <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_id]" value="<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_id']; ?>" >
                          <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kiemelesek_id]"           value="<?php echo $product_elhelyezkedes['kiemelesek_id']; ?>" >
                          <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][idoszak_id]"              value="<?php echo $product_elhelyezkedes['idoszak_id']; ?>" >
                          <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][idoszak]"                 value="<?php echo $product_elhelyezkedes['idoszak']; ?>" >
                          <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][datum_tol]"               value="<?php echo $product_elhelyezkedes['datum_tol']; ?>" >
                          <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][priority]"                value="<?php echo $product_elhelyezkedes['priority']; ?>" >
                      <?php } ?>

                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][fizetes_elbiralas_status_id]" value="<?php echo $product_elhelyezkedes['fizetes_elbiralas_status_id']; ?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][penzugyi_status_id]" value="<?php echo $product_elhelyezkedes['penzugyi_status_id']; ?>" >

                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][total_netto_ar]"                      value="<?php echo $product_elhelyezkedes['total_netto_ar']?>"  >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][total_brutto_ar]"                     value="<?php echo $product_elhelyezkedes['total_brutto_ar']?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][netto_ar]"                            value="<?php echo $product_elhelyezkedes['netto_ar']?>"  >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][brutto_ar]"                           value="<?php echo $product_elhelyezkedes['brutto_ar']?>" >

                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_neve]"                  value="<?php echo $product_elhelyezkedes['elhelyezkedes_neve']?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_netto_ara]"             value="<?php echo $product_elhelyezkedes['elhelyezkedes_netto_ara']?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_brutto_ara]"            value="<?php echo $product_elhelyezkedes['elhelyezkedes_brutto_ara']?>" >

                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_neve]"        value="<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_netto_ara]"   value="<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_netto_ara']?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_brutto_ara]"  value="<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara']?>" >

                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kiemelesek_neve]"                     value="<?php echo $product_elhelyezkedes['kiemelesek_neve']?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kiemelesek_netto_ara]"                value="<?php echo $product_elhelyezkedes['kiemelesek_netto_ara']?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kiemelesek_brutto_ara]"               value="<?php echo $product_elhelyezkedes['kiemelesek_brutto_ara']?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezes_id]"                       value="<?php echo $product_elhelyezkedes['elhelyezes_id']?>" >
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][sor]"                                 value="<?php echo $product_elhelyezkedes['sor']?>" >


                      <tr>
                          <td >
                              <span id="ar_osszesen<?php echo $product_elhelyezkedes['sor']?>"  class="ar_megjelenit_ossesen"><?php echo $product_elhelyezkedes['brutto_ar'] ? $text_osszesen . number_format($product_elhelyezkedes['brutto_ar'],0,"",".").$penznem : "" ;?></span>
                          </td>
                          <td >
                              <span id="ar_sor_total<?php echo $product_elhelyezkedes['sor']?>"  class="ar_megjelenit_ossesen">
                                  <?php echo $text_fizetendo?> <?php echo number_format($product_elhelyezkedes['total_brutto_ar'],0,"",".").$penznem; ?>
                              </span>

                          </td>
                      </tr>

                      <tr>
                          <td colspan="9" class="csik"></td>
                      </tr>
                  </tbody>
                  <script>
                      <?php if (empty($disabled)) { ?>
                          var sorszam = <?php echo $product_elhelyezkedes['sor']?>;
                          Osszesit(sorszam);
                      <?php } ?>
                  </script>
              <?php } ?>
              <tfoot style="display: table; width: 100%">
              <tr>
                  <td class="right"><a onclick="addElhelyezkedes();" class="button"><?php echo $button_add_elhelyezkedes; ?></a></td>
              </tr>
              </tfoot>
          </table>




          </div>
    </form>
  </div>
  <div style="text-align:right; margin-top:10px;"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>&nbsp;<a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
  <?php echo $content_bottom; ?></div>


  <script>
        $(document).ready(function() {
            $('.elhelyezkedes_datum').datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 'd',

                onSelect: function(text){

                    datumig_atszamol(this,text);

                }

            });


        });


        var elhelyezkedes_row = <?php echo isset($elhelyezkedes_row_max) ? $elhelyezkedes_row_max : 0; ?>;

        function addElhelyezkedes() {

            $('#elhelyezkedes-rowNULL .elhelyezkedes_datum').attr('id',"");
            $('#elhelyezkedes-rowNULL .elhelyezkedes_datum').attr('class',"elhelyezkedes_datum");
            var emptyrow = $('#elhelyezkedes-rowNULL')[0];
            var newrow = emptyrow.cloneNode(true);
            newrow.id = "elhelyezkedes-row"+elhelyezkedes_row;
            newrow.innerHTML = newrow.innerHTML.replace(/NULL/g, elhelyezkedes_row);
            newrow.style.display = '';

            $('#elhelyezkedes tfoot').before(newrow);

            $('#elhelyezkedes-row' + elhelyezkedes_row + ' .elhelyezkedes_datum').datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 'd',

                onSelect: function(text){
                    datumig_atszamol(this,text);

                }
            });

            elhelyezkedes_row++;
        }



        function datumig_atszamol(obj,text) {

            var melyik_sor=obj.attributes.sor.value;
            if (typeof(text) == "undefined") {
                text = $("[sor="+melyik_sor+"]").filter("[neve=datum_tol]").val();
                $("[sor="+melyik_sor+"]").blur();


                if (text == "" && false){
                    return false;
                }
            }

            var newdate = new Date(text);
            var mennyiseg = $("[sor="+melyik_sor+"]").filter("[neve=mennyi]").val()*1;
            var idoszak_id = $("[sor="+melyik_sor+"]").filter("[neve=idoszak_id]").val();

            switch (idoszak_id) {
                case "1":
                    newdate.setDate(newdate.getDate() + mennyiseg-1);
                    break;

                case "2":
                    mennyiseg = mennyiseg*7;
                    newdate.setDate(newdate.getDate() + mennyiseg-1);
                    break;

                case "3":
                    newdate.setMonth(newdate.getMonth() + mennyiseg);
                    break;

                case "4":
                    newdate.setFullYear(newdate.getFullYear() + mennyiseg);
                    break;
            }

            var nd = new Date(newdate);
            var honap = "0"+(nd.getMonth()+1);
            honap = honap.substr(honap.length-2);
            var nap = "0"+nd.getDate();
            nap = nap.substr(nap.length-2);
            var datum_ig = nd.getFullYear()+"-"+honap+"-"+nap;

            if (mennyiseg > 0 && nap != "00") {

                $("[sor="+melyik_sor+"]").filter("[neve=datum_ig]").attr("value",datum_ig);

                Osszesit(melyik_sor);
            } else {
                $("[sor="+melyik_sor+"]").filter("[neve=datum_ig]").attr("value","");
                if (text > "0000-00-00") {
                    $('#ures_datum_kezdo'+melyik_sor).slideUp(500);
                    $("[name=\'product_elhelyezkedes["+melyik_sor+"][datum_tol]\']").css("border","none");

                }
            }

        }

      function AratKiikr(obj,ara,sor) {
          var ar = obj[obj.selectedIndex].getAttribute("ara");
          $("#"+ara).html(ar);

              if (obj.classList[0] == "elhelyezkedes") {
                  var elhelyezkedes_id = obj.value;

                  $.ajax({
                      url: 'index.php?route=checkout/cart_elhelyezkedes/alcsoportVizsgal',
                      type: 'post',
                      data: 'elhelyezkedes_id='+elhelyezkedes_id +
                            '&sor=' + sor,

                      dataType: 'json',

                      success: function(json) {

                          $('.success, .warning, .error').remove();
                          if (json['kell_alcsoport']) {
                              $("input[type=hidden][name=\'product_elhelyezkedes["+json['sor']+"][elhelyezkedes_alcsoport_id]\']").attr('disabled', 'disabled');
                              $("#alcsoport"+json['sor']).slideDown(500);

                          } else {
                              $("[name=\'product_elhelyezkedes["+json['sor']+"][elhelyezkedes_alcsoport_id]\']").val(0)
                              $("#alcsoport"+json['sor']).slideUp(500);
                          }
                      },
                      error: function(e) {
                      }


                  });
              }

          $(obj).blur();

          Osszesit(sor);
      }



      function addToCartKiemeles(product_id, sor) {

          var formom = $("#form").serialize();

          $.ajax({
              url: 'index.php?route=checkout/cart_elhelyezkedes/addMentes&product_id='+product_id,
              type: 'post',
              data: formom,

              dataType: '',

              success: function() {
                  debugger;
                  $('.success, .warning, .error').remove();
              },
              error: function(e) {
                  debugger;
              }


          });


          var datum_tol = $("[name=\'product_elhelyezkedes["+sor+"][datum_tol]\']").val();
          var datum_ig = $("[name=\'product_elhelyezkedes["+sor+"][datum_ig]\']").val();
          var elhelyezes_id=   $("[name=\'product_elhelyezkedes["+sor+"][elhelyezes_id]\']").val();

          var elhelyezkedes = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_id]\']");
          var elhelyezkedes_alcsoport = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_id]\']");
          var kiemelesek = $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_id]\']");
          var brutto    = $("[name=\'product_elhelyezkedes["+sor+"][total_brutto_ar]\']").val();

          var elhelyezkedes_neve    = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_neve]\']").val();
          var elhelyezkedes_netto   = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_netto_ara]\']").val();
          var elhelyezkedes_brutto  = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_brutto_ara]\']").val();

          var elhelyezkedes_alcsoport_neve      = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_neve]\']").val();
          var elhelyezkedes_alcsoport_netto     = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_netto_ara]\']").val();
          var elhelyezkedes_alcsoport_brutto    = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_brutto_ara]\']").val();

          var kiemelesek_neve       = $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_neve]\']").val();
          var kiemelesek_netto      = $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_netto_ara]\']").val();
          var kiemelesek_brutto     = $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_brutto_ara]\']").val();

          var idoszak               = $("[name=\'product_elhelyezkedes["+sor+"][idoszak]\']").val();
          var idoszak_mennyiseg     = $("[name=\'product_elhelyezkedes["+sor+"][idoszak_id]\']");
          var idoszak_mennyiseg_sor     = idoszak_mennyiseg[0].selectedIndex;
          var idoszak_mennyiseg_text = idoszak_mennyiseg[0][idoszak_mennyiseg_sor].innerText;

          $.ajax({
              url: 'index.php?route=checkout/cart_elhelyezkedes/addElhelyezkedes',
              data: 'datum_kezdo='                    + datum_tol
                  + '&datum_vege='                    + datum_ig
                  + '&elhelyezes_id='                 + elhelyezes_id
                  + '&sor='                           + sor
                  + '&elhelyezkedes_id='              + elhelyezkedes.val()
                  + '&elhelyezkedes_alcsoport_id='    + elhelyezkedes_alcsoport.val()
                  + '&kiemelesek_id='                 + kiemelesek.val()

                  + '&elhelyezkedes_neve='             + elhelyezkedes_neve
                  + '&elhelyezkedes_netto='            + elhelyezkedes_netto
                  + '&elhelyezkedes_brutto='           + elhelyezkedes_brutto

                  + '&elhelyezkedes_alcsoport_neve='   + elhelyezkedes_alcsoport_neve
                  + '&elhelyezkedes_alcsoport_netto='  + elhelyezkedes_alcsoport_netto
                  + '&elhelyezkedes_alcsoport_brutto=' + elhelyezkedes_alcsoport_brutto

                  + '&kiemelesek_neve='                +kiemelesek_neve
                  + '&kiemelesek_netto='               + kiemelesek_netto
                  + '&kiemelesek_brutto='              + kiemelesek_brutto

                  + '&idoszak='                        + idoszak
                  + '&idoszak_mennyiseg='              + idoszak_mennyiseg_text
                  + '&idoszak_id='                     + idoszak_mennyiseg_text

                  + '&product_id='                 + product_id
                  + '&brutto='                      + brutto,

              dataType: 'json',

              success: function(json) {
                  debugger;
                  $('.success, .warning, .error, .warning_kosar').remove();

                  if (json['error']) {


                      if (json['error']['ures_id']) {
                          $('#elhelyezkedes-row'+json['sor'] ).before('<div class="warning_kosar" id="ures_id'+json['sor']+'" style="display: none;">' + json['error']['ures_id'] + '</div>');
                          $('.warning_kosar').fadeIn('slow');
                          $('#elhelyezkedes-row'+json['sor']+' .szelesseg').css("border","2px solid #df190a");

                      }
                      if (json['error']['ures_idoszak']) {
                          $('#elhelyezkedes-row'+json['sor'] ).before('<div class="warning_kosar"  id="ures_idoszak'+json['sor']+'" style="display: none;">' + json['error']['ures_idoszak'] + '</div>');
                          $('.warning_kosar').fadeIn('slow');
                          $("[name=\'product_elhelyezkedes["+json['sor']+"][idoszak]\']").css("border","2px solid #df190a");

                      }
                      if (json['error']['ures_datum_kezdo']) {
                          $('#elhelyezkedes-row'+json['sor'] ).before('<div class="warning_kosar ures_datum_kezdo" id="ures_datum_kezdo'+json['sor']+'" style="display: none;">' + json['error']['ures_datum_kezdo'] + '</div>');
                          $('.warning_kosar').fadeIn('slow');
                          $("[name=\'product_elhelyezkedes["+json['sor']+"][datum_tol]\']").css("border","2px solid #df190a");

                      }




                  } else {
                      var sor = json['sor'];

                      $('.success, .warning, .error').remove();
                      $("#cart_elhelyezkedes").add("#cart_elhelyezkedes").load('index.php?route=module/cart_elhelyezkedes #cart_elhelyezkedes > *');

                      var elhelyezkedes_id              = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_id]\']").val();
                      var elhelyezkedes_alcsoport_id    = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_id]\']").val();
                      var kiemelesek_id                 = $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_id]\']").val();
                      var hanynap                       = $("[name=\'product_elhelyezkedes["+sor+"][idoszak]\']").val();
                      var nap_het                    = $("[name=\'product_elhelyezkedes["+sor+"][idoszak_id]\']").val();
                      var datum_eleje                   = $("[name=\'product_elhelyezkedes["+sor+"][datum_tol]\']").val();
                      //var datum_vege                    = $("[name=\'product_elhelyezkedes["+sor+"][datum_vege]\']").val();
                      var priority                      = $("[name=\'product_elhelyezkedes["+sor+"][priority]\']").val();



                      $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_id]\']").attr("disabled",true);
                      $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_id]\']").attr("disabled",true);
                      $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_id]\']").attr("disabled",true);

                      $("[name=\'product_elhelyezkedes["+sor+"][idoszak]\']").attr("disabled",true);
                      $("[name=\'product_elhelyezkedes["+sor+"][idoszak_id]\']").attr("disabled",true);
                      $("[name=\'product_elhelyezkedes["+sor+"][datum_tol]\']").attr("disabled",true);
                      $("[name=\'product_elhelyezkedes["+sor+"][priority]\']").attr("disabled",true);




                      var input_hidden  =  '<input type="hidden" name="product_elhelyezkedes['+sor+'][elhelyezkedes_id]"            value="'+elhelyezkedes_id+'" >';
                      input_hidden      += '<input type="hidden" name="product_elhelyezkedes['+sor+'][elhelyezkedes_alcsoport_id]"  value="'+elhelyezkedes_alcsoport_id+'" >';
                      input_hidden      += '<input type="hidden" name="product_elhelyezkedes['+sor+'][kiemelesek_id]"               value="'+kiemelesek_id+'" >';
                      input_hidden      += '<input type="hidden" name="product_elhelyezkedes['+sor+'][idoszak_id]"                  value="'+hanynap+'" >';
                      input_hidden      += '<input type="hidden" name="product_elhelyezkedes['+sor+'][idoszak]"                     value="'+nap_het+'" >';
                      input_hidden      += '<input type="hidden" name="product_elhelyezkedes['+sor+'][datum_tol]"                   value="'+datum_eleje+'" >';
                      //input_hidden      += '<input type="hidden" name="product_elhelyezkedes['+sor+'][datum_ig]"                    value="'+datum_vege+'" >';
                      input_hidden      += '<input type="hidden" name="product_elhelyezkedes['+sor+'][priority]"                    value="'+priority+'" >';

                      $('#elhelyezkedes-row'+sor+" tr").before(input_hidden);
                      $("#torles_kosarba"+sor).fadeOut(500);

                  }
              },
              error: function(e) {
                  debugger;
              }


          });

      }

  </script>



<?php echo $footer; ?>