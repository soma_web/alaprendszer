<?php echo $header; ?>


    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="box">
            <div class="heading">
                <h1><img src="view/image/information.png" alt="" /> <?php echo $heading_title; ?></h1>
                <div class="buttons">
                    <a onclick="optionMentes()" class="button"><?php echo $button_save; ?></a>

                    <!--<a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>-->
                    <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
                </div>
            </div>
            <div class="content">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                    <table class="form">
                        <tr>
                            <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                            <td>
                                <?php foreach ($languages as $language) { ?>
                                    <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" />
                                    <input type="text" name="option_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($option_description[$language['language_id']]) ? $option_description[$language['language_id']]['name'] : ''; ?>" />

                                    <span class="table_option">
                                        <?php echo " ".$entry_description." "?>
                                        <input type="text" name="option_description[<?php echo $language['language_id']; ?>][description]" value="<?php echo isset($option_description[$language['language_id']]) ? $option_description[$language['language_id']]['description'] : ''; ?>"  size="34"/>
                                    </span>
                                    <br />
                                    <?php if (isset($error_name[$language['language_id']])) { ?>
                                        <span class="error"><?php echo $error_name[$language['language_id']]; ?></span><br />
                                    <?php } ?>
                                <?php } ?>
                            </td>
                            <td class="left"><?php echo $entry_option_szin_select; ?>
                                <div id="selectem">

                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td><?php echo $entry_type; ?></td>
                            <td><select name="type" readonly>
                                    <?php if ($type == 'checkbox_qty') { ?>
                                        <option value="checkbox_qty" selected><?php echo $text_table?></option>
                                    <?php } else { ?>
                                        <option value="checkbox_qty"><?php echo $text_table?></option>
                                    <?php } ?>
                                </select>
                            </td>


                        <tr>
                            <td><?php echo $entry_sort_order; ?></td>
                            <td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="1" /></td>
                        </tr>
                    </table>
                    <table id="option-value" class="list">
                        <thead>
                        <tr>
                            <td class="left"><span class="required">*</span> <?php echo $entry_value; ?></td>
                            <td class="left table_option"><?php echo $entry_heading; ?></td>
                            <td class="right"><?php echo $entry_sort_order; ?></td>
                            <td></td>
                        </tr>
                        </thead>
                        <?php $option_value_row = 0; ?>
                        <?php foreach ($option_values as $option_value) { ?>
                            <tbody id="option-value-row<?php echo $option_value_row; ?>">
                            <tr>
                                <td class="left">
                                    <input type="hidden" name="option_value[<?php echo $option_value_row; ?>][option_value_id]" value="<?php echo $option_value['option_value_id']; ?>" />
                                    <input type="hidden" name="option_value[<?php echo $option_value_row; ?>][option_szin_id]" value="<?php echo $option_value['option_szin_id']; ?>" />
                                    <input type="hidden" name="option_value[<?php echo $option_value_row; ?>][azonosito]" value="<?php echo $option_value['azonosito']; ?>" />
                                    <?php foreach ($languages as $language) { ?>
                                        <input readonly type="text" name="option_value[<?php echo $option_value_row; ?>][option_value_description][<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($option_value['option_value_description'][$language['language_id']]) ? $option_value['option_value_description'][$language['language_id']]['name'] : ''; ?>" />
                                        <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                                        <?php if (isset($error_option_value[$option_value_row][$language['language_id']])) { ?>
                                            <span class="error"><?php echo $error_option_value[$option_value_row][$language['language_id']]; ?></span>
                                        <?php } ?>
                                    <?php } ?>
                                </td>

                                <td class="left table_option"><input type="hidden" name="option_value[<?php echo $option_value_row; ?>][option_value_id]" value="<?php echo $option_value['option_value_id']; ?>" />
                                    <input readonly type="text" name="option_value[<?php echo $option_value_row; ?>][heading]" value="<?php echo isset($option_value['heading']) ? $option_value['heading'] : ''; ?>" />
                                </td>

                                <td class="right"><input type="text" name="option_value[<?php echo $option_value_row; ?>][sort_order]" value="<?php echo $option_value['sort_order']; ?>" size="1" /></td>
                                <td class="left"><a onclick="$('#option-value-row<?php echo $option_value_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
                            </tr>
                            </tbody>
                            <?php $option_value_row++; ?>


                        <?php } ?>
                        <tfoot>
                        </tfoot>
                    </table>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript"><!--

        $(document).ready(function(){

            selectkivet = new Array();
            tombszamlal = 0;
            <?php foreach ($option_values as $szin) { ?>
                <?php if (isset($szin['option_szin_id'])) { ?>
                    selectkivet[tombszamlal] = "<?php echo $szin['option_szin_id']?>";
                    tombszamlal ++;
                <?php } ?>
            <?php }?>

            selectEpit();

        });

        var option_value_row = <?php echo $option_value_row; ?>;


        function addFilter(nevtombje, option_szin_id, szinkod, azonosito) {

            debugger;

            html  = '<tbody id="option-value-row' + option_value_row + '">';
            html += '<tr>';
            html += '<td class="left"><input type="hidden" name="option_value[' + option_value_row + '][option_value_id]" value="" />';

            <?php foreach ($languages as $language) { ?>
                if ( typeof(nevtombje) != "undefined" ){
                    if (typeof(nevtombje[<?php echo $language['language_id']?>]) != "undefined") {

                        html += '<input readonly type="text" name="option_value[' + option_value_row + '][option_value_description][<?php echo $language['language_id']; ?>][name]"';
                        html += ' value="'+nevtombje[<?php echo $language['language_id']?>]+'" />';
                        html += ' <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';


                    } else {
                        html += '<input readonly type="text" name="option_value[' + option_value_row + '][option_value_description][<?php echo $language['language_id']; ?>][name]" value="" /> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
                    }
                } else {
                    html += '<input readonly type="text" name="option_value[' + option_value_row + '][option_value_description][<?php echo $language['language_id']; ?>][name]" value="" /> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
                }
            <?php } ?>

            html += '</td>';
            html += '<td class="left"><input type="hidden" name="option_value[' + option_value_row + '][option_value_id]" value="" />';
            html += '<input type="hidden" name="option_value[' + option_value_row + '][option_szin_id]" value="'+option_szin_id+'" />';
            html += '<input type="hidden" name="option_value[' + option_value_row + '][azonosito]" value="'+azonosito+'" />';

            html += '<input readonly class="left table_option" type="text" name="option_value[' + option_value_row + '][heading]" value="'+szinkod+'" />';

            html += '</td>';
            html += '<td class="right"><input type="text" name="option_value[' + option_value_row + '][sort_order]" value="" size="1" /></td>';
            html += '<td class="left"><a onclick="$(\'#option-value-row' + option_value_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
            html += '</tr>';
            html += '</tbody>';

            $('#option-value tfoot').before(html);

            option_value_row++;
        }

        function szuroHozzaad(obj, teljes){
            if (obj > 0) {
                selectkivet[tombszamlal] = obj;
                tombszamlal ++;
                var nevtomb = new Array();
                var option_szin_id;
                var szinkod = '';
                var azonosito = '';
                <?php foreach($szinek as $szin) {?>
                    if (obj ==  <?php echo $szin['option_szin_id']?> ){
                        nevtomb['<?php echo $szin['language_id']?>'] = "<?php echo $szin['name']?>";
                        option_szin_id = "<?php echo $szin['option_szin_id']?>";
                        szinkod = "<?php echo $szin['szinkod']?>";
                        azonosito = "<?php echo $szin['azonosito']?>";
                    }
                <?php }?>
            debugger;
                addFilter(nevtomb, option_szin_id, szinkod, azonosito);

                selectEpit();
            }
        }

        function selectEpit() {
            html1 = '<select name="select_oprion_szin" size="<?php echo $select_language_darab?>" id="szuro_hozzaad" onclick="szuroHozzaad(this.value)">';

            <?php foreach ($szinek as $szin) { ?>
                <?php if ($this->config->get('config_language_id') == $szin['language_id']) { ?>
                    var mehet = true;
                    if (tombszamlal > 0) {
                        for (var i in selectkivet) {
                            if (selectkivet[i] == <?php echo $szin['option_szin_id']; ?>) {
                                mehet = false;
                                break;
                            }
                        }
                    }

                    if (mehet) {
                        var nev = "<?php echo $szin['name']; ?>"
                        if (nev.length == 0 ){
                            nev = "Általános"
                            html1 += '<option style="background: red; color: white;" value="<?php echo $szin['option_szin_id']; ?>">'+nev+'</option>';
                        } else {
                            html1 += '<option value="<?php echo $szin['option_szin_id']; ?>">'+nev+'</option>';

                        }
                    }
                <?php }?>
            <?php } ?>

            html1 += "</select>";
            $('#selectem').html(html1);

            $('#szuro_hozzaad').keypress(function(e){
                if(e.which == 13)
                    $('#szuro_hozzaad').click();
            });
        }

        function optionMentes() {

            var utolso = false;
            var elso = true;
            var datatombatad = $('input[type=\'text\']:enabled, input[type=\'hidden\'], input[type=\'radio\']:checked, input[type=\'checkbox\']:checked, select:enabled, textarea');
            debugger;

            if (datatombatad.length){
                var i = 0;
                while(true) {
                    if (i > datatombatad.length) {
                        break;
                    }
                    var atkuld = datatombatad.slice(i,i+900);
                    ajaxhivasOption(atkuld,utolso,elso);
                    elso = false;
                    i = i+900;
                }
            }
            ajaxhivasOption('',true,elso);
            location = 'index.php?route=catalog/option&token=<?php echo $token.$url; ?>';
        }

        function ajaxhivasOption(tomb,utolso,elso) {
            var vissza = true;
            $.ajax({
                url: 'index.php?route=catalog/option/ajaxMentes&token=<?php echo $token; ?>&option_id=<?php echo isset($this->request->get['option_id']) ? $this->request->get['option_id'] : ''?>&utolso='+utolso+'&elso='+elso,
                type: "post",
                data: tomb,
                dataType: 'json',
                async: false,

                success: function(json) {
                    vissza = true;
                },
                error: function(e) {
                    vissza = false;
                }
            });
            return vissza;
        }



        //--></script>

<?php echo $footer; ?>