<?php
// Heading
$_['heading_title']       = 'NMS net 3 az 1-ben Képsor';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeres képsor módosítás!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal Oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_pleaseselect']     = 'Válassz';
$_['text_pause']      = 'Szünet ha az egérrel rámegy: ';
// Entry
$_['entry_banner']        = 'Banner:';
$_['entry_dimension']     = 'Felbontás (SZ x M):';
$_['entry_layout']        = 'Kiosztás:';
$_['entry_position']      = 'Pozició:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_delay']      = 'Szünet az átmenetek közt (miliszek): ';
$_['entry_speed']      = 'Animációs átmeneti képsor sebessége (miliszek):';
$_['entry_pause']      = 'Szünet ha az egér rámegy: ';
$_['entry_effect']      = 'Átmeneti hatás: ';
$_['entry_slices']      = 'Szeletek: <br /><span class="help">A Szelet animációknak.</span>';
$_['entry_boxcolumns']   = 'Oszlop dobozok száma: <br /><span class="help">Box animációknak.</span>';
$_['entry_boxrows']      = 'Sorok száma: <br /><span class="help">Box animációknak.</span>';
$_['entry_accordion_max']      = 'Max Szélesség: <br /><span class="help">A szélessége egy teljesen kinyílt képnek.</span>';
$_['entry_accordion_sticky'] = 'Ragadás: <br /><span class="help">Egy kép mindíg ki lesz nyitva ha ez az érték igazon-van.</span>';
$_['entry_accordion_event']      = 'Esemény: <br /><span class="help"> Az esemény ami beindítja a kinyílási hatást.</span>';	

// Error
$_['error_permission']    = 'Nincs módosítási engedélye!';
$_['error_dimension']     = 'Szélesség &amp; Magasság méretek szükségesek!';
?>