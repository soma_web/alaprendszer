<?php
// Heading
$_['heading_title']   = 'Megnézett kuponok';


// Text
$_['text_account']    = 'Fiók';
$_['text_home']       = 'Webáruház';
$_['button_reset']    = 'Visszaállít';
$_['column_name']     = 'Termék neve';
$_['column_model']    = 'Modell';
$_['column_viewed']   = 'Megnézett';
$_['column_percent']  = 'Százalék';
$_['text_no_results']  = 'Nincs megnézett termék';
?>