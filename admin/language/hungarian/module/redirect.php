<?php
// Heading
$_['heading_title']     		  = 'Átirányítás nem létező oldalak';
$_['heading_title_exception']     = 'Átirányítás nem létező oldalak - Kivételek';

// Текст
$_['text_tracking']    	    = 'Követés:';
$_['text_enabled']     	    = 'Engedélyezve';
$_['text_disabled']     	= 'Letiltva';
$_['text_success']      	= 'Kész változtatás jogát fenntartjuk!';
$_['text_save']         	= 'Mentés...';
$_['text_save_redirects'] 	= 'Átirányítások mentése';
$_['text_exceptions']   	= 'Kivételek';
$_['text_module']   	    = 'Modulok';


// Column
$_['column_old_url']     = 'Eredeti URL';
$_['column_new_url']     = 'Új URL';
$_['column_referer']     = 'Hivatkozás';
$_['column_date_added']  = 'Beszúrás';
$_['column_action']      = 'Mentés';
$_['column_status']      = 'Státusz';

// Entry
$_['entry_old_url']    = 'Eredeti URL';
$_['entry_new_url']    = 'Új URL';
$_['entry_status']     = 'Státusz:';
$_['entry_exceptions'] = 'Kivételek:<br /><span class="help">A szavak listáját.Ha jelen van az URL-ben, akkor nem kell rögzíteni. A szavakat enterrel kell elválasztani.</span>';

// Error
$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága módosítani az átirányítás modult!';

$_['error_exists']     = 'Figyelmeztetés: a cím már létezik!';
$_['error_url']   	   = 'Figyelmeztetés: a cím minimum 3, maximum 255 karakter legyen!';
$_['error_save']       = 'Hiba történt, kérjük, próbálja meg később!';