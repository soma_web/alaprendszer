<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error) { ?>
  <div class="warning"><?php echo $error; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="buttons"><a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
            </div>
            <!--<a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>-->

    </div>
    <div class="content">
      <table class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $column_from; ?></td>
            <td class="left"><?php echo $column_to; ?></td>
            <td class="left"><?php echo $column_value; ?></td>
            <td class="right"><span style="padding: 0 15px 0 0"><?php echo $column_torol; ?></span> <?php echo $column_modosit; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($biztositasok) { ?>
          <?php foreach ($biztositasok as $biztositas) { ?>
          <tr class=sor>
            <td class="left"><?php echo number_format($biztositas['biztositas_tol'],0,"",".")." Ft"; ?></td>
            <td class="left"><?php echo number_format($biztositas['biztositas_ig'],0,"",".")." Ft"; ?></td>
            <td class="left"><?php echo number_format($biztositas['biztositas_ertek'],0,"",".")." Ft"; ?></td>
            <td class="right">  <a style="padding: 0 15px 0 0" href="<?php echo $biztositas['torol'] ?>"><?php echo $column_torol ?></a>
                                <a href="<?php echo $biztositas['action'] ?>"><?php echo $column_modosit ?></a></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php echo $footer; ?>