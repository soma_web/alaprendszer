<?php
/* K&H bankkártya elfogadást megvalósító osztály
 * @author Istvan Dobrentei http://dobrenteiistvan.hu
 *
 * timeout: http://<url>/index.php?route=payment/kandh/handleTimeout
 * required: curl, openssl
 * */

class ControllerPaymentKandH extends Controller 
{
	const KANDH_LOG_TABLE_NAME 			= 'kandh_log';
	const KANDH_TEST_REQUEST_URL		= 'https://ebank.khb.hu/PaymentGatewayTest/PGPayment?';
	const KANDH_TEST_RESULT_URL 		= 'https://ebank.khb.hu/PaymentGatewayTest/PGResult?';
	const KANDH_LIVE_REQUEST_URL		= 'https://ebank.khb.hu/PaymentGateway/PGPayment?';
	const KANDH_LIVE_RESULT_URL 		= 'https://ebank.khb.hu/PaymentGateway/PGResult?';
	
	/* timeout value in minutes */
	const KANDH_TIMEOUT 				= 26;
	
	/* TRANSACTION TYPES */
	const KANDH_SELLING					= 'PU';
	const KANDH_REVERSE 				= 'RE';
	
	/* BANK RESPONSE CODES */
	const KANDH_UNSUCCESSFULL_PAYMENT 	= 'NAK';
	const KANDH_UNKNOWN_TRID 			= 'UTX';
	const KANDH_NORESULT_YET 			= 'PEN';
	const KANDH_ERROR_DURING_PAYMENT 	= 'ERR';
	const KANDH_CANCEL_PAYMENT 			= 'CAN';
	const KANDH_EXPIRED_CARD 			= 'EXP';
	const KANDH_SUCCESSFULL_PAYMENT 	= 'ACK';
	const KANDH_REVERSE_IN_PROGRESS		= 'PE2';
	const KANDH_REVERSE_DONE			= 'VOI';
	const KANDH_TIMEOUT_PAYMENT			= 'START';	//Amikor a banki fizetőoldalon nem csinálnak semmit, és bezárják a böngészőt
	const KANDH_TIMEOUT_PROCESSED		= 'TO_PR';	//Amikor a cron feldolgozta a timeout -ot és küldött levelet
	const KANDH_CANCEL_PROCESSED		= 'CAN_PR'; //Amikor a cron feldolgozta a mégsem gomra kattintást és küldött levelet
	
	private $_keyfile;
	
	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->_keyfile = '';
		if($this->config->get('kandh_prod_key_file'))
		{
			$this->_keyfile = DIR_SYSTEM . "config/" . $this->config->get('kandh_prod_key_file');
		}
	}
	
	public function index()
	{
		$this->language->load('payment/kandh');
		$this->data['error'] = '';
		$url = "";
		$trid = "";
		if($this->request->post)
		{
			$this->load->model('checkout/order');
			$orderId = (int)$this->request->post['order_id'];
			$userId = (int)$this->request->post['user_id'];
			$order = $this->model_checkout_order->getOrder($orderId);	
			$type = isset($this->request->post['transaction_type']) && ($this->request->post['transaction_type'] == self::KANDH_REVERSE) ? $this->request->post['transaction_type'] : self::KANDH_SELLING;
			$amount = $this->currency->format($order['total'], $order['currency_code'], $order['currency_value'], false) * 100;
			$trid = $this->genTrid();
			$url = $this->getUrlData($trid, $type, $amount);
			$this->db->query("INSERT INTO " . self::KANDH_LOG_TABLE_NAME . " (trid, order_id, status) VALUES('" . $this->db->escape($trid) . "', '" . (int)$orderId . "', 'START')");
			$this->redirect($url);
		}


    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/kandh/kandh_logo.jpg')) {
        $this->data['kandh_logo'] = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/kandh/kandh_logo.jpg';
    } elseif(file_exists(DIR_TEMPLATE_IMAGE.'default/image/kandh/kandh_logo.jpg')) {
        $this->data['kandh_logo'] = DIR_TEMPLATE_IMAGE.'default/image/kandh/kandh_logo.jpg';
    } else {
        $this->data['kandh_logo'] = false;
    }

	
		$this->language->load('payment/kandh');
    	$this->data['button_confirm'] = $this->language->get('button_confirm');
		$this->data['back'] = $this->url->link('checkout/checkout');
		$this->data['text_sending_code'] = $this->language->get('text_sending_code');
        $this->data['text_kandh_information'] = $this->language->get('text_kandh_information');
		$this->data['text_sending_warning'] = $this->language->get('text_sending_warning');
		$this->data['order_id'] = $this->session->data['order_id'];
		$this->data['user_id'] = $this->customer->getId();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/kandh.tpl')) 
		{
            $this->template = $this->config->get('config_template') . '/template/payment/kandh.tpl';
		} else 
		{
            $this->template = 'default/template/payment/kandh.tpl';
        }
		$this->response->setOutput($this->render());	
	}
	
	
	private function getStatusMsg($statusCode)
	{
		$this->language->load('payment/kandh');
		$bankMsg = "";
		switch($statusCode)
			{
				case self::KANDH_UNSUCCESSFULL_PAYMENT :
					$bankMsg = $this->language->get('text_unsuccessfull_payment');
					break;
					
				case self::KANDH_EXPIRED_CARD:
					$bankMsg = $this->language->get('text_expired_card');
					break;
				
				case self::KANDH_ERROR_DURING_PAYMENT:
					$bankMsg = $this->language->get('text_error_during_payment');
					break;
				
				case self::KANDH_CANCEL_PAYMENT:
					$bankMsg = $this->language->get('text_cancel_payment');
					break;
				
				case self::KANDH_UNKNOWN_TRID :
					$bankMsg = $this->language->get('text_unknown_trid');
					break;
				
				case self::KANDH_NORESULT_YET :
					$bankMsg = $this->language->get('text_noresult_yet');
					break;
				
				case self::KANDH_TIMEOUT_PAYMENT:
					$bankMsg = $this->language->get('text_timeout_payment');
					break;
		}
		return $bankMsg;
	}
	
	/* Ezt hivja meg a bank a felhasznalo visszairányításakor, miután megadta a kártyaadatokat
	 * 
	 * */
	public function confirm() 
	{
		$trid = $this->request->get['txid'];
		if($trid)
		{
			$this->language->load('payment/kandh');
			$orderId = $this->getOrderIdByTransaction($trid);
			$this->load->model('checkout/order');
			$order = $this->model_checkout_order->getOrder($orderId);
			$url = $this->config->get('kandh_is_test') ? self::KANDH_TEST_RESULT_URL : self::KANDH_LIVE_RESULT_URL;
			$url .= "mid=" . $this->config->get('kandh_shop_code') . "&txid=" . $trid;
			$response = $this->fetchPage($url);
			$answers = preg_split('/[\n\r]+/', $response);
			$statusCode = isset($answers[0]) ? trim($answers[0]) : "";
			$bankResultCode = isset($answers[3]) ? trim($answers[3]) : "";
			$this->logKandH($trid, $statusCode, $bankResultCode);
			$bankMsg = $this->getStatusMsg($statusCode);
			$this->data['trid'] = utf8_encode($trid);
			$this->data['anum'] = utf8_encode($bankResultCode);
			$this->data['amo'] = $this->currency->format($order['total'], $order['currency_code'], $order['currency_value'], false) . " " . $order['currency_code'];
			
			if($statusCode == self::KANDH_SUCCESSFULL_PAYMENT)
			{	
    			$this->data['text_message'] = sprintf($this->language->get('text_success_customer'),
    															$this->data['trid'],
    															$this->data['anum'],
    															$this->data['amo'],
    															$this->url->link('account/account', '', 'SSL'), 
    															$this->url->link('account/order', '', 'SSL'), 
    															$this->url->link('account/download', '', 'SSL'), 
    															$this->url->link('information/contact'));
				
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/kandh_success.tpl')) 
        		{
            		$this->template = $this->config->get('config_template') . '/template/payment/kandh_success.tpl';
				} 
				else 
				{
            		$this->template = 'default/template/payment/kandh_success.tpl';
        		}
        		
        		/* a rendelés állapotának átállítása és értesítés küldése */
        		$comment = 	$this->language->get('text_bankpayment_success') . "<br />" . 
        					$this->language->get('text_bankauth_number') . " " . $bankResultCode . " <br />" .
        					$this->language->get('text_trid_number') . " " . $trid . "<br />" .
        					$this->language->get('text_amount') . $this->data['amo'];
        		$this->model_checkout_order->confirm($orderId, $this->config->get('kandh_order_status_id'), $comment, true);
        		
        		/* minden változó ürítése */
        		$this->finishCheckout();
			}
			else
			{


				$this->data['text_message'] = sprintf($this->language->get('text_failed_customer'),
    															$this->data['trid'],
    															$this->data['anum'],
    															$this->data['amo'],
    															$this->url->link('account/account', '', 'SSL'), 
    															$this->url->link('account/order', '', 'SSL'), 
    															$this->url->link('account/download', '', 'SSL'), 
    															$this->url->link('information/contact'));
    			
    			$this->data['checkout_link'] = $this->url->link('checkout/checkout');
				$this->data['button_checkout_again'] = $this->language->get('button_checkout_again');									
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/kandh_failed.tpl')) 
        		{
            		$this->template = $this->config->get('config_template') . '/template/payment/kandh_failed.tpl';
				}
				else
				{
            		$this->template = 'default/template/payment/kandh_failed.tpl';
        		}
        		
        		/* a rendelés állapotának átállítása és értesítés küldése */
        		$comment = 	$this->language->get('text_bankpayment_failed') . "<br />" .
        					$this->language->get('text_failedpayment_reason') . " " . $bankMsg . "<br />" .
        					$this->language->get('text_bankauth_number') . " " . $bankResultCode . " <br />" .
        					$this->language->get('text_trid_number') . " " . $trid;
        		$this->model_checkout_order->confirm($orderId, $this->config->get('kandh_failed_status'), $comment, true);	
			}
			
			$this->document->setTitle($this->language->get('heading_title'));
			$this->data['breadcrumbs'] = array(); 

			$this->data['breadcrumbs'][] = array(
			        	'href'      => $this->url->link('common/home'),
			        	'text'      => $this->language->get('text_home'),
			        	'separator' => false
			); 
						
			$this->data['breadcrumbs'][] = array(
			        	'href'      => $this->url->link('checkout/cart'),
			        	'text'      => $this->language->get('text_basket'),
			        	'separator' => $this->language->get('text_separator')
			);
								
			$this->data['breadcrumbs'][] = array(
						'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
						'text'      => $this->language->get('text_checkout'),
						'separator' => $this->language->get('text_separator')
			);	
										
			$this->data['heading_title'] = $this->language->get('heading_title_checkout');
    		$this->data['button_continue'] = $this->language->get('button_continue');
    		$this->data['continue'] = $this->url->link('common/home');
        			
        	$this->children = array(
						'common/column_left',
						'common/column_right',
						'common/content_top',
						'common/content_bottom',
						'common/footer',
						'common/header'			
			);
				
        	$this->response->setOutput($this->render());
		}
	}
	
	/* Timeout lekérdezése, majd az ügyfél értesítése
	 * ezt kell cron ból meghívni percenként
	 * Kigyűjti az összes 11 (állítható) perccel ezelőtti tranzakciókat, lekérdez és/vagy értesíti a vásárlót a tranzakció eredményéről  
	 * */
	public function handleTimeout()
	{
		$this->language->load('payment/kandh');
		$dateTime = new DateTime("Europe/Budapest");
		$dateTime->modify('-' . self::KANDH_TIMEOUT . ' minutes'); 
		$affected = $dateTime->format('Y-m-d H:i:s');
		$query = $this->db->query("SELECT * FROM " . self::KANDH_LOG_TABLE_NAME . " 
									WHERE (status='" . self::KANDH_TIMEOUT_PAYMENT . "' OR status='" . self::KANDH_NORESULT_YET . "') 
									AND created < '" . $affected . "' GROUP BY trid");
		if($query->num_rows)
		{
			$this->load->model('checkout/order');
			foreach($query->rows as $row)
			{
				$bankMsg = $this->getStatusMsg($row['status']);
				/* ertesito level a timeoutrol */
				if($row['status'] == self::KANDH_CANCEL_PAYMENT)
				{
					/* a megsem gombot nyomtak meg */
					$comment = 	$this->language->get('text_bankpayment_failed') . "<br />" .
        						$this->language->get('text_failedpayment_reason') . " " . $bankMsg . "<br />" .
        						$this->language->get('text_trid_number') . " " . $row['trid'];
					$this->model_checkout_order->confirm($row['order_id'], $this->config->get('kandh_failed_status'), $comment, true);
					$this->logKandH($row['trid'], self::KANDH_CANCEL_PROCESSED);
				}
				elseif(($row['status'] == self::KANDH_NORESULT_YET) || ($row['status'] == self::KANDH_TIMEOUT_PAYMENT))
				{
					/* ha a tranzakcio sikeres volt es bezartak a bongeszot vagy meg nincs eredmenye a tranzakcionak */
					/* a tranzakcio eredmenyenek elkerese a banktol, log status atallitasa, ertesito level a valtozasrol */
					$order = $this->model_checkout_order->getOrder($row['order_id']);
					$url = $this->config->get('kandh_is_test') ? self::KANDH_TEST_RESULT_URL : self::KANDH_LIVE_RESULT_URL;
					$url .= "mid=" . $this->config->get('kandh_shop_code') . "&txid=" . $row['trid'];
					$response = $this->fetchPage($url);
					$answers = preg_split('/[\n\r]+/', $response);
					$statusCode = isset($answers[0]) ? trim($answers[0]) : "";
					$bankResultCode = isset($answers[3]) ? trim($answers[3]) : "";
					$this->logKandH($row['trid'], $statusCode, $bankResultCode);
					$bankMsg = $this->getStatusMsg($statusCode);
					if($statusCode == self::KANDH_SUCCESSFULL_PAYMENT)
					{
						$comment = 	$this->language->get('text_bankpayment_success') . "<br />" . 
        							$this->language->get('text_bankauth_number') . " " . $bankResultCode . " <br />" .
        							$this->language->get('text_trid_number') . " " . $row['trid'] . "<br />" .
        							$this->language->get('text_amount') . $this->currency->format($order['total'], $order['currency_code'], $order['currency_value'], false) . " " . $order['currency_code'];
						$this->model_checkout_order->confirm($row['order_id'], $this->config->get('kandh_order_status_id'), $comment, true);
					}
					else
					{
						$comment = 	$this->language->get('text_bankpayment_failed') . "<br />" .
        							$this->language->get('text_failedpayment_reason') . " " . $bankMsg . "<br />" .
        							$this->language->get('text_bankauth_number') . " " . $bankResultCode . " <br />" .
        							$this->language->get('text_trid_number') . " " . $row['trid'];
						$this->model_checkout_order->confirm($row['order_id'], $this->config->get('kandh_failed_status'), $comment, true);
					}
				}
			}
		}
	}
	
	protected function getUrlData($trid, $type, $amount)
	{
		$url = $this->config->get('kandh_is_test') ? self::KANDH_TEST_REQUEST_URL : self::KANDH_LIVE_REQUEST_URL;
		$data = "mid=" . $this->config->get('kandh_shop_code') .
				"&txid=" . $trid .
				"&type=" . $type .
				"&amount=" . $amount .
				"&ccy=" . $this->config->get('kandh_payment_currency');
		$url .= $data . "&lang=" . $this->config->get('kandh_payment_language');
		
		if($this->config->get('kandh_prod_key_file'))
		{
			$file = DIR_SYSTEM . "config/" . $this->config->get('kandh_prod_key_file');
			$url .= "&sign=" . $this->getSign($file, $data);
		}
		else
		{
			$url .= "&sign=1224545&nocheck=1";
		}
		
		return $url;
	}

    /* https://ebank.khb.hu/PaymentGateway/PGPayment?
    mid=84033048
    &txid=1347386958
    &type=PU
    &amount=999000
    &ccy=HUF
    &lang=HU
    &sign=534221b2d2de1044b36d05432ea47c8fd7371a9357dde561f8ec068446550a4081e782d63750e5d0d1ba6839c40ce31a716b890fa1f80d3f710c713d7de0c02c19737a7a9078ad9e20fd73fb09238ca265aaefce3134bdf367db616cd3d7638d29dd41d4038c13ebda620f8b4a5c7ad38a4f563e15a8f1325357118ed54d205b */
	/*  */
	protected function getSign($file, $data)
	{
		$signature = "";
		$fp = fopen($file, "r");
		$priv_key = fread($fp, 8192);
		fclose($fp);
		$pkeyid = openssl_get_privatekey($priv_key);
		openssl_sign($data, $signature, $pkeyid);
		openssl_free_key($pkeyid);
		
		return bin2hex($signature);
	}
	
	/* A success folyamat lemásolása (lecserélése) a banki információk megjelenítése miatt
	 * elvileg lehetne vqmodba is tenni de most itt van, nem zavar vizet
	 * 
	 * */
	private function finishCheckout()
	{
		if (isset($this->session->data['order_id'])) {
			$this->cart->clear();
			
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);	
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
		}	
	}
	
	/* Egyedi tranzakció azonosító generálása
	 * 10 számjegyű egyedi azonositó, ami még nem szerepel az áruházban
	 * 
	 * return trid string
	 * */
	private function genTrid()
	{
		$trid = "";
		do
		{
			for($i=1;$i<11;$i++)
			{
				$num = rand(1,9);
				$trid .= $num;
			}
		}
		while($this->isExistsTrid($trid));
		
		return $trid;
	}
	
	/* Leellenőrizzük, hogy létezik-e már
	 * 
	 * param trid string
	 * return boolean
	 * */
	private function isExistsTrid($trid)
	{
		$query = $this->db->query("SELECT * FROM " . self::KANDH_LOG_TABLE_NAME . " WHERE trid='" . $trid . "'");
		if ($query->num_rows) 
		{
			return true;
		}
		
		return false; 
	}
	
	/* A tranzakcióhoz tartozó eseményt loggolja az adatbázisba
	 * 
	 * */
	private function logKandH($trid, $statusCode, $authNo=0)
	{	
		if(!empty($authNo))
		{
			$this->db->query("UPDATE " . self::KANDH_LOG_TABLE_NAME . " SET auth_no='" . $this->db->escape($authNo) . "', status='" . $this->db->escape($statusCode) . "' WHERE trid='" . $this->db->escape($trid) . "'");
		}
		else
		{
			$this->db->query("UPDATE " . self::KANDH_LOG_TABLE_NAME . " SET status='" . $this->db->escape($statusCode) . "' WHERE trid='" . $this->db->escape($trid) . "'");	
		}
	}
	
	private function fetchPage($url, $timeOut=60) 
	{
		$content = "";
		$cp = curl_init();
		curl_setopt($cp, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($cp, CURLOPT_URL, $url);
		curl_setopt($cp, CURLOPT_TIMEOUT, $timeOut);
		$content = curl_exec($cp);
		curl_close($cp);

 		return $content;
	}
	
	private function getOrderIdByTransaction($trid)
	{
		$query = $this->db->query("SELECT order_id FROM " . self::KANDH_LOG_TABLE_NAME . " WHERE trid='". $this->db->escape($trid) . "' GROUP BY 1");
		
		return isset($query->row['order_id']) ? (int)$query->row['order_id'] : 0;
	} 
	
	public function install()
	{
		$query = $this->db->query("CREATE TABLE " . self::KANDH_LOG_TABLE_NAME . " (
  									`id` INT NOT NULL AUTO_INCREMENT ,
  									`trid` VARCHAR(10) NOT NULL ,
  									`status` VARCHAR(6) DEFAULT NULL ,
  									`auth_no` VARCHAR(20) DEFAULT NULL ,
  									`order_id` INT NOT NULL ,
  									`created` TIMESTAMP NOT NULL DEFAULT NOW() ,
  									PRIMARY KEY (`id`) ,
  									INDEX `idx_trid` (`trid` ASC) )
									ENGINE = MyISAM
									DEFAULT CHARACTER SET = utf8
									COLLATE = utf8_hungarian_ci");
	}
}
?>