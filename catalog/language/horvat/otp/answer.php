<?php


$_['heading_title'] = "Vásárlás Összesítő";

$_['text_thank'] = "<p>Köszönjük a vásárlást!</p>";

$_['text_dear_customer'] = "Kedves Látogatónk,";

$_['text_details'] = "<p>Köszönjük, hogy felkereste webáruházunkat és megrendelésével megtisztelt bennünket. A megrendelés visszaigazolását e-mailben elküldtük az Ön által megadott címre. A megrendelés teljesítésének pontos időpontjáról még tájékoztató e-mailt küldünk.</p>";

$_['text_transaction_id']   = 'Az Ön tranzakció azonositója:';

$_['text_see_you_soon'] = '<p>Reméljük hamarosan újra viszontláthatjuk áruházunkban, elégedett vevőink között.</p>';


$_['text_cancelled'] = "<p>Ön elállt a vásárlástól!</p>";

$_['text_no_purchase'] = "<p>Nem történt vásárlás.</p>";

$_['text_error'] = "<p>Hiba:</p>";

$_['text_store_error'] = "<p>Hiba történt a vásárlás indatása vagy adatainak feldolgozása során.</p>";

$_['text_bank_error'] = "<p>A tranzakció nem hajtódott végre, a banki válasz nem érhető el vagy nem értelmezhető.</p>";




?>