<?php  function alap_informaciok($tomb) { ?>
    <div class="column">
         <h3><?php echo $tomb['title']; ?></h3>
         <ul>
             <?php if(isset($tomb['adatok']) && !empty($tomb['adatok'])) { ?>
                <?php foreach ($tomb['adatok'] as $information) { ?>
                     <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                <?php } ?>
             <?php } ?>
         </ul>
    </div>
<?php  } ?>

<?php  function alap_kategoriak($tomb) { ?>
    <div class="column">
        <h3><?php echo $tomb['title']; ?></h3>
        <ul class="footer_inside_category" >
            <?php foreach ($tomb['categories'] as $category) { ?>
                <li>
                    <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>

                    <?php if (!empty($category['children'])) { ?>
                        <ul>
                            <?php foreach ($category['children'] as $child) { ?>
                                <li>
                                    <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php  } ?>

<?php  function alap_ugyfelszol($tomb) { ?>
    <div class="column">
        <h3><?php echo $tomb['adatok']['title']; ?></h3>
        <div>
            <?php echo html_entity_decode($tomb['adatok']['href']);?>
        </div>
    </div>
<?php  } ?>

<?php  function alap_extrak($tomb) { ?>
    <div class="column">
        <h3><?php echo $tomb['adatok']['title']; ?></h3>
        <div>
            <?php echo html_entity_decode($tomb['adatok']['href']);?>
        </div>
    </div>
<?php  } ?>

<?php  function alap_egyeb($tomb) { ?>
    <div class="column">
        <h3><?php echo $tomb['adatok']['title']; ?></h3>
        <div>
            <?php echo html_entity_decode($tomb['adatok']['href']);?>
        </div>
    </div>
<?php  } ?>

<?php  function alap_fiok($tomb) { ?>
    <div class="column">
        <h3><?php echo $tomb['adatok']['title']; ?></h3>
        <div>
            <?php echo html_entity_decode($tomb['adatok']['href']);?>
        </div>
    </div>
<?php  } ?>

<?php  function alap_uzenet($tomb) { ?>
    <div class="column">
        <h3><?php echo $tomb['adatok']['text_message']; ?></h3>
        <ul>
            <li>
                <form action="index.php?route=information/contact" method="post">

                    <input type="text" name="email" id="email_address" placeholder="<?php echo $tomb['adatok']['text_email']; ?>">
                    <textarea value="" name="enquiry" id="messagebox" placeholder="<?php echo $tomb['adatok']['text_content']; ?>"></textarea>
                    <input type="submit" id="submit" value="<?php echo $tomb['adatok']['text_submit']; ?>">
                    <input type="hidden" id="" name="parameter" value="true ">
                </form>
            </li>
        </ul>
    </div>
<?php  } ?>

<?php  function alap_hirlevel($tomb) { ?>
    <div class="column">
        <h3><?php echo $tomb['adatok']['text_newsletter']; ?></h3>
        <ul>
            <li>
                <form id="newsletter1" name="newsletter1">
                    <input type="text" value="<?php echo $tomb['adatok']['text_name']; ?>" name="subscribe_name" id="newsletter_name" onclick="document.getElementById('newsletter_name').value=''" /><br /><br />
                    <input type="text" value="<?php echo $tomb['adatok']['text_email']; ?>" name="subscribe_email" id="newsletter_email" onclick="document.getElementById('newsletter_email').value=''" /><br /><br />
                    <input type="button" value="<?php echo $tomb['adatok']['text_submit']; ?>"  onclick="email_subscribe_footer()" id="newsletter_submit" />
                </form>
                <div id="subscribe_result_footer"></div>
            </li>
        </ul>

    </div>
<?php  } ?>

<?php  function alap_likebox($tomb) { ?>
    <div class="column">
        <?php echo html_entity_decode($tomb['adatok']['likebox_code'])?>
    </div>
<?php  } ?>

<?php  function tg_helios_footer_default($tomb, $alap_footer) { ?>

        <?php  foreach( $alap_footer as $value) {
            $value['fuggveny']($value['adatok']);
        }

        ?>

<?php } ?>


<?php
function tg_helios_footer_info($tomb){
    ?>
    <div id="tg_helios_footer_info" style="width: <?php echo $tomb['width']?>px; height:  <?php echo $tomb['height']?>px";>

        <h3>
            <?php echo $tomb['mymodule_title']; ?>
        </h3>
        <div>
            <?php echo html_entity_decode($tomb['mymodule_code']);?>
        </div>
    </div>

<? }?>

<?php
function tg_helios_footer_contact($tomb){
    ?>
    <div id="tg_helios_footer_contact" style="width: <?php echo $tomb['width']?>px; height:  <?php echo $tomb['height']?>px; display: table";>

        <h3>
            <?php echo $tomb['mymodule_title2']; ?>
        </h3>


        <div class="column2" >
            <ul>
                <?php if ($tomb['telefon_show'] == "1") { ?>
                    <li><span class="ceg_ikon"><img src="catalog/view/theme/hadrianus/image/homephone-iconnew.png" alt="" /></span>
                        <span class="ceg_cime"><?php echo $tomb['telefon']; ?></span>
                        <span style="clear: both"></span>
                    </li>
                <?php } ?>

                <?php if ($tomb['mobile_show'] == "1") { ?>
                    <li><span class="ceg_ikon"><img src="catalog/view/theme/hadrianus/image/phone-icon.png" alt="" /></span>
                        <span class="ceg_cime"><?php echo $tomb['mobile']; ?></span>
                        <span style="clear: both"></span>
                    </li>
                <?php } ?>

                <?php if ($tomb['email_show'] == "1") { ?>
                    <li><span class="ceg_ikon"><img src="catalog/view/theme/hadrianus/image/email-icon.png" alt="" /></span>
                        <span class="ceg_cime"><?php echo $tomb['email']; ?></span>
                        <span style="clear: both"></span>
                    </li>
                <?php } ?>

                <?php if ($tomb['skype_show'] == "1") { ?>
                    <li><span class="ceg_ikon"><img src="catalog/view/theme/hadrianus/image/skype-icon.png" alt="" /></span>
                        <span class="ceg_cime"><?php echo  $tomb['skype']; ?></span>
                        <span style="clear: both"></span>
                    </li>
                <?php } ?>

                <?php if ($tomb['address_show'] == "1") { ?>
                    <li><span class="ceg_ikon"><img src="catalog/view/theme/hadrianus/image/address-icon.png" alt="" /></span>
                        <span class="ceg_cime"><?php echo  $tomb['address']; ?></span>
                        <span style="clear: both"></span>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<? }?>

<?php
function tg_helios_footer_twitter($tomb){
    ?>
    <div id="tg_helios_footer_twitter" style="width: <?php echo $tomb['width']?>px; height:  <?php echo $tomb['height']?>px";>

        <h3>
            <?php echo $tomb['mymodule_title3']; ?>
        </h3>
        <div id="twitter" >
            <ul id="twitter_update_list"></ul>
            <script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
            <script type="text/javascript" src="http://twitter.com/statuses/user_timeline/<?php echo $tomb['username']; ?>.json?callback=twitterCallback2&amp;count=<?php echo $tomb['tweets']; ?>"></script>
        </div>
    </div>
<?php } ?>

<?php
function tg_helios_footer_fbfanpage($tomb){
    ?>
    <div id="tg_helios_footer_fbfanpage" style="width: <?php echo $tomb['width']?>px; height:  <?php echo $tomb['height']?>px";>

        <h3>
            <?php echo $tomb['mymodule_title4']; ?>
        </h3>
        <div class="s_widget_holder">
            <fb:fan profileid="<?php echo $tomb['fpid']; ?>" stream="0" logobar="0" width="200px" css="<?php echo HTTPS_SERVER; ?>catalog/view/theme/hadrianus/stylesheet/facebook.css.php?370"></fb:fan>
        </div>
    </div>

<?php } ?>

<?php
function tg_helios_footer_terkep($tomb){
    ?>
    <div id="footer_terkep" style="width: <?php echo $tomb['width']?>px; height:  <?php echo $tomb['height']?>px";>
            <?php echo $tomb['terkep'] ?>
    </div>
<?php }?>


<?php
function tg_helios_footer_fbgomb($tomb){
    ?>
    <div id="footer_facebook_button" style="width: <?php echo $tomb['width']?>px; height:  <?php echo $tomb['height']?>px";>

        <? for ($i=0; $i<= 10; $i++){
            if ($tomb['gomb'.$i.'_show'] == '1'){
                echo $tomb['gomb'.$i];
            }
        } ?>
    </div>

<?php }?>


<?php
function tg_helios_footer_egyeb($tomb){
    ?>
    <div id="footer_facebook_box">
        <?php echo $tomb['box'] ?>
    </div>
<?php }?>










    <?php if($tg_helios_footer_status == '1'){ ?>
        <div id="footer">
                <?php if (  $footer_default['status'] == 1 && $footer_default['megjelenes_alul'] == 0 && $alap_footer != null){
                    echo '<div id="bottomwrapper">';
                        tg_helios_footer_default($footer_default['valtozok'], $alap_footer);
                    echo '</div>';
                } ?>


                <div class="footer-holder footer-area footer-wrapper">
                    <?php
                    $oszlop = -10000000;
                    $elso=true;

                    foreach($footer_beallitasok as $value){
                        if( $oszlop < $value['sort'] ){
                            $oszlop = $value['sort'];
                            if (!$elso){
                                echo "</div>";
                            }
                            echo "<div class=footer_doboz >";

                            $elso = false;
                        }

                        $value['module_name']($value);
                    }

                    if (!$elso){
                        echo "</div>";
                    }
                    ?>
                </div>

                <? if (  $footer_default['status'] == 1 && $footer_default['megjelenes_alul'] == 1 && $alap_footer != null){
                    echo '<div id="bottomwrapper">';
                tg_helios_footer_default($footer_default['valtozok'], $alap_footer);
                    echo '</div>';
                } ?>
        </div>
    <?php }; ?>


<script>
    var szelesseg   = <?php echo isset($tg_helios_footer_terkep_szelesseg) && $tg_helios_footer_terkep_szelesseg ? $tg_helios_footer_terkep_szelesseg : 0 ;?>;
    var magassag    = <?php echo isset($tg_helios_footer_terkep_magassag) && $tg_helios_footer_terkep_magassag ? $tg_helios_footer_terkep_magassag : 0;?>;
    $("#footer_terkep iframe").css("width",szelesseg+"px");
    $("#footer_terkep iframe").css("height",magassag+"px");
</script>
<script>

    function email_subscribe_footer(){
        $.ajax({
            type: 'post',
            url: 'index.php?route=module/newslettersubscribe/subscribe&footer_result_div=subscribe_result_footer',
            dataType: 'html',
            data:$("#newsletter1").serialize(),
            success: function (html) {
                debugger;
                eval(html);
            }});
    }
</script>
