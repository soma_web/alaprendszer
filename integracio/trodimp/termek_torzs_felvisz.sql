BEGIN
DECLARE VasarolhatoKod    INT(1) default 5;
DECLARE product_azonosito INT(11) default 0;
DECLARE azonosito         INT(11) default 0;
DECLARE kategoriaNeve     varchar(255) default "";

IF (uj_Torolve=0) THEN
IF (uj_Statusza="V") THEN
SET VasarolhatoKod:=7;
END IF;

call termek_felvitel(uj_TermekKod);
SELECT product_id into product_azonosito FROM trodimp_aruhaz.product WHERE cikkszam LIKE uj_TermekKod limit 1;

UPDATE trodimp_aruhaz.product set
megyseg               = uj_MennyisegiEgyseg,
csomagolasi_mennyiseg = uj_RendelesiEgyseg,
model                 = (SELECT Megnevezes FROM trodimp_trodimp.web_termek_tipus WHERE TermekTipus LIKE uj_TermekTipus AND Nyelv LIKE "HUN" limit 1),
quantity              = (SELECT Mennyiseg FROM trodimp_trodimp.web_keszlet WHERE TermekKod LIKE uj_TermekKod AND Raktar = 0  limit 1),
price                 = uj_ListaAr,
weight                = uj_Suly,
date_added            = NOW(),
`status`              = 1,
subtract              = 0,
weight_class_id       = 1,
tax_class_id          = 12,
date_available        = NOW(),
stock_status_id       = VasarolhatoKod,
upc                   = uj_Vonalkod                 WHERE product_id=product_azonosito;

SELECT product_id into product_azonosito FROM trodimp_crm.product WHERE cikkszam LIKE uj_TermekKod limit 1;
UPDATE trodimp_crm.product set
MennyisegiEgyseg  = uj_MennyisegiEgyseg,
productname	      = (SELECT concat(TermekNev," ",NevMasodik," ",NevHarmadik) FROM trodimp_trodimp.web_termek_szovegek WHERE TermekKod LIKE uj_TermekKod AND Nyelv LIKE "HUN" limit 1),
cikkszam	        = uj_TermekKod,
discontinued      = uj_Statusza,

manufacturer_id	  = uj_TermekTipus,
manufacturer      = (SELECT Megnevezes FROM trodimp_trodimp.web_termek_tipus WHERE TermekTipus LIKE uj_TermekTipus AND Nyelv LIKE "HUN" limit 1),

category_name     = (select Nev from trodimp_trodimp.web_kategoriak where Kod LIKE (select Kod from trodimp_trodimp.web_termek_kategoria where TermekKod like uj_TermekKod limit 1) limit 1),

category_id       = (select Kod from trodimp_trodimp.web_termek_kategoria where TermekKod like uj_TermekKod limit 1),

createdtime       = NOW(),
modifiedtime	    = NOW(),
Vonalkod          = uj_Vonalkod,

unit_price        = uj_ListaAr,
taxclass          = uj_Afa,

qty_per_unit      = uj_RendelesiEgyseg,
qtyinstock        = (SELECT Mennyiseg FROM trodimp_trodimp.web_keszlet WHERE TermekKod LIKE uj_TermekKod AND Raktar = 0  limit 1),
description	      = (SELECT Megjegyzes FROM trodimp_trodimp.web_termek_szovegek WHERE TermekKod LIKE uj_TermekKod AND Nyelv LIKE "HUN" limit 1),
Felvitel          = '1',
Torolve           = '0'
WHERE product_id=product_azonosito;

END IF;
END