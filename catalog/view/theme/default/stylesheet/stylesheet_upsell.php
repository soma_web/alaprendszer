<?php
function ervenyesBlokkupsell($megjelenit_upsell_csoportok,$tol=0,$ig=0) {
    $vissza = array();
    foreach ($megjelenit_upsell_csoportok as $key=>$value) {
        if($value['default']['status'] == 1) {
            if($value['default']['width_tol'] >= $tol && ($value['default']['width_ig'] <= $ig || $value['default']['width_ig'] == 0) || ($tol == 0 && $ig == 0) ) {
                $kulcs = explode("_",$key);
                $kulcs = count($kulcs);
                if ($kulcs % 2 == 0){
                    $kulcs_ertek = "upsell_oszlop_";
                } else {
                    $kulcs_ertek = "upsell_sor_";
                }
                $display_ertek = $value['default']['display'];

                $vissza[] = array(
                    'key'       =>  $kulcs_ertek.$key,
                    'display'   =>  $display_ertek
                );
            }
        }
    }
    return $vissza;
}

if (isset($megjelenit_upsell_csoportok) && $megjelenit_upsell_csoportok) {
    $vissza = array();
    foreach ($megjelenit_upsell_csoportok as $key=>$value) {
        if($value['default']['status'] == 1) {
            $kulcs = explode("_",$key);
            $kulcs = count($kulcs);
            if ($kulcs % 2 == 0){
                $kulcs_ertek = "upsell_oszlop_";
            } else {
                $kulcs_ertek = "upsell_sor_";
            }
            $display_ertek = $value['default']['display'];

            $vissza[$value['default']['width_tol']."_".$value['default']['width_ig']][] = array(
                'key'       =>  $kulcs_ertek.$key,
                'display'   =>  $display_ertek
            );
        }
    }
    echo "";
    ?>
    <style>
        <?php $tomb = ervenyesBlokkupsell($megjelenit_upsell_csoportok);?>
        <?php if($tomb) { ?>
            <?php foreach($tomb as $value) { ?>
                .<?php echo $value['key']?> {
                    display: none;
                }
            <? } ?>
        <?php } ?>

        <?php foreach($vissza as $key=>$value) {?>
            <?php
            $tol_ig = explode('_',$key);
            $width_min = isset($tol_ig[0]) ? $tol_ig[0] : 0;
            $width_max = isset($tol_ig[1]) ? $tol_ig[1] : 100000;
            ?>
            @media screen and (min-width:<?php echo $width_min?>px) and (max-width: <?php echo $width_max?>px) {
                <?php foreach($value as $ertek) {?>
                    .<?php echo $ertek['key']?> {
                        display: <?php echo $ertek['display']?>;
                    }
                <?php }?>
            }
        <?php } ?>
    </style>
<?php } ?>