<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div style="float: left">
    <div class="content">
      <div class="vtabs"><a href="#tab-general"><?php echo $tab_general; ?></a>
        <?php foreach ($geo_zones as $geo_zone) { ?>
        <a href="#tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></a>
        <?php } ?>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general" class="vtabs-content">
          <table class="form">
            <tr>
              <td><?php echo $entry_tax_class; ?></td>
              <td><select name="weight_tax_class_id">
                  <option value="0"><?php echo $text_none; ?></option>
                  <?php foreach ($tax_classes as $tax_class) { ?>
                  <?php if ($tax_class['tax_class_id'] == $weight_tax_class_id) { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>
              <tr>
                  <td><?php echo $entry_title_heading; ?></td>
                  <td><input type="text" name="weight_header" value="<?php echo $weight_header; ?>" /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_title_text; ?></td>
                  <td><input type="text" name="weight_text" value="<?php echo $weight_text; ?>" /></td>
              </tr>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="weight_status">
                  <?php if ($weight_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>


              <tr>
                  <td><?php echo $entry_mail; ?></td>
                  <td><textarea name="weight_mail" style="width: 100%; height: 70px;"><?php echo $weight_mail; ?></textarea></td>
              </tr>

              <tr>
              <td><?php echo $entry_sort_order; ?></td>
              <td><input type="text" name="weight_sort_order" value="<?php echo $weight_sort_order; ?>" size="1" /></td>
            </tr>
            <tr>
                <td><?php echo $text_biztositas; ?></td>
                <td>
                    <?php if ($weight_biztositas == 1) { ?>
                        <input type="checkbox" name="weight_biztositas" value="<?php echo $weight_biztositas; ?>" checked="checked" />
                    <?php } else { ?>
                        <input type="checkbox" name="weight_biztositas" value="<?php echo $weight_biztositas; ?>" />
                    <?php } ?>
                </td>
            </tr>
              <?php if(isset($megjelenit_gls) && $megjelenit_gls == 1) { ?>
                  <tr>
                      <td><?php echo $text_gls_csomagpont; ?></td>
                      <td>
                          <?php if ($gls_csomagpont == 1) { ?>
                              <input type="checkbox" name="gls_csomagpont" value="<?php echo $gls_csomagpont; ?>"
                                     checked="checked"/>
                          <?php } else { ?>
                              <input type="checkbox" name="gls_csomagpont" value="<?php echo $gls_csomagpont; ?>"/>
                          <?php } ?>
                      </td>
                  </tr>
              <?php } else { $gls_csomagpont_cl = 0; } ?>
          </table>
        </div>
        <?php foreach ($geo_zones as $geo_zone) { ?>
        <div id="tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>" class="vtabs-content">
          <table class="form">
            <tr>
              <td><?php echo $entry_rate; ?></td>
              <td><textarea name="weight_<?php echo $geo_zone['geo_zone_id']; ?>_rate" cols="40" rows="5"><?php echo ${'weight_' . $geo_zone['geo_zone_id'] . '_rate'}; ?></textarea></td>
            </tr>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="weight_<?php echo $geo_zone['geo_zone_id']; ?>_status">
                  <?php if (${'weight_' . $geo_zone['geo_zone_id'] . '_status'}) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </table>
        </div>
        <?php } ?>
    </div>
    </div>
      <div  style="float: left; margin-left: 60px;"><h2>Fizetési módok</h2>
          <table class="form">
              <?php foreach($payment as $key =>$value) {?>
                  <tr>
                      <td><?php echo $value['name']; ?></td>
                      <td><select name="weight_<?php echo $key?>">
                              <?php if ($value['valasztva']) { ?>
                                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                  <option value="0"><?php echo $text_disabled; ?></option>
                              <?php } else { ?>
                                  <option value="1"><?php echo $text_enabled; ?></option>
                                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                              <?php } ?>
                          </select></td>
                  </tr>
              <?php } ?>
          </table>
      </div>
      </form>


  </div>
</div>
<script type="text/javascript"><!--
$('.vtabs a').tabs(); 
//--></script> 
<?php echo $footer; ?> 