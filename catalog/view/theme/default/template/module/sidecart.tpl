<div class="box-right-sidecart" id="sidecart"> 
    <div class="box-right-sidecart-heading"><?php echo $heading_title; ?></div>  
	<div class="box-right-sidecart-content">
   <?php if ($products || $vouchers) { ?>
    <div class="mini-cart-info">
      <table>
        <?php foreach ($products as $product) { ?>
        <tr>
          <td class="image"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
            <?php } ?></td>
          <td class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <div>
              <?php foreach ($product['option'] as $option) { ?>
              - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small><br />
              <?php } ?>
            </div></td>
          <td class="quantity">x&nbsp;<?php echo $product['quantity']; ?></td>
            <td class="total">
                <span><?php echo $product['total']; ?></span>
                <span class="remove">
                    <img src="catalog/view/theme/<?php echo $this->config->get('config_template')?>/image/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>"
                         onclick="removeFromCart('<?php echo $product['key']; ?>');" />
                </span>
            </td>
          <!--<td class="total"><?php echo $product['total']; ?></td>
          <td class="remove"><img src="catalog/view/theme/helios/image/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" onclick="removeFromCart('<?php echo $product['key']; ?>');" /></td>-->
        </tr>
        <?php } ?>
        <?php foreach ($vouchers as $voucher) { ?>
        <tr>
          <td class="image"></td>
          <td class="name"><?php echo $voucher['description']; ?></td>
          <td class="quantity">x&nbsp;1</td>
          <td class="total">
              <span><?php echo $voucher['amount']; ?></span>
              <span class="remove"><img src="catalog/view/theme/<?php echo $this->config->get('config_template')?>/image/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>"
                                        onclick="removeFromCart('<?php echo $voucher['key']; ?>');" /></span>
          </td>

          <!--<td class="remove"><img src="catalog/view/theme/helios/image/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" onclick="removeFromCart('<?php echo $voucher['key']; ?>');" /></td>-->
        </tr>
        <?php } ?>
      </table>
    </div>
    <div class="mini-cart-total">
      <table>
        <?php foreach ($totals as $total) { ?>
        <tr>
          <td align="right"><b><?php echo $total['title']; ?>:</b></td>
          <td align="right"><span style="color: #ffffff;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $total['text']; ?></span></td>
        </tr>
        <?php } ?>
      </table>
    </div>
    <div class="checkout"><a href="<?php echo $cart; ?>" class="button" type="button" ><?php echo $text_cart; ?></a> &nbsp;&nbsp; <a href="<?php echo $checkout; ?>" class="button" type="button" ><?php echo $text_checkout; ?></a></div>
    <?php } else { ?>
    <div class="empty"><?php echo $text_empty; ?></div>
    <?php } ?>

  
  </div>
</div>
