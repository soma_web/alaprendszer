<?php
class ControllerToolXmlExportSzamlazz extends Controller {

    public function index() {

    }
    public function xmlSql($xml_szamlazz=array()) {

        if (!$xml_szamlazz) {
            $xml_szamlazz = $this->request->get['xml_szamlazz'];
        }

        $this->load->model('sale/order');
        $this->load->model('tool/xml_export_szamlazz');
        $this->model_tool_xml_export_szamlazz->szamlazzellenor();

        $settings = $this->config->get("szamlazz_hu_module");


        (isset($settings['szamlazz_hu_eszamla']) && $settings['szamlazz_hu_eszamla']) == 0 ? $settings['szamlazz_hu_eszamla'] = 'false' : $settings['szamlazz_hu_eszamla'] = 'true';
        (isset($settings['szamlazz_hu_szamla_download']) && $settings['szamlazz_hu_szamla_download']) == 0 ? $settings['szamlazz_hu_szamla_download'] = 'false' : $settings['szamlazz_hu_szamla_download'] = 'true';

        (isset($xml_szamlazz['eloleg_szamla']) && $xml_szamlazz['eloleg_szamla']) == 0 ? $xml_szamlazz['eloleg_szamla'] = 'false' : $xml_szamlazz['eloleg_szamla'] = 'true';
        (isset($xml_szamlazz['vegszamla']) && $xml_szamlazz['vegszamla']) == 0 ? $xml_szamlazz['vegszamla'] = 'false' : $xml_szamlazz['vegszamla'] = 'true';
        (isset($xml_szamlazz['dijbekero']) && $xml_szamlazz['dijbekero']) == 0 ? $xml_szamlazz['dijbekero'] = 'false' : $xml_szamlazz['dijbekero'] = 'true';
        (isset($xml_szamlazz['fizetve']) && $xml_szamlazz['fizetve']) == 0 ? $xml_szamlazz['fizetve'] = 'false' : $xml_szamlazz['fizetve'] = 'true';
        if(isset($xml_szamlazz['peldany_szam']) && $xml_szamlazz['peldany_szam'] == ""){
            $xml_szamlazz['peldany_szam'] = 0;
        }

        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<xmlszamla xmlns="http://www.szamlazz.hu/xmlszamla" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.szamlazz.hu/xmlszamla xmlszamla.xsd ">';
        $xml .= '<beallitasok>';
        $xml .= '    <felhasznalo>'.$settings['szamlazz_hu_username'].'</felhasznalo>';
        $xml .= '    <jelszo>'.$settings['szamlazz_hu_password'].'</jelszo>';
        $xml .= '    <eszamla>'.$settings['szamlazz_hu_eszamla'].'</eszamla>';
        $xml .= '    <kulcstartojelszo>'.$settings['szamlazz_hu_kulcstartojelszo'].'</kulcstartojelszo>';
        $xml .= '    <szamlaLetoltes>'.$settings['szamlazz_hu_szamla_download'].'</szamlaLetoltes>';
        $xml .= '    <szamlaLetoltesPld>'.$xml_szamlazz['peldany_szam'].'</szamlaLetoltesPld>';
        $xml .= '    <valaszVerzio>'.$settings['szamlazz_hu_valaszverzio'].'</valaszVerzio>';
        $xml .= '    <aggregator>'.$settings['szamlazz_hu_aggregator'].'</aggregator>';
        $xml .= '</beallitasok>';

        $rendeles = $this->model_sale_order->getOrder($this->request->request['order_id']);
        $customer = $this->model_sale_order->get_customer($rendeles['customer_id']);
        $customer = $customer[0];
        (isset($customer['adoszam']) && $customer['adoszam'] != '') ? $adoszam = $customer['adoszam'] : $adoszam = '';
        preg_match("/\((.*?)\)/", $xml_szamlazz['arfolyam_bank'], $arfolyam_bank);
        $arfolyam_bank = $arfolyam_bank[1];

        $xml .= '<fejlec>';
        $xml .= '   <keltDatum>'.$xml_szamlazz['kelt_datum'].'</keltDatum>';
        $xml .= '   <teljesitesDatum>'.$xml_szamlazz['teljesites_datum'].'</teljesitesDatum>';
        $xml .= '   <fizetesiHataridoDatum>'.$xml_szamlazz['fizetesi_hatarido_datum'].'</fizetesiHataridoDatum>';
        $xml .= '   <fizmod>'.$xml_szamlazz['fizetesi_mod'].'</fizmod>';
        $xml .= '   <penznem>'.$xml_szamlazz['penznem'].'</penznem>';
        $xml .= '   <szamlaNyelve>'.$xml_szamlazz['szamla_nyelve'].'</szamlaNyelve>';
        $xml .= '   <megjegyzes>'.$xml_szamlazz['megjegyzes'].'</megjegyzes>';
        $xml .= '   <arfolyamBank>'.$arfolyam_bank.'</arfolyamBank>';
        $xml .= '   <arfolyam>'.$xml_szamlazz['arfolyam'].'</arfolyam>';
        $xml .= '   <rendelesSzam>'.$rendeles['order_id'].'</rendelesSzam>';
        $xml .= '   <elolegszamla>'.$xml_szamlazz['eloleg_szamla'].'</elolegszamla>';
        $xml .= '   <vegszamla>'.$xml_szamlazz['vegszamla'].'</vegszamla>';
        $xml .= '   <dijbekero>'.$xml_szamlazz['dijbekero'].'</dijbekero>';
        $xml .= '   <szamlaszamElotag>'.$xml_szamlazz['szamlaszam_elotag'].'</szamlaszamElotag>';
        $xml .= '   <fizetve>'.$xml_szamlazz['fizetve'].'</fizetve>';
        $xml .= '</fejlec>';

        $xml .= '<elado>';
        $xml .= '   <bank>'.$settings['szamlazz_hu_bank'].'</bank>';
        $xml .= '   <bankszamlaszam>'.$settings['szamlazz_hu_bankszamlaszam'].'</bankszamlaszam>';
        $xml .= '   <emailReplyto>'.$settings['szamlazz_hu_email_reply_to'].'</emailReplyto>';
        $xml .= '   <emailTargy>'.$settings['szamlazz_hu_email_targy'].'</emailTargy>';
        $xml .= '   <emailSzoveg>'.$settings['szamlazz_hu_email_szoveg'].'</emailSzoveg>';
        $xml .= '</elado>';

        $xml .= '<vevo>';
        $xml .= '   <nev>' .$rendeles['firstname'].' '.$rendeles['lastname'].'</nev>';
        $xml .= '   <irsz>'.$rendeles['payment_postcode'].'</irsz>';
        $xml .= '   <telepules>'.$rendeles['payment_city'].'</telepules>';
        $xml .= '   <cim>'.$rendeles['payment_address_1'].'</cim>';
        $xml .= '   <adoszam>'.$adoszam.'</adoszam>';
        $xml .= '</vevo>';



        $xml .= '<tetelek>';
        $products = $this->model_sale_order->getOrderProducts($this->request->request['order_id']);

        foreach($products as $product) {
            $products_variables = $this->model_sale_order->get_product_settings($product['product_id']);
            $products_variables = $products_variables[0];
            $product['price'] = (int)$product['price'];
            $product['total'] = (int)$product['total'];
            $product['tax'] = (int)$product['tax'];
            $product['egysegar'] = round((($product['tax']/$product['quantity'])*100)/$product['price']);

            $xml .= '<tetel>';
            $xml .= '   <megnevezes>'.$product['name'].'</megnevezes>';
            $xml .= '   <mennyiseg>'.$product['quantity'].'</mennyiseg>';
            $xml .= '   <mennyisegiEgyseg>'.$products_variables['megyseg'].'</mennyisegiEgyseg>';
            $xml .= '   <nettoEgysegar>'.($product['price']*$xml_szamlazz['arfolyam']).'</nettoEgysegar>';
            $xml .= '   <afakulcs>'.$product['egysegar'].'</afakulcs>';
            $xml .= '   <nettoErtek>'.($product['total']*$xml_szamlazz['arfolyam']).'</nettoErtek>';
            $xml .= '   <afaErtek>'.($product['tax']*$xml_szamlazz['arfolyam']).'</afaErtek>';
            $xml .= '   <bruttoErtek>'.(($product['total']*$xml_szamlazz['arfolyam'])+($product['tax']*$xml_szamlazz['arfolyam'])).'</bruttoErtek>';
            $xml .= '   <megjegyzes>'.$xml_szamlazz['megjegyzes'].'</megjegyzes>';
            $xml .= '</tetel>';
        }

        $xml .= '</tetelek>';
        $xml .= '</xmlszamla>';

        $output = fopen(DIR_ARUHAZ."csv/szamlazz.xml", "w");

        fputs($output, $xml);
        fclose($output);


        // cookie file teljes elérési útja a szerveren
        $cookie_file = str_replace('szamlazz.xml','szamlazz_cookie.txt',$output) ;
// ebbe a fájlba menti a pdf-et, ha az xml-ben kértük
        $pdf_file = str_replace('szamlazz.xml','szamla.pdf',$output) ;
// ezt az xml fájlt küldi a számla agentnek
        $xmlfile = DIR_ARUHAZ."csv/szamlazz.xml";
// a számla agentet ezen az urlen lehet elérni
        $agent_url = 'https://www.szamlazz.hu/szamla/';
// ha kérjük a számla pdf-et, akkor legyen true
        $szamlaletoltes = true;

        // ha még nincs --> létrehozzuk a cookie file-t --> léteznie kell, hogy a CURL írhasson bele
        if (!file_exists($cookie_file)) {
            file_put_contents($cookie_file, '');
        }

// a CURL inicializálása
        $ch = curl_init($agent_url);

// A curl hívás esetén tanúsítványhibát kaphatunk az SSL tanúsítvány valódiságától
// függetlenül, ez az alábbi CURL paraméter állítással kiküszöbölhető,
// ilyenkor nincs külön SSL ellenőrzés:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// POST-ban küldjük az adatokat
        curl_setopt($ch, CURLOPT_POST, true);

// Kérjük a HTTP headert a válaszba, fontos információk vannak benne
        curl_setopt($ch, CURLOPT_HEADER, true);

// változóban tároljuk a válasz tartalmát, nem írjuk a kimenetbe
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);










// Beállítjuk, hol van az XML, amiből számlát szeretnénk csinálni (= file upload)
// az xmlfile-t itt fullpath-al kell megadni

        // eredeti :
        //curl_setopt($ch, CURLOPT_POSTFIELDS, array('action-xmlagentxmlfile'=>'@' . $xmlfile));

        $ver = phpversion();
        if (substr($ver,0,3) > "5.4") {
            //$args['name'] = 'action-xmlagentxmlfile';
            $args['action-xmlagentxmlfile'] = new CurlFile($xmlfile, 'application/xml','szamlazz.hu');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('action-xmlagentxmlfile'=>'@' . $xmlfile));
        }

// 30 másodpercig tartjuk fenn a kapcsolatot (ha valami bökkenő volna)
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

// Itt állítjuk be, hogy az érkező cookie a $cookie_file-ba kerüljön mentésre
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);

// Ha van már cookie file-unk, és van is benne valami, elküldjük a Számlázz.hu-nak
        if (file_exists($cookie_file) && filesize($cookie_file) > 0) {
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        }

// elküldjük a kérést a Számlázz.hu felé, és eltároljuk a választ
        $agent_response = curl_exec($ch);

// kiolvassuk a curl-ból volt-e hiba
        $http_error = curl_error($ch);

// ezekben a változókban tároljuk a szétbontott választ
        $agent_header = '';
        $agent_body = '';
        $agent_http_code = '';

// lekérjük a válasz HTTP_CODE-ját, ami ha 200, akkor a http kommunikáció rendben volt
// ettől még egyáltalán nem biztos, hogy a számla elkészült
        $agent_http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);

// a válasz egy byte kupac, ebből az első "header_size" darab byte lesz a header
        $header_size = curl_getinfo($ch,CURLINFO_HEADER_SIZE);

// a header tárolása, ebben lesznek majd a számlaszám, bruttó nettó összegek, errorcode, stb.
        $agent_header = substr($agent_response, 0, $header_size);

// a body tárolása, ez lesz a pdf, vagy szöveges üzenet
        $agent_body = substr( $agent_response, $header_size );

// a curl már nem kell, lezárjuk
        curl_close($ch);

// a header soronként tartalmazza az információkat, egy tömbbe teszük a külön sorokat
        $header_array = explode("\n", $agent_header);

// ezt majd true-ra állítjuk ha volt hiba
        $volt_hiba = false;

// ebben lesznek a hiba információk, plusz a bodyban
        $agent_error = '';
        $agent_error_code = '';

// menjünk végig a header sorokon, ami "szlahu"-val kezdődik az érdekes nekünk és írjuk ki
        foreach ($header_array as $val) {
            if (substr($val, 0, strlen('szlahu')) === 'szlahu') {

                // megvizsgáljuk, hogy volt-e hiba
                if (substr($val, 0, strlen('szlahu_error:')) === 'szlahu_error:') {
                    // sajnos volt
                    $volt_hiba = true;
                    $agent_error = substr($val, strlen('szlahu_error:'));
                }
                if (substr($val, 0, strlen('szlahu_error_code:')) === 'szlahu_error_code:') {
                    // sajnos volt
                    $volt_hiba = true;
                    $agent_error_code = substr($val, strlen('szlahu_error_code:'));
                }
            }
        }

// ha volt http hiba dobunk egy kivételt


        if ($volt_hiba) {
            $utm = '&order_id='.$this->request->request['order_id'].'&hibakod='.(int)$agent_error_code;
            if((int)$agent_error_code == 173){
                preg_match("/[0-9]{4}.(0[1-9]|1[0-2]).(0[1-9]|[1-2][0-9]|3[0-1])/",$agent_body,$date);
                $utm .= "&last_date=".$date[0];
            }

            $this->redirect($this->url->link($this->request->request['honnan'], 'token=' . $this->session->data['token'] . $utm, 'SSL'));


            // ha a számla nem készült el kiírjuk amit lehet
            //echo 'Agent hibakód: '.$agent_error_code.'<br>';
            //echo 'Agent hibaüzenet: '.urldecode($agent_error).'<br>';
            //echo 'Agent válasz: '.urldecode($agent_body).'<br>';

            // dobunk egy kivételt
            //throw new Exception('Számlakészítés sikertelen:'.$agent_error_code);

        }else{
            $utm = '&order_id='.$this->request->request['order_id'] . '&success=true';
            $this->redirect($this->url->link($this->request->request['honnan'], 'token=' . $this->session->data['token'] . $utm, 'SSL'));
        }







    }


}
?>
