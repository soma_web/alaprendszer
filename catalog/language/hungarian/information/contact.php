<?php

// Heading
$_['heading_title']  = 'Kapcsolat';

// Text
$_['text_location']  = 'Helyszín';
$_['text_contact']   = 'Kapcsolat űrlap';
$_['text_address']   = 'Cím:';
$_['text_email']     = 'E-mail cím:';
$_['text_telephone'] = 'Telefon:';
$_['text_fax']       = 'Fax:';
$_['text_message']   = '<p>Érdeklődésének elküldése az üzlettulajdonosnak sikerült!</p>';
$_['text_map']   = 'Térkép';

// Entry Fields
$_['entry_name']     = 'Név:';
$_['entry_email']    = 'E-mail cím:';
$_['entry_enquiry']  = 'Érdeklődés:';
$_['entry_captcha']  = 'Írja be az alábbi keretben lévő kódot:';

// Email
$_['email_subject']  = 'Érdeklődés %s';

// Errors
$_['error_name']     = 'A név legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_email']    = 'Nem tűnik érvényesnek az e-mail cím!';
$_['error_enquiry']  = 'Az érdeklődés legalább 10, és legfeljebb 3000 karakterből álljon!';
$_['error_captcha']  = 'Nem egyezik a képen láthatóval az ellenőrző kód!';
?>
