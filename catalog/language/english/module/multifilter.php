<?php
// Heading
$_['heading_title']                 = 'Refine Search';
$_['heading_title_category']        = 'Category';
$_['heading_title_filter_group']    = 'Cashing place';
$_['heading_title_filter']          = 'Redeem points';
$_['heading_title_arszuro']         = 'Search Price: from to';
$_['heading_title_meret']           = 'Size table';
$_['heading_title_szin']            = 'Color table';
$_['heading_title_gyarto']          = 'Manufacturers';
$_['heading_title_raktaron']        = 'Availability';
$_['entry_raktaron']                = 'In stock';
$_['entry_nincs_raktaron']          = 'Pre-order necessary';
$_['entry_filter_ar_osszevont']     = 'Price filter / Search';



$_['entry_filter_select_artol'] = 'Discount rate: minimum 100 Ft';
$_['entry_filter_select_arig']  = 'Discount rate: maximum 100 Ft';
$_['text_search']  = 'Search... ';
$_['filter_name']  = '';
?>