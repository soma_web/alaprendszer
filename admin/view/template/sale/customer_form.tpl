<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/customer.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <div id="htabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a>
        <?php if ($customer_id) { ?>
        <a href="#tab-transaction"><?php echo $tab_transaction; ?></a><a href="#tab-reward"><?php echo $tab_reward; ?></a>
        <?php } ?>
        <a href="#tab-ip"><?php echo $tab_ip; ?></a>
        <a href="#tab-fizetes"><?php echo $tab_fizetes; ?></a>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <div id="vtabs" class="vtabs"><a href="#tab-customer"><?php echo $tab_general; ?></a>
            <?php $address_row = 1; ?>
            <?php foreach ($addresses as $address) { ?>
            <a href="#tab-address-<?php echo $address_row; ?>" id="address-<?php echo $address_row; ?>"><?php echo $tab_address . ' ' . $address_row; ?>&nbsp;<img src="view/image/delete.png" alt="" onclick="$('#vtabs a:first').trigger('click'); $('#address-<?php echo $address_row; ?>').remove(); $('#tab-address-<?php echo $address_row; ?>').remove(); return false;" /></a>
            <?php $address_row++; ?>
            <?php } ?>
            <span id="address-add"><?php echo $button_add_address; ?>&nbsp;<img src="view/image/add.png" alt="" onclick="addAddress();" /></span></div>
          <div id="tab-customer" class="vtabs-content">
            <table class="form" >

              <?php if ($customer_id && $sap == 1) { ?>
                <?php if ($atkuldve == 0 && strtoupper(substr($vevokod,0,1)) =='W') {?>
                  <tr id="sap_kod_kiir">
                    <td ><?php echo $entry_sap_kod; ?></td>
                      <td ><input type="text" name="sap_kod" value="<?php echo $vevokod; ?>" id="sap_vevokod"/>
                          <div style="display: inline-block; margin-left: 30px;" class="buttons"><a onclick=sap_nak(<?echo $customer_id?>) class="button"><?php echo $button_sap; ?></a></div>
                      </td>
                  </tr>
                <?}?>
              <?}?>

              <tr >
                <td ><span class="required">*</span> <?php echo $entry_firstname; ?></td>
                <td colspan=2><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
                  <?php if ($error_firstname) { ?>
                  <span class="error"><?php echo $error_firstname; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
                <td colspan=2><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
                  <?php if ($error_lastname) { ?>
                  <span class="error"><?php echo $error_lastname; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_email; ?></td>
                <td colspan=2><input type="text" name="email" value="<?php echo $email; ?>" />
                  <?php if ($error_email) { ?>
                  <span class="error"><?php echo $error_email; ?></span>
                  <?php  } ?></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
                <td colspan=2><input type="text" name="telephone" value="<?php echo $telephone; ?>" />
                  <?php if ($error_telephone) { ?>
                  <span class="error"><?php echo $error_telephone; ?></span>
                  <?php  } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_fax; ?></td>
                <td colspan=2><input type="text" name="fax" value="<?php echo $fax; ?>" /></td>
              </tr>
              <?php if ($this->config->get('megjelenit_eletkor_regisztracio') == 1 ) { ?>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_eletkor; ?></td>
                    <td><select name="eletkor" >
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($eletkors as $eletkora) { ?>
                                <?php if ($eletkora['megnevezes'] == $eletkor) { ?>
                                    <option value="<?php echo $eletkora['megnevezes']; ?>" selected="selected"><?php echo $eletkora['megnevezes']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $eletkora['megnevezes']; ?>"><?php echo $eletkora['megnevezes']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php if ($error_eletkor) { ?>
                            <span class="error"><?php echo $error_eletkor; ?></span>
                        <?php } ?>
                    </td>
                </tr>
              <?php } ?>
              <?php if ($this->config->get('megjelenit_nem_regisztracio') == 1 ) { ?>

                    <tr>
                        <td><span class="required">*</span> <?php echo $entry_nem; ?></td>
                        <td><select name="nem" >
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($nems as $value) { ?>
                                    <?php if ($value['ertek'] == $nem) { ?>
                                        <option value="<?php echo $value['ertek']; ?>" selected="selected"><?php echo $value['nev']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $value['ertek']; ?>"><?php echo $value['nev']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <?php if ($error_nem) { ?>
                                <span class="error"><?php echo $error_nem; ?></span>
                            <?php } ?>
                        </td>
                    </tr>
              <?php } ?>

              <?php if ($this->config->get('megjelenit_regisztracio_iskolai_vegzettseg') == 1 ) { ?>
                    <tr>
                        <td><?php echo $entry_iskolai_vegzettseg; ?></td>
                        <td><select name="iskolai_vegzettseg" >
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($iskolai_vegzettsegek as $value) { ?>
                                    <?php if ($value['megnevezes'] == $iskolai_vegzettseg) { ?>
                                        <option value="<?php echo $value['megnevezes']; ?>" selected="selected"><?php echo $value['megnevezes']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $value['megnevezes']; ?>"><?php echo $value['megnevezes']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
              <?php } ?>

                <?php if ($this->config->get('megjelenit_modul_frontend') == 1 ) {?>
                    <tr>
                        <td><?php echo $entry_feltolto; ?></td>
                        <td><select name="feltolto" >
                                <?php foreach ($feltoltos as $value) { ?>
                                    <?php if ($value['ertek'] == $feltolto) { ?>
                                        <option value="<?php echo $value['ertek']; ?>" selected="selected"><?php echo $value['nev']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $value['ertek']; ?>"><?php echo $value['nev']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>

                        </td>
                    </tr>
                <?php } ?>

                <?php if (!empty($megjelenit_vevok['partner_kod'])) { ?>
                    <tr>
                        <td><?php echo $entry_partnerkod; ?></td>
                        <td><input type="text" name="partner_kod" value="<?php echo $partner_kod; ?>" /></td>
                    </tr>
                <?php } ?>

              <tr>
                <td><?php echo $entry_password; ?></td>
                <td colspan=2><input type="password" name="password" value="<?php echo $password; ?>"  />
                  <br />
                  <?php if ($error_password) { ?>
                  <span class="error"><?php echo $error_password; ?></span>
                  <?php  } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_confirm; ?></td>
                <td colspan=2><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
                  <?php if ($error_confirm) { ?>
                  <span class="error"><?php echo $error_confirm; ?></span>
                  <?php  } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_newsletter; ?></td>
                <td colspan=2><select name="newsletter">
                    <?php if ($newsletter) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select></td>
              </tr>
              <?php if($newsletter == '1') { ?>
              <tr id="newslettercategories">
                  <?php } else { ?>
              <tr id="newslettercategories" class="newslettertr" style="display: none">
                  <?php } ?>
                  <td><?php echo $entry_newsletter_categories ?></td>

                  <td class="newslettertd">
                      <?php foreach($categories as $category) { ?>
                          <?php if(in_array($category['category_id'], $newslettercategories)) { ?>
                              <input type="checkbox" checked="checked" name="newslettercategories[]" value="<?php echo $category['category_id']; ?>" /><?php echo $category['name']; ?><br />
                          <?php } else { ?>
                              <input type="checkbox" name="newslettercategories[]" value="<?php echo $category['category_id']; ?>" /><?php echo $category['name']; ?><br />
                          <?php } ?>
                      <?php } ?>
                  </td>
              </tr>
              <tr>
                <td><?php echo $entry_customer_group; ?></td>
                <td colspan=2><select name="customer_group_id">
                    <?php foreach ($customer_groups as $customer_group) { ?>
                    <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                    <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select></td>
              </tr>
              <tr>
                <td><?php echo $entry_status; ?></td>
                <td colspan=2><select name="status">
                    <?php if ($status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select></td>
              </tr>
              <tr>
                  <td><?php echo $entry_company; ?></td>
                  <td><input type="text" name="company" value="<?php echo $company; ?>" /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_adoszam; ?></td>
                  <td colspan=2><input type="text" name="adoszam" value="<?php echo $adoszam; ?>" />
                      <?php if ($error_adoszam) { ?>
                          <span class="error"><?php echo $error_adoszam; ?></span>
                      <?php  } ?></td>
              </tr>
              <tr>
                  <td><?php echo $entry_vallalkozasi_forma; ?></td>
                  <td colspan=2><input type="text" name="vallalkozasi_forma" value="<?php echo $vallalkozasi_forma; ?>" />
              </tr>
              <tr>
                  <td><?php echo $entry_szekhely; ?></td>
                  <td colspan=2><input type="text" name="szekhely" value="<?php echo $szekhely; ?>" />
              </tr>
              <tr>
                  <td><?php echo $entry_ugyvezeto_neve; ?></td>
                  <td colspan=2><input type="text" name="ugyvezeto_neve" value="<?php echo $ugyvezeto_neve; ?>" />
              </tr>
              <tr>
                  <td><?php echo $entry_ugyvezeto_telefonszama; ?></td>
                  <td colspan=2><input type="text" name="ugyvezeto_telefonszama" value="<?php echo $ugyvezeto_telefonszama; ?>" />
              </tr>
                <tr>
                    <td><?php echo $entry_szazalek; ?></td>
                    <td colspan=2>
                        <input type="text" name="szazalek" value="<?php echo $szazalek; ?>" <?php echo $szazalek_read_only ? 'readonly' : ''?>/>
                        <?php if ($error_szazalek) { ?>
                            <span class="error"><?php echo $error_szazalek; ?></span>
                        <?php  } ?></td>
                </tr>

                <?php if ($this->config->get('megjelenit_regisztracio_weblap') == 1 ) {?>
                    <tr>
                        <td><?php echo $entry_weblap; ?></td>
                        <td colspan=2>
                            <input type="text" name="weblap" value="<?php echo !empty($weblap) ? $weblap : ''; ?>" />
                        </td>

                    </tr>
                <?php } ?>
            </table>
          </div>
          <?php $address_row = 1; ?>
          <?php foreach ($addresses as $address) { ?>
          <div id="tab-address-<?php echo $address_row; ?>" class="vtabs-content">
            <input type="hidden" name="address[<?php echo $address_row; ?>][address_id]" value="<?php echo $address['address_id']; ?>" />
            <table class="form">
              <tr>
                <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][firstname]" value="<?php echo $address['firstname']; ?>" />
                  <?php if (isset($error_address_firstname[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_firstname[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][lastname]" value="<?php echo $address['lastname']; ?>" />
                  <?php if (isset($error_address_lastname[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_lastname[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_company; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][company]" value="<?php echo $address['company']; ?>" /></td>
              </tr>
                <tr>
                    <td><?php echo $entry_vallalkozasi_forma; ?></td>
                    <td><input type="text" name="address[<?php echo $address_row; ?>][vallalkozasi_forma]" value="<?php echo $address['vallalkozasi_forma']; ?>" /></td>
                </tr>
                <tr>
                    <td><?php echo $entry_szekhely; ?></td>
                    <td><input type="text" name="address[<?php echo $address_row; ?>][szekhely]" value="<?php echo $address['szekhely']; ?>" /></td>
                </tr>
                <tr>
                    <td><?php echo $entry_ugyvezeto_neve; ?></td>
                    <td><input type="text" name="address[<?php echo $address_row; ?>][ugyvezeto_neve]" value="<?php echo $address['ugyvezeto_neve']; ?>" /></td>
                </tr>
                <tr>
                    <td><?php echo $entry_ugyvezeto_telefonszama; ?></td>
                    <td><input type="text" name="address[<?php echo $address_row; ?>][ugyvezeto_telefonszama]" value="<?php echo $address['ugyvezeto_telefonszama']; ?>" /></td>
                </tr>
                <tr>
                    <td><?php echo $entry_adoszam; ?></td>
                    <td><input type="text" name="address[<?php echo $address_row; ?>][adoszam]" value="<?php echo $address['adoszam']; ?>" /></td>
                </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][address_1]" value="<?php echo $address['address_1']; ?>" />
                  <?php if (isset($error_address_address_1[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_address_1[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_address_2; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][address_2]" value="<?php echo $address['address_2']; ?>" /></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_city; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][city]" value="<?php echo $address['city']; ?>" />
                  <?php if (isset($error_address_city[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_city[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][postcode]" value="<?php echo $address['postcode']; ?>" /></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_country; ?></td>
                <td><select name="address[<?php echo $address_row; ?>][country_id]" id="address[<?php echo $address_row; ?>][country_id]" onchange="$('select[name=\'address[<?php echo $address_row; ?>][zone_id]\']').load('index.php?route=sale/customer/zone&token=<?php echo $token; ?>&country_id=' + this.value + '&zone_id=<?php echo $address['zone_id']; ?>');">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($countries as $country) { ?>
                    <?php if ($country['country_id'] == $address['country_id']) { ?>
                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                  <?php if (isset($error_address_country[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_country[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
                <td><select name="address[<?php echo $address_row; ?>][zone_id]">
                  </select>
                  <?php if (isset($error_address_zone[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_zone[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_default; ?></td>
                <td><?php if (($address['address_id'] == $address_id) || !$addresses) { ?>
                  <input type="radio" name="address[<?php echo $address_row; ?>][default]" value="<?php echo $address_row; ?>" checked="checked" /></td>
                <?php } else { ?>
                <input type="radio" name="address[<?php echo $address_row; ?>][default]" value="<?php echo $address_row; ?>" />
                  </td>
                <?php } ?>
              </tr>
            </table>
            <script type="text/javascript"><!--
		    $('select[name=\'address[<?php echo $address_row; ?>][zone_id]\']').load('index.php?route=sale/customer/zone&token=<?php echo $token; ?>&country_id=<?php echo $address['country_id']; ?>&zone_id=<?php echo $address['zone_id']; ?>');
		    //--></script> 
          </div>
          <?php $address_row++; ?>
          <?php } ?>
        </div>
        <?php if ($customer_id) { ?>
        <div id="tab-transaction">
          <table class="form">
            <tr>
              <td><?php echo $entry_description; ?></td>
              <td><input type="text" name="description" value="" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_amount; ?></td>
              <td><input type="text" name="amount" value="" /></td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: right;"><a id="button-reward" class="button" onclick="addTransaction();"><span><?php echo $button_add_transaction; ?></span></a></td>
            </tr>
          </table>
          <div id="transaction"></div>
        </div>
        <div id="tab-reward">
          <table class="form">
            <tr>
              <td><?php echo $entry_description; ?></td>
              <td><input type="text" name="description" value="" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_points; ?></td>
              <td><input type="text" name="points" value="" /></td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: right;"><a id="button-reward" class="button" onclick="addRewardPoints();"><span><?php echo $button_add_reward; ?></span></a></td>
            </tr>
          </table>
          <div id="reward"></div>
        </div>
        <?php } ?>


        <div id="tab-ip">
            <table class="list">
                <thead>
                <tr>
                    <td class="left"><?php echo $column_ip; ?></td>
                    <td class="right"><?php echo $column_total; ?></td>
                    <td class="left"><?php echo $column_date_added; ?></td>
                    <td class="right"><?php echo $column_action; ?></td>
                </tr>
                </thead>
                <tbody>
                <?php if ($ips) { ?>
                    <?php foreach ($ips as $ip) { ?>
                        <tr>
                            <td class="left"><a onclick="window.open('http://www.geoiptool.com/en/?IP=<?php echo $ip['ip']; ?>');"><?php echo $ip['ip']; ?></a></td>
                            <td class="right"><a onclick="window.open('<?php echo $ip['filter_ip']; ?>');"><?php echo $ip['total']; ?></a></td>
                            <td class="left"><?php echo $ip['date_added']; ?></td>
                            <td class="right"><?php if ($ip['blacklist']) { ?>
                                    <b>[</b> <a id="<?php echo str_replace('.', '-', $ip['ip']); ?>" onclick="removeBlacklist('<?php echo $ip['ip']; ?>');"><?php echo $text_remove_blacklist; ?></a> <b>]</b>
                                <?php } else { ?>
                                    <b>[</b> <a id="<?php echo str_replace('.', '-', $ip['ip']); ?>" onclick="addBlacklist('<?php echo $ip['ip']; ?>');"><?php echo $text_add_blacklist; ?></a> <b>]</b>
                                <?php } ?></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td class="center" colspan="3"><?php echo $text_no_results; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

      <!--  <div id="tab-fizetes">
            <table class="list">
                <thead>
                <tr>
                    <td class="left"><?php echo $entry_fizetes; ?></td>
                    <td class="left"><?php echo $entry_fizetes_engedely; ?></td>
                    <td class="right"><?php echo $entry_fizetes_modositas; ?></td>
                </tr>
                </thead>

                <tbody>
                <?php if (isset($fizetes)) { ?>
                    <?php foreach ($fizetes as $sor) { ?>
                        <tr>
                            <td class="left"><?php echo $sor['fizetesi_mod']; ?></td>
                            <?php if ($sor['engedelyezve']== 0) { ?>
                                <td class="left" id="fizetes_engedelyezve<?echo  $sor['fizetes_id']?>">Letiltva</td>
                            <? } else { ?>
                                <td class="left" id="fizetes_engedelyezve<?echo  $sor['fizetes_id']?>">Engedélyezve</td>
                            <? } ?>
                            <td class="right"><a ><?php echo $entry_fizetes_torles; ?></a> <a onclick=FizetesModositas(<?php echo "fizetes_engedelyezve".$sor['fizetes_id']?>,<?php echo $sor['fizetes_id']?>,<?php echo $sor['engedelyezve']?>)><?php echo $entry_fizetes_modositas; ?></a></td>
                        </tr>

                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td class="center" colspan="2"><?php echo $text_no_results; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div style="margin:0px 0; float:right" class="buttons"><a onclick=sap_nak(<?echo $customer_id?>) class="button"><?php echo $button_uj_fizetes; ?></a></div>

        </div>-->



        <div id="tab-fizetes">
            <table id="vevo" class="list">
                <thead>
                <tr>
                    <td class="left"><?php echo $entry_fizetes; ?></td>
                    <td class="left"><?php echo $entry_fizetes_engedely; ?></td>
                    <td class="right"><?php echo $entry_fizetes_modositas; ?></td>
                </tr>
                </thead>

                <?php $vevo_row = 0; ?>
                <?php if (isset($product_vevo)) { ?>
                    <?php foreach ($product_vevo as $sor) { ?>
                        <tbody id="vevo-row<?php echo $vevo_row; ?>">
                          <tr>
                            <td class="left">
                                <select name="product_vevo[<?php echo $vevo_row; ?>][fizetesi_mod]">
                                    <?php foreach ($payment as $fizetesi_mod) { ?>
                                        <?php if ($fizetesi_mod['fizetesi_modok'] == $sor['fizetesi_mod_eredeti']) { ?>
                                            <option value="<?php echo $fizetesi_mod['fizetesi_modok']; ?>" selected="selected"><?php echo $fizetesi_mod['fizetesi_megnevezesek']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $fizetesi_mod['fizetesi_modok']; ?>"><?php echo $fizetesi_mod['fizetesi_megnevezesek']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </td>

                            <td class="left">
                               <select name="product_vevo[<?php echo $vevo_row; ?>][engedelyezve]">
                               <?php if ($sor['engedelyezve'] == 1) { ?>
                                    <option value="1" selected="selected">Engedélyezett</option>
                                    <option value="0" >Letiltott</option>
                               <?php } else { ?>
                                     <option value="1">Engedélyezett</option>
                                     <option value="0"  selected="selected">Letiltott</option>
                               <?php } ?>
                                </select></td>

                             <td class="right"><a  onclick="$('#vevo-row<?php echo $vevo_row; ?>').remove();" ><?php echo $entry_fizetes_torles; ?></a> </td>
                          </tr>
                        </tbody>

                        <?php $vevo_row++; ?>
                    <?php } ?>
                <?}?>
                <tfoot>
                <tr>
                    <td colspan="3" class="right"><a onclick="addFizetes();" class="button"><?php echo $button_uj_fizetes; ?></a></td>
                </tr>
                </tfoot>
            </table>
        </div>




      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--

    var vevo_row = <?php echo $vevo_row; ?>;

    function addFizetes() {


       html = '<tbody id="vevo-row' + vevo_row + '">';

        html += '  <tr>';
        html += '    <td class="left"><select name="product_vevo[' + vevo_row + '][fizetesi_mod]">';

        <?php foreach ($payment as $fizetesi_mod) { ?>
        html += '<option value="<?php echo $fizetesi_mod['fizetesi_modok']; ?>"><?php echo $fizetesi_mod['fizetesi_megnevezesek']; ?></option>';
        <?php } ?>
        html += '    </select></td>';

        html += '   <td class="left"><select name="product_vevo['+vevo_row+'][engedelyezve]">';

        html += '<option value="1">Engedélyezett</option>';
        html += '<option value="0" >Letiltott</option>';
        html += '</select></td>';

        html += '<td class="right"><a  onclick="$(\'#vevo-row' + vevo_row + '\').remove();" ><?php echo $entry_fizetes_torles; ?></a></td>';
        html += '  </tr>';
        html += '</tbody>';



        $('#vevo tfoot').before(html);

        vevo_row++;
    }


var address_row = <?php echo $address_row; ?>;

function addAddress() {	
	html  = '<div id="tab-address-' + address_row + '" class="vtabs-content" style="display: none;">';
	html += '  <input type="hidden" name="address[' + address_row + '][address_id]" value="" />';
	html += '  <table class="form">'; 
	html += '    <tr>';
    html += '	   <td><?php echo $entry_firstname; ?></td>';
    html += '	   <td><input type="text" name="address[' + address_row + '][firstname]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><?php echo $entry_lastname; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][lastname]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><?php echo $entry_company; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][company]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '       <td><?php echo $entry_vallalkozasi_forma; ?></td>';
    html += '           <td colspan=2><input type="text" name="address[' + address_row + '][vallalkozasi_forma]" value="" />';
    html += '    </tr>';
    html += '    <tr>';
    html += '       <td><?php echo $entry_szekhely; ?></td>';
    html += '       <td colspan=2><input type="text" name="address[' + address_row + '][szekhely]" value="" />';
    html += '    </tr>';
    html += '    <tr>';
    html += '       <td><?php echo $entry_ugyvezeto_neve; ?></td>';
    html += '       <td colspan=2><input type="text" name="address[' + address_row + '][ugyvezeto_neve]" value="" />';
    html += '    </tr>';
    html += '    <tr>';
    html += '       <td><?php echo $entry_ugyvezeto_telefonszama; ?></td>';
    html += '       <td colspan=2><input type="text" name="address[' + address_row + '][ugyvezeto_telefonszama]" value="" />';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><?php echo $entry_adoszam; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][adoszam]" value="" /></td>';
    html += '    </tr>';

    html += '    <tr>';
    html += '      <td><?php echo $entry_address_1; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][address_1]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><?php echo $entry_address_2; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][address_2]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><?php echo $entry_city; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][city]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><?php echo $entry_postcode; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][postcode]" value="" /></td>';
    html += '    </tr>';
    html += '      <td><?php echo $entry_country; ?></td>';
    html += '      <td><select name="address[' + address_row + '][country_id]" onchange="$(\'select[name=\\\'address[' + address_row + '][zone_id]\\\']\').load(\'index.php?route=sale/customer/zone&token=<?php echo $token; ?>&country_id=\' + this.value + \'&zone_id=0\');">';
    html += '         <option value=""><?php echo $text_select; ?></option>';
    <?php foreach ($countries as $country) { ?>
    html += '         <option value="<?php echo $country['country_id']; ?>"><?php echo addslashes($country['name']); ?></option>';
    <?php } ?>
    html += '      </select></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><?php echo $entry_zone; ?></td>';
    html += '      <td><select name="address[' + address_row + '][zone_id]"><option value="false"><?php echo $this->language->get('text_none'); ?></option></select></td>';
    html += '    </tr>';
	html += '    <tr>';
    html += '      <td><?php echo $entry_default; ?></td>';
    html += '      <td><input type="radio" name="address[' + address_row + '][default]" value="1" /></td>';
    html += '    </tr>';
    html += '  </table>';
    html += '</div>';
	
	$('#tab-general').append(html);
	
	$('#address-add').before('<a href="#tab-address-' + address_row + '" id="address-' + address_row + '"><?php echo $tab_address; ?> ' + address_row + '&nbsp;<img src="view/image/delete.png" alt="" onclick="$(\'#vtabs a:first\').trigger(\'click\'); $(\'#address-' + address_row + '\').remove(); $(\'#tab-address-' + address_row + '\').remove(); return false;" /></a>');
		 
	$('.vtabs a').tabs();
	
	$('#address-' + address_row).trigger('click');
	
	address_row++;
}
//--></script> 
<script type="text/javascript"><!--
$('#transaction .pagination a').on('click', function() {
	$('#transaction').load(this.href);
	
	return false;
});			

$('#transaction').load('index.php?route=sale/customer/transaction&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

function addTransaction() {
	$.ajax({
		url: 'index.php?route=sale/customer/transaction&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
		type: 'post',
		dataType: 'html',
		data: 'description=' + encodeURIComponent($('#tab-transaction input[name=\'description\']').val()) + '&amount=' + encodeURIComponent($('#tab-transaction input[name=\'amount\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-transaction').attr('disabled', true);
			$('#transaction').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-transaction').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(html) {
			$('#transaction').html(html);
			
			$('#tab-transaction input[name=\'amount\']').val('');
			$('#tab-transaction input[name=\'description\']').val('');
		}
	});
}
//--></script> 
<script type="text/javascript"><!--
$('#reward .pagination a').on('click', function() {
	$('#reward').load(this.href);
	
	return false;
});			

$('#reward').load('index.php?route=sale/customer/reward&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

function addRewardPoints() {
	$.ajax({
		url: 'index.php?route=sale/customer/reward&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
		type: 'post',
		dataType: 'html',
		data: 'description=' + encodeURIComponent($('#tab-reward input[name=\'description\']').val()) + '&points=' + encodeURIComponent($('#tab-reward input[name=\'points\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-reward').attr('disabled', true);
			$('#reward').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-reward').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(html) {
			$('#reward').html(html);
								
			$('#tab-reward input[name=\'points\']').val('');
			$('#tab-reward input[name=\'description\']').val('');
		}
	});
}

function addBlacklist(ip) {
	$.ajax({
		url: 'index.php?route=sale/customer/addblacklist&token=<?php echo $token; ?>',
		type: 'post',
		dataType: 'json',
		data: 'ip=' + encodeURIComponent(ip),
		beforeSend: function() {
			$('.success, .warning').remove();
			
			$('.box').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> Please wait!</div>');			
		},
		complete: function() {
			$('.attention').remove();
		},			
		success: function(json) {
			if (json['error']) {
				 $('.box').before('<div class="warning" style="display: none;">' + json['error'] + '</div>');
				
				$('.warning').fadeIn('slow');
			}
						
			if (json['success']) {
                $('.box').before('<div class="success" style="display: none;">' + json['success'] + '</div>');
				
				$('.success').fadeIn('slow');
				
				$('#' + ip.replace(/\./g, '-')).replaceWith('<a id="' + ip.replace(/\./g, '-') + '" onclick="removeBlacklist(\'' + ip + '\');"><?php echo $text_remove_blacklist; ?></a>');
			}
		}
	});	
}

function removeBlacklist(ip) {
	$.ajax({
		url: 'index.php?route=sale/customer/removeblacklist&token=<?php echo $token; ?>',
		type: 'post',
		dataType: 'json',
		data: 'ip=' + encodeURIComponent(ip),
		beforeSend: function() {
			$('.success, .warning').remove();
			
			$('.box').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> Please wait!</div>');				
		},
		complete: function() {
			$('.attention').remove();
		},			
		success: function(json) {
			if (json['error']) {
				 $('.box').before('<div class="warning" style="display: none;">' + json['error'] + '</div>');
				
				$('.warning').fadeIn('slow');
			}
			
			if (json['success']) {
				 $('.box').before('<div class="success" style="display: none;">' + json['success'] + '</div>');
				
				$('.success').fadeIn('slow');
				
				$('#' + ip.replace(/\./g, '-')).replaceWith('<a id="' + ip.replace(/\./g, '-') + '" onclick="addBlacklist(\'' + ip + '\');"><?php echo $text_add_blacklist; ?></a>');
			}
		}
	});	
};

function sap_nak(customer_id){

    var sap_vevokod=$('#sap_vevokod').val().toUpperCase();

    $.ajax({
        url: 'index.php?route=sale/customer/SAP_nak_kuld&token=<?php echo $token; ?>',
        type: 'post',
        dataType: 'json',
        data: 'vevokod=' + sap_vevokod+'&cust_id='+customer_id,

        beforeSend: function() {
            $('.success, .warning').remove();

            $('.box').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> Please wait!</div>');
        },
        complete: function() {
            $('.attention').remove();
            $("#sap_kod_kiir").remove();


        }
    });
}
//--></script> 
<script type="text/javascript"><!--
$('.htabs a').tabs();
$('.vtabs a').tabs();
//--></script>
    <script type="text/javascript">
        $('[name=newsletter]').on('change', function() {
            if (this.value == '1') {
                $('#newslettercategories').fadeIn(1000);
            } else {
                $('#newslettercategories').fadeOut();
            }
        });
    </script>
<?php echo $footer; ?>