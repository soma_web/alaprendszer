<?php
// Text
$_['text_refine']       = 'Refine Search';
$_['text_product']      = 'Termékek';
$_['text_error']        = 'Nincs találat!';
$_['text_empty']        = 'Ebben a kategóriában nincsenek termékek.';
$_['text_quantity']     = 'Menny.:';
$_['text_manufacturer'] = 'Márka:';
$_['text_model']        = 'Termékkód:';
$_['text_points']       = 'Jutalom pontok:';
$_['text_price']        = 'Ár:';
$_['text_tax']          = 'Nettó:';
$_['text_reviews']      = '%s vélemény alapján.';
$_['text_compare']      = 'Termék összehasonlítása (%s)';
$_['text_display']      = 'Nézet:';
$_['text_list']         = 'Lista';
$_['text_grid']         = 'Rács';
$_['text_stock_in']     = 'Várható beérkezés:';
$_['text_cikkszam']     = 'Cikkszám: ';

$_['text_sort']         = 'Rendezés:';
$_['text_default']      = 'Alapértelmezett';
$_['text_name_asc']     = 'Név szerint növekvő';
$_['text_name_desc']    = 'Név szerint csökkenő';
$_['text_price_asc']    = 'Ár szerint növekvő';
$_['text_price_desc']   = 'Ár szerint csökkenő';
$_['text_rating_asc']   = 'Értékelés (Legrosszabbak elől)';
$_['text_rating_desc']  = 'Értékelés (Legjobbak elől)';
$_['text_model_asc']    = 'Cikkszám szerint növekvő';
$_['text_model_desc']   = 'Cikkszám szerint csökkenő';


$_['text_limit']              = 'Mutat:';
$_['text_pardarab_title']     = 'Pár darab';
$_['text_uj_title']           = 'Új termék';
$_['text_kifuto_title']       = 'Kifutó termék';
$_['text_mennyisegi_title']   = 'Mennyiségi kedvezmény';
$_['text_raktar']             = 'Boltonkénti készlet';
$_['text_ajandek_title']     = 'Ajándék';

$_['text_akcio_title']        = 'Akciós termék';
$_['text_instock']            = 'Raktáron';
$_['text_nincs_raktaron']     = 'Nincs raktáron';
$_['text_compare']            = 'Termék összehasonlítás';
$_['text_wishlist']           = 'Kívánságlista';
$_['text_raktaron']           = 'Raktáron';

$_['text_raktar']             = 'Boltonkénti készlet';
$_['entry_garancia_honap']    = 'hónap';
$_['entry_garancia_ev']       = 'év';
$_['text_garancia']           = 'garancia';
$_['text_szallitas']          = 'Szállítási költség:';

// Ikonok szöveges (iconok_szoveges.tpl)
$_['text_uj']                = 'Új';
$_['text_akcios']            = 'Akció';
$_['text_pardarab']          = 'Pár darab';
$_['text_mennyisegi']        = 'Mennyiségi kedvezmény';
$_['text_ujdonsag_title']    = 'Újdonság';
$_['text_kifuto_title']      = 'Kifutó termék';
// Ikonok szöveges vége

?>
