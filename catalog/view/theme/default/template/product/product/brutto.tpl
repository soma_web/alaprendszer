<?php if(!(isset($beallitas_termek_aloldal['null_off_brutto']) && $beallitas_termek_aloldal['null_off_brutto'])) { ?>
    <?php $not_null = true; ?>
<?php } ?>

<?php if ($not_null && empty($ar_tiltasa)) { ?>

    <?php if ($utalvany == 1) { ?>
        <?php $class="utalvany";?>
    <?php } else {?>
        <?php $class="";?>
    <?php } ?>


    <div class="price <?php echo $class?> <?php echo !$mutat_arat ? 'price_nemkell' : ''?>">
        <?php if (!$special && $this->config->get('megjelenit_product_brutto_ar') == 1) { ?>
            <div class=price_egyedul><?php echo $price; ?></div>
        <?php } elseif ($this->config->get('megjelenit_product_brutto_ar') == 1) { ?>
            <div class="price_ujarral">
                <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
            </div>
        <?php } ?>
    </div>
<?php } ?>