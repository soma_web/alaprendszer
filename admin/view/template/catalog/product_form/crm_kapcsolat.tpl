<div id="tab-crm_kapcsolat"  class="kepek_helyszinenkent">
    <table class="form">
        <tr>
            <td><?php echo  $text_crm_tipus?></td>
            <td>
                <select name="crm_tipus" onchange="ChangeCrmTipus(this.value)">
                    <option value=""><?php echo $text_select?></option>
                    <option value="1" <?php echo $crm_tipus == 1 ? "selected" : ''?>>Termék</option>
                    <option value="2" <?php echo $crm_tipus == 2 ? "selected" : ''?>>Szolgáltatás</option>
                    <option value="3" <?php echo $crm_tipus == 3 ? "selected" : ''?>>Csomag</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><?php echo  $text_crm_category?></td>
            <td>
                <select name="crm_category_id" onchange="ChangeCrmCategory(this.value)">
                    <option value=""><?php echo $text_select?></option>
                    <?php foreach($crm_categoryes as $crm_category) { ?>
                        <option value="<?php echo $crm_category['productcategoryid']?>" <?php echo $crm_category_id == $crm_category['productcategoryid'] ? "selected" : ''?>><?php echo $crm_category['productcategory']?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><?php echo  $text_crm_product?></td>
            <td>
                <select name="crm_product_id">
                    <option value=""><?php echo $text_select?></option>
                    <?php foreach($crm_products as $crm_product) { ?>
                        <option value="<?php echo $crm_product['productid']; ?>" <?php echo $crm_product_id == $crm_product['productid'] ? "selected" : ''?>><?php echo $crm_product['productname'];?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
    </table>
</div>
<script>
    function ChangeCrmTipus(ertek) {

        $.ajax({
            url: 'index.php?route=catalog/product/CrmTipus&token=<?php echo $token; ?>',
            type: "post",
            data: 'crm_tipus=' + ertek,
            dataType: 'json',

            success: function(json) {
                $('.success, .warning, .error').remove();
                if (json['error']) {
                    if ( json['error']['crmtipus']) {
                        $('.breadcrumb').append('<div class="warning" style="display: none;">' + json['error']['crmtipus'] + '</div>');
                        $('.warning').slideDown("slow");
                    }

                } else {
                    if (json["categoryes"]) {
                        var html = '';
                        html += '<select name="crm_category_id">';
                        html += '<option value=""><?php echo $text_select?></option>';
                        for (i in json['categoryes']) {
                            html += '<option value="'+json['categoryes'][i]['productcategoryid']+'">'+json['categoryes'][i]['productcategory']+'</option>';
                        }
                        html += '</select>';
                    } else {
                        var html = '';
                        html += '<select name="crm_category_id">';
                        html += '<option value=""><?php echo $text_select?></option>';
                        html += '</select>';

                    }
                    $('select[name=crm_category_id]').html(html);


                    if (json["products"]) {
                        var html = '';
                        html += '<select name="crm_product_id">';
                        html += '<option value=""><?php echo $text_select?></option>';
                        for (i in json['products']) {
                            html += '<option value="'+json['products'][i]['productid']+'">'+json['products'][i]['productname']+'</option>';
                        }
                        html += '</select>';
                    } else {
                        var html = '';
                        html += '<select name="crm_product_id">';
                        html += '<option value=""><?php echo $text_select?></option>';
                        html += '</select>';

                    }
                    $('select[name=crm_product_id]').html(html);

                    /*var html = '';
                    html += '<select name="crm_product_id">';
                    html += '<option value=""><?php echo $text_select?></option>';
                    html += '</select>';

                    $('select[name=crm_product_id]').html(html);*/
                }
            },
            error: function(e) {
                debugger;
            }
        });
    }

    function ChangeCrmCategory(ertek) {
        debugger;

        $.ajax({
            url: 'index.php?route=catalog/product/CrmCategory&token=<?php echo $token; ?>',
            type: "post",
            data: 'crm_tipus=' + $('select[name=crm_tipus]').val() + '&crm_category_id=' + ertek,
            dataType: 'json',

            success: function(json) {
                $('.success, .warning, .error').remove();

                if (json['error']) {
                    if ( json['error']['crmtipus']) {
                        $('.breadcrumb').append('<div class="warning" style="display: none;">' + json['error']['crmtipus'] + '</div>');
                        $('.warning').slideDown("slow");
                    }

                } else {
                    if (json["products"]) {
                        var html = '';
                        html += '<select name="crm_product_id">';
                        html += '<option value=""><?php echo $text_select?></option>';
                        for (i in json['products']) {
                            html += '<option value="'+json['products'][i]['productid']+'">'+json['products'][i]['productname']+'</option>';
                        }
                        html += '</select>';
                    } else {
                        var html = '';
                        html += '<select name="crm_product_id">';
                        html += '<option value=""><?php echo $text_select?></option>';
                        html += '</select>';

                    }
                    $('select[name=crm_product_id]').html(html);
                }
            },
            error: function(e) {
                debugger;
            }
        });
    }
</script>