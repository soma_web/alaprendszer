<?php
class ControllerModuleMSBestSeller extends Controller {
	protected function index($setting) {
		static $module = 0; 
 
      	$this->data['heading_title'] = $this->config->get('ms_bestseller_heading_title_' . $this->config->get('config_language_id') );

        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
		$this->data['show_class'] = ($setting['initial'] == "up") ? "active" : "";
        $this->data['button_bovebben'] = $this->language->get('button_bovebben');
        $this->data['module_name'] = "ms_bestseller";

        $this->data['text_netto'] = $this->language->get('text_netto');

        $this->load->model('mobile_store/product');
		
		$this->load->model('tool/image');

		$this->data['products'] = array();

		$results = $this->model_mobile_store_product->getBestSellerProducts($setting['limit']);

        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );
        $results1 = $this->model_catalog_product->getProductsNew($data1);


        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']);
            } else {
                $image = $this->model_tool_image->resize('no_image.jpg', $setting['image_width'], $setting['image_height'], false);

            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }

            if ((float)$result['special']) {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $special = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = $result['rating'];
            } else {
                $rating = false;
            }
            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
            } else {
                $tax = false;
            }

            $uj=false;
            if (array_key_exists($result['product_id'],$results1)){
                $uj=1;
            }
            $this->data['button_wishlist'] = $this->language->get('button_wishlist');
            $this->data['button_compare'] = $this->language->get('button_compare');

            $discount_query = $this->model_catalog_product->getProductDiscounts($result['product_id']);
            $discounts = array();
            $legolcsobb_kulcs=0;
            if($result['price'])  {
                foreach ($discount_query as $discount) {
                    $discounts[] = array(
                        'quantity' => $discount['quantity'],
                        'netto'    => $this->currency->format($discount['price']),
                        'brutto'    => $this->currency->format($this->tax->calculate($discount['price'], $result['tax_class_id'], $this->config->get('config_tax'))),
                        'ara'    => $this->tax->calculate($discount['price'], $result['tax_class_id'], $this->config->get('config_tax'))
                    );
                }
                if (count($discounts) > 0){
                    $legolcsobb=$discounts[0]['ara'];
                    foreach ($discounts as $key => $value) {
                        if ($value['ara'] < $legolcsobb ){
                            $legolcsobb_kulcs=$key;
                            $legolcsobb=$value['ara'];
                        }
                    }
                }
            }
            if (count($discounts) == 0) $discounts[$legolcsobb_kulcs]=false;

            if ($result['quantity'] > 0 && $result['quantity'] <= 4) {
                $pardarab = true;
            } else {
                $pardarab = false;
            }
            $descr= strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'));
            $descr=str_replace("&nbsp;","",$descr);
            $descr=str_replace("\r","",$descr);
            $descr=str_replace("\t","",$descr);
            $descr=str_replace("\n","",$descr);
            $descr=trim($descr);
            $descr=mb_substr($descr, 0, 1000, "UTF-8");

            $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
            $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');

            $this->data['products'][] = array(
                'product_id' => $result['product_id'],
                'szazalek'    => $result['szazalek'],
                'utalvany'    => $result['utalvany'],
                'thumb'   	 => $image,
                'name'    	 => $result['name'],
                'description'=> $descr,
                'price'   	 => $price,
                'special' 	 => $special,
                'date_ervenyes_ig' => $result['date_ervenyes_ig'],
                'tax'         => $tax,
                'price_netto' => $this->currency->format($result['price']),
                'special_netto' => $this->currency->format($result['special']),
                'rating'     => $rating,
                'original_href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                'href'    	 => $this->url->link('mobile_store/product', 'product_id=' . $result['product_id']),
                'pardarab'	 => $pardarab,
                'uj'         => $uj,
                'discounts' => $discounts[$legolcsobb_kulcs],
                'imagedesabled' => $result['imagedesabled'],
                'csomagolasi_egyseg' => $result['csomagolasi_egyseg'],
                'megyseg'    => $result['megyseg'],
                'csomagolasi_mennyiseg' => $result['csomagolasi_mennyiseg'] > 1 ? $result['csomagolasi_mennyiseg'] : 0
            );
        }

		
		/*foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']);
			} else {
				$image = false;
			}
			
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
					
			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}	
			
			if ($this->config->get('config_review_status')) {
				$rating = $result['rating'];
			} else {
				$rating = false;
			}
							
			$this->data['products'][] = array(
				'product_id' => $result['product_id'],
				'thumb'   	 => $image,
				'name'    	 => $result['name'],
				'description'=> mb_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
				'price'   	 => $price,
				'special' 	 => $special,
				'rating'     => $rating,
				'original_href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				'href'    	 => $this->url->link('mobile_store/product', 'product_id=' . $result['product_id'])
			);
		}*/

		$this->data['module'] = $module++;
		
		$template_file = "ms_bestseller.tpl";

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/' . $template_file )) {
			$this->template = $this->config->get('config_template') . '/template/module/' . $template_file;
		} else {
			$this->template = 'default/template/module/' . $template_file;
		}

		$this->render();
	}
}
?>