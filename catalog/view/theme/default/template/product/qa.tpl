<?php if ($qas) { ?>
<?php foreach ($qas as $qa) { ?>
<div class="qa content">
  <div class="q_q">
    <span class="q_details"><?php echo ($qa_display_question_author || $qa_display_question_date) ? "(" : ""; ?>
  <?php if ($qa_display_question_author && $qa['q_author']) { ?>
  <span class="q_qa"><?php echo $qa['q_author']; ?></span><?php echo ($qa_display_question_date) ? ", " : ""; ?>
  <?php } ?>
  <?php if ($qa_display_question_date) { ?>
  <span class="q_qd"><?php echo $qa['date_asked']; ?></span>
  <?php } ?>
  <?php echo ($qa_display_question_author || $qa_display_question_date) ? ")" : ""; ?>
    </span>
    <span class="q_qt"><?php echo $text_question; ?></span> <?php echo $qa['question']; ?>
  </div>
  <div class="q_a">
    <?php if ($qa['answer']) { ?>
        <span class="q_details"><?php echo ($qa_display_answer_author || $qa_display_answer_date) ? "(" : ""; ?>
      <?php if ($qa_display_answer_author && $qa['a_author']) { ?>
      <span class="q_aa"><?php echo $qa['a_author']; ?></span><?php echo ($qa_display_answer_date) ? ", " : ""; ?>
      <?php } ?>
      <?php if ($qa_display_answer_date) { ?>
      <span class="q_ad"><?php echo $qa['date_answered']; ?></span>
      <?php } ?>
      <?php echo ($qa_display_answer_author || $qa_display_answer_date) ? ")" : ""; ?>
        </span>
    <?php } ?>
    <span class="q_at"><?php echo $text_answer; ?></span> <?php echo ($qa['answer']) ? $qa['answer'] : '<em>' . $text_no_answer . '</em>'; ?>
  </div>
</div>
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="content"><?php echo $text_no_questions; ?></div>
<?php } ?>
