<?php

$vattermeknev=$data["product_description"][$this->config->get("config_language_id")]["name"];


if (isset($data["vatera"][0]['vazonosito']) ){

    $this->db->query("INSERT INTO vatera SET
            vazonosito              = '" . $data["vatera"][0]['vazonosito'] . "',
            termeknev               = '" . $data["vatera"][0]['termeknev'] . "',
            alcim                   = '" . $data["vatera"][0]['alcim'] . "',
            darabszam               = '" . $data["vatera"][0]['darabszam'] . "',
            kikialtasiar            = '" . $data["vatera"][0]['kikialtasiar'] . "',
            minimalar               = '" . $data["vatera"][0]['minimalar'] . "',
            villamar                = '" . $data["vatera"][0]['villamar'] . "',
            licitlepcso             = '" . $data["vatera"][0]['licitlepcso'] . "',
            aukcio_inditasa         = '" . $data["vatera"][0]['aukcio_inditasa'] . "',
            aukcio_idotartam        = '" . $data["vatera"][0]['aukcio_idotartam'] . "',
            vatera_kategoria_kod    = '" . $data["vatera"][0]['vatera_kategoria_kod'] . "',
            megye                   = '" . $data["vatera"][0]['megye'] . "',
            orszag                  = '" . $data["vatera"][0]['orszag'] . "',
            sablon_azonosito        = '" . $data["vatera"][0]['sablon_azonosito'] . "',
            ujrainditasok_szama     = '" . $data["vatera"][0]['ujrainditasok_szama'] . "',
            kiemelesek              = '" . $data["vatera"][0]['kiemelesek'] . "',
            termek_jellemzok        = '" . $data["vatera"][0]['termek_jellemzok'] . "',
            garancia                = '" . $data["vatera"][0]['garancia'] . "',
            elfogadas_szazalek      = '" . $data["vatera"][0]['elfogadas_szazalek'] . "',
            kepek1                  = '" . $data["vatera"][0]['kepek1'] . "',
            kepek2                  = '" . $data["vatera"][0]['kepek2'] . "',
            kepek3                  = '" . $data["vatera"][0]['kepek3'] . "',
            kepek4                  = '" . $data["vatera"][0]['kepek4'] . "',
            kinezeti_sablon         = '" . $data["vatera"][0]['kinezeti_sablon'] . "',
            product_id              = '" . (int)$product_id . "',
            kategoria_specifikus    = '" . $data["vatera"][0]['kategoria_specifikus'] . "',
            kepek5                  = '" . $data["vatera"][0]['kepek5'] . "',
            kepek6                  = '" . $data["vatera"][0]['kepek6'] . "',
            kepek7                  = '" . $data["vatera"][0]['kepek7'] . "' ,
            kepek8                  = '" . $data["vatera"][0]['kepek8'] . "',
            leiras                  = '" . $data["vatera"][0]['leiras'] . "'");

} else{
    if (!isset($data['vatkepek1'])) $data['vatkepek1']="";
    if (!isset($data['vatkepek2'])) $data['vatkepek2']="";
    if (!isset($data['vatkepek3'])) $data['vatkepek3']="";
    if (!isset($data['vatkepek4'])) $data['vatkepek4']="";
    if (!isset($data['vatkepek5'])) $data['vatkepek5']="";
    if (!isset($data['vatkepek6'])) $data['vatkepek6']="";
    if (!isset($data['vatkepek7'])) $data['vatkepek7']="";
    if (!isset($data['vatkepek8'])) $data['vatkepek8']="";

    if (!isset($data['quantity'])) $data['quantity']=0;
    if (!isset($data['vatkepek8'])) $data['vatkepek8']="";

    $this->db->query("INSERT INTO vatera SET
                                    vazonosito              = '" . $data['vatvazonosito'] . "',
                                    termeknev               = '" . $vattermeknev . "',
                                    alcim                   = '" . $data['vatalcim'] . "',
                                    darabszam               = '" . $data['quantity'] . "',
                                    kikialtasiar            = '" . $data['vatkikialtasiar'] . "',
                                    minimalar               = '" . $data['vatminimalar'] . "',
                                    villamar                = '" . $data['vatvillamar'] . "',
                                    licitlepcso             = '" . $data['vatlicitlepcso'] . "',
                                    aukcio_inditasa         = '" . $data['vataukcioinditasa'] . "',
                                    aukcio_idotartam        = '" . $data['vataukcioidotartam'] . "',
                                    vatera_kategoria_kod    = '" . $data['vatvaterakategoriakod'] . "',
                                    megye                   = '" . $data['vatmegye'] . "',
                                    orszag                  = '" . $data['vatorszag'] . "',
                                    sablon_azonosito        = '" . $data['vatsablonazonosito'] . "',
                                    ujrainditasok_szama     = '" . $data['vatujrainditasokszama'] . "',
                                    kiemelesek              = '" . $data['vatkiemelesek'] . "',
                                    termek_jellemzok        = '" . $data['vattermekjellemzok'] . "',
                                    garancia                = '" . $data['vatgarancia'] . "',
                                    elfogadas_szazalek      = '" . $data['vatelfogadasszazalek'] . "',
                                    kepek1                  = '" . $data['vatkepek1'] . "',
                                    kepek2                  = '" . $data['vatkepek2'] . "',
                                    kepek3                  = '" . $data['vatkepek3'] . "',
                                    kepek4                  = '" . $data['vatkepek4'] . "',
                                    kinezeti_sablon         = '" . $data['vatkinezetisablon'] . "',
                                    product_id              = '" . (int)$product_id . "',
                                    kategoria_specifikus    = '" . $data['vatkategoriaspecifikus'] . "',
                                    kepek5                  = '" . $data['vatkepek5'] . "',
                                    kepek6                  = '" . $data['vatkepek6'] . "',
                                    kepek7                  = '" . $data['vatkepek7'] . "' ,
                                    kepek8                  = '" . $data['vatkepek8'] . "',
                                    vat_automatikus         = '" . $data['vatautomatikus'] . "',
                                    leiras                  = '" . $data['vatleiras'] . "'");
}

?>