<?php
// Heading
$_['heading_title']       = 'Árajánlat kérés';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeresen módosította az Árajánlat kérés modult!';


// Entry
$_['entry_status']        = 'Státusz:';
$_['entry_ar_tol']        = 'Ár -tól:';
$_['entry_ar_ig']         = 'Ár -ig:';
$_['entry_levalogatott']  = 'Leválogatott termékek:';
$_['entry_hozzaad']       = 'Termék hozzáadása';
$_['entry_kivett']        = '<b>Engedélyezett termékek</b>';

$_['button_levalogat']    = 'Leválogat';


// Error
$_['error_permission'] = 'Figyelmeztetés: Az Árajánlat kérés modul módosítása az Ön számára nem engedélyezett!';
?>