<?php  
class ControllerModulePriceAlert extends Controller {
	public function index() {
		$this->load->model('module/price_alert');
		
    	$texts_all_languages = $this->config->get('price_alert_popup');
		$text_current_language = $texts_all_languages[$this->config->get('config_language_id')];
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['price_alert_desired_price']) ) {
			if (!$json) {
				
				$dinamic_strlen = 'utf8_strlen';
		 
				if ( !function_exists('utf8_strlen') ) {
					$dinamic_strlen = 'strlen';
				}
				
				if (is_numeric(trim($this->request->post['price_alert_desired_price'])) && is_numeric(trim($this->request->post['price_alert_product_price']))) {
					if ((float)$this->request->post['price_alert_desired_price'] >= (float)$this->request->post['price_alert_product_price']) {
						$json['error']['warning'] = $text_current_language['error_price_already'];
					}	
				}
				
				if ($dinamic_strlen(trim($this->request->post['price_alert_desired_price'])) < 1 || !is_numeric(trim($this->request->post['price_alert_desired_price']))){
					$json['error']['warning'] = $text_current_language['error_desired_price'];
				}
				
				if (!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['price_alert_email'])) {
					$json['error']['warning'] = $text_current_language['error_email'];
				}
				
				if ($dinamic_strlen($this->request->post['price_alert_name']) < 1){
					$json['error']['warning'] = $text_current_language['error_name'];
				}
			}
			
			if (!$json) {
				$alert_id = $this->model_module_price_alert->getAlertIdByEmailAndProductId($this->request->post);
				
				if (!$alert_id) {
					$this->model_module_price_alert->addAlert($this->request->post);
				} else {
					$this->model_module_price_alert->editAlert($alert_id, $this->request->post);
				}				
				
				$json['success'] = $text_current_language['success'];
			}
		
		}
		
		$this->response->setOutput(json_encode($json));
		
	}
}
?>