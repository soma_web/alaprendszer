<?php
class ControllerCheckoutCheckoutMegrendelem extends Controller {
    public function index() {

        if ((empty($_SESSION['megrendelem']) ) ) {
            $this->redirect($this->url->link('checkout/cart'));
        }

        $this->language->load('checkout/checkout_megrendelem');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_termekeim'),
            'href'      => $this->url->link('account/product'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('checkout/checkout_megrendelem', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_checkout_account']                = $this->language->get('text_checkout_account');
        $this->data['text_checkout_payment_address']        = $this->language->get('text_checkout_payment_address');
        $this->data['text_checkout_payment_method']         = $this->language->get('text_checkout_payment_method');
        $this->data['text_checkout_option_number']          = $this->language->get('text_checkout_option_number');
        $this->data['text_checkout_option']                 = $this->language->get('text_checkout_option');
        $this->data['text_checkout_account_number']         = $this->language->get('text_checkout_account_number');
        $this->data['text_checkout_account']                = $this->language->get('text_checkout_account');
        $this->data['text_checkout_payment_address_number'] = $this->language->get('text_checkout_payment_address_number');
        $this->data['text_checkout_payment_address']        = $this->language->get('text_checkout_payment_address');
        $this->data['text_checkout_shipping_address_number']= $this->language->get('text_checkout_shipping_address_number');
        $this->data['text_checkout_shipping_address']       = $this->language->get('text_checkout_shipping_address');
        $this->data['text_checkou_nyomtat']                 = $this->language->get('text_checkou_nyomtat');
        $this->data['text_checkout_shipping_method_number'] = $this->language->get('text_checkout_shipping_method_number');
        $this->data['text_checkout_shipping_method']        = $this->language->get('text_checkout_shipping_method');
        $this->data['text_checkout_payment_method_number']  = $this->language->get('text_checkout_payment_method_number');
        $this->data['text_checkout_payment_method']         = $this->language->get('text_checkout_payment_method');
        $this->data['text_checkout_confirm_number']         = $this->language->get('text_checkout_confirm_number');
        $this->data['text_checkout_confirm']                = $this->language->get('text_checkout_confirm');
        $this->data['text_modify']                          = $this->language->get('text_modify');

        $this->data['shipping_required'] = $this->cart->hasShipping("megrendelem");
        $this->data['logged'] = $this->customer->isLogged();

        $products = $this->cart->getProducts(false,false,false,"megrendelem");

        $erkezteto_oldal = false;
        if (isset($this->request->request['ekezteto_oldal']) && $this->request->request['ekezteto_oldal'] == 1)  {
            $erkezteto_oldal = true;
        }
        $this->data['fizetos'] = false;
        $this->data['ingyenes'] = false;


        foreach ($products as $product) {
            if ($product['utalvany'] == null || $product['utalvany'] == 0) {
                $this->data['fizetos'] = true;
            } else {
                $this->data['ingyenes'] = true;
            }
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $this->redirect($this->url->link('checkout/cart'));
            }
        }


        if ($this->data['logged'] != null && isset($_SESSION['customer_id']) && $_SESSION['customer_id'] > 0  )  {
            $this->load->model('account/customer');
            $customer_info = $this->model_account_customer->getCustomer($_SESSION['customer_id']);
        }
        $template = $this->config->get('megjelenit_checkout_template') ? $this->config->get('megjelenit_checkout_template') : "checkout_megrendelem";
        $this->data['kosarban_van'] = count($this->cart->getProducts(false,false,false,"megrendelem"));
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/'.$template.'.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/checkout/'.$template.'.tpl';
        } else {
            $this->template = 'default/template/checkout/'.$template.'.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }
}
?>