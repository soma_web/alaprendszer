<?php
class ControllerCheckoutPaymentMethodMegrendelem extends Controller {

    public $vevo;
    public function index() {
        $this->language->load('checkout/checkout');

        $this->load->model('account/address');

        if ($this->customer->isLogged() && isset($this->session->data['payment_address_id'])) {
            $payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
        } elseif (!empty($this->session->data['guest']['payment'])) {
            $payment_address = $this->session->data['guest']['payment'];
        } else {
            $payment_address['country_id'] = $this->config->get('config_country_id');
            $payment_address['zone_id'] = '';
            $this->session->data['guest']['payment'] = $payment_address;
        }

        if (!empty($payment_address)) {
            // Totals
            $total_data = array();
            $total = 0;
            $taxes = $this->cart->getTaxes('megrendelem');

            $this->load->model('setting/extension');

            $sort_order = array();

            $results = $this->model_setting_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes,"megrendelem");
                }
            }

            // Payment Methods
            $method_data = array();

            $this->load->model('setting/extension');

            $results = $this->model_setting_extension->getExtensions('payment');

            $vanszallitas = $this->cart->hasShipping("megrendelem");

            if ($vanszallitas) {
                $fizetesi_modok = $this->session->data['shipping_method']['fizetesi_mod'];
            }

            foreach ($results as $result) {

                if ($vanszallitas) {
                    $mehet = false;
                    foreach($fizetesi_modok as $fizetes){
                        if ($fizetes['fizetesi_mod'] == $result['code']){
                            $mehet = true;
                            break;
                        }
                    }
                } else {
                    $mehet = true;
                }
                if ($mehet){
                    $vevo_id = $this->customer->getId();
                    $results_extension = $this->model_setting_extension->getExtensionsCustomer($vevo_id);

                    if ($results_extension['num_rows'] > 0){

                        if ($this->config->get($result['code'] . '_status') ) {
                            $this->load->model('payment/' . $result['code']);

                            $method = $this->{'model_payment_' . $result['code']}->getMethod($payment_address, $total);

                            if ($method) {
                                $method_data[$result['code']] = $method;
                            }
                        } else{
                            $mitkeres=$result['code'];
                            $mibenkeres=$results_extension['rows'];
                            foreach ($mibenkeres as $sor){
                                if ($sor['code'] == $mitkeres && $sor['value'] == 1){
                                    $this->load->model('payment/' . $result['code']);

                                    $method = $this->{'model_payment_' . $result['code']}->getMethod($payment_address, $total);

                                    if ($method) {
                                        $method_data[$result['code']] = $method;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    else{
                        if ($this->config->get($result['code'] . '_status') ) {
                            $this->load->model('payment/' . $result['code']);

                            $method = $this->{'model_payment_' . $result['code']}->getMethod($payment_address, $total);

                            if ($method) {
                                $method_data[$result['code']] = $method;
                            }
                        }
                    }
                }

            }

            $sort_order = array();

            foreach ($method_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $method_data);

            if (array_key_exists("bank_transfer",$method_data)) {
                $customer_group_id = $this->customer->isLogged() ? $this->customer->getCustomerGroupId() : $this->config->get('config_customer_group_id');
                if ($this->config->get('bank_transfer_customer_groups') !== ""  && $customer_group_id != $this->config->get('bank_transfer_customer_groups')) {
                    unset($method_data['bank_transfer']);
                }
            }

            $this->session->data['payment_methods'] = $method_data;
        }

        $this->data['text_payment_method'] = $this->language->get('text_payment_method');
        $this->data['text_comments'] = $this->language->get('text_comments');

        $this->data['button_continue'] = $this->language->get('button_continue');

        if (empty($this->session->data['payment_methods'])) {
            $this->data['error_warning'] = sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'));
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['payment_methods'])) {
            $this->data['payment_methods'] = $this->session->data['payment_methods'];
        } else {
            $this->data['payment_methods'] = array();
        }

        if (isset($this->session->data['payment_method']['code'])) {
            $this->data['code'] = $this->session->data['payment_method']['code'];
        } else {
            $this->data['code'] = '';
        }

        if (isset($this->session->data['comment'])) {
            $this->data['comment'] = $this->session->data['comment'];
        } else {
            $this->data['comment'] = '';
        }

        /*if ($this->config->get('config_checkout_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

            if ($information_info) {
                $this->data['text_agree'] = sprintf($this->language->get('text_agree_2'), $this->url->link('information/information/info', 'information_id=' . $this->config->get('config_checkout_id'), 'SSL'), $information_info['title'], $information_info['title']);
            } else {
                $this->data['text_agree'] = '';
            }
        } else {
            $this->data['text_agree'] = '';
        }*/

        $this->data['text_agree'] = '';

        if ($this->config->get('config_checkout_id')) {
            $this->load->model('catalog/information');
            $elolvastam_informaciok = $this->config->get('megjelenit_elolvastam_informaciok_kosar');
            $elolvastam = $this->config->get('megjelenit_elolvastam_kosar');
            $elolvastam = html_entity_decode($elolvastam[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
            $this->load->model('catalog/information');


            if ($elolvastam_informaciok) {

                $information_tomb = array();

                $elolvastam_informaciok =$this->config->rendezes($elolvastam_informaciok,"sort_order");
                foreach($elolvastam_informaciok as $elolvastam_informacio) {
                    $information_info = $this->model_catalog_information->getInformation($elolvastam_informacio['information_id']);
                    if (count($information_info) > 0) {
                        $information_tomb[] = $this->url->link('information/information/info', 'information_id='.$elolvastam_informacio['information_id'], 'SSL');
                        $information_tomb[] = $information_info['title'];
                        $information_tomb[] = $information_info['title'];
                    } else {
                        $information_tomb[] = "";
                        $information_tomb[] = "";
                        $information_tomb[] = "";
                    }

                }
                $this->data['text_agree'] = vsprintf($elolvastam, $information_tomb);
            }
        }

        if (isset($this->session->data['agree'])) {
            $this->data['agree'] = $this->session->data['agree'];
        } else {
            $this->data['agree'] = '';
        }

        if (empty($this->session->data['payment_method'])) {
            foreach($this->session->data['payment_methods'] as $value) {
                $this->session->data['payment_method'] = $value;
                break;
            }
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/payment_method_megrendelem.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/checkout/payment_method_megrendelem.tpl';
        } else {
            $this->template = 'default/template/checkout/payment_method_megrendelem.tpl';
        }

        $this->response->setOutput($this->render());
    }

    public function validate() {
        $this->language->load('checkout/checkout');

        $json = array();

        // Validate if payment address has been set.
        $this->load->model('account/address');

        if ($this->customer->isLogged() && isset($this->session->data['payment_address_id'])) {
            $payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
        } elseif (isset($this->session->data['guest'])) {
            $payment_address = $this->session->data['guest']['payment'];
        }

        if (empty($payment_address)) {
            $json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
        }

        // Validate cart has products and has stock.
        if ( empty($this->session->data['megrendelem']) ) {
            $json['redirect'] = $this->url->link('checkout/cart');
        }

        // Validate minimum quantity requirments.
        $products = $this->cart->getProducts(false,false,false,"megrendelem");

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $json['redirect'] = $this->url->link('checkout/cart');

                break;
            }
        }

        if (!$json) {
            if (!isset($this->request->post['payment_method'])) {
                $json['error']['warning'] = $this->language->get('error_payment');
                $json['error']['payment'] = 'payment';

            } else {
                if (!isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
                    $json['error']['warning'] = $this->language->get('error_payment');
                    $json['error']['payment'] = 'payment';
                }
            }

            if ($this->config->get('config_checkout_id')) {
                $this->load->model('catalog/information');

                if (!isset($this->request->post['agree'])) {
                    $this->load->model('catalog/information');
                    $elolvastam_informaciok = $this->config->get('megjelenit_elolvastam_informaciok_kosar');
                    foreach($elolvastam_informaciok as $elolvastam_informacio) {
                        $information_info = $this->model_catalog_information->getInformation($elolvastam_informacio['information_id']);
                        if (count($information_info) > 0) {
                            break;
                        }
                    }
                    $information_info['title'] = isset($information_info['title']) ? $information_info['title'] : "";
                    $json['error']['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
                    if (isset( $json['error']['payment'])) {
                        unset ( $json['error']['payment']);
                    }
                }
            }

            if (!$json) {
                $this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];

                $this->session->data['comment'] = strip_tags($this->request->post['comment']);
            }
        }

        $this->response->setOutput(json_encode($json));
    }
}
?>