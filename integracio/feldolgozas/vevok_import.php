<?php
class Vevok_Import {                 /* SAP_vevok segédtábla feldolgozása a:
                                        customer
                                        customer_group
                                        address        táblákba.
                                    */







    private $query;
    public $azonosito;
    public $csvadatok;
    public $costumer_group_kod;

    public function __construct($query) {
        $this->query = $query;
        $this->csvadatok=$this->query->query("SELECT * FROM sap_vevok");
    }

    public function Vevok($sorok) {
        $sap_vevokod            = $sorok['vevokod'];
        $sap_vevo_csoportkod    = $sorok['vevo_csoport'];
        $sap_telefonszam        = $sorok['telefonszam'];
        $sap_mail               = $sorok['mail'];
        $sap_adoszam            = $sorok['adoszam'];
        $sap_datum              = date('Y-m-d');
        $sap_jelszo             = md5($sap_mail);

        $sap_vevo_neve=explode(" ", $sorok['vevo_neve']);
        $nev_db=count($sap_vevo_neve);
        $sap_firstname="";
        $sap_lastname="";
        if ($nev_db > 0){
            $sap_firstname=$sap_vevo_neve[0];
            for ($i=1; $i <= $nev_db; $i++){
                $sap_lastname.=$sap_vevo_neve[$i]." ";
            }
        }

        $this->Vevo_Csoport($sap_vevo_csoportkod);


        $sql="SELECT * FROM ".DB_PREFIX."customer WHERE vevokod LIKE '$sap_vevokod'";
        $query= $this->query->query($sql);
        if (isset($query->num_rows) && $query->num_rows > 0){
            $modosit="UPDATE ".DB_PREFIX."customer SET
                                                        firstname	        = '$sap_firstname',
                                                        lastname            = '$sap_lastname',
                                                        email	            = '$sap_mail',
                                                        telephone	        = '$sap_telefonszam',
                                                        password	        = '$sap_jelszo',
                                                        customer_group_id	= '$this->costumer_group_kod',
                                                        adoszam	            = '$sap_adoszam'
                      where vevokod LIKE '$sap_vevokod' ";
            //mysql_query($modosit) or die("Nem sikerült a customer táblába a vevő módosítása");
            mysql_query($modosit) or  Hibalista("Vevők import", "customer", "módosítás", $sap_vevokod);
            $this->Vevohoz_Cim_Felvitel(0,$sorok,1);
            $this->Vevohoz_Cim_Felvitel(1,$sorok,0);

        } else {
            $modosit="INSERT INTO ".DB_PREFIX."customer SET
                                                        customer_id	        = '',
                                                        vevokod             = '$sap_vevokod',
                                                        store_id	        = '0',
                                                        firstname	        = '$sap_firstname',
                                                        lastname            = '$sap_lastname',
                                                        email	            = '$sap_mail',
                                                        telephone	        = '$sap_telefonszam',
                                                        password	        = '$sap_jelszo',
                                                        cart	            = '',
                                                        wishlist	        = '',
                                                        newsletter	        = 0,
                                                        customer_group_id	= '$this->costumer_group_kod',
                                                        ip	                = '',
                                                        status	            = '1',
                                                        approved	        = '1',
                                                        adoszam	            = '$sap_adoszam',
                                                        date_added	        = '$sap_datum'";
            //mysql_query($modosit) or die("Nem sikerült a customer táblába az új vevő létrehozása");
            mysql_query($modosit) or   Hibalista("Vevők import", "customer", "felvitel",  $sap_vevokod);

            $this->Vevohoz_Cim_Felvitel(0,$sorok,0);
            $this->Vevohoz_Cim_Felvitel(1,$sorok,0);

            $sql="SELECT * FROM ".DB_PREFIX."customer WHERE vevokod LIKE '$sap_vevokod'";
            $query= $this->query->query($sql);
            $vevo_id=$query->row['customer_id'];

            $sql="SELECT * FROM ".DB_PREFIX."address WHERE customer_id=$vevo_id";
            $query= $this->query->query($sql);
            $sap_cimek = $query->row['address_id'];

            $modosit="UPDATE ".DB_PREFIX."customer  SET  address_id = '$sap_cimek'
                                        WHERE vevokod LIKE '$sap_vevokod'";

            //mysql_query($modosit) or die("Nem sikerült a customer táblába az új vevő cím azonosító rögzítése");
            mysql_query($modosit) or Hibalista("Vevők import", "customer",  "módosítás", $sap_vevokod);
        }
    }

    public function Vevo_Csoport($sap_vevo_csoportkod) {
        $sql="SELECT * FROM ".DB_PREFIX."customer_group WHERE csoportkod LIKE '$sap_vevo_csoportkod'";
        $query= $this->query->query($sql);
        if (!isset($query->num_rows) ||  $query->num_rows == 0){
            $modosit="INSERT INTO ".DB_PREFIX."customer_group SET
                                                        customer_group_id   = '',
                                                        csoportkod          = '$sap_vevo_csoportkod',
                                                        name    	        = '$sap_vevo_csoportkod' ";
            //mysql_query($modosit) or die("Nem sikerült a customer_group táblába az új vevőcsoportot létrehozni");
            mysql_query($modosit) or  Hibalista("Vevők import", "customer_group", "felvitel", $sap_vevo_csoportkod);

            $sql="SELECT * FROM ".DB_PREFIX."customer_group WHERE csoportkod LIKE '$sap_vevo_csoportkod'";
            $query= $this->query->query($sql);
            $this->costumer_group_kod=$query->row['customer_group_id'];
        } else {
            $this->costumer_group_kod=$query->row['customer_group_id'];
        }

    }

    public function Vevohoz_Cim_Felvitel($szamlazasi,$sorok,$modositas) {

        if ($szamlazasi==1){
            $sap_iranyitoszam = $sorok['szall_iranyitoszam'];
            $sap_varos        = $sorok['szall_varos'];
            $sap_utca         = $sorok['szall_utca'];
            $sap_orszagkod    = $sorok['szall_orszagkod'];
        } else {
            $sap_iranyitoszam = $sorok['szaml_iranyitoszam'];
            $sap_varos        = $sorok['szaml_varos'];
            $sap_utca         = $sorok['szaml_utca'];
            $sap_orszagkod    = $sorok['szaml_orszagkod'];
        }
        $sap_vevokod            = $sorok['vevokod'];
        $sap_vevo_neve=explode(" ", $sorok['vevo_neve']);
        $nev_db=count($sap_vevo_neve);
        $sap_firstname="";
        $sap_lastname="";
        if ($nev_db > 0){
            $sap_firstname=$sap_vevo_neve[0];
            for ($i=1; $i <= $nev_db; $i++){
                $sap_lastname.=$sap_vevo_neve[$i];
            }
        }


        $hossz=strlen($sap_orszagkod);
        if ($hossz == 2){
            $sql="SELECT * FROM ".DB_PREFIX."country WHERE iso_code_2 LIKE '$sap_orszagkod'";
            $query= $this->query->query($sql);
            $sap_country_id=$query->row['country_id'];
        } elseif ($hossz == 3){
            $sql="SELECT * FROM ".DB_PREFIX."country WHERE iso_code_3 LIKE '$sap_orszagkod'";
            $query= $this->query->query($sql);
            $sap_country_id=$query->row['country_id'];

        } else {
            $sap_orszagkod="HU";
            $sql="SELECT * FROM ".DB_PREFIX."country WHERE iso_code_2 LIKE '$sap_orszagkod'";
            $query= $this->query->query($sql);
            $sap_country_id=$query->row['country_id'];
        }

        $sql="SELECT * FROM ".DB_PREFIX."customer WHERE vevokod LIKE '$sap_vevokod'";
        $query= $this->query->query($sql);
        $vevo_id = $query->row['customer_id'];

        if ($modositas == 1){
            $modosit="DELETE FROM ".DB_PREFIX."address WHERE customer_id = $vevo_id";
            //mysql_query($modosit) or die("Nem sikerült az address táblában az vevő szállítási címeinek a törlése");
            mysql_query($modosit) or Hibalista("Vevők import", "customer_group", "törlés", $vevo_id);
        }
        $modosit="INSERT INTO ".DB_PREFIX."address  SET
                                                    address_id	        = '',
	                                                customer_id	        = '$vevo_id',
	                                                firstname	        = '$sap_firstname',
	                                                lastname	        = '$sap_lastname',
	                                                company	            = '',
	                                                address_1	        = '$sap_utca',
	                                                address_2	        = '',
	                                                city	            = '$sap_varos',
	                                                postcode	        = '$sap_iranyitoszam',
	                                                country_id	        = '$sap_country_id',
                                                	zone_id	            = '0'";
        //mysql_query($modosit) or die("Nem sikerült az address táblában az új vevő szállítási címének a rögzítése");
        mysql_query($modosit) or Hibalista("Vevők import", "customer_group", "felvitel", $vevo_id);
    }
}
?>