<?php
class ControllerModulePop3Freemail extends Controller {

    private $address = array();
    private $collect = array();
    private $encode;

    public function index() {
        $vissza = true;



        if (!empty($_POST['pop3']) && !empty($_POST['address']) && !empty($_POST['email_password'])  ) {

            require_once(DIR_SYSTEM."library/Encoding.php");
            $this->encode = new Encoding();


            $mb = @imap_open($_POST['pop3'], $_POST['address'], $_POST['email_password'],1,0 );
            imap_errors();
            imap_alerts();
            //$mb = false;


            if ($mb) {
                $messageCount = imap_num_msg($mb);
                if ($messageCount > 0) {
                    $vissza = 1;
                } else {
                    $vissza = '';
                }

                for( $MID = 1; $MID <= $messageCount; $MID++ ) {
                    $EmailHeaders = imap_headerinfo( $mb, $MID );
                    if (!empty($EmailHeaders->to)) {
                        $this->collect = $EmailHeaders->to;
                        $this->emailCollect();
                    }

                    if (!empty($EmailHeaders->from)) {
                        $this->collect = $EmailHeaders->from;
                        $this->emailCollect();
                    }

                }

                imap_close($mb);

                if ($this->address && !empty($_POST['interested_id'])) {
                    $sql = "INSERT INTO ".DB_PREFIX."interested_to SET
                        interested_id   = '".$_POST['interested_id']."',
                        oldal_neve      = 'emails->".$_POST['email_kulcs']."',
                        kulcs           = '9999999'";
                    $query = $this->db->query($sql);

                    /* ------------ CRM ------------ */

                    /*$mezo = $_POST['email_kulcs'].'=1';

                    if (!empty($mezo) && !empty($_POST['crm_kalkulator_id'])) {
                        $sql = "UPDATE vtiger_kalkulator SET ".$mezo." WHERE kalkulatorid = '".$_POST['crm_kalkulator_id']."'";
                        $query = $this->db_crm->query($sql);
                    }*/
                    /* ------------ CRM vége ------------ */
                }

                foreach ($this->address as $key=>$title) {
                    $sql = "SELECT email FROM ".DB_PREFIX."gmail_cimek WHERE email='".$key."'";
                    $query  = $this->db->query($sql);
                    if($query->num_rows == 0) {
                        $sql = "INSERT INTO ".DB_PREFIX."gmail_cimek SET
                        email   ='".$key."',
                        `user`  = '".$title."',
                        honnan  = '".(!empty($_POST['email_kulcs']) ? $_POST['email_kulcs'] : "")."',
                        ajanlo  = '".$_POST['ajanlo']."'";
                        $query = $this->db->query($sql);
                    }
                    if ($_POST['to_crm']) {
                        $this->crmexport->writeEmail($key,$title);
                    }
                }

            } else {
                $vissza = '';
            }
        } else {
            $vissza = '';
        }


        $this->response->setOutput($vissza);

    }

    private function emailCollect() {
        foreach($this->collect as $value) {
            if (!empty($value->personal)) {
                $text_conv = imap_mime_header_decode($value->personal);
                $text_conv = $text_conv[0]->text;
            }
            $this->address[$this->encode->toUTF8($value->mailbox."@".$value->host)] = !empty($text_conv) ? $this->encode->toUTF8($text_conv) : '';
        }
    }
}
?>