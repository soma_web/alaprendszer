<?php

// Heading
$_['heading_title']  = 'Jelszócsere';

// Text
$_['text_account']   = 'Fiók';
$_['text_password']  = 'Az Ön jelszava';
$_['text_success']   = 'Sikeresen megváltoztatta a jelszavát!';
$_['text_password_changed_subject'] = 'Értesítés jelszó változtatásról';
$_['text_password_changed_html'] = 'Ön sikeresen megváltoztatta jelszavát a(z) %s oldalon.';
$_['text_password_changed_text'] = 'Ön sikeresen megváltoztatta jelszavát a(z) %s oldalon.';
$_['button_continue']     = 'Változtatások mentése';

// Entry
$_['entry_password'] = 'Jelszó:';
$_['entry_confirm']  = 'Jelszó megerősítése:';

// Error
$_['error_password'] = 'A jelszó legalább 3 és legfeljebb 20 karakterből állhat!';
$_['error_confirm']  = 'A jelszó megerősítése nem egyezik meg a jelszóval!';
?>
