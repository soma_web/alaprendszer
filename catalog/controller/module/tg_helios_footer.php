<?php  
class ControllerModuletgheliosfooter extends Controller {
//class ControllerModuletgpradafooter extends Controller {

    public $footer_default = array();
    public $footer_beallitasok = array();


	public function index($module) {

        $aruhaz_id = $module->config->get('config_store_id') ? $module->config->get('config_store_id') : '';
        $nyelv_kod = $module->config->get('config_language_id');
        $nyelv_kod .= $aruhaz_id ? '_'.$aruhaz_id : '';

        $this->footer_default  = array(
            'sort' => 100,
            'module_name'       => 'tg_helios_footer_default',
            'status'            => $module->config->get('tg_helios_footer_default_show'.$aruhaz_id),
            'valtozok'          => $module->data,
            'megjelenes_alul'   => $module->config->get('tg_helios_footer_default_show_alul'.$aruhaz_id)
        );

        if ($module->config->get('tg_helios_footer_info_status'.$aruhaz_id) == 1){
            $this->footer_beallitasok[]  = array(
                'sort'          => $module->config->get('tg_helios_footer_info_sort'.$aruhaz_id),
                'module_name'   => 'tg_helios_footer_info',
                'status'        => $module->config->get('tg_helios_footer_info_status'.$aruhaz_id),
                'mymodule_code' => $module->config->get('mymodule_code' . $nyelv_kod),
                'mymodule_title' =>  $module->config->get('mymodule_title' .$nyelv_kod),

                'fuggoleges'        => $module->config->get('tg_helios_footer_info_fuggoleges'.$aruhaz_id),
                'width'        => $module->config->get('tg_helios_footer_info_szelesseg'.$aruhaz_id),
                'height'        => $module->config->get('tg_helios_footer_info_magassag'.$aruhaz_id),

            );
        }

        if ($module->config->get('tg_helios_footer_contact_status'.$aruhaz_id) == 1){
            $this->footer_beallitasok[]  = array(
                'sort'              => $module->config->get('tg_helios_footer_contact_sort'.$aruhaz_id),
                'module_name'       => 'tg_helios_footer_contact',
                'status'            => $module->config->get('tg_helios_footer_contact_status'.$aruhaz_id),
                'telefon_show'      => $module->config->get('tg_helios_footer_contact_phone_show'.$aruhaz_id),
                'telefon'           => $module->config->get('tg_helios_footer_contact_phone'.$aruhaz_id),
                'mobile_show'       => $module->config->get('tg_helios_footer_contact_mobile_show'.$aruhaz_id),
                'mobile'            => $module->config->get('tg_helios_footer_contact_mobile'.$aruhaz_id),
                'email_show'        => $module->config->get('tg_helios_footer_contact_email_show'.$aruhaz_id),
                'email'             => $module->config->get('tg_helios_footer_contact_email'.$aruhaz_id),
                'skype_show'        => $module->config->get('tg_helios_footer_contact_skype_show'.$aruhaz_id),
                'skype'             => $module->config->get('tg_helios_footer_contact_skype'.$aruhaz_id),
                'address_show'      => $module->config->get('tg_helios_footer_contact_address_show'.$aruhaz_id),
                'address'           => $module->config->get('tg_helios_footer_contact_address'.$aruhaz_id),
                'mymodule_title2'   => $module->config->get('mymodule_title2' . $nyelv_kod),

                'fuggoleges'        => $module->config->get('tg_helios_footer_contact_fuggoleges'.$aruhaz_id),
                'width'        => $module->config->get('tg_helios_footer_contact_szelesseg'.$aruhaz_id),
                'height'        => $module->config->get('tg_helios_footer_contact_magassag'.$aruhaz_id)
            );
        }

        if ($module->config->get('tg_helios_footer_twitter_status'.$aruhaz_id) == 1){
            $this->footer_beallitasok[]  = array(
                'sort'              => $module->config->get('tg_helios_footer_twitter_sort'.$aruhaz_id),
                'module_name'       => 'tg_helios_footer_twitter',
                'status'            => $module->config->get('tg_helios_footer_twitter_status'.$aruhaz_id),
                'username'          => $module->config->get('tg_helios_footer_twitter_username'.$aruhaz_id),
                'tweets'            => $module->config->get('tg_helios_footer_twitter_tweets'.$aruhaz_id),
                'mymodule_title3'   => $module->config->get('mymodule_title3' . $nyelv_kod),

                'fuggoleges'        => $module->config->get('tg_helios_footer_twitter_fuggoleges'.$aruhaz_id),
                'width'        => $module->config->get('tg_helios_footer_twitter_szelesseg'.$aruhaz_id),
                'height'        => $module->config->get('tg_helios_footer_twitter_magassag'.$aruhaz_id)

            );
        }

        if ($module->config->get('tg_helios_footer_facebook_fanpage_status'.$aruhaz_id) == 1){
            $this->footer_beallitasok[]  = array(
                'sort'              => $module->config->get('tg_helios_footer_facebook_fanpage_sort'.$aruhaz_id),
                'module_name'       => 'tg_helios_footer_fbfanpage',
                'status'            => $module->config->get('tg_helios_footer_facebook_fanpage_status'.$aruhaz_id),
                'fpid'              => $module->config->get('tg_helios_footer_facebook_fanpage_id'.$aruhaz_id),
                'mymodule_title4'   => $module->config->get('mymodule_title4' . $nyelv_kod),

                'fuggoleges'        => $module->config->get('tg_helios_footer_fanpage_fuggoleges'.$aruhaz_id),
                'width'        => $module->config->get('tg_helios_footer_fanpage_szelesseg'.$aruhaz_id),
                'height'        => $module->config->get('tg_helios_footer_fanpage_magassag'.$aruhaz_id)
            );
        }

        if ($module->config->get('tg_helios_footer_terkep_status'.$aruhaz_id) == 1){
            $this->footer_beallitasok[]  = array(
                'sort' => $module->config->get('tg_helios_footer_terkep_sort'.$aruhaz_id),
                'module_name'  => 'tg_helios_footer_terkep',
                'status' => $module->config->get('tg_helios_footer_terkep_status'.$aruhaz_id),
                'magassag' => $module->config->get('tg_helios_footer_terkep_magassag'.$aruhaz_id),
                'szelesseg' => $module->config->get('tg_helios_footer_terkep_szelesseg'.$aruhaz_id),
                'terkep' => $module->data['terkep'],

                'fuggoleges'        => $module->config->get('tg_helios_footer_terkep_fuggoleges'.$aruhaz_id),
                'width'        => $module->config->get('tg_helios_footer_terkep_szelesseg'.$aruhaz_id),
                'height'        => $module->config->get('tg_helios_footer_terkep_magassag'.$aruhaz_id)
            );
        }

        if ($module->config->get('tg_helios_footer_facebook_gomb_status'.$aruhaz_id) == 1){
            $this->footer_beallitasok[]  = array(
                'sort' => $module->config->get('tg_helios_footer_facebook_gomb_sort'.$aruhaz_id),
                'module_name' => 'tg_helios_footer_fbgomb',

                'status' => $module->config->get('tg_helios_footer_facebook_gomb_status'.$aruhaz_id),
                'gomb0' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb0'.$aruhaz_id)),
                'gomb0_show' => $module->config->get('tg_helios_footer_facebook_gomb0_show'.$aruhaz_id),

                'gomb1' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb1'.$aruhaz_id)),
                'gomb1_show' => $module->config->get('tg_helios_footer_facebook_gomb1_show'.$aruhaz_id),

                'gomb2' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb2'.$aruhaz_id)),
                'gomb2_show' => $module->config->get('tg_helios_footer_facebook_gomb2_show'.$aruhaz_id),

                'gomb3' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb3'.$aruhaz_id)),
                'gomb3_show' => $module->config->get('tg_helios_footer_facebook_gomb3_show'.$aruhaz_id),

                'gomb4' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb4'.$aruhaz_id)),
                'gomb4_show' => $module->config->get('tg_helios_footer_facebook_gomb4_show'.$aruhaz_id),

                'gomb5' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb5'.$aruhaz_id)),
                'gomb5_show' => $module->config->get('tg_helios_footer_facebook_gomb5_show'.$aruhaz_id),

                'gomb6' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb6'.$aruhaz_id)),
                'gomb6_show' => $module->config->get('tg_helios_footer_facebook_gomb6_show'.$aruhaz_id),

                'gomb7' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb7'.$aruhaz_id)),
                'gomb7_show' => $module->config->get('tg_helios_footer_facebook_gomb7_show'.$aruhaz_id),

                'gomb8' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb8'.$aruhaz_id)),
                'gomb8_show' => $module->config->get('tg_helios_footer_facebook_gomb8_show'.$aruhaz_id),

                'gomb9' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb9'.$aruhaz_id)),
                'gomb9_show' => $module->config->get('tg_helios_footer_facebook_gomb9_show'.$aruhaz_id),

                'gomb10' => html_entity_decode($module->config->get('tg_helios_footer_facebook_gomb10'.$aruhaz_id)),
                'gomb10_show' => $module->config->get('tg_helios_footer_facebook_gomb10_show'.$aruhaz_id),


                'fuggoleges'        => $module->config->get('tg_helios_footer_facebook_gomb_fuggoleges'.$aruhaz_id),
                'width'        => $module->config->get('tg_helios_footer_facebook_gomb_szelesseg'.$aruhaz_id),
                'height'        => $module->config->get('tg_helios_footer_facebook_gomb_magassag'.$aruhaz_id)
            );
        }

        if ($module->config->get('tg_helios_footer_egyeb_status'.$aruhaz_id) == 1){
            $this->footer_beallitasok[]  = array(
                'sort' => $module->config->get('tg_helios_footer_egyeb_sort'.$aruhaz_id),
                'module_name' => 'tg_helios_footer_egyeb',
                'status' => $module->config->get('tg_helios_footer_egyeb_status'.$aruhaz_id),
                'box' => html_entity_decode($module->config->get('tg_helios_footer_egyeb'.$aruhaz_id)),

                'fuggoleges'        => $module->config->get('tg_helios_footer_egyeb_fuggoleges'.$aruhaz_id),
                'width'        => $module->config->get('tg_helios_footer_egyeb_szelesseg'.$aruhaz_id),
                'height'        => $module->config->get('tg_helios_footer_egyeb_magassag'.$aruhaz_id)
            );
        }


        $this->footer_beallitasok = $module->config->rendezes( $this->footer_beallitasok, "sort", "ASC", "fuggoleges","ASC");



        $this->data['footer_beallitasok'] = $this->footer_beallitasok;
        $this->data['footer_default'] = $this->footer_default;



    }


}
?>