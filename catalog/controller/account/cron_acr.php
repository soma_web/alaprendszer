<?php   
class ControllerAccountCronACR extends Controller {
	public function index() {
	
		$this->language->load('mail/abandoned_cart_reminder');
		
		$this->load->model('module/abandoned_cart_reminder');		
		$this->load->model('catalog/product');
		
		if (!isset($this->request->get['secret_code'])){
			echo $this->language->get('text_log_mail_sent_to');
			exit;
		}
		
		if ($this->request->get['secret_code'] != $this->config->get('abandoned_cart_reminder_secret_code')){
			echo $this->language->get('text_log_mail_sent_to');
			exit;
		}
		
		$log_subject = $this->language->get('text_log_subject');
		
		$operation_type = 'send';
		
		if (isset($this->request->get['op_type'])){
			$operation_type = $this->request->get['op_type'];
		}
		
		// will remove customers with cart content: 
		// - products not available anymore in store 
		// - or same products from prevoius order (related to some payment extensions who forgot to claer cart after order is complete )
		$customers = $this->filterCustomers($this->model_module_abandoned_cart_reminder->getCustomersForReminder());
		
		if ($customers){
			
			$log_message = $this->language->get('text_log_mail_sent_to') . "\n";
			
			$subject  = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
			
			if ($operation_type == 'preview'){
				
				shuffle($customers);
				$customer = end($customers);
				
				$html = $this->getReminderHtml($customer, $operation_type);
				
				if ($this->config->get('abandoned_cart_reminder_use_html_email') && $this->isHTMLEmailExtensionInstalled()) {
					$this->load->model('tool/html_email');
					$html = $this->model_tool_html_email->getHTMLEmail($this->config->get('config_language_id'), $subject, $html, 'html');
				}
				
				echo $html;
			
			} else {
					
				$mail = new Mail(); 
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');	
				$mail->setSubject($subject);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
			
				foreach($customers as $customer){
					$html = $this->getReminderHtml($customer, $operation_type);
					
					if ($this->config->get('abandoned_cart_reminder_use_html_email') && $this->isHTMLEmailExtensionInstalled()) {
						$this->load->model('tool/html_email');
						$html = $this->model_tool_html_email->getHTMLEmail($this->config->get('config_language_id'), $subject, $html, 'html');
					}
					
					$mail->setTo($customer['email']);
					$mail->setHtml($html);
					$mail->send();
					
					if ( $this->config->get('abandoned_cart_reminder_add_coupon') && ((int)$this->config->get('abandoned_cart_reminder_reward_limit') == 0 || $customer['number_reward_sent'] == 0 || (int)$this->config->get('abandoned_cart_reminder_reward_limit') < $customer['number_reward_sent'] )) {
						$coupon_attached = true;
					} else {
						$coupon_attached = false;
					}
					
					$log_message .= $customer['email'] . "; " . $customer['firstname'] . " " . $customer['lastname']. " ";
					echo " ". $this->language->get('text_reminder_sent_to') . " " . $customer['email'];
					
					if (!$coupon_attached) {
						$log_message .= sprintf($this->language->get('text_log_mail_no_coupon'), $customer['number_reward_sent']) . "\n";
						echo $this->language->get('text_without_coupon') . $customer['number_reward_sent'] . $this->language->get('text_sent_coupon');
					}
					
					$this->model_module_abandoned_cart_reminder->increaseNumberReminderSent($customer['customer_id']);
					
					if ($coupon_attached) {
						$this->model_module_abandoned_cart_reminder->increaseNumberRewardSent($customer['customer_id']);
					}	
				}
			}	
				
		} else {
			echo $this->language->get('text_no_customer_to_alert');
			$log_message = $this->language->get('text_log_no_customers');
		}	

		if ($operation_type != 'preview' && $this->config->get('abandoned_cart_reminder_log_admin')){   // if is not preview mode will send log summary to admin
			
			// send summary mail to admin
			$mail = new Mail(); 
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');	
			$mail->setSubject($log_subject);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->language->get('text_email_sender'));
			$mail->setTo($this->config->get('config_email'));
			$mail->setText(html_entity_decode($log_message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}		
		
		$this->model_module_abandoned_cart_reminder->deleteExpiredCoupons();
	}

	private function getReminderHtml($customer_info, $operation_type){
	
		$this->language->load('mail/abandoned_cart_reminder');
		
		$this->load->model('module/abandoned_cart_reminder');
		$this->load->model('catalog/product');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		
		$template = new Template();
		
		$template->data['table_border_color'] = $this->config->get('html_email_main_table_border_color');				
		$template->data['table_body_bg'] = $this->config->get('html_email_main_table_body_bg');		
		$template->data['table_body_text_color'] = $this->config->get('html_email_main_table_body_text_color');		
		
		$template->data['logo'] = $server . 'image/' . $this->config->get('config_logo');		
		$template->data['store_name'] = $this->config->get('config_name');	
		$template->data['store_url'] = $this->config->get('config_use_ssl') ? $this->config->get('config_ssl') : $this->config->get('config_url');
		
		$template->data['title'] = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
		$template->data['text_hi'] = sprintf($this->language->get('text_hi'), $customer_info['firstname'] . ' ' . $customer_info['lastname']);
		$template->data['text_info'] = $this->language->get('text_info');
		$template->data['text_info2'] = $this->language->get('text_info2'); // for second mail
		$template->data['text_cart_content'] = $this->language->get('text_cart_content');
		$template->data['text_view_product_detail'] = $this->language->get('text_view_product_detail');
		$template->data['text_shop_methods'] = sprintf($this->language->get('text_shop_methods'), $this->url->link('account/login', '', 'SSL'), $this->config->get('config_telephone') ); // for second mail
	
	
		$template->data['products'] = $this->getCartProducts($customer_info);
		
		if ( $this->config->get('abandoned_cart_reminder_add_coupon') && ((int)$this->config->get('abandoned_cart_reminder_reward_limit') == 0 || $customer_info['number_reward_sent'] == 0 || (int)$this->config->get('abandoned_cart_reminder_reward_limit') < $customer_info['number_reward_sent'] )) {
			$allow_coupon = true;
		} else {
			$allow_coupon = false;
		}
		
		$template->data['allow_coupon'] = $allow_coupon;
		
		$coupon_code = ($operation_type == 'preview')? 'preview' : $this->generateCode($customer_info);
		
		if ($allow_coupon && $operation_type != 'preview') {
			$this->model_module_abandoned_cart_reminder->addCoupon($coupon_code, $customer_info);
		}
		
		$template->data['text_coupon']        = sprintf($this->language->get('text_coupon'), $this->config->get('abandoned_cart_reminder_coupon_expire') );
		$template->data['text_coupon2']       = sprintf($this->language->get('text_coupon2'), $this->config->get('abandoned_cart_reminder_coupon_expire') );
		$template->data['text_coupon_code']   = sprintf($this->language->get('text_coupon_code'), $coupon_code );
		$template->data['text_coupon_notice'] = $this->language->get('text_coupon_notice');
		$template->data['text_order_now']     = sprintf($this->language->get('text_order_now'), $this->url->link('account/login', '', 'SSL'));
		$template->data['text_need_help']     = $this->language->get('text_need_help'); // for second mail
		
		$template->data['text_thanks'] = sprintf($this->language->get('text_thanks'), $this->config->get('config_name'), $this->config->get('config_email'), $this->config->get('config_telephone'), $this->config->get('config_use_ssl') ? $this->config->get('config_ssl') : $this->config->get('config_url') );
		
		if ($this->config->get('abandoned_cart_reminder_use_html_email') && $this->isHTMLEmailExtensionInstalled()) {
			$tpl_prefix = 'html_email_';
		} else {
			$tpl_prefix = '';
		}
		
		if ($customer_info['number_reminder_sent'] == 0) {
			$template_file = $tpl_prefix . 'acr.tpl';
		} else {
			$template_file = $tpl_prefix . 'acr2.tpl';
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/' . $template_file)) {
			$html = $template->fetch($this->config->get('config_template') . '/template/mail/' . $template_file);
		} else {
			$html = $template->fetch('default/template/mail/' . $template_file);
		}
		
		return $html;
	}

	private function getCartProducts($customer_info){
		$this->load->model('tool/image');
		
		$cart_products = array();
		
		if ($customer_info['cart'] && is_string($customer_info['cart'])) {
			$cart = unserialize($customer_info['cart']);
			
			foreach ($cart as $key => $value) {
				$key_split = explode(":", $key);
				$product_id = $key_split[0];
				
				$product_info = $this->model_catalog_product->getProduct($product_id);
				
				if ($product_info){ 
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], 50,50);
					} else {
						$image = $this->model_tool_image->resize('no_image.jpg', 50,50);
					}
				
					$cart_products[] = array( 
						'name'     => $product_info['name'],
						'image'    => $image,
						'quantity' => $value,
						'href'  => $this->url->link('product/product', 'product_id=' . $product_info['product_id'], 'SSL')
					);
				}
			}			
		}
		
		return $cart_products;
	}	
	
	private function hasActiveProducts($customer_info) {
		$active_products = 0;
		
		if ($customer_info['cart'] && is_string($customer_info['cart'])) {
			$cart = unserialize($customer_info['cart']);
			
			foreach ($cart as $key => $value) {
				$key_split = explode(":", $key);
				$product_id = $key_split[0];
				
				$product_info = $this->model_catalog_product->getProduct($product_id);
				
				if ($product_info){ 
					$active_products++;
				}
			}			
		}
		
		return $active_products;
	}
	
	private function isPreviousOrder($customer_info) {
		$is_previous_order = false;
		
		$last_order_id = $this->model_module_abandoned_cart_reminder->getLastOrderId($customer_info['customer_id']);
		
		if ($last_order_id) {
			$last_order_products = $this->model_module_abandoned_cart_reminder->getLastOrderProducts($last_order_id);
		
			if ($customer_info['cart'] && is_string($customer_info['cart'])) {
				$cart = unserialize($customer_info['cart']);
				
				if ($this->hasSameProducts($cart, $last_order_products)) {
					$is_previous_order = true;
				}				
			}
		}
	
		return $is_previous_order;
	}
	
	private function hasSameProducts($cart, $last_order_products) {
		$same_products = true;  
		
		if (count($cart) != count($last_order_products)) {
			$same_products = false;
		} else {
		
			foreach ($cart as $key => $value) {
				$key_split = explode(":", $key);
				$product_id = $key_split[0];
				
				if (!in_array($product_id, $last_order_products)) {
					$same_products = false;
				}
			}	
		}
		
		return $same_products;
	}
	
	private function filterCustomers($customers) {
		$filtred_customers = array();
		
		if ($customers) {
			foreach($customers as $customer) {
				if ($this->hasActiveProducts($customer) && !$this->isPreviousOrder($customer)) {
					$filtred_customers[] = $customer; 
				}
			}			
		}
		
		return $filtred_customers;
	}
	
	private function generateCode($customer_info){
		$code = 'C' . $customer_info['customer_id'];
		$temp_len = strlen($code);
		$diff = 10 - $temp_len;
		$ucode = md5(time());
		$code .= substr($ucode,0, $diff);
		
		return strtoupper($code);
	}
	
	private function isHTMLEmailExtensionInstalled() {
		$installed = false;
		
		if ($this->config->get('html_email_default_word') && file_exists(DIR_APPLICATION . 'model/tool/html_email.php')) {
			$installed = true;	
		}
		
		return $installed;
	}
	
}
?>