<?php
// Heading
$_['heading_title']       = 'Oldal Tartalom';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeresen módosította az oldal tartalom modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_date_instructions']   = 'nn-hh-éééé (pl: 17-03-2011) vagy hagyja üresen';

// Entry
$_['entry_title'] 		  = 'Oldal tartalom címe:';
$_['entry_startdate'] 	  = 'Kezdő Dátum:';
$_['entry_enddate'] 	  = 'Vég Dátum:';
$_['entry_description']   = 'Oldal tartalom csökk.:';
$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';

// Error
$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága módosítani az oldal tartalom modult!';
?>