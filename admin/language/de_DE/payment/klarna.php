<?php
$_['heading_title']       = 'Klarna';
$_['entry_total']         = 'Summe:<br /><span class="help">Der Warenkorb muss diese Summe beinhalten, damit dieses Zahlungsverfahren verfügbar ist.</span>';
$_['text_beta']           = 'Beta';
$_['text_success']        = 'Erfolgreich: Zahlungsverfahren Web Payment Software erfolgreich geändert!';
$_['text_klarna']	      = '<a onclick="window.open(\'http://www.klarna.com\');"><img src="view/image/payment/klarna.png" alt="Klarna" title="Klarna" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']           = 'Live';
$_['text_payment']        = 'Zahlung';
$_['error_secret']        = 'Schlüssel erforderlich!';
$_['entry_invoice']       = 'Rechnung:<br /><span class="help">Möchten Sie automatische Rechnung aktivieren?</span>';
$_['entry_invoice_delay'] = 'Rechnungsverzögerung:<br /><span class="help">Bei der Autoaktivierung wird die Rechnung für x Tage gesendet.</span>';
$_['entry_merchant']      = 'Klarna Händlernummer:<br /><span class="help">(estore id) für den Klarna service (wird von Klarna bereitgestellt.</span>';
$_['entry_order_status']  = 'Auftragsstatus:';
$_['entry_secret']        = 'Klarna Schlüssel:<br /><span class="help">Schlüssel, der mit dem Klarna service benutzt wird (wird von Klarna bereitgestellt.</span>';
$_['entry_server']        = 'Server:';
$_['entry_sort_order']    = 'Reihenfolge:';
$_['entry_status']        = 'Status:';
$_['entry_test']          = 'Testmodus:';
$_['error_merchant']      = 'Händlernummer erforderlich!';
$_['error_permission']    = 'Warnung: Sie haben keine Berechtigung, um Klarna zu ändern!';
?>
