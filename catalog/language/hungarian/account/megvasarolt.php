<?php
// Heading
$_['heading_title']              = 'Letöltött kuponok';
$_['heading_title_katagoria']    = 'Szektorbeli kuponok';


// Text
$_['text_account']      = 'Fiók';
$_['text_home']         = 'Webáruház';
$_['column_name']       = 'Termék neve';
$_['column_quantity']   = 'Menny.';
$_['column_total']       = 'Összesen';
$_['button_filter']     = 'Szűrés';
$_['entry_date_start']  = 'Kezdete:';
$_['entry_date_end']    = 'Vége:';
$_['entry_status']     = 'Rendelés állapota';
$_['text_all_status']  = '-- Minden állapot --';
$_['text_no_results']  = 'Nincs találat!';

$_['column_category_name']     = 'Kategória';
$_['column_hirdetok']          = 'Kategóriabeli<br>ajánlatok száma<br>(db)';
$_['column_vasaroltak']        = 'Ajánlatok<br>száma';
$_['column_vasaroltak_nalam']  = 'Kinyomtatott<br>Saját (db)';
$_['column_atlag']             = 'Átlag';
$_['column_atlag_szazalek']    = 'Átlag %';
$_['column_lattak']            = 'Ajánlatok<br />megtekintésének<br />száma';
?>