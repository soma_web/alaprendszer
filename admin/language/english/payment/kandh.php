<?php
// Heading
$_['heading_title']      = 'K&H Debit Payment';

// Text
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified K&H Card payment module!';
$_['text_bank_mode_block']    = 'Blocking';
$_['text_bank_mode_load']    = 'Loading';
$_['text_kandh'] 			= '<img src="view/image/payment/kandh.jpg" alt="K&H Payments" title="K&H Payments" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_order_status'] = 'Order Status:';
$_['entry_status']       = 'Status:';
$_['entry_order_failed_status']       = 'Failed status:';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_test_mode']       = 'Testing mode:';
$_['entry_shop_code']       = 'Shop ID:';

$_['entry_shop_code']       = 'Webshop ID:';
$_['entry_prod_key_file']   = 'Key file:';
$_['entry_payment_language'] = 'Frontend language of bank payment:';
$_['entry_payment_currency'] = 'Payment currency:';
$_['entry_test_userid'] = 'Test user id:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment CIB Card!';
?>