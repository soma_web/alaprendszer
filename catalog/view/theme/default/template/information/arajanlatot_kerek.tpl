<?php
if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/close_upsell.png')) {
    $close = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/close_upsell.png';
} else {
    $close = DIR_TEMPLATE_IMAGE.'default/image/close_upsell.png';
}
?>
    <div id="arajanlatot_kerek_mail">

        <div class="ajanlat_form">
            <img class="close_arajanlat_icon" title="<?php echo $text_close; ?>" alt="<?php echo $text_close;?>"  src="<?php echo $close?>" >

            <h1><?php echo $heading_title; ?></h1>

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="email_adatok">
                    <h2><?php echo $text_location; ?></h2>
                    <b><?php echo $text_address; ?></b><br/>
                    <?php echo $store; ?><br/>
                    <?php echo $ceg_neve; ?><br/>
                    <?php echo $address; ?>

                    <?php if ($telephone) { ?>
                        <br/><b><?php echo $text_telephone; ?></b>
                        <?php echo $telephone; ?>

                    <?php } ?>
                    <?php if ($fax) { ?>
                        <br/><b><?php echo $text_fax; ?></b>
                        <?php echo $fax; ?>
                    <?php } ?>
                </div>
                <div class="valasztott_termek">
                    <h2><?php echo 'Termék adatai:'; ?></h2>
                    <?php if ($product_id) { ?>
                        <div>
                            <span class="cimke"><?php echo $text_product_id?></span>
                            <span class="ertek"><?php echo $product_id?></span>
                        </div>
                    <?php } ?>
                    <?php if ($product_name) { ?>
                        <div>
                            <span class="cimke"><?php echo $text_product_name?></span>
                            <span class="ertek"><?php echo $product_name?></span>
                        </div>
                    <?php } ?>
                    <?php if ($product_model) { ?>
                        <div>
                            <span class="cimke"><?php echo $text_product_model?></span>
                            <span class="ertek"><?php echo $product_model?></span>
                        </div>
                    <?php } ?>
                    <?php if ($product_cikkszam) { ?>
                        <div>
                            <span class="cimke"><?php echo $text_product_cikkszam?></span>
                            <span class="ertek"><?php echo $product_cikkszam?></span>
                        </div>
                    <?php } ?>
                    <?php if ($product_gyarto) { ?>
                        <div>
                            <span class="cimke"><?php echo $text_product_gyarto?></span>
                            <span class="ertek"><?php echo $product_gyarto?></span>
                        </div>
                    <?php } ?>



                </div>

                <br/>
                <h2><?php echo $text_contact; ?></h2>
                <div class="content kapcsolat">

                    <span class="required">*</span> <b><?php echo $entry_name; ?></b><br/>
                        <input type="text" name="name" value="<?php echo $name; ?>"/>
                    <br/>
                    <?php if ($error_name) { ?>
                        <span class="error"><?php echo $error_name; ?></span>
                    <?php } ?>
                    <br/>


                    <?php if ($megjelenit_telefonszam) { ?>
                        <?php if ($megjelenit_telefonszam_kotelezo) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $entry_telefon; ?></b><br/>
                        <input type="text" name="telefon" value="<?php echo $telefon; ?>"/>
                        <br/>
                        <?php if ($error_telefon) { ?>
                            <span class="error"><?php echo $error_telefon; ?></span>
                        <?php } ?>
                        <br/>
                    <?php } ?>

                    <?php if ($megjelenit_iranyitoszam) { ?>
                        <?php if ($megjelenit_iranyitoszam_kotelezo) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $entry_iranyitoszam; ?></b><br/>
                        <input type="text" name="iranyitoszam" value="<?php echo $iranyitoszam; ?>" size="5"/>
                        <br/>
                        <?php if ($error_iranyitoszam) { ?>
                            <span class="error"><?php echo $error_iranyitoszam; ?></span>
                        <?php } ?>
                        <br/>
                    <?php } ?>

                    <?php if ($megjelenit_varos) { ?>
                        <?php if ($megjelenit_varos_kotelezo) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $entry_varos; ?></b><br/>
                        <input type="text" name="varos" value="<?php echo $varos; ?>" />
                        <br/>
                        <?php if ($error_varos) { ?>
                            <span class="error"><?php echo $error_varos; ?></span>
                        <?php } ?>
                        <br/>
                    <?php } ?>

                    <?php if ($megjelenit_utca) { ?>
                        <?php if ($megjelenit_utca_kotelezo) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $entry_utca; ?></b><br/>
                        <input type="text" name="utca" value="<?php echo $utca; ?>" style="width: 100%"/>
                        <br/>
                        <?php if ($error_utca) { ?>
                            <span class="error"><?php echo $error_utca; ?></span>
                        <?php } ?>
                        <br/>
                    <?php } ?>


                    <span class="required">*</span> <b><?php echo $entry_email; ?></b><br/>
                        <input type="text" name="email" value="<?php echo $email; ?>"/>
                    <br/>
                    <?php if ($error_email) { ?>
                        <span class="error"><?php echo $error_email; ?></span>
                    <?php } ?>
                    <br/>


                    <span class="required">*</span> <b><?php echo $entry_enquiry; ?></b><br/>
                        <textarea name="enquiry" cols="40" rows="10" style="width: 99%;"><?php echo $enquiry; ?></textarea>
                    <br/>
                    <?php if ($error_enquiry) { ?>
                        <span class="error"><?php echo $error_enquiry; ?></span>
                    <?php } ?>
                    <br/>


                    <b><?php echo $entry_captcha; ?></b><br/>
                    <input type="text" name="captcha" value="<?php echo $captcha; ?>"/>
                    <br/>
                    <img src="index.php?route=information/contact/captcha" alt=""/>
                    <?php if ($error_captcha) { ?>
                        <span class="error"><?php echo $error_captcha; ?></span>
                    <?php } ?>


                </div>
                <div class="kapcsolat">
                    <?php if ($megjelenit_ceg) { ?>
                        <?php if ($megjelenit_ceg_kotelezo) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $entry_ceg; ?></b><br/>
                        <input type="text" name="ceg" value="<?php echo $ceg; ?>" style="width: 100%"/>
                        <br/>
                        <?php if ($error_ceg) { ?>
                            <span class="error"><?php echo $error_ceg; ?></span>
                        <?php } ?>
                        <br/>
                    <?php } ?>
                    <?php if ($megjelenit_adoszam) { ?>
                        <?php if ($megjelenit_adoszam_kotelezo) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $entry_adoszam; ?></b><br/>
                        <input type="text" name="adoszam" value="<?php echo $adoszam; ?>" style="width: 100%"/>
                        <br/>
                        <?php if ($error_adoszam) { ?>
                            <span class="error"><?php echo $error_adoszam; ?></span>
                        <?php } ?>
                        <br/>
                    <?php } ?>
                </div>
                <div class="buttons">
                    <div class="right">
                        <input type="button" onclick="ajnlatkeresFeldolgoz()" value="<?php echo $button_continue; ?>" class="button"/>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <script>
        <?php if($kilep) { ?>
            $("#arajanlatot_kerek_mail").slideUp('slow',function(){
                $("#arajanlatot_kerek_mail").remove();
                $('.success, .warning, .attention, .information, .error').remove();

                $('#notification').html('<div class="success" style="display: none;">Sikerült, a levelet továbbítottuk<img src="catalog/view/theme/<?php echo $this->config->get('config_template')?>/image/close.png" alt="" class="close" /></div>');
                $('.success').slideDown('slow');
                $('html, body').animate({ scrollTop: 0 }, '1200');
                notaficationBezar();
            });
        <?php } ?>

        function ajnlatkeresFeldolgoz(product_id) {
            $.ajax({
                url: 'index.php?route=information/arajanlatot_kerek&product_id=<?php echo $product_id?>',
                type: 'POST',
                data: $('#arajanlatot_kerek_mail input, #arajanlatot_kerek_mail textarea').serialize(),
                dataType: 'html',
                beforeSend: function() {
                    var html = '';
                    html += '<div id="lepel" style="position: fixed; top: 0; left: 0;  width: 100%; height: 100%; z-index: 10000001; background-color: #ddd; opacity: 0.3;" ></div>';
                    $("#container").before(html);


                },
                success: function(html) {
                    $("#lepel").slideUp('slow',function(){
                        $("#lepel").remove();
                    });

                    if ($("#arajanlatot_kerek_mail").length > 0) {
                        $("#arajanlatot_kerek_mail").remove();
                        $("#container").before(html);
                        $("#arajanlatot_kerek_mail").css("height",$('body').height());
                        $('.ajanlat_form').css('top',$(window).scrollTop());

                    } else {
                        $("#container").before(html);
                        $("#arajanlatot_kerek_mail").css("height",$('body').height());
                        $('.ajanlat_form').css('top',$(window).scrollTop());

                    }

                },
                error: function(e) {
                },
                complete: function() {
                    $("#lepel").remove();
                }

                });
        }

        $('.close_arajanlat_icon').bind("click",function(){
            $("#arajanlatot_kerek_mail").slideUp('slow',function(){
                $("#arajanlatot_kerek_mail").remove();
                $('.success, .warning, .attention, .information, .error').remove();
                $('#lepel').remove();

            });
        });

        $(document).keyup(function(e) {
            if (e.keyCode == 27) {
                if ($('#lepel').length < 1) {
                    ajanlatkeroBezar();
                }
            }
        });

        $(document).bind("click",function(e) {
            if (e.target.id == 'arajanlatot_kerek_mail') {
                if ($('#lepel').length < 1) {
                    ajanlatkeroBezar();
                }
            }

        });

        function ajanlatkeroBezar() {
            $("#arajanlatot_kerek_mail").slideUp('slow',function(){
                $("#arajanlatot_kerek_mail").remove();
                $('.success, .warning, .attention, .information, .error').remove();
                $('#lepel').remove();
            });
        }



    </script>