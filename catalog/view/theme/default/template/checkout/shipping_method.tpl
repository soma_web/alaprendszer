<script>
    var gls_pont = new Array();
</script>

<?php if ($error_warning) { ?>

<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>

    <table class="radio">
        <tr>
            <td colspan=2 style="width: 60%"><?php echo $text_shipping_method; ?></td>
            <td align=right><?php echo $text_brutto; ?></td>
        </tr>

        <?php $display = "none"?>
        <?php $first_bigcanvas = true; ?>
        <?php $title = "";?>
        <?php foreach ($shipping_methods as $shipping_method) { ?>
            <?php if (trim($title) != trim($shipping_method['title'])) { ?>
                <tr>
                    <td class="shipping_method_header" colspan="3"><b class="shipping_method_header" ><?php echo $shipping_method['title']; ?></b></td>
                </tr>
            <?php } ?>
            <?php $title =  $shipping_method['title'];?>

            <?php if (!$shipping_method['error']) { ?>
                <?php foreach ($shipping_method['quote'] as $quote) { ?>
                    <tr class="highlight">
                        <td>
                            <?php //if ($quote['code'] == $code || !$code) { ?>
                            <?php if ($quote['code'] == $code) { ?>
                                <?php  $code = $quote['code']; ?>
                                <?php $display = ($code == "pickup.pickup") ? "block" : "none"; ?>
                                <input
                                       type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
                            <?php } else { ?>
                                <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
                           <?php  } ?>
                        </td>
                        <td><label for="<?php echo $quote['code']; ?>"><?php echo $quote['title']; ?></label></td>
                        <td style="text-align: right;">
                            <?php if(!empty($quote['ado_title'])) { ?>
                                <span style="margin-right: 20px; display: inline-table;"><label for="<?php echo $quote['code']; ?>"><?php echo $quote['netto_text']."+".$quote['ado_title']; ?></label></span>
                            <?php } ?>
                            <span style="display: inline-table;"><label for="<?php echo $quote['code']; ?>"><?php echo $quote['text']; ?></label></span>
                        </td>
                    </tr>
                    <?php $code = explode('.',$quote['code']); ?>
                    <?php $shipping_method_code = $code[0]; ?>
                    <?php $status = $code[0].'_status'; ?>
                    <?php if (isset($quote['gls'][$status]) && $quote['gls'][$status] == 1) { ?>
                        <tr>
                            <td colspan="3">
                                <script>
                                    $( ".checkout-content input[type='radio']:not(.uzlet)" ).change(function() {
                                        if ($(this).attr('value') == "<?php echo $quote['code']; ?>") {

                                            $("#big-canvas").detach().appendTo('#<?php echo $shipping_method_code; ?>');
                                            <?php if (isset($shipping_address_city) && !empty($shipping_address_city)) { ?>
                                                glsMap.initAddress(',<?php echo $shipping_address_city; ?>,HU');
                                                google.maps.event.trigger(glsMap, 'resize');
                                            <?php } else { ?>
                                                glsMap.initAddress(',Budapest,HU');
                                                google.maps.event.trigger(glsMap, 'resize');
                                            <?php } ?>
                                            $('.<?php echo $shipping_method_code; ?>.animal').animate({height: "445px"}, 700);
                                        } else {
                                            $('.<?php echo $shipping_method_code; ?>.animal').animate({height: "0px"}, 700);

                                        }
                                    });
                                </script>
                                <div id="<?php echo $shipping_method_code; ?>" class="<?php echo $shipping_method_code; ?> animal" style="height: 0; width:100%; overflow: hidden;  margin: 0 auto; text-align: center;">
                                    <b style="display:block; padding: 4px"><?php echo $text_gls_csomagpontok; ?></b>
                                    <?php if($first_bigcanvas) { ?>
                                        <div id="big-canvas" class="<?php echo $shipping_method_code; ?>_big-canvas" style="display: block; background: white; margin: 0 auto; text-align: center; width: 96%; height: 400px;"></div>

                                        <script>

                                            var glsMap;

                                            function initGLSPSMap()
                                            {
                                                glsMap = new GLSPSMap();
                                                glsMap.init('HU', 'big-canvas', '1165, Budapest, HU', 0);
                                                google.maps.event.trigger(glsMap, 'resize');
                                            }

                                            //var gls_pont = new Array();
                                            function glsPSMap_OnSelected_Handler(data) {
                                                //alert("Azonosító: "+ data.pclshopid + "; Cím: " + data.zipcode + ", " + data.city + ", " + data.address);
                                                gls_pont = new Array();
                                                gls_pont[0]   =  data.pclshopid.replace(',','');
                                                gls_pont[1]   =  data.zipcode.replace(',','');
                                                gls_pont[2]   =  data.name.replace(',','');
                                                gls_pont[3]   =  data.city.replace(',','');
                                                gls_pont[4]   =  data.address.replace(',','');

                                            }


                                            $(document).ready(function(){
                                                initGLSPSMap();
                                            });
                                        </script>

                                    <?php $first_bigcanvas = false; ?>
                                    <?php } ?>
                                </div>
                            </td>
                        </tr>

                    <?php } ?>
                    <?php if (isset($shipping_method['warehouse']) && $shipping_method['warehouse']) { ?>
                        <tr>
                            <td colspan="3" style="">
                                <div class="warehouse_shipping warehouse_open_closed close_div" style="display: <?php echo $display;?>">
                                    <table style="background: #fff; width: 100%; ">
                                        <?php $class = 'odd'; ?>

                                        <tr>
                                            <td style="width: 50%"></td>
                                            <?php foreach($shipping_method['warehouse'][0]['warehouse'] as $uzlet) { ?>
                                                <td class="raktar_elforgat"><?php echo $uzlet['address'];?></td>
                                            <?php } ?>
                                        </tr>

                                        <?php foreach($shipping_method['warehouse'] as $raktar) { ?>
                                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                            <tr class="<?php echo $class; ?>">
                                                <td><a style="text-decoration: none; cursor: default" title="<?php echo $raktar['termek'];?>"> <?php echo mb_substr($raktar['termek'],0,60,"UTF-8");?></a></td>

                                                <?php foreach($raktar['warehouse'] as $uzlet) { ?>
                                                    <?php if ($uzlet['quantity'] > 0) { ?>
                                                        <td class="raktar_elforgat"><img src="catalog/view/theme/default/image/success.png"></td>
                                                    <?php } else { ?>
                                                        <td class="raktar_elforgat"><img style="width: 11px" src="catalog/view/theme/default/image/remove.png"></td>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px; text-align: right; padding-right: 30px;"><b>Ebben az üzletben veszem át:</b></td>


                                            <?php foreach($raktar['warehouse'] as $uzlet) { ?>
                                                <?php if (isset($melyik_uzlet) && $melyik_uzlet == $uzlet['warehouse_id']) { ?>
                                                    <td class="text_center"><input type="radio" class="uzlet" name="uzlet" value="<?php echo $uzlet['warehouse_id']?>" checked="checked"></td>
                                                <?php } else { ?>
                                                    <td class="text_center"><input type="radio" class="uzlet"  name="uzlet" value="<?php echo $uzlet['warehouse_id']?>"></td>
                                                <?php } ?>
                                            <?php } ?>
                                        </tr>
                                    </table>
                                    <div id="map_canvas" style="display: none">
                                        <div class="cimek">
                                        </div>

                                        <iframe src="" width="600" height="300" frameborder="0" style="display: none;">
                                        </iframe>
                                    </div>
                                </div>

                                <?php
                                //$raktar['warehouse']['0']['map'] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2694.7747379701464!2d19.059229999999992!3d47.51377800000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741dc0b680cfa1f%3A0xa8fd4550e6a898c1!2sWestEnd+City+Center!5e0!3m2!1shu!2shu!4v1422963009820";
                                $raktar['warehouse']['0']['map'] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d674.0528164066393!2d19.070232617192357!3d47.4857975884707!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741dc581fc44a25%3A0x5508162f9f9f39db!2sBudapest%2C+1085!5e0!3m2!1shu!2shu!4v1413894838138";
                                $raktar['warehouse']['1']['map'] = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2697.4369825549534!2d19.0475001!3d47.46191340000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741dda3d403d25f%3A0x23a572d5b909edfb!2sHengermalom+%C3%BAt+14-16%2C+Budapest%2C+1117!5e0!3m2!1shu!2shu!4v1413894889058";
                                $raktar['warehouse']['2']['map'] = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2696.326578666655!2d19.068786599999996!3d47.483551!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741dc57cb98d019%3A0x73c27374aa78edd2!2zRmVyZW5jIGvDtnLDunQgMjYsIEJ1ZGFwZXN0LCAxMDky!5e0!3m2!1shu!2shu!4v1413894934340";
                                ?>
                                <script>
                                    var terkepek = {
                                        <?php foreach($raktar['warehouse'] as $uzlet) { ?>
                                            <?php echo $uzlet['warehouse_id']?>: '<?php echo $uzlet['map']?>',
                                        <?php } ?>
                                    };

                                    var varos = {
                                    <?php foreach($raktar['warehouse'] as $uzlet) { ?>
                                        <?php echo $uzlet['warehouse_id']?>: '<?php echo $uzlet['city']?>',
                                    <?php } ?>
                                    };

                                    var postcode = {
                                    <?php foreach($raktar['warehouse'] as $uzlet) { ?>
                                        <?php echo $uzlet['warehouse_id']?>: '<?php echo $uzlet['postcode']?>',
                                    <?php } ?>
                                    };

                                    var address = {
                                    <?php foreach($raktar['warehouse'] as $uzlet) { ?>
                                        <?php echo $uzlet['warehouse_id']?>: '<?php echo $uzlet['address']?>',
                                    <?php } ?>
                                    };

                                    $(".text_center input[type='radio']").change(function(){
                                        $("map_canvas").slideDown(800);
                                        var warehouseid = $(this).attr("value");
                                        $("#map_canvas").fadeIn('fast', function () {
                                            $("#map_canvas iframe").fadeOut(600).attr("src",terkepek[warehouseid]).fadeIn(600);
                                            $("#map_canvas .cimek").html((varos[warehouseid].length > 0 ? varos[warehouseid]+",<br>" : "")+(postcode[warehouseid].length > 0 ? postcode[warehouseid]+"<br>" : "") +address[warehouseid]+"<br>");

                                        });
                                    });

                                </script>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
             <?php } else { ?>
                 <tr>
                     <td colspan="3"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
                 </tr>
             <?php } ?>
        <?php } ?>
    </table>
    <br />
<?php } ?>
<b><?php echo $text_comments; ?></b>
<textarea name="comment" rows="8" style="width: 100%; box-sizing: border-box; margin: 0;"><?php echo $comment; ?></textarea>
<br />
<br />
<div class="buttons">
  <div class="right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" class="button" />
  </div>
</div>
<script>




    $('input[name=\'uzlet\']:checked').trigger('change');

</script>
<script>

    $( ".checkout-content input[type='radio']:not(.uzlet)" ).change(function() {
        $('.close_div').slideUp(700);
        if ($(this).attr('value') == "pickup.pickup") {
            $('.warehouse_open_closed').slideDown(700);
        }

    });


</script>
