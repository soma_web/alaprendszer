<table class="list">
<thead>
  <tr>
	<td class="left"><?php echo $column_customer; ?></td>
	<td class="left"><?php echo $column_email; ?></td>
	<td class="left"><?php echo $column_sent; ?></td>
	<td class="left"><?php echo $column_action; ?></td>
  </tr>
</thead> 
<tbody>
  <?php if ($alerts) { ?>
  <?php foreach ($alerts as $alert) { ?>
  <tr>
	<td class="left"><?php echo $alert['name']; ?></td>
	<td class="left"><?php echo $alert['email']; ?></td>
	<td class="left"><?php echo $alert['date_added']; ?></td>
	<td class="left"><a onclick="showHistoryEmail(<?php echo $alert['history_id']; ?>);"><img src="view/image/price_alert/view.png" /></a></td>
  </tr> 
  <?php } ?>
  <?php } else { ?>
  <tr>
	<td class="center" colspan="4"><?php echo $text_no_results; ?></td>
  </tr>
  <?php } ?>
</tbody>
</table>

<div class="pagination"><?php echo $pagination; ?></div>