<?php if( (isset($attributes) && $attributes) || $beallitasok['tulajdonsagok']['teljes'] == 1){ ?>

    <?php if ($beallitasok['tulajdonsagok']['teljes'] != 1) { ?>
        <?php foreach ($attributes as $attribute) { ?>
            <div class="box">
                <div class="box-heading reszek masodik-filter"><?php echo $attribute['name']; ?>

                </div>

                    <div class="box-content">
                        <?php $szukit = $attribute['multifilter_szukit'] == 1 ? "szukit" : ($attribute['multifilter_szukit'] == 2 ? "bovit" : ($beallitas['szukit'] == 1 ? "szukit" : "bovit"))?>

                        <ul class="box-filter box-filter-tulajdonsagok <?php echo $szukit?>">
                            <?php foreach ($attribute['attributes'] as $value) { ?>
                                <?php if ($value['multifilter_csuszka'] == 1) { ?>

                                    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/multifilter/attribute_csuszka.tpl') ) {
                                        $csuszka_tpl = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/multifilter/attribute_csuszka.tpl';
                                    } elseif( file_exists(DIR_TEMPLATE . 'default/template/module/multifilter/attribute_csuszka.tpl') ) {
                                        $csuszka_tpl = DIR_TEMPLATE.'default/template/module/multifilter/attribute_csuszka.tpl';
                                    } ?>
                                    <?php if (!empty($csuszka_tpl)) include($csuszka_tpl); ?>


                                <?php } else { ?>
                                    <?php if (!empty($value['name'])) { ?>
                                        <?php $checked = ''; ?>
                                        <?php if (in_array($value['attribute_id'], $filter_tulajdonsagok)) { ?>
                                            <?php $checked = 'checked="checked"'; ?>
                                        <?php } ?>


                                        <li><input class="css-checkbox" type="checkbox" value="<?php echo $value['attribute_id']; ?>" id="filter_attribute<?php echo $value['attribute_id']; ?>"  <?php echo $checked; ?>/>
                                            <label class="css-label mac-style" for="filter_attribute<?php echo $value['attribute_id']; ?>"><?php echo $value['name']; ?></label>
                                        </li>
                                    <?php } ?>
                                <?php } ?>

                            <?php } ?>
                        </ul>
                    </div>

            </div>
        <?php }?>
    <?php } else { ?>
        <?php foreach ($attributes_teljes as $attribute_teljes) { ?>
            <?php $talalt = false;
                if (isset($attributes) && !empty($attributes)) {
                    foreach($attributes as $attribute_key=>$attribute) {
                        if ($attribute['attribute_group_id'] == $attribute_teljes['attribute_group_id'])  {
                            $talalt = true;
                            break;
                        }
                    }
                }
            ?>

            <?php if ($talalt) { ?>
                <div class="box">
                    <div class="box-heading reszek masodik-filter"><?php echo $attribute_teljes['name']; ?>
                        <div class="togglebutton"><img /></div>
                    </div>

                    <div class="box-content">
                        <ul class="box-filter box-filter-tulajdonsagok">
                            <?php foreach ($attribute_teljes['attributes'] as $value) { ?>
                                <?php if ($value['multifilter_csuszka'] == 1) { ?>

                                <?php } else { ?>

                                    <?php $bennevan = false; ?>
                                    <?php foreach($attributes[$attribute_key]['attributes'] as $attribute) {?>
                                        <?php if ($value['attribute_id'] == $attribute['attribute_id']) { ?>
                                                <?php $bennevan = true;?>
                                                <?php break;?>
                                        <?php }?>

                                    <?php } ?>

                                    <?php $szukit = $attribute_teljes['multifilter_szukit'] == 1 ? "szukit" : ($attribute_teljes['multifilter_szukit'] == 2 ? "bovit" : ($beallitas['szukit'] == 1 ? "szukit" : "bovit"))?>
                                    <?php if ($bennevan) { ?>
                                        <?php $checked = ''; ?>
                                        <?php if (in_array($value['attribute_id'], $filter_tulajdonsagok)) { ?>
                                                <?php $checked = 'checked="checked"'; ?>
                                        <?php } ?>

                                        <li><input class="css-checkbox  <?php echo $szukit?>" type="checkbox" value="<?php echo $value['attribute_id']; ?>" id="filter_attribute<?php echo $value['attribute_id']; ?>" <?php echo $checked; ?>/>
                                            <label class="css-label mac-style" for="filter_attribute<?php echo $value['attribute_id']; ?>"><?php echo $value['name']; ?></label>
                                        </li>

                                    <?php } else { ?>
                                        <li><input class="css-checkbox" type="checkbox" value="<?php echo $value['attribute_id']; ?>" id="filter_attribute<?php echo $value['attribute_id']; ?>" disabled="disabled"/>
                                            <label class="css-label mac-style halvany" for="filter_attribute<?php echo $value['attribute_id']; ?>" ><?php echo $value['name']; ?></label>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } else {?>
                <div class="box">
                    <div class="box-heading reszek masodik-filter"><?php echo $attribute_teljes['name']; ?>
                        <div class="togglebutton"><img /></div>
                    </div>

                    <div class="box-content">
                        <ul class="box-filter box-filter-tulajdonsagok">
                            <?php foreach ($attribute_teljes['attributes'] as $value) { ?>


                                    <li><input class="css-checkbox" type="checkbox" value="<?php echo $value['attribute_id']; ?>" id="filter_attribute<?php echo $value['attribute_id']; ?>" disabled="disabled"/>
                                        <label class="css-label mac-style halvany" for="filter_attribute<?php echo $value['attribute_id']; ?>" ><?php echo $value['name']; ?></label>

                                    </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
     <?php } ?>

<?php } ?>