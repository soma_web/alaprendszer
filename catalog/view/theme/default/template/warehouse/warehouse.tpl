<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?>
    <img src="catalog/view/theme/default/image/close.png" alt="" class="close" />
</div>
<?php } ?>

<?php if ($error) { ?>
   <div class="warning"><?php echo $error; ?>
       <img src="catalog/view/theme/default/image/close.png" alt="" class="close" />
   </div>
<?php } ?>

<div class="breadcrumb">
   <?php $elemszam=count($breadcrumbs);
   $i=0;
   foreach ($breadcrumbs as $breadcrumb) { $i++;
       if ($i==$elemszam) { ?>
           <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
       <? } else {
           echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
       <? }
   } ?>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

    <h1><?php echo $heading_title; ?></h1>
    <div class="warehouse">
        <ul>
            <?php foreach($warehouses as $warehouse) { ?>
                <li><?php echo $warehouse['warehouse_name']?> <?php echo $warehouse['address']?></li>
            <?php } ?>
        <ul>
    </div>



  <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?> 