<?php

/**
 * Class DatabaseUpdate
 *
 * @property MySQL              $db
 */

class DatabaseUpdate  {

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->customer = $registry->get('customer');
        $this->session = $registry->get('session');
        $this->db = $registry->get('db');
        $this->tax = $registry->get('tax');
        $this->weight = $registry->get('weight');



        // cron
        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "cron (
            `cron_id`	            int(11) AUTO_INCREMENT,
	        `program`	            varchar(64),
	        `rendszeresseg`         int(1),
	        `idonkent`              int(1),
	        `status`                int(1),
	        input_elements          text,
            PRIMARY KEY (`cron_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "cron_pontosido (
            `cron_pontosido_id`	    int(11) AUTO_INCREMENT,
            `cron_id`	            int(11),
	        `pontosido`	            time,
	        `datum`	                date,
	        `het_napja`	            int(1),
            PRIMARY KEY (`cron_pontosido_id`) )   engine=MyISAM default charset=UTF8");



        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "cron_allapot (
                    `cron_allapot_id`	    int(11) AUTO_INCREMENT,
                    `date_start`	        datetime,
                    `date_end`	            datetime,
                    `cron_id`	            int(11),
                    `futtatas_eredmenyek`	text,
                PRIMARY KEY (`cron_allapot_id`),
                INDEX cron_id_date_end (cron_id,date_end)
            ) engine=MyISAM default charset=UTF8");

        $mezok = $this->tableRows("cron_allapot");
        if (!in_array("futtatas_eredmenyek",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "cron_allapot ADD `futtatas_eredmenyek` text");
        }


        // cron vége



        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "checkout_click_history (
            `checkout_click_history_id`  int(11) AUTO_INCREMENT,
            `firstname`	                varchar(64),
            `lastname`	                varchar(64),
            `email`	                    varchar(128),
            `telefon`	                varchar(32),
            `orszag`	                varchar(32),
            `varos`	                    varchar(32),
            `iranyitoszam`	            varchar(8),
            `utca_hazszam`	            varchar(64),
            `ip`	                    varchar(32),
            `date_added`	            datetime,
            `formertek_id`              int(11),
            `tovabbitva`                int(1),
            PRIMARY KEY (`checkout_click_history_id`) )   engine=MyISAM default charset=UTF8");


        $mezok = $this->tableRows("checkout_click_history");
        if (!in_array("tovabbitva",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "checkout_click_history ADD `tovabbitva` int(1)");
        }


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "checkout_click_history_to (
            `checkout_click_history_to_id`  int(11) AUTO_INCREMENT,
            `checkout_click_history_id`  int(11),
            `oldal_neve`	            varchar(64),
            `oldal_ertek`	            varchar(64),
            `tovabbitva`                int(1),
            PRIMARY KEY (`checkout_click_history_to_id`) )   engine=MyISAM default charset=UTF8");

        $this->tableSimpleIndex("checkout_click_history_to","checkout_click_history_id");

        $mezok = $this->tableRows("order_status");
        if (!in_array("szamla_tipusa",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "order_status ADD `szamla_tipusa` int(1)");
        }



        $mezok = $this->tableRows("checkout_click_history_to");
        if (!in_array("tovabbitva",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "checkout_click_history_to ADD `tovabbitva` int(1)");
        }

        $oszlop=$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."customer_special");
        $oszlop=$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."product_to_special");


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."option_value");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("heading",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "option_value ADD `heading` varchar(128)");
        }

        if (!in_array("azonosito",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "option_value ADD `azonosito` varchar(64)");
        }

        if (!in_array("option_szin_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "option_value ADD `option_szin_id` int(11)");
        }


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "termek_integracio (
            `termek_integracio_id`  int(11) AUTO_INCREMENT,
            `products`	                longtext,
            `connects`	                text,
            `name`	                    varchar(64),
            `status`	                int(1),
            `shipping`	                varchar(64),
            `shipping_time_in_stock`	int(3),
            `shipping_time_not_stock`	int(3),
            `warranty`	                int(3),
            `pieces`	                int(3),
            `description`	            varchar(32),
            `pickpack`	                int(1),
            `shipping_price`	        int(6),
            PRIMARY KEY (`termek_integracio_id`) )   engine=MyISAM default charset=UTF8");

        $mezok = $this->tableRows("termek_integracio");
        if (!in_array("shipping",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "termek_integracio ADD `shipping` varchar(64)");
        }
        if (!in_array("shipping_time_in_stock",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "termek_integracio ADD `shipping_time_in_stock` int(3)");
        }
        if (!in_array("shipping_time_not_stock",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "termek_integracio ADD `shipping_time_not_stock` int(3)");
        }
        if (!in_array("warranty",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "termek_integracio ADD `warranty` int(3)");
        }
        if (!in_array("pieces",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "termek_integracio ADD `pieces` int(3)");
        }
        if (!in_array("description",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "termek_integracio ADD `description` varchar(32)");
        }
        if (!in_array("pickpack",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "termek_integracio ADD `pickpack` int(1)");
        }
        if (!in_array("shipping_price",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "termek_integracio ADD `shipping_price` int(6)");
        }


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."product_option_value");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("option_szin_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD `option_szin_id` int(11)");
        }
        if (!in_array("checked",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD `checked` int(11) default '0'");
        }
        if (!in_array("azonosito",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD `azonosito` varchar(64)");
        }
        $index=$this->db->query("SHOW index from ".DB_PREFIX. "product_option_value");

        $talalt = false;
        foreach($index->rows as $value) {
            if ($value['Key_name'] == 'product_id') {
                $talalt = true;
                break;
            }
        }
        if (!$talalt) {
            $this->db->query("ALTER TABLE " . DB_PREFIX ."product_option_value ADD INDEX (product_id)");
        }



        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."option_description");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("description",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "option_description ADD `description` varchar(128)");
        }

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."option_value_description");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (in_array("heading",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "option_value_description DROP COLUMN `heading`");
        }


        $query = $this->db->query('SHOW TABLES LIKE "'.DB_PREFIX.'postcode"');
        if($query->num_rows == 0) {
            $this->createPostcodeTable();
        }

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."product_description");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("short_description",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_description` ADD `short_description` text");
        }
        if (!in_array("test_description",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_description` ADD `test_description` text");
        }
        if (!in_array("garancia_description",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_description` ADD `garancia_description` text");
        }

        $oszlop=$this->db->query("SHOW FIELDS from ".DB_PREFIX."product_description  where Field ='description'");
        if ($oszlop->row['Type'] == "text") {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_description` MODIFY `description` longtext");

        }

        $oszlop=$this->db->query("SHOW FIELDS from ".DB_PREFIX."setting  where Field ='value'");
        if ($oszlop->row['Type'] == "text") {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "setting` MODIFY `value` longtext");

        }


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."product");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }

        if (!in_array("egyszer_kosarba",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD egyszer_kosarba int(1)");
        }

        if (!in_array("egyedi_szallitas",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD egyedi_szallitas int(1)");
        }

        if (!in_array("packaging",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD packaging decimal(15,8)");
        }

        if (!in_array("packaging_class_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD packaging_class_id int(11)");
        }

        if (!in_array("szin_meret_szukseges",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD szin_meret_szukseges int(1)");
        }

        if (!in_array("checked",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD checked int(1) default '0'");
        }

        if (!in_array("stock_in_date",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD stock_in_date date");
        }

        if (!in_array("garancia_ertek",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD garancia_ertek int(6)");
        }
        if (!in_array("garancia_egyseg",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD garancia_egyseg int(2)");
        }

        if (!in_array("egyedi_szallitasi_dij",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD egyedi_szallitasi_dij decimal(15,4)");
        }

        if (!in_array("date_ervenyes_ig",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD date_ervenyes_ig date");
        }
        if (!in_array("cikkszam",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD cikkszam varchar(15)");
        }
        if (!in_array("cikkszam2",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD cikkszam2 varchar(15)");
        }
        if (!in_array("megyseg",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD megyseg varchar(15)");
        }
        if (!in_array("csomagolasi_egyseg",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD csomagolasi_egyseg varchar(15)");
        }
        if (!in_array("csomagolasi_mennyiseg",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD csomagolasi_mennyiseg float");
        }
        if (!in_array("darabaru_meret",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD darabaru_meret text");
        }
        if (!in_array("szazalek",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD szazalek int(1)");
        }
        if (!in_array("eredeti_ar",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD eredeti_ar decimal(15,4)");
        }

        if (!in_array("max_szazalek_kedvezmeny",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD max_szazalek_kedvezmeny decimal(6,2)");
        }

        if (!in_array("utalvany",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD utalvany int(1)");
        }
        if (!in_array("letoltheto",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD letoltheto varchar(255)");
        }
        if (!in_array("video",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD video text");
        }
        if (!in_array("lizing",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD lizing boolean DEFAULT FALSE");
        }
        if (!in_array("kifuto",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD kifuto int(1)");
        }
        if (!in_array("ujdonsag",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD ujdonsag int(1)");
        }
        if (!in_array("elorendeles",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD elorendeles int(1)");
        }
        if (!in_array("megrendelem",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD megrendelem int(1)");
        }
        if (!in_array("ar_tiltasa",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD ar_tiltasa int(1)");
        }

        if (!in_array("imagedesabled",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD imagedesabled boolean DEFAULT false");
        }

        if (!in_array("securitycode",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD securitycode varchar(255)");
        }

        if (!in_array("correctsecuritycode",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD correctsecuritycode BOOLEAN DEFAULT FALSE");
        }
        if (!in_array("crm_tipus",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD crm_tipus int(1)");
        }
        if (!in_array("crm_category_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD crm_category_id int(11)");
        }
        if (!in_array("crm_product_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD crm_product_id int(11)");
        }

        $oszlop=$this->db->query("SHOW FIELDS from ".DB_PREFIX."product where Field ='minimum'");
        if ($oszlop->row['Type'] == "int(11)") {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product` MODIFY `minimum` decimal(13,2)");

        }

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."address");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("adoszam",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "address ADD adoszam varchar(30)");
        }

        if (!in_array("adoszam",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "affiliate ADD adoszam varchar(30)");
        }

        if (!in_array("vallalkozasi_forma",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "address ADD vallalkozasi_forma varchar(100)");
        }
        if (!in_array("szekhely",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "address ADD szekhely varchar(100)");
        }

        if (!in_array("ugyvezeto_neve",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "address ADD ugyvezeto_neve varchar(100)");
        }

        if (!in_array("ugyvezeto_telefonszama",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "address ADD ugyvezeto_telefonszama varchar(100)");
        }

        $this->tableSimpleIndex("product","product_id","AUTO_INCREMENT");
        $this->tableSimpleIndex("category","category_id","AUTO_INCREMENT");
        $this->tableSimpleIndex("manufacturer","manufacturer_id","AUTO_INCREMENT");
        $this->tableSimpleIndex("product_to_category","product_id,category_id","PRIMARY KEY");
        $this->tableSimpleIndex("product_to_category","category_id");
        $this->tableSimpleIndex("setting","setting_id","AUTO_INCREMENT");


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."manufacturer");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }

        if (!in_array("integracios_kod",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "manufacturer ADD integracios_kod int(11)");
        }



        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."store");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }

        if (!in_array("start_url",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "store ADD start_url varchar(255)");
        }






        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."category");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("piacter",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "category` ADD `piacter` int(11)");
        }
        if (!in_array("kategoria_kod",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "category ADD kategoria_kod text");
        }
        if (!in_array("integracios_kod",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "category ADD integracios_kod varchar(11)");
        }


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "option_szin (
            `option_szin_id`	    int(11) AUTO_INCREMENT,
	        `szinkod`	            varchar(128),
	        `azonosito`             varchar(64),
	        `sort_order`            int(11),
            PRIMARY KEY (`option_szin_id`) )   engine=MyISAM default charset=UTF8");

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."option_szin");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("sort_order",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "option_szin` ADD `sort_order` int(11)");
        }


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "option_szin_description (
            `option_szin_id`	    int(11),
	        `language_id`	        int(11),
	        `name`                  varchar(128),
            PRIMARY KEY (`option_szin_id`,`language_id`) )   engine=MyISAM default charset=UTF8");


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "packaging_class_description (
            `packaging_class_id`	    int(11) auto_increment,
	        `language_id`	            int(11),
	        `title`                     varchar(32),
	        `unit`                      varchar(4),
            PRIMARY KEY (`packaging_class_id`,`language_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "packaging_class (
            `packaging_class_id`	    int(11) auto_increment,
	        `value`                     decimal(15,8),
            PRIMARY KEY (`packaging_class_id`) )   engine=MyISAM default charset=UTF8");




        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "option_szin_group (
            `option_szin_group_id`	    int(11) AUTO_INCREMENT,
	        `szinkod`	                varchar(128),
	        `sort_order`                int(11),
            PRIMARY KEY (`option_szin_group_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "option_szin_group_description (
            `option_szin_group_id`	    int(11),
            `language_id`	            int(11),
	        `name`                  varchar(128),
            PRIMARY KEY (`option_szin_group_id`,`language_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "option_szin_to_group (
            `option_szin_group_id`	    int(11),
	        `option_szin_id`	        int(11)
             )   engine=MyISAM default charset=UTF8");



        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "arajanlat (
            `arajanlat_id`	    int(11) AUTO_INCREMENT,
	        `name`	            varchar(128),
	        `customer_id`	    int(11),
	        `date_added`	    datetime,
	        `date_modified`	    datetime,
	        `total`	            decimal(15,4),
            szallito_neve              varchar(128),
            szallito_varos             varchar(128),
            szallito_utca              varchar(128),
            szallito_orszag            varchar(128),
            szallito_adoszam           varchar(128),
            szallito_bank              varchar(128),
            szallito_bankszamla_szam   varchar(128),
            szallito_bankszamla_swift  varchar(128),
            szallito_bankszamla_iban   varchar(128),
            vevo_ceg                   varchar(128),
            vevo_neve                  varchar(128),
            vevo_varos                 varchar(128),
            vevo_utca                  varchar(128),
            vevo_orszag                varchar(128),
            vevo_adoszam               varchar(128),
            vevo_bank                  varchar(128),
            vevo_bankszamla_szam       varchar(128),
            ajanlat_kelte              date,
            ajanlat_szallitasi_hatarido varchar(128),
            ajanlat_fizetesi_hatarido  varchar(128),
            ajanlat_fizetesi_mod       varchar(128),
            ajanlat_kuldo_neve         varchar(128),
            ajanlat_kuldo_telefon      varchar(128),
            ajanlat_kuldo_email        varchar(128),
            ajanlat_megszolitas        varchar(128),
            ajanlat_altalanos_szoveg_felul  varchar(128),
            ajanlat_altalanos_szoveg_alul  text,

             PRIMARY KEY (`arajanlat_id`) )   engine=MyISAM default charset=UTF8");

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."arajanlat");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("date_added",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `date_added` datetime");
        }
        if (!in_array("date_modified",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `date_modified` datetime");
        }
        if (!in_array("customer_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `customer_id` int(11)");
        }
        if (!in_array("total",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `total` decimal(15,4)");
        }

        if (!in_array("szallito_neve",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `szallito_neve` varchar(128)");
        }
        if (!in_array("szallito_varos",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `szallito_varos` varchar(128)");
        }
        if (!in_array("szallito_utca",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `szallito_utca` varchar(128)");
        }
        if (!in_array("szallito_orszag",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `szallito_orszag` varchar(128)");
        }
        if (!in_array("szallito_adoszam",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `szallito_adoszam` varchar(128)");
        }
        if (!in_array("szallito_bank",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `szallito_bank` varchar(128)");
        }
        if (!in_array("szallito_bankszamla_szam",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `szallito_bankszamla_szam` varchar(128)");
        }
        if (!in_array("szallito_bankszamla_swift",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `szallito_bankszamla_swift` varchar(128)");
        }
        if (!in_array("szallito_bankszamla_iban",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `szallito_bankszamla_iban` varchar(128)");
        }
        if (!in_array("vevo_ceg",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `vevo_ceg` varchar(128)");
        }
        if (!in_array("vevo_neve",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `vevo_neve` varchar(128)");
        }
        if (!in_array("vevo_varos",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `vevo_varos` varchar(128)");
        }
        if (!in_array("vevo_utca",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `vevo_utca` varchar(128)");
        }
        if (!in_array("vevo_orszag",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `vevo_orszag` varchar(128)");
        }
        if (!in_array("vevo_adoszam",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `vevo_adoszam` varchar(128)");
        }
        if (!in_array("vevo_bank",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `vevo_bank` varchar(128)");
        }
        if (!in_array("vevo_bankszamla_szam",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `vevo_bankszamla_szam` varchar(128)");
        }
        if (!in_array("ajanlat_kelte",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_kelte` date");
        }
        if (!in_array("ajanlat_szallitasi_hatarido",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_szallitasi_hatarido` varchar(128)");
        }
        if (!in_array("ajanlat_fizetesi_hatarido",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_fizetesi_hatarido` varchar(128)");
        }
        if (!in_array("ajanlat_fizetesi_mod",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_fizetesi_mod` varchar(128)");
        }
        if (!in_array("ajanlat_kuldo_neve",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_kuldo_neve` varchar(128)");
        }
        if (!in_array("ajanlat_kuldo_telefon",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_kuldo_telefon` varchar(128)");
        }
        if (!in_array("ajanlat_kuldo_email",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_kuldo_email` varchar(128)");
        }
        if (!in_array("ajanlat_megszolitas",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_megszolitas` varchar(128)");
        }
        if (!in_array("ajanlat_altalanos_szoveg_felul",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_altalanos_szoveg_felul` varchar(128)");
        }
        if (!in_array("ajanlat_altalanos_szoveg_alul",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat` ADD `ajanlat_altalanos_szoveg_alul` text");
        }





        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "arajanlat_to_store (
            `arajanlat_id`	    int(11),
	        `store_id`	        int(11),
             PRIMARY KEY (`arajanlat_id`,`store_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "arajanlat_product (
            `arajanlat_product_id`	 int(11) AUTO_INCREMENT,
	        `arajanlat_id`	         int(11),
	        `product_id`	         int(11),
	        `szazalek`	             decimal(5,1),
	        `egysegar`	             decimal(15,4),
	        `mennyiseg`	             int(11),
	        `ertek`	                 decimal(15,4),
	        `ertek_osszesen`	     decimal(15,4),
	        `product_ertek_osszesen` decimal(5,1),
             PRIMARY KEY (`arajanlat_product_id`) )   engine=MyISAM default charset=UTF8");

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."arajanlat_product");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("egysegar",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_product` ADD `egysegar` decimal(15,4)");
        }
        if (!in_array("ertek",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_product` ADD `ertek` decimal(15,4)");
        }
        if (!in_array("ertek_osszesen",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_product` ADD `ertek_osszesen` decimal(15,4)");
        }
        if (!in_array("szazalek",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_product` ADD `szazalek` decimal(5,1)");
        }
        if (!in_array("product_ertek_osszesen",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_product` ADD `product_ertek_osszesen` decimal(15,1)");
        }



        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "arajanlat_option (
            `arajanlat_option_id`	int(11) AUTO_INCREMENT,
            `arajanlat_product_id`	int(11),
            `arajanlat_id`	        int(11),
            `product_option_id`	    int(11),
	        `option_id`	            int(11),
             PRIMARY KEY (`arajanlat_option_id`) )   engine=MyISAM default charset=UTF8");


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."arajanlat_option");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("product_option_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option` ADD `product_option_id` int(11)");
        }
        if (!in_array("arajanlat_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option` ADD `arajanlat_id` int(11)");
        }
        if (!in_array("arajanlat_product_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option` ADD `arajanlat_product_id` int(11)");
        }




        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "arajanlat_option_value (
            `arajanlat_option_value_id`	int(11) AUTO_INCREMENT,
            `arajanlat_option_id`	    int(11),
	        `option_value_id`	        int(11),
            `arajanlat_id`	            int(11),
            `arajanlat_product_id`	    int(11),
	        `mennyiseg`	                int(11),
	        `product_option_value_id`	int(11),
	        `egysegar`	                decimal(15,4),
	        `ertek`	                    decimal(15,4),
	        `szazalek`	                decimal(5,1),
	        `product_option_value_ertek` varchar(128),
             PRIMARY KEY (`arajanlat_option_value_id`) )   engine=MyISAM default charset=UTF8");

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."arajanlat_option_value");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("product_option_value_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option_value` ADD `product_option_value_id` int(11)");
        }
        if (!in_array("egysegar",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option_value` ADD `egysegar` decimal(15,4)");
        }
        if (!in_array("ertek",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option_value` ADD `ertek` decimal(15,4)");
        }
        if (!in_array("szazalek",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option_value` ADD `szazalek` decimal(5,1)");
        }
        if (!in_array("product_option_value_ertek",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option_value` ADD `product_option_value_ertek` varchar(128)");
        }
        if (!in_array("arajanlat_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option_value` ADD `arajanlat_id` int(11)");
        }
        if (!in_array("arajanlat_product_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "arajanlat_option_value` ADD `arajanlat_product_id` int(11)");
        }





        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "pre_order (
            `pre_order_id`	            int(11) AUTO_INCREMENT,
	        `product_id`	            int(11),
            `model`           	        varchar(64),
            `cikkszam`           	    varchar(15),
            `product_name`              varchar(255),
            `gyarto`           	        varchar(64),
            `megrendelo_name`           varchar(64),
            `megrendelo_email`          varchar(96),
            `megrendelo_telefon`        varchar(32),

            `megrendelo_iranyitoszam`   varchar(8),
            `megrendelo_varos`          varchar(32),
            `megrendelo_utca`           varchar(64),
            `megrendelo_ceg`            varchar(64),
            `megrendelo_adoszam`        varchar(32),

            `megrendelo_uzenet`         text,
            `sajat_megjegyzes`          text,
            `date_added`                date,
            `date_modified`             date,
            PRIMARY KEY (`pre_order_id`) )   engine=MyISAM default charset=UTF8");

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."pre_order");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("date_added",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "pre_order` ADD `date_added` date");
        }
        if (!in_array("date_modified",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "pre_order` ADD `date_modified` date");
        }
        if (!in_array("sajat_megjegyzes",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "pre_order` ADD `sajat_megjegyzes` text");
        }
        if (!in_array("megrendelo_telefon",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "pre_order` ADD `megrendelo_telefon` varchar(32)");
        }
        if (!in_array("megrendelo_iranyitoszam",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "pre_order` ADD `megrendelo_iranyitoszam` varchar(8)");
        }
        if (!in_array("megrendelo_varos",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "pre_order` ADD `megrendelo_varos` varchar(32)");
        }
        if (!in_array("megrendelo_utca",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "pre_order` ADD `megrendelo_utca` varchar(64)");
        }
        if (!in_array("megrendelo_ceg",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "pre_order` ADD `megrendelo_ceg` varchar(64)");
        }
        if (!in_array("megrendelo_adoszam",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "pre_order` ADD `megrendelo_adoszam` varchar(32)");
        }

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_pdf (
            `pdf_id`	            int(11) AUTO_INCREMENT,
	        `sort_order`	        int(3),
            `path`           	    varchar(128),
            `pdf_title`           	varchar(128),
            `product_id`           	int(11),
            PRIMARY KEY (`pdf_id`) )   engine=MyISAM default charset=UTF8");


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."product_pdf");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("pdf_title",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_pdf` ADD `pdf_title` varchar(128)");
        }

        $this->db->query("
            CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "qa (
                qa_id INT(11) NOT NULL AUTO_INCREMENT,
                product_id INT(11) NOT NULL,
                customer_id INT(11),
                language_id INT(11) NOT NULL DEFAULT '" . (int)$this->config->get('config_language_id') . "',
                store_id INT(11) NOT NULL DEFAULT '0',
                question_author_name VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
                question_author_email VARCHAR(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
                question_author_phone VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
                question_author_custom VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
                answer_author_name VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
                question TEXT COLLATE utf8_unicode_ci NOT NULL,
                answer TEXT COLLATE utf8_unicode_ci NOT NULL,
                status TINYINT(1) NOT NULL DEFAULT '0',
                notified TINYINT(1) NOT NULL DEFAULT '0',
                date_asked DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
                date_answered DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
                date_modified DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
                PRIMARY KEY (qa_id),
                INDEX fk_qa_product (product_id),
                INDEX fk_qa_language (language_id)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
        );

        $this->db->query("
            CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "qa_to_store (
                qa_id INT(11) NOT NULL,
                store_id INT(11) NOT NULL,
                PRIMARY KEY (qa_id, store_id),
                INDEX fk_qa2s_store (store_id)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
        );


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "fogalom_magyarazat (
            `fogalom_magyarazat_id`	    int(11) AUTO_INCREMENT,
            `status`           	        int(1),
            PRIMARY KEY (`fogalom_magyarazat_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "fogalom_magyarazat_description (
            `fogalom_magyarazat_id`	    int(11),
	        `language_id`	            int(11),
            `name`           	        varchar(255),
            `description`           	longtext )   engine=MyISAM default charset=UTF8");


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_customer_utalvany (
            `product_id`           	int(11),
	        `customer_id`	        int(11) )   engine=MyISAM default charset=UTF8");


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "filter (
            `filter_id`	        int(11)	AUTO_INCREMENT,
	        `filter_group_id`	int(11),
	        `sort_order`	    int(3),
	        `filter_select_id`	int(11),
            PRIMARY KEY (`filter_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "remembered_logins (
            `id`	        int(11)	AUTO_INCREMENT,
	        `customer_id`	int(11),
	        `token`	        varchar(100),
            PRIMARY KEY (`id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "filter_description (
            `filter_id`     	int(11),
	        `language_id`	    int(11),
	        `filter_group_id`	int(11),
	        `name`	            varchar(64),
            PRIMARY KEY (`filter_id`, `language_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "filter_group (
            `filter_group_id`	    int(11)		AUTO_INCREMENT,
	        `sort_order`	        int(3),
	        `filter_select_id`	int(11),
            PRIMARY KEY (`filter_group_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "filter_group_description (
            `filter_group_id`	    int(11),
        	`language_id`	        int(11),
	        `name`	                varchar(64),
	        filter_select_id	int(11),
            PRIMARY KEY (`filter_group_id`, `language_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "filter_select (
            `filter_select_id`	        int(11),
	        `filter_group_select_id`	int(11),
	        `language_id`	            int(2),
	        `name`	                    varchar(255),
	        `sort_order`	            int(3),
            PRIMARY KEY (`filter_select_id`, `language_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "category_filter (
            `category_id`           	int(11),
	        `filter_id`	                int(11) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "category_path (
            `category_id`	            int(11),
	        `path_id`	                int(11),
	        `level`	                    int(11),
             PRIMARY KEY (`category_id`, `path_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_filter (
            `product_id`	            int(11),
	        `filter_id`	                int(11),
	        `filter_group_id`	        int(11),
	        `filter_select_id`	        int(11),
                 PRIMARY KEY (`product_id`, `filter_id`) )   engine=MyISAM default charset=UTF8");


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_customer (
            `id`                        int(11) NOT NULL AUTO_INCREMENT,
            `customer_id`               int(11) NOT NULL,
            `product_id`                int(11) NOT NULL,
                  PRIMARY KEY (`id`)  ) engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "profile (
            `profile_id` int(11) NOT NULL AUTO_INCREMENT,
            `sort_order` int(11) NOT NULL,
            `status` tinyint(4) NOT NULL,
            `price` decimal(10,4) NOT NULL,
            `frequency` enum('day','week','semi_month','month','year') NOT NULL,
            `duration` int(10) unsigned NOT NULL,
            `cycle` int(10) unsigned NOT NULL,
            `trial_status` tinyint(4) NOT NULL,
            `trial_price` decimal(10,4) NOT NULL,
            `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
            `trial_duration` int(10) unsigned NOT NULL,
            `trial_cycle` int(10) unsigned NOT NULL,
                PRIMARY KEY (`profile_id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ") ;

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "profile_description (
            `profile_id` int(11) NOT NULL,
            `language_id` int(11) NOT NULL,
            `name` varchar(255) NOT NULL,
              PRIMARY KEY (`profile_id`,`language_id`) )  ENGINE=MyISAM DEFAULT CHARSET=utf8") ;


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_customer_special (
        	`product_customer_special_id`	int(11) NOT NULL AUTO_INCREMENT,
        	`product_id`	        int(11),
        	`customer_id`	        int(11)	,
	        `quantity`	        int(4)	,
	        `price`	            decimal(15,4)	,
	        `date_start`	        date	,
	        `date_end`            date,
                  PRIMARY KEY (`product_customer_special_id`)  ) engine=MyISAM default charset=UTF8");


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."product_customer_special");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("product_customer_special_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product_customer_special ADD `product_customer_special_id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY");


        }


        $this->db->query("CREATE TABLE if not exists  vatera (

            `vazonosito`	        int(9),
            `vat_automatikus`	    int(1),
            `termeknev`	            varchar(50),
            `alcim`	                text,
            `darabszam`	            int(4),
            `kikialtasiar`	        int(9),
            `minimalar`	            int(9),
            `villamar`	            int(9),
            `licitlepcso`	        int(4),
            `aukcio_inditasa`	    date,
            `aukcio_idotartam`	    int(4),
            `vatera_kategoria_kod`	int(11),
            `megye`	                int(2),
            `orszag`	            int(3),
            `sablon_azonosito`	    int(7),
            `ujrainditasok_szama`	int(3),
            `kiemelesek`	        int(11),
            `termek_jellemzok`	    int(9),
            `garancia`	            int(2),
            `elfogadas_szazalek`	int(2),
            `kepek1`	            text,
            `kepek2`	            text,
            `kepek3`	            text,
            `kepek4`	            text,
            `kinezeti_sablon`	    int(11),
            `product_id`	        int(11),
            `kategoria_specifikus`	text,
            `kepek5`	            text,
            `kepek6`	            text,
            `kepek7`	            text,
            `kepek8`	            text,
            `leiras`	            text,
                  PRIMARY KEY (`product_id`)  ) engine=MyISAM default charset=UTF8");

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."customer");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }

        if (!in_array("partner_kod",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `partner_kod` int(11)");
        }


        if (!in_array("Paypal_email",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `Paypal_email` VARCHAR(256) NOT NULL AFTER `email`");
        }
        if (!in_array("feltolto",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `feltolto` int(1)");
        }
        if (!in_array("date_last_action",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `date_last_action` datetime");
        }
        if (!in_array("number_reminder_sent",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `number_reminder_sent` int(11)");
        }
        if (!in_array("number_reward_sent",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `number_reward_sent` int(11)");
        }
        if (!in_array("eletkor",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `eletkor` varchar(30)");
        }
        if (!in_array("nem",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `nem` int(1)");
        }
        if (!in_array("iskolai_vegzettseg",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `iskolai_vegzettseg` varchar(30)");
        }
        if (!in_array("vallalkozasi_forma",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `vallalkozasi_forma` varchar(100)");
        }
        if (!in_array("szekhely",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `szekhely` varchar(100)");
        }
        if (!in_array("ugyvezeto_neve",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `ugyvezeto_neve` varchar(100)");
        }
        if (!in_array("ugyvezeto_telefonszama",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `ugyvezeto_telefonszama` varchar(30)");
        }
        if (!in_array("company",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `company` varchar(30)");
        }
        if (!in_array("integracios_kod",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `integracios_kod` varchar(30)");
        }
        if (!in_array("weblap",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD `weblap` varchar(100)");
        }


        $mezok = $this->tableRows("order");
        if ( !in_array("tranzakcio_azonosito",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `tranzakcio_azonosito` varchar(128)");
        }
        if ( !in_array("eletkor",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `eletkor` varchar(30)");
        }
        if ( !in_array("nem",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `nem` int(1)");
        }

        if ( !in_array("iskolai_vegzettseg",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `iskolai_vegzettseg` varchar(30)");
        }

        if ( !in_array("vallalkozasi_forma",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `vallalkozasi_forma` varchar(100)");
        }
        if ( !in_array("szekhely",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `szekhely` varchar(100)");
        }
        if ( !in_array("ugyvezeto_neve",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `ugyvezeto_neve` varchar(100)");
        }
        if ( !in_array("ugyvezeto_telefonszama",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `ugyvezeto_telefonszama` varchar(100)");
        }

        if ( !in_array("shipping_vallalkozasi_forma",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_vallalkozasi_forma` varchar(100)");
        }
        if ( !in_array("shipping_szekhely",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_szekhely` varchar(100)");
        }
        if ( !in_array("shipping_ugyvezeto_neve",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_ugyvezeto_neve` varchar(100)");
        }
        if ( !in_array("shipping_adoszam",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_adoszam` varchar(30)");
        }
        if ( !in_array("payment_adoszam",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `payment_adoszam` varchar(30)");
        }
        if ( !in_array("shipping_ugyvezeto_telefonszama",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_ugyvezeto_telefonszama` varchar(100)");
        }

        if ( !in_array("payment_vallalkozasi_forma",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `payment_vallalkozasi_forma` varchar(100)");
        }
        if ( !in_array("payment_szekhely",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `payment_szekhely` varchar(100)");
        }
        if ( !in_array("payment_ugyvezeto_neve",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `payment_ugyvezeto_neve` varchar(100)");
        }
        if ( !in_array("payment_ugyvezeto_telefonszama",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `payment_ugyvezeto_telefonszama` varchar(100)");
        }
        if ( !in_array("shipping_warehouse_name",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_warehouse_name` varchar(128)");
        }
        if ( !in_array("shipping_warehouse_email",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_warehouse_email` varchar(96)");
        }
        if ( !in_array("shipping_warehouse_id",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_warehouse_id` int(11)");
        }
        if ( !in_array("shipping_gls_pont_name",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_gls_pont_name` varchar(128)");
        }
        if ( !in_array("shipping_gls_pont_shopid",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_gls_pont_shopid` varchar(40)");
        }
        if ( !in_array("shipping_gls_pont_address",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_gls_pont_address` varchar(128)");
        }
        if ( !in_array("shipping_gls_pont_city",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_gls_pont_city` varchar(128)");
        }
        if ( !in_array("shipping_gls_pont_zipcode",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `shipping_gls_pont_zipcode` varchar(20)");
        }
        if ( !in_array("to_crm",$mezok) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `to_crm` int(1)");
        }
        if (!in_array("weblap",$mezok)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `weblap` varchar(100)");
        }

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_vonalkod (
            `product_vonalkod_id`	    int(11)	AUTO_INCREMENT,
            `order_product_id`          int(11),
			`order_id`                  int(11),
			`product_id`                int(11),
			`status`                    int(1),
			`vonalkod`                  varchar(13),
			`date_added`                date,
			`letoltve`                  int(1),
			`felhasznalva`              int(1),
            PRIMARY KEY (`product_vonalkod_id`) )   engine=MyISAM default charset=UTF8");

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_vonalkod` LIMIT 0,1");
        if ( $query->num_rows > 0 && !array_key_exists("felhasznalva",$query->row) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_vonalkod` ADD `felhasznalva` int(1)");
        }

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_product` LIMIT 0,1");
        if ( $query->num_rows > 0 && !array_key_exists("ingyenes",$query->row) ) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order_product` ADD `ingyenes` int(1)");
        }


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "no_module (
        	`module_name`	            varchar(200),
	        `description`	            text )  engine=MyISAM default charset=UTF8");

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path");
        if ($query->num_rows == 0) {
            $this->categoryPath();
        }

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_elhelyezes (
            `elhelyezes_id`	                        int(11)	AUTO_INCREMENT,
	        `product_id`	                        int(11),
	        `elhelyezkedes_id`	                    int(3),
	        `elhelyezkedes_alcsoport_id`	        int(3),
	        `idoszak`                   	        int(3),
	        `idoszak_id`                            int(1),
	        `priority`                              int(3),
	        `kiemelesek_id`                         int(3),
	        `datum_tol`                 	        date,
	        `datum_ig`          	                date,
	        `penzugyi_status_id`                    int(1),
	        `fizetes_elbiralas_status_id`           int(1),
	        `sor`                                   int(11),

            `elhelyezkedes_neve`                    varchar(50),
            `elhelyezkedes_netto_ara`               decimal(10,4),
            `elhelyezkedes_brutto_ara`              decimal(10,4),

            `elhelyezkedes_alcsoport_neve`          varchar(50),
            `elhelyezkedes_alcsoport_netto_ara`     decimal(10,4),
            `elhelyezkedes_alcsoport_brutto_ara`    decimal(10,4),
            `kell_alcsoport`                        int(1),
            `elhelyezkedes_alcsoport_sort_order`    int(3),
            `elhelyezkedes_sort_order`              int(3),

            `kiemelesek_neve`                       varchar(50),
            `kiemelesek_netto_ara`                  decimal(10,4),
            `kiemelesek_brutto_ara`                 decimal(10,4),

            `netto_ar`                              decimal(10,4),
            `brutto_ar`                             decimal(10,4),
            `total_netto_ar`                        decimal(10,4),
            `total_brutto_ar`                       decimal(10,4),
            PRIMARY KEY (`elhelyezes_id`) )   engine=MyISAM default charset=UTF8");

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."product_elhelyezes");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "customer_newsletter_categories (
            `newsletter_category_id`        int(11)	AUTO_INCREMENT,
	        `customer_id`	                int(11),
	        `category_id`	                int(11),
            PRIMARY KEY (`newsletter_category_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE IF NOT EXISTS ".DB_PREFIX."product_imagebylocations (
            `imagebylocations_id`           INT(11) AUTO_INCREMENT,
            `product_id`                    INT(11) NOT NULL,
            `image`                         TEXT,
            `sort_order`                    INT(11),
            `generated_text`                BOOLEAN DEFAULT TRUE,
            `location_name`                 TEXT DEFAULT '',
            PRIMARY KEY(`imagebylocations_id`)) engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE IF NOT EXISTS ".DB_PREFIX."product_imageinlocations (
            `imageinlocations_id`           INT(11) AUTO_INCREMENT,
            `imagebylocations_id`           INT(11) NOT NULL,
            `filter_select_id`              INT(11) NOT NULL,
        PRIMARY KEY(`imageinlocations_id`)) engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE IF NOT EXISTS ".DB_PREFIX."product_video (
            `video_id`           INT(11) AUTO_INCREMENT,
            `product_id`                    INT(11) NOT NULL,
            `video_url`                         TEXT,
            `sort_order`                    INT(11),
            `video_title`                VARCHAR (200),
            PRIMARY KEY(`video_id`)) engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "category_view (
            `category_view_id`        int(11)	AUTO_INCREMENT,
	        `datum`	                  date,
	        `counter`	              int(11),
	        `category_id`	          int(11),
            PRIMARY KEY (`category_view_id`) )   engine=MyISAM default charset=UTF8");

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."category_view");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("category_id",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "category_view` ADD `category_id` int(11)");
        }

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."currency");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("sort_order",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "currency` ADD `sort_order` int(3)");
        }

        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."product_special");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("kiemelt",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_special` ADD `kiemelt` int(1)");
        }
        $this->tableSimpleIndex("product_special","product_id");


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."order_total");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("ado",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order_total` ADD `ado` decimal(15,4)");
        }


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."category");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("image_icon",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "category` ADD `image_icon` varchar(255)");
        }


        $oszlop=$this->db->query("SHOW columns from ".DB_PREFIX."user_group");
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        if (!in_array("url",$product_elhelyezes)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "user_group` ADD `url` text");
        }

        $query = $this->db->query('SHOW COLUMNS FROM '.DB_PREFIX."information");
        $informationFields = array();
        foreach($query->rows as $field) {
            $informationFields[] = $field['Field'];
        }
        if(!in_array('toheader', $informationFields)) {
            $this->db->query('ALTER TABLE `'.DB_PREFIX.'information` ADD `toheader` BOOLEAN DEFAULT FALSE;');
        }
        if(!in_array('tofooter', $informationFields)) {
            $this->db->query('ALTER TABLE `'.DB_PREFIX.'information` ADD `tofooter` BOOLEAN DEFAULT FALSE;');
        }


        $query = $this->db->query('SHOW COLUMNS FROM '.DB_PREFIX."attribute");
        $informationFields = array();
        foreach($query->rows as $field) {
            $informationFields[] = $field['Field'];
        }
        if(!in_array('kiemelt', $informationFields)) {
            $this->db->query('ALTER TABLE `'.DB_PREFIX.'attribute` ADD `kiemelt` BOOLEAN DEFAULT FALSE;');
        }
        if(!in_array('multifilter_csuszka', $informationFields)) {
            $this->db->query('ALTER TABLE `'.DB_PREFIX.'attribute` ADD `multifilter_csuszka` BOOLEAN DEFAULT 2;');
        }

        $query = $this->db->query('SHOW COLUMNS FROM '.DB_PREFIX."attribute_group");
        $informationFields = array();
        foreach($query->rows as $field) {
            $informationFields[] = $field['Field'];
        }
        if(!in_array('multifilter_szukit', $informationFields)) {
            $this->db->query('ALTER TABLE `'.DB_PREFIX.'attribute_group` ADD `multifilter_szukit` int(1);');
        }
        if(!in_array('multifilter_csuszka', $informationFields)) {
            $this->db->query('ALTER TABLE `'.DB_PREFIX.'attribute_group` ADD `multifilter_csuszka` int(1);');
        }
        if(!in_array('vezerel_kategoriat', $informationFields)) {
            $this->db->query('ALTER TABLE `'.DB_PREFIX.'attribute_group` ADD `vezerel_kategoriat` int(1);');
        }

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "warehouse (
            `warehouse_id`          int(11)	AUTO_INCREMENT,
            `country_id`            int(11),
            `zone_id`               int(11),
            `city`                  varchar(64),
            `postcode`              varchar(10),
            `address`               varchar(128),
            `contact_name`          varchar(64),
            `telephone`             varchar(32),
            `position`              varchar(32),
            `sort_order`            int(3),
            `status`                int(1),
            `email`                 varchar(96),
            PRIMARY KEY (`warehouse_id`) )   engine=MyISAM default charset=UTF8");

        $mezok = $this->tableRows("warehouse");
        if (!in_array("email",$mezok)) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "warehouse` ADD `email` varchar(96)");
            $this->db->query("UPDATE `" . DB_PREFIX . "warehouse` set `email` = 'w1@videopart.hu'                   WHERE warehouse_id = 1");
            $this->db->query("UPDATE `" . DB_PREFIX . "warehouse` set `email` = 'panasonic.allee@gmail.com'         WHERE warehouse_id = 2");
            $this->db->query("UPDATE `" . DB_PREFIX . "warehouse` set `email` = 'f46_videopart_hu@mail.datanet.hu'  WHERE warehouse_id = 3");
            $this->db->query("UPDATE `" . DB_PREFIX . "warehouse` set `email` = 'shop@videopart.hu'                 WHERE warehouse_id = 4");
            $this->db->query("UPDATE `" . DB_PREFIX . "warehouse` set `email` = 'studio@videopart.hu'               WHERE warehouse_id = 5");
        }


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "warehouse_description (
            `warehouse_id`          int(11),
            `language_id`           int(11)	,
            `warehouse_name`        varchar(128),
            `description`           text)   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_warehouse (
            `warehouse_id`          int(11),
            `product_id`            int(11),
            `quantity`              int(11)
             )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "setting` (
          `setting_id` int(11) NOT NULL AUTO_INCREMENT,
          `store_id` int(11) NOT NULL DEFAULT '0',
          `group` varchar(32) COLLATE utf8_bin NOT NULL,
          `key` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
          `value` text COLLATE utf8_bin NOT NULL,
          `serialized` tinyint(1) NOT NULL,
          PRIMARY KEY (`setting_id`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_package (
            `product_id`    int(11),
            `package_id`    int(11),
            `quantity`      int(4),
            `price`         decimal(15,4),
            PRIMARY KEY (`product_id`, `package_id`) )   engine=MyISAM default charset=UTF8");

        $mezok = $this->tableRows("product_package");
        if (!in_array("price",$mezok)) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product_package ADD `price` decimal(15,4)");
        }

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_accessory (
            `product_id`    int(11),
            `accessory_id`    int(11),
            PRIMARY KEY (`product_id`, `accessory_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_general_accessory (
            `product_id`    int(11),
            `general_accessory_id`    int(11),
            PRIMARY KEY (`product_id`, `general_accessory_id`) )   engine=MyISAM default charset=UTF8");

        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_ajandek (
            `product_id`    int(11),
            `ajandek_id`    int(11),
            PRIMARY KEY (`product_id`, `ajandek_id`) )   engine=MyISAM default charset=UTF8");


        $this->db->query("CREATE TABLE if not exists " . DB_PREFIX . "product_helyettesito (
            `product_id`    int(11),
            `helyettesito_id`    int(11),
            PRIMARY KEY (`product_id`, `helyettesito_id`) )   engine=MyISAM default charset=UTF8");


        $this->createPavblogTables();
    }

    private function createPostcodeTable() {
        $sql = 'CREATE TABLE IF NOT EXISTS '.DB_PREFIX.'postcode (
                    postcodeid INT(19) PRIMARY KEY NOT NULL AUTO_INCREMENT,
                    postcode INT(4) NOT NULL,
                    city VARCHAR(100) NOT NULL,
                    country VARCHAR(100) NOT NULL,
                    INDEX `postcode` (`postcode` ASC),
                    INDEX `city` (`city` ASC))
                    CHARACTER SET utf8
                    COLLATE utf8_general_ci;;';
        $this->db->query($sql);
        $sql = "LOAD DATA LOCAL INFILE 'iranyitoszamok.csv' INTO TABLE ".DB_PREFIX."postcode
                CHARACTER SET utf8
                FIELDS TERMINATED BY ';' ENCLOSED BY '\"'
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (postcode, city, country);";
        $this->db->query($sql);
    }

    public function createPavblogTables() {
        $sql[] = "
        CREATE TABLE IF NOT EXISTS `".DB_PREFIX."pavblog_blog` (
        `blog_id` int(11) NOT NULL AUTO_INCREMENT,
			   `category_id` int(11) NOT NULL,
			   `position` int(11) NOT NULL,
			   `created` date NOT NULL,
			   `status` tinyint(1) NOT NULL,
			   `user_id` int(11) NOT NULL,
			   `hits` int(11) NOT NULL,
			   `image` varchar(255) NOT NULL,
			   `meta_keyword` varchar(255) NOT NULL,
			   `meta_description` varchar(255) NOT NULL,
			   `meta_title` varchar(255) NOT NULL,
			   `date_modified` date NOT NULL,
			   `video_code` varchar(255) NOT NULL,
			   `params` text NOT NULL,
			   `tags` varchar(255) NOT NULL,
			   `featured` tinyint(1) NOT NULL,
			   `keyword` varchar(255) NOT NULL,
			  PRIMARY KEY (`blog_id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;";

		$sql[] = "
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."pavblog_blog_description` (
        `blog_id` int(11) NOT NULL,
			  `language_id` int(11) NOT NULL,
			  `title` varchar(255) NOT NULL,
			  `description` text NOT NULL,
			  `content` text NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8; ";

		$sql[] = "
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."pavblog_category` (
        `category_id`        int(11) unsigned NOT NULL AUTO_INCREMENT,
			  `image`               varchar(255) NOT NULL DEFAULT '',
			  `parent_id`           int(11) NOT NULL DEFAULT '0',
			  `is_group`            smallint(6) NOT NULL DEFAULT '2',
			  `width`               varchar(255) DEFAULT NULL,
			  `submenu_width`       varchar(255) DEFAULT NULL,
			  `colum_width`         varchar(255) DEFAULT NULL,
			  `submenu_colum_width` varchar(255) DEFAULT NULL,
			  `item`                varchar(255) DEFAULT NULL,
			  `colums`              varchar(255) DEFAULT '1',
			  `type`                varchar(255) NOT NULL,
			  `is_content`          smallint(6) NOT NULL DEFAULT '2',
			  `show_title`          smallint(6) NOT NULL DEFAULT '1',
			  `meta_keyword`        varchar(255) NOT NULL DEFAULT '1',
			  `level_depth`         smallint(6) NOT NULL DEFAULT '0',
			  `type_submenu`        varchar(10) NOT NULL DEFAULT '1',
			  `published`           smallint(6) NOT NULL DEFAULT '1',
			  `store_id`            smallint(5) unsigned NOT NULL DEFAULT '0',
			  `position`            int(11) unsigned NOT NULL DEFAULT '0',
			  `show_sub`            smallint(6) NOT NULL DEFAULT '0',
			  `url`                 varchar(255) DEFAULT NULL,
			  `target`              varchar(25) DEFAULT NULL,
			  `privacy`             smallint(5) unsigned NOT NULL DEFAULT '0',
			  `position_type`       varchar(25) DEFAULT 'top',
			  `menu_class`          varchar(25) DEFAULT NULL,
			  `description`         text,
			  `content_text`        text,
			  `submenu_content`     text,
			  `meta_description`    text,
			  `meta_title`          varchar(255) DEFAULT NULL,
			  `level`               int(11) NOT NULL,
			  `left`                int(11) NOT NULL,
			  `right`               int(11) NOT NULL,
			  `keyword`             varchar(255) NOT NULL,
			  PRIMARY KEY (`category_id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ; ";

		$sql[] = "
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."pavblog_category_description` (
        `category_id`     int(11) NOT NULL,
				  `language_id`     int(11) NOT NULL,
				  `title`           varchar(255) NOT NULL,
				  `description`     text NOT NULL,
				  PRIMARY KEY (`category_id`,`language_id`),
				  KEY `name` (`title`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8; ";

		$sql[] = "
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."pavblog_comment` (
        `comment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
					  `blog_id` int(11) unsigned NOT NULL,
					  `comment` text NOT NULL,
					  `status` tinyint(1) NOT NULL DEFAULT '0',
					  `created` datetime DEFAULT NULL,
					  `user` varchar(255) NOT NULL,
					  `email` varchar(255) NOT NULL,
					  PRIMARY KEY (`comment_id`),
					  KEY `FK_blog_comment` (`blog_id`)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;";

		foreach( $sql as $q ){
			$query = $this->db->query( $q );
		}


        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "news`
                (`news_id` int(11) NOT NULL auto_increment,
                `status` int(1) NOT NULL default '0',
                `image` VARCHAR(255) COLLATE utf8_general_ci default NULL,
                `image_size` int(1) NOT NULL default '0',
                `date_added` datetime default NULL,
                `viewed` int(5) NOT NULL DEFAULT '0',
                PRIMARY KEY (`news_id`))
            ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");



        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "apri` (
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "apri_unsubscribe` (
  `md5_email` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "biztositas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `biztositas_tol` int(11) NOT NULL,
  `biztositas_ig` int(11) NOT NULL,
  `biztositas_ertek` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ecbanner` (
  `ecbanner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `main_width` int(11) DEFAULT '0',
  `main_height` int(11) DEFAULT '0',
  PRIMARY KEY (`ecbanner_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;");


        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ecbanner_image` (
  `ecbanner_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `ecbanner_id` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `ordering` int(11) DEFAULT '0',
  `params` text,
  PRIMARY KEY (`ecbanner_image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=11 ;");


        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ecbanner_image_description` (
  `ecbanner_image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `ecbanner_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `custom_code` text,
  PRIMARY KEY (`ecbanner_image_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "faq` (
  `idFaq` int(11) NOT NULL AUTO_INCREMENT,
  `idCategory` int(11) DEFAULT NULL,
  `question` mediumtext,
  `response` mediumtext,
  PRIMARY KEY (`idFaq`),
  KEY `idCategory` (`idCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "faq_categories` (
  `idCategory` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_product_elhelyezes` (
  `elhelyezes_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `order_product_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `elhelyezkedes_id` int(3) DEFAULT NULL,
  `elhelyezkedes_alcsoport_id` int(3) DEFAULT NULL,
  `idoszak` int(3) DEFAULT NULL,
  `idoszak_id` int(1) DEFAULT NULL,
  `priority` int(3) DEFAULT NULL,
  `kiemelesek_id` int(3) DEFAULT NULL,
  `datum_tol` date DEFAULT NULL,
  `datum_ig` date DEFAULT NULL,
  `penzugyi_status_id` int(1) DEFAULT NULL,
  `fizetes_elbiralas_status_id` int(1) DEFAULT NULL,
  `sor` int(11) DEFAULT NULL,
  `elhelyezkedes_neve` varchar(50) DEFAULT NULL,
  `elhelyezkedes_netto_ara` decimal(10,4) DEFAULT NULL,
  `elhelyezkedes_brutto_ara` decimal(10,4) DEFAULT NULL,
  `elhelyezkedes_alcsoport_neve` varchar(50) DEFAULT NULL,
  `elhelyezkedes_alcsoport_netto_ara` decimal(10,4) DEFAULT NULL,
  `elhelyezkedes_alcsoport_brutto_ara` decimal(10,4) DEFAULT NULL,
  `kiemelesek_neve` varchar(50) DEFAULT NULL,
  `kiemelesek_netto_ara` decimal(10,4) DEFAULT NULL,
  `kiemelesek_brutto_ara` decimal(10,4) DEFAULT NULL,
  `idoszak_mennyiseg` varchar(50) DEFAULT NULL,
  `netto_ar` decimal(10,4) DEFAULT NULL,
  `brutto_ar` decimal(10,4) DEFAULT NULL,
  `total_netto_ar` decimal(10,4) DEFAULT NULL,
  `total_brutto_ar` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`elhelyezes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

        $sql = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."szamlazzhu` (
        `szamlazzhu_id` int(11) NOT NULL AUTO_INCREMENT,
        `order_id` int(11) NOT NULL,
        `eloleg_szamla` int(11) NOT NULL,
        `vegszamla` int(11) NOT NULL,
        `dijbekero` int(11) NOT NULL,
        `fizetve` int(11) NOT NULL,
        `kelt_datum` varchar(255) NOT NULL,
        `teljesites_datum` varchar(255) NOT NULL,
        `fizetesi_hatarido_datum` varchar(255) NOT NULL,
        `fizetesi_mod` varchar(255) NOT NULL,
        `penznem` varchar(255) NOT NULL,
        `peldany_szam` int(11) NOT NULL,
        `szamla_nyelve` varchar(255) NOT NULL,
        `arfolyam_bank` varchar(255) NOT NULL,
        `arfolyam` varchar(255) NOT NULL,
        `szamlaszam_elotag` varchar(255) NOT NULL,
        `megjegyzes` text NOT NULL,
        PRIMARY KEY (`szamlazzhu_id`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8";
        $this->db->query($sql);


        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "price_alert` (
  `price_alert_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `currency_code` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`price_alert_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=11 ;");

       /* $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "product_to_special` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `quantity` int(4) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;");*/

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "redirect` (
  `url_id` int(11) NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) NOT NULL,
  `referer` varchar(150) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`url_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "special_offer_reminder` (
  `reminder_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `days_before` int(11) NOT NULL,
  PRIMARY KEY (`reminder_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=2 ;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "store_setting` (
  `store_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  `go` int(1) NOT NULL,
  `setting_id` int(11) NOT NULL,
  PRIMARY KEY (`store_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=6 ;");








        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "news_description`
                (`news_id` int(11) NOT NULL default '0',
                `language_id` int(11) NOT NULL default '0',
                `title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                `meta_description` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
                `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                `keyword` varchar(255) COLLATE utf8_general_ci NOT NULL,
                PRIMARY KEY (`news_id`,`language_id`))
            ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "news_to_store`
                (`news_id` int(11) NOT NULL,
                `store_id` int(11) NOT NULL,
                PRIMARY KEY (`news_id`, `store_id`))
            ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");

    }


    public function categoryPath() {
        $sql = "SELECT * FROM " . DB_PREFIX . "category";

        $arr2 = $this->db->query($sql);

        $ketegoria_tree = array();
        foreach ( $arr2->rows as $value ){
            $ketegoria_tree[] = $this->buildTree($value);
        }

        foreach($ketegoria_tree as $value){
            $szint = 0;
            if (isset($value['categoria']['szulo'])) {
                $szint = count($value['categoria']['szulo']);
            }
            $sql = "INSERT INTO " .DB_PREFIX. "category_path set
                category_id = '" .$value['categoria']['kategoria']. "',
                path_id ='" .$value['categoria']['kategoria']. "',
                level ='" .$szint.  "'";
            $this->db->query($sql);

            if (isset($value['categoria']['szulo'])) {
                foreach($value['categoria']['szulo'] as $szulo){
                    $szint--;
                    $sql = "INSERT INTO " .DB_PREFIX. "category_path set
                    category_id = '" .$value['categoria']['kategoria']. "',
                    path_id = '".$szulo."',
                    level ='".$szint."'";
                $this->db->query($sql);
                }
            }
        }

        echo "";
    }

    function buildTree($value) {
        $branch = array();

        $branch['categoria']['kategoria'] = $value['category_id'];
        if ($value['parent_id'] > 0 ) {
            $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$value['parent_id'] . "'";
            $uj_categori_id = $this->db->query($sql);
            $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];
            if ($uj_categori_id->row['parent_id'] > 0 ) {
                $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                $uj_categori_id = $this->db->query($sql);
                $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                if ($uj_categori_id->row['parent_id'] > 0 ) {
                    $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                    $uj_categori_id = $this->db->query($sql);
                    $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                    if ($uj_categori_id->row['parent_id'] > 0 ) {
                        $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                        $uj_categori_id = $this->db->query($sql);
                        $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                        if ($uj_categori_id->row['parent_id'] > 0 ) {
                            $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                            $uj_categori_id = $this->db->query($sql);
                            $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                            if ($uj_categori_id->row['parent_id'] > 0 ) {
                                $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                $uj_categori_id = $this->db->query($sql);
                                $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                if ($uj_categori_id->row['parent_id'] > 0 ) {
                                    $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                    $uj_categori_id = $this->db->query($sql);
                                    $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                    if ($uj_categori_id->row['parent_id'] > 0 ) {
                                        $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                        $uj_categori_id = $this->db->query($sql);
                                        $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                        if ($uj_categori_id->row['parent_id'] > 0 ) {
                                            $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                            $uj_categori_id = $this->db->query($sql);
                                            $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                            if ($uj_categori_id->row['parent_id'] > 0 ) {
                                                $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                                $uj_categori_id = $this->db->query($sql);
                                                $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                                if ($uj_categori_id->row['parent_id'] > 0 ) {
                                                    $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                                    $uj_categori_id = $this->db->query($sql);
                                                    $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                                    if ($uj_categori_id->row['parent_id'] > 0 ) {
                                                        $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                                        $uj_categori_id = $this->db->query($sql);
                                                        $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];

                                                        if ($uj_categori_id->row['parent_id'] > 0 ) {
                                                            $sql = "SELECT * FROM " . DB_PREFIX . "category where category_id = '" . (int)$uj_categori_id->row['parent_id'] . "'";
                                                            $uj_categori_id = $this->db->query($sql);
                                                            $branch['categoria']['szulo'][] = $uj_categori_id->row['category_id'];
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
         }


        return $branch;
    }

    public function tableRows($tabla_name) {

        $sql = "SHOW columns from `".DB_PREFIX.$tabla_name."`";

        $oszlop=$this->db->query($sql);
        $product_elhelyezes = array();
        foreach($oszlop->rows as $curoszlop) {
            $product_elhelyezes[] = $curoszlop['Field'];
        }
        return $product_elhelyezes;
    }

    public function tableSimpleIndex($tabla_name,$index_key,$index_type="INDEX") {

        $index=$this->db->query("SHOW index from ".DB_PREFIX.$tabla_name);

        $talalt = false;

        if ($index_type == "AUTO_INCREMENT") {
            $index2=$this->db->query("SHOW COLUMNS from ".DB_PREFIX.$tabla_name);
            foreach($index2->rows as $column) {
                if ($column['Field'] == $index_key) {
                    if ($column['Extra'] == 'auto_increment') {
                        return false;
                    } else {
                        $this->db->query("ALTER TABLE " . DB_PREFIX .$tabla_name." MODIFY ".$index_key." INT NOT NULL");
                        $query  = $this->db->query("SHOW INDEXES FROM " .DB_PREFIX .$tabla_name." WHERE Key_name = 'PRIMARY'");
                        if ($query->row) {
                            $this->db->query("ALTER TABLE " . DB_PREFIX .$tabla_name." DROP PRIMARY KEY");
                        }
                    }
                }
            }
            $this->db->query("ALTER TABLE " . DB_PREFIX .$tabla_name." CHANGE ".$index_key." ".$index_key." INT(11)  AUTO_INCREMENT PRIMARY KEY");
            return true;
        }


        if (substr($index_type,0,7) == "PRIMARY") {
            $query  = $this->db->query("SHOW INDEXES FROM " .DB_PREFIX .$tabla_name." WHERE Key_name = 'PRIMARY'");
            if ($query->row) {
                return false;

            }

            $index_keys = explode(',',$index_key);
            foreach($index_keys as $key=>$index_kulcs) {
                foreach($index->rows as $value) {
                    echo '';
                    if ($value['Key_name'] == 'PRIMARY' && $value['Column_name'] == $index_kulcs) {
                        unset ($index_keys[$key]);
                    }

                }
            }
            if (!$index_keys) $talalt = true;

        } else {
            foreach($index->rows as $value) {
                if ($value['Key_name'] == $index_key) {
                    $talalt = true;
                    break;
                }
            }
        }
        if (!$talalt) {
            $this->db->query("ALTER TABLE " . DB_PREFIX .$tabla_name." ADD ".$index_type." (".$index_key.")");
        }
    }
}

?>