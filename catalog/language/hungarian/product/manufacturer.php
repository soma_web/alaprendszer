<?php

// Heading
$_['heading_title']     = 'Kiemelt gyártóink';

// Text
$_['text_brand']        = 'Márka';
$_['text_index']        = 'Márka szám:';
$_['text_error']        = 'Nem található a gyártó!';
$_['text_empty']        = 'Nincsenek listázandó termékek ettől a gyártótól.';
$_['text_quantity']     = 'Menny.:';
$_['text_manufacturer'] = 'Márka:';
$_['text_model']        = 'Termékkód:';
$_['text_points']       = 'Jutalom pontok:';
$_['text_price']        = 'Ár:';
$_['text_tax']          = 'Nettó:';
$_['text_reviews']      = '%s vélemény alapján.';
$_['text_compare']      = 'termék összehasonlítása (%s)';
$_['text_display']      = 'Nézet:';
$_['text_list']         = 'Lista';
$_['text_grid']         = 'Rács';
$_['text_stock_in']     = 'Várható beérkezés:';


$_['text_sort']         = 'Rendezés:';
$_['text_default']      = 'Alapértelmezett';
$_['text_name_asc']     = 'Név szerint növekvő';
$_['text_name_desc']    = 'Név szerint csökkenő';
$_['text_price_asc']    = 'Olcsóbbtól a drágábbig';
$_['text_price_desc']   = 'Drágábbtól az olcsóbbig';
$_['text_rating_asc']   = 'Értékelés (Legrosszabbak elől)';
$_['text_rating_desc']  = 'Értékelés (Legjobbak elől)';
$_['text_model_asc']    = 'Cikkszám szerint növekvő';
$_['text_model_desc']   = 'Cikkszám szerint csökkenő';



$_['text_limit']        = 'Mutat:';

$_['text_raktar']       = 'Boltonkénti készlet';
$_['entry_garancia_honap']      = 'hónap';
$_['entry_garancia_ev']         = 'év';
$_['text_garancia']             = 'garancia';
$_['text_szallitas']            = 'Szállítási költség:';
?>
