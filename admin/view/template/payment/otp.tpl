<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                    <tr>
                        <td><?php echo $entry_total; ?></td>
                        <td><input type="text" name="otp_total" value="<?php echo $otp_total; ?>" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_order_status; ?></td>
                        <td><select name="otp_order_status_id">
                            <?php foreach ($order_statuses as $order_status) { ?>
                            <?php if ($order_status['order_status_id'] == $otp_order_status_id) { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_geo_zone; ?></td>
                        <td><select name="otp_geo_zone_id">
                            <option value="0"><?php echo $text_all_zones; ?></option>
                            <?php foreach ($geo_zones as $geo_zone) { ?>
                            <?php if ($geo_zone['geo_zone_id'] == $otp_geo_zone_id) { ?>
                                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_privkey; ?></td>
                        <td><input type="text" name="otp_privkey" value="<?php echo $otp_privkey; ?>" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_haromszereplos; ?></td>
                        <td><input type="text" name="otp_haromszereplos" value="<?php echo $otp_haromszereplos; ?>" size="30"/></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_otp_webshop_client; ?></td>
                        <td><input type="text" name="otp_webshop_client" value="<?php echo $otp_webshop_client; ?>" size='30'/></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_status; ?></td>
                        <td><select name="otp_status">
                            <?php if ($otp_status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                        </select></td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_sort_order; ?></td>
                        <td><input type="text" name="otp_sort_order" value="<?php echo $otp_sort_order; ?>" size="1" /></td>
                    </tr>
                    <!--<tr>
                        <td>OTP bank kulcs fájl:<br><?php if(isset($otp_key_file)){echo "(".$otp_key_file.")";}?></td>
                        <td><input type="file" name="otp_key_file"></td>
                    </tr>-->

                    <?php if ($crm_kapcsolat) { ?>
                        <tr>
                            <td><?php echo $entry_crm_fizetesi_mod; ?></td>
                            <td>
                                <select name="otp_crm_fizetesi_mod">
                                    <option value=""><?php echo $text_select; ?> </option>
                                    <?php foreach($crm_fizetesi_modok as $value) { ?>
                                        <option value="<?php echo $value['cf_2437']?>"  <?php echo $value['cf_2437'] == $otp_crm_fizetesi_mod ? 'selected="selected"' : ""; ?> ><?php echo $value['cf_2437']?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo $entry_crm_fizetesi_hatarido; ?></td>
                            <td>
                                <input type="number" name = "otp_crm_fizetesi_hatarido" size="2" value="<?php echo $otp_crm_fizetesi_hatarido?>">
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?> 