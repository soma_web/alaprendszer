<?php

// Heading
$_['heading_title']    = 'Ingyenes szállítás';

// Text
$_['text_shipping']    = 'Szállítás';
$_['text_success']     = 'Siker: Az ingyenes szállítás módosítása megtörtént!';
$_['text_gls_csomagpont']  = 'GLS Csomagpont megjelenítése';

// Entry
$_['entry_total']      = 'Összesen:<br /><span class="help">Részösszeg szükséges, mielőtt az ingyenes szállítási modul elérhető.</span>';
$_['entry_geo_zone']   = 'Földrajzi zóna:';
$_['entry_status']     = 'Állapot:';
$_['entry_sort_order'] = 'Sorrend:';
$_['entry_title_heading'] = 'Fejléc megnevezése:';
$_['entry_title_text'] = 'Szállítási mód megnevezése:';
$_['entry_mail']        = 'Egyéb figyelmeztető e-mailek:<br /><span class="help">Egyéb figyelmeztető e-mailek, amiket kapni szeretne, a közponzi áruház  e-mailjei mellett. (vesszővel elválasztva)</span>';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az Ingyenes szállítás módosítása az Ön számára nem engedélyezett!';
?>
