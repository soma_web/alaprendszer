<?php

// Heading
$_['heading_title'] = 'Product CSV Mentés/Visszaállítás';

$_['entry_backup']  = 'CSV export';
$_['entry_restore'] = 'CSV import:';

$_['text_success']  = 'Siker: Az adatbázis importálása sikerült!';
$_['text_tol']      = 'Elemszámtól:';
$_['text_db']       = 'db:';
$_['text_sorrend']  = 'Sorrend';

$_['button_backup'] = 'CSV export';
$_['button_restore']= 'CSV import';

// Error
$_['error_permission'] = 'Figyelmeztetés: A biztonsági mentések módosítása az Ön számára nem engedélyezett';
$_['error_empty']      = 'Figyelmeztetés: a feltöltött file üres!';
$_['error_empty_result'] = 'Figyelmeztetés: nincs a beállítással megegyező adat!';
$_['error_format']      = 'Figyelmeztetés: Helytelen formátum! A CSV nem került beolvasásra.';
?>