<?php
// Text
$_['text_subject']              = '[%s] Cart Reminder';
$_['text_hi']                   = 'Hi <strong>%s</strong>,';
$_['text_info']                 = 'We noticed that you placed the following product(s) in your shopping cart, but did not complete your order:';
$_['text_cart_content']         = '<strong>Shopping Cart content:</strong>';
$_['text_view_product_detail']  = 'view product details';
$_['text_coupon']               = 'To show how much we value your bussines, we are giving you a special coupon <strong><u>if you order in next %s day(s):</u></strong>';
$_['text_coupon_code']          = 'Coupon code: <strong>%s</strong>';
$_['text_coupon_notice']        = '**To use coupon - need to be logged.';
$_['text_order_now']            = 'To restore your shopping cart just login in your account: [ <a href="%s" target="blank">Login here</a> ]';
$_['text_thanks']               = '<strong>Thanks</strong>, <br />%s<br />%s<br />%s<br />%s';

// part mail 2
$_['text_info2']                = 'We are not sure if you received our previous email. <br /><br /> Few days ago, you started to place an order, but somehow, you were unable to complete the form.<br />';
$_['text_coupon2']              = 'We decided to give you another special coupon <strong><u>if you order in next %s day(s):</u></strong>';
$_['text_shop_methods']         = 'There are two ways to complete your order in two minutes or less: <br /><ul><li>Use this link to login and order on our page: <a href="%s">login here</a></li><li> Call %s. We are standing by to answer your questions.</li></ul>';
$_['text_need_help']            = 'Please let us know if there is any other way we can help.';


// summary log for admin

$_['text_log_subject']                  = 'Log';
$_['text_log_mail_sent_to']             = 'Reminders sent to: ';
$_['text_log_mail_no_coupon']           = 'WITHOUT COUPON (Reason: Already sent until now %s coupons)';
$_['text_log_no_customers']             = 'No customers with abandoned cart at this moment.';
$_['text_reminder_sent_to']             = 'Reminders sent to: ';
$_['text_access_denied_secret_code']    = 'Access Denied: Wrong secret code';
$_['text_forgot_secret_code']           = 'You forgot secret code';
$_['text_without_coupon']               = ' WITHOUT COUPON (Reason: ';
$_['text_email_sender']                 = 'Abandoned Cart Reminder';
$_['text_sent_coupon']                  = ' already sent) <br />';
$_['text_no_customer_to_alert']         = 'There is no any customers to alert. <br />';
?>