<?php

$utvonal    = false;
$image_icon = false;

if ( isset($this->request->request['route']) && $this->request->request['route'] != "design/megjelenito_header") {



    if (isset($this->request->request['route']) && !isset($this->request->request['path']) ) {
        if ($this->request->request['route'] != "common/home") {
            $this->load->model('design/layout');
            $layout_id = $this->model_design_layout->getLayout($this->request->request['route']);
            $utvonal = $this->model_design_layout->getLayoutName($layout_id);
        }

    } elseif (isset($this->request->request['path']) ) {
        $path = explode("_",$this->request->request['path']);
        if (isset($path[0])) {
            $this->load->model('catalog/category');
            $category = $this->model_catalog_category->getCategory($path[0]);

            $this->load->model('tool/image');
            if (isset($category['image_icon']) && $category['image_icon']) {
                $image_icon = $this->model_tool_image->resize($category['image_icon'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
            } else {
                $image_icon =  $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'),false);
            }

            if(isset($category['name']) && $category['name']) {
                $utvonal = $category['name'];
            }
        }
    }
}

?>
