<?php

// Locale
$_['code']                    = 'hu';
$_['direction']               = 'ltr';
$_['date_format_short']       = 'Y-m-d';
$_['date_format_long']        = 'Y-m-d';
$_['time_format']             = 'h:i:s';
$_['decimal_point']           = ',';
$_['thousand_point']          = '.';

// Text
$_['text_yes']                = 'Igen';
$_['text_no']                 = 'Nem';
$_['text_enabled']            = 'Engedélyezett';
$_['text_disabled']           = 'Letiltott';
$_['text_none']               = ' --- Nincs --- ';
$_['text_select']             = ' --- Válasszon --- ';
$_['text_select_all']         = 'Mindent kijelöl';
$_['text_unselect_all']       = 'Kijelölés visszavonása';
$_['text_all_zones']          = 'Minden zóna';
$_['text_default']            = ' <b>(Alapértelmezett)</b>';
$_['text_close']              = 'Bezárás';
$_['text_pagination']         = 'Tételek: {start} - {end} / {total} ({pages} oldal)';
$_['text_no_results']         = 'Nincs találat!';
$_['text_separator']          = ' > ';
$_['text_edit']               = 'Módosítás';
$_['text_view']               = 'Nézet';
$_['text_home']               = 'Kezdőlap';
$_['tab_customer']            = 'Vevő adatai';
$_['tab_elhelyezkedes']       = 'Elhelyezkedés - kiemelés';
$_['entry_fejlec_latszik']    = 'Fejléc látszik';
$_['text_order']              = 'Rendelés';
$_['text_success']           = 'Siker: A módosítás megtörtént!';

$_['text_image_manager']     = 'Képszerkesztő';
$_['text_browse']            = 'Fájlok tallózása';
$_['text_clear']             = 'Kép törlése';


// Button
$_['button_insert']           = 'Beszúrás';
$_['button_delete']           = 'Törlés';
$_['button_save']             = 'Mentés';
$_['button_cancel']           = 'Mégse';
$_['button_clear']            = 'Log törlése';
$_['button_close']            = 'Bezárás';
$_['button_filter']           = 'Szűrés';
$_['button_send']             = 'Küldés';
$_['button_edit']             = 'Módosítás';
$_['button_copy']             = 'Másolás';
$_['button_back']             = 'Vissza';
$_['button_remove']           = 'Eltávolítás';
$_['button_remove_pdf']       = 'Törlés';
$_['button_backup']           = 'Biztonsági másolat';
$_['button_restore']          = 'Helyreállít';
$_['button_upload']           = 'Feltöltés';
$_['button_invoice']          = 'Megrendelések nyomtatás';
$_['button_add_address']      = 'Cím hozzáadása';
$_['button_add_attribute']    = 'Tulajdonság hozzáadása';
$_['button_add_banner']       = 'Banner hozzáadása';
$_['button_add_product']      = 'Termék hozzáadása';
$_['button_add_option']       = 'Választék hozzáadása';
$_['button_add_option_value'] = 'Választékérték hozzáadása';
$_['button_add_discount']     = 'Mennyiségi kedvezmény hozzáadása';
$_['button_add_vevo']         = 'Vevő kedvezmény hozzáadása';
$_['button_add_special']      = 'Akciós ár hozzáadása';
$_['button_add_image']        = 'Kép hozzáadása';
$_['button_add_geo_zone']     = 'Földrajzi zóna hozzáadása';
$_['button_add_history']      = 'Rendelési előzmény hozzáadása';
$_['button_add_transaction']  = 'Kredit hozzáadása';
$_['button_add_total']        = 'Összesítés hozzáadása';
$_['button_add_reward']       = 'Jutalompont hozzáadása';
$_['button_add_route']        = 'Útvonal hozzáadása';
$_['button_add_rule' ]        = 'Szabály hozzáadása';
$_['button_add_module']       = 'Modul Hozzáadás';
$_['button_add_link']         = 'Link hozzáadása';
$_['button_add_elhelyezkedes']= 'Elhelyezkedés hozzáadása';
$_['button_approve']          = 'Jóváhagy';
$_['button_reset']            = 'Visszaállít';
$_['button_add_filter']       = 'Szúrő hozzáadása';
$_['button_nyomtatas']        = 'Nyomtatás';
$_['button_exportalas']       = 'Exportálás';


// Tab
$_['tab_address']             = 'Cím';
$_['tab_admin']         = 'Kezelés';
$_['tab_attribute']           = 'Tulajdonság';
$_['tab_coupon_history']      = 'Utalványtörténet';
$_['tab_data']                = 'Adatok';
$_['tab_design']              = 'Dizájn';
$_['tab_discount']            = 'Mennyiségi kedvezmény';
$_['tab_fraud']               = 'Csalás detektáló';
$_['tab_general']             = 'Általános';
$_['tab_ip']                  = 'IP cím';
$_['tab_fizetes']             = 'Egyedi fizetési mód';
$_['tab_szazalek']            = 'IP cím';
$_['tab_links']               = 'Kapcsolatok';
$_['tab_image']               = 'Kép';
$_['tab_option']              = 'Választék';
$_['tab_server']              = 'Szerver';
$_['tab_store']               = 'Üzlet';
$_['tab_special']             = 'Akciós ár';
$_['tab_local']               = 'Hely';
$_['tab_mail']                = 'Levelezés';
$_['tab_module']              = 'Modul';
$_['tab_order']               = 'Rendelések';
$_['tab_order_history']       = 'Rendelési előzmények';
$_['tab_payment']             = 'Számlázási cím';
$_['tab_product']             = 'Termékek';
$_['tab_return']              = 'Garancia részletek';
$_['tab_return_history']      = 'Garancia történet';
$_['tab_reward']              = 'Jutalom pontok';
$_['tab_shipping']            = 'Szállítási cím';
$_['tab_vevok']               = 'Vevő kedvezmény';


$_['tab_total']               = 'Összesítések';
$_['tab_transaction']         = 'Kreditek';
$_['tab_voucher_history']     = 'Utalvány történet';

// Error
$_['error_upload_1']          = 'Figyelmeztetés: A feltöltendő fájl mérete túllépi a php.ini fájlban beállított upload_max_filesize utasítás értékét!';
$_['error_upload_2']          = 'Figyelmeztetés: A feltöltendő fájl mérete túllépi a HTML-űrlapban megadott MAX_FILE_SIZE utasítás értékét!';
$_['error_upload_3']          = 'Figyelmeztetés: A fájl feltöltése csak részben történt meg!';
$_['error_upload_4']          = 'Figyelmeztetés: Fájl nem került feltöltésre!';
$_['error_upload_6']          = 'Figyelmeztetés: Hiányzik egy ideiglenes mappa!';
$_['error_upload_7']          = 'Figyelmeztetés: A fájl lemezre írása nem sikerült!';
$_['error_upload_8']          = 'Figyelmeztetés: Bővítmény által leállított fájlfeltöltés!';
$_['error_upload_999']        = 'Figyelmeztetés: Nincs elérhető hibakód!';

$_['success_modositas']  = 'Sikerült! A módosítás megtörtént.';
$_['error_sql']  = 'Figyelem: Hiba történt az adatbázis módosítása során!';
$_['error_post']  = 'Figyelem: Hiba történt az adatátvitel során!';
?>
