<?php
class ModelExtensionFaq extends Model {	
	public function creationTables(){
		$queryFaqCategories = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "faq_categories` (
			  `idCategory` int(11) NOT NULL auto_increment,
			  `category` varchar(50) character set utf8 default NULL,
			  PRIMARY KEY  (`idCategory`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$this->db->query($queryFaqCategories);
	
		$queryFaq = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "faq` (
					  `idFaq` int(11) NOT NULL auto_increment,
					  `idCategory` int(11) default NULL,
					  `question` varchar(50000) character set utf8 default NULL,
					  `response` varchar(50000) character set utf8 default NULL,
					  PRIMARY KEY  (`idFaq`),
					  KEY `idCategory` (`idCategory`),
					  CONSTRAINT `faq_ibfk_1` FOREIGN KEY (`idCategory`) REFERENCES `" . DB_PREFIX . "faq_categories` (`idCategory`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$this->db->query($queryFaq);
	}
	
	public function getFaqCategories() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq_categories fc ORDER BY fc.category");
		
		return $query->rows;
	}
	
	public function getFaqs($limitMin, $limitMax, $sort, $column) {
		if($column == "category"){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq f, " . DB_PREFIX . "faq_categories fc WHERE f.idCategory = fc.idCategory ORDER BY fc.category ". $sort. " LIMIT ".$limitMin.", ".$limitMax);
		}
		else if($column == "question"){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq f, " . DB_PREFIX . "faq_categories fc WHERE f.idCategory = fc.idCategory ORDER BY f.question ". $sort. " LIMIT ".$limitMin.", ".$limitMax);
		}
		return $query->rows;
	}
	
	public function getFaqsQuery($query, $sort, $column) {
		if($column == "category"){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq f, " . DB_PREFIX . "faq_categories fc WHERE f.idCategory = fc.idCategory AND (f.question LIKE '%".$query."%' OR f.response LIKE '%".$query."%') ORDER BY fc.category ". $sort);
		}
		else if($column == "question"){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq f, " . DB_PREFIX . "faq_categories fc WHERE f.idCategory = fc.idCategory AND (f.question LIKE '%".$query."%' OR f.response LIKE '%".$query."%') ORDER BY f.question ". $sort);
		}
		return $query->rows;
	}

	public function addFaq($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "faq SET idCategory = '" . $data['category'] . "', question = '" . $data['question'] . "', response = '" . $data['response'] . "'");
		
		$this->cache->delete('faq');
	}	
	
	public function editFaq($idFaq, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "faq SET idCategory = '" . $data['category'] . "', question = '" . $data['question'] . "', response = '" . $data['response'] . "' WHERE idFaq = '" . (int)$idFaq . "'");
		
		$this->cache->delete('faq');
	}
	
	public function deleteFaq($idFaq) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "faq WHERE idFaq = '" . (int)$idFaq . "'");
		$this->cache->delete('faq');
	}	
		public function getTotalFaqs() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "faq");
		
		return $query->row['total'];
	}	
	
	public function getTotalFaqsByCategory($idCategory) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "faq where idCategory= '".(int)$idCategory."'");
		
		return $query->row['total'];
	}	
	
	public function getFaqCategoryById($idCategory) {
		$query = $this->db->query("SELECT idCategory, category FROM " . DB_PREFIX . "faq_categories f WHERE f.idCategory = '" . (int)$idCategory . "'");
		return $query->row;
	}
	
	public function testCategory($idCategory) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq f WHERE f.idCategory = '" . (int)$idCategory . "'");
		if(count($query->row) != 0){
			return true;
		}
		return false;
	}

	public function addFaqCategories($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "faq_categories SET category = '" . $data['category'] . "'");
		
		$this->cache->delete('faq_categories');
	}	
	
	public function editFaqCategories($idCategory, $data) {
		$sql = "UPDATE " . DB_PREFIX . "faq_categories SET category = '" . $data['category'] . "' WHERE idCategory = '" . (int)$idCategory . "'";
		$this->db->query($sql);

		$this->cache->delete('faq_categories');
	}
	
	public function deleteFaqCategories($idCategory) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "faq_categories WHERE idCategory = '" . (int)$idCategory . "'");
		$this->cache->delete('faq_categories');
	}	
	
	public function getFaqCategoriesDescriptions($idCategory) {
		$faqCategories_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faqCategories_description WHERE idCategory = '" . (int)$idCategory . "'");

		foreach ($query->rows as $result) {
			$faqCategories_description_data[$result['language_id']] = array(
				'title'       => $result['title'],
				'description' => $result['description']
			);
		}
		
		return $faqCategories_description_data;
	}
	
	public function getFaqCategoriesStores($idCategory) {
		$faqCategories_store_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faqCategories_to_store WHERE idCategory = '" . (int)$idCategory . "'");

		foreach ($query->rows as $result) {
			$faqCategories_store_data[] = $result['store_id'];
		}
		
		return $faqCategories_store_data;
	}

	public function getFaqCategoriesLayouts($idCategory) {
		$faqCategories_layout_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faqCategories_to_layout WHERE idCategory = '" . (int)$idCategory . "'");
		
		foreach ($query->rows as $result) {
			$faqCategories_layout_data[$result['store_id']] = $result['layout_id'];
		}
		
		return $faqCategories_layout_data;
	}
		
	public function getTotalFaqCategories() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "faq_categories");
		
		return $query->row['total'];
	}	
	
	public function getTotalFaqCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "faqCategories_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}	
}
?>