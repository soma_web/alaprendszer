<?php
// Heading
$_['heading_title']    = 'Packaging Unit';

// Text
$_['text_success']     = 'Success: You have modified packaging units!';

// Column
$_['column_title']     = 'Packaging Unit Title';
$_['column_unit']      = 'Packaging Unit Unit';
$_['column_value']     = 'Value';
$_['column_action']    = 'Action';

// Entry
$_['entry_title']      = 'Packaging Unit Title:';
$_['entry_unit']       = 'Packaging Unit Unit:';
$_['entry_value']      = 'Value:<br /><span class="help">Set to 1.00000 if this is your default Packaging Unit.</span>';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Packaging classes!';
$_['error_title']      = 'Packaging Unit Title must be between 3 and 32 characters!';
$_['error_unit']       = 'Packaging Unit Unit must be between 1 and 4 characters!';
$_['error_default']    = 'Warning: This Packaging Unit class cannot be deleted as it is currently assigned as the default store Packaging Unit class!';
$_['error_product']    = 'Warning: This Packaging Unit class cannot be deleted as it is currently assigned to %s products!';

?>
