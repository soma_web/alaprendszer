<?php
// Heading
$_['heading_title']       = 'Árlista termékek';
$_['common_title']		  = 'Árlista';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeresen módosította az  árlista modult!';

//Entry
$_['entry_login']         = 'Belépés szükséges:<br /><span class="help">Árlista modul megtekintéséhez bejelentkezés szükséges</span>';

// Error
$_['error_permission']    = 'Figyelmeztetés: Az árlista modul módosítása az Ön számára nem engedélyezett!';
?>