<!--<div style="clear: both"></div>-->
</div>
</div>
</div>
</div>


   <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer_include.tpl')) {
        $footer_plusz = DIR_TEMPLATE. $this->config->get('config_template') . '/template/common/footer_include.tpl';
    } else {
        $footer_plusz = DIR_TEMPLATE.'default/template/common/footer_include.tpl';
    } ?>
    <?php include($footer_plusz); ?>

    <div id="powered"><div class="powered-inner"><?php echo $powered; ?></div></div>


    <?php if ($this->config->get('megjelenit_footer_kategoria') == 1){ ?>
        <ul class="footer_category" >
            <?php foreach ($categories as $category) { ?>
                <li>
                    <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>

                    <?php if (!empty($category['children'])) { ?>
                        <ul>
                            <?php foreach ($category['children'] as $child) { ?>
                                <li>
                                    <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    <?php }?>

    <div id="second_footer">
        <div id="powered1">
            <?php if($this->config->get('tg_helios_paymentimages_status') == '1') { ?>
                <div id="paymentimage">
                    <?php if (unserialize($this->config->get('tg_helios_paymentimages_slide_image'))) {?>
                        <?php foreach( unserialize($this->config->get('tg_helios_paymentimages_slide_image')) as $image){ ?>
                            <?php if ($image['url']) {?>
                                <span style="margin-left:14px;"><a href="<?php echo $image['url'];?>" target="_blank"><img src="<?php echo HTTPS_SERVER . 'image/' . $image['file'];?>" alt="" /></a></span>
                            <?php } else { ?>
                                <span style="margin-left:14px;"><img src="<?php echo HTTPS_SERVER . 'image/' . $image['file'];?>" alt="" /></span>
                            <?php } ?>
                        <?php }?>
                    <?php } ?>
                </div>
            <?php }; ?>
        </div>
    </div>
<!--</div>-->


<?php if($this->config->get('sidebar_code_status') == '1') { ?>
        <?php echo $sidebar_code; ?>
<?php } ?>

<?php

?>

<?php if(isset($megjelenit_altalanos['cookie_figyelmeztetes']) && $megjelenit_altalanos['cookie_figyelmeztetes'] == 1 && !isset($_COOKIE[$cookie_domain.'_accept_cookies'])){ ?>
    <div id="cookie_figyelmeztetes">
    <div>
        <div>
            <?php echo $text_cookie_figyelmeztetes; ?>
        </div>
        <div id="cookie_figyelmeztetes_bezar" onclick="cookiefigyelmeztetBezar('<?php echo $cookie_domain; ?>');">
        Bezár
        </div>
    </div>
</div>
<?php } ?>

<?php if(isset($footer_popup) && $footer_popup){ ?>
    <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/exitpopup.tpl')) {
        include_once(DIR_TEMPLATE. $this->config->get('config_template') . '/template/exitpopup.tpl');
    } else {
        include_once(DIR_TEMPLATE.'default/template/common/exitpopup.tpl');
    } ?>
<?php } ?>

</body></html>


