<script>

    key_escape = false;
    aktualis_kivalaztott = '';
    eredet_ertek = '';
    aktualis_list_elem = '';

    $(document).ready(function(){
        uzenofal_torles = setTimeout(" $('.warning, .success').slideUp('slow')",5000);
    });

    $('.list_modify input, .list_modify select').focusout(function() {

        clearTimeout(uzenofal_torles);

        if (key_escape) {
            $('.warning, .success').slideUp('slow',function(){
                $('.success, .warning, .error').remove();
            });

            $(this).val(eredet_ertek);

            $(this).parent().slideUp("slow",function(){
                $(this).prev().css('display','block');
            });


        } else {
            if ($(this).prop('nodeName') == "SELECT") {
                var html = $(this).find('option:selected').html();

            } else {
                var html = this.value;

            }
            $(this).parent().prev().html(html);


            $(this).parent().slideUp("slow",function(){
                $(this).prev().css('display','block');

            });


            var eljaras = $(this).attr('modositas');
            var ertek   = this.value;

            if (eredet_ertek != this.value) {
                $.ajax({
                    url: 'index.php?route=<?php echo $modositas_route?>/modosit&token=<?php echo $token; ?>',
                    type: "post",
                    data: 'eljaras='+ eljaras+'&ertek='+ertek,

                    dataType: 'json',

                    success: function(json) {
                        $('.success, .warning, .error').remove();
                        if (json['success']) {
                            var html = '<div class="success" style="display: none;">'+json['success']+'</div>';
                            $('.breadcrumb').after(html);
                            $('.success').slideDown('slow');

                        }

                        if (json['error']) {
                            var html = '<div class="warning" style="display: none;">'+json['error']+'</div>';
                            $('.breadcrumb').after(html);
                            $('.warning').slideDown('slow');
                            $(aktualis_kivalaztott).val(eredet_ertek);
                            $(aktualis_list_elem).html(aktualis_list_ertek);
                        }
                        uzenofal_torles = setTimeout(" $('.warning, .success').slideUp('slow')",5000);

                    },
                    error: function(e) {
                    }


                });
            }
        }
    });

    function listElemValueModify(aktualis) {
        $('body').off('click');
        var td_width = $(aktualis).closest('td').width();
        debugger;
        if ($('.list_modify[style*="display: block"]').length == 0) {
            key_escape = false;
            $(aktualis).closest('td').width(td_width);

            $(aktualis).css('display','none');
            $('.list_modify').css('display','none');

            $(aktualis).next().slideDown('slow',function(){
                $(aktualis).next().children().focus();
            }).css("display","block");

            eredet_ertek = $(aktualis).next().children().val();
            aktualis_kivalaztott = $(aktualis).next().children();
            aktualis_list_elem = $(aktualis);
            aktualis_list_ertek = $(aktualis).html();

        } else {
            $('.list_modify').slideUp('slow',function(){
                $(this).prev().css('display','block');
            });
        }
    }

    //$(document).delegate('body','click', function() {

        $('body').on('click', function(e){
        //alert('click');
        var element_neve = $(e.target).prop('nodeName');
        if (element_neve != "SELECT" && element_neve != "OPTION" && element_neve != "INPUT" && $(e.target).attr('class') != 'list_ertek') {
            $('.list_modify').slideUp('slow',function(){
                $(this).prev().css('display','block');
            });
        }
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            key_escape = true;
            $(aktualis_kivalaztott).blur();

        } else if (e.keyCode == 13) {
            key_escape = false;
            $(aktualis_kivalaztott).blur();
        }
    });

    Number.prototype.format = function(n, x, s, c) {
        if(n == undefined) {
            n = 0;
        }
        if(s == undefined) {
            s = '.';
        }
        if(c == undefined) {
            c = ',';
        }
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };

    function number_format(number, n, x, s, c) {
        return number.format(n, x, s, c);
    }

    function ajxMentes(feldolgozo_php,location_php,id) {

        if (typeof(id) == 'undefined') {
            id = 'product_id';
        }

        var height = $("body").height();
        var html = '';
        html += '<div class="takaro" style="height: '+height+'px">';
        html += '<img class="wait" src="view/image/loading2.gif" alt="" />';
        html += '</div>';
        $("body").append(html);

        if (typeof(CKEDITOR) != "undefined") {
            for (instance in CKEDITOR.instances)
                CKEDITOR.instances[instance].updateElement();
        }

        var utolso = false;
        var elso = true;
        var datatombatad = $('#form').serialize().split('&');

        if (datatombatad.length){
            var i = 0;

            while(true) {
                if (i > datatombatad.length) {
                    break;
                }
                var atkuld = '';
                var ideiglenes = new Array();
                for (var l=i; l<i+800; l++) {
                    if (l > datatombatad.length) {
                        break;
                    }
                    ideiglenes.push(datatombatad[l]);
                }
                var atkuld = ideiglenes.join('&');

                ajaxHivas(atkuld,utolso,elso,feldolgozo_php,id);
                elso = false;
                i = i+800;
            }
        }

        ajaxHivas('',true,false,feldolgozo_php,id);
        $(".takaro").remove();

        location = location_php+'&token=<?php echo $token?>';
    }

    function ajaxHivas(tomb,utolso,elso,feldolgozo_php,id) {
        var vissza = true;
        var url = '';

        if (id == 'product_id') {
            url = feldolgozo_php + '&token=<?php echo $token; ?>&product_id=<?php echo isset($this->request->get['product_id']) ? $this->request->get['product_id'] : ''?>&utolso=' + utolso + '&elso=' + elso;
        } else if (id == 'termek_integracio_id') {
            url = feldolgozo_php + '&token=<?php echo $token; ?>&termek_integracio_id=<?php echo isset($this->request->get['termek_integracio_id']) ? $this->request->get['termek_integracio_id'] : ''?>&utolso=' + utolso + '&elso=' + elso;;
        }
        debugger;

        $.ajax({
            url: url,
            type: "post",
            data: tomb,
            dataType: 'json',
            async: false,

            success: function(json) {
                vissza = true;
            },
            error: function(e) {
                vissza = false;
            }
        });
        return vissza;
    }


</script>