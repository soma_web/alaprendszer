<?php  echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
    <div class="heading">
        <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="buttons">
            <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
            <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
        </div>
    </div>
    <div class="content">
    <div id="tabs" class="htabs">
        <a href="#tab-frontend-top"><?php echo $tab_frontend_top; ?></a>
        <a href="#tab-backend-top"><?php echo $tab_backend_top; ?></a>
    </div>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">


    <div id="tab-frontend-top">

        <div id="ttab" class="htabs">
            <a href="#ttab-altalanos"><?php echo $tab_altalanos; ?></a>
            <a href="#ttab-regisztracio"><?php echo $tab_regisztracio; ?></a>
            <a href="#ttab-data"><?php echo $tab_view; ?></a>
            <a href="#ttab-product"><?php echo $tab_product; ?></a>
            <a href="#ttab-kosar"><?php echo $tab_kosar; ?></a>
            <a href="#ttab-email"><?php echo $tab_email; ?></a>
            <a href="#ttab-kereso"><?php echo "Keresés"; ?></a>
            <a href="#ttab-arajanlat"><?php echo "Árajánlat kérés"; ?></a>
        </div>


        <div id="ttab-altalanos">



        <table class="form">
        <thead>
        <tr>
            <td>Képekkel kapcsolatos beállítások
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td title="megjelenit_vizjel"><?php echo $entry_vizjel; ?></td>
            <td><?php if ($megjelenit_vizjel) { ?>
                    <input type="radio" name="megjelenit_vizjel" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_vizjel" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_vizjel" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_vizjel" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_altalanos[no_kep_tiltas]" style="width: 244px;"><?php echo $entry_nokep_megjelenites_tiltasa; ?></td>
            <td style="width: 144px;">
                <?php if ($no_kep_tiltas['no_kep_tiltas']) { ?>
                    <input type="radio" name="megjelenit_altalanos[no_kep_tiltas]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[no_kep_tiltas]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[no_kep_tiltas]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[no_kep_tiltas]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_altalanos[megjelenit_popup_eredeti_meret]" style="width: 244px;"><?php echo $entry_megjelenit_popup_eredeti_meret; ?></td>
            <td style="width: 144px;">
                <?php if ($megjelenit_popup_eredeti_meret) { ?>
                    <input type="radio" name="megjelenit_altalanos[megjelenit_popup_eredeti_meret]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[megjelenit_popup_eredeti_meret]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[megjelenit_popup_eredeti_meret]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[megjelenit_popup_eredeti_meret]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        </tbody>

        <thead>
        <tr>
            <td>Kategóriákkal kapcsolatos beállítások
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td title="megjelenit_category_description"><?php echo $entry_category_leiras; ?></td>
            <td><?php if ($megjelenit_category_description) { ?>
                    <input type="radio" name="megjelenit_category_description" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_category_description" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_category_description" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_category_description" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_category_header_image"><?php echo $entry_category_kep_fejlecben; ?></td>
            <td><?php if ($megjelenit_category_header_image) { ?>
                    <input type="radio" name="megjelenit_category_header_image" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_category_header_image" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_category_header_image" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_category_header_image" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_category_header_image_above"><?php echo $entry_category_kep_fejlec_felett; ?></td>
            <td><?php if ($megjelenit_category_header_image_above) { ?>
                    <input type="radio" name="megjelenit_category_header_image_above" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_category_header_image_above" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_category_header_image_above" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_category_header_image_above" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_category_header_null"><?php echo $entry_category_null_fejlecben; ?></td>
            <td><?php if ($megjelenit_category_header_null) { ?>
                    <input type="radio" name="megjelenit_category_header_null" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_category_header_null" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_category_header_null" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_category_header_null" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_footer_kategoria"><?php echo $entry_footer_kategoria; ?></td>
            <td><?php if ($megjelenit_footer_kategoria) { ?>
                    <input type="radio" name="megjelenit_footer_kategoria" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_footer_kategoria" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_footer_kategoria" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_footer_kategoria" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        </tbody>

        <tr>
            <td title="megjelenit_slideshow"><?php echo $entry_slideshow; ?></td>
            <td><?php if ($megjelenit_slideshow) { ?>
                    <input type="radio" name="megjelenit_slideshow" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_slideshow" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_slideshow" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_slideshow" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_jobb_egerbolt"><?php echo $entry_jobb_egerbolt; ?></td>
            <td><?php if ($megjelenit_jobb_egerbolt) { ?>
                    <input type="radio" name="megjelenit_jobb_egerbolt" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_jobb_egerbolt" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_jobb_egerbolt" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_jobb_egerbolt" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_masolhato"><?php echo $entry_masolhato; ?></td>
            <td><?php if ($megjelenit_masolhato) { ?>
                    <input type="radio" name="megjelenit_masolhato" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_masolhato" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_masolhato" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_masolhato" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

            <td><?php if ($megjelenit_piacter) { ?>
                    <input type="radio" name="megjelenit_piacter" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_piacter" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_piacter" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_piacter" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>





        <tr>
            <td title="megjelenit_felul_lapszamozas"><?php echo $entry_pagination; ?></td>
            <td><?php if ($megjelenit_felul_lapszamozas) { ?>
                    <input type="radio" name="megjelenit_felul_lapszamozas" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_felul_lapszamozas" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_felul_lapszamozas" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_felul_lapszamozas" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_regisztracio_szukitett"><?php echo $entry_szukitett_regisztracio; ?></td>
            <td><?php if ($megjelenit_regisztracio_szukitett) { ?>
                    <input type="radio" name="megjelenit_regisztracio_szukitett" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_regisztracio_szukitett" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_regisztracio_szukitett" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_regisztracio_szukitett" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_vonalkod_kuldes"><?php echo $entry_vonalkod_kuldes; ?></td>
            <td><?php if ($megjelenit_vonalkod_kuldes) { ?>
                    <input type="radio" name="megjelenit_vonalkod_kuldes" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_vonalkod_kuldes" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_vonalkod_kuldes" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_vonalkod_kuldes" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_netto_ujsor"><?php echo $entry_netto_ujsor; ?></td>
            <td><?php if ($megjelenit_netto_ujsor) { ?>
                    <input type="radio" name="megjelenit_netto_ujsor" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_netto_ujsor" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_netto_ujsor" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_netto_ujsor" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_utalvany_szerint_rendez"><?php echo $entry_utalvany_sorrendben; ?></td>
            <td><?php if ($megjelenit_utalvany_szerint_rendez) { ?>
                    <input type="radio" name="megjelenit_utalvany_szerint_rendez" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_utalvany_szerint_rendez" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_utalvany_szerint_rendez" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_utalvany_szerint_rendez" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_utalvany_szerint_rendez_ask"><?php echo $entry_utalvany_sorrendben_asc; ?></td>
            <td><?php if ($megjelenit_utalvany_szerint_rendez_ask) { ?>
                    <input type="radio" name="megjelenit_utalvany_szerint_rendez_ask" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_utalvany_szerint_rendez_ask" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_utalvany_szerint_rendez_ask" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_utalvany_szerint_rendez_ask" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_altalanos[fixmenu]" style="width: 244px;"><?php echo $entry_fixmenu; ?></td>
            <td style="width: 144px;">
                <?php if ($fixmenu['fixmenu']) { ?>
                    <input type="radio" name="megjelenit_altalanos[fixmenu]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[fixmenu]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[fixmenu]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[fixmenu]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_altalanos[fixfilter]" style="width: 244px;"><?php echo $entry_fixfilter; ?></td>
            <td style="width: 144px;">
                <?php if ($fixfilter['fixfilter']) { ?>
                    <input type="radio" name="megjelenit_altalanos[fixfilter]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[fixfilter]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[fixfilter]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[fixfilter]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_altalanos[mennyisegre_szur]" style="width: 244px;"><?php echo $entry_mennyisegre_szur; ?></td>
            <td style="width: 144px;">
                <?php if ($mennyisegre_szur['mennyisegre_szur']) { ?>
                    <input type="radio" name="megjelenit_altalanos[mennyisegre_szur]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[mennyisegre_szur]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[mennyisegre_szur]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[mennyisegre_szur]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>



        <tr>
            <td title="megjelenit_altalanos[ervenesseg_datumra_szur]" style="width: 244px;"><?php echo $entry_ervenesseg_datumra_szur; ?></td>
            <td style="width: 144px;">
                <?php if ($ervenesseg_datumra_szur['ervenesseg_datumra_szur']) { ?>
                    <input type="radio" name="megjelenit_altalanos[ervenesseg_datumra_szur]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[ervenesseg_datumra_szur]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[ervenesseg_datumra_szur]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[ervenesseg_datumra_szur]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_altalanos[emlekezz_ram_login]" style="width: 244px;"><?php echo $entry_emlekezz_ram; ?></td>
            <td style="width: 144px;">
                <?php if ($emlekezz_ram_login) { ?>
                    <input type="radio" name="megjelenit_altalanos[emlekezz_ram_login]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[emlekezz_ram_login]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[emlekezz_ram_login]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[emlekezz_ram_login]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_altalanos[facebook_login_fiok]" style="width: 244px;"><?php echo $entry_facebook_login_fiok; ?></td>
            <td style="width: 144px;">
                <?php if ($facebook_login_fiok) { ?>
                    <input type="radio" name="megjelenit_altalanos[facebook_login_fiok]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[facebook_login_fiok]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[facebook_login_fiok]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[facebook_login_fiok]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_altalanos[facebook_login_penztar]" style="width: 244px;"><?php echo $entry_facebook_login_penztar; ?></td>
            <td style="width: 144px;">
                <?php if ($facebook_login_penztar) { ?>
                    <input type="radio" name="megjelenit_altalanos[facebook_login_penztar]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[facebook_login_penztar]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[facebook_login_penztar]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[facebook_login_penztar]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_altalanos[cookie_figyelmeztetes]" style="width: 244px;"><?php echo $entry_cookie_figyelmeztetes; ?></td>
            <td style="width: 144px;">
                <?php if ($cookie_figyelmeztetes) { ?>
                    <input type="radio" name="megjelenit_altalanos[cookie_figyelmeztetes]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[cookie_figyelmeztetes]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[cookie_figyelmeztetes]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[cookie_figyelmeztetes]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_altalanos[penznem_hosszu_kiiras]" style="width: 244px;"><?php echo $entry_penznem_kiiras; ?></td>
            <td style="width: 144px;">
                <?php if ($penznem_hosszu_kiiras) { ?>
                    <input type="radio" name="megjelenit_altalanos[penznem_hosszu_kiiras]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[penznem_hosszu_kiiras]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_altalanos[penznem_hosszu_kiiras]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_altalanos[penznem_hosszu_kiiras]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>


            <tr>
                <td title="megjelenit_altalanos[footer_popup]" style="width: 244px;"><?php echo $entry_footer_popup; ?></td>
                <td style="width: 144px;">
                    <?php if ($footer_popup) { ?>
                        <input type="radio" name="megjelenit_altalanos[footer_popup]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_altalanos[footer_popup]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_altalanos[footer_popup]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_altalanos[footer_popup]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_altalanos[sebesseg_nyomkovetes]" style="width: 244px;"><?php echo "Oldal sebesség vizsgálata: (microtime.txt)"; ?></td>
                <td style="width: 144px;">
                    <?php if ($sebesseg_nyomkovetes) { ?>
                        <input type="radio" name="megjelenit_altalanos[sebesseg_nyomkovetes]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_altalanos[sebesseg_nyomkovetes]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_altalanos[sebesseg_nyomkovetes]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_altalanos[sebesseg_nyomkovetes]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_altalanos_cache" style="width: 244px;"><?php echo "Gyorsítás (CACHE) engedélyezése:"; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_altalanos_cache) { ?>
                        <input type="radio" name="megjelenit_altalanos_cache" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_altalanos_cache" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_altalanos_cache" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_altalanos_cache" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

        </table>



        </div>


        <div id="ttab-regisztracio">
            <div id="fttab" class="htabs">
                <a href="#fttab-megjelenit_regisztracio"><?php echo $tab_regisztracio; ?></a>
                <a href="#fttab-megjelenit_regisztracio_ceges"><?php echo $tab_regisztracio_ceges; ?></a>
                <a href="#fttab-megjelenit_regisztracio_elolvastam"><?php echo $tab_regisztracio_elolvastam; ?></a>
            </div>



            <table class="form" id="fttab-megjelenit_regisztracio_elolvastam">

                <tr>
                    <td><div class="scrollbox">
                            <?php $class = 'odd'; ?>
                            <?php foreach ($information_pages as $information_page) { ?>
                                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <div class="<?php echo $class; ?>">
                                    <?php $talalt = false;?>
                                    <?php if(isset($megjelenit_elolvastam_informaciok)) {
                                        foreach ($megjelenit_elolvastam_informaciok as $informaciok){
                                            if ($informaciok['information_id'] == $information_page['information_id']) {
                                                $talalt = true;
                                                break;
                                            }
                                        }
                                    }?>
                                    <?php if ($talalt) { ?>
                                        <input type="checkbox" name="megjelenit_elolvastam_informaciok[<?php echo $information_page['information_id']; ?>][information_id]" value="<?php echo $information_page['information_id']; ?>" checked="checked" />
                                        <?php echo " ".$entry_sort_order; ?>
                                        <input type="text" name="megjelenit_elolvastam_informaciok[<?php echo $information_page['information_id']; ?>][sort_order]" value="<?php echo $informaciok['sort_order']; ?>" size="2"/>
                                        <?php echo " ".$information_page['title']; ?>
                                    <?php } else { ?>
                                        <input type="checkbox" name="megjelenit_elolvastam_informaciok[<?php echo $information_page['information_id']; ?>][information_id]" value="<?php echo $information_page['information_id']; ?>" />
                                        <?php echo " ".$entry_sort_order; ?>
                                        <input type="text" name="megjelenit_elolvastam_informaciok[<?php echo $information_page['information_id']; ?>][sort_order]" value="0" size="2"/>
                                        <?php echo " ".$information_page['title']; ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        </br>
                        <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
                    </br>


                    <td class="left">
                        <?php foreach ($languages as $language) { ?>

                            <textarea name="megjelenit_elolvastam[<?php echo $language['language_id']; ?>]" cols="40" rows="4"
                                      id="megjelenit_elolvastam<?php echo $language['language_id']; ?>"><?php echo isset( $megjelenit_elolvastam[$language['language_id']] ) ? $megjelenit_elolvastam[$language['language_id']] : ''; ?></textarea>
                            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                        <?php } ?>
                    </td>
                </tr>


            </table>

            <table class="form" id="fttab-megjelenit_regisztracio">
                <tr>
                    <td><b><?php echo $entry_registration_default; ?></b></td>
                    <td><input type="checkbox" name="registration_default" value="1" id="registration_default" /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracioblokk_szemelyes"><b><?php echo $entry_registerblock_szemelyes; ?></b></td>
                    <td><?php if ($megjelenit_regisztracioblokk_szemelyes) { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_szemelyes" class="igen"  value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_szemelyes" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_szemelyes" class="igen"  value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_szemelyes" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_vezeteknev"><?php echo $entry_register_vezeteknev; ?></td>
                    <td><?php if ($megjelenit_regisztracio_vezeteknev) { ?>
                            <input type="radio" name="megjelenit_regisztracio_vezeteknev" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_vezeteknev" class="nem" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_vezeteknev" class="igen"  value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_vezeteknev" class="nem" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_keresztnev"><?php echo $entry_register_keresztnev; ?></td>
                    <td><?php if ($megjelenit_regisztracio_keresztnev) { ?>
                            <input type="radio" name="megjelenit_regisztracio_keresztnev" class="igen"  value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_keresztnev" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_keresztnev" class="igen"  value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_keresztnev" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_cegnev"><?php echo $entry_register_cegnev; ?></td>
                    <td><?php if ($megjelenit_regisztracio_cegnev) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cegnev"  value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cegnev" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cegnev"  value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cegnev" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>

                    <td title="megjelenit_regisztracio_cegnev_kotelezo">Kötelező: </td>
                    <td><?php if ($megjelenit_regisztracio_cegnev_kotelezo) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cegnev_kotelezo"  value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cegnev_kotelezo" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cegnev_kotelezo"  value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cegnev_kotelezo" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>


                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_adoszam"><?php echo $entry_register_adoszam; ?></td>
                    <td><?php if ($megjelenit_regisztracio_adoszam) { ?>
                            <input type="radio" name="megjelenit_regisztracio_adoszam" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_adoszam" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_adoszam" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_adoszam" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                    <td title="megjelenit_regisztracio_adoszam_kotelezo">Kötelező: </td>
                    <td><?php if ($megjelenit_regisztracio_adoszam_kotelezo) { ?>
                            <input type="radio" name="megjelenit_regisztracio_adoszam_kotelezo"  value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_adoszam_kotelezo" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_adoszam_kotelezo"  value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_adoszam_kotelezo" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_vallalkozasi_forma"><?php echo $entry_register_vallalkozasi_forma; ?></td>
                    <td><?php if ($megjelenit_regisztracio_vallalkozasi_forma) { ?>
                            <input type="radio" name="megjelenit_regisztracio_vallalkozasi_forma" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_vallalkozasi_forma" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_vallalkozasi_forma" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_vallalkozasi_forma" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                    <td title="megjelenit_regisztracio_vallalkozasi_forma_kotelezo">Kötelező: </td>
                    <td><?php if ($megjelenit_regisztracio_vallalkozasi_forma_kotelezo) { ?>
                            <input type="radio" name="megjelenit_regisztracio_vallalkozasi_forma_kotelezo" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_vallalkozasi_forma_kotelezo" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_vallalkozasi_forma_kotelezo" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_vallalkozasi_forma_kotelezo" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_szekhely"><?php echo $entry_register_szekhely; ?></td>
                    <td><?php if ($megjelenit_regisztracio_szekhely) { ?>
                            <input type="radio" name="megjelenit_regisztracio_szekhely" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_szekhely" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_szekhely" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_szekhely" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                    <td title="megjelenit_regisztracio_szekhely_kotelezo">Kötelező: </td>
                    <td><?php if ($megjelenit_regisztracio_szekhely_kotelezo) { ?>
                            <input type="radio" name="megjelenit_regisztracio_szekhely_kotelezo" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_szekhely_kotelezo" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_szekhely_kotelezo" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_szekhely_kotelezo" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ugyvezeto_neve"><?php echo $entry_register_ugyvezeto_neve; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ugyvezeto_neve) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_neve" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_neve" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_neve" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_neve" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                    <td title="megjelenit_regisztracio_ugyvezeto_neve_kotelezo">Kötelező: </td>
                    <td><?php if ($megjelenit_regisztracio_ugyvezeto_neve_kotelezo) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_neve_kotelezo" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_neve_kotelezo" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_neve_kotelezo" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_neve_kotelezo" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ugyvezeto_telefonszama"><?php echo $entry_register_ugyvezeto_telefonszama; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ugyvezeto_telefonszama) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_telefonszama" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_telefonszama" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_telefonszama" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_telefonszama" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                    <td title="megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo">Kötelező: </td>
                    <td><?php if ($megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_nem_regisztracio"><?php echo $entry_nem_regisztracio; ?></td>
                    <td><?php if ($megjelenit_nem_regisztracio) { ?>
                            <input type="radio" name="megjelenit_nem_regisztracio" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_nem_regisztracio" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_nem_regisztracio" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_nem_regisztracio" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_eletkor_regisztracio"><?php echo $entry_eletkor_regisztracio; ?></td>
                    <td><?php if ($megjelenit_eletkor_regisztracio) { ?>
                            <input type="radio" name="megjelenit_eletkor_regisztracio" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_eletkor_regisztracio" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_eletkor_regisztracio" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_eletkor_regisztracio" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_email"><?php echo $entry_register_email; ?></td>
                    <td><?php if ($megjelenit_regisztracio_email) { ?>
                            <input type="radio" name="megjelenit_regisztracio_email" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_email" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_email" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_email" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_email_megerosites"><?php echo $entry_register_email_megerosites; ?></td>
                    <td><?php if ($megjelenit_regisztracio_email_megerosites) { ?>
                            <input type="radio" name="megjelenit_regisztracio_email_megerosites" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_email_megerosites" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_email_megerosites" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_email_megerosites" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_telefon"><?php echo $entry_register_telefon; ?></td>
                    <td><?php if ($megjelenit_regisztracio_telefon) { ?>
                            <input type="radio" name="megjelenit_regisztracio_telefon" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_telefon" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_telefon" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_telefon" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_fax"><?php echo $entry_register_fax; ?></td>
                    <td><?php if ($megjelenit_regisztracio_fax) { ?>
                            <input type="radio" name="megjelenit_regisztracio_fax" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_fax" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_fax" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_fax" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_iskolai_vegzettseg"><?php echo $entry_register_iskolai_vegzettseg; ?></td>
                    <td><?php if ($megjelenit_regisztracio_iskolai_vegzettseg) { ?>
                            <input type="radio" name="megjelenit_regisztracio_iskolai_vegzettseg" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_iskolai_vegzettseg" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_iskolai_vegzettseg" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_iskolai_vegzettseg" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_weblap">Weblap URL</td>
                    <td>
                        <input type="radio" name="megjelenit_regisztracio_weblap" value="1" <?php echo !empty($megjelenit_regisztracio_weblap)? 'checked="checked"' : ''?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_regisztracio_weblap" value="0" <?php echo empty($megjelenit_regisztracio_weblap)? 'checked="checked"' : ''?> />
                        <?php echo $text_no; ?>

                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracioblokk_cim"><b><?php echo $entry_registerblock_cim; ?></b></td>
                    <td><?php if ($megjelenit_regisztracioblokk_cim) { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_cim" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_cim" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_cim" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_cim" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_cimadat_cegnev"><?php echo $entry_register_address_cegnev; ?></td>
                    <td><?php if ($megjelenit_regisztracio_cimadat_cegnev) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_cegnev" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_cegnev" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_cegnev" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_cegnev" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_cimadat_adoszam"><?php echo $entry_register_address_adoszam; ?></td>
                    <td><?php if ($megjelenit_regisztracio_cimadat_adoszam) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_adoszam" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_adoszam" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_adoszam" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_adoszam" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_cimadat_vallalkozasi_forma"><?php echo $entry_register_address_vallalkozasi_forma; ?></td>
                    <td><?php if ($megjelenit_regisztracio_cimadat_vallalkozasi_forma) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_vallalkozasi_forma" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_vallalkozasi_forma" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_vallalkozasi_forma" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_vallalkozasi_forma" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_cimadat_szekhely"><?php echo $entry_register_address_szekhely; ?></td>
                    <td><?php if ($megjelenit_regisztracio_cimadat_szekhely) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_szekhely" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_szekhely" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_szekhely" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_szekhely" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_cimadat_ugyvezeto_neve"><?php echo $entry_register_address_ugyvezeto_neve; ?></td>
                    <td><?php if ($megjelenit_regisztracio_cimadat_ugyvezeto_neve) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_ugyvezeto_neve" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_ugyvezeto_neve" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_ugyvezeto_neve" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_ugyvezeto_neve" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama"><?php echo $entry_register_address_ugyvezeto_telefonszama; ?></td>
                    <td><?php if ($megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_ugyvezeto_telefonszama" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_utca"><?php echo $entry_register_address_street; ?></td>
                    <td><?php if ($megjelenit_regisztracio_utca) { ?>
                            <input type="radio" name="megjelenit_regisztracio_utca" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_utca" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_utca" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_utca" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_cimadat_kiegeszito"><?php echo $entry_register_address_kiegeszito; ?></td>
                    <td><?php if ($megjelenit_regisztracio_cimadat_kiegeszito) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_kiegeszito" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_kiegeszito" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_kiegeszito" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimadat_kiegeszito" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_varos"><?php echo $entry_register_address_city; ?></td>
                    <td><?php if ($megjelenit_regisztracio_varos) { ?>
                            <input type="radio" name="megjelenit_regisztracio_varos" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_varos" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_varos" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_varos" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_iranyitoszam"><?php echo $entry_register_address_postcode; ?></td>
                    <td><?php if ($megjelenit_regisztracio_iranyitoszam) { ?>
                            <input type="radio" name="megjelenit_regisztracio_iranyitoszam" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_iranyitoszam" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_iranyitoszam" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_iranyitoszam" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_orszag"><?php echo $entry_register_address_country; ?></td>
                    <td><?php if ($megjelenit_regisztracio_orszag) { ?>
                            <input type="radio" name="megjelenit_regisztracio_orszag" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_orszag" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_orszag" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_orszag" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_megye"><?php echo $entry_register_address_region; ?></td>
                    <td><?php if ($megjelenit_regisztracio_megye) { ?>
                            <input type="radio" name="megjelenit_regisztracio_megye" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_megye" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_megye" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_megye" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracioblokk_paypal"><b><?php echo $entry_registerblock_paypal; ?></b></td>
                    <td><?php if ($megjelenit_regisztracioblokk_paypal) { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_paypal" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_paypal" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_paypal" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_paypal" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_paypal"><?php echo $entry_register_paypal; ?></td>
                    <td><?php if ($megjelenit_regisztracio_paypal) { ?>
                            <input type="radio" name="megjelenit_regisztracio_paypal" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_paypal" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_paypal" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_paypal" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracioblokk_hirlevel"><b><?php echo $entry_registerblock_hirlevel; ?></b></td>
                    <td><?php if ($megjelenit_regisztracioblokk_hirlevel) { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_hirlevel" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_hirlevel" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_hirlevel" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_hirlevel" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_hirlevel_kategoriak"><?php echo $entry_register_hirlevel_kategoriak; ?></td>
                    <td><?php if ($megjelenit_regisztracio_hirlevel_kategoriak) { ?>
                            <input type="radio" name="megjelenit_regisztracio_hirlevel_kategoriak" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_hirlevel_kategoriak" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_hirlevel_kategoriak" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_hirlevel_kategoriak" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>

                <tr>
                    <td><b>Regisztrációs email</b></td>
                    <td><br /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracios_email[jelszo]"><?php echo $entry_regisztracios_email_jelszo; ?></td>
                    <td><?php if (isset($regisztracios_email['jelszo']) && $regisztracios_email['jelszo']) { ?>
                            <input type="radio" name="megjelenit_regisztracios_email[jelszo]" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracios_email[jelszo]" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracios_email[jelszo]" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracios_email[jelszo]" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>

                <tr>
                    <td><b>Beállítások</b></td>
                    <td><br /></td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben"><?php echo $entry_cimmodositas_a_szerkeztesben; ?></td>
                    <td><?php if ($megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben) { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_cimmodositas_a_fiok_szerkeztesben" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
            </table>


            <table class="form" id="fttab-megjelenit_regisztracio_ceges">

                <tr>
                    <td><b><?php echo $entry_registration_company; ?></b></td>
                    <td><input type="checkbox" name="registration_company" value="1" id="registration_company" /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracioblokk_ceges_szemelyes"><b><?php echo $entry_registerblock_szemelyes; ?></b></td>
                    <td><?php if ($megjelenit_regisztracioblokk_ceges_szemelyes) { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_szemelyes" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_szemelyes" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_szemelyes" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_szemelyes" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_ceges_vezeteknev"><?php echo $entry_register_vezeteknev; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_vezeteknev) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_vezeteknev" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_vezeteknev" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_vezeteknev" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_vezeteknev" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_keresztnev"><?php echo $entry_register_keresztnev; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_keresztnev) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_keresztnev" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_keresztnev" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_keresztnev" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_keresztnev" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_cegnev"><?php echo $entry_register_cegnev; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_cegnev) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cegnev" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cegnev" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cegnev" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cegnev" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_adoszam"><?php echo $entry_register_adoszam; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_adoszam) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_adoszam" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_adoszam" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_adoszam" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_adoszam" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_vallalkozasi_forma"><?php echo $entry_register_vallalkozasi_forma; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_vallalkozasi_forma) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_vallalkozasi_forma" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_vallalkozasi_forma" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_vallalkozasi_forma" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_vallalkozasi_forma" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_szekhely"><?php echo $entry_register_szekhely; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_szekhely) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_szekhely" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_szekhely" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_szekhely" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_szekhely" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_ugyvezeto_neve"><?php echo $entry_register_ugyvezeto_neve; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_ugyvezeto_neve) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_ugyvezeto_neve" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_ugyvezeto_neve" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_ugyvezeto_neve" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_ugyvezeto_neve" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_ugyvezeto_telefonszama"><?php echo $entry_register_ugyvezeto_telefonszama; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_ugyvezeto_telefonszama) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_ugyvezeto_telefonszama" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_ugyvezeto_telefonszama" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_ugyvezeto_telefonszama" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_ugyvezeto_telefonszama" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_nem_regisztracio_ceges"><?php echo $entry_nem_regisztracio; ?></td>
                    <td><?php if ($megjelenit_nem_regisztracio_ceges) { ?>
                            <input type="radio" name="megjelenit_nem_regisztracio_ceges" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_nem_regisztracio_ceges" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_nem_regisztracio_ceges" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_nem_regisztracio_ceges" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_eletkor_regisztracio_ceges"><?php echo $entry_eletkor_regisztracio; ?></td>
                    <td><?php if ($megjelenit_eletkor_regisztracio_ceges) { ?>
                            <input type="radio" name="megjelenit_eletkor_regisztracio_ceges" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_eletkor_regisztracio_ceges" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_eletkor_regisztracio_ceges" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_eletkor_regisztracio_ceges" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_ceges_email"><?php echo $entry_register_email; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_email) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_email" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_email" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_email" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_email" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_ceges_email_megerosites"><?php echo $entry_register_email_megerosites; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_email_megerosites) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_email_megerosites" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_email_megerosites" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_email_megerosites" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_email_megerosites" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_ceges_telefon"><?php echo $entry_register_telefon; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_telefon) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_telefon" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_telefon" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_telefon" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_telefon" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_ceges_fax"><?php echo $entry_register_fax; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_fax) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_fax" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_fax" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_fax" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_fax" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_ceges_iskolai_vegzettseg"><?php echo $entry_register_iskolai_vegzettseg; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_iskolai_vegzettseg) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_iskolai_vegzettseg" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_iskolai_vegzettseg" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_iskolai_vegzettseg" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_iskolai_vegzettseg" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracioblokk_ceges_cim"><b><?php echo $entry_registerblock_cim; ?></b></td>
                    <td><?php if ($megjelenit_regisztracioblokk_ceges_cim) { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_cim" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_cim" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_cim" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_cim" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_cimadat_cegnev"><?php echo $entry_register_address_cegnev; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_cimadat_cegnev) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_cegnev" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_cegnev" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_cegnev" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_cegnev" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_cimadat_adoszam"><?php echo $entry_register_address_adoszam; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_cimadat_adoszam) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_adoszam" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_adoszam" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_adoszam" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_adoszam" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma"><?php echo $entry_register_address_vallalkozasi_forma; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_vallalkozasi_forma" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_cimadat_szekhely"><?php echo $entry_register_address_szekhely; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_cimadat_szekhely) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_szekhely" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_szekhely" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_szekhely" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_szekhely" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve"><?php echo $entry_register_address_ugyvezeto_neve; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_neve" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama"><?php echo $entry_register_address_ugyvezeto_telefonszama; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_ugyvezeto_telefonszama" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_utca"><?php echo $entry_register_address_street; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_utca) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_utca" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_utca" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_utca" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_utca" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_cimadat_kiegeszito"><?php echo $entry_register_address_kiegeszito; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_cimadat_kiegeszito) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_kiegeszito" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_kiegeszito" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_kiegeszito" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimadat_kiegeszito" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_varos"><?php echo $entry_register_address_city; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_varos) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_varos" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_varos" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_varos" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_varos" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_iranyitoszam"><?php echo $entry_register_address_postcode; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_iranyitoszam) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_iranyitoszam" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_iranyitoszam" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_iranyitoszam" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_iranyitoszam" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_orszag"><?php echo $entry_register_address_country; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_orszag) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_orszag" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_orszag" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_orszag" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_orszag" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_megye"><?php echo $entry_register_address_region; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_megye) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_megye" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_megye" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_megye" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_megye" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracioblokk_ceges_paypal"><b><?php echo $entry_registerblock_paypal; ?></b></td>
                    <td><?php if ($megjelenit_regisztracioblokk_ceges_paypal) { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_paypal" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_paypal" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_paypal" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_paypal" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_paypal"><?php echo $entry_register_paypal; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_paypal) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_paypal" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_paypal" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_paypal" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_paypal" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracioblokk_ceges_hirlevel"><b><?php echo $entry_registerblock_hirlevel; ?></b></td>
                    <td><?php if ($megjelenit_regisztracioblokk_ceges_hirlevel) { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_hirlevel" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_hirlevel" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_hirlevel" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracioblokk_ceges_hirlevel" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracio_ceges_hirlevel_kategoriak"><?php echo $entry_register_hirlevel_kategoriak; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_hirlevel_kategoriak) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_hirlevel_kategoriak" class="igen" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_hirlevel_kategoriak" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_hirlevel_kategoriak" class="igen" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_hirlevel_kategoriak" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>

                <tr>
                    <td><b>Regisztrációs email</b></td>
                    <td><br /></td>
                </tr>
                <tr>
                    <td title="megjelenit_regisztracios_email[ceges_jelszo]"><?php echo $entry_regisztracios_email_jelszo; ?></td>
                    <td><?php if (isset($regisztracios_email['ceges_jelszo']) && $regisztracios_email['ceges_jelszo']) { ?>
                            <input type="radio" name="megjelenit_regisztracios_email[ceges_jelszo]" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracios_email[ceges_jelszo]" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracios_email[ceges_jelszo]" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracios_email[ceges_jelszo]" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td><br /></td>
                </tr>

                <tr>
                    <td><b>Beállítások</b></td>
                    <td><br /></td>
                </tr>

                <tr>
                    <td title="megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben"><?php echo $entry_cimmodositas_a_szerkeztesben; ?></td>
                    <td><?php if ($megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben) { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_regisztracio_ceges_cimmodositas_a_fiok_szerkeztesben" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
            </table>


        </div>


        <div id="ttab-data">

        <div id="fttab" class="htabs">
            <a href="#fttab-allitott"><?php echo $tab_allitott; ?></a>
            <!--<a href="#fttab-fektetett"><?php echo $tab_fektetett; ?></a>-->
            <a href="#fttab-lista"><?php echo $tab_lista; ?></a>
        </div>

        <table class="form1" id="fttab-allitott">

        <tr>
            <td title="megjelenit_allitott"><?php echo $entry_allitott; ?></td>
            <td colspan=3><?php if ($megjelenit_allitott) { ?>
                    <input type="radio" name="megjelenit_allitott" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_allitott" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_allitott" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_allitott" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_modularis"><?php echo "Moduláris megjelenítés"; ?></td>
            <td colspan=3><?php if ($megjelenit_modularis) { ?>
                    <input type="radio" name="megjelenit_modularis" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_modularis" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_modularis" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_modularis" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr><td><br></td></tr>


        <tr><td colspan=4>
        <table  class="form1">
        <tr>
            <td><b>Sablon neve</b></td>
            <td><b>Státusz</b></td>
            <td><b>Sor</b></td>
            <td><b>Oszlop</b></td>
        </tr>
        <tr><td><br></td></tr>

        <?php foreach($megjelenit_termek_doboz as $key=>$doboz) {?>
            <tr>
                <td><?php echo $key.".tpl";?></td>
                <td style="padding-right: 20px;">
                    <?php if ($doboz['status'] == 1) { ?>
                        <input type="radio" name="megjelenit_termek_doboz[<?php echo $key?>][status]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_termek_doboz[<?php echo $key?>][status]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_termek_doboz[<?php echo $key?>][status]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_termek_doboz[<?php echo $key?>][status]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>

                <td><input type="text" name="megjelenit_termek_doboz[<?php echo $key?>][sort_order_sor]" value="<?php echo $doboz['sort_order_sor']?>"        size=2 /></td>
                <td><input type="text" name="megjelenit_termek_doboz[<?php echo $key?>][sort_order_oszlop]" value="<?php echo $doboz['sort_order_oszlop']?>"  size=2 /></td>

                <?php if (isset($megjelenit_termek_doboz_beallitas[$key])) {?>
                    <?php $melyik_doboz = "megjelenit_termek_doboz"?>

                    <?php foreach ($megjelenit_termek_doboz_beallitas[$key] as $keydoboz=>$tomb) { ?>
                        <?php if(count($tomb) > 1) { ?>
                            <td>
                                <?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>
                            </td>

                        <?php } ?>
                    <?php } ?>
                <?php } ?>

            </tr>

        <?php }?>

        </table>
        </td></tr>
        <tr><td><br></td></tr>




        <tr>
            <td title="megjelenit_kosarba"><?php echo $entry_kosarba; ?></td>
            <td colspan=3>
                <?php if ($megjelenit_kosarba) { ?>
                    <input type="radio" name="megjelenit_kosarba" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kosarba" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_kosarba" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kosarba" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_mennyiseg"><?php echo $entry_mennyiseg; ?></td>
            <td colspan=3><?php if ($megjelenit_mennyiseg) { ?>
                    <input type="radio" name="megjelenit_mennyiseg" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_mennyiseg" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_mennyiseg" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_mennyiseg" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_csomagolasi_mennyiseg"><?php echo $entry_csomagolasi_mennyiseg; ?></td>
            <td colspan=3><?php if ($megjelenit_csomagolasi_mennyiseg) { ?>
                    <input type="radio" name="megjelenit_csomagolasi_mennyiseg" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_csomagolasi_mennyiseg" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_csomagolasi_mennyiseg" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_csomagolasi_mennyiseg" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_brutto_ar"><?php echo $entry_brutto_ar; ?></td>
            <td colspan=3><?php if ($megjelenit_brutto_ar) { ?>
                    <input type="radio" name="megjelenit_brutto_ar" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_brutto_ar" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_brutto_ar" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_brutto_ar" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_netto_ar"><?php echo $entry_netto_ar; ?></td>
            <td colspan=3><?php if ($megjelenit_netto_ar) { ?>
                    <input type="radio" name="megjelenit_netto_ar" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_netto_ar" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_netto_ar" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_netto_ar" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_negativ_ar"><?php echo $entry_negativ_ar; ?></td>
            <td colspan=3><?php if ($megjelenit_negativ_ar) { ?>
                    <input type="radio" name="megjelenit_negativ_ar" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_negativ_ar" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_negativ_ar" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_negativ_ar" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_brutto_netto"><?php echo $entry_brutto_netto; ?></td>
            <td colspan=3><?php if ($megjelenit_brutto_netto) { ?>
                    <input type="radio" name="megjelenit_brutto_netto" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_brutto_netto" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_brutto_netto" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_brutto_netto" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_description"><?php echo $entry_description; ?></td>
            <td style="padding-right: 30px;"><?php if ($megjelenit_description) { ?>
                    <input type="radio" name="megjelenit_description" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_description" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_description" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_description" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_description_karakterek"><?php echo $entry_description_karakter_szam; ?></td>
            <td><input type="text" name="megjelenit_description_karakterek" value="<?php echo $megjelenit_description_karakterek?>" size=2 /></td>
        </tr>

        <tr><td><br></td></tr>
        <tr>
            <td colspan="5"><b><?php echo $entry_ikonok; ?></b></td>
        </tr>

        <tr>
            <td><?php echo $entry_ikon_helye; ?></td>

            <td class="right" colspan="3">
                <select name="megjelenit_ikon_hely">
                    <?php if ($megjelenit_ikon_hely == 0) { ?>
                        <option value="0" selected="selected"><?php echo $ikon_fent_vizszintes; ?></option>
                        <option value="1"><?php echo $ikon_fent_fuggoleges; ?></option>
                        <option value="2"><?php echo $ikon_kep_felett; ?></option>
                        <option value="3"><?php echo $ikon_kep_mellett; ?></option>

                    <?php } elseif ($megjelenit_ikon_hely == 1) { ?>
                        <option value="0"><?php echo $ikon_fent_vizszintes; ?></option>
                        <option value="1" selected="selected"><?php echo $ikon_fent_fuggoleges; ?></option>
                        <option value="2"><?php echo $ikon_kep_felett; ?></option>
                        <option value="3"><?php echo $ikon_kep_mellett; ?></option>

                    <?php } elseif ($megjelenit_ikon_hely == 2) { ?>
                        <option value="0"><?php echo $ikon_fent_vizszintes; ?></option>
                        <option value="1"><?php echo $ikon_fent_fuggoleges; ?></option>
                        <option value="2" selected="selected"><?php echo $ikon_kep_felett; ?></option>
                        <option value="3"><?php echo $ikon_kep_mellett; ?></option>

                    <?php } elseif ($megjelenit_ikon_hely == 3) { ?>
                        <option value="0"><?php echo $ikon_fent_vizszintes; ?></option>
                        <option value="1"><?php echo $ikon_fent_fuggoleges; ?></option>
                        <option value="2"><?php echo $ikon_kep_felett; ?></option>
                        <option value="3" selected="selected"><?php echo $ikon_kep_mellett; ?></option>

                    <?php } ?>
                </select>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_uj"><?php echo $entry_uj; ?></td>
            <td colspan=3><?php if ($megjelenit_uj) { ?>
                    <input type="radio" name="megjelenit_uj" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_uj" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_uj" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_uj" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_pardarab"><?php echo $entry_pardarab; ?></td>
            <td colspan=3><?php if ($megjelenit_pardarab) { ?>
                    <input type="radio" name="megjelenit_pardarab" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_pardarab" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_pardarab" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_pardarab" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_szazalek"><?php echo $entry_szazalek; ?></td>
            <td colspan=3><?php if ($megjelenit_szazalek) { ?>
                    <input type="radio" name="megjelenit_szazalek" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_szazalek" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_szazalek" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_szazalek" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr><td><br></td>
        </tr>


        <tr>
            <td title="megjelenit_kep_szoveg"><?php echo $entry_kep_szoveg; ?></td>
            <td colspan=3><?php if ($megjelenit_kep_szoveg) { ?>
                    <input type="radio" name="megjelenit_kep_szoveg" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kep_szoveg" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_kep_szoveg" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kep_szoveg" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_kivansaglista"><?php echo $entry_kivansaglista; ?></td>
            <td colspan=3><?php if ($megjelenit_kivansaglista) { ?>
                    <input type="radio" name="megjelenit_kivansaglista" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kivansaglista" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_kivansaglista" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kivansaglista" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_osszehasonlitas"><?php echo $entry_osszehasonlitas; ?></td>
            <td colspan=3><?php if ($megjelenit_osszehasonlitas) { ?>
                    <input type="radio" name="megjelenit_osszehasonlitas" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_osszehasonlitas" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_osszehasonlitas" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_osszehasonlitas" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        </table>

        <!--<table class="form1" id="fttab-fektetett">

            <tr><td colspan=4>
                <table  class="form1">
                    <tr>
                        <td><b>Sablon neve</b></td>
                        <td><b>Státusz</b></td>
                        <td><b>Oszlop</b></td>
                        <td><b>Sor</b></td>
                    </tr>
                    <tr><td><br></td></tr>

                    <?php foreach($megjelenit_termek_fektetett as $key=>$doboz) {?>
                        <tr>
                            <td><?php echo $key.".tpl";?></td>
                            <td style="padding-right: 20px;">
                                <?php if ($doboz['status'] == 1) { ?>
                                    <input type="radio" name="megjelenit_termek_fektetett[<?php echo $key?>][status]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_termek_fektetett[<?php echo $key?>][status]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_termek_fektetett[<?php echo $key?>][status]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_termek_fektetett[<?php echo $key?>][status]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td><input type="text" name="megjelenit_termek_fektetett[<?php echo $key?>][sort_order_oszlop]" value="<?php echo $doboz['sort_order_oszlop']?>"  size=2 /></td>
                            <td><input type="text" name="megjelenit_termek_fektetett[<?php echo $key?>][sort_order_sor]" value="<?php echo $doboz['sort_order_sor']?>"        size=2 /></td>

                            <?php if (isset($megjelenit_termek_fektetett_beallitas[$key])) {?>
                                <?php $melyik_doboz = "megjelenit_termek_fektetett"?>
                                <?php foreach ($megjelenit_termek_fektetett_beallitas[$key] as $keydoboz=>$tomb) { ?>
                                    <?php if(count($tomb) > 1) { ?>
                                        <td>
                                            <?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>
                                        </td>

                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>

                        </tr>

                    <?php }?>

                </table>
            </td></tr>
        <tr><td><br></td></tr>



        <tr>
            <td title="megjelenit_kosarba_fk"><?php echo $entry_kosarba_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_kosarba_fk) { ?>
                    <input type="radio" name="megjelenit_kosarba_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kosarba_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_kosarba_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kosarba_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_minicart_fk"><?php echo $entry_minicart_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_minikosar_fk) { ?>
                    <input type="radio" name="megjelenit_minikosar_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_minikosar_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_minikosar_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_minikosar_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_megnezemgomb_fk"><?php echo $entry_megnezemgomb_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_megnezemgomb_fk) { ?>
                    <input type="radio" name="megjelenit_megnezemgomb_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_megnezemgomb_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_megnezemgomb_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_megnezemgomb_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_mennyiseg_fk"><?php echo $entry_mennyiseg_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_mennyiseg_fk) { ?>
                    <input type="radio" name="megjelenit_mennyiseg_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_mennyiseg_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_mennyiseg_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_mennyiseg_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_csomagolasi_mennyiseg_fk"><?php echo $entry_csomagolasi_mennyiseg_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_csomagolasi_mennyiseg_fk) { ?>
                    <input type="radio" name="megjelenit_csomagolasi_mennyiseg_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_csomagolasi_mennyiseg_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_csomagolasi_mennyiseg_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_csomagolasi_mennyiseg_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_brutto_ar_fk"><?php echo $entry_brutto_ar_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_brutto_ar_fk) { ?>
                    <input type="radio" name="megjelenit_brutto_ar_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_brutto_ar_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_brutto_ar_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_brutto_ar_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_netto_ar_fk"><?php echo $entry_netto_ar_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_netto_ar_fk) { ?>
                    <input type="radio" name="megjelenit_netto_ar_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_netto_ar_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_netto_ar_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_netto_ar_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_brutto_netto_fk"><?php echo $entry_brutto_netto_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_brutto_netto_fk) { ?>
                    <input type="radio" name="megjelenit_brutto_netto_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_brutto_netto_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_brutto_netto_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_brutto_netto_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_description_fk"><?php echo $entry_description_fk; ?></td>
            <td style="padding-right: 30px;"><?php if ($megjelenit_description_fk) { ?>
                    <input type="radio" name="megjelenit_description_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_description_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_description_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_description_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title=""><?php echo $entry_description_karakter_szam_fk; ?></td>
            <td><input type="text" name="megjelenit_description_karakterek_fk" value="<?php echo $megjelenit_description_karakterek_fk?>" size=2 /></td>
        </tr>

        <tr><td><br></td>
        </tr>
        <tr>
            <td colspan="5"><b><?php echo $entry_ikonok_fk; ?></b></td>
        </tr>

        <tr>
            <td title="megjelenit_uj_fk"><?php echo $entry_uj_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_uj_fk) { ?>
                    <input type="radio" name="megjelenit_uj_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_uj_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_uj_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_uj_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_pardarab_fk"><?php echo $entry_pardarab_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_pardarab_fk) { ?>
                    <input type="radio" name="megjelenit_pardarab_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_pardarab_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_pardarab_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_pardarab_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_szazalek_fk"fttab-fektete><?php echo $entry_szazalek_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_szazalek_fk) { ?>
                    <input type="radio" name="megjelenit_szazalek_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_szazalek_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_szazalek_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_szazalek_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr><td><br></td>
        </tr>


        <tr>
            <td title="megjelenit_kep_szoveg_fk"><?php echo $entry_kep_szoveg_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_kep_szoveg_fk) { ?>
                    <input type="radio" name="megjelenit_kep_szoveg_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kep_szoveg_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_kep_szoveg_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kep_szoveg_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_kivansaglista_fk"><?php echo $entry_kivansaglista_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_kivansaglista_fk) { ?>
                    <input type="radio" name="megjelenit_kivansaglista_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kivansaglista_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_kivansaglista_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kivansaglista_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_osszehasonlitas_fk"><?php echo $entry_osszehasonlitas_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_osszehasonlitas_fk) { ?>
                    <input type="radio" name="megjelenit_osszehasonlitas_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_osszehasonlitas_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_osszehasonlitas_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_osszehasonlitas_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_facebook_fk"><?php echo $entry_facebook_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_facebook_fk) { ?>
                    <input type="radio" name="megjelenit_facebook_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_facebook_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_facebook_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_facebook_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_email_fk"><?php echo $entry_email_fk; ?></td>
            <td colspan=3><?php if ($megjelenit_email_fk) { ?>
                    <input type="radio" name="megjelenit_email_fk" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_email_fk" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_email_fk" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_email_fk" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        </table>-->

        <table class="form1" id="fttab-lista">

        <tr>
            <td title="megjelenit_lista_modularis"><?php echo "Moduláris megjelenítés"; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_modularis) { ?>
                    <input type="radio" name="megjelenit_lista_modularis" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_modularis" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_modularis" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_modularis" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr><td><br></td></tr>


        <tr><td colspan=4>
                <table  class="form1">
                    <tr>
                        <td><b>Sablon neve</b></td>
                        <td><b>Státusz</b></td>
                        <td><b>Oszlop</b></td>
                        <td><b>Sor</b></td>
                    </tr>
                    <tr><td><br></td></tr>

                    <?php foreach($megjelenit_lista_termek_doboz as $key=>$doboz) {?>
                        <tr>
                            <td><?php echo $key.".tpl";?></td>
                            <td style="padding-right: 20px;">
                                <?php if ($doboz['status'] == 1) { ?>
                                    <input type="radio" name="megjelenit_lista_termek_doboz[<?php echo $key?>][status]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_lista_termek_doboz[<?php echo $key?>][status]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_lista_termek_doboz[<?php echo $key?>][status]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_lista_termek_doboz[<?php echo $key?>][status]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>

                            <td><input type="text" name="megjelenit_lista_termek_doboz[<?php echo $key?>][sort_order_oszlop]" value="<?php echo $doboz['sort_order_oszlop']?>"        size=2 /></td>
                            <td><input type="text" name="megjelenit_lista_termek_doboz[<?php echo $key?>][sort_order_sor]" value="<?php echo $doboz['sort_order_sor']?>"  size=2 /></td>

                            <?php if (isset($megjelenit_termek_lista_beallitas[$key])) {?>
                                <?php $melyik_doboz = "megjelenit_lista_termek_doboz"?>

                                <?php foreach ($megjelenit_termek_lista_beallitas[$key] as $keydoboz=>$tomb) { ?>
                                    <?php if(count($tomb) > 1) { ?>
                                        <td>
                                            <?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>
                                        </td>

                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>

                        </tr>

                    <?php }?>

                </table>
            </td></tr>
        <tr><td><br></td></tr>


        <tr>
            <td title="megjelenit_lista_kosarba"><?php echo $entry_lista_kosarba; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_kosarba) { ?>
                    <input type="radio" name="megjelenit_lista_kosarba" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_kosarba" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_kosarba" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_kosarba" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_lista_mennyiseg"><?php echo $entry_lista_mennyiseg; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_mennyiseg) { ?>
                    <input type="radio" name="megjelenit_lista_mennyiseg" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_mennyiseg" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_mennyiseg" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_mennyiseg" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_lista_csomagolasi_mennyiseg"><?php echo $entry_lista_csomagolasi_mennyiseg; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_csomagolasi_mennyiseg) { ?>
                    <input type="radio" name="megjelenit_lista_csomagolasi_mennyiseg" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_csomagolasi_mennyiseg" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_csomagolasi_mennyiseg" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_csomagolasi_mennyiseg" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_lista_brutto_ar"><?php echo $entry_lista_brutto_ar; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_brutto_ar) { ?>
                    <input type="radio" name="megjelenit_lista_brutto_ar" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_brutto_ar" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_brutto_ar" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_brutto_ar" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_lista_netto_ar"><?php echo $entry_lista_netto_ar; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_netto_ar) { ?>
                    <input type="radio" name="megjelenit_lista_netto_ar" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_netto_ar" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_netto_ar" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_netto_ar" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_lista_brutto_netto"><?php echo $entry_lista_brutto_netto; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_brutto_netto) { ?>
                    <input type="radio" name="megjelenit_lista_brutto_netto" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_brutto_netto" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_brutto_netto" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_brutto_netto" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_lista_description"><?php echo $entry_lista_description; ?></td>
            <td style="padding-right: 30px;"><?php if ($megjelenit_lista_description) { ?>
                    <input type="radio" name="megjelenit_lista_description" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_description" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_description" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_description" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>

            <td title="megjelenit_description_lista_karakterek"><?php echo $entry_description_karakter_szam; ?></td>
            <td><input type="text" name="megjelenit_description_lista_karakterek" value="<?php echo $megjelenit_description_lista_karakterek?>" size=2 /></td>
        </tr>


        <tr><td></td></tr>
        <tr>
            <td colspan="5"><b><?php echo $entry_ikonok; ?></b></td>
        </tr>
        <tr>
            <td title="megjelenit_lista_uj"><?php echo $entry_lista_uj; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_uj) { ?>
                    <input type="radio" name="megjelenit_lista_uj" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_uj" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_uj" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_uj" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_lista_pardarab"><?php echo $entry_lista_pardarab; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_pardarab) { ?>
                    <input type="radio" name="megjelenit_lista_pardarab" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_pardarab" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_pardarab" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_pardarab" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_lista_szazalek"><?php echo $entry_lista_szazalek; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_szazalek) { ?>
                    <input type="radio" name="megjelenit_lista_szazalek" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_szazalek" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_szazalek" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_szazalek" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>


        <tr>
            <td title="megjelenit_lista[mennyisegi_ikon]" style="width: 244px;"><?php echo $entry_lista_mennyisegi; ?></td>
            <td style="width: 144px;">
                <?php if ($mennyisegi_ikon['mennyisegi_ikon']) { ?>
                    <input type="radio" name="megjelenit_lista[mennyisegi_ikon]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista[mennyisegi_ikon]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista[mennyisegi_ikon]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista[mennyisegi_ikon]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr><td></td></tr>

        <tr>
            <td title="megjelenit_lista_kivansaglista"><?php echo $entry_lista_kivansaglista; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_kivansaglista) { ?>
                    <input type="radio" name="megjelenit_lista_kivansaglista" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_kivansaglista" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_kivansaglista" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_kivansaglista" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_lista_osszehasonlitas"><?php echo $entry_lista_osszehasonlitas; ?></td>
            <td colspan=3><?php if ($megjelenit_lista_osszehasonlitas) { ?>
                    <input type="radio" name="megjelenit_lista_osszehasonlitas" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_osszehasonlitas" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_lista_osszehasonlitas" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_lista_osszehasonlitas" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>


        </table>

        </div>


        <div id="ttab-product">
        <table class="form1">
        <tr>
            <td title="megjelenit_product_jelentkezzen"><?php echo $entry_product_jelentkezzen; ?></td>
            <td colspan=3><?php if ($megjelenit_product_jelentkezzen) { ?>
                    <input type="radio" name="megjelenit_product_jelentkezzen" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_jelentkezzen" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_jelentkezzen" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_jelentkezzen" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product_nev_fejlec"><?php echo $entry_termek_neve_fejlec; ?></td>
            <td colspan=3><?php if ($megjelenit_product_nev_fejlec) { ?>
                    <input type="radio" name="megjelenit_product_nev_fejlec" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_nev_fejlec" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_nev_fejlec" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_nev_fejlec" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[product_model_termeknevben]"><?php echo $entry_product_model_termeknevben; ?></td>
            <td style="width: 144px;">
                <?php if ($product_model_termeknevben['product_model_termeknevben']) { ?>
                    <input type="radio" name="megjelenit_product[product_model_termeknevben]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_model_termeknevben]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_model_termeknevben]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_model_termeknevben]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[product_cikkszam_termeknevben]"><?php echo "Cikkszám megjelenítése termék név előtt:"; ?></td>
            <td style="width: 144px;">
                <?php if ($product_cikkszam_termeknevben['product_cikkszam_termeknevben']) { ?>
                    <input type="radio" name="megjelenit_product[product_cikkszam_termeknevben]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_cikkszam_termeknevben]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_cikkszam_termeknevben]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_cikkszam_termeknevben]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[product_gyarto_termeknevben]"><?php echo $entry_product_gyarto_termeknevben; ?></td>
            <td style="width: 144px;">
                <?php if ($product_gyarto_termeknevben['product_gyarto_termeknevben']) { ?>
                    <input type="radio" name="megjelenit_product[product_gyarto_termeknevben]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_gyarto_termeknevben]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_gyarto_termeknevben]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_gyarto_termeknevben]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>


        <tr>
            <td title="megjelenit_kiskepek"><?php echo $entry_kiskepek_megjelenitese; ?></td>
            <td colspan=3><?php if ($megjelenit_kiskepek) { ?>
                    <input type="radio" name="megjelenit_kiskepek" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kiskepek" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_kiskepek" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kiskepek" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_kiskepek_slider"><?php echo $entry_kiskepek_slider; ?></td>
            <td colspan=3><?php if ($megjelenit_kiskepek_slider) { ?>
                    <input type="radio" name="megjelenit_kiskepek_slider" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kiskepek_slider" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_kiskepek_slider" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kiskepek_slider" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>


        <tr style="padding-bottom: 20px; display: table;"><td colspan=4></tr>

        <tr>
            <td title="megjelenit_product_megnevezes"><?php echo $entry_termek_neve_dobozban; ?></td>
            <td colspan=3><?php if ($megjelenit_product_megnevezes) { ?>
                    <input type="radio" name="megjelenit_product_megnevezes" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_megnevezes" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_megnevezes" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_megnevezes" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[product_leiras]" style="width: 244px;"><?php echo $entry_product_leiras; ?></td>
            <td style="width: 144px;">
                <?php if ($product_leiras['product_leiras']) { ?>
                    <input type="radio" name="megjelenit_product[product_leiras]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_leiras]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_leiras]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_leiras]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product_kaphato"><?php echo $entry_termek_kaphato; ?></td>
            <td style="padding-right: 30px;"><?php if ($megjelenit_product_kaphato) { ?>
                    <input type="radio" name="megjelenit_product_kaphato" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_kaphato" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_kaphato" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_kaphato" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_kaphato_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_kaphato_sorrend" value="<?php echo $megjelenit_product_kaphato_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_gropup_filter"><?php echo $entry_termek_kapcsolod_group_filter; ?></td>
            <td style="padding-right: 30px;"><?php if ($megjelenit_product_gropup_filter) { ?>
                    <input type="radio" name="megjelenit_product_gropup_filter" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_gropup_filter" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_gropup_filter" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_gropup_filter" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_gropup_filter_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_gropup_filter_sorrend" value="<?php echo $megjelenit_product_gropup_filter_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_filter_gropup"><?php echo $entry_termek_filter_csoport; ?></td>
            <td style="padding-right: 30px;"><?php if ($megjelenit_product_filter_gropup) { ?>
                    <input type="radio" name="megjelenit_product_filter_gropup" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_filter_gropup" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_filter_gropup" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_filter_gropup" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_filter_gropup_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_filter_gropup_sorrend" value="<?php echo $megjelenit_product_filter_gropup_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_filter"><?php echo $entry_termek_filter; ?></td>
            <td style="padding-right: 30px;"><?php if ($megjelenit_product_filter) { ?>
                    <input type="radio" name="megjelenit_product_filter" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_filter" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_filter" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_filter" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_filter_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_filter_sorrend" value="<?php echo $megjelenit_product_filter_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_elerhetoseg"><?php echo $entry_product_elerhetoseg; ?></td>
            <td style="padding-right: 30px;"><?php if ($megjelenit_product_elerhetoseg) { ?>
                    <input type="radio" name="megjelenit_product_elerhetoseg" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_elerhetoseg" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_elerhetoseg" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_elerhetoseg" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_elerhetoseg_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_elerhetoseg_sorrend" value="<?php echo $megjelenit_product_elerhetoseg_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_suly"><?php echo $entry_product_suly; ?></td>
            <td><?php if ($megjelenit_product_suly) { ?>
                    <input type="radio" name="megjelenit_product_suly" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_suly" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_suly" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_suly" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_suly_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_suly_sorrend" value="<?php echo $megjelenit_product_suly_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_mennyisegi_egyseg"><?php echo $entry_product_mennyisegi_egyseg; ?></td>
            <td><?php if ($megjelenit_product_mennyisegi_egyseg) { ?>
                    <input type="radio" name="megjelenit_product_mennyisegi_egyseg" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_mennyisegi_egyseg" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_mennyisegi_egyseg" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_mennyisegi_egyseg" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_mennyisegi_egyseg_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_mennyisegi_egyseg_sorrend" value="<?php echo $megjelenit_product_mennyisegi_egyseg_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_cikkszam"><?php echo $entry_product_cikkszam; ?></td>
            <td><?php if ($megjelenit_product_cikkszam) { ?>
                    <input type="radio" name="megjelenit_product_cikkszam" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_cikkszam" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_cikkszam" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_cikkszam" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_cikkszam_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_cikkszam_sorrend" value="<?php echo $megjelenit_product_cikkszam_sorrend?>" size=2 /></td>
        </tr>

        <tr>
            <td title="megjelenit_product_cikkszam2"><?php echo $entry_product_cikkszam2; ?></td>
            <td><?php if ($megjelenit_product_cikkszam2) { ?>
                    <input type="radio" name="megjelenit_product_cikkszam2" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_cikkszam2" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_cikkszam2" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_cikkszam2" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_cikkszam2_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_cikkszam2_sorrend" value="<?php echo $megjelenit_product_cikkszam2_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_model"><?php echo $entry_product_model; ?></td>
            <td><?php if ($megjelenit_product_model) { ?>
                    <input type="radio" name="megjelenit_product_model" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_model" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_model" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_model" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_model_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_model_sorrend" value="<?php echo $megjelenit_product_model_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_gyarto"><?php echo $entry_product_gyarto; ?></td>
            <td><?php if ($megjelenit_product_gyarto) { ?>
                    <input type="radio" name="megjelenit_product_gyarto" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_gyarto" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_gyarto" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_gyarto" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_gyarto_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_gyarto_sorrend" value="<?php echo $megjelenit_product_gyarto_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_reward"><?php echo $entry_product_reward; ?></td>
            <td><?php if ($megjelenit_product_reward) { ?>
                    <input type="radio" name="megjelenit_product_reward" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_reward" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_reward" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_reward" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_reward_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_reward_sorrend" value="<?php echo $megjelenit_product_reward_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_sku"><?php echo $entry_product_sku; ?></td>
            <td> <?php if ($megjelenit_product_sku) { ?>
                    <input type="radio" name="megjelenit_product_sku" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_sku" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_sku" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_sku" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td style="width: 50px;" title="megjelenit_product_sku_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_sku_sorrend" value="<?php echo $megjelenit_product_sku_sorrend?>" size=2 /></td>
        </tr>
        <tr>
            <td title="megjelenit_product_meret"><?php echo $entry_product_meret; ?></td>
            <td><?php if ($megjelenit_product_meret) { ?>
                    <input type="radio" name="megjelenit_product_meret" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_meret" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_meret" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_meret" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            <td title="megjelenit_product_meret_sorrend"><?php echo $entry_product_sorrend; ?></td>
            <td><input type="text" name="megjelenit_product_meret_sorrend" value="<?php echo $megjelenit_product_meret_sorrend?>" size=2 /></td>
        </tr>

        <tr style="padding-bottom: 20px; display: table;"><td colspan=4></tr>

        <tr>
            <td title="megjelenit_product_brutto_ar"><?php echo $entry_brutto_ar; ?></td>
            <td><?php if ($megjelenit_product_brutto_ar) { ?>
                    <input type="radio" name="megjelenit_product_brutto_ar" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_brutto_ar" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_brutto_ar" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_brutto_ar" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product_netto_ar"><?php echo $entry_netto_ar; ?></td>
            <td><?php if ($megjelenit_product_netto_ar) { ?>
                    <input type="radio" name="megjelenit_product_netto_ar" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_netto_ar" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_netto_ar" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_netto_ar" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product_brutto_netto"><?php echo $entry_brutto_netto; ?></td>
            <td><?php if ($megjelenit_product_brutto_netto) { ?>
                    <input type="radio" name="megjelenit_product_brutto_netto" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_brutto_netto" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_brutto_netto" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_brutto_netto" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product_quantity"><?php echo $entry_termek_quantity; ?></td>
            <td><?php if ($megjelenit_product_quantity) { ?>
                    <input type="radio" name="megjelenit_product_quantity" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_quantity" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_quantity" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_quantity" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product_csomagolasi_mennyiseg"><?php echo $entry_csomagolasi_mennyiseg; ?></td>
            <td><?php if ($megjelenit_product_csomagolasi_mennyiseg) { ?>
                    <input type="radio" name="megjelenit_product_csomagolasi_mennyiseg" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_csomagolasi_mennyiseg" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_csomagolasi_mennyiseg" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_csomagolasi_mennyiseg" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>


        <tr style="padding-bottom: 20px; display: table;"><td colspan=4></tr>

        <tr>
            <td title="megjelenit_product[product_velemeny_megosztas_doboz]" style="width: 244px;"><?php echo $entry_product_velemeny_megosztas_doboz; ?></td>
            <td style="width: 144px;">
                <?php if ($product_velemeny_megosztas_doboz['product_velemeny_megosztas_doboz']) { ?>
                    <input type="radio" name="megjelenit_product[product_velemeny_megosztas_doboz]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_velemeny_megosztas_doboz]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_velemeny_megosztas_doboz]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_velemeny_megosztas_doboz]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product_jobb_jobb_elso"><?php echo $entry_termek_jobb_jobb_elso; ?></td>
            <td><?php if ($megjelenit_product_jobb_jobb_elso) { ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_elso" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_elso" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_elso" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_elso" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_kosar_kivansag"><?php echo $entry_megjelenit_kosar_kivansag; ?></td>
            <td><?php if ($megjelenit_kosar_kivansag) { ?>
                    <input type="radio" name="megjelenit_kosar_kivansag" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kosar_kivansag" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_kosar_kivansag" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_kosar_kivansag" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_product_jobb_jobb_masodik"><?php echo $entry_termek_jobb_jobb_masodik; ?></td>
            <td><?php if ($megjelenit_product_jobb_jobb_masodik) { ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_masodik" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_masodik" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_masodik" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_masodik" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_product_jobb_jobb_harmadik"><?php echo $entry_termek_jobb_jobb_harmadik; ?></td>
            <td><?php if ($megjelenit_product_jobb_jobb_harmadik) { ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_harmadik" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_harmadik" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_harmadik" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_jobb_jobb_harmadik" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>




        <tr>
            <td title="megjelenit_product_social"><?php echo $entry_termek_social; ?></td>
            <td><?php if ($megjelenit_product_social) { ?>
                    <input type="radio" name="megjelenit_product_social" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_social" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_social" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_social" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_product_leiras"><?php echo $entry_termek_leiras; ?></td>
            <td><?php if ($megjelenit_product_leiras) { ?>
                    <input type="radio" name="megjelenit_product_leiras" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_leiras" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_leiras" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_leiras" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product_kapcsolodo"><?php echo $entry_termek_kapcsolod_allitott; ?></td>
            <td><?php if ($megjelenit_product_kapcsolodo) { ?>
                    <input type="radio" name="megjelenit_product_kapcsolodo" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_kapcsolodo" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product_kapcsolodo" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product_kapcsolodo" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr style="height: 20px;"></tr>

        <tr>
            <td title="megjelenit_product[product_koztes]" style="width: 244px;"><b><?php echo $entry_product_koztes; ?></b></td>
            <td style="width: 144px;">
                <?php if ($product_koztes['product_koztes']) { ?>
                    <input type="radio" name="megjelenit_product[product_koztes]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_koztes]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_koztes]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_koztes]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>


        <tr>
            <td title="megjelenit_product[product_garancia]" style="width: 244px;"><?php echo $entry_product_garancia; ?></td>
            <td style="width: 144px;">
                <?php if ($product_garancia['product_garancia']) { ?>
                    <input type="radio" name="megjelenit_product[product_garancia]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_garancia]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_garancia]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_garancia]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_product[product_szallitas]" style="width: 244px;"><?php echo $entry_product_szallitas; ?></td>
            <td style="width: 144px;">
                <?php if ($product_szallitas['product_szallitas']) { ?>
                    <input type="radio" name="megjelenit_product[product_szallitas]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_szallitas]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_szallitas]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_szallitas]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_product[product_velemeny]" style="width: 244px;"><?php echo $entry_product_velemeny; ?></td>
            <td style="width: 144px;">
                <?php if ($product_velemeny['product_velemeny']) { ?>
                    <input type="radio" name="megjelenit_product[product_velemeny]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_velemeny]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_velemeny]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_velemeny]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_product[product_osszehasonlit_kivansag]" style="width: 244px;"><?php echo $entry_product_osszehasonlit_kivansag; ?></td>
            <td style="width: 144px;">
                <?php if ($product_osszehasonlit_kivansag['product_osszehasonlit_kivansag']) { ?>
                    <input type="radio" name="megjelenit_product[product_osszehasonlit_kivansag]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_osszehasonlit_kivansag]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_osszehasonlit_kivansag]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_osszehasonlit_kivansag]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="megjelenit_product[product_addthis]" style="width: 244px;"><?php echo $entry_product_addthis; ?></td>
            <td style="width: 144px;">
                <?php if ($product_addthis['product_addthis']) { ?>
                    <input type="radio" name="megjelenit_product[product_addthis]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_addthis]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_addthis]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_addthis]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr><td><br></td></tr>
        <tr>
            <td colspan="5"><b><?php echo $entry_ikonok; ?></b></td>
        </tr>

        <tr>
            <td title="megjelenit_product[product_uj_ikon]" style="width: 244px;"><?php echo $entry_uj; ?></td>
            <td style="width: 144px;">
                <?php if ($product_uj_ikon['product_uj_ikon']) { ?>
                    <input type="radio" name="megjelenit_product[product_uj_ikon]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_uj_ikon]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_uj_ikon]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_uj_ikon]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[product_par_ikon]" style="width: 244px;"><?php echo $entry_pardarab; ?></td>
            <td style="width: 144px;">
                <?php if ($product_par_ikon['product_par_ikon']) { ?>
                    <input type="radio" name="megjelenit_product[product_par_ikon]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_par_ikon]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_par_ikon]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_par_ikon]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[product_szazalek_ikon]" style="width: 244px;"><?php echo $entry_szazalek; ?></td>
            <td style="width: 144px;">
                <?php if ($product_szazalek_ikon['product_szazalek_ikon']) { ?>
                    <input type="radio" name="megjelenit_product[product_szazalek_ikon]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_szazalek_ikon]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_szazalek_ikon]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_szazalek_ikon]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[product_mennyisegi_ikon]" style="width: 244px;"><?php echo $entry_product_mennyisegi_ikon; ?></td>
            <td style="width: 144px;">
                <?php if ($product_mennyisegi_ikon['product_mennyisegi_ikon']) { ?>
                    <input type="radio" name="megjelenit_product[product_mennyisegi_ikon]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_mennyisegi_ikon]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[product_mennyisegi_ikon]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[product_mennyisegi_ikon]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr><td><br></td></tr>
        <tr>
            <td colspan="5"><b><?php echo $entry_tabs_settings; ?></b></td>
        </tr>

        <tr>
            <td title="megjelenit_product[tab_description]" style="width: 244px;"><?php echo $entry_product_tab_description; ?></td>
            <td style="width: 144px;">
                <?php if ($megjelenit_product['tab_description']) { ?>
                    <input type="radio" name="megjelenit_product[tab_description]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_description]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[tab_description]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_description]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[tab_meret]" style="width: 244px;"><?php echo $entry_product_tab_meret; ?></td>
            <td style="width: 144px;">
                <?php if ($megjelenit_product['tab_meret']) { ?>
                    <input type="radio" name="megjelenit_product[tab_meret]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_meret]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[tab_meret]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_meret]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[tab_attribute_groups]" style="width: 244px;"><?php echo $entry_product_tab_attribute_groups; ?></td>
            <td style="width: 144px;">
                <?php if ($megjelenit_product['tab_attribute_groups']) { ?>
                    <input type="radio" name="megjelenit_product[tab_attribute_groups]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_attribute_groups]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[tab_attribute_groups]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_attribute_groups]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[tab_review_status]" style="width: 244px;"><?php echo $entry_product_tab_review_status; ?></td>
            <td style="width: 144px;">
                <?php if ($megjelenit_product['tab_review_status']) { ?>
                    <input type="radio" name="megjelenit_product[tab_review_status]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_review_status]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[tab_review_status]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_review_status]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[tab_products]" style="width: 244px;"><?php echo $entry_product_tab_products; ?></td>
            <td style="width: 144px;">
                <?php if ($megjelenit_product['tab_products']) { ?>
                    <input type="radio" name="megjelenit_product[tab_products]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_products]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[tab_products]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_products]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[tab_test_description]" style="width: 244px;"><?php echo $entry_product_tab_test_description; ?></td>
            <td style="width: 144px;">
                <?php if ($megjelenit_product['tab_test_description']) { ?>
                    <input type="radio" name="megjelenit_product[tab_test_description]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_test_description]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[tab_test_description]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_test_description]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td title="megjelenit_product[tab_image]" style="width: 244px;"><?php echo $entry_product_tab_image; ?></td>
            <td style="width: 144px;">
                <?php if ($megjelenit_product['tab_image']) { ?>
                    <input type="radio" name="megjelenit_product[tab_image]" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_image]" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_product[tab_image]" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_product[tab_image]" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
        </tr>


        <tr><td><br></td></tr>


        <tr><td colspan=5>
                <table  class="form1">
                    <tr>
                        <td><b>Sablon neve</b></td>
                        <td><b>Státusz</b></td>
                        <td><b>Sor</b></td>
                        <td><b>Oszlop</b></td>
                    </tr>
                    <tr><td><br></td></tr>




                    <?php foreach($megjelenit_termek_tabok as $key=>$doboz) {?>
                        <tr>
                            <td><?php echo $key.".tpl";?></td>
                            <td style="padding-right: 20px;">
                                <?php if ($doboz['status'] == 1) { ?>
                                    <input type="radio" name="megjelenit_termek_tabok[<?php echo $key?>][status]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_termek_tabok[<?php echo $key?>][status]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_termek_tabok[<?php echo $key?>][status]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_termek_tabok[<?php echo $key?>][status]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td><input type="text" name="megjelenit_termek_tabok[<?php echo $key?>][sort_order_sor]" value="<?php echo $doboz['sort_order_sor']?>"  size=2 /></td>
                            <td><input type="text" name="megjelenit_termek_tabok[<?php echo $key?>][sort_order_oszlop]" value="<?php echo $doboz['sort_order_oszlop']?>"        size=2 /></td>

                            <?php if (isset($megjelenit_termek_tabok_beallitas[$key])) {?>
                                <?php $melyik_doboz = "megjelenit_termek_tabok"?>
                                <?php foreach ($megjelenit_termek_tabok_beallitas[$key] as $keydoboz=>$tomb) { ?>
                                    <?php if(count($tomb) > 1) { ?>
                                        <td>
                                            <?php require(DIR_APPLICATION."controller/design/inputelements/".$tomb['type'].'.tpl')?>
                                        </td>

                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>

                        </tr>

                    <?php }?>
                </table>
            </td></tr>
        <tr><td><br></td></tr>




        </table>
        </div>





        <div id="ttab-kosar">

            <table class="form">

                <tr>
                    <td><?php echo $entry_megjelenit_minicart_kosar; ?></td>

                    <td class="right" colspan="3">
                        <select name="megjelenit_minicart_kosar">
                            <?php if ($megjelenit_minicart_kosar == 0) { ?>

                                <option value="0" selected="selected"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 1) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1" selected="selected"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 2) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2" selected="selected"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 3) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3" selected="selected"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 4) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4" selected="selected"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 5) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5" selected="selected"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 6) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6" selected="selected"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 7) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7" selected="selected"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 8) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8" selected="selected"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 9) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9" selected="selected"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 10) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10" selected="selected"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } elseif ($megjelenit_minicart_kosar == 11) { ?>

                                <option value="0"><?php echo $kep_szoveg; ?></option>
                                <option value="1"><?php echo $kep_szam; ?></option>
                                <option value="2"><?php echo $kep_szoveg_szam; ?></option>
                                <option value="3"><?php echo $kep_szam_szoveg; ?></option>
                                <option value="4"><?php echo $szam_kep; ?></option>
                                <option value="5"><?php echo $szam_szoveg; ?></option>
                                <option value="6"><?php echo $szam_kep_szoveg; ?></option>
                                <option value="7"><?php echo $szam_szoveg_kep; ?></option>
                                <option value="8"><?php echo $szoveg_szam; ?></option>
                                <option value="9"><?php echo $szoveg_kep; ?></option>
                                <option value="10"><?php echo $szoveg_szam_kep; ?></option>
                                <option value="11" selected="selected"><?php echo $szoveg_kep_szam; ?></option>

                            <?php } ?>
                        </select>
                    </td>
                </tr>




                <tr>
                    <td title="megjelenit_kosar_kupon"><?php echo $entry_kosar_kupon; ?></td>
                    <td><?php if ($megjelenit_kosar_kupon) { ?>
                            <input type="radio" name="megjelenit_kosar_kupon" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_kosar_kupon" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_kosar_kupon" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_kosar_kupon" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_kosar_suly"><?php echo $entry_kosar_suly; ?></td>
                    <td><?php if ($megjelenit_kosar_suly) { ?>
                            <input type="radio" name="megjelenit_kosar_suly" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_kosar_suly" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_kosar_suly" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_kosar_suly" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_mini_cart"><?php echo $entry_mini_cart; ?></td>
                    <td><?php if ($megjelenit_mini_cart) { ?>
                            <input type="radio" name="megjelenit_mini_cart" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_mini_cart" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_mini_cart" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_mini_cart" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_penztar_fizetesi_mod"><?php echo $entry_penztar_fizetesi_mod; ?></td>
                    <td><?php if ($megjelenit_penztar_fizetesi_mod) { ?>
                            <input type="radio" name="megjelenit_penztar_fizetesi_mod" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_penztar_fizetesi_mod" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_penztar_fizetesi_mod" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_penztar_fizetesi_mod" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_admin_kosar[model_adat]"><?php echo $entry_model_adat; ?></td>
                    <td><?php if ($model_adat['model_adat']) { ?>
                            <input type="radio" name="megjelenit_admin_kosar[model_adat]" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_kosar[model_adat]" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_admin_kosar[model_adat]" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_kosar[model_adat]" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_admin_kosar[netto_adat]"><?php echo $entry_netto_adat; ?></td>
                    <td><?php if ($netto_adat['netto_adat']) { ?>
                            <input type="radio" name="megjelenit_admin_kosar[netto_adat]" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_kosar[netto_adat]" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_admin_kosar[netto_adat]" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_kosar[netto_adat]" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_admin_kosar[cart_shipping_region]" style="width: 244px;"><?php echo $entry_admin_cart_shipping_region; ?></td>
                    <td style="width: 144px;">
                        <?php if ($megjelenit_admin_kosar['cart_shipping_region']) { ?>
                            <input type="radio" name="megjelenit_admin_kosar[cart_shipping_region]" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_kosar[cart_shipping_region]" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_admin_kosar[cart_shipping_region]" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_kosar[cart_shipping_region]" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_admin_kosar[cart_shipping_postcode]" style="width: 244px;"><?php echo $entry_admin_cart_shipping_postcode; ?></td>
                    <td style="width: 144px;">
                        <?php if ($megjelenit_admin_kosar['cart_shipping_postcode']) { ?>
                            <input type="radio" name="megjelenit_admin_kosar[cart_shipping_postcode]" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_kosar[cart_shipping_postcode]" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_admin_kosar[cart_shipping_postcode]" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_kosar[cart_shipping_postcode]" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>


                <tr>
                    <td title="megjelenit_checkout_template" style="width: 244px;">Megrendelem gombra checkout template<span class="help">A template név eleje midig 'checkout_megrendelem' legyen!</span></td>
                    <td style="width: 144px;">
                        <select name="megjelenit_checkout_template">
                            <?php foreach($megjelenit_checkout_templates as $key=>$template_value) { ?>
                                <?php $selected = $megjelenit_checkout_template == $template_value ? 'selected="selected"' : '' ?>
                                <option value="<?php echo $template_value?>" <?php echo $selected?> ><?php echo $template_value; ?></option>
                            <?php } ?>
                        </select>

                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_csak_vendegkent_vasarol" style="width: 244px;">Fizetésnél csak vendégként vásárolhat <span class='help'>Nincs bejelentkezés, nincs regisztráció</span></td>
                    <td style="width: 144px;">
                        <?php if ($megjelenit_csak_vendegkent_vasarol) { ?>
                            <input type="radio" name="megjelenit_csak_vendegkent_vasarol" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_csak_vendegkent_vasarol" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_csak_vendegkent_vasarol" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_csak_vendegkent_vasarol" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>



                <tr>
                    <td><div class="scrollbox">
                            <?php $class = 'odd'; ?>
                            <?php foreach ($information_pages as $information_page) { ?>
                                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <div class="<?php echo $class; ?>">
                                    <?php $talalt = false;?>
                                    <?php if(isset($megjelenit_elolvastam_informaciok_kosar)) {
                                        foreach ($megjelenit_elolvastam_informaciok_kosar as $informaciok){
                                            if ($informaciok['information_id'] == $information_page['information_id']) {
                                                $talalt = true;
                                                break;
                                            }
                                        }
                                    }?>
                                    <?php if ($talalt) { ?>
                                        <input type="checkbox" name="megjelenit_elolvastam_informaciok_kosar[<?php echo $information_page['information_id']; ?>][information_id]" value="<?php echo $information_page['information_id']; ?>" checked="checked" />
                                        <?php echo " ".$entry_sort_order; ?>
                                        <input type="text" name="megjelenit_elolvastam_informaciok_kosar[<?php echo $information_page['information_id']; ?>][sort_order]" value="<?php echo $informaciok['sort_order']; ?>" size="2"/>
                                        <?php echo " ".$information_page['title']; ?>
                                    <?php } else { ?>
                                        <input type="checkbox" name="megjelenit_elolvastam_informaciok_kosar[<?php echo $information_page['information_id']; ?>][information_id]" value="<?php echo $information_page['information_id']; ?>" />
                                        <?php echo " ".$entry_sort_order; ?>
                                        <input type="text" name="megjelenit_elolvastam_informaciok_kosar[<?php echo $information_page['information_id']; ?>][sort_order]" value="0" size="2"/>
                                        <?php echo " ".$information_page['title']; ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        </br>
                        <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
                    </br>


                    <td class="left">
                        <?php foreach ($languages as $language) { ?>

                            <textarea name="megjelenit_elolvastam_kosar[<?php echo $language['language_id']; ?>]" cols="40" rows="4"
                                      id="megjelenit_elolvastam_kosar<?php echo $language['language_id']; ?>"><?php echo isset( $megjelenit_elolvastam_kosar[$language['language_id']] ) ? $megjelenit_elolvastam_kosar[$language['language_id']] : ''; ?></textarea>
                            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                        <?php } ?>
                    </td>
                </tr>

            </table>

        </div>

        <div id="ttab-email">
            <div id="fttab" class="htabs">
                <a href="#fttab-emailreg"><?php echo $entry_emailreg; ?></a>
            </div>
            <table id="#fttab-emailreg" class="form1">
                <tr>
                    <td title="megjelenit_emailreg[jelszo]" style="width: 244px;"><?php echo $entry_emailreg_jelszo; ?></td>
                    <td style="width: 144px;">
                        <?php if ($jelszo['jelszo']) { ?>
                            <input type="radio" name="megjelenit_emailreg[jelszo]" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_emailreg[jelszo]" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_emailreg[jelszo]" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_emailreg[jelszo]" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </div>

        <div id="ttab-kereso">
            <table class="form">
                <tr>
                    <td title="megjelenit_keres_bovit"><?php echo "Kereső: bővítve keres"; ?></td>
                    <td><?php if ($megjelenit_keres_bovit) { ?>
                            <input type="radio" name="megjelenit_keres_bovit" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_bovit" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_keres_bovit" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_bovit" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_keres_autocomplete"><?php echo "Kereső: autocomplate engedélyezése"; ?></td>
                    <td><?php if ($megjelenit_keres_autocomplete) { ?>
                            <input type="radio" name="megjelenit_keres_autocomplete" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_autocomplete" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_keres_autocomplete" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_autocomplete" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_keres_gyartora"><?php echo "Kereső: gyártóra is keres"; ?></td>
                    <td><?php if ($megjelenit_keres_gyartora) { ?>
                            <input type="radio" name="megjelenit_keres_gyartora" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_gyartora" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_keres_gyartora" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_gyartora" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_keres_modell"><?php echo "Kereső: modellre is keres"; ?></td>
                    <td><?php if ($megjelenit_keres_modell) { ?>
                            <input type="radio" name="megjelenit_keres_modell" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_modell" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_keres_modell" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_modell" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_keres_cikkszam"><?php echo "Kereső: cikkszámra is keres"; ?></td>
                    <td><?php if ($megjelenit_keres_cikkszam) { ?>
                            <input type="radio" name="megjelenit_keres_cikkszam" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_cikkszam" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_keres_cikkszam" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_cikkszam" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_keres_cikkszam2"><?php echo "Kereső: cikkszám 2-re is keres"; ?></td>
                    <td><?php if ($megjelenit_keres_cikkszam2) { ?>
                            <input type="radio" name="megjelenit_keres_cikkszam2" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_cikkszam2" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_keres_cikkszam2" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_cikkszam2" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                 <tr>
                    <td title="megjelenit_keres_rovid_leiras"><?php echo "Kereső: rövid leírásra is keres"; ?></td>
                    <td><?php if ($megjelenit_keres_rovid_leiras) { ?>
                            <input type="radio" name="megjelenit_keres_rovid_leiras" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_rovid_leiras" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_keres_rovid_leiras" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_rovid_leiras" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_keres_hosszu_leiras"><?php echo "Kereső: hosszú leírásra is keres"; ?></td>
                    <td><?php if ($megjelenit_keres_hosszu_leiras) { ?>
                            <input type="radio" name="megjelenit_keres_hosszu_leiras" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_hosszu_leiras" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_keres_hosszu_leiras" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_keres_hosszu_leiras" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td title="megjelenit_admin_keres_model"><?php echo $entry_admin_keres_model; ?></td>
                    <td><?php if ($megjelenit_admin_keres_model) { ?>
                            <input type="radio" name="megjelenit_admin_keres_model" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_keres_model" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_admin_keres_model" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_keres_model" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_admin_keres_cikkszam"><?php echo $entry_admin_keres_cikkszam; ?></td>
                    <td><?php if ($megjelenit_admin_keres_cikkszam) { ?>
                            <input type="radio" name="megjelenit_admin_keres_cikkszam" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_keres_cikkszam" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_admin_keres_cikkszam" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_keres_cikkszam" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_admin_keres_cikkszam2"><?php echo $entry_admin_keres_cikkszam2; ?></td>
                    <td><?php if ($megjelenit_admin_keres_cikkszam2) { ?>
                            <input type="radio" name="megjelenit_admin_keres_cikkszam2" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_keres_cikkszam2" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="megjelenit_admin_keres_cikkszam2" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="megjelenit_admin_keres_cikkszam2" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </div>

        <div id="ttab-arajanlat">
            <table class="form">
                <tr>
                    <td title="megjelenit_arajanlat_telefon">Telefonszám:</td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_telefon" value="1" <?php echo !empty($megjelenit_arajanlat_telefon) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_telefon" value="0" <?php echo empty($megjelenit_arajanlat_telefon) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>

                    <td title="megjelenit_arajanlat_telefon_kotelezo">Kötelező: </td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_telefon_kotelezo" value="1" <?php echo !empty($megjelenit_arajanlat_telefon_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_telefon_kotelezo" value="0" <?php echo empty($megjelenit_arajanlat_telefon_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_arajanlat_iranyitoszam">Irányítószám:</td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_iranyitoszam" value="1" <?php echo !empty($megjelenit_arajanlat_iranyitoszam) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_iranyitoszam" value="0" <?php echo empty($megjelenit_arajanlat_iranyitoszam) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                    <td title="megjelenit_arajanlat_iranyitoszam_kotelezo">Kötelező: </td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_iranyitoszam_kotelezo" value="1" <?php echo !empty($megjelenit_arajanlat_iranyitoszam_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_iranyitoszam_kotelezo" value="0" <?php echo empty($megjelenit_arajanlat_iranyitoszam_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_arajanlat_varos">Város:</td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_varos" value="1" <?php echo !empty($megjelenit_arajanlat_varos) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_varos" value="0" <?php echo empty($megjelenit_arajanlat_varos) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                    <td title="megjelenit_arajanlat_varos_kotelezo">Kötelező: </td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_varos_kotelezo" value="1" <?php echo !empty($megjelenit_arajanlat_varos_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_varos_kotelezo" value="0" <?php echo empty($megjelenit_arajanlat_varos_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                </tr>
                <tr>
                    <td title="megjelenit_arajanlat_utca">Utca, házszáma:</td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_utca" value="1" <?php echo !empty($megjelenit_arajanlat_utca) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_utca" value="0" <?php echo empty($megjelenit_arajanlat_utca) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                    <td title="megjelenit_arajanlat_utca_kotelezo">Kötelező: </td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_utca_kotelezo" value="1" <?php echo !empty($megjelenit_arajanlat_utca_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_utca_kotelezo" value="0" <?php echo empty($megjelenit_arajanlat_utca_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                </tr>
                 <tr>
                    <td title="megjelenit_arajanlat_vallalkozas">Vállalkozás neve:</td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_vallalkozas" value="1" <?php echo !empty($megjelenit_arajanlat_vallalkozas) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_vallalkozas" value="0" <?php echo empty($megjelenit_arajanlat_vallalkozas) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                    <td title="megjelenit_arajanlat_vallalkozas_kotelezo">Kötelező: </td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_vallalkozas_kotelezo" value="1" <?php echo !empty($megjelenit_arajanlat_vallalkozas_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_vallalkozas_kotelezo" value="0" <?php echo empty($megjelenit_arajanlat_vallalkozas_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                </tr>
                 <tr>
                    <td title="megjelenit_arajanlat_adoszam">Adószám:</td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_adoszam" value="1" <?php echo !empty($megjelenit_arajanlat_adoszam) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_adoszam" value="0" <?php echo empty($megjelenit_arajanlat_adoszam) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                    <td title="megjelenit_arajanlat_adoszam_kotelezo">Kötelező: </td>
                    <td>
                        <input type="radio" name="megjelenit_arajanlat_adoszam_kotelezo" value="1" <?php echo !empty($megjelenit_arajanlat_adoszam_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_arajanlat_adoszam_kotelezo" value="0" <?php echo empty($megjelenit_arajanlat_adoszam_kotelezo) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_no; ?>
                    </td>
                </tr>
            </table>

        </div>
    </div>

    <?php require(DIR_TEMPLATE."design/megjelenito/backend.tpl");?>


    </div>

    </form>
    </div>
    </div>
    </div>


    <script type="text/javascript"><!--

        $('#tabs a').tabs();
        $('#languages a').tabs();
        $('#vtab-option a').tabs();
        $('#fttab a').tabs();
        $('#ttab a').tabs();


        /* regisztrációs mezők beállítása */
        $('#registration_default').click("bind",function(){
            if ($('#registration_default').prop("checked")) {
                $('input[name=\'megjelenit_regisztracioblokk_szemelyes\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracioblokk_szemelyes\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_vezeteknev\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_vezeteknev\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_keresztnev\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_keresztnev\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_email\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_email\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_email_megerosites\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_email_megerosites\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_telefon\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_telefon\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_fax\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_fax\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracioblokk_cim\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracioblokk_cim\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_cimadat_cegnev\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_cimadat_cegnev\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_cimadat_adoszam\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_cimadat_adoszam\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_utca\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_utca\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_cimadat_kiegeszito\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_cimadat_kiegeszito\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_varos\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_varos\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_iranyitoszam\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_iranyitoszam\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_orszag\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_orszag\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_megye\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_megye\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracioblokk_paypal\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracioblokk_paypal\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_paypal\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_paypal\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracioblokk_hirlevel\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracioblokk_hirlevel\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_hirlevel_kategoriak\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_hirlevel_kategoriak\']').filter('.igen').prop("checked",true);
            }
        });


        $('#registration_company').click("bind",function(){
            if ($('#registration_company').prop("checked")) {
                $('input[name=\'megjelenit_regisztracioblokk_ceges_szemelyes\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracioblokk_ceges_szemelyes\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_vezeteknev\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_vezeteknev\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_keresztnev\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_keresztnev\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_email\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_email\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_email_megerosites\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_email_megerosites\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_telefon\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_telefon\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_fax\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_fax\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracioblokk_ceges_cim\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracioblokk_ceges_cim\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_cimadat_cegnev\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_cimadat_cegnev\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_cimadat_adoszam\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_cimadat_adoszam\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_utca\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_utca\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_cimadat_kiegeszito\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_cimadat_kiegeszito\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_varos\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_varos\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_iranyitoszam\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_iranyitoszam\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_orszag\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_orszag\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_megye\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_megye\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracioblokk_ceges_paypal\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracioblokk_ceges_paypal\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_paypal\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_paypal\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracioblokk_ceges_hirlevel\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracioblokk_ceges_hirlevel\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_ceges_hirlevel_kategoriak\']').filter('.igen').val("1");
                $('input[name=\'megjelenit_regisztracio_ceges_hirlevel_kategoriak\']').filter('.igen').prop("checked",true);
                $('input[name=\'megjelenit_regisztracio_vezeteknev\']').val("1");
                $('input[name=\'megjelenit_regisztracio_vezeteknev\']').filter(".igen").prop("checked",true);
            }
        });


        //--></script>


<?php echo $footer; ?>