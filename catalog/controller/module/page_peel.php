<?php
class ControllerModulePagePeel extends Controller {
	protected function index($setting) {
		static $module = 0;
		
		$this->load->model('tool/image');
		
		$this->document->addScript('catalog/view/javascript/jquery/jquery.splash.js');
		$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/splash.css');
		
		$this->data['small_swf'] = 'catalog/view/theme/' . $this->config->get('config_template') . '/flash/small.swf';
		$this->data['large_swf'] = 'catalog/view/theme/' . $this->config->get('config_template') . '/flash/large.swf';
		
		if ($setting['small_image']){
			$this->data['small_image'] = $this->model_tool_image->resize($setting['small_image'],75,75);
		} else {
			$this->data['small_image'] = $this->model_tool_image->resize($setting['big_image'],75,75);
		}
		
		if ($setting['big_image']){
			$this->data['big_image'] = $this->model_tool_image->resize($setting['big_image'],500,500);
		} else {
			$this->data['big_image'] = $this->model_tool_image->resize($setting['big_image'],500,500);
		}
		
		$this->data['link'] = $setting['link'];
		$this->data['new_window'] = $setting['new_window'];
		
		$this->data['module'] = $module++;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/page_peel.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/page_peel.tpl';
		} else {
			$this->template = 'default/template/module/page_peel.tpl';
		}

		$this->render();
	}
}
?>