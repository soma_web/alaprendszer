<?php
/*
Developer: Ekrem KAYA
Web Page: www.e-piksel.com
Free Page: free.e-piksel.com
Lisence: GNU General Public License version 3 (GPLv3)
*/

// Heading
$_['heading_title_normal']= 'Landing page Module';
$_['heading_title']       = '<font color="green"><b>E-Piksel HTML Module</b></font>';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Landing page!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_content_header'] = 'Header';

$_['text_or']			  = 'or';
$_['text_create_paypal']  = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=YS4L83BNSEC6A\');">Create your PayPal account</a>';
$_['text_developer']	  = 'Developer:';
$_['text_html_version']	  = 'Landing page Module';

// Entry
$_['entry_heading']		  = 'Page URL:';
$_['entry_description']   = 'HTML Code / Message:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_box_status']    = 'Box Status:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_minden_oldal']  = 'All subpage (header):';
$_['entry_product_id']    = 'Product ID:';

// Error
$_['error_product_id']    = 'Please enter the product ID!';
$_['error_not_exist_product_id_']= 'Incorrect identification! Please change!';
$_['error_warning']       = 'Warning: Please carefully review the errors on the form!';
$_['error_permission']    = 'Warning: You do not have permission to modify module Landing page!';
?>