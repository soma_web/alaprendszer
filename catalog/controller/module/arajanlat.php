<?php
class ControllerModuleArajanlat extends Controller {
	private $error = array();
	
	public function index() {

        if (isset($this->session->data['arajanlat']) && $this->session->data['arajanlat']) {

        } else return false;


            $this->language->load('module/arajanlat');
        $this->load->model('module/arajanlat');

        $this->data['text_success'] = "";
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_ajanlat_neve'] = $this->language->get('text_ajanlat_neve');
        $this->data['column_image'] = $this->language->get('column_image');
        $this->data['column_name'] = $this->language->get('column_name');
        $this->data['column_quantity'] = $this->language->get('column_quantity');
        $this->data['column_price'] = $this->language->get('column_price');
        $this->data['column_total'] = $this->language->get('column_total');

        if ($this->config->get('megjelenit_form_admin_model') == 1) {
            $this->data['column_model'] = $this->language->get('column_model');
        } elseif ($this->config->get('megjelenit_form_admin_cikkszam') == 1) {
            $this->data['column_model'] = $this->language->get('column_cikkszam');
        } elseif ( $this->config->get('megjelenit_form_admin_cikkszam2') == 1) {
            $this->data['column_model'] = $this->language->get('column_cikkszam');
        } else {
            $this->data['column_model'] = "";
        }

        $this->data['button_torol_metese']  = $this->language->get('button_torol_metese');
        $this->data['button_arajanlat_metese']  = $this->language->get('button_arajanlat_metese');
        $this->data['button_tovabb']            = $this->language->get('button_tovabb');

        $this->data['megjelenit_product'] = $this->config->get('megjelenit_product');


        $this->data['megjelenit_kosar'] = $this->config->get('megjelenit_admin_kosar');

        $darab = $this->model_module_arajanlat->countProducts();

        $this->data['text_empty']               = $this->language->get('text_empty');
        $this->data['text_cart']                = $this->language->get('text_cart');


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/close_upsell.png')) {
            $close = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/close_upsell.png';
        } else {
            $close = DIR_TEMPLATE_IMAGE.'default/image/close_upsell.png';
        }

        $this->data['close'] = $close;
        $this->data['text_checkout'] = $this->language->get('text_checkout');
        $this->data['text_close'] = $this->language->get('text_close');

        $this->data['button_remove'] = $this->language->get('button_remove');

        $this->load->model('tool/image');


        $this->data['products'] = array();
        if (isset( $this->request->request['arajanlat_neve'])) {
            $this->session->data['arajanlat_neve'] = $this->request->request['arajanlat_neve'];
        }

        $this->data['ajanlat_neve'] = isset($this->session->data['arajanlat_neve']) ?  $this->session->data['arajanlat_neve'] : '';


        foreach ($this->cart->getProducts(false,false,false,'arajanlat') as $product) {

            if ($product['image']) {
                $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
            } else {
                $image = '';
            }

            $szin_meret_szukseges = 0;
            if ($product['szin_meret_szukseges']) {
                $meret_szin_db = 0;
                foreach ($product['option'] as $option) {
                    if ($option['type'] == 'checkbox_qty') {
                        $meret_szin_db ++;
                    }
                }
                if ($meret_szin_db < 2) {
                    $szin_meret_szukseges = 1;
                }
            }



            $option_data = array();

            foreach ($product['option'] as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['option_value'];
                } else {
                    $filename = $this->encryption->decrypt($option['option_value']);

                    $value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
                }


                $option_data[] = array(
                    'name'                      => $option['name'],
                    'value'                     => (utf8_strlen($value) > 40 ? utf8_substr($value, 0, 40) . '...' : $value),
                    'type'                      => $option['type'],
                    'option_id'                 => $option['option_id'],
                    'option_value_id'           => $option['option_value_id'],
                    'product_option_id'         => $option['product_option_id'],
                    'product_option_value_id'   => $option['product_option_value_id'],
                    'required'                  => ($option['type'] == 'checkbox_qty' ? $szin_meret_szukseges : $option['required'])
                );
            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));

            } else {
                $price = false;
            }

            $mennyiseg = $product['quantity'];

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                 $total = $this->currency->format($this->tax->calculate($product['price']*$mennyiseg, $product['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $total = false;
            }


            $this->load->model("catalog/manufacturer");
            $manufacturer = $this->model_catalog_manufacturer->getManufacturer($product['manufacturer_id']);
            $manufacturer = isset($manufacturer['name']) && $manufacturer['name'] ? $manufacturer['name'] : "";

            $this->data['products'][] = array(
                'key'               => $product['key'],
                'thumb'             => $image,
                'name'              => $product['name'],
                'model'             => $product['model'],
                'cikkszam'          => $product['cikkszam'],
                'manufacturer'      => $manufacturer,
                'option'            => $option_data,
                'quantity'          => $mennyiseg,
                'price'             => $price,
                'total'             => $total,
                'szin_meret_szukseges' => $product['szin_meret_szukseges'],
                'href'              => $this->url->link('product/product', 'product_id=' . $product['product_id'])
            );
        }



        $this->data['arajanlat'] = $this->url->link('module/arajanlat/ajanlatTarol', '', 'SSL');




        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arajanlat.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/arajanlat.tpl';
        } else {
            $this->template = 'default/template/module/arajanlat.tpl';
        }

        return $this->response->setOutput($this->render());
    }


	public function add() {
		$this->language->load('module/arajanlat');
        $this->load->model('module/arajanlat');

        $json = array();
        $redirect = false;

        $error = '';
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');
						
		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
            $quantity = isset($this->request->post['quantity']) && $this->request->post['quantity'] > 0 ? $this->request->post['quantity'] : 1;
            $option =  isset($this->request->post['option']) ? array_filter($this->request->post['option']) : array();
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                if( $product_option['type'] != 'checkbox_qty') {
                    if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                        $json['error']['option'] = sprintf($this->language->get('error_required'), $product_option['name']);
                        $redirect = true;
                        $error = $json['error']['option'];

                        break;
                    }
                } else {
                    if ($this->request->get['termek_aloldal'] != "true" && $product_info['szin_meret_szukseges']) {
                        $json['error']['option'] = $this->language->get('error_meret_szin');
                        $redirect = true;
                        $error = $json['error']['option'];
                        break;
                    }
                }
            }

			if ($product_info['szin_meret_szukseges'] ) {

				if ($this->request->get['termek_aloldal'] == "true") {
					$mehet = false;

					if ($option) {
						foreach ($option as $valasztekok) {
							if ($valasztekok && is_array($valasztekok)) {
								foreach ($valasztekok as $valasztek) {
									$valasztek = explode('|', $valasztek);
									$valasztek = $valasztek[0] ? $valasztek[0] : false;
									if ($valasztek) {


										foreach ($product_options as $product_option) {
											if ($product_option['type'] == "checkbox_qty") {
												foreach ($product_option['option_value'] as $value) {
													if ($value['product_option_value_id'] == $valasztek) {
														$mehet = true;
														break;
													}
												}
											}
											if ($mehet) break;
										}


									} else break;

									if ($mehet) break;
								}
							} else break;
						}
					}
					if (!$mehet) {
						$redirect = true;
						$error = $this->language->get('error_meret_szin');
					}
                } else {
                    $redirect = true;
                    $error = $this->language->get('error_meret_szin');
                }
            }

            if (!$redirect) {

                $this->model_module_arajanlat->add($this->request->post['product_id'], $quantity, $option);

                $this->index();

			} elseif (isset($redirect) && $redirect && $this->request->get['termek_aloldal'] != "true") {
                $this->output = 'error_redirect-index.php?route=product/product&product_id='.$product_id.'-'.$error;


               /* $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']) . ($error ? ('&warning='.$error) : ''));*/
			} elseif (isset($redirect) ) {
                $this->output = 'error'.$error;

            }
		}
		
		$this->response->setOutput($this->output);
	}

    public function ajanlatotKerek() {

        $this->language->load('module/arajanlat');
        $this->load->model('module/arajanlat');
        $json = array();


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/close.png')) {
            $template_close = $this->config->get('config_template');
        } else {
            $template_close = 'default';
        }
        $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('account/arajanlat'));
        $json['template'] = $template_close;

        if (isset($this->session->data['arajanlat'])) {
            $this->model_module_arajanlat->writeDatabase($this->session->data['arajanlat']);
        }


        $this->response->setOutput(json_encode($json));

    }

    public function delete() {
        if (isset($this->request->request['product_key'])) {
            if (isset($this->session->data['arajanlat'][$this->request->request['product_key']])) {
                unset ($this->session->data['arajanlat'][$this->request->request['product_key']]);
                if (count($this->session->data['arajanlat']) == 0) {
                    unset ($this->session->data['arajanlat']);

                    if ( isset($this->session->data['arajanlat_neve']) ) {
                        unset ($this->session->data['arajanlat_neve']);
                    }
                    if ( isset($this->session->data['arajanlat_id']) ) {
                        unset ($this->session->data['arajanlat_id']);
                    }
                }
                $this->index();

            }
        }
        $this->response->setOutput($this->output);
    }

    public function deleteOption() {
        if (!empty($this->request->post['product_key']) && !empty($this->request->post['option_id']) ) {
            $this->load->model('module/arajanlat');
            $this->model_module_arajanlat->deleteOption($this->request->post['product_key'],$this->request->post['option_id'],$this->request->post['szin_meret_szukseges']);


            $this->index();
        }
        $this->response->setOutput($this->output);
    }

    public function deleteAll() {
            if (isset($this->session->data['arajanlat']) ) {
                unset ($this->session->data['arajanlat']);
            }

            if ( isset($this->session->data['arajanlat_neve']) ) {
                unset ($this->session->data['arajanlat_neve']);
            }
            if ( isset($this->session->data['arajanlat_id']) ) {
                unset ($this->session->data['arajanlat_id']);
            }

        $this->response->setOutput('');
    }

    public function mennyiseg() {
        if (isset($this->request->request['product_key'])) {
            if (isset($this->session->data['arajanlat'][$this->request->request['product_key']])) {
                $this->session->data['arajanlat'][$this->request->request['product_key']] = $this->request->request['mennyiseg'];
                $this->index();

            }
        }
        $this->response->setOutput($this->output);

    }

}
?>