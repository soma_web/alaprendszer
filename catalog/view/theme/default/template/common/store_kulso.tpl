<?php echo $header; ?>
<?php echo $content_top; ?>

<?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/felnott.css')) { ?>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/felnott.css" />
<?php } else { ?>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE?>/default/stylesheet/felnott.css" />
<?php } ?>

<?php if (isset($slider) && !empty($slider) && $slider['slider'] == "1") { ?>
<script type="text/javascript" src="catalog/view/javascript/contentflow/contentflow.js" media="screen"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/contentflow/contentflow.css" />
<?php } ?>

<div id="aruhaz_valaszt">
    <div class="felnott" >
        <?php if(isset($store_text_status) && !$store_text_status) { ?>
            <div class="box-heading" style="text-align: center;"><?php echo $heading_title; ?></div>
            <div class="box-content" style="text-align: center;">
                <p><?php echo $text_store; ?></p>
        <?php } ?>

            <?php foreach ($stores as $store) { ?>
                <div class="stooore">
                    <div class="store-title"><?php echo $store['name']?></div>
                    <div class="store-outer" style="height: <?php echo (isset($image_height) && $image_height) ? $image_height+8 : 288 ?>px; width: <?php echo (isset($image_width) && $image_width) ? $image_width+8 : 288 ?>px" >
                        <a class="store" href="<?php echo $store['url']?>">

                            <img class="content" src="<?php echo $store['image']?>" alt='<?php echo $store['name']?>' title='<?php echo $store['name']?>'>

                        </a>

                    </div>
                    <?php if(isset($slider['title']) && $slider['title']) { ?>
                        <a class="title" href="<?php echo $store['url']?>"><span><?php echo $store['name']?></span></a>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if (!empty($egyeb_link)) { ?>
                <div class="stooore">
                    <div class="store-title"><?php echo $egyeb_link_title?></div>

                    <div class="store-outer" style="height: <?php echo (isset($image_height) && $image_height) ? $image_height+8 : 288 ?>px; width: <?php echo (isset($image_width) && $image_width) ? $image_width+8 : 288 ?>px" >
                        <a class="store" href="<?php echo $egyeb_link?>">

                            <img class="content" src="<?php echo $egyeb_link_image?>" alt='<?php echo $egyeb_link_title?>' title='<?php echo $egyeb_link_title?>'>

                        </a>

                    </div>

                </div>
            <?php } ?>

            </div>
    </div>


</div>
<script>
    /* Kategória menü eltűntetése, az összehasonlítás és a hármas menü eltűntetése */
    $(document).ready(function(){
        //$('#content').css("display","none");

        $('#menu-kategoriak').css("display","none");
        $('.product-compare').css("display","none");
        $('.menu-three').css("display","none");
        $('.price-calculator').css("display","none");

    });

</script>

<?php echo $footer; ?>
