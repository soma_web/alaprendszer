<?php
// HTTP

define('HTTP_SERVER', 'http://video.nmsnet.hu/');
define('HTTP_IMAGE', 'http://video.nmsnet.hu/image/');
define('HTTP_ADMIN', 'http://video.nmsnet.hu/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://video.nmsnet.hu/');
define('HTTPS_IMAGE', 'http://video.nmsnet.hu/image/');

// DIR
define('DIR_ARUHAZ', '/home/videop/public_html/');
define('DIR_APPLICATION', '/home/videop/public_html/catalog/');
define('DIR_SYSTEM', '/home/videop/public_html/system/');
define('DIR_DATABASE', '/home/videop/public_html/system/database/');
define('DIR_LANGUAGE', '/home/videop/public_html/catalog/language/');
define('DIR_TEMPLATE', 'catalog/view/theme/');
define('DIR_CONFIG', '/home/videop/public_html/system/config/');
define('DIR_IMAGE_NYOMTAT', 'image/');
define('DIR_IMAGE', '/home/videop/public_html/image/');
define('DIR_CACHE', '/home/videop/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/videop/public_html/download/');
define('DIR_LOGS', '/home/videop/public_html/system/logs/');
define('DIR_TEMPLATE_IMAGE', '/catalog/view/theme/');

// DB

define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'videop');
define('DB_PASSWORD', 'vid_119');
define('DB_DATABASE', 'videop_video');
define('DB_PREFIX', 'hungaro_');

?>
