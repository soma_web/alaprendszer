<?php
class ControllerCommonHeader extends Controller {
    protected function index() {
        if ($this->config->get('ne_key')) {
            $this->load->language('module/ne');
            $this->data['text_ne_email']        = $this->language->get('text_ne_email');
            $this->data['text_ne_draft']        = $this->language->get('text_ne_draft');
            $this->data['text_ne_marketing']    = $this->language->get('text_ne_marketing');
            $this->data['text_ne_subscribers']  = $this->language->get('text_ne_subscribers');
            $this->data['text_ne_stats']        = $this->language->get('text_ne_stats');
            $this->data['text_ne_robot']        = $this->language->get('text_ne_robot');
            $this->data['text_ne_template']     = $this->language->get('text_ne_template');
            $this->data['text_ne_subscribe_box'] = $this->language->get('text_ne_subscribe_box');
            $this->data['text_ne_blacklist']    = $this->language->get('text_ne_blacklist');
            $this->data['text_ne_support']      = $this->language->get('text_ne_support');
            $this->data['text_ne_support_register'] = $this->language->get('text_ne_support_register');
            $this->data['text_ne_support_login'] = $this->language->get('text_ne_support_login');
            $this->data['text_ne_support_dashboard'] = $this->language->get('text_ne_support_dashboard');
            $this->data['text_ne']              = $this->language->get('text_ne');
            $this->data['text_ne_update_check'] = $this->language->get('text_ne_update_check');
        }
        $this->data['title'] = $this->document->getTitle();

        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $this->data['base'] = HTTPS_SERVER;
        } else {
            $this->data['base'] = HTTP_SERVER;
        }


        $this->data['description'] = $this->document->getDescription();
        $this->data['keywords'] = $this->document->getKeywords();
        $this->data['links'] = $this->document->getLinks();
        $this->data['styles'] = $this->document->getStyles();
        $this->data['scripts'] = $this->document->getScripts();
        $this->data['lang'] = $this->language->get('code');
        $this->data['direction'] = $this->language->get('direction');
        $this->data['distinition'] = isset($this->session->data['in_cart']) ? $this->session->data['in_cart'] : '';

        $this->load->language('common/header');

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_affiliate']           = $this->language->get('text_affiliate');
        $this->data['text_attribute']           = $this->language->get('text_attribute');
        $this->data['text_csv_attribute']       = $this->language->get('text_attribute');
        $this->data['text_attribute_group']     = $this->language->get('text_attribute_group');
        $this->data['text_csv_attribute_group'] = $this->language->get('text_attribute_group');
        $this->data['text_filter']              = $this->language->get('text_filter');
        $this->data['text_filter_group']        = $this->language->get('text_filter_group');
        $this->data['text_backup']              = $this->language->get('text_backup');
        $this->data['text_export']              = $this->language->get('text_export');
        $this->data['text_banner']              = $this->language->get('text_banner');
        $this->data['text_megjelenito']         = $this->language->get('text_megjelenito');
        $this->data['text_megjelenito_header']  = $this->language->get('text_megjelenito_header');
        $this->data['text_integration_kapcsolat'] = $this->language->get('text_integration_kapcsolat');
        $this->data['text_integration_export']  = $this->language->get('text_integration_export');
        $this->data['text_integration_import']  = $this->language->get('text_integration_import');
        $this->data['text_catalog']             = $this->language->get('text_catalog');
        $this->data['text_category']            = $this->language->get('text_category');
        $this->data['text_confirm']             = $this->language->get('text_confirm');
        $this->data['text_country']             = $this->language->get('text_country');
        $this->data['text_coupon']              = $this->language->get('text_coupon');
        $this->data['text_currency']            = $this->language->get('text_currency');
        $this->data['text_address']             = $this->language->get('text_address');
        $this->data['text_customer']            = $this->language->get('text_customer');
        $this->data['text_csv_customer']        = $this->language->get('text_customer');
        $this->data['text_customer_report']     = $this->language->get('text_customer');
        $this->data['text_customer_group']      = $this->language->get('text_customer_group');
        $this->data['text_customer_blacklist']  = $this->language->get('text_customer_blacklist');
        $this->data['text_sale']                = $this->language->get('text_sale');
        $this->data['text_design']              = $this->language->get('text_design');
        $this->data['text_documentation']       = $this->language->get('text_documentation');
        $this->data['text_download']            = $this->language->get('text_download');
        $this->data['text_error_log']           = $this->language->get('text_error_log');
        $this->data['text_biztositas']          = $this->language->get('text_biztositas');
        $this->data['text_extension']           = $this->language->get('text_extension');
        $this->data['text_feed']                = $this->language->get('text_feed');
        $this->data['text_front']               = $this->language->get('text_front');
        $this->data['text_geo_zone']            = $this->language->get('text_geo_zone');
        $this->data['text_dashboard']           = $this->language->get('text_dashboard');
        $this->data['text_help']                = $this->language->get('text_help');
        $this->data['text_information']         = $this->language->get('text_information');
        $this->data['text_fogalom_magyarazat']  = $this->language->get('text_fogalom_magyarazat');
        $this->data['text_language']            = $this->language->get('text_language');
        $this->data['text_layout']              = $this->language->get('text_layout');
        $this->data['text_localisation']        = $this->language->get('text_localisation');
        $this->data['text_logout']              = $this->language->get('text_logout');
        $this->data['text_contact']             = $this->language->get('text_contact');
        $this->data['text_manufacturer']        = $this->language->get('text_manufacturer');
        $this->data['text_module']              = $this->language->get('text_module');
        $this->data['text_csv_option']          = $this->language->get('text_option');
        $this->data['text_option']              = $this->language->get('text_option');
        $this->data['text_csv_option_value']    = $this->language->get('text_option_value');
        $this->data['text_szin']                = $this->language->get('text_szin');
        $this->data['text_option_szin']         = $this->language->get('text_option_szin');
        $this->data['text_csv_option_szin']     = $this->language->get('text_option_szin');
        $this->data['text_option_szin_group']   = $this->language->get('text_option_szin_group');
        $this->data['text_csv_option_szin_group']= $this->language->get('text_option_szin_group');
        $this->data['text_csv_option_szin_to_group']= $this->language->get('text_option_szin_to_group');
        $this->data['text_order']               = $this->language->get('text_order');
        $this->data['text_pre_order']           = $this->language->get('text_pre_order');
        $this->data['text_order_status']        = $this->language->get('text_order_status');
        $this->data['text_opencart']            = $this->language->get('text_opencart');
        $this->data['text_payment']             = $this->language->get('text_payment');
        $this->data['text_product']             = $this->language->get('text_product');
        $this->data['text_product_report']      = $this->language->get('text_product');
        $this->data['text_csv_product']         = $this->language->get('text_product');
        $this->data['text_product_special']     = $this->language->get('text_product_special');
        $this->data['text_product_customer_special'] = $this->language->get('text_product_customer_special');
        $this->data['text_product_discount']    = $this->language->get('text_product_discount');
        $this->data['text_product_option']      = $this->language->get('text_product_option');
        $this->data['text_product_option_value']= $this->language->get('text_product_option_value');
        $this->data['text_product_relations']   = $this->language->get('text_product_relations');
        $this->data['text_elhelyezkedes']       = $this->language->get('text_elhelyezkedes');
        $this->data['text_elhelyezkedes_group'] = $this->language->get('text_elhelyezkedes_group');
        $this->data['text_kiemelesek']          = $this->language->get('text_kiemelesek');
        $this->data['text_fizetes_allapot']     = $this->language->get('text_fizetes_allapot');
        $this->data['text_fizetes_elbiralas_statusz'] = $this->language->get('text_fizetes_elbiralas_statusz');
        $this->data['text_customer_product']    = $this->language->get('text_customer_product');
        $this->data['text_reports']             = $this->language->get('text_reports');
        $this->data['text_report_sale_order']   = $this->language->get('text_report_sale_order');
        $this->data['text_report_sale_tax']     = $this->language->get('text_report_sale_tax');
        $this->data['text_report_sale_shipping'] = $this->language->get('text_report_sale_shipping');
        $this->data['text_report_sale_return']  = $this->language->get('text_report_sale_return');
        $this->data['text_report_sale_coupon']  = $this->language->get('text_report_sale_coupon');
        $this->data['text_report_product_viewed'] = $this->language->get('text_report_product_viewed');
        $this->data['text_report_product_purchased'] = $this->language->get('text_report_product_purchased');
        $this->data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
        $this->data['text_report_customer_reward'] = $this->language->get('text_report_customer_reward');
        $this->data['text_report_customer_credit'] = $this->language->get('text_report_customer_credit');
        $this->data['text_report_affiliate_commission'] = $this->language->get('text_report_affiliate_commission');
        $this->data['text_report_sale_return']  = $this->language->get('text_report_sale_return');
        $this->data['text_report_product_purchased'] = $this->language->get('text_report_product_purchased');
        $this->data['text_report_product_viewed'] = $this->language->get('text_report_product_viewed');
        $this->data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
        $this->data['text_report_customer_wishlist'] = $this->language->get('text_report_customer_wishlist');
        $this->data['text_review']              = $this->language->get('text_review');

        $this->data['text_csv']                 = $this->language->get('text_csv');
        $this->data['text_description'] = $this->language->get('text_description');


        //QA
        $this->data['text_qa']                  = $this->language->get('text_qa');
        //QA END
        $this->data['text_return']              = $this->language->get('text_return');
        $this->data['text_return_action']       = $this->language->get('text_return_action');
        $this->data['text_return_reason']       = $this->language->get('text_return_reason');
        $this->data['text_return_status']       = $this->language->get('text_return_status');
        $this->data['text_support']             = $this->language->get('text_support');
        $this->data['text_shipping']            = $this->language->get('text_shipping');
        $this->data['text_setting']             = $this->language->get('text_setting');
        $this->data['text_stock_status']        = $this->language->get('text_stock_status');
        $this->data['text_system']              = $this->language->get('text_system');
        $this->data['text_tax']                 = $this->language->get('text_tax');
        $this->data['text_tax_class']           = $this->language->get('text_tax_class');
        $this->data['text_tax_rate']            = $this->language->get('text_tax_rate');
        $this->data['text_total']               = $this->language->get('text_total');
        $this->data['text_user']                = $this->language->get('text_user');
        $this->data['text_user_group']          = $this->language->get('text_user_group');




        $this->data['text_users']               = $this->language->get('text_users');
        $this->data['text_voucher']             = $this->language->get('text_voucher');
        $this->data['text_voucher_theme']       = $this->language->get('text_voucher_theme');
        $this->data['text_weight_class']        = $this->language->get('text_weight_class');
        $this->data['text_packaging_class']     = $this->language->get('text_packaging_class');
        $this->data['text_length_class']        = $this->language->get('text_length_class');
        $this->data['text_zone']                = $this->language->get('text_zone');

        $this->load->model('setting/extension');
        $extensions = $this->model_setting_extension->getInstalled('module');
        $no_modules = $this->model_setting_extension->getNoModule('module');
        $megjelen = $this->data['distinition'];
        //Show or not Hirlevel in menu
        $hirlevel = true;
        if (in_array("ne",$no_modules)) {
            $hirlevel = false;
        } elseif (!in_array("ne",$extensions) ) {
            $hirlevel = false;
        }

        $this->data['hirlevel'] = $hirlevel;

        //Show or not Blog in menu
        $pavblog_installed = true;
        if (in_array("pavblog",$no_modules)) {
            $pavblog_installed = false;
        } elseif (!in_array("pavblog",$extensions) ) {
            $pavblog_installed = false;
        }

        $this->data['pavblog_installed'] = $pavblog_installed;


       /* $this->data['pavblog_installed'] = false;
        if(in_array("pavblog", $extensions)){
            $this->data['pavblog_installed'] = true;
        }*/

        $this->data['text_pavblog_manage_cate']     = $this->language->get('text_pavblog_manage_cate');
        $this->data['text_pavblog_manage_blog']     = $this->language->get('text_pavblog_manage_blog');
        $this->data['text_pavblog_add_blog']        = $this->language->get('text_pavblog_add_blog');
        $this->data['text_pavblog_manage_comment']  = $this->language->get('text_pavblog_manage_comment');
        $this->data['text_pavblog_general_setting'] = $this->language->get('text_pavblog_general_setting');
        $this->data['text_pavblog_front_mods']      = $this->language->get('text_pavblog_front_mods');
        $this->data['text_pavblog_blog']            = $this->language->get('text_pavblog_blog');
        $this->data['text_pavblog_category']        = $this->language->get('text_pavblog_category');
        $this->data['text_pavblog_comment']         = $this->language->get('text_pavblog_comment');
        $this->data['text_pavblog_latest']          = $this->language->get('text_pavblog_latest');



        $this->data['text_newssubscribe']               = $this->language->get('text_newssubscribe');
        $this->data['text_customer_eletkor']            = $this->language->get('text_customer_eletkor');
        $this->data['text_customer_vegzettseg']         = $this->language->get('text_customer_vegzettseg');
        $this->data['text_report_sale_catgories']       = $this->language->get('text_report_sale_catgories');
        $this->data['text_report_sale_uploaders']       = $this->language->get('text_report_sale_uploaders');
        $this->data['text_disabledproduct']             = $this->language->get('text_disabledproduct');
        $this->data['text_disabledproduct']             = $this->language->get('text_disabledproduct');
        $this->data['megjelenit_admin_altalanos']       = $this->config->get('megjelenit_admin_altalanos');
        $this->data['megjelenit_termekful']             = $this->config->get('megjelenit_admin_termekful');
        $this->data['megjelenit_eletkor_regisztracio']  = $this->config->get('megjelenit_eletkor_regisztracio');
        $this->data['megjelenit_regisztracio_iskolai_vegzettseg']     = $this->config->get('megjelenit_regisztracio_iskolai_vegzettseg');
        $this->data['megjelenit_racsful']               = $this->config->get('megjelenit_racsful');
        $this->data['megjelenit_listaful']              = $this->config->get('megjelenit_listaful');
        $this->data['text_faq']                         = $this->language->get('text_faq');
        $this->data['text_szuperadmin']                 = 'Szuperadmin';

        $this->load->language('LanguageEditor/LanguageEditor');
        $this->data['menu_language'] = $this->language->get('menu_language');
        $this->data['user_name'] = $this->user->getUserName();


        if (!$this->user->isLogged() || !isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token'])) {
            $this->data['logged'] = '';

            $this->data['home'] = $this->url->link('common/login', '', 'SSL');
        } else {
            $this->data['logged'] = sprintf($this->language->get('text_logged'), $this->user->getUserName());


            $this->data['home']                             = $this->permission("common/home") ? $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL') : '';



            /* ------ KATALÓGUS ------ */

            $this->data['category']         = $this->permission("catalog/category") ? $this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL') : "";
            $this->data['product']          = $this->permission("catalog/product") ? $this->url->link('catalog/product', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['disabledproduct']  = $this->permission("catalog/product") ? $this->url->link('catalog/product', 'token=' . $this->session->data['token']."&filter_status=0", 'SSL') : '';
            $this->data['cusproduct']       = $this->permission("catalog/product/customerproducts") ? $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'], 'SSL') : '';

            $this->data['filter']       = $this->permission("catalog/filter") ? $this->url->link('catalog/filter', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['filter_group'] = $this->permission("catalog/filter_group") ? $this->url->link('catalog/filter_group', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['filter']) && empty($this->data['filter_group'])) {
                $this->data['text_filter'] = '';
            }

            $this->data['attribute']                        = $this->permission("catalog/attribute") ? $this->url->link('catalog/attribute', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['attribute_group']                  = $this->permission("catalog/attribute_group") ? $this->url->link('catalog/attribute_group', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['attribute']) && empty($this->data['attribute_group'])) {
                $this->data['text_attribute'] = '';
            }

            $this->data['option']               = $this->permission("catalog/option") ? $this->url->link('catalog/option', 'token=' . $this->session->data['token'], 'SSL') : '';

            $this->data['option_szin']          = $this->permission("catalog/option_szin") ? $this->url->link('catalog/option_szin', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['option_szin_group']    = $this->permission("catalog/option_szin_group") ? $this->url->link('catalog/option_szin_group', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['option_szin']) && empty($this->data['option_szin_group'])) {
                $this->data['text_option_szin'] = '';
            }

            $this->data['manufacturer']         = $this->permission("catalog/manufacturer") ? $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['download']     = $this->permission("catalog/download") ? $this->url->link('catalog/download', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['review']               = $this->permission("catalog/review") ? $this->url->link('catalog/review', 'token=' . $this->session->data['token'], 'SSL') : '';
            if ($this->config->get('qa_status')) {
                $this->data['qa'] = $this->permission("catalog/qa") ? $this->url->link('catalog/qa', 'token=' . $this->session->data['token'], 'SSL') : '';
            }

            $this->data['information']  = $this->permission("catalog/information") ? $this->url->link('catalog/information', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (isset($this->data['megjelenit_admin_altalanos']['fogalom_magyarazat']) && $this->data['megjelenit_admin_altalanos']['fogalom_magyarazat']) {
                $this->data['fogalom_magyarazat'] = $this->permission("catalog/fogalom_magyarazat") ? $this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'], 'SSL') : '';
            }
            $this->data['elhelyezkedes']             = $this->permission("catalog/elhelyezkedes") ? $this->url->link('catalog/elhelyezkedes', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['elhelyezkedes_group']       = $this->permission("catalog/elhelyezkedes_alcsoport") ? $this->url->link('catalog/elhelyezkedes_alcsoport', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['kiemelesek']                = $this->permission("catalog/kiemelesek") ? $this->url->link('catalog/kiemelesek', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['penzugyi_status']           = $this->permission("catalog/penzugyi_status") ? $this->url->link('catalog/penzugyi_status', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['fizetes_elbiralas_statusz'] = $this->permission("catalog/fizetes_elbiralas") ? $this->url->link('catalog/fizetes_elbiralas', 'token=' . $this->session->data['token'], 'SSL') : '';

            if (empty($this->data['elhelyezkedes']) && empty($this->data['elhelyezkedes_group']) && empty($this->data['kiemelesek']) && empty($this->data['penzugyi_status']) && empty($this->data['fizetes_elbiralas_statusz'])) {
                $this->data['text_elhelyezkedes'] = '';
            }

            if (empty($this->data['category']) &&
                empty($this->data['product']) &&
                empty($this->data['disabledproduct']) &&
                empty($this->data['cusproduct']) &&
                empty($this->data['filter']) &&
                empty($this->data['filter_group']) &&
                empty($this->data['attribute']) &&
                empty($this->data['attribute_group']) &&
                empty($this->data['option']) &&
                empty($this->data['option_szin']) &&
                empty($this->data['option_szin_group']) &&
                empty($this->data['manufacturer']) &&
                empty($this->data['download']) &&
                empty($this->data['review']) &&
                empty($this->data['qa']) &&
                empty($this->data['fogalom_magyarazat']) &&
                empty($this->data['elhelyezkedes_group']) &&
                empty($this->data['kiemelesek']) &&
                empty($this->data['penzugyi_status']) &&
                empty($this->data['fizetes_elbiralas_statusz']) ) {
                $this->data['text_catalog'] = '';
            }
            /* ------ KATALÓGUS vége ------ */


            /* ------ BŐVÍTMÉNYEK ------ */
            $this->data['module']               = $this->permission("extension/module") ? $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['shipping']     = $this->permission("extension/shipping") ? $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['payment']              = $this->permission("extension/payment") ? $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['total']        = $this->permission("extension/total") ? $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['feed']         = $this->permission("extension/feed") ? $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['biztositas']           = $this->permission("extension/biztositas") ? $this->url->link('extension/biztositas', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['faq']          = $this->permission("extension/faq") ? $this->url->link('extension/faq', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['module']) &&
                empty($this->data['shipping']) &&
                empty($this->data['payment']) &&
                empty($this->data['total']) &&
                empty($this->data['feed']) &&
                empty($this->data['biztositas']) &&
                empty($this->data['faq']) ) {
                $this->data['text_extension'] = '';
            }
            /* ------ BŐVÍTMÉNYEK vége ------ */



            /* ------ ELADÁSOK ------ */

            $this->data['text_sale_return']     = $this->language->get('text_return');
            $this->data['order']                = $this->permission("sale/order") ? $this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['pre_order']            = $this->permission("sale/pre_order") ? $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['return']               = $this->permission("sale/return") ? $this->url->link('sale/return', 'token=' . $this->session->data['token'], 'SSL') : '';

            $this->data['customer']         = $this->permission("sale/customer") ? $this->url->link('sale/customer', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['customer_group']   = $this->permission("sale/customer_group") ? $this->url->link('sale/customer_group', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['customer_blacklist']=$this->permission("sale/customer_blacklist") ? $this->url->link('sale/customer_blacklist', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['eletkor']      = $this->permission("sale/eletkor") ? $this->url->link('sale/eletkor', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['vegzettseg']   = $this->permission("sale/vegzettseg") ? $this->url->link('sale/vegzettseg', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['customer']) &&
                empty($this->data['customer_group']) &&
                empty($this->data['customer_blacklist']) &&
                empty($this->data['eletkor']) &&
                empty($this->data['vegzettseg']) ) {
                $this->data['text_customer'] = '';
            }

            $this->data['affiliate']                        = $this->permission("sale/affiliate") ? $this->url->link('sale/affiliate', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['coupon']           = $this->permission("sale/coupon") ? $this->url->link('sale/coupon', 'token=' . $this->session->data['token'], 'SSL') : '';

            $this->data['voucher']      = $this->permission("sale/voucher") ? $this->url->link('sale/voucher', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['voucher_theme']= $this->permission("sale/voucher_theme") ? $this->url->link('sale/voucher_theme', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['voucher']) &&
                empty($this->data['voucher_theme']) ) {
                $this->data['text_voucher'] = '';
            }
            $this->data['newssubscribe']= $this->permission("sale/newssubscribers") ? $this->url->link('sale/newssubscribers', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['contact']              = $this->permission("sale/contact") ? $this->url->link('sale/contact', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['order']) &&
                empty($this->data['pre_order']) &&
                empty($this->data['return']) &&
                empty($this->data['customer']) &&
                empty($this->data['customer_group']) &&
                empty($this->data['customer_blacklist']) &&
                empty($this->data['eletkor']) &&
                empty($this->data['vegzettseg']) &&
                empty($this->data['affiliate']) &&
                empty($this->data['coupon']) &&
                empty($this->data['voucher']) &&
                empty($this->data['voucher_theme']) &&
                empty($this->data['newssubscribe']) &&
                empty($this->data['contact']) ) {
                $this->data['text_sale'] = '';
            }
            /* ------ ELADÁSOK vége ------ */


            /* ------ RENDSZER ------ */
            $this->data['setting']                          = $this->permission("setting/store") ? $this->url->link('setting/store', 'token=' . $this->session->data['token'], 'SSL') : '';

            $this->data['layout']                           = $this->permission("design/layout") ? $this->url->link('design/layout', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['banner']                           = $this->permission("design/banner") ? $this->url->link('design/banner', 'token=' . $this->session->data['token'], 'SSL') : "";
            $this->data['megjelenito']                      = $this->permission("design/megjelenito") && ($this->data['user_name'] == "admin" || $this->data['user_name'] == "somaweb" ) && !empty($megjelen) ? $this->url->link('design/megjelenito', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['megjelenito_frontend']             = $this->permission("design/megjelenito_frontend")  && ($this->data['user_name'] == "admin" || $this->data['user_name'] == "somaweb" ) && !empty($megjelen)  ? $this->url->link('design/megjelenito_frontend', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['megjelenito_header']               = $this->permission("design/megjelenito_header")  && ($this->data['user_name'] == "admin" || $this->data['user_name'] == "somaweb" ) && !empty($megjelen)  ? $this->url->link('design/megjelenito_header', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['megjelenito_termek']               = $this->permission("design/megjelenito_termek")  && ($this->data['user_name'] == "admin" || $this->data['user_name'] == "somaweb" ) && !empty($megjelen)  ? $this->url->link('design/megjelenito_termek', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['megjelenito_lista']                = $this->permission("design/megjelenito_lista")  && ($this->data['user_name'] == "admin" || $this->data['user_name'] == "somaweb" ) && !empty($megjelen)  ? $this->url->link('design/megjelenito_lista', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['megjelenito_racs']                 = $this->permission("design/megjelenito_racs")  && ($this->data['user_name'] == "admin" || $this->data['user_name'] == "somaweb" ) && !empty($megjelen)  ? $this->url->link('design/megjelenito_racs', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['integration_kapcsolat']            = $this->permission("design/megjelenito_csv")  && ($this->data['user_name'] == "admin" || $this->data['user_name'] == "somaweb" ) && !empty($megjelen)  ? $this->url->link('design/megjelenito_csv', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['integration_kapcsolat_export_xml'] = $this->permission("design/megjelenito_xml")  && ($this->data['user_name'] == "admin" || $this->data['user_name'] == "somaweb" ) && !empty($megjelen)  ? $this->url->link('design/megjelenito_xml', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['megjelenito']) &&
                empty($this->data['megjelenito_frontend']) &&
                empty($this->data['megjelenito_header']) &&
                empty($this->data['megjelenito_termek']) &&
                empty($this->data['megjelenito_lista']) &&
                empty($this->data['megjelenito_racs']) &&
                empty($this->data['integration_kapcsolat']) &&
                empty($this->data['integration_kapcsolat_export_xml']) ) {
                $this->data['text_szuperadmin'] = '';
            }

            if (empty($this->data['layout']) &&
                empty($this->data['banner']) &&
                empty($this->data['text_szuperadmin']) ) {
                $this->data['text_design'] = '';
            }

            $this->data['user'] = $this->permission("user/user") ? $this->url->link('user/user', 'token=' . $this->session->data['token'], 'SSL') : '';
            if ($this->data['user_name'] == "admin" || $this->data['user_name'] == "somaweb") {
                $this->data['user_group']   = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], 'SSL');
            } else {
                $this->data['user_group']   = $this->permission("user/user_permission") ? $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], 'SSL') : '';
            }
            if (empty($this->data['user']) &&
                empty($this->data['user_group']) ) {
                $this->data['text_users'] = '';
            }




            $this->data['language']         = $this->permission("localisation/language") ? $this->url->link('localisation/language', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['currency']         = $this->permission("localisation/currency") ? $this->url->link('localisation/currency', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['stock_status']     = $this->permission("localisation/stock_status") ? $this->url->link('localisation/stock_status', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['order_status']     = $this->permission("localisation/order_status") ? $this->url->link('localisation/order_status', 'token=' . $this->session->data['token'], 'SSL') : '';

            $this->data['return_status']    = $this->permission("localisation/return_status") ? $this->url->link('localisation/return_status', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['return_action']    = $this->permission("localisation/return_action") ? $this->url->link('localisation/return_action', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['return_reason']    = $this->permission("localisation/return_reason") ? $this->url->link('localisation/return_reason', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['return_status']) &&
                empty($this->data['return_action']) &&
                empty($this->data['return_reason']) ) {
                $this->data['text_return'] = '';
            }

            $this->data['country']          = $this->permission("localisation/country") ? $this->url->link('localisation/country', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['zone']             = $this->permission("localisation/zone") ? $this->url->link('localisation/zone', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['geo_zone']         = $this->permission("localisation/geo_zone") ? $this->url->link('localisation/geo_zone', 'token=' . $this->session->data['token'], 'SSL') : '';

            $this->data['tax_class']        = $this->permission("localisation/tax_class") ? $this->url->link('localisation/tax_class', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['tax_rate']         = $this->permission("localisation/tax_rate") ? $this->url->link('localisation/tax_rate', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['tax_class']) &&
                empty($this->data['tax_class']) ) {
                $this->data['text_tax'] = '';
            }

            $this->data['length_class']     = $this->permission("localisation/length_class") ? $this->url->link('localisation/length_class', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['weight_class']     = $this->permission("localisation/weight_class") ? $this->url->link('localisation/weight_class', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['packaging_class']  = $this->permission("localisation/packaging_class") ? $this->url->link('localisation/packaging_class', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['language']) &&
                empty($this->data['currency']) &&
                empty($this->data['stock_status']) &&
                empty($this->data['order_status']) &&
                empty($this->data['text_return']) &&
                empty($this->data['country']) &&
                empty($this->data['zone']) &&
                empty($this->data['geo_zone']) &&
                empty($this->data['text_tax']) &&
                empty($this->data['length_class']) &&
                empty($this->data['weight_class']) &&
                empty($this->data['packaging_class']) ) {
                $this->data['text_localisation'] = '';
            }




            $this->data['error_log']        = $this->permission("tool/error_log") ? $this->url->link('tool/error_log', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['backup']           = $this->permission("tool/backup") ? $this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['export']           = $this->permission("tool/export") ? $this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['link_language']    = $this->permission("LanguageEditor/LanguageEditor") ? $this->url->link('LanguageEditor/LanguageEditor', 'token=' . $this->session->data['token']) : '';

            // CSV export -import
            $this->data['csv_product']              = $this->permission("csv/product") ? $this->url->link('csv/product', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_product_relations']    = $this->permission("csv/product_relations") ? $this->url->link('csv/product_relations', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_product_option']       = $this->permission("csv/product_option") ? $this->url->link('csv/product_option', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_product_special']      = $this->permission("csv/product_special") ? $this->url->link('csv/product_special', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_product_discount']     = $this->permission("csv/product_discount") ? $this->url->link('csv/product_discount', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_product_customer_special']=$this->permission("csv/product_customer_special") ? $this->url->link('csv/product_customer_special', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['csv_product']) &&
                empty($this->data['csv_product_relations']) &&
                empty($this->data['csv_product_option']) &&
                empty($this->data['csv_product_special']) &&
                empty($this->data['csv_product_discount']) &&
                empty($this->data['csv_product_customer_special']) ) {
                $this->data['text_csv_product'] = '';
            }
            $this->data['csv_category']             = $this->permission("csv/category") ? $this->url->link('csv/category', 'token=' . $this->session->data['token'], 'SSL') : '';

            $this->data['csv_attribute_group']      = $this->permission("csv/attribute_group") ? $this->url->link('csv/attribute_group', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_attribute']            = $this->permission("csv/attribute") ? $this->url->link('csv/attribute', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['csv_attribute_group']) &&
                empty($this->data['csv_attribute']) ) {
                $this->data['text_csv_attribute'] = '';
            }

            $this->data['csv_option']               = $this->permission("csv/option") ? $this->url->link('csv/option', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_option_value']         = $this->permission("csv/option_value") ? $this->url->link('csv/option_value', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_option_szin']          = $this->permission("csv/option_szin") ? $this->url->link('csv/option_szin', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_option_szin_group']    = $this->permission("csv/option_szin_group") ? $this->url->link('csv/option_szin_group', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_option_szin_to_group'] = $this->permission("csv/option_szin_to_group") ? $this->url->link('csv/option_szin_to_group', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['csv_option']) &&
                empty($this->data['csv_option_value']) &&
                empty($this->data['csv_option_szin']) &&
                empty($this->data['csv_option_szin_group']) &&
                empty($this->data['csv_option_szin_to_group']) ) {
                $this->data['text_csv_option'] = '';
            }

            $this->data['csv_customer']             = $this->permission("csv/customer") ? $this->url->link('csv/customer', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['csv_address']              = $this->permission("csv/address") ? $this->url->link('csv/address', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['csv_customer']) &&
                empty($this->data['csv_address']) ) {
                $this->data['text_csv_customer'] = '';
            }

            if (empty($this->data['text_csv_product']) &&
                empty($this->data['csv_category']) &&
                empty($this->data['text_csv_attribute']) &&
                empty($this->data['text_csv_option']) &&
                empty($this->data['text_csv_customer']) ) {
                $this->data['text_csv'] = '';
            }

            if (empty($this->data['setting']) &&
                empty($this->data['layout']) &&
                empty($this->data['banner']) &&
                empty($this->data['megjelenito']) &&
                empty($this->data['megjelenito_frontend']) &&
                empty($this->data['megjelenito_header']) &&
                empty($this->data['megjelenito_termek']) &&
                empty($this->data['megjelenito_lista']) &&
                empty($this->data['megjelenito_racs']) &&
                empty($this->data['integration_kapcsolat']) &&
                empty($this->data['integration_kapcsolat_export_xml']) &&
                empty($this->data['integration_kapcsolat_export']) &&
                empty($this->data['integration_kapcsolat_import']) &&
                empty($this->data['user']) &&
                empty($this->data['user_group']) &&
                empty($this->data['language']) &&
                empty($this->data['currency']) &&
                empty($this->data['stock_status']) &&
                empty($this->data['order_status']) &&
                empty($this->data['return_status']) &&
                empty($this->data['return_action']) &&
                empty($this->data['return_reason']) &&
                empty($this->data['country']) &&
                empty($this->data['zone']) &&
                empty($this->data['geo_zone']) &&
                empty($this->data['tax_class']) &&
                empty($this->data['tax_rate']) &&
                empty($this->data['length_class']) &&
                empty($this->data['weight_class']) &&
                empty($this->data['packaging_class']) &&
                empty($this->data['error_log']) &&
                empty($this->data['backup']) &&
                empty($this->data['export']) &&
                empty($this->data['link_language']) &&
                empty($this->data['csv_product']) &&
                empty($this->data['csv_product_relations']) &&
                empty($this->data['csv_product_option']) &&
                empty($this->data['csv_product_special']) &&
                empty($this->data['csv_product_discount']) &&
                empty($this->data['csv_product_customer_special']) &&
                empty($this->data['csv_category']) &&
                empty($this->data['csv_attribute_group']) &&
                empty($this->data['csv_attribute']) &&
                empty($this->data['csv_option']) &&
                empty($this->data['csv_option_value']) &&
                empty($this->data['csv_option_szin']) &&
                empty($this->data['csv_option_szin_group']) &&
                empty($this->data['csv_option_szin_to_group']) &&
                empty($this->data['csv_customer']) &&
                empty($this->data['csv_address']) ) {
                $this->data['text_system'] = '';
            }

            /* ------ RENDSZER vége ------ */


            /* ------ HIRLEVÉL ------ */


            if ($this->config->get('ne_key')) {
                $this->data['ne_email']         = $this->permission("ne/newsletter") ? $this->url->link('ne/newsletter', 'token=' . $this->session->data['token'], 'SSL') : '';
                $this->data['ne_draft']         = $this->permission("ne/draft") ? $this->url->link('ne/draft', 'token=' . $this->session->data['token'], 'SSL') : '';
                $this->data['ne_marketing']     = $this->permission("ne/marketing") ? $this->url->link('ne/marketing', 'token=' . $this->session->data['token'], 'SSL') : '';
                $this->data['ne_subscribers']   = $this->permission("ne/subscribers") ? $this->url->link('ne/subscribers', 'token=' . $this->session->data['token'], 'SSL') : '';
                $this->data['ne_stats']         = $this->permission("ne/stats") ? $this->url->link('ne/stats', 'token=' . $this->session->data['token'], 'SSL') : '';
                $this->data['ne_robot']         = $this->permission("ne/schedule") ? $this->url->link('ne/schedule', 'token=' . $this->session->data['token'], 'SSL') : '';
                $this->data['ne_template']      = $this->permission("ne/template") ? $this->url->link('ne/template', 'token=' . $this->session->data['token'], 'SSL') : '';
                $this->data['ne_subscribe_box'] = $this->permission("ne/subscribe_box") ? $this->url->link('ne/subscribe_box', 'token=' . $this->session->data['token'], 'SSL') : '';
                $this->data['ne_blacklist']     = $this->permission("ne/blacklist") ? $this->url->link('ne/blacklist', 'token=' . $this->session->data['token'], 'SSL') : '';
                $this->data['ne_update_check']  = $this->permission("ne/check_update") ? $this->url->link('ne/check_update', 'token=' . $this->session->data['token'], 'SSL') : '';

                if (empty($this->data['ne_key']) &&
                    empty($this->data['ne_email']) &&
                    empty($this->data['ne_draft']) &&
                    empty($this->data['ne_marketing']) &&
                    empty($this->data['ne_subscribers']) &&
                    empty($this->data['ne_stats']) &&
                    empty($this->data['ne_robot']) &&
                    empty($this->data['ne_template']) &&
                    empty($this->data['ne_subscribe_box']) &&
                    empty($this->data['ne_update_check']) ) {
                    $this->data['text_ne'] = '';
                }
            }
            /* ------ HIRLEVÉL vége ------ */

            /* ------ JELENTÉSEK  ------ */

            $this->data['report_sale_order']    = $this->permission("report/sale_order") ? $this->url->link('report/sale_order', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_sale_tax']      = $this->permission("report/sale_tax") ? $this->url->link('report/sale_tax', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_sale_shipping'] = $this->permission("report/sale_shipping") ? $this->url->link('report/sale_shipping', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_sale_return']   = $this->permission("report/sale_return") ? $this->url->link('report/sale_return', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_sale_coupon']   = $this->permission("report/sale_coupon") ? $this->url->link('report/sale_coupon', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_sale_categories']=$this->permission("report/sale_categories") ? $this->url->link('report/sale_categories', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_sale_uploaders']= $this->permission("report/sale_uploaders") ? $this->url->link('report/sale_uploaders', 'token=' . $this->session->data['token'], 'SSL') : '';


            $this->data['report_product_viewed']= $this->permission("report/product_viewed") ? $this->url->link('report/product_viewed', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_product_purchased'] = $this->permission("report/product_purchased") ? $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['report_product_viewed']) &&
                empty($this->data['report_product_purchased']) ) {
                $this->data['text_product_report'] = '';
            }


            $this->data['report_customer_order']= $this->permission("report/customer_order") ? $this->url->link('report/customer_order', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_customer_reward']= $this->permission("report/customer_reward") ? $this->url->link('report/customer_reward', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_customer_credit']= $this->permission("report/customer_credit") ? $this->url->link('report/customer_credit', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['report_customer_wishlist'] = $this->permission("report/customer_wishlist") ? $this->url->link('report/customer_wishlist', 'token=' . $this->session->data['token'], 'SSL') : '';

            if (empty($this->data['report_customer_order']) &&
                empty($this->data['report_customer_reward']) &&
                empty($this->data['report_customer_credit']) &&
                empty($this->data['report_customer_wishlist']) ) {
                $this->data['text_customer_report'] = '';
            }

            $this->data['report_affiliate_commission'] = $this->permission("report/affiliate_commission") ? $this->url->link('report/affiliate_commission', 'token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['report_customer_order']) ){
                $this->data['report_affiliate_commission'] = '';
            }

            if (empty($this->data['report_sale_order']) &&
                empty($this->data['report_sale_tax']) &&
                empty($this->data['report_sale_shipping']) &&
                empty($this->data['report_sale_return']) &&
                empty($this->data['report_sale_coupon']) &&
                empty($this->data['report_sale_categories']) &&
                empty($this->data['report_sale_uploaders']) &&
                empty($this->data['text_product_report']) &&
                empty($this->data['text_customer_report']) &&
                empty($this->data['report_affiliate_commission']) ) {
                $this->data['text_reports'] = '';
            }
            /* ------ JELENTÉSEK vége ------ */


            /* ------ PAVBLOG  ------ */
            $this->data['pavblogs_category']            = $this->permission("module/pavblog") ? $this->url->link('module/pavblog/category', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['pavblogs_blogs']               = $this->permission("module/pavblog") ? $this->url->link('module/pavblog/blogs', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['pavblogs_add_blog']            = $this->permission("module/pavblog") ? $this->url->link('module/pavblog/blog', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['pavblogs_comments']            = $this->permission("module/pavblog") ? $this->url->link('module/pavblog/comments', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['pavblogs_general']             = $this->permission("module/pavblog") ? $this->url->link('module/pavblog/modules', 'token=' . $this->session->data['token'], 'SSL') : '';

            $this->data['pavblogs_category_mod']        = $this->permission("module/pavblog") ? $this->url->link('module/pavblog/frontmodules', 'mod="pavblogcategory"&token='  . $this->session->data['token'], 'SSL') : '';
            $this->data['pavblogs_latest_comment_mod']  = $this->permission("module/pavblog") ? $this->url->link('module/pavblog/frontmodules', 'mod="pavblogcomment"&token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['pavblogs_latest_mod']          = $this->permission("module/pavblog") ? $this->url->link('module/pavblog/frontmodules', 'mod="pavbloglatest"&token=' . $this->session->data['token'], 'SSL') : '';
            if (empty($this->data['pavblogs_category_mod']) &&
                empty($this->data['pavblogs_latest_comment_mod']) &&
                empty($this->data['pavblogs_latest_mod']) ) {
                $this->data['text_pavblog_front_mods'] = '';
            }

            if (empty($this->data['pavblogs_category']) &&
                empty($this->data['pavblogs_blogs']) &&
                empty($this->data['pavblogs_add_blog']) &&
                empty($this->data['pavblogs_comments']) &&
                empty($this->data['pavblogs_general']) &&
                empty($this->data['text_pavblog_front_mods']) ) {
                $this->data['text_pavblog_blog'] = '';
            }
            /* ------ PAVBLOG vége  ------ */

            //$this->data['nyelv']        = $this->permission("module/nyelv") ? $this->url->link('module/nyelv', 'token=' . $this->session->data['token'], 'SSL') : '';
            $this->data['logout']       = $this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL');

            $this->data['token'] = $this->session->data['token'];
            $this->data['store'] = HTTP_CATALOG;



            $this->data['stores'] = array();

            $this->load->model('setting/store');

            $results = $this->model_setting_store->getStores();

            foreach ($results as $result) {
                $this->data['stores'][] = array(
                    'name' => $result['name'],
                    'href' => $result['url']
                );
            }
        }

        $this->template = 'common/header.tpl';

        $this->render();
    }

    public function permission($route) {

        $ignore = array(
            'common/home',
            'common/login',
            'common/logout',
            'common/forgotten',
            'common/reset',
            'error/not_found',
            'error/permission'
        );

        if (!in_array($route, $ignore) && !$this->user->hasPermission('visible', $route)) {
            return false;
        } else {
            return true;
        }
    }

}
?>