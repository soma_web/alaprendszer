<?php echo $header; ?>
<div id="content" class="round"><?php echo $content_top; ?>
  <h1><?php echo $heading_title; ?></h1>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <p><?php echo $text_account_already; ?></p>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="register">
    <h2><?php echo $text_your_details; ?></h2>
    <div class="content"> 
      <table class="form">
        <tr>
		    <td><span class="required">*</span> <?php echo $entry_firstname; ?><br />
            <input type="text" class="maximize" name="firstname" value="<?php echo $firstname; ?>" />
            <?php if ($error_firstname) { ?>
            <span class="error"><?php echo $error_firstname; ?></span>
            <?php } ?></td>
        </tr>
        <tr><td><span class="required">*</span> <?php echo $entry_lastname; ?><br />
			<input type="text" class="maximize" name="lastname" value="<?php echo $lastname; ?>" />
            <?php if ($error_lastname) { ?>
            <span class="error"><?php echo $error_lastname; ?></span>
            <?php } ?></td>
        </tr>
        <tr><td><span class="required">*</span> <?php echo $entry_email; ?><br />
			<input type="text" class="maximize" name="email" value="<?php echo $email; ?>" />
            <?php if ($error_email) { ?>
            <span class="error"><?php echo $error_email; ?></span>
            <?php } ?></td>
        </tr>
        <tr><td><span class="required">*</span> <?php echo $entry_telephone; ?><br />
			<input type="text" class="maximize" name="telephone" value="<?php echo $telephone; ?>" />
            <?php if ($error_telephone) { ?>
            <span class="error"><?php echo $error_telephone; ?></span>
            <?php } ?></td>
        </tr>
        <tr><td><?php echo $entry_fax; ?><br />
			<input type="text"  class="maximize" name="fax" value="<?php echo $fax; ?>" /></td>
        </tr>
      </table>
    </div>
    <h2><?php echo $text_your_address; ?> </h2>
    <div class="content">
      <table class="form">
        <tr><td><?php echo $entry_company; ?><br />
			<input type="text" class="maximize" name="company" value="<?php echo $company; ?>" /></td>
        </tr>
          <tr><td><?php echo $entry_adoszam; ?><br />
                  <input type="text" class="maximize" name="adoszam" value="<?php echo $adoszam; ?>" /></td>
          </tr>

        <tr><td><span class="required">*</span> <?php echo $entry_address_1; ?><br />
			<input type="text" class="maximize" name="address_1" value="<?php echo $address_1; ?>" />
            <?php if ($error_address_1) { ?>
            <span class="error"><?php echo $error_address_1; ?></span>
            <?php } ?></td>
        </tr>
        <tr><td><?php echo $entry_address_2; ?><br />
			<input type="text" class="maximize" name="address_2" value="<?php echo $address_2; ?>" /></td>
        </tr>
        <tr><td><span class="required">*</span> <?php echo $entry_city; ?><br />
			<input type="text" class="maximize" name="city" value="<?php echo $city; ?>" />
            <?php if ($error_city) { ?>
            <span class="error"><?php echo $error_city; ?></span>
            <?php } ?></td>
        </tr>
        <tr><td><span class="required">*</span> <?php echo $entry_postcode; ?><br />
			<input type="text" class="maximize" name="postcode" value="<?php echo $postcode; ?>" />
            <?php if ($error_postcode) { ?>
            <span class="error"><?php echo $error_postcode; ?></span>
            <?php } ?></td>
        </tr>
        <tr><td><span class="required">*</span> <?php echo $entry_country; ?><br />
			<select name="country_id" onchange="$('select[name=\'zone_id\']').load('index.php?route=account/register/zone&country_id=' + this.value + '&zone_id=<?php echo $zone_id; ?>');">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($countries as $country) { ?>
              <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
            <?php if ($error_country) { ?>
            <span class="error"><?php echo $error_country; ?></span>
            <?php } ?></td>
        </tr>
        <tr><td><span class="required">*</span> <?php echo $entry_zone; ?><br />
			<select name="zone_id">
            </select>
            <?php if ($error_zone) { ?>
            <span class="error"><?php echo $error_zone; ?></span>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <h2><?php echo $text_your_password; ?></h2>
    <div class="content">
      <table class="form">
        <tr><td><span class="required">*</span> <?php echo $entry_password; ?><br />
			<input type="password" class="maximize" name="password" value="<?php echo $password; ?>" />
            <?php if ($error_password) { ?>
            <span class="error"><?php echo $error_password; ?></span>
            <?php } ?></td>
        </tr>
        <tr><td><span class="required">*</span> <?php echo $entry_confirm; ?><br />
			<input type="password" class="maximize" name="confirm" value="<?php echo $confirm; ?>" />
            <?php if ($error_confirm) { ?>
            <span class="error"><?php echo $error_confirm; ?></span>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <h2><?php echo $text_newsletter; ?></h2>
    <div class="content">
      <table class="form">
        <tr><td><?php echo $entry_newsletter; ?><br />
			<?php if ($newsletter == 1) { ?>
            <input type="radio" name="newsletter" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="newsletter" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="newsletter" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="newsletter" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <?php if ($text_agree) { ?>
    <div class="buttons">
      <div class="left">
        <?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" />
        <?php } ?><?php echo $text_agree; ?>
	  </div>
	  
      <div class="right"><a onclick="$('#register').submit();" class="button"><span><?php echo $button_continue; ?></span></a></div>
    </div>
    <?php } else { ?>
    <div class="buttons">
      <div class="right"><a onclick="$('#register').submit();" class="button"><span><?php echo $button_continue; ?></span></a></div>
    </div>
    <?php } ?>
  </form>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('select[name=\'zone_id\']').load('index.php?route=account/register/zone&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
//--></script> 
<script type="text/javascript"><!--
$('.fancybox').fancybox({
	width: 460,
	height: 460,
	autoDimensions: false
});
//--></script>  
<?php echo $footer; ?>