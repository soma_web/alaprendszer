<?php
class ControllerModuleMyocpricelist extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/product');
        $this->load->language('module/myocpricelist');

        $this->document->setTitle($this->language->get('common_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('myocpricelist', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

        $this->data['heading_title'] = $this->language->get('common_title');

        $this->data['entry_fejlec_latszik'] = $this->language->get('entry_fejlec_latszik');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_login'] = $this->language->get('entry_login');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_store'] = $this->language->get('entry_store');
        $this->data['entry_column_picture'] = $this->language->get('entry_column_picture');
        $this->data['entry_column_model'] = $this->language->get('entry_column_model');
        $this->data['entry_column_rate'] = $this->language->get('entry_column_rate');
        $this->data['entry_column_buy'] = $this->language->get('entry_column_buy');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

        $this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('common_title'),
			'href'      => $this->url->link('module/myocpricelist', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

        $this->data['action'] = $this->url->link('module/myocpricelist', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

        if (isset($this->request->post['myocwpl_status'])) {
      		$this->data['myocwpl_status'] = $this->request->post['myocwpl_status'];
    	} else {
			$this->data['myocwpl_status'] = $this->config->get('myocwpl_status');
		}

		if (isset($this->request->post['myocwpl_login'])) {
      		$this->data['myocwpl_login'] = $this->request->post['myocwpl_login'];
    	} else {
			$this->data['myocwpl_login'] = $this->config->get('myocwpl_login');
		}

        $this->load->model('setting/store');
        $this->data['stores'] = $this->model_setting_store->getStores();

        if (isset($this->request->post['myocwpl_store'])) {
			$this->data['myocwpl_stores'] = $this->request->post['myocwpl_store'];
        } else {
			$this->data['myocwpl_stores'] = $this->config->get('myocwpl_store');
		}

        $this->load->model('sale/customer_group');
        $this->data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups();

        if (isset($this->request->post['myocwpl_customer_group'])) {
			$this->data['myocwpl_customer_groups'] = $this->request->post['myocwpl_customer_group'];
        } else {
			$this->data['myocwpl_customer_groups'] = $this->config->get('myocwpl_customer_group');
		}

        if (isset($this->request->post['myocwpl_customer_fejlec'])) {
            $this->data['myocwpl_customer_fejlec'] = $this->request->post['myocwpl_customer_fejlec'];
        } else {
            $this->data['myocwpl_customer_fejlec'] = $this->config->get('myocwpl_customer_fejlec');
        }

        if (isset($this->request->post['myocwpl_picture'])) {
            $this->data['myocwpl_picture'] = $this->request->post['myocwpl_picture'];
        } else {
            $myocwpl_picture = $this->config->get('myocwpl_picture');
            $this->data['myocwpl_picture'] = isset($myocwpl_picture) ? $myocwpl_picture : 0;
        }

        if (isset($this->request->post['myocwpl_model'])) {
            $this->data['myocwpl_model'] = $this->request->post['myocwpl_model'];
        } else {
            $myocwpl_model = $this->config->get('myocwpl_model');
            $this->data['myocwpl_model'] = isset($myocwpl_model) ? $myocwpl_model : 0;
        }

        if (isset($this->request->post['myocwpl_rate'])) {
            $this->data['myocwpl_rate'] = $this->request->post['myocwpl_rate'];
        } else {
            $myocwpl_rate = $this->config->get('myocwpl_rate');
            $this->data['myocwpl_rate'] = isset($myocwpl_rate) ? $myocwpl_rate : 0;
        }

        if (isset($this->request->post['myocwpl_buy'])) {
            $this->data['myocwpl_buy'] = $this->request->post['myocwpl_buy'];
        } else {
            $myocwpl_buy = $this->config->get('myocwpl_buy');
            $this->data['myocwpl_buy'] = isset($myocwpl_buy) ? $myocwpl_buy : 0;
        }

        $this->template = 'module/myocpricelist.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
    }

    private function validate() {
		if (!$this->user->hasPermission('modify', 'module/myocpricelist')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function install() {
		$this->load->model('design/layout');
		$this->load->model('setting/store');
		$layouts = $this->model_design_layout->getLayouts();

		$layout_name = 'Price List';
		foreach($layouts as $layout){
			if($layout['name'] == $layout_name){
				$this->model_design_layout->deleteLayout($layout['layout_id']);
				break;
			}
		}
		$layout_data = array();
		$layout_data['name'] = $layout_name;
		$layout_data['layout_route'][0] = array(
			'store_id'=> '0',
			'route'	  => 'myoc/pricelist'
		);

		$stores = $this->model_setting_store->getStores();
		foreach ($stores as $store) {
			
			$layout_data['layout_route'][] = array(
				'store_id'=> $store['store_id'],
				'route'	  => 'myoc/pricelist'
			);
		}
		$this->model_design_layout->addLayout($layout_data);
	}

	public function uninstall() {
		$this->load->model('design/layout');
		$layouts = $this->model_design_layout->getLayouts();

		$layout_name = 'Price List';
		foreach($layouts as $layout){
			if($layout['name'] == $layout_name){
				$this->model_design_layout->deleteLayout($layout['layout_id']);
				break;
			}
		}
	}
}
?>