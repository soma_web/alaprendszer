BEGIN
DECLARE customer_id_azonosito INT(11) default 0;
DECLARE customer_group_azonosito INT(11) default 8;
DECLARE customer_extension_azonosito INT(11) default 0;



SELECT customer_id into customer_id_azonosito FROM trodimp_aruhaz.customer WHERE customer_id = uj_PartnerKod OR vevokod LIKE uj_PartnerKod limit 1;
if customer_id_azonosito!=0 then
if uj_Arkategoria != 0 then
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` like concat("Ar",uj_Arkategoria) limit 1;
end if;
if customer_group_azonosito = 0 then
insert into trodimp_aruhaz.customer_group set `name` = concat("Ar",uj_Arkategoria);
select customer_group_id into customer_group_azonosito from trodimp_aruhaz.customer_group where `name` like concat("Ar",uj_Arkategoria) limit 1;
end if;
update trodimp_aruhaz.customer set customer_group_id=customer_group_azonosito where customer_id = uj_PartnerKod OR vevokod LIKE uj_PartnerKod;
if uj_FizetesiMod="A" then
select customer_extension_id into customer_extension_azonosito from trodimp_aruhaz.customer_extension where `code` LIKE "bank_transfer" and customer_id=customer_id_azonosito limit 1;
if customer_extension_azonosito=0 then
insert into trodimp_aruhaz.customer_extension set  customer_id   = customer_id_azonosito,
`type`        = "payment",
`code`        = "bank_transfer",
`value`       = "1";
end if;
else
delete from trodimp_aruhaz.customer_extension where `code` LIKE "bank_transfer" and customer_id=customer_id_azonosito;
end if;
end if;

END