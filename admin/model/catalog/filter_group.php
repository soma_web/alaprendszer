<?php 
class ModelCatalogFilterGroup extends Model {
	public function addFilterGroup($data) {
        $query = $this->db->query("SELECT filter_select_id FROM " . DB_PREFIX . "filter_select ORDER BY filter_select_id DESC LIMIT 1");
        if ($query->num_rows > 0 ){
            $last_id = (int)$query->row['filter_select_id'] + 1;
        } else {
            $last_id = 1;
        }



        foreach ($data['filter_group_description'] as $key=>$nyelv){
            $sql = "INSERT INTO " . DB_PREFIX . "filter_select SET
		    sort_order                  = '" . (int)$data['sort_order'] . "',
            filter_select_id            = '" . $last_id . "',
	        filter_group_select_id      = '',
            `name` = '" . $this->db->escape($nyelv['name']) . "' ,
            `language_id` = '" . $key . "'";

            $this->db->query($sql);
        }
    }


	public function editFilterGroup($filter_group_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "filter_select
		    SET sort_order = '" . (int)$data['sort_order'] . "' WHERE filter_select_id = '" . (int)$filter_group_id . "'");
		

		foreach ($data['filter_group_description'] as $language_id => $value) {

            $this->db->query("UPDATE " . DB_PREFIX . "filter_select SET
                sort_order  = '" . (int)$data['sort_order'] . "',
                `name`      = '" . $value['name'] . "'

            WHERE filter_select_id = '" . (int)$filter_group_id . "' AND language_id = '" . (int)$language_id . "'");

		}
	}
	
	public function deleteFilterGroup($filter_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "filter_select WHERE filter_select_id = '" . (int)$filter_group_id . "'");
	}
		
	public function getFilterGroup($filter_group_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter_select WHERE filter_select_id = '" . (int)$filter_group_id . "'");
		
		return $query->row;
	}
		
	public function getFilterGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "filter_select  WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";
			
		$sort_data = array(
			'name',
			'sort_order'
		);	
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";
		}	
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getFilterGroupDescriptions($filter_group_id) {
		$filter_group_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter_select WHERE filter_select_id = '" . (int)$filter_group_id . "'");
		
		foreach ($query->rows as $result) {
			$filter_group_data[$result['language_id']] = array('name' => $result['name']);
		}
		
		return $filter_group_data;
	}
	
	public function getTotalFilterGroups() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "filter_group");
		
		return $query->row['total'];
	}	
}
?>