<?php
// Heading
$_['heading_title']       = 'Ask for price';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Ask for price!';


// Entry
$_['entry_status']        = 'Status:';
$_['entry_ar_tol']        = 'Price from:';
$_['entry_ar_ig']         = 'Price to:';
$_['entry_levalogatott']  = 'Collated products:';
$_['entry_hozzaad']       = 'Add product';
$_['entry_kivett']        = 'Removed from the list of products';

$_['button_levalogat']    = 'Collate';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify module Ask for price!';
?>