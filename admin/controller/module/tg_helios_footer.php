<?php
class ControllerModuletgheliosfooter extends Controller {
	private $error = array(); 
	private $_name = 'mymodule';
	private $alap_name = 'ugyfelszol';
	public function index() {
		$this->load->language('module/tg_helios_footer');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('tg_helios_footer', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			//$this->redirect($this->url->link('extension/module/', 'token=' . $this->session->data['token'], 'SSL'));
			$this->redirect($this->url->link('module/tg_helios_footer', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_informacio'] = $this->language->get('text_informacio');
        $this->data['text_ugyfelszolgalat'] = $this->language->get('text_ugyfelszolgalat');
        $this->data['text_elerhetoseg'] = $this->language->get('text_elerhetoseg');
        $this->data['text_kategoriak'] = $this->language->get('text_kategoriak');
        $this->data['text_extrak'] = $this->language->get('text_extrak');
        $this->data['text_fiok'] = $this->language->get('text_fiok');
        $this->data['text_uzenet'] = $this->language->get('text_uzenet');
        $this->data['text_hirlevel'] = $this->language->get('text_hirlevel');
        $this->data['tab_likebox'] = $this->language->get('tab_likebox');

        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_description'] = $this->language->get('text_description');
		$this->data['text_info_title'] = $this->language->get('text_info_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_limit'] = $this->language->get('entry_limit');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_aruhaz'] = $this->language->get('entry_aruhaz');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_url'] = $this->language->get('entry_url');
		$this->data['entry_storetitle'] = $this->language->get('entry_storetitle');
		$this->data['entry_informationurl'] = $this->language->get('entry_informationurl');
		$this->data['entry_informationtitle'] = $this->language->get('entry_informationtitle');
		$this->data['entry_categoryurl'] = $this->language->get('entry_categoryurl');
		$this->data['entry_categorytitle'] = $this->language->get('entry_categorytitle');
		$this->data['entry_supporttitle'] = $this->language->get('entry_supporttitle');
		$this->data['entry_image']          = $this->language->get('entry_image');
		$this->data['entry_url']            = $this->language->get('entry_url');

		$this->data['tab_terkep']                = $this->language->get('tab_terkep');
		$this->data['tab_facebook_like_button']  = $this->language->get('tab_facebook_like_button');
		$this->data['tab_egyeb']     = $this->language->get('tab_egyeb');
		$this->data['tab_elerhetoseg']     = $this->language->get('tab_elerhetoseg');

        $this->data['tab_footer_alap']     = $this->language->get('tab_footer_alap');
        $this->data['tab_footer_masodlagos']     = $this->language->get('tab_footer_masodlagos');
        $this->data['tab_sidebar']     = $this->language->get('tab_sidebar');

		$this->data['token'] = $this->session->data['token'];
		
		$this->data['button_save']          = $this->language->get('button_save');
		$this->data['button_cancel']        = $this->language->get('button_cancel');
        $this->data['button_addslide']      = $this->language->get('button_addslide');
        $this->data['button_remove']        = $this->language->get('button_remove');
        
		$this->data['tab_gen']              =  $this->language->get('tab_gen');
		$this->data['tab_info'] =  $this->language->get('tab_info');
		$this->data['tab_twitter'] =  $this->language->get('tab_twitter');
		$this->data['tab_facebook'] =  $this->language->get('tab_facebook');
		$this->data['tab_contact'] =  $this->language->get('tab_contact');
		$this->data['tab_payment'] =  $this->language->get('tab_payment');
		$this->data['tab_default'] =  $this->language->get('tab_default');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['text_image_manager'] =  $this->language->get('text_image_manager');
		$this->data['image_instruction'] =  $this->language->get('image_instruction');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/tg_helios_footer', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/tg_helios_footer', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');



		$this->load->model('localisation/language');
		
		$languages = $this->model_localisation_language->getLanguages();


        foreach ($languages as $language) {
            if (isset($this->request->post[$this->_name . '_code' . $language['language_id']])) {
                $this->data[$this->alap_name . '_code' . $language['language_id']] = $this->request->post[$this->alap_name . '_code' . $language['language_id']];
            } else {
                $this->data[$this->alap_name . '_code' . $language['language_id']] = $this->config->get($this->alap_name . '_code' . $language['language_id']);
            }
            if (isset($this->request->post[$this->_name . '_title' . $language['language_id']])) {
                $this->data[$this->alap_name . '_title' . $language['language_id']] = $this->request->post[$this->alap_name . '_title' . $language['language_id']];
            } else {
                $this->data[$this->alap_name . '_title' . $language['language_id']] = $this->config->get($this->alap_name . '_title' . $language['language_id']);
            }

            if (isset($this->request->post['extrak_code' . $language['language_id']])) {
                $this->data['extrak_code' . $language['language_id']] = $this->request->post['extrak_code' . $language['language_id']];
            } else {
                $this->data['extrak_code' . $language['language_id']] = $this->config->get('extrak_code' . $language['language_id']);
            }
            if (isset($this->request->post['extrak_title' . $language['language_id']])) {
                $this->data['extrak_title' . $language['language_id']] = $this->request->post['extrak_title' . $language['language_id']];
            } else {
                $this->data['extrak_title' . $language['language_id']] = $this->config->get('extrak_title' . $language['language_id']);
            }

            if (isset($this->request->post['fiok_code' . $language['language_id']])) {
                $this->data['fiok_code' . $language['language_id']] = $this->request->post['fiok_code' . $language['language_id']];
            } else {
                $this->data['fiok_code' . $language['language_id']] = $this->config->get('fiok_code' . $language['language_id']);
            }
            if (isset($this->request->post['fiok_title' . $language['language_id']])) {
                $this->data['fiok_title' . $language['language_id']] = $this->request->post['fiok_title' . $language['language_id']];
            } else {
                $this->data['fiok_title' . $language['language_id']] = $this->config->get('fiok_title' . $language['language_id']);
            }

            if (isset($this->request->post['elerhetoseg_title' . $language['language_id']])) {
                $this->data['elerhetoseg_title' . $language['language_id']] = $this->request->post['elerhetoseg_title' . $language['language_id']];
            } else {
                $this->data['elerhetoseg_title' . $language['language_id']] = $this->config->get('elerhetoseg_title' . $language['language_id']);
            }
            if (isset($this->request->post['elerhetoseg_code' . $language['language_id']])) {
                $this->data['elerhetoseg_code' . $language['language_id']] = $this->request->post['elerhetoseg_code' . $language['language_id']];
            } else {
                $this->data['elerhetoseg_code' . $language['language_id']] = $this->config->get('elerhetoseg_code' . $language['language_id']);
            }
            if (isset($this->request->post['sidebar_code' . $language['language_id']])) {
                $this->data['sidebar_code' . $language['language_id']] = $this->request->post['sidebar_code' . $language['language_id']];
            } else {
                $this->data['sidebar_code' . $language['language_id']] = $this->config->get('sidebar_code' . $language['language_id']);
            }


        }



		foreach ($languages as $language) {
			if (isset($this->request->post[$this->_name . '_code' . $language['language_id']])) {
				$this->data[$this->_name . '_code' . $language['language_id']] = $this->request->post[$this->_name . '_code' . $language['language_id']];
			} else {
				$this->data[$this->_name . '_code' . $language['language_id']] = $this->config->get($this->_name . '_code' . $language['language_id']);
			}
		}
		
		foreach ($languages as $language) {
			if (isset($this->request->post[$this->_name . '_title' . $language['language_id']])) {
				$this->data[$this->_name . '_title' . $language['language_id']] = $this->request->post[$this->_name . '_title' . $language['language_id']];
			} else {
				$this->data[$this->_name . '_title' . $language['language_id']] = $this->config->get($this->_name . '_title' . $language['language_id']);
			}
		}
		
		foreach ($languages as $language) {
			if (isset($this->request->post[$this->_name . '_title2' . $language['language_id']])) {
				$this->data[$this->_name . '_title2' . $language['language_id']] = $this->request->post[$this->_name . '_title2' . $language['language_id']];
			} else {
				$this->data[$this->_name . '_title2' . $language['language_id']] = $this->config->get($this->_name . '_title2' . $language['language_id']);
			}
		}
		
		foreach ($languages as $language) {
			if (isset($this->request->post[$this->_name . '_title2b' . $language['language_id']])) {
				$this->data[$this->_name . '_title2b' . $language['language_id']] = $this->request->post[$this->_name . '_title2b' . $language['language_id']];
			} else {
				$this->data[$this->_name . '_title2b' . $language['language_id']] = $this->config->get($this->_name . '_title2b' . $language['language_id']);
			}
		}
		
		foreach ($languages as $language) {
			if (isset($this->request->post[$this->_name . '_title3' . $language['language_id']])) {
				$this->data[$this->_name . '_title3' . $language['language_id']] = $this->request->post[$this->_name . '_title3' . $language['language_id']];
			} else {
				$this->data[$this->_name . '_title3' . $language['language_id']] = $this->config->get($this->_name . '_title3' . $language['language_id']);
			}
		}
		
		foreach ($languages as $language) {
			if (isset($this->request->post[$this->_name . '_title4' . $language['language_id']])) {
				$this->data[$this->_name . '_title4' . $language['language_id']] = $this->request->post[$this->_name . '_title4' . $language['language_id']];
			} else {
				$this->data[$this->_name . '_title4' . $language['language_id']] = $this->config->get($this->_name . '_title4' . $language['language_id']);
			}
		}

		$this->data['languages'] = $languages;	
		
		if (isset($this->request->post[$this->_name . '_code'])) {
			$this->data[$this->_name . 'mymodule_code'] = $this->request->post[$this->_name . '_code'];
		} else {
			$this->data[$this->_name . '_code'] = $this->config->get($this->_name . '_code');
		}	
		
		
		
		/* --- Általános beállítások --- */

        $this->load->model('setting/store');

        $this->data['stores'] = array();


        $this->data['stores'] = array();
        $this->data['stores'][] = array(
            'store_id' 	=> 0,
            'name'     	=> $this->language->get('text_default'),
            'url'      	=> HTTP_SERVER
        );

        $aruhazak = $this->model_setting_store->getStores();

        if ($aruhazak) {
            foreach($aruhazak as $aruhaz) {
                $this->data['stores'][] = $aruhaz;
                if (isset($this->request->post['tg_helios_footer_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_status'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_default_show'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_default_show'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_default_show'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_default_show'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_default_show'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_default_show_alul'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_default_show_alul'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_default_show_alul'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_default_show_alul'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_default_show_alul'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_informaciok_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_informaciok_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_informaciok_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_informaciok_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_informaciok_status'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['tg_helios_footer_informaciok_status_sorrend'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_informaciok_status_sorrend'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_informaciok_status_sorrend'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_informaciok_status_sorrend'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_informaciok_status_sorrend'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_kategoriak_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_kategoriak_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_kategoriak_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_kategoriak_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_kategoriak_status'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['tg_helios_footer_kategoriak_status_sorrend'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_kategoriak_status_sorrend'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_kategoriak_status_sorrend'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_kategoriak_status_sorrend'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_kategoriak_status_sorrend'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_ugyfelszol_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_ugyfelszol_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_ugyfelszol_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_ugyfelszol_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_ugyfelszol_status'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['tg_helios_footer_ugyfelszol_status_sorrend'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_ugyfelszol_status_sorrend'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_ugyfelszol_status_sorrend'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_ugyfelszol_status_sorrend'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_ugyfelszol_status_sorrend'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_extrak_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_extrak_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_extrak_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_extrak_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_extrak_status'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['tg_helios_footer_extrak_status_sorrend'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_extrak_status_sorrend'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_extrak_status_sorrend'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_extrak_status_sorrend'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_extrak_status_sorrend'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_fiok_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_fiok_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_fiok_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_fiok_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_fiok_status'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['tg_helios_footer_fiok_status_sorrend'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_fiok_status_sorrend'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_fiok_status_sorrend'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_fiok_status_sorrend'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_fiok_status_sorrend'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_elerhetoseg_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_elerhetoseg_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_elerhetoseg_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_elerhetoseg_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_elerhetoseg_status'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['tg_helios_footer_elerhetoseg_status_sorrend'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_elerhetoseg_status_sorrend'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_elerhetoseg_status_sorrend'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_elerhetoseg_status_sorrend'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_elerhetoseg_status_sorrend'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_uzenet_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_uzenet_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_uzenet_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_uzenet_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_uzenet_status'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['tg_helios_footer_uzenet_status_sorrend'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_uzenet_status_sorrend'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_uzenet_status_sorrend'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_uzenet_status_sorrend'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_uzenet_status_sorrend'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_hirlevel_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_hirlevel_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_hirlevel_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_hirlevel_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_hirlevel_status'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['tg_helios_footer_hirlevel_status_sorrend'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_hirlevel_status_sorrend'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_hirlevel_status_sorrend'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_hirlevel_status_sorrend'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_hirlevel_status_sorrend'.$aruhaz['store_id']);
                }

                if (isset($this->request->post['tg_helios_footer_likebox_status'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_likebox_status'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_likebox_status'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_likebox_status'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_likebox_status'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['tg_helios_footer_likebox_status_sorrend'.$aruhaz['store_id']])) {
                    $this->data['tg_helios_footer_likebox_status_sorrend'.$aruhaz['store_id']] = $this->request->post['tg_helios_footer_likebox_status_sorrend'.$aruhaz['store_id']];
                } else {
                    $this->data['tg_helios_footer_likebox_status_sorrend'.$aruhaz['store_id']] = $this->config->get('tg_helios_footer_likebox_status_sorrend'.$aruhaz['store_id']);
                }
                if (isset($this->request->post['likebox_code'.$aruhaz['store_id']])) {
                    $this->data['likebox_code'.$aruhaz['store_id']] = $this->request->post['likebox_code'.$aruhaz['store_id']];
                } else {
                    $this->data['likebox_code'.$aruhaz['store_id']] = $this->config->get('likebox_code'.$aruhaz['store_id']);
                }
                $this->data['likebox_code'.$aruhaz['store_id']] = isset($this->request->post['likebox_code'.$aruhaz['store_id']]) ? $this->request->post['likebox_code'.$aruhaz['store_id']] : $this->config->get('likebox_code'.$aruhaz['store_id']);

                $this->data['tg_helios_footer_info_status'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_info_status'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_info_status'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_info_status'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_info_sort'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_info_sort'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_info_sort'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_info_sort'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_info_fuggoleges'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_info_fuggoleges'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_info_fuggoleges'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_info_fuggoleges'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_info_szelesseg'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_info_szelesseg'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_info_szelesseg'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_info_szelesseg'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_info_magassag'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_info_magassag'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_info_magassag'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_info_magassag'.$aruhaz['store_id']);

                $this->data['tg_helios_footer_contact_status'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_status'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_status'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_status'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_sort'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_sort'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_sort'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_sort'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_fuggoleges'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_fuggoleges'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_fuggoleges'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_fuggoleges'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_szelesseg'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_szelesseg'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_szelesseg'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_szelesseg'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_magassag'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_magassag'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_magassag'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_magassag'.$aruhaz['store_id']);

                $this->data['tg_helios_footer_contact_phone'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_phone'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_phone'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_phone'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_phone_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_phone_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_phone_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_phone_show'.$aruhaz['store_id']);

                $this->data['tg_helios_footer_contact_mobile'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_mobile'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_mobile'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_mobile'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_mobile_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_mobile_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_mobile_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_mobile_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_email'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_email'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_email'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_email'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_email_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_email_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_email_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_email_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_skype'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_skype'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_skype'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_status'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_skype_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_skype_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_skype_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_skype_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_address'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_address'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_address'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_address'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_contact_address_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_contact_address_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_contact_address_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_contact_address_show'.$aruhaz['store_id']);

                $this->data['tg_helios_footer_terkep_status'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_terkep_status'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_terkep_status'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_terkep_status'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_terkep_sort'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_terkep_sort'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_terkep_sort'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_terkep_sort'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_terkep_fuggoleges'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_terkep_fuggoleges'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_terkep_fuggoleges'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_terkep_fuggoleges'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_terkep_szelesseg'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_terkep_szelesseg'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_terkep_szelesseg'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_terkep_szelesseg'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_terkep_magassag'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_terkep_magassag'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_terkep_magassag'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_terkep_magassag'.$aruhaz['store_id']);

                $this->data['tg_helios_footer_twitter_status'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_twitter_status'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_twitter_status'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_twitter_status'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_twitter_sort'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_twitter_sort'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_twitter_sort'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_twitter_sort'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_twitter_szelesseg'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_twitter_szelesseg'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_twitter_szelesseg'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_twitter_szelesseg'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_twitter_magassag'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_twitter_magassag'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_twitter_magassag'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_twitter_magassag'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_twitter_tweets'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_twitter_tweets'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_twitter_tweets'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_twitter_tweets'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_twitter_username'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_twitter_username'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_twitter_username'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_twitter_username'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_twitter_fuggoleges'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_twitter_fuggoleges'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_twitter_fuggoleges'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_twitter_fuggoleges'.$aruhaz['store_id']);

                $this->data['tg_helios_footer_facebook_fanpage_status'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_fanpage_status'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_fanpage_status'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_fanpage_status'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_fanpage_sort'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_fanpage_sort'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_fanpage_sort'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_fanpage_sort'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_fanpage_fuggoleges'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_fanpage_fuggoleges'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_fanpage_fuggoleges'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_fanpage_fuggoleges'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_fanpage_szelesseg'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_fanpage_szelesseg'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_fanpage_szelesseg'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_fanpage_szelesseg'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_fanpage_magassag'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_fanpage_magassag'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_fanpage_magassag'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_fanpage_magassag'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_fanpage_id'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_fanpage_id'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_fanpage_id'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_fanpage_id'.$aruhaz['store_id']);

                $this->data['tg_helios_footer_facebook_gomb_status'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb_status'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb_status'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb_status'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb_sort'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb_sort'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb_sort'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb_sort'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb_fuggoleges'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb_fuggoleges'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb_fuggoleges'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb_fuggoleges'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb_szelesseg'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb_szelesseg'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb_szelesseg'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb_szelesseg'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb_magassag'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb_magassag'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb_magassag'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb_magassag'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb0'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb0'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb0'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb0'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb0_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb0_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb0_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb0_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb1'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb1'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb1'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb1'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb1_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb1_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb1_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb1_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb2'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb2'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb2'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb2'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb2_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb2_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb2_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb2_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb3'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb3'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb3'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb3'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb3_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb3_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb3_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb3_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb4'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb4'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb4'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb4'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb4_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb4_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb4_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb4_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb5'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb5'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb5'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb5'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb5_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb5_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb5_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb5_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb6'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb6'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb6'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb6'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb6_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb6_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb6_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb6_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb7'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb7'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb7'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb7'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb7_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb7_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb7_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb7_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb8'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb8'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb8'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb8'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb8_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb8_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb8_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb8_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb9'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb9'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb9'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb9'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb9_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb9_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb9_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb9_show'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb10'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb10'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb10'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb10'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_facebook_gomb10_show'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_facebook_gomb10_show'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_facebook_gomb10_show'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_facebook_gomb10_show'.$aruhaz['store_id']);

                $this->data['tg_helios_footer_egyeb_status'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_egyeb_status'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_egyeb_status'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_egyeb_status'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_egyeb_sort'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_egyeb_sort'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_egyeb_sort'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_egyeb_sort'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_egyeb_fuggoleges'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_egyeb_fuggoleges'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_egyeb_fuggoleges'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_egyeb_fuggoleges'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_egyeb_szelesseg'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_egyeb_szelesseg'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_egyeb_szelesseg'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_egyeb_szelesseg'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_egyeb_magassag'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_egyeb_magassag'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_egyeb_magassag'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_egyeb_magassag'.$aruhaz['store_id']);
                $this->data['tg_helios_footer_egyeb'.$aruhaz['store_id']] = isset($this->request->post['tg_helios_footer_egyeb'.$aruhaz['store_id']]) ? $this->request->post['tg_helios_footer_egyeb'.$aruhaz['store_id']] : $this->config->get('tg_helios_footer_egyeb'.$aruhaz['store_id']);

                $this->data['sidebar_code_status'.$aruhaz['store_id']] = isset($this->request->post['sidebar_code_status'.$aruhaz['store_id']]) ? $this->request->post['sidebar_code_status'.$aruhaz['store_id']] : $this->config->get('sidebar_code_status'.$aruhaz['store_id']);



                foreach ($languages as $language) {
                    if (isset($this->request->post['ugyfelszol_title' . $language['language_id'].'_'.$aruhaz['store_id']])) {
                        $this->data['ugyfelszol_title' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->request->post['ugyfelszol_title' . $language['language_id'].'_'.$aruhaz['store_id']];
                    } else {
                        $this->data['ugyfelszol_title' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->config->get('ugyfelszol_title' . $language['language_id'].'_'.$aruhaz['store_id']);
                    }
                    if (isset($this->request->post['ugyfelszol_code' . $language['language_id'].'_'.$aruhaz['store_id']])) {
                        $this->data['ugyfelszol_code' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->request->post['ugyfelszol_code' . $language['language_id'].'_'.$aruhaz['store_id']];
                    } else {
                        $this->data['ugyfelszol_code' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->config->get('ugyfelszol_code' . $language['language_id'].'_'.$aruhaz['store_id']);
                    }

                    if (isset($this->request->post['extrak_title' . $language['language_id'].'_'.$aruhaz['store_id']])) {
                        $this->data['extrak_title' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->request->post['extrak_title' . $language['language_id'].'_'.$aruhaz['store_id']];
                    } else {
                        $this->data['extrak_title' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->config->get('extrak_title' . $language['language_id'].'_'.$aruhaz['store_id']);
                    }
                    if (isset($this->request->post['extrak_code' . $language['language_id'].'_'.$aruhaz['store_id']])) {
                        $this->data['extrak_code' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->request->post['extrak_code' . $language['language_id'].'_'.$aruhaz['store_id']];
                    } else {
                        $this->data['extrak_code' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->config->get('extrak_code' . $language['language_id'].'_'.$aruhaz['store_id']);
                    }

                    if (isset($this->request->post['fiok_title' . $language['language_id'].'_'.$aruhaz['store_id']])) {
                        $this->data['fiok_title' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->request->post['fiok_title' . $language['language_id'].'_'.$aruhaz['store_id']];
                    } else {
                        $this->data['fiok_title' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->config->get('fiok_title' . $language['language_id'].'_'.$aruhaz['store_id']);
                    }
                    if (isset($this->request->post['fiok_code' . $language['language_id'].'_'.$aruhaz['store_id']])) {
                        $this->data['fiok_code' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->request->post['fiok_code' . $language['language_id'].'_'.$aruhaz['store_id']];
                    } else {
                        $this->data['fiok_code' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->config->get('fiok_code' . $language['language_id'].'_'.$aruhaz['store_id']);
                    }

                    if (isset($this->request->post['elerhetoseg_title' . $language['language_id'].'_'.$aruhaz['store_id']])) {
                        $this->data['elerhetoseg_title' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->request->post['elerhetoseg_title' . $language['language_id'].'_'.$aruhaz['store_id']];
                    } else {
                        $this->data['elerhetoseg_title' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->config->get('elerhetoseg_title' . $language['language_id'].'_'.$aruhaz['store_id']);
                    }
                    if (isset($this->request->post['elerhetoseg_code' . $language['language_id'].'_'.$aruhaz['store_id']])) {
                        $this->data['elerhetoseg_code' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->request->post['elerhetoseg_code' . $language['language_id'].'_'.$aruhaz['store_id']];
                    } else {
                        $this->data['elerhetoseg_code' . $language['language_id'].'_'.$aruhaz['store_id']] = $this->config->get('elerhetoseg_code' . $language['language_id'].'_'.$aruhaz['store_id']);
                    }

                    $this->data['mymodule_title'.$language['language_id'].'_'.$aruhaz['store_id']] = isset($this->request->post['mymodule_title' . $language['language_id'].'_'.$aruhaz['store_id']])
                                ? $this->request->post['mymodule_title' . $language['language_id'].'_'.$aruhaz['store_id']]
                                : $this->config->get('mymodule_title' . $language['language_id'].'_'.$aruhaz['store_id']);
                    $this->data['mymodule_code'.$language['language_id'].'_'.$aruhaz['store_id']] = isset($this->request->post['mymodule_code' . $language['language_id'].'_'.$aruhaz['store_id']])
                        ? $this->request->post['mymodule_code' . $language['language_id'].'_'.$aruhaz['store_id']]
                        : $this->config->get('mymodule_code' . $language['language_id'].'_'.$aruhaz['store_id']);

                    $this->data['mymodule_title2'.$language['language_id'].'_'.$aruhaz['store_id']] = isset($this->request->post['mymodule_title2' . $language['language_id'].'_'.$aruhaz['store_id']])
                        ? $this->request->post['mymodule_title2' . $language['language_id'].'_'.$aruhaz['store_id']]
                        : $this->config->get('mymodule_title2' . $language['language_id'].'_'.$aruhaz['store_id']);

                    $this->data['mymodule_title3'.$language['language_id'].'_'.$aruhaz['store_id']] = isset($this->request->post['mymodule_title3' . $language['language_id'].'_'.$aruhaz['store_id']])
                        ? $this->request->post['mymodule_title3' . $language['language_id'].'_'.$aruhaz['store_id']]
                        : $this->config->get('mymodule_title3' . $language['language_id'].'_'.$aruhaz['store_id']);

                    $this->data['mymodule_title4'.$language['language_id'].'_'.$aruhaz['store_id']] = isset($this->request->post['mymodule_title4' . $language['language_id'].'_'.$aruhaz['store_id']])
                        ? $this->request->post['mymodule_title4' . $language['language_id'].'_'.$aruhaz['store_id']]
                        : $this->config->get('mymodule_title4' . $language['language_id'].'_'.$aruhaz['store_id']);

                    $this->data['sidebar_code'.$language['language_id'].'_'.$aruhaz['store_id']] = isset($this->request->post['sidebar_code' . $language['language_id'].'_'.$aruhaz['store_id']])
                        ? $this->request->post['sidebar_code' . $language['language_id'].'_'.$aruhaz['store_id']]
                        : $this->config->get('sidebar_code' . $language['language_id'].'_'.$aruhaz['store_id']);



                }


            }
        }



        if (isset($this->request->post['tg_helios_footer_stores'])) {
            $this->data['tg_helios_footer_stores'] = $this->request->post['tg_helios_footer_stores'];
        } else {
            $this->data['tg_helios_footer_stores'] = $this->config->get('tg_helios_footer_stores');
        }




		if (isset($this->request->post['tg_helios_footer_status'])) {
			$this->data['tg_helios_footer_status'] = $this->request->post['tg_helios_footer_status'];
		} else {
			$this->data['tg_helios_footer_status'] = $this->config->get('tg_helios_footer_status');
		}

		if (isset($this->request->post['tg_helios_footer_footer_enabled_footer_columns_cnt'])) {
			$this->data['tg_helios_footer_footer_enabled_footer_columns_cnt'] = $this->request->post['tg_helios_footer_footer_enabled_footer_columns_cnt'];
		} else {
			$this->data['tg_helios_footer_footer_enabled_footer_columns_cnt'] = $this->config->get('tg_helios_footer_footer_enabled_footer_columns_cnt');
		}

        if (isset($this->request->post['likebox_code'])) {
			$this->data['likebox_code'] = $this->request->post['likebox_code'];
		} else {
			$this->data['likebox_code'] = $this->config->get('likebox_code');
		}


        /* --- Default --- */
		if (isset($this->request->post['tg_helios_footer_default'])) {
			$this->data['tg_helios_footer_default'] = $this->request->post['tg_helios_footer_default'];
		} else {
			$this->data['tg_helios_footer_default'] = $this->config->get('tg_helios_footer_default');
		}
		
		if (isset($this->request->post['tg_helios_footer_default_show'])) {
			$this->data['tg_helios_footer_default_show'] = $this->request->post['tg_helios_footer_default_show'];
		} else {
			$this->data['tg_helios_footer_default_show'] = $this->config->get('tg_helios_footer_default_show');
		}

        if (isset($this->request->post['tg_helios_footer_default_show_alul'])) {
            $this->data['tg_helios_footer_default_show_alul'] = $this->request->post['tg_helios_footer_default_show_alul'];
        } else {
            $this->data['tg_helios_footer_default_show_alul'] = $this->config->get('tg_helios_footer_default_show_alul');
        }




        /* --- Alap --- */
        if (isset($this->request->post['tg_helios_footer_informaciok_status'])) {
            $this->data['tg_helios_footer_informaciok_status'] = $this->request->post['tg_helios_footer_informaciok_status'];
        } else {
            $this->data['tg_helios_footer_informaciok_status'] = $this->config->get('tg_helios_footer_informaciok_status');
        }
        if (isset($this->request->post['tg_helios_footer_informaciok_status_sorrend'])) {
            $this->data['tg_helios_footer_informaciok_status_sorrend'] = $this->request->post['tg_helios_footer_informaciok_status_sorrend'];
        } else {
            $this->data['tg_helios_footer_informaciok_status_sorrend'] = $this->config->get('tg_helios_footer_informaciok_status_sorrend');
        }

        if (isset($this->request->post['tg_helios_footer_kategoriak_status'])) {
            $this->data['tg_helios_footer_kategoriak_status'] = $this->request->post['tg_helios_footer_kategoriak_status'];
        } else {
            $this->data['tg_helios_footer_kategoriak_status'] = $this->config->get('tg_helios_footer_kategoriak_status');
        }
        if (isset($this->request->post['tg_helios_footer_kategoriak_status_sorrend'])) {
            $this->data['tg_helios_footer_kategoriak_status_sorrend'] = $this->request->post['tg_helios_footer_kategoriak_status_sorrend'];
        } else {
            $this->data['tg_helios_footer_kategoriak_status_sorrend'] = $this->config->get('tg_helios_footer_kategoriak_status_sorrend');
        }

        if (isset($this->request->post['tg_helios_footer_ugyfelszol_status'])) {
            $this->data['tg_helios_footer_ugyfelszol_status'] = $this->request->post['tg_helios_footer_ugyfelszol_status'];
        } else {
            $this->data['tg_helios_footer_ugyfelszol_status'] = $this->config->get('tg_helios_footer_ugyfelszol_status');
        }
        if (isset($this->request->post['tg_helios_footer_ugyfelszol_status_sorrend'])) {
            $this->data['tg_helios_footer_ugyfelszol_status_sorrend'] = $this->request->post['tg_helios_footer_ugyfelszol_status_sorrend'];
        } else {
            $this->data['tg_helios_footer_ugyfelszol_status_sorrend'] = $this->config->get('tg_helios_footer_ugyfelszol_status_sorrend');
        }



        if (isset($this->request->post['tg_helios_footer_elerhetoseg_status'])) {
            $this->data['tg_helios_footer_elerhetoseg_status'] = $this->request->post['tg_helios_footer_elerhetoseg_status'];
        } else {
            $this->data['tg_helios_footer_elerhetoseg_status'] = $this->config->get('tg_helios_footer_elerhetoseg_status');
        }
        if (isset($this->request->post['tg_helios_footer_elerhetoseg_status_sorrend'])) {
            $this->data['tg_helios_footer_elerhetoseg_status_sorrend'] = $this->request->post['tg_helios_footer_elerhetoseg_status_sorrend'];
        } else {
            $this->data['tg_helios_footer_elerhetoseg_status_sorrend'] = $this->config->get('tg_helios_footer_elerhetoseg_status_sorrend');
        }



        if (isset($this->request->post['tg_helios_footer_extrak_status'])) {
            $this->data['tg_helios_footer_extrak_status'] = $this->request->post['tg_helios_footer_extrak_status'];
        } else {
            $this->data['tg_helios_footer_extrak_status'] = $this->config->get('tg_helios_footer_extrak_status');
        }
        if (isset($this->request->post['tg_helios_footer_extrak_status_sorrend'])) {
            $this->data['tg_helios_footer_extrak_status_sorrend'] = $this->request->post['tg_helios_footer_extrak_status_sorrend'];
        } else {
            $this->data['tg_helios_footer_extrak_status_sorrend'] = $this->config->get('tg_helios_footer_extrak_status_sorrend');
        }

        if (isset($this->request->post['tg_helios_footer_fiok_status'])) {
            $this->data['tg_helios_footer_fiok_status'] = $this->request->post['tg_helios_footer_fiok_status'];
        } else {
            $this->data['tg_helios_footer_fiok_status'] = $this->config->get('tg_helios_footer_fiok_status');
        }
        if (isset($this->request->post['tg_helios_footer_fiok_status_sorrend'])) {
            $this->data['tg_helios_footer_fiok_status_sorrend'] = $this->request->post['tg_helios_footer_fiok_status_sorrend'];
        } else {
            $this->data['tg_helios_footer_fiok_status_sorrend'] = $this->config->get('tg_helios_footer_extrak_status_sorrend');
        }

        if (isset($this->request->post['tg_helios_footer_uzenet_status'])) {
            $this->data['tg_helios_footer_uzenet_status'] = $this->request->post['tg_helios_footer_uzenet_status'];
        } else {
            $this->data['tg_helios_footer_uzenet_status'] = $this->config->get('tg_helios_footer_uzenet_status');
        }
        if (isset($this->request->post['tg_helios_footer_uzenet_status_sorrend'])) {
            $this->data['tg_helios_footer_uzenet_status_sorrend'] = $this->request->post['tg_helios_footer_uzenet_status_sorrend'];
        } else {
            $this->data['tg_helios_footer_uzenet_status_sorrend'] = $this->config->get('tg_helios_footer_uzenet_status_sorrend');
        }

        if (isset($this->request->post['tg_helios_footer_hirlevel_status'])) {
            $this->data['tg_helios_footer_hirlevel_status'] = $this->request->post['tg_helios_footer_hirlevel_status'];
        } else {
            $this->data['tg_helios_footer_hirlevel_status'] = $this->config->get('tg_helios_footer_hirlevel_status');
        }
        if (isset($this->request->post['tg_helios_footer_hirlevel_status_sorrend'])) {
            $this->data['tg_helios_footer_hirlevel_status_sorrend'] = $this->request->post['tg_helios_footer_hirlevel_status_sorrend'];
        } else {
            $this->data['tg_helios_footer_hirlevel_status_sorrend'] = $this->config->get('tg_helios_footer_hirlevel_status_sorrend');
        }

        if (isset($this->request->post['tg_helios_footer_likebox_status'])) {
            $this->data['tg_helios_footer_likebox_status'] = $this->request->post['tg_helios_footer_likebox_status'];
        } else {
            $this->data['tg_helios_footer_likebox_status'] = $this->config->get('tg_helios_footer_likebox_status');
        }
        if (isset($this->request->post['tg_helios_footer_likebox_status_sorrend'])) {
            $this->data['tg_helios_footer_likebox_status_sorrend'] = $this->request->post['tg_helios_footer_likebox_status_sorrend'];
        } else {
            $this->data['tg_helios_footer_likebox_status_sorrend'] = $this->config->get('tg_helios_footer_likebox_status_sorrend');
        }

        if (isset($this->request->post['tg_helios_footer_sidebar_status'])) {
            $this->data['tg_helios_footer_sidebar_status'] = $this->request->post['tg_helios_footer_sidebar_status'];
        } else {
            $this->data['tg_helios_footer_sidebar_status'] = $this->config->get('tg_helios_footer_sidebar_status');
        }























        /* --- Info --- */
        if (isset($this->request->post['tg_helios_footer_info_status'])) {
            $this->data['tg_helios_footer_info_status'] = $this->request->post['tg_helios_footer_info_status'];
        } else {
            $this->data['tg_helios_footer_info_status'] = $this->config->get('tg_helios_footer_info_status');
        }

        if (isset($this->request->post['tg_helios_footer_info_sort'])) {
            $this->data['tg_helios_footer_info_sort'] = $this->request->post['tg_helios_footer_info_sort'];
        } else {
            $this->data['tg_helios_footer_info_sort'] = $this->config->get('tg_helios_footer_info_sort');
        }

        if (isset($this->request->post['tg_helios_footer_info_fuggoleges'])) {
            $this->data['tg_helios_footer_info_fuggoleges'] = $this->request->post['tg_helios_footer_info_fuggoleges'];
        } else {
            $this->data['tg_helios_footer_info_fuggoleges'] = $this->config->get('tg_helios_footer_info_fuggoleges');
        }

        if (isset($this->request->post['tg_helios_footer_info_szelesseg'])) {
            $this->data['tg_helios_footer_info_szelesseg'] = $this->request->post['tg_helios_footer_info_szelesseg'];
        } else {
            $this->data['tg_helios_footer_info_szelesseg'] = $this->config->get('tg_helios_footer_info_szelesseg');
        }

        if (isset($this->request->post['tg_helios_footer_info_magassag'])) {
            $this->data['tg_helios_footer_info_magassag'] = $this->request->post['tg_helios_footer_info_magassag'];
        } else {
            $this->data['tg_helios_footer_info_magassag'] = $this->config->get('tg_helios_footer_info_magassag');
        }




        /* --- Contact --- */
        if (isset($this->request->post['tg_helios_footer_contact_status'])) {
            $this->data['tg_helios_footer_contact_status'] = $this->request->post['tg_helios_footer_contact_status'];
        } else {
            $this->data['tg_helios_footer_contact_status'] = $this->config->get('tg_helios_footer_contact_status');
        }

        if (isset($this->request->post['tg_helios_footer_contact_phone_show'])) {
            $this->data['tg_helios_footer_contact_phone_show'] = $this->request->post['tg_helios_footer_contact_phone_show'];
        } else {
            $this->data['tg_helios_footer_contact_phone_show'] = $this->config->get('tg_helios_footer_contact_phone_show');
        }

        if (isset($this->request->post['tg_helios_footer_contact_phone'])) {
            $this->data['tg_helios_footer_contact_phone'] = $this->request->post['tg_helios_footer_contact_phone'];
        } else {
            $this->data['tg_helios_footer_contact_phone'] = $this->config->get('tg_helios_footer_contact_phone');
        }

        if (isset($this->request->post['tg_helios_footer_contact_mobile_show'])) {
            $this->data['tg_helios_footer_contact_mobile_show'] = $this->request->post['tg_helios_footer_contact_mobile_show'];
        } else {
            $this->data['tg_helios_footer_contact_mobile_show'] = $this->config->get('tg_helios_footer_contact_mobile_show');
        }

        if (isset($this->request->post['tg_helios_footer_contact_mobile'])) {
            $this->data['tg_helios_footer_contact_mobile'] = $this->request->post['tg_helios_footer_contact_mobile'];
        } else {
            $this->data['tg_helios_footer_contact_mobile'] = $this->config->get('tg_helios_footer_contact_mobile');
        }

        if (isset($this->request->post['tg_helios_footer_contact_email_show'])) {
            $this->data['tg_helios_footer_contact_email_show'] = $this->request->post['tg_helios_footer_contact_email_show'];
        } else {
            $this->data['tg_helios_footer_contact_email_show'] = $this->config->get('tg_helios_footer_contact_email_show');
        }

        if (isset($this->request->post['tg_helios_footer_contact_email'])) {
            $this->data['tg_helios_footer_contact_email'] = $this->request->post['tg_helios_footer_contact_email'];
        } else {
            $this->data['tg_helios_footer_contact_email'] = $this->config->get('tg_helios_footer_contact_email');
        }

        if (isset($this->request->post['tg_helios_footer_contact_skype_show'])) {
            $this->data['tg_helios_footer_contact_skype_show'] = $this->request->post['tg_helios_footer_contact_skype_show'];
        } else {
            $this->data['tg_helios_footer_contact_skype_show'] = $this->config->get('tg_helios_footer_contact_skype_show');
        }

        if (isset($this->request->post['tg_helios_footer_contact_skype'])) {
            $this->data['tg_helios_footer_contact_skype'] = $this->request->post['tg_helios_footer_contact_skype'];
        } else {
            $this->data['tg_helios_footer_contact_skype'] = $this->config->get('tg_helios_footer_contact_skype');
        }

        if (isset($this->request->post['tg_helios_footer_contact_address_show'])) {
            $this->data['tg_helios_footer_contact_address_show'] = $this->request->post['tg_helios_footer_contact_address_show'];
        } else {
            $this->data['tg_helios_footer_contact_address_show'] = $this->config->get('tg_helios_footer_contact_address_show');
        }

        if (isset($this->request->post['tg_helios_footer_contact_address'])) {
            $this->data['tg_helios_footer_contact_address'] = $this->request->post['tg_helios_footer_contact_address'];
        } else {
            $this->data['tg_helios_footer_contact_address'] = $this->config->get('tg_helios_footer_contact_address');
        }

        if (isset($this->request->post['tg_helios_footer_contact_sort'])) {
            $this->data['tg_helios_footer_contact_sort'] = $this->request->post['tg_helios_footer_contact_sort'];
        } else {
            $this->data['tg_helios_footer_contact_sort'] = $this->config->get('tg_helios_footer_contact_sort');
        }

        if (isset($this->request->post['tg_helios_footer_contact_fuggoleges'])) {
            $this->data['tg_helios_footer_contact_fuggoleges'] = $this->request->post['tg_helios_footer_contact_fuggoleges'];
        } else {
            $this->data['tg_helios_footer_contact_fuggoleges'] = $this->config->get('tg_helios_footer_contact_fuggoleges');
        }

        if (isset($this->request->post['tg_helios_footer_contact_szelesseg'])) {
            $this->data['tg_helios_footer_contact_szelesseg'] = $this->request->post['tg_helios_footer_contact_szelesseg'];
        } else {
            $this->data['tg_helios_footer_contact_szelesseg'] = $this->config->get('tg_helios_footer_contact_szelesseg');
        }

        if (isset($this->request->post['tg_helios_footer_contact_magassag'])) {
            $this->data['tg_helios_footer_contact_magassag'] = $this->request->post['tg_helios_footer_contact_magassag'];
        } else {
            $this->data['tg_helios_footer_contact_magassag'] = $this->config->get('tg_helios_footer_contact_magassag');
        }



        /* --- Twitter --- */
        if (isset($this->request->post['tg_helios_footer_twitter_status'])) {
            $this->data['tg_helios_footer_twitter_status'] = $this->request->post['tg_helios_footer_twitter_status'];
        } else {
            $this->data['tg_helios_footer_twitter_status'] = $this->config->get('tg_helios_footer_twitter_status');
        }

        if (isset($this->request->post['tg_helios_footer_twitter_username'])) {
            $this->data['tg_helios_footer_twitter_username'] = $this->request->post['tg_helios_footer_twitter_username'];
        } else {
            $this->data['tg_helios_footer_twitter_username'] = $this->config->get('tg_helios_footer_twitter_username');
        }

        if (isset($this->request->post['tg_helios_footer_twitter_tweets'])) {
            $this->data['tg_helios_footer_twitter_tweets'] = $this->request->post['tg_helios_footer_twitter_tweets'];
        } else {
            $this->data['tg_helios_footer_twitter_tweets'] = $this->config->get('tg_helios_footer_twitter_tweets');
        }

        if (isset($this->request->post['tg_helios_footer_twitter_sort'])) {
            $this->data['tg_helios_footer_twitter_sort'] = $this->request->post['tg_helios_footer_twitter_sort'];
        } else {
            $this->data['tg_helios_footer_twitter_sort'] = $this->config->get('tg_helios_footer_twitter_sort');
        }

        if (isset($this->request->post['tg_helios_footer_twitter_fuggoleges'])) {
            $this->data['tg_helios_footer_twitter_fuggoleges'] = $this->request->post['tg_helios_footer_twitter_fuggoleges'];
        } else {
            $this->data['tg_helios_footer_twitter_fuggoleges'] = $this->config->get('tg_helios_footer_twitter_fuggoleges');
        }

        if (isset($this->request->post['tg_helios_footer_twitter_szelesseg'])) {
            $this->data['tg_helios_footer_twitter_szelesseg'] = $this->request->post['tg_helios_footer_twitter_szelesseg'];
        } else {
            $this->data['tg_helios_footer_twitter_szelesseg'] = $this->config->get('tg_helios_footer_twitter_szelesseg');
        }

        if (isset($this->request->post['tg_helios_footer_twitter_magassag'])) {
            $this->data['tg_helios_footer_twitter_magassag'] = $this->request->post['tg_helios_footer_twitter_magassag'];
        } else {
            $this->data['tg_helios_footer_twitter_magassag'] = $this->config->get('tg_helios_footer_twitter_magassag');
        }



        /* --- Facebook Fanpage --- */
        if (isset($this->request->post['tg_helios_footer_facebook_fanpage_status'])) {
            $this->data['tg_helios_footer_facebook_fanpage_status'] = $this->request->post['tg_helios_footer_facebook_fanpage_status'];
        } else {
            $this->data['tg_helios_footer_facebook_fanpage_status'] = $this->config->get('tg_helios_footer_facebook_fanpage_status');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_fanpage_id'])) {
            $this->data['tg_helios_footer_facebook_fanpage_id'] = $this->request->post['tg_helios_footer_facebook_fanpage_id'];
        } else {
            $this->data['tg_helios_footer_facebook_fanpage_id'] = $this->config->get('tg_helios_footer_facebook_fanpage_id');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_fanpage_sort'])) {
            $this->data['tg_helios_footer_facebook_fanpage_sort'] = $this->request->post['tg_helios_footer_facebook_fanpage_sort'];
        } else {
            $this->data['tg_helios_footer_facebook_fanpage_sort'] = $this->config->get('tg_helios_footer_facebook_fanpage_sort');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_fanpage_fuggoleges'])) {
            $this->data['tg_helios_footer_facebook_fanpage_fuggoleges'] = $this->request->post['tg_helios_footer_facebook_fanpage_fuggoleges'];
        } else {
            $this->data['tg_helios_footer_facebook_fanpage_fuggoleges'] = $this->config->get('tg_helios_footer_facebook_fanpage_fuggoleges');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_fanpage_szelesseg'])) {
            $this->data['tg_helios_footer_facebook_fanpage_szelesseg'] = $this->request->post['tg_helios_footer_facebook_fanpage_szelesseg'];
        } else {
            $this->data['tg_helios_footer_facebook_fanpage_szelesseg'] = $this->config->get('tg_helios_footer_facebook_fanpage_szelesseg');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_fanpage_magassag'])) {
            $this->data['tg_helios_footer_facebook_fanpage_magassag'] = $this->request->post['tg_helios_footer_facebook_fanpage_magassag'];
        } else {
            $this->data['tg_helios_footer_facebook_fanpage_magassag'] = $this->config->get('tg_helios_footer_facebook_fanpage_magassag');
        }




        /* --- Terkep --- */
        if (isset($this->request->post['tg_helios_footer_terkep_status'])) {
            $this->data['tg_helios_footer_terkep_status'] = $this->request->post['tg_helios_footer_terkep_status'];
        } else {
            $this->data['tg_helios_footer_terkep_status'] = $this->config->get('tg_helios_footer_terkep_status');
        }

        if (isset($this->request->post['tg_helios_footer_terkep_magassag'])) {
            $this->data['tg_helios_footer_terkep_magassag'] = $this->request->post['tg_helios_footer_terkep_magassag'];
        } else {
            $this->data['tg_helios_footer_terkep_magassag'] = $this->config->get('tg_helios_footer_terkep_magassag');
        }

        if (isset($this->request->post['tg_helios_footer_terkep_szelesseg'])) {
            $this->data['tg_helios_footer_terkep_szelesseg'] = $this->request->post['tg_helios_footer_terkep_szelesseg'];
        } else {
            $this->data['tg_helios_footer_terkep_szelesseg'] = $this->config->get('tg_helios_footer_terkep_szelesseg');
        }

        if (isset($this->request->post['tg_helios_footer_terkep_sort'])) {
            $this->data['tg_helios_footer_terkep_sort'] = $this->request->post['tg_helios_footer_terkep_sort'];
        } else {
            $this->data['tg_helios_footer_terkep_sort'] = $this->config->get('tg_helios_footer_terkep_sort');
        }

        if (isset($this->request->post['tg_helios_footer_terkep_fuggoleges'])) {
            $this->data['tg_helios_footer_terkep_fuggoleges'] = $this->request->post['tg_helios_footer_terkep_fuggoleges'];
        } else {
            $this->data['tg_helios_footer_terkep_fuggoleges'] = $this->config->get('tg_helios_footer_terkep_fuggoleges');
        }



        /* --- Facebook Like Gomb --- */
        if (isset($this->request->post['tg_helios_footer_facebook_gomb_status'])) {
            $this->data['tg_helios_footer_facebook_gomb_status'] = $this->request->post['tg_helios_footer_facebook_gomb_status'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb_status'] = $this->config->get('tg_helios_footer_facebook_gomb_status');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_gomb0'])) {
            $this->data['tg_helios_footer_facebook_gomb0'] = $this->request->post['tg_helios_footer_facebook_gomb0'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb0'] = $this->config->get('tg_helios_footer_facebook_gomb0');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb1'])) {
            $this->data['tg_helios_footer_facebook_gomb1'] = $this->request->post['tg_helios_footer_facebook_gomb1'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb1'] = $this->config->get('tg_helios_footer_facebook_gomb1');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb2'])) {
            $this->data['tg_helios_footer_facebook_gomb2'] = $this->request->post['tg_helios_footer_facebook_gomb2'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb2'] = $this->config->get('tg_helios_footer_facebook_gomb2');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb3'])) {
            $this->data['tg_helios_footer_facebook_gomb3'] = $this->request->post['tg_helios_footer_facebook_gomb3'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb3'] = $this->config->get('tg_helios_footer_facebook_gomb3');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb4'])) {
            $this->data['tg_helios_footer_facebook_gomb4'] = $this->request->post['tg_helios_footer_facebook_gomb4'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb4'] = $this->config->get('tg_helios_footer_facebook_gomb4');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb5'])) {
            $this->data['tg_helios_footer_facebook_gomb5'] = $this->request->post['tg_helios_footer_facebook_gomb5'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb5'] = $this->config->get('tg_helios_footer_facebook_gomb5');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb6'])) {
            $this->data['tg_helios_footer_facebook_gomb6'] = $this->request->post['tg_helios_footer_facebook_gomb6'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb6'] = $this->config->get('tg_helios_footer_facebook_gomb6');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb7'])) {
            $this->data['tg_helios_footer_facebook_gomb7'] = $this->request->post['tg_helios_footer_facebook_gomb7'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb7'] = $this->config->get('tg_helios_footer_facebook_gomb7');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb8'])) {
            $this->data['tg_helios_footer_facebook_gomb8'] = $this->request->post['tg_helios_footer_facebook_gomb8'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb8'] = $this->config->get('tg_helios_footer_facebook_gomb8');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb9'])) {
            $this->data['tg_helios_footer_facebook_gomb9'] = $this->request->post['tg_helios_footer_facebook_gomb9'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb9'] = $this->config->get('tg_helios_footer_facebook_gomb9');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb10'])) {
            $this->data['tg_helios_footer_facebook_gomb10'] = $this->request->post['tg_helios_footer_facebook_gomb10'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb10'] = $this->config->get('tg_helios_footer_facebook_gomb10');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_gomb0_show'])) {
            $this->data['tg_helios_footer_facebook_gomb0_show'] = $this->request->post['tg_helios_footer_facebook_gomb0_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb0_show'] = $this->config->get('tg_helios_footer_facebook_gomb0_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb1_show'])) {
            $this->data['tg_helios_footer_facebook_gomb1_show'] = $this->request->post['tg_helios_footer_facebook_gomb1_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb1_show'] = $this->config->get('tg_helios_footer_facebook_gomb1_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb2_show'])) {
            $this->data['tg_helios_footer_facebook_gomb2_show'] = $this->request->post['tg_helios_footer_facebook_gomb2_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb2_show'] = $this->config->get('tg_helios_footer_facebook_gomb2_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb3_show'])) {
            $this->data['tg_helios_footer_facebook_gomb3_show'] = $this->request->post['tg_helios_footer_facebook_gomb3_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb3_show'] = $this->config->get('tg_helios_footer_facebook_gomb3_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb4_show'])) {
            $this->data['tg_helios_footer_facebook_gomb4_show'] = $this->request->post['tg_helios_footer_facebook_gomb4_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb4_show'] = $this->config->get('tg_helios_footer_facebook_gomb4_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb5_show'])) {
            $this->data['tg_helios_footer_facebook_gomb5_show'] = $this->request->post['tg_helios_footer_facebook_gomb5_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb5_show'] = $this->config->get('tg_helios_footer_facebook_gomb5_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb6_show'])) {
            $this->data['tg_helios_footer_facebook_gomb6_show'] = $this->request->post['tg_helios_footer_facebook_gomb6_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb6_show'] = $this->config->get('tg_helios_footer_facebook_gomb6_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb7_show'])) {
            $this->data['tg_helios_footer_facebook_gomb7_show'] = $this->request->post['tg_helios_footer_facebook_gomb7_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb7_show'] = $this->config->get('tg_helios_footer_facebook_gomb7_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb8_show'])) {
            $this->data['tg_helios_footer_facebook_gomb8_show'] = $this->request->post['tg_helios_footer_facebook_gomb8_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb8_show'] = $this->config->get('tg_helios_footer_facebook_gomb8_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb9_show'])) {
            $this->data['tg_helios_footer_facebook_gomb9_show'] = $this->request->post['tg_helios_footer_facebook_gomb9_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb9_show'] = $this->config->get('tg_helios_footer_facebook_gomb9_show');
        }
        if (isset($this->request->post['tg_helios_footer_facebook_gomb10_show'])) {
            $this->data['tg_helios_footer_facebook_gomb10_show'] = $this->request->post['tg_helios_footer_facebook_gomb10_show'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb10_show'] = $this->config->get('tg_helios_footer_facebook_gomb10_show');
        }



        if (isset($this->request->post['tg_helios_footer_facebook_gomb_sort'])) {
            $this->data['tg_helios_footer_facebook_gomb_sort'] = $this->request->post['tg_helios_footer_facebook_gomb_sort'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb_sort'] = $this->config->get('tg_helios_footer_facebook_gomb_sort');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_gomb_fuggoleges'])) {
            $this->data['tg_helios_footer_facebook_gomb_fuggoleges'] = $this->request->post['tg_helios_footer_facebook_gomb_fuggoleges'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb_fuggoleges'] = $this->config->get('tg_helios_footer_facebook_gomb_fuggoleges');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_gomb_szelesseg'])) {
            $this->data['tg_helios_footer_facebook_gomb_szelesseg'] = $this->request->post['tg_helios_footer_facebook_gomb_szelesseg'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb_szelesseg'] = $this->config->get('tg_helios_footer_facebook_gomb_szelesseg');
        }

        if (isset($this->request->post['tg_helios_footer_facebook_gomb_magassag'])) {
            $this->data['tg_helios_footer_facebook_gomb_magassag'] = $this->request->post['tg_helios_footer_facebook_gomb_magassag'];
        } else {
            $this->data['tg_helios_footer_facebook_gomb_magassag'] = $this->config->get('tg_helios_footer_facebook_gomb_magassag');
        }




        /* --- Facebook Like Box --- */
        if (isset($this->request->post['tg_helios_footer_egyeb_status'])) {
            $this->data['tg_helios_footer_egyeb_status'] = $this->request->post['tg_helios_footer_egyeb_status'];
        } else {
            $this->data['tg_helios_footer_egyeb_status'] = $this->config->get('tg_helios_footer_egyeb_status');
        }

        if (isset($this->request->post['tg_helios_footer_egyeb'])) {
            $this->data['tg_helios_footer_egyeb'] = $this->request->post['tg_helios_footer_egyeb'];
        } else {
            $this->data['tg_helios_footer_egyeb'] = $this->config->get('tg_helios_footer_egyeb');
        }

        if (isset($this->request->post['tg_helios_footer_egyeb_sort'])) {
            $this->data['tg_helios_footer_egyeb_sort'] = $this->request->post['tg_helios_footer_egyeb_sort'];
        } else {
            $this->data['tg_helios_footer_egyeb_sort'] = $this->config->get('tg_helios_footer_egyeb_sort');
        }

        if (isset($this->request->post['tg_helios_footer_egyeb_fuggoleges'])) {
            $this->data['tg_helios_footer_egyeb_fuggoleges'] = $this->request->post['tg_helios_footer_egyeb_fuggoleges'];
        } else {
            $this->data['tg_helios_footer_egyeb_fuggoleges'] = $this->config->get('tg_helios_footer_egyeb_fuggoleges');
        }

        if (isset($this->request->post['tg_helios_footer_egyeb_szelesseg'])) {
            $this->data['tg_helios_footer_egyeb_szelesseg'] = $this->request->post['tg_helios_footer_egyeb_szelesseg'];
        } else {
            $this->data['tg_helios_footer_egyeb_szelesseg'] = $this->config->get('tg_helios_footer_egyeb_szelesseg');
        }

        if (isset($this->request->post['tg_helios_footer_egyeb_magassag'])) {
            $this->data['tg_helios_footer_egyeb_magassag'] = $this->request->post['tg_helios_footer_egyeb_magassag'];
        } else {
            $this->data['tg_helios_footer_egyeb_magassag'] = $this->config->get('tg_helios_footer_egyeb_magassag');
        }

        if (isset($this->request->post['sidebar_code_status'])) {
            $this->data['sidebar_code_status'] = $this->request->post['sidebar_code_status'];
        } else {
            $this->data['sidebar_code_status'] = $this->config->get('sidebar_code_status');
        }

        if (isset($this->request->post['sidebar_code'])) {
            $this->data['sidebar_code'] = $this->request->post['sidebar_code'];
        } else {
            $this->data['sidebar_code'] = $this->config->get('sidebar_code');
        }


                $this->template = 'module/tg_helios_footer.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/tg_helios_footer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		
		
		
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>