<?php 
class ControllerCheckoutSuccessElhelyezkedes extends Controller {
	public function index() { 

		$this->language->load('checkout/success_elhelyezkedes');
		
		$this->document->setTitle($this->language->get('heading_title'));

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('common/home'),
            'text'      => $this->language->get('text_home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('account/product'),
            'text'      => $this->language->get('text_termek'),
            'separator' => $this->language->get('text_separator')
        );


        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/success_elhelyezkedes'),
            'text'      => $this->language->get('text_transaction_success_title'),
            'separator' => $this->language->get('text_separator')
        );


        $this->data['heading_title'] = $this->language->get('heading_title');

		if ($this->customer->isLogged()) {
    		$this->data['text_message'] = $this->language->get('text_customer');
		} else {
    		$this->data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}
		
    	$this->data['button_continue'] = $this->language->get('button_continue');

    	$this->data['continue'] = $this->url->link('common/home');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
		} else {
			$this->template = 'default/template/common/success.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'			
		);
				
		$this->response->setOutput($this->render());
  	}
}
?>