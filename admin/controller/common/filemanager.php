<?php
class ControllerCommonFileManager extends Controller {
	private $error = array();
	
	public function index() {
		$this->load->language('common/filemanager');
		
		$this->data['title'] = $this->language->get('heading_title');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = HTTPS_SERVER;
		} else {
			$this->data['base'] = HTTP_SERVER;
		}
		
		$this->data['entry_folder'] = $this->language->get('entry_folder');
		$this->data['entry_move'] = $this->language->get('entry_move');
		$this->data['entry_copy'] = $this->language->get('entry_copy');
		$this->data['entry_rename'] = $this->language->get('entry_rename');
		
		$this->data['button_folder'] = $this->language->get('button_folder');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_move'] = $this->language->get('button_move');
		$this->data['button_copy'] = $this->language->get('button_copy');
		$this->data['button_rename'] = $this->language->get('button_rename');
		$this->data['button_upload'] = $this->language->get('button_upload');
		$this->data['button_refresh'] = $this->language->get('button_refresh');
		$this->data['button_refresh'] = $this->language->get('button_refresh'); 
		$this->data['button_submit'] = $this->language->get('button_submit'); 
		$this->data['button_short'] = $this->language->get('button_short');
		$this->data['button_short_nevsor'] = $this->language->get('button_short_nevsor');
		$this->data['button_short_datum'] = $this->language->get('button_short_datum');

		$this->data['text_kozvetlen_url'] = $this->language->get('text_kozvetlen_url');
		$this->data['text_kozvetlen_feltolt'] = $this->language->get('text_kozvetlen_feltolt');
		$this->data['text_confirm_image'] = $this->language->get('text_confirm_image');
		$this->data['text_confirm_path'] = $this->language->get('text_confirm_path');

		$this->data['error_select'] = $this->language->get('error_select');
		$this->data['error_directory'] = $this->language->get('error_directory');

        $this->load->model('tool/image');
        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

        $this->data['token'] = $this->session->data['token'];
		
		$this->data['directory'] = HTTP_IMAGE . 'data/';
		
		if (isset($this->request->get['field'])) {
			$this->data['field'] = $this->request->get['field'];
		} else {
			$this->data['field'] = '';
		}
		
		if (isset($this->request->get['CKEditorFuncNum'])) {
			$this->data['fckeditor'] = $this->request->get['CKEditorFuncNum'];
		} else {
			$this->data['fckeditor'] = false;
		}
		
		$this->template = 'common/filemanager.tpl';
		
		$this->response->setOutput($this->render());
	}	
	
	public function image() {
		$this->load->model('tool/image');

        if (isset($this->request->get['image'])) {
            if (isset($this->request->get['pdf']) && $this->request->get['pdf'] == 1) {
                $this->response->setOutput($this->request->get['image']);
            } else {
			    $this->response->setOutput($this->model_tool_image->resize(html_entity_decode($this->request->get['image'], ENT_QUOTES, 'UTF-8'), 100, 100, false));
            }
		}
	}
	
	public function directory() {	
		$json = array();
		
		if (isset($this->request->post['directory'])) {
			$directories = glob(rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->request->post['directory']), '/') . '/*', GLOB_ONLYDIR); 
			
			if ($directories) {
				$i = 0;
			
				foreach ($directories as $directory) {
					$json[$i]['data'] = basename($directory);
					$json[$i]['attributes']['directory'] = utf8_substr($directory, strlen(DIR_IMAGE . 'data/'));
					
					$children = glob(rtrim($directory, '/') . '/*', GLOB_ONLYDIR);
					
					if ($children)  {
						$json[$i]['children'] = ' ';
					}
					
					$i++;
				}
			}		
		}
		
		$this->response->setOutput(json_encode($json));		
	}
	
	public function sorrend($shor="datum") {

    }

	public function files() {
		$json = array();
		
		if (!empty($this->request->post['directory'])) {
			$directory = DIR_IMAGE . 'data/' . str_replace('../', '', $this->request->post['directory']);
		} else {
			$directory = DIR_IMAGE . 'data/';
		}
		
		$allowed = array(
			'.jpg',
			'.jpeg',
			'.png',
			'.gif',
			'.pdf'
		);
		
		$files = glob(rtrim($directory, '/') . '/*');
		
		if ($files) {
			foreach ($files as $file) {
				if (is_file($file)) {
					$ext = strrchr($file, '.');
				} else {
					$ext = '';
				}	
				
				if (in_array(strtolower($ext), $allowed)) {
					$size = filesize($file);
		
					$i = 0;
		
					$suffix = array(
						'B',
						'KB',
						'MB',
						'GB',
						'TB',
						'PB',
						'EB',
						'ZB',
						'YB'
					);
		
					while (($size / 1024) > 1) {
						$size = $size / 1024;
						$i++;
					}

                    $this->load->model("tool/image");
					$json[] = array(
						'filename' => basename($file),
						'file'     => utf8_substr($file, strlen(DIR_IMAGE . 'data/')),
						'size'     => round(utf8_substr($size, 0, strpos($size, '.') + 4), 2) . $suffix[$i],
                        'date' => filemtime($file)
					);


				}
			}
		}


        function sort_images_by_date($a, $b){
            return ($a['date'] == $b['date']) ? 0 : ($a['date'] > $b['date']) ? -1 : 1;
        }

        function sort_images_by_filename($a, $b){
            return ($a['filename'] == $b['filename']) ? 1 : ($a['filename'] < $b['filename']) ? -1 : 1;
        }


        if (isset($this->request->get['sorrend']) && $this->request->get['sorrend'] == "nev") {
            usort($json, "sort_images_by_filename");
        } else {
            usort($json, "sort_images_by_date");
        }


            $this->response->setOutput(json_encode($json));
	}	
	
	public function create() {
		$this->load->language('common/filemanager');
				
		$json = array();
		
		if (isset($this->request->post['directory'])) {
			if (isset($this->request->post['name']) || $this->request->post['name']) {
				$directory = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->request->post['directory']), '/');							   
				
				if (!is_dir($directory)) {
					$json['error'] = $this->language->get('error_directory');
				}
				
				if (file_exists($directory . '/' . str_replace('../', '', $this->request->post['name']))) {
					$json['error'] = $this->language->get('error_exists');
				}
			} else {
				$json['error'] = $this->language->get('error_name');
			}
		} else {
			$json['error'] = $this->language->get('error_directory');
		}
		
		if (!$this->user->hasPermission('modify', 'common/filemanager')) {
      		$json['error'] = $this->language->get('error_permission');  
    	}
		
		if (!isset($json['error'])) {	
			mkdir($directory . '/' . str_replace('../', '', $this->request->post['name']), 0777);
			
			$json['success'] = $this->language->get('text_create');
		}	
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function delete() {
		$this->load->language('common/filemanager');
		
		$json = array();

        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if (!isset($json['error'])) {
            if (isset($this->request->post['path'])) {
                $images = explode(',',$this->request->post['path']);

                foreach($images as $value) {
                    $path = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', html_entity_decode($value, ENT_QUOTES, 'UTF-8')), '/');

                    if (!file_exists($path)) {
                        $json['error'] = $this->language->get('error_select');
                        break;
                    }

                    if ($path == rtrim(DIR_IMAGE . 'data/', '/')) {
                        $json['error'] = $this->language->get('error_delete');
                        break;
                    }


                    if (!isset($json['error'])) {
                        if (is_file($path)) {
                            unlink($path);
                        } elseif (is_dir($path)) {
                            $this->recursiveDelete($path);
                        }
                    }

                }

                if (!isset($json['error'])) {
                    $json['success'] = $this->language->get('text_delete');
                }

            } else {
                $json['error'] = $this->language->get('error_select');
            }
        }
		

		
		$this->response->setOutput(json_encode($json));
	}

	protected function recursiveDelete($directory) {
		if (is_dir($directory)) {
			$handle = opendir($directory);
		}
		
		if (!$handle) {
			return false;
		}
		
		while (false !== ($file = readdir($handle))) {
			if ($file != '.' && $file != '..') {
				if (!is_dir($directory . '/' . $file)) {
					unlink($directory . '/' . $file);
				} else {
					$this->recursiveDelete($directory . '/' . $file);
				}
			}
		}
		
		closedir($handle);
		
		rmdir($directory);
		
		return true;
	}

	public function move() {
		$this->load->language('common/filemanager');
        $this->load->model('tool/image');
		$json = array();

        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if (!isset($json['error'])) {
            if (isset($this->request->post['from']) && isset($this->request->post['to'])) {

                $images = explode(',',$this->request->post['from']);

                foreach($images as $value) {
                    $from = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', html_entity_decode($value, ENT_QUOTES, 'UTF-8')), '/');

                    if (!file_exists($from)) {
                        $json['error'] = $this->language->get('error_missing');
                        break;
                    }

                    if ($from == DIR_IMAGE . 'data') {
                        $json['error'] = $this->language->get('error_default');
                        break;
                    }

                    $to = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', html_entity_decode($this->request->post['to'], ENT_QUOTES, 'UTF-8')), '/');

                    if (!file_exists($to)) {
                        $json['error'] = $this->language->get('error_move');
                        break;
                    }

                    if (file_exists($to . '/' . basename($from))) {
                        $json['error'] = $this->language->get('error_exists');
                        break;
                    }

                    $old_link=strstr($from,"data");
                    $new_link=strstr($to,"data");
                    $this->model_tool_image->imageLinkMove($old_link,$new_link);
                    rename($from, $to . '/' . basename($from));

                }

                if (!isset($json['error'])) {
                    $json['success'] = $this->language->get('text_move');
                }

            } else {
                $json['error'] = $this->language->get('error_directory');
            }

		}

		$this->response->setOutput(json_encode($json));
	}	
	
	public function copy() {
		$this->load->language('common/filemanager');
		
		$json = array();

        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if (!isset($json['error'])) {
            if (isset($this->request->post['path']) && isset($this->request->post['name'])) {
                if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 255)) {
                    $json['error'] = $this->language->get('error_filename');
                }

                if (!isset($json['error'])) {

                    $images = explode(',',$this->request->post['path']);

                    foreach($images as $value) {

                        $old_name = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', html_entity_decode($value, ENT_QUOTES, 'UTF-8')), '/');

                        if (!file_exists($old_name) || $old_name == DIR_IMAGE . 'data') {
                            $json['error'] = $this->language->get('error_copy');
                            break;
                        }

                        if (is_file($old_name)) {
                            $ext = strrchr($old_name, '.');
                        } else {
                            $ext = '';
                        }

                        $new_name = dirname($old_name) . '/' . str_replace('../', '', html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8') . $ext);

                        if (file_exists($new_name)) {
                            for($i=1; true; $i++) {
                                $new_name = dirname($old_name) . '/' . str_replace('../', '', html_entity_decode($this->request->post['name']."[".$i."]", ENT_QUOTES, 'UTF-8') . $ext);

                                if (!file_exists($new_name)) {
                                    break;
                                }
                            }
                        }

                        if (is_file($old_name)) {
                            copy($old_name, $new_name);
                        } else {
                            $this->recursiveCopy($old_name, $new_name);
                        }

                    }

                    if (!isset($json['error'])) {
                        $json['success'] = $this->language->get('text_copy');
                    }
                }
            } else {
                $json['error'] = $this->language->get('error_select');
            }
        }

		$this->response->setOutput(json_encode($json));
	}

	function recursiveCopy($source, $destination) { 
		$directory = opendir($source); 
		
		@mkdir($destination); 
		
		while (false !== ($file = readdir($directory))) {
			if (($file != '.') && ($file != '..')) { 
				if (is_dir($source . '/' . $file)) { 
					$this->recursiveCopy($source . '/' . $file, $destination . '/' . $file); 
				} else { 
					copy($source . '/' . $file, $destination . '/' . $file); 
				} 
			} 
		} 
		
		closedir($directory); 
	} 

	public function folders() {
		$this->response->setOutput($this->recursiveFolders(DIR_IMAGE . 'data/'));	
	}
	
	protected function recursiveFolders($directory) {
		$output = '';
		
		$output .= '<option value="' . utf8_substr($directory, strlen(DIR_IMAGE . 'data/')) . '">' . utf8_substr($directory, strlen(DIR_IMAGE . 'data/')) . '</option>';
		
		$directories = glob(rtrim(str_replace('../', '', $directory), '/') . '/*', GLOB_ONLYDIR);
		
		foreach ($directories  as $directory) {
			$output .= $this->recursiveFolders($directory);
		}
		
		return $output;
	}
	
	public function rename() {
		$this->load->language('common/filemanager');
		
		$json = array();

        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if (!isset($json['error'])) {

            if (isset($this->request->post['path']) && isset($this->request->post['name'])) {
                if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 255)) {
                    $json['error'] = $this->language->get('error_filename');
                }


                if (!isset($json['error'])) {

                    $images = explode(',',$this->request->post['path']);

                    foreach($images as $value) {

                        $old_name = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', html_entity_decode($value, ENT_QUOTES, 'UTF-8')), '/');

                        if (!file_exists($old_name) || $old_name == DIR_IMAGE . 'data') {
                            $json['error'] = $this->language->get('error_rename');
                            break;
                        }

                        if (is_file($old_name)) {
                            $ext = strrchr($old_name, '.');
                        } else {
                            $ext = '';
                        }
                        //$ext = '';

                        $new_name = dirname($old_name) . '/' . str_replace('../', '', html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8') . $ext);

                        if (file_exists($new_name)) {
                            for($i=1; true; $i++) {
                                $new_name = dirname($old_name) . '/' . str_replace('../', '', html_entity_decode($this->request->post['name']."[".$i."]", ENT_QUOTES, 'UTF-8') . $ext);
                                if (!file_exists($new_name)) {
                                    break;
                                }
                            }
                        }

                        rename($old_name, $new_name);
                    }

                    if (!isset($json['error'])) {
                        $json['success'] = $this->language->get('text_rename');
                    }


                }

            } else {
                $json['error'] = $this->language->get('error_select');
            }
        }

		$this->response->setOutput(json_encode($json));
	}
	
	public function upload() {
		$this->load->language('common/filemanager');
		
		$json = array();
		
		if (isset($this->request->post['directory'])) {
			if (isset($this->request->files['image']) && $this->request->files['image']['tmp_name']) {
				$filename = basename(html_entity_decode($this->request->files['image']['name'], ENT_QUOTES, 'UTF-8'));
				
				if ((strlen($filename) < 3) || (strlen($filename) > 255)) {
					$json['error'] = $this->language->get('error_filename');
				}
					
				$directory = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->request->post['directory']), '/');
				
				if (!is_dir($directory)) {
					$json['error'] = $this->language->get('error_directory');
				}

				if ($this->request->files['image']['size'] > 4319322) {
					$json['error'] = $this->language->get('error_file_size');
				}
				
				$allowed = array(
					'image/jpeg',
					'image/pjpeg',
					'image/png',
					'image/x-png',
					'image/gif',
					'application/x-shockwave-flash',
					'application/pdf'
				);
						
				if (!in_array($this->request->files['image']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_file_type');
				}
				
				$allowed = array(
					'.jpg',
					'.jpeg',
					'.gif',
					'.png',
					'.flv',
					'.pdf'
				);
						
				if (!in_array(strtolower(strrchr($filename, '.')), $allowed)) {
					$json['error'] = $this->language->get('error_file_type');
				}

				if ($this->request->files['image']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = 'error_upload_' . $this->request->files['image']['error'];
				}			
			} else {
				$json['error'] = $this->language->get('error_file');
			}
		} else {
			$json['error'] = $this->language->get('error_directory');
		}
		
		if (!$this->user->hasPermission('modify', 'common/filemanager')) {
      		$json['error'] = $this->language->get('error_permission');  
    	}
		


        if (!isset($json['error'])) {
            $keptipus=$this->request->files['image']['type'];
            if ($keptipus=='image/jpeg'){
                $meretezve=$this->resizeImage($this->request->files['image']['tmp_name'],1920,1080);
                if ($meretezve){
                    imagejpeg($meretezve, $this->request->files['image']['tmp_name']);
                }
            }



            if (@move_uploaded_file($this->request->files['image']['tmp_name'], $directory . '/' . $filename)) {
                if ($keptipus == "application/pdf") {
                    chmod($directory . '/' . $filename,777);
                }
                $json['success'] = $this->language->get('text_uploaded');
            } else {
                $json['error'] = $this->language->get('error_uploaded');
            }
        }
		
		$this->response->setOutput(json_encode($json));
	}

    public function upload_url() {
        $this->load->language('common/filemanager');
        $directory = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->request->post['aktualis_konyvtar']), '/');
        $file = $_POST['kozvetlen_url'];
        $json = array();


        $file_headers = @get_headers($file);
        if($file_headers[0] == 'HTTP/1.1 404 Not Found' ||$file_headers[0] == 'HTTP/1.1 403 Forbidden' ) {
            $exists = false;
        }
        else {
            $exists = true;
        }

        if ($exists) {
            $allowed = array(
                '.jpg',
                '.jpeg',
                '.gif',
                '.png',
                '.flv',
                '.pdf'
            );

            if (!in_array(strtolower(strrchr($file, '.')), $allowed)) {
                $json['error'] = $this->language->get('error_file_type');
            } else {
                if (file_exists($directory.'/'.basename($file))) {
                    $json['error'] = $this->language->get('error_exists_file');
                } else {
                    $data = file_get_contents($file);
                    $new = $directory.'/'.basename($file);
                    file_put_contents($new, $data);
                    $json['success'] = $this->language->get('text_uploaded');
                }
            }

        } else {
            $json['error'] = $this->language->get('error_no_exists_file');
        }


        $this->response->setOutput(json_encode($json));


    }
    function resizeImage($filename, $max_width, $max_height) {

        list($orig_width, $orig_height) = getimagesize($filename);

        $width = $orig_width;
        $height = $orig_height;

        # taller
        if ($height > $max_height) {
            $width = ($max_height / $height) * $width;
            $height = $max_height;
        }

        # wider
        if ($width > $max_width) {
            $height = ($max_width / $width) * $height;
            $width = $max_width;
        }

        $image_p = imagecreatetruecolor($width, $height);

        $image = imagecreatefromjpeg($filename);

        imagecopyresampled($image_p, $image, 0, 0, 0, 0,
            $width, $height, $orig_width, $orig_height);

        return $image_p;
    }

} 
?>