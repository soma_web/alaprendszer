
<!-- csomag ajánlatok-->
<?php if(isset($packages) && $packages) { ?>

<div id="package_fejlec" class="box">

    <?php if ($this->config->get('package_module_fejlec') == 1 ) { ?>
        <div class="box-heading"><span><?php echo $package_heading_title; ?></span></div>
    <?php } ?>


    <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/plus.png')) {
        $plus = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/plus.png';
    } else {
        $plus = DIR_TEMPLATE_IMAGE.'default/image/plus.png';
    } ?>


   <div class="box-product">
       <?php  foreach ($packages as $termekek) { ?>

           <div class="csomag">
               <?php $product = $termekek['package']; ?>
               <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>


           <?php $pluszjel = false;?>
           <?php  foreach ($termekek['products'] as $product) { ?>
               <?php if ($pluszjel) {?>
                   <div><image src="<?php echo $plus?>"></div>
               <?php }?>
               <?php $pluszjel = true;?>
               <div class="csomag-termekek">

                   <?php if ($product['thumb']) { ?>
                       <div class="image">
                           <?php if(!empty($product['href'])) { ?>
                           <a href="<?php echo $product['href']; ?>">
                               <?php } ?>
                               <img id="<?php echo $module_name.$product['product_id']  ?>" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
                               <?php if(!empty($product['href'])) { ?>
                           </a>
                       <?php }?>
                       </div>
                   <?php } ?>

                   <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>

               </div>

           <?php } ?>

       <div style="padding-bottom: 15px;">
           <?php $product = $termekek['package']; ?>
           <?php if($this->config->get('megjelenit_brutto_ar') == 1){ ?>
               <?php if($this->config->get('megjelenit_brutto_netto') == 1){ ?>
                   <?php if ($product['price']) { ?>
                       <div class="price <?php echo $szazalek_class?>" >
                           <?php if (!$product['special']) { ?>
                               <span class="price-new"><?php echo $product['price']; ?></span>
                           <?php } else { ?>
                               <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                           <?php } ?>
                       </div>
                       <?php if ($product['tax']) { ?>
                           <?php if($this->config->get('megjelenit_netto_ar') == 1){ ?>
                               <?php if (!$product['special']) { ?>
                                   <span class="price-tax"><?php echo "text_netto"; ?> <?php echo $product['price_netto']; ?></span>
                               <?php } else { ?>
                                   <span class="price-tax"><?php echo "text_netto"; ?> <?php echo $product['special_netto']; ?></span>
                               <?php } ?>
                           <?php } ?>
                       <?php } ?>
                   <?php } ?>
               <?php } else {?>
                   <?php if ($product['price_netto']) { ?>
                       <?php if($this->config->get('megjelenit_netto_ar') == 1){ ?>
                           <div class="price  <?php echo $szazalek_class?>"">
                               <?php if (!$product['special_netto']) { ?>
                                   <span class="price-new"><?php echo $product['price_netto']; ?></span>
                               <?php } else { ?>
                                   <span class="price-old"><?php echo $product['price_netto']; ?></span> <span class="price-new"><?php echo $product['special_netto']; ?></span>
                               <?php } ?>
                           </div>
                           <?php if (!$product['special_netto']) { ?>
                               <span class="price-tax"><?php echo "Brutto:"; ?> <?php echo $product['price']; ?></span>
                           <?php } else { ?>
                               <span class="price-tax"><?php echo "Brutto:"; ?> <?php echo $product['special']; ?></span>
                           <?php } ?>
                       <?php } else {?>

                           <div class="price">
                               <?php if (!$product['special']) { ?>
                                   <span class="price-new"><?php echo $product['price']; ?></span>
                               <?php } else { ?>
                                   <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                               <?php } ?>
                           </div>
                       <?php } ?>
                   <?php } ?>
                <?php } ?>


           <?php } elseif ($this->config->get('megjelenit_netto_ar') == 1){ ?>
               <?php if ($product['price_netto']) { ?>
                   <div class="price  <?php echo $szazalek_class?>">
                       <?php if (!$product['special_netto']) { ?>
                           <span class="text-netto"><?php echo $text_netto; ?></span>
                           <?php if ($this->config->get('megjelenit_netto_ujsor') == 1){ ?>
                               <br />
                           <?php } ?>
                           <span class="price-new"><?php echo $product['price_netto']; ?></span>
                       <?php } else { ?>
                           <span class="text-netto"><?php echo $text_netto; ?></span>
                           <?php if ($this->config->get('megjelenit_netto_ujsor') == 1){ ?>
                               <br />
                           <?php } ?>
                           <span class="price-old"><?php echo $product['price_netto']; ?></span> <span class="price-new"><?php echo $product['special_netto']; ?></span>
                       <?php } ?>
                   </div>
               <?php } ?>
           <?php } ?>
           <div class="price"><?php echo $text_eredeti_ar?> <?php echo $product['eredeti_ar']?></div>



           <?if ($product['csomagolasi_mennyiseg'] > 1 && $this->config->get('megjelenit_csomagolas_admin') == 1 ){ ?>
               <div class="cart">

                   <input type="hidden" name="quantity2" />
                   <input id="category_list_csomagolas_<?php echo $product['product_id']; ?>" type="hidden" value="<? echo $product['csomagolasi_mennyiseg']?>" />
                   <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){ ?>
                   <div class="csomagolasi_mennyiseg_racs" >
                       <? }?>

                       <?php if($this->config->get('megjelenit_mennyiseg') == 1 || true){ ?>
                           <input class="mennyit_vasarol" id="category-grid-quantity_<?php echo $product['product_id']; ?>" type="text" value="1" />
                           <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){ ?>
                               <span class="csomagolasi_mennyiseg_span" style="margin-left: 4px;"><?echo 'x '.$product['csomagolasi_mennyiseg'].' '. $product['megyseg'].'/'.$product['csomagolasi_egyseg']?></span>
                           <?php }?>

                       <?php } else {?>
                           <input  id="category-grid-quantity_<?php echo $product['product_id']; ?>" type="hidden" value="1" />
                           <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){ ?>
                               <span class="csomagolasi_mennyiseg_span"><?echo $product['csomagolasi_mennyiseg'].' '. $product['megyseg'].'/'.$product['csomagolasi_egyseg']?></span>
                           <?php }?>
                       <?php }?>

                       <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){ ?>
                   </div>
               <? }?>
                   <span class="atmenet-hatter">
                <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>',$('#category-grid-quantity_<?php echo $product['product_id']; ?>').val()*<? echo $product['csomagolasi_mennyiseg']?>,'<?php echo $this->config->get('config_template')?>','<?php echo $module_name.$product['product_id']?>')" class="button" />
                </span>
               </div>

           <? } else { ?>
               <div class="cart">
                   <?php $product['csomagolasi_mennyiseg'] = 1; ?>
                   <?php if($this->config->get('megjelenit_mennyiseg') == 1 || true){ ?>
                       <input class="mennyit_vasarol" id="category-grid-quantity_<?php echo $product['product_id']; ?>" type="text" value="1" style="margin-right: 1px;" />
                   <?php } else { ?>
                       <input id="category-grid-quantity_<?php echo $product['product_id']; ?>" type="hidden" value="1" style="margin-right: 1px;" />
                   <?php }?>
                   <span class="atmenet-hatter">
                <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>',$('#category-grid-quantity_<?php echo $product['product_id']; ?>').val()*<? echo $product['csomagolasi_mennyiseg']?>,'<?php echo $this->config->get('config_template')?>','<?php echo $module_name.$product['product_id']?>')" class="button" />
            <span>
               </div>

           <? }?>
        </div>
   </div>


       <?php } ?>
   </div>

</div>


<?php } ?>


