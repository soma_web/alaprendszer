<?php
// Heading
$_['heading_title']     = 'Export / Import';

// Text
$_['text_success']      = 'Siker: A kategóriák és termékek importálása sikeres!';

// Entry
$_['entry_restore']     = 'Importálás a táblázat file-ból:';
$_['entry_description'] = 'Használja ezt a funciót összes kategóriái és termékei exportálására vagy importálására XLS táblázatból vagy abba bele.';

// Error
$_['error_permission']                  = 'Figyelmeztetés! Nincs engedélye az export/import módosítására.';
$_['error_upload']                      = 'A feltöltött file nem érvényes táblázat file vagy az értékei nincsenek elvárt formátumban!';
$_['error_not_upload']                  = 'Nem töltött fel XLS táblázatot! (Choose file)';
$_['error_sheet_count']                 = 'Export/Import: Érvénytelen számú munkalap, 7 munkalap elvárt';
$_['error_categories_header']           = 'Export/Import: Érvénytelen fejléc a kategóriák munkalapon';
$_['error_products_header']             = 'Export/Import: Érvénytelen fejléc a termékek munkalapon';
$_['error_options_header']              = 'Export/Import: Érvénytelen fejléc a beallítások munkalapon';
$_['error_attributes_header']           = 'Export/Import: Érvénytelen fejléc az attribútum munkalapon';
$_['error_specials_header']             = 'Export/Import: Érvénytelen fejléc a speciális munkalapon';
$_['error_discounts_header']            = 'Export/Import: Érvénytelen fejléc a akciók munkalapon';
$_['error_rewards_header']              = 'Export/Import: Érvénytelen fejléc a jutalmak munkalapon';
$_['warning_download_limit_tol']        = 'Érvénytelen kezdő érték! Az adatbázisban nincs ennyi termék ...';
$_['warning_download_limit_ig_kisebb']  = 'Érvénytelen záró érték! A záró érték nem lehet kevesebb, mint a kezdő! (Vagy a zárónak ne adjon meg értéket...)';

// Button labels
$_['button_import']     = 'Import';
$_['button_export']     = 'Export';
?>