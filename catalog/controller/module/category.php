<?php
class ControllerModuleCategory extends Controller {
    protected function index($setting) {
        $this->language->load('module/category',$this->config->get('config_template'));

        $this->data['heading_title'] = $this->language->get('heading_title');
        if ($this->config->get('category_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string)$this->request->get['path']);
        } else {
            $parts = array();
        }

        if (isset($parts[0])) {
            $this->data['category_id'] = $parts[0];
        } else {
            $this->data['category_id'] = 0;
        }

        if (isset($parts[1])) {
            $this->data['child_id'] = $parts[1];
        } else {
            $this->data['child_id'] = 0;
        }

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $this->data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            $children_data = array();

            $children = $this->model_catalog_category->getCategories($category['category_id']);

            foreach ($children as $child) {
                $data = array(
                    'filter_category_id'  => $child['category_id'],
                    'filter_sub_category' => true
                );

                $ha_piacter=$this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']);

                if ($category['piacter'] == 1){
                    $ha_piacter.="&piacter=piacter";
                }

                if ($setting['count']) {
                    $product_total = $this->model_catalog_product->getTotalProducts($data);

                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name'        => $child['name'] . ' (' . $product_total . ')',
                        'href'        => $ha_piacter
                    );
                } else {
                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name'        => $child['name'],
                        'href'        => $ha_piacter
                    );
                }
            }

            $data = array(
                'filter_category_id'  => $category['category_id'],
                'filter_sub_category' => true
            );

            if ($setting['count']) {
                $product_total = $this->model_catalog_product->getTotalProducts($data);

                $ha_piacter=$this->url->link('product/category', 'path=' . $category['category_id']);
                if ($category['piacter'] == 1){
                    $ha_piacter.="&piacter=piacter";
                }
                $this->data['categories'][] = array(
                    'category_id' => $category['category_id'],
                    'name'        => $category['name'],
                    'product-count' => $this->model_catalog_product->getTotalProducts($data),
                    'children'    => $children_data,
                    'href'        => $ha_piacter
                );
            } else {
                $this->data['categories'][] = array(
                    'category_id' => $category['category_id'],
                    'name'        => $category['name'],
                    'children'    => $children_data,
                    'href'        => $ha_piacter
                );
            }
        }

        $this->data['kategoria_lista'] = false;

        if ($this->config->get('category_module_lista') == 1 ) {
            $this->data['kategoria_lista'] = false;
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/category.tpl';
        } else {
            $this->template = 'default/template/module/category.tpl';
        }

        $this->render();
    }
}
?>