<?php

// Heading
$_['heading_title']     = 'Tulajdonság csoportok';

// Text
$_['text_success']      = 'Sikeresen módosította a Tulajdonság csoportokat!';
$_['text_szukit']       = 'Szűkít';
$_['text_bovit']        = 'Bővít';
$_['text_alap']         = 'Alap beállítás';

// Column
$_['column_name']       = 'Tulajdonság csoport neve';
$_['column_sort_order'] = 'Sorrend';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_name']            = 'Tulajdonság csoport neve:';
$_['entry_sort_order']      = 'Sorrend:';
$_['entry_szuro_bovit']     = 'Szűrő - szűkít/bővít:';
$_['entry_szuro_csuszka']   = 'Csúszkás megjelenítés:';
$_['entry_vezerel_kategoriat'] = 'Kategória felülbírálása:<br><span class="help">A tulajdonságnak megfelelő kategóriákat tölti be a kiválasztásnál.</span>';

// Error
$_['error_permission']  = 'Figyelem: Önnek nincs joga módodsítani a tulajdonság csoportokat!';
$_['error_name']        = 'A tulajdonság csoport hosszának 3 és 64 karakter közt kell lennie!';
$_['error_attribute']   = 'Figyelem: Ez a tulajdonság csoport nem törölhető, amíg hozzá van rendelve %s tulajdonsághoz!';
$_['error_product']     = 'Figyelem: Ez a tulajdonság csoport nem törölhető, amíg hozzá van rendelve %s termékhez!';
?>
