<p style="margin-top: 0px !important; margin-bottom: 20px !important;"><?php echo $text_hi; ?></p>
<p style="margin-top: 0px !important; margin-bottom: 20px !important;"><?php echo $text_info; ?></p>
<p style="margin-top: 20px !important; margin-bottom: 5px !important;"><?php echo $text_cart_content; ?></p>

<table style="border-collapse: collapse; width: 100%; border-top: 1px dotted <?php echo $table_border_color; ?>;  margin-bottom: 20px;">
<tbody>
  <?php foreach ($products as $product) { ?>
  <tr>
	<td style="font-size: 12px; text-align: left; background-color:<?php echo $table_body_bg; ?>; color:<?php echo $table_body_text_color; ?>; border-bottom: 1px dotted <?php echo $table_border_color; ?>; padding: 7px; width: 55px;"><a href="<?php echo $product['href']; ?>" target="blank"><img src="<?php echo $product['image']; ?>" /></a></td>
	<td style="font-size: 12px;	text-align: left; background-color:<?php echo $table_body_bg; ?>; color:<?php echo $table_body_text_color; ?>; border-bottom: 1px dotted <?php echo $table_border_color; ?>; padding: 7px;"><?php echo $product['quantity']; ?> X <a href="<?php echo $product['href']; ?>" target="blank"><?php echo $product['name']; ?></a></td>
	<td style="font-size: 12px;	text-align: left; background-color:<?php echo $table_body_bg; ?>; color:<?php echo $table_body_text_color; ?>; border-bottom: 1px dotted <?php echo $table_border_color; ?>; padding: 7px;">[ <a href="<?php echo $product['href']; ?>" target="blank"><?php echo $text_view_product_detail; ?></a> ]</td>
  </tr>
  <?php } ?>
</tbody>
</table>

<?php if ($allow_coupon) { ?>
<p style="margin-top: 30px !important;"><?php echo $text_coupon; ?></p> 
<p style="margin-top: 10px !important; font-size: 14px;"><?php echo $text_coupon_code; ?></p> 
<p style="margin-top: 3px !important; font-size: 10px;"><?php echo $text_coupon_notice; ?></p> 
<?php } ?>

<p style="margin-top: 10px !important; margin-bottom: 20px !important;"><?php echo $text_order_now; ?></p>