<?php if (isset($thumb) && isset($thumb_height) && isset($thumb_width) && $thumb && $thumb_height && $thumb_width && false) { ?>
    <div class="image" style="max-width: <?php echo $thumb_width; ?>px;">

        <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="fancy" ref="fancy-pics" data-fancybox-group="gallery-1" >
            <img src="<?php echo $thumb; ?>" style="max-width: <?php echo $thumb_width; ?>px; max-height: <?php echo $thumb_height; ?>px;" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="<?php echo $module_name.$product_id  ?>" />
        </a>
    </div>
<?php } elseif (!empty($thumb)) { ?>
    <div class="image">

        <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="fancy" ref="fancy-pics" data-fancybox-group="gallery-1" >
            <img src="<?php echo $thumb; ?>" style="max-width:100%;" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="<?php echo $module_name.$product_id  ?>" />
        </a>
    </div>
<?php } ?>
