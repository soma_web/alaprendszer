<?php echo $header;?>

<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/category.png" alt="" /> <?php echo $heading_title; ?></h1>


            <div class="buttons"><a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a><a onclick="$('#form').submit();" class="button"><?php echo $button_delete; ?></a></div>

            <?php if ($megjelenit_piacter) {?>
                <div style="display: inline-block; margin-right: 50px; float: right;padding-top: 8px;">
                    <form name=talloz enctype='multipart/form-data' action="<? echo $csvinsert;?>" method='post'>
                        <input type="hidden" name="MAX_FILE_SIZE" value="300000" /><?php echo $entry_csvk_piacternek; ?>
                        <input name='userfile' type='file' accept='text/csv' id=filein onChange=kk(this)>
                        <input type='submit' name="kepekrogzit" style='display:none' id=inputfeltolt value='Feltöltés'>
                        <input type="hidden" id="hiddenobj" name="melyik_editkep">
                    </form>
                </div>
            <?php } ?>
        </div>
        <div class="content">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="list">
                    <thead>
                    <tr>
                        <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                        <td class="left"><?php echo $column_name; ?></td>
                        <td class="right"><?php echo $column_sort_order; ?></td>
                        <td class="right"><?php echo $column_action; ?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if ($categories) { ?>
                        <?php foreach ($categories as $category) { ?>
                            <tr>
                                <td style="text-align: center;"><?php if ($category['selected']) { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                                    <?php } else { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" />
                                    <?php } ?></td>
                                <td class="left"><?php echo $category['name']; ?></td>
                                <td class="right"><?php echo $category['sort_order']; ?></td>
                                <td class="right"><?php foreach ($category['action'] as $action) { ?>
                                        [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                    <?php } ?></td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>

<script>

    function kk(para1){
        var kiterjesztes=para1.value.substr(para1.value.length-3,3);

        if (kiterjesztes.toUpperCase() =="CSV"){
            eval("document.getElementById('inputfeltolt').style.display=''");
        } else{
            alert("Csak CSV kiterjesztésű file tölthető fel!");
            eval("document.getElementById('inputfeltolt').style.display='none'");
        }
    }

</script>