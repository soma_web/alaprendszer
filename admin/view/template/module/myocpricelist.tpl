<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
            <tr>
                <td><?php echo $entry_fejlec_latszik; ?></td>
                <td colspan=3><?php if ($myocwpl_customer_fejlec) { ?>
                        <input type="radio" name="myocwpl_customer_fejlec" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="myocwpl_customer_fejlec" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="myocwpl_customer_fejlec" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="myocwpl_customer_fejlec" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

        </table>
      <table class="form">
        <tr>
          <td><?php echo $entry_status; ?></td>
          <td><select name="myocwpl_status">
                <option value="1"<?php if ($myocwpl_status) { ?> selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                <option value="0"<?php if (!$myocwpl_status) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
              </select>
          </td>
        </tr>
        <tr>
          <td><?php echo $entry_login; ?></td>
          <td>
            <input type="radio" name="myocwpl_login" value="1" id="myocwpl_login_1"<?php if ($myocwpl_login) { ?> checked="checked"<?php } ?>>
            <label for="myocwpl_login_1"><?php echo $text_yes; ?></label>
            <input type="radio" name="myocwpl_login" value="0" id="myocwpl_login_0"<?php if (!$myocwpl_login) { ?> checked="checked"<?php } ?>>
            <label for="myocwpl_login_0"><?php echo $text_no; ?></label>
          </td>
        </tr>
        <tr>
          <td><?php echo $entry_customer_group; ?></td>
          <td><div class="scrollbox">
              <?php $class = 'even'; ?>
              <?php foreach ($customer_groups as $customer_group) { ?>
              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
              <div class="<?php echo $class; ?>">
                <?php if (!empty($myocwpl_customer_groups) && in_array($customer_group['customer_group_id'], $myocwpl_customer_groups)) { ?>
                <input type="checkbox" name="myocwpl_customer_group[]" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
                <?php echo $customer_group['name']; ?>
                <?php } else { ?>
                <input type="checkbox" name="myocwpl_customer_group[]" value="<?php echo $customer_group['customer_group_id']; ?>" />
                <?php echo $customer_group['name']; ?>
                <?php } ?>
              </div>
              <?php } ?>
            </div></td>
        </tr>
        <tr>
          <td><?php echo $entry_store; ?></td>
          <td><div class="scrollbox">
              <?php $class = 'even'; ?>
              <div class="<?php echo $class; ?>">
                <?php if (!empty($myocwpl_stores) && in_array('0', $myocwpl_stores)) { ?>
                <input type="checkbox" name="myocwpl_store[]" value="0" checked="checked" />
                <?php echo $text_default; ?>
                <?php } else { ?>
                <input type="checkbox" name="myocwpl_store[]" value="0" />
                <?php echo $text_default; ?>
                <?php } ?>
              </div>
              <?php foreach ($stores as $store) { ?>
              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
              <div class="<?php echo $class; ?>">
                <?php if (!empty($myocwpl_stores) && in_array($store['store_id'], $myocwpl_stores)) { ?>
                <input type="checkbox" name="myocwpl_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                <?php echo $store['name']; ?>
                <?php } else { ?>
                <input type="checkbox" name="myocwpl_store[]" value="<?php echo $store['store_id']; ?>" />
                <?php echo $store['name']; ?>
                <?php } ?>
              </div>
              <?php } ?>
            </div></td>
        </tr>
          <tr>
              <td><?php echo $entry_column_picture;?></td>
              <td>
                  <?php if($myocwpl_picture == 1) { ?>
                      <input type="checkbox" value="1" name="myocwpl_picture" checked="checked" />
                  <?php } else { ?>
                      <input type="checkbox" value="1" name="myocwpl_picture" />
                  <?php } ?>
              </td>
          </tr>
          <tr>
              <td><?php echo $entry_column_model;?></td>
              <td>
                  <?php if($myocwpl_model == 1) { ?>
                      <input type="checkbox" value="1" name="myocwpl_model" checked="checked" />
                  <?php } else { ?>
                      <input type="checkbox" value="1" name="myocwpl_model" />
                  <?php } ?>
              </td>
          </tr>
          <tr>
              <td><?php echo $entry_column_rate;?></td>
              <td>
                  <?php if($myocwpl_rate == 1) { ?>
                      <input type="checkbox" value="1" name="myocwpl_rate" checked="checked" />
                  <?php } else { ?>
                      <input type="checkbox" value="1" name="myocwpl_rate" />
                  <?php } ?>
              </td>
          </tr>
          <tr>
              <td><?php echo $entry_column_buy;?></td>
              <td>
                  <?php if($myocwpl_buy == 1) { ?>
                      <input type="checkbox" value="1" name="myocwpl_buy" checked="checked" />
                  <?php } else { ?>
                      <input type="checkbox" value="1" name="myocwpl_buy" />
                  <?php } ?>
              </td>
          </tr>
      </table>
    </form>

  </div>
</div>
<?php echo $footer; ?>