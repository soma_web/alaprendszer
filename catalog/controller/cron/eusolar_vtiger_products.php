<?php   
class ControllerCronEusolarVtigerProducts extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;


        $this->load->model("cron/altalanos");
        $this->load->model('cron/eusolar_vtiger_products');

        if ($cron_id) {
            $input = $this->model_cron_altalanos->getInputElements($cron_id);
        }
        echo 'Inditás<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $products = $this->model_cron_eusolar_vtiger_products->getChangeProducts();

        echo '<br><br>Products sor: '.count($products).' db';
        $products_change = array(
            'felvitel'  => 0,
            'modositas'  => 0
        );

        if ($products) {
            foreach($products as $value) {
                $vissza = $this->model_cron_eusolar_vtiger_products->WriteProducts($value['productid']);
                if (!empty($input['csomagarak_aktualizalasa'])) {
                    $vissza = $this->model_cron_eusolar_vtiger_products->csomagarakAktualizalasa($value['productid']);
                }
                $products_change['modositas'] += $vissza['modositas'];
                $products_change['felvitel'] += $vissza['felvitel'];
                $this->model_cron_eusolar_vtiger_products->deleteTrigger($value['product_to_webshop_id']);
            }
        }

        $this->log->write_crone('Prosducts össz.: ' .count($products_change).'   módosítva: '. $products_change['modositas'].'   új: '.$products_change['felvitel'] );

        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}
?>