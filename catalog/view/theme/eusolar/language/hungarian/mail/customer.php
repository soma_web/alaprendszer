<?php
// Text
$_['text_subject']  = '%s - Köszönjük a regisztrációját!';
$_['text_user']     = 'Kedves %s!';
$_['text_welcome']  = 'Üdvözöljük és köszönjük a regisztrációját a %s webáruházban!';
$_['text_login']    = 'Elkészült a felhasználói fiókja! Mostmár beléphet az e-mail címével és jelszavával a weboldalunkra vagy a következő hivatkozásra kattintva:';
$_['text_approval'] = 'A bejelentkezéshez fiókját még jóvá kell hagynunk. A jóváhagyást követően beléphet az e-mail címével és jelszavával a weboldalunkra vagy a következő URL-en:';
$_['text_services'] = 'Bejelentkezés után a következő szolgáltatásokat tudja majd elérni: régi és új rendelések nyomonkövetése, felhasználói adatok módosítása.';
$_['text_thanks']   = 'Köszönet,';

$_['text_adatai']   = 'Megadott adatai:';
$_['text_data_email']     = 'Email címe: %s';
$_['text_data_jelszo']    = 'Jelszava  : %s';
$_['text_data_telefon']   = 'Telefonszáma: %s';
$_['text_data_ceg']       = 'Cég neve: %s';
$_['text_data_ado']       = 'Adószáma: %s';
$_['text_data_iranyito']  = 'Irányítószáma: %s';
$_['text_data_varos']     = 'Város: %s';
$_['text_data_utca']      = 'Utca: %s';
$_['text_data_orszag']    = 'Ország: %s';

$_['text_data_vezeteknev']          = 'Vezetéknév: %s';
$_['text_data_keresztnev']          = 'Keresztnév: %s';
$_['text_data_fax']                 = 'Fax: %s';
$_['text_data_cimadat_kiegeszito']  = 'Kiegészítő címadatok: %s';
$_['text_data_megye']               = 'Megye: %s';

$_['text_data_hirlevel']           = 'Hírlevél: %s';
$_['text_data_igen']               = 'Igen';
$_['text_data_nem']                = 'Nem';
?>