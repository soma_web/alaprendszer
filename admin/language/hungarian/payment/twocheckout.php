<?php

// Heading
$_['heading_title']      = '2Checkout';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Siker: A 2Checkout fiók adatainak módosítása megtörtént!';

// Entry
$_['entry_account']      = '2Checkout Fiók ID:';
$_['entry_secret']       = 'Titkos szó:<br /><span class="help">A titkos szó amivel megerősíti a tranzakciót (ugyan annak kell lennie, mint ami meg lett adva a kereskedő fiók beállítás oldalon.)</span>';
$_['entry_test']         = 'Teszt mód:';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';

// Error
$_['error_permission']   = 'Figyelmeztetés: Az 2Checkout történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_account']      = 'A fiókszám megadása kötelező!';
$_['error_secret']       = 'Titkos szó szükséges!';
?>
