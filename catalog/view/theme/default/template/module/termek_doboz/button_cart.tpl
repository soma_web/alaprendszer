<?php if ( !empty($product['elorendeles']) || $product['mutat_arat'] || !empty($product['megrendelem']) ) { ?>

    <div class="<?php echo isset($beallitas['class_name']) ? $beallitas['class_name'] : 'cart' ?> " >

        <?php if(empty($_SESSION['ar_atszamol'])  ) { ?>

            <?php if(empty($product['elorendeles']) ) { ?>
                <?php if(empty($product['megrendelem']) ) { ?>

                    <?php if (isset($beallitas['cart_input'])) { ?>
                        <input class="mennyit_vasarol" id="quantity_<?php echo $product['product_id']; ?>" type="text"
                               value="<?php if(isset($product['minimum'])) { echo $product['minimum']; } else { echo "1"; } ?>" />
                    <?php } else { ?>
                        <input type="hidden" id="quantity_<?php echo $product['product_id']; ?>" value="1" />
                    <?php } ?>

                    <?php $product['csomagolasi_mennyiseg'] = $product['csomagolasi_mennyiseg'] > 1 ? $product['csomagolasi_mennyiseg'] : 1?>

                    <input type="button" value="<?php echo $button_cart; ?>"
                       onclick="addToCart('<?php echo $product['product_id']; ?>',$('#quantity_<?php echo $product['product_id']; ?>').val()*<? echo $product['csomagolasi_mennyiseg']?>,'<?php echo $this->config->get('config_template')?>','<?php echo $module_name.$product['product_id']?>')" class="button" />

                <?php } else { ?>
                    <input type="button" value="<?php echo $button_megrendelem; ?>" class="button arajanlat"
                           onclick="megrendelem('<?php echo $product['product_id']; ?>')"/>
                <?php }  ?>

            <?php } else { ?>
                <input type="button" value="<?php echo $button_arajanlat; ?>" class="button arajanlat"
                    onclick="arajanlatotKerek('<?php echo $product['product_id']; ?>')"/>
            <?php } ?>



        <?php } else { ?>
            <input type="button" class="kosar button elorendeles" value="<?php echo $button_set_valos_ar; ?>"
                   onclick="ujratolt(0)"/>
        <?php }  ?>
    </div>
<?php } ?>
