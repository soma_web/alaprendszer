<?php

// Heading
$_['heading_title']                = 'UPS';

// Text
$_['text_shipping']                = 'Szállítás';
$_['text_success']                 = 'Siker: Az UPS szállítási mód módosítása megtörtént!';
$_['text_regular_daily_pickup']    = 'Rendszeres napi felvétel';
$_['text_daily_pickup']            = 'Napi felvétel';
$_['text_customer_counter']        = 'Vevőszámláló';
$_['text_one_time_pickup']         = 'Egyszeri felvétel';
$_['text_on_call_air_pickup']      = 'Hívásra légi felvétel';
$_['text_letter_center']           = 'Levélközpont';
$_['text_air_service_center']      = 'Légi szolgáltatás központ';
$_['text_suggested_retail_rates']  = 'Ajánlott fogyasztói árak (UPS Bolt)';
$_['text_package']                 = 'Csomag';
$_['text_ups_letter']              = 'UPS Levél';
$_['text_ups_tube']                = 'UPS Cső';
$_['text_ups_pak']                 = 'UPS Pak';
$_['text_ups_express_box']         = 'UPS Expressz Doboz';
$_['text_ups_25kg_box']            = 'UPS 25kg doboz';
$_['text_ups_10kg_box']            = 'UPS 10kg doboz';
$_['text_us']                      = 'USA-beli származás';
$_['text_ca']                      = 'Canadai származás';
$_['text_eu']                      = 'Európai únióbeli származás';
$_['text_pr']                      = 'Puerto Rico-i származás';
$_['text_mx']                      = 'Mexikói származás';
$_['text_other']                   = 'Minden egyéb származás';
$_['text_test']                    = 'Teszt';
$_['text_production']              = 'Előállítás';
$_['text_residential']             = 'Tartózkodási';
$_['text_commercial']              = 'Kereskedelmi';
$_['text_next_day_air']            = 'UPS Következő napi légi';
$_['text_2nd_day_air']             = 'UPS Második napi légi';
$_['text_ground']                  = 'UPS Földi';
$_['text_3_day_select']            = 'UPS 3 napi választás';
$_['text_next_day_air_saver']      = 'UPS Következő napi légi megtakarító';
$_['text_next_day_air_early_am']   = 'UPS Következő napi légi korareggeli';
$_['text_2nd_day_air_am']          = 'UPS Második napi légi reggeli';
$_['text_saver']                   = 'UPS Megtakarító';
$_['text_worldwide_express']       = 'UPS Expressz világszerte';
$_['text_worldwide_expedited']     = 'UPS Gyorsított világszerte';
$_['text_standard']                = 'UPS Szabványos';
$_['text_worldwide_express_plus']  = 'UPS Expressz plusz világszerte';
$_['text_express']                 = 'UPS Expressz';
$_['text_expedited']               = 'UPS Gyorsított';
$_['text_express_early_am']        = 'UPS Expressz kora reggeli';
$_['text_express_plus']            = 'UPS Expressz plusz';
$_['text_today_standard']          = 'UPS Mai szabványos';
$_['text_today_dedicated_courier'] = 'UPS Mai ajánlott futár';
$_['text_today_intercity']         = 'UPS Mai városközi';
$_['text_today_express']           = 'UPS Mai expressz';
$_['text_today_express_saver']     = 'UPS Mai expressz megtakarító';

// Entry
$_['entry_key']                    = 'Hozzáférési kulcs:<span class="help">Adja meg az XML értékek hozzáférési kulcsát amit a UPS Önnek meghatározott.</span>';
$_['entry_username']               = 'Felhasználónév:<span class="help">Adja meg a UPS szolgáltatások fiókjának felhasználónevét.</span>';
$_['entry_password']               = 'Jelszó:<span class="help">Adja meg a UPS szolgáltatások fiókjának jelszavát.</span>';
$_['entry_pickup']                 = 'Felvételi mód:<span class="help">Hogyan adja át a csomagokat a UPS-nek (csak akkor, ha az USÁ-ból származik)?</span>';
$_['entry_packaging']              = 'Csomagolás típusa:<span class="help">milyen csomagolást használ?</span>';
$_['entry_classification']         = 'Ügyfél besorolás kódja:<span class="help">01 - Ha egy UPS fiókba számláz és napi UPS felvétele van, 03 - Ha nincs UPS fiókja vagy egy UPS fiókba számláz, de nincs napi felvétel, 04 - Ha egy kiskereskedelmi egységből szállít (csak akkor, ha az USÁ-ból származik)</span>';
$_['entry_origin']                 = 'Szállítás származási kódja:<span class="help">Milyen származási pont legyen használva (ez a beállítás csak azokra UPS termékek nevére van hatással, amik a felhasználónak meg vannak mutatva)</span>';
$_['entry_city']                   = 'Származási város:<span class="help">Adja meg a származási város nevét.</span>';
$_['entry_state']                  = 'Származási állam/tartomány:<span class="help">Adja meg az Ön származási állam/tartományányának kétbetűs kódját.</span>';
$_['entry_country']                = 'Származási ország:<span class="help">Adja meg az Ön származási országának kétbetűs kódját.</span>';
$_['entry_postcode']               = 'Származási Zip-kódját/irányítószámát:<span class="help">Adja meg az Ön származási zip-kódját/irányítószámát.</span>';
$_['entry_test']                   = 'Teszt mód:<span class="help">Használja ezt a modult Teszt (IGEN) vagy Kibocsátás (NEM) módban?</span>';
$_['entry_quote_type']             = 'Árajánlat típusa:<span class="help">Árajánlat lakossági vagy kereskedelmi szállításra.</span>';
$_['entry_service']                = 'Szolgáltatások:<span class="help">Válassza ki a UPS által felajánlott szolgáltatásokat.</span>';
$_['entry_display_weight']         = 'Szállítási súly kijelzése:<br /><span class="help">Akarja, hogy ki legyen jelezve a szállítási súly? (pl. Szállítási súly : 2.7674 Kg)</span>';
$_['entry_weight_code']            = 'Súly kód:<br /><span class="help">Kg vagy lb megengedett. Győződjön meg róla, hogy megegyezik a súlymérték a UPS által elfogadott súly kóddal.</span>';
$_['entry_weight_class']           = 'Súlymérték:<span class="help">Állítsa kilogrammra vagy fontra.</span>';
$_['entry_tax']                    = 'Adóosztály:';
$_['entry_geo_zone']               = 'Földrajzi zóna:';
$_['entry_status']                 = 'Állapot:';
$_['entry_sort_order']             = 'Sorrend:';
$_['entry_insurance']              = 'Biztosítást kér:';
$_['entry_length_code']            = 'Hosszúság kód:';
$_['entry_length_class']           = 'Hosszúság mérték:';
$_['entry_dimension']              = 'Méret x y z:';
$_['entry_tax_class']              = 'Adóosztály:';
$_['entry_debug']                  = 'Hibakeresési mód:';
$_['entry_title_heading'] = 'Fejléc megnevezése:';
$_['entry_title_text'] = 'Szállítási mód megnevezése:';
$_['entry_mail']        = 'Egyéb figyelmeztető e-mailek:<br /><span class="help">Egyéb figyelmeztető e-mailek, amiket kapni szeretne, a közponzi áruház  e-mailjei mellett. (vesszővel elválasztva)</span>';


// Error
$_['error_permission']             = 'Figyelmeztetés: Az UPS szállítási mód módosítása az Ön számára nem engedélyezett!';
$_['error_key']                    = 'Hozzáférési kulcs szükséges!';
$_['error_username']               = 'Felhasználónév szükséges!';
$_['error_password']               = 'Jelszó szükséges!';
$_['error_city']                   = 'Származási város!';
$_['error_state']                  = 'Származási Állam/Megye szükséges!';
$_['error_country']                = 'Származási ország szükséges!';
?>
