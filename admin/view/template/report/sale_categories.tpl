<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="box">
    <div class="heading">
        <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="nyomtat">
            <a onclick="window.print();" class="button"><?php echo $button_nyomtatas; ?></a>
            <a href='<?php echo $csv_be; ?>' class="button"><?php echo $button_exportalas; ?></a>
        </div>
    </div>
    <div class="content">

      <table class="list">
        <thead>
          <tr>
            <td class="center"><?php echo $column_categories; ?></td>
            <td class="center"><?php echo $column_ajanlat_kupon; ?></td>
            <td class="center"><?php echo $column_ajanlat_voucher; ?></td>
            <td class="center"><?php echo $column_vasarolt_kupon; ?></td>
            <td class="center"><?php echo $column_vasarolt_voucher; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($sorok) { ?>
          <?php foreach ($sorok as $sor) { ?>
          <tr>
            <td class="left"><?php echo $sor['name']; ?></td>
            <td class="center"><?php echo $sor['ajanlat_kupon']; ?></td>
            <td class="center"><?php echo $sor['ajanlat_voucher']; ?></td>
            <td class="center"><?php echo $sor['vasarolt_kupon']; ?></td>
            <td class="center"><?php echo $sor['vasarolt_voucher']; ?></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=report/sale_categories&token=<?php echo $token; ?>';
	
	var filter_categories = $('input[name=\'filter_categories\']').attr('value');
	
	if (filter_categories) {
		url += '&filter_categories=' + encodeURIComponent(filter_categories);
	}

	location = url;
}
//--></script> 

<?php echo $footer; ?>