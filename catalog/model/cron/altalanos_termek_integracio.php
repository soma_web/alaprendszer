<?php
class ModelCronAltalanosTermekIntegracio extends Model {

    public function getIntegracions() {
        $sql = "SELECT * FROM " . DB_PREFIX . "termek_integracio WHERE status = 1";

        $query = $this->db->query($sql);
        foreach($query->rows as $key=>$value) {
            if (!empty($value['connects'])) {
                $query->rows[$key]['products'] = unserialize($value['products']);
                $query->rows[$key]['connects'] = unserialize($value['connects']);
            } else {
                unset ($query->rows[$key]);
            }
        }
        return $query->rows;
    }

}