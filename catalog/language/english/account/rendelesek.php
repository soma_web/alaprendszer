<?php
// Heading
$_['heading_title']   = 'Download the report';


// Text
$_['text_account']    = 'Account';
$_['text_home']       = 'Shop';

$_['entry_date_start']  = 'Begin:';
$_['entry_date_end']    = 'Over:';
$_['entry_korosztaly']  = 'Ages:';
$_['entry_nem']         = 'Sex:';
$_['entry_group']       = 'Grouping: ';
$_['entry_status']      = 'order Status:';

$_['text_all_status']   = 'each state';
$_['text_week']         = 'Grouping:';

$_['select_ferfi']      = 'Man';
$_['select_holgy']      = 'Woman';

$_['text_year']         = 'Year';
$_['text_month']        = 'Month';
$_['text_week']         = 'Seven';
$_['text_day']          = 'Day';
$_['button_filter']     = 'Filter';

$_['column_date_start'] = 'Begin';
$_['column_date_end']   = 'Over';
$_['column_orders']     = 'Number of downloads';
$_['column_products']   = 'Number of Coupons';
$_['column_total']      = 'All';

$_['text_no_results']  = 'According to the given conditions do not download product';
?>