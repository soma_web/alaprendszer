<?php

// Heading
$_['heading_title']     = 'Garanciális jelentés';

// Text
$_['text_year']         = 'Év';
$_['text_month']        = 'Hónap';
$_['text_week']         = 'Hét';
$_['text_day']          = 'Nap';
$_['text_all_status']   = 'Minden állapot';

// Column
$_['column_date_start'] = 'Dátum indítás';
$_['column_date_end']   = 'Dátum vége';
$_['column_returns']    = 'Garanciák száma';
$_['column_products']   = 'Termékek száma';

// Entry
$_['entry_date_start']  = 'Kezdete:';
$_['entry_date_end']    = 'Vége:';
$_['entry_group']       = 'Csoportosítás:';
$_['entry_status']      = 'Garancia állapot:';
?>