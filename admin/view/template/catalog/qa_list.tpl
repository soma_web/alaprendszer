<?php echo $header; ?>
<!-- confirm deletion -->
<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="confirmDeleteLabel"><?php echo $text_confirm_delete; ?></h4>
      </div>
      <div class="modal-body">
        <p><?php echo $text_are_you_sure; ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> <?php echo $button_cancel; ?></button>
        <button type="button" class="btn btn-danger delete"><i class="fa fa-trash-o"></i> <?php echo $button_delete; ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="content" class="main-content">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li<?php echo ($breadcrumb['active']) ? ' class="active bread"' : ''; ?>><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <div class="alerts">
    <div class="container" id="alerts">
      <?php foreach ($alerts as $type => $_alert) { ?>
        <?php foreach ($_alert as $alert) { ?>
          <?php if ($alert) { ?>
      <div class="alert alert-<?php echo ($type == "error") ? "danger" : $type; ?> fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $alert; ?>
      </div>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    </div>
  </div>

  <div class="navbar-placeholder">
    <nav class="navbar navbar-default" role="navigation" id="bull5i-navbar">
      <div class="nav-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
            <span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand"><i class="fa fa-question-circle fa-fw ext-icon"></i> <?php echo $heading_title; ?></span>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
          <div class="navbar-right">
            <div class="nav navbar-nav navbar-form" id="nav-filter">
              <div class="form-group search-form">
                <label for="global-search" class="sr-only"><?php echo $text_search;?></label>
                <div class="search">
                  <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="<?php echo $text_search;?>" id="global-search" value="<?php echo $search; ?>">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button" id="<?php echo ($search) ? 'clear-' : ''; ?>global-search-btn"><i class="fa fa-<?php echo ($search) ? 'times' : 'search'; ?> fa-fw"></i></button>
                    </span>
                  </div>
                </div>
              </div>
              <?php if ($multilingual) { ?>
              <div class="form-group">
                <select id="selectLanguage" name="filter_language" class="form-control" rel="tooltip" data-original-title="<?php echo $text_display_language; ?>">
                  <option value=""<?php echo (!isset($filters['language']) || $filters['language'] == '') ? ' selected="selected"' : ''; ?>><?php echo $text_all_languages; ?></option>
                  <?php foreach ($languages as $lang) { ?>
                  <option value="<?php echo $lang['language_id']; ?>"<?php echo (isset($filters['language']) && $filters['language'] == $lang['language_id']) ? ' selected="selected"' : ''; ?>><?php echo $lang['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
              <?php } ?>
            </div>
            <div class="nav navbar-nav btn-group navbar-btn">
              <button type="button" class="btn btn-default" data-action="<?php echo $insert; ?>" id="btn-insert"><i class="fa fa-plus"></i> <?php echo $button_insert; ?></button>
              <button type="button" class="btn btn-default" data-action="<?php echo $copy; ?>" id="btn-copy" disabled><i class="fa fa-files-o"></i> <?php echo $button_copy; ?></button>
              <button type="button" class="btn btn-default" data-action="<?php echo $delete; ?>" id="btn-delete" disabled><i class="fa fa-trash-o"></i> <?php echo $button_delete; ?></button>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>

  <div class="content">
    <form method="post" enctype="multipart/form-data" id="beForm" class="form-horizontal" role="form">
      <fieldset>
        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-condensed table-striped table-list">
          <thead>
            <tr>
              <?php foreach ($columns as $column => $attr) {
               switch($column) {
                case 'selector': ?>
              <th class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?> col_<?php echo $column; ?>" width="1"><input type="checkbox" id="master-selector" /></th>
                <?php break;
                default: ?>
                <?php if (!empty($attr['sort'])) { ?>
              <th class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?> col_<?php echo $column; ?>"><a href="<?php echo $sorts[$column]; ?>"><?php echo $attr['name']; ?><?php echo ($sort == $attr['sort']) ? ' <i class="fa fa-sort-' . strtolower($order) . '"></i>' : ''; ?></a></th>
                <?php } else { ?>
              <th class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?> col_<?php echo $column; ?>"><?php echo $attr['name']; ?></th>
                <?php } ?>
                <?php break;
               } ?>
              <?php } ?>
            </tr>
            <tr class="filters">
              <?php foreach ($columns as $column => $attr) {
               switch($column) {
                case 'selector': ?>
              <td></td>
                <?php break;
                case 'status': ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>">
                <select name="filter_<?php echo $column; ?>" class="form-control input-sm fltr <?php echo $column; ?>" data-column="<?php echo $column; ?>">
                  <option value=""<?php echo (!isset($filters[$column]) || $filters[$column] == '') ? ' selected' : ''; ?>></option>
                  <option value="1"<?php echo (isset($filters[$column]) && $filters[$column] == '1') ? ' selected' : ''; ?>><?php echo $text_enabled; ?></option>
                  <option value="0"<?php echo (isset($filters[$column]) && $filters[$column] == '0') ? ' selected' : ''; ?>><?php echo $text_disabled; ?></option>
                </select>
              </td>
                <?php break;
                case 'action': ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-default" id="filter" rel="tooltip" title="<?php echo $text_filter; ?>"><i class="fa fa-filter fa-fw"></i></button>
                  <button type="button" class="btn btn-sm btn-default" id="clear-filter" rel="tooltip" title="<?php echo $text_clear_filter; ?>"><i class="fa fa-times fa-fw"></i></button>
                </div>
              </td>
                <?php break;
                case 'store': ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>">
                <select name="filter_<?php echo $column; ?>" class="form-control input-sm fltr <?php echo $column; ?>" data-column="<?php echo $column; ?>">
                  <option value=""<?php echo (!isset($filters[$column]) || $filters[$column] == '') ? ' selected' : ''; ?>></option>
                  <option value="*"<?php echo (isset($filters[$column]) && $filters[$column] == '*') ? ' selected' : ''; ?>><?php echo $text_none; ?></option>
                  <?php foreach ($stores as $s) { ?>
                  <option value="<?php echo $s['store_id']; ?>"<?php echo (isset($filters[$column]) && $filters[$column] == $s['store_id']) ? ' selected' : ''; ?>><?php echo $s['name']; ?></option>
                  <?php } ?>
                </select>
              </td>
                <?php break;
                case 'product': ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>"><input type="text" name="filter_<?php echo $column; ?>" value="<?php echo isset($filters[$column]) ? $filters[$column] : ''; ?>" class="form-control input-sm fltr <?php echo $column; ?> typeahead" placeholder="<?php echo $text_autocomplete; ?>" data-column="<?php echo $column; ?>"></td>
                <?php break;
                default: ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>"><input type="text" name="filter_<?php echo $column; ?>" value="<?php echo isset($filters[$column]) ? $filters[$column] : ''; ?>" class="form-control input-sm fltr <?php echo $column; ?>" data-column="<?php echo $column; ?>"></td>
                <?php break;
               } ?>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php if ($questions) { ?>
              <?php foreach ($questions as $q) { ?>
            <tr>
                <?php foreach ($columns as $column => $attr) {
                  switch($column) {
                    case 'selector': ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>"><input type="checkbox" name="selected[]" value="<?php echo $q['qa_id']; ?>"<?php echo ($q['selected']) ? ' checked' : ''; ?> /></td>
                <?php break;
                    case 'status': ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>"><span class="label label-<?php echo $q['status_class'];?>"><?php echo $q[$column]; ?></span></td>
                <?php break;
                    case 'question_author_name': ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>">
                <?php if ($q['customer_id']) { ?>
                <a href="<?php echo $customer . $q['customer_name']; ?>"><?php echo $q[$column]; ?></a>
                <?php } else { ?>
                <?php echo $q[$column]; ?>
                <?php } ?>
                <?php if ($q['author_details']) { ?>
                <button type="button" class="btn btn-default btn-xs pull-right" rel="popover" data-placement="right" data-html="true" data-trigger="manual" data-content="<?php echo $q['author_details']; ?>" data-popover-group="author-details"><i class="fa fa-eye"></i></button>
                <?php } ?>
              </td>
                <?php break;
                    case 'question':
                    case 'answer': ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>">
                <?php echo $q[$column]; ?>
                <?php if ($q[$column] != $q[$column . '_full']) { ?>
                <button type="button" class="btn btn-default btn-xs pull-right" rel="popover" data-placement="right" data-html="true" data-trigger="manual" data-content="<?php echo $q[$column . '_full']; ?>" data-popover-group="<?php echo $column; ?>"><i class="fa fa-eye"></i></button>
                <?php } ?>
              </td>
                <?php break;
                    case 'action': ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?> action">
                <div class="btn-group">
                  <?php foreach ($q['action'] as $action) { ?>
                    <?php if ($action['url']) { ?>
                  <a href="<?php echo $action['url']; ?>" id="<?php echo $action['name'] . "-" . $q['qa_id']; ?>" class="btn btn-default btn-sm btn-icon <?php echo $action['name']; ?>"><?php if ($action['icon']) {?><i class="fa fa-<?php echo $action['icon']; ?>"></i><?php } ?><?php echo $action['text']; ?></a>
                    <?php } else { ?>
                  <button type="button" id="<?php echo $action['name'] . "-" . $q['qa_id']; ?>" class="btn btn-default btn-xs <?php echo $action['name']; ?>"><?php if ($action['icon']) {?><i class="fa fa-<?php echo $action['icon']; ?>"></i><?php } ?><?php echo $action['text']; ?></button>
                    <?php } ?>
                  <?php } ?>
                </div>
              </td>
                <?php break;
                    default: ?>
              <td class="<?php echo $attr['align']; ?> <?php echo $attr['class']; ?>"><?php echo $q[$column]; ?></td>
                <?php break;
                  }
                } ?>
            </tr>
              <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="text-center" colspan="<?php echo count($columns); ?>"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </fieldset>
    </form>
    <?php echo $pagination; ?>
  </div>

</div>
<script type="text/javascript"><!--
(function(bull5i,$,undefined){
bull5i.texts=$.extend({},bull5i.texts,{error_ajax_request:"<?php echo addslashes($error_ajax_request); ?>"}),$("body").tooltip({selector:"[rel=tooltip]",container:"body"});var filter=function(){var t="index.php?route=catalog/qa",e=$("#nav-filter").find("input,select").filter(function(){return $.trim(this.value).length>0}).serialize(),o=$("tr.filters .fltr:input").filter(function(){return $.trim(this.value).length>0}).serialize(),r="sort=<?php echo $sort; ?>",n="order=<?php echo $order; ?>",i="token=<?php echo $token; ?>",l=$.grep([t,e,o,r,n,i],function(t){return $.trim(t).length>0}).join("&");location=l},update_nav_buttons=function(){$("input[name*='selected']:checked").length?($("#btn-delete").prop("disabled",!1),$("#btn-copy").prop("disabled",!1)):($("#btn-delete").prop("disabled",!0),$("#btn-copy").prop("disabled",!0))};$("body").on("click","[rel=popover]",function(){var t=$(this).data("popover-group"),e=$(this).data("bs.popover");$('[data-popover-group="'+t+'"]').not(this).popover("destroy"),e?$(".popover").not(e.$tip).remove():$(".popover").remove(),$(this).popover("toggle")}).on("click",function(t){var e=$(t.target).is("[rel=popover]")||$(t.target).parent().is("[rel=popover]"),o=$(t.target).closest(".popover").length>0;e||o||($("[rel=popover]").popover("destroy"),$(".popover").remove())}).on("click","#clear-global-search-btn",function(){$("#global-search").val(""),filter()}).on("click","#global-search-btn,#filter",function(){filter()}).on("change","#selectLanguage",function(){filter()}).on("click","#clear-filter",function(){$("tr.filters .fltr:input").val(""),filter()}).on("keypress","#global-search,tr.filters .fltr:input",function(t){13==t.which&&filter()}).on("change","input[name*='selected']",function(){update_nav_buttons()}).on("click","#btn-delete",function(){var t=$(this).attr("data-action"),e=!1;t&&($("#confirmDelete").length?($("#confirmDelete").modal("show"),$("#confirmDelete button.delete").click(function(){$("#confirmDelete").modal("hide"),e=!0}),$("#confirmDelete").on("hidden.bs.modal",function(){e&&$("#beForm").attr("action",t).submit()})):$("#beForm").attr("action",t).submit())}).on("click","#btn-copy",function(){var t=$(this).attr("data-action");$("#beForm").attr("action",t).submit()}).on("click","#btn-insert",function(){var t=$(this).attr("data-action");t&&(window.location=t)}).on("click","#master-selector",function(){$("input[name*='selected']").prop("checked",this.checked).trigger("change")});
<?php foreach ($typeahead as $column => $attr) { switch ($column) { case 'product': ?>$('.<?php echo $column; ?>.typeahead').typeahead({name:'<?php echo $column; ?>',<?php if (isset($attr['prefetch'])) { ?>prefetch:'<?php echo $attr['prefetch']; ?>',<?php } else if (isset($attr['remote'])) { ?>remote:'<?php echo $attr['remote']; ?>',<?php } ?>limit:10,template:['<p><span style="white-space:nowrap;">{{value}}</span></p>'].join(''),engine:Hogan});<?php break; default: break; } } ?>
$(".typeahead.input-sm").length&&$(".typeahead.input-sm").siblings("input.tt-hint").addClass("hint-small");
}(window.bull5i=window.bull5i||{},jQuery));
//--></script>
<?php echo $footer; ?>
