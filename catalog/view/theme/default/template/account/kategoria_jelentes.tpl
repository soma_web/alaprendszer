<?php echo $header; ?>


    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>


    <div class="box">


        <div class="account-heading">
            <h1><img src="catalog/view/theme/default/image/product.png" alt="" /> <?php echo $heading_title_katagoria; ?></h1>
            <div class="nyomtat">
                <a onclick="JelentesNyomtat();" class="button"><?php echo $button_nyomtatas; ?></a>
                <a href='<?php echo $csv_be; ?>' class="button"><?php echo $button_exportalas; ?></a>
            </div>
        </div>

        <div style="margin: 10px 0 10px 0">
            <span><?php echo $entry_date_start; ?></span>
            <span><input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="12" /></span>
            <span><?php echo $entry_date_end; ?></span>
            <span><input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="12" /></span>
            <a onclick="filter();" class="button"><?php echo $button_filter; ?></a>
        </div>

        <div class="">
            <table class="list">


                <thead>
                <tr>
                    <td class="left"> <?php echo $column_category_name; ?></td>
                    <td class="right"><?php echo $column_hirdetok; ?></td>
                    <td class="right"><?php echo $column_lattak; ?></td>
                    <td class="right"><?php echo $column_vasaroltak; ?></td>
                    <!--td class="right"><?php echo $column_vasaroltak_nalam; ?></td-->
                    <td class="right"><?php echo $column_atlag; ?></td>
                    <td class="right"><?php echo $column_atlag_szazalek; ?></td>
                </tr>
                </thead>
                <tbody>
                <?php if (isset($sectors) && $sectors) { ?>
                    <?php foreach ($sectors as $sector) { ?>
                        <tr>
                            <td class="left"> <?php echo $sector['category']; ?></td>
                            <td class="right"><?php echo $sector['hirdetok']; ?></td>
                            <td class="right"><?php echo $sector['lattak']; ?></td>
                            <td class="right"><?php echo $sector['vasaroltak']; ?></td>
                            <!--td class="right"><?php echo $sector['vasaroltak_nalam']; ?></td-->
                            <td class="right"><?php echo $sector['atlag']; ?></td>
                            <td class="right"><?php echo $sector['atlag_szazalek']; ?></td>
                        </tr>

                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
    </div>

</div>


    <script type="text/javascript"><!--
        function filter() {
//            url = 'index.php?route=account/kategoria_jelentes&token=<?php echo $token; ?>';
            url = 'index.php?route=account/kategoria_jelentes';

            var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');

            if (filter_date_start) {
                url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
            }

            var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');

            if (filter_date_end) {
                url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
            }



            location = url;
        }
        //--></script>
    <script type="text/javascript"><!--
        $(document).ready(function() {
            $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});

            $('#date-end').datepicker({dateFormat: 'yy-mm-dd'});
        });



        //--></script>


<?php echo $footer; ?>