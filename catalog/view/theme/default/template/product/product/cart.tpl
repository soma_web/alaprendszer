<?php if ($elorendeles || $mutat_arat ) { ?>

<div class="cart">
    <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />

    <?php if(!isset($_SESSION['ar_atszamol']) || (isset($_SESSION['ar_atszamol']) && !$_SESSION['ar_atszamol']) ) { ?>
        <?php if (!$elorendeles){ ?>
            <?php if(!$megrendelem) { ?>

                <?php if ($csomagolasi_mennyiseg > 1 && $this->config->get('megjelenit_csomagolas_admin')) { ?>
                    <input type="hidden" name="quantity2" />
                    <span style="padding-left: 5px;"><?php echo ' x '.$csomagolasi_mennyiseg.' '. $megyseg. (!empty($csomagolasi_egyseg) ? '/'.$csomagolasi_egyseg : '')?></span>
                    <span class="atmenet-hatter">
                                  <input type="button" class="kosar button" value="<?php echo $button_cart; ?>"
                                         onclick="addToCart('<?php echo $product_id; ?>',
                                             $('#product-quantity_<?php echo $product_id; ?>').val()*<?php echo $csomagolasi_mennyiseg?>,
                                             '<?php echo $this->config->get('config_template')?>',
                                             '<?php echo $module_name.$product_id?>',
                                             true)" />
                              </span>
                <?php } else { ?>
                    <?php $csomagolasi_mennyiseg = 1 ?>
                    <span class="atmenet-hatter">
                        <input type="button" class="kosar button" value="<?php echo $button_cart; ?>"
                                 onclick="addToCart('<?php echo $product_id; ?>',
                                     $('#product-quantity_<?php echo $product_id; ?>').val()*<? echo $csomagolasi_mennyiseg?>,
                                     '<?php echo $this->config->get('config_template')?>',
                                     '<?php echo $module_name.$product_id?>',
                                     true)" />
                    </span>
                <?php } ?>
                <?php if ($minimum > 1) { ?>
                    <div class="minimum"><?php echo $text_minimum; ?></div>
                <?php } ?>


            <?php } else { ?>

                <input type="button" value="<?php echo $button_megrendelem; ?>" class="button"
                       onclick="megrendelem('<?php echo $product_id; ?>',1,'<?php echo $this->config->get('config_template')?>',
                           '<?php echo $module_name.$product_id?>',true)"/>
            <?php } ?>


        <?php } else { ?>
            <input type="button" class="kosar button elorendeles" value="<?php echo $button_arajanlat; ?>"
                   onclick="arajanlatotKerek('<?php echo $product_id; ?>')"/>
        <?php } ?>


    <?php } else { ?>
        <input type="button" class="kosar button elorendeles" value="<?php echo $button_set_valos_ar; ?>"
               onclick="ujratolt(0)"/>
    <?php }  ?>
</div>
<?php } else { ?>
    <div class="cart">
        <input type="button" class="kosar button" value="<?php echo $text_login; ?>"
               onclick="location='index.php?route=account/login'">
    </div>
<?php } ?>