<?php
// Heading
$_['heading_title']  = 'Jelentés a megtekintett termékekről';

// Text
$_['text_success']   = 'Sikeres lenullázás!';

// Column
$_['column_name']    = 'Termék neve';
$_['column_model']   = 'Típus';
$_['column_viewed']  = 'Megtekintve';
$_['column_percent'] = 'Százalék';
?>