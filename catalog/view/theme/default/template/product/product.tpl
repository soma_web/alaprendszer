<?php echo $header; ?>

<?php if (isset($this->request->request['warning']) && $this->request->request['warning']) { ?>
    <script>
        var html = '<div class="warning"><?php echo $this->request->request['warning']; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>';
        $('#notification').html(html);
        notaficationBezar();
    </script>
    <?php unset($this->request->request['warning']); ?>

<?php } ?>

<?php
function selfURL() {
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]),0,strpos($_SERVER["SERVER_PROTOCOL"],'/')).$s;
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
    return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
}

function strleft($s1, $s2) {
    return substr($s1, 0, strpos($s1, $s2));
}
?>



<? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/mennyisegi.png')) {
    $mennyisegikep = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/mennyisegi.png';
} else {
    $mennyisegikep = DIR_TEMPLATE_IMAGE.'default/image/mennyisegi.png';
} ?>

    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>


<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>


<?php if (!isset($megjelenit_termek['modularis']) || ($megjelenit_termek['modularis'] == 0)) { ?>

    <?php $megjelenit_product = $this->config->get('megjelenit_product');?>


    <?php if($this->config->get('megjelenit_product_nev_fejlec') == 1) {?>
        <h1 class="product_title">
            <?php if (isset($megjelenit_product['product_model_termeknevben']) && $megjelenit_product['product_model_termeknevben']) { ?>

                <?php if (isset($model)) { ?>
                    <span class="model"><?php echo $model;?></span> <?php echo " - ";?>
                <?php }?>

            <?php }?>

            <?php if (isset($megjelenit_product['product_cikkszam_termeknevben']) && $megjelenit_product['product_cikkszam_termeknevben']) { ?>

                <?php if (isset($cikkszam)) { ?>
                    <span class="model"><?php echo $cikkszam;?></span> <?php echo " - ";?>
                <?php }?>

            <?php }?>

            <?php if (isset($megjelenit_product['product_gyarto_termeknevben']) && $megjelenit_product['product_gyarto_termeknevben']) { ?>

                <?php if (isset($manufacturer)) { ?>
                    <span class="manufacturer"><?php echo $manufacturer;?></span> <?php echo " - ";?>
                <?php }?>

            <?php }?>
            <?php echo $heading_title; ?>
        </h1>
    <?php }?>


    <?php if ($megjelenit_product['product_uj_ikon'] || $megjelenit_product['product_par_ikon'] || $megjelenit_product['product_szazalek_ikon'] || $megjelenit_product['product_mennyisegi_ikon']) { ?>
        <div class="product_icons">
            <?php if ($uj) { ?>
                <?php if ($megjelenit_product['product_uj_ikon']) { ?>
                    <img class="icon_meret" title="<? if (isset($text_uj_title)) { echo $text_uj_title; }?>" src="<? echo $ujtermek?>" >
                <?php } ?>
            <?php } ?>
            <?php if ($pardarab) { ?>
                <?php if ($megjelenit_product['product_par_ikon']) { ?>
                    <img class="icon_meret" title="<? if (isset($text_pardarab_title)) { echo $text_pardarab_title; }?>" src="<? echo $pardarabkep?>" >
                <?php } ?>
            <?php } ?>
            <?php if ($special) { ?>
                <?php if ($megjelenit_product['product_szazalek_ikon']) { ?>
                    <img class="icon_meret" title="<? if (isset($text_akcio_title)) { echo $text_akcio_title; }?>" src="<? echo $sale?>" >
                <?php } ?>
            <?php } ?>
            <?php if ($mennyisegi) { ?>
                <?php if ($megjelenit_product['product_mennyisegi_ikon']) { ?>
                    <img class="icon_meret"  src="<? echo $mennyisegikep?>" >
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>

    <? if (!$logged) { ?>
        <?php if($this->config->get('megjelenit_product_jelentkezzen') == 1) {?>
            <div class=product-bejelentkezes ><? echo $text_bejelentkezes; ?>&nbsp
                <a class="button" href="<? echo $bejelentkezes?>"><? echo $text_bejelentkezes_button; ?></a>
            </div>
        <? } ?>
    <? } ?>

    <?php if (isset($megjelenit_product['tab_description']) && $megjelenit_product['tab_description'] != 1 ) { ?>
        <div class="leirassal">
    <?php } ?>

    <div class="product-info">
    <div class="info-div">
    <?php if ($thumb || $images) { ?>
        <div class="left" id = "product_left">
            <?php if ($thumb) { ?>
                <div class="image">

                    <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="fancy" ref="fancy-pics" data-fancybox-group="gallery-1" >
                        <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="<?php echo $module_name.$product_id  ?>" />
                    </a>
                </div>
            <?php } ?>
            <?php if ($images && $megjelenit_product['tab_image'] != 1) { ?>
                <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product/product_info_bal/kiskepek.tpl')) {
                    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/product/product/product_info_bal/kiskepek.tpl';
                } else {
                    $arkiir = DIR_TEMPLATE.'default/template/product/product/product_info_bal/kiskepek.tpl';
                } ?>
                <?php require_once($arkiir); ?>
            <?php } ?>
        </div>
    <?php } ?>
    <div class="right" id = "product_right">
    <div class="bal">
    <div class="description">
        <table>
            <?php if ($this->config->get('megjelenit_product_megnevezes') == 1 ) { ?>
                <tr>
                    <td colspan="2"><span class="poruduct_neve" ><?php echo $heading_title; ?></span></td>
                </tr>
            <?php } if ($megjelenit_product['product_leiras']) { ?>
                <tr>
                    <td colspan="2"><span class="description_top" ><?php echo $short_description; ?></span></td>
                </tr>
            <?php } ?>
            <?php foreach($informaciok as $value) {?>
                <tr>
                    <?php if (count($value['value']) == 1 && !$value['group_filter']) {?>
                        <td><span><?php echo $value['text_name']; ?></span></td>
                        <?php if (gettype($value['value']) == "array") { ?>
                            <?php if (count($value['value']) > 0) { ?>
                                <?php foreach ($value['value'] as $tomb){ ?>
                                    <td> <?php echo $tomb; ?></td>
                                <?php } ?>
                            <?php } ?>
                        <?php } else { ?>
                            <td> <?php echo $value['value']; ?></td>
                        <?php } ?>
                    <?php } elseif (!$value['group_filter']) {?>
                        <td style="display: table-row-group;"><span><?php echo $value['text_name']; ?></span></td>

                        <td>
                            <table>
                                <?php foreach($value['value'] as $megnevezes) { ?>
                                    <tr>
                                        <td> <?php echo $megnevezes; ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </td>




                    <?php } elseif ($value['group_filter']) {?>
                        <td style="display: table-row-group;"><?php echo $value['text_name']; ?></td>
                        <td>
                            <table class="form">
                                <?php foreach($value['value'] as $csoport) { ?>
                                    <tr>
                                        <td style="display: table-row-group;"><?php echo $csoport['group']; ?><td>


                                            <table class="form">
                                                <?php foreach($csoport['filter'] as $filter) { ?>
                                                    <tr>
                                                        <td> <?php echo $filter['filter_name']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>

                                    </tr>
                                <?php } ?>
                            </table>
                        </td>

                    <?php }?>




                </tr>
            <?php }?>
        </table>
    </div>

    <?php if($this->config->get('megjelenit_product_brutto_netto') == 1) {?>
        <?php if ($price) {    //eredeti  ?>
            <?php if ($utalvany == 1) { ?>
                <?php $class="utalvany";?>
            <?php } else {?>
                <?php $class="";?>
            <?php } ?>
            <div class="price <?php echo $class?>">
                <?php if (!$special && $this->config->get('megjelenit_product_brutto_ar') == 1) { ?>
                    <div class=price_egyedul><?php echo $price; ?></div>
                <?php } elseif ($this->config->get('megjelenit_product_brutto_ar') == 1) { ?>
                    <div class="price_ujarral">
                        <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
                    </div>
                <?php } ?>

                <?php if ($price_netto && $this->config->get('megjelenit_product_netto_ar') == 1 && $price != $price_netto ) { ?>

                    <?php if ($this->config->get('megjelenit_product_brutto_ar') == 0) { ?>
                        <?php if (!$special) {?>
                            <span class="price_egyedul"><?php echo $price_netto; ?></span>
                            <span class="puszafa">+ ÁFA</span>
                        <?php } else {?>
                            <div class="price_ujarral">
                                <span class="price-old"><?php echo $price_netto; ?></span> <span class="price-new"><?php echo $special_netto; ?></span>
                                <span class="puszafa">+ ÁFA</span>
                            </div>
                        <?php }?>
                    <?php } else {?>
                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span>
                    <?php } ?>

                <?php } elseif ($price_netto && $this->config->get('megjelenit_product_netto_ar') == 1){?>
                    <?php if ($this->config->get('megjelenit_product_brutto_ar') == 0) { ?>
                        <?php if (!$special) { ?>
                            <div class=price_egyedul><?php echo $price; ?></div>
                        <?php } else {?>
                            <div class="price_ujarral">
                                <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
                            </div>
                        <?php }?>
                    <?php }?>
                <?php }?>

                <?php if ($points) { ?>
                    <span class="reward"><small><?php echo $text_points; ?> <?php echo $points; ?></small></span><br />
                <?php } ?>
                <?php if ($discounts) { ?>
                    <br />
                    <div class="discount">
                        <table>
                            <?php foreach ($discounts as $discount) { ?>
                                <tr>
                                    <?php if ($this->config->get('megjelenit_product_brutto_ar') == 1) { ?>
                                        <td><?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?></td>
                                        <?php if ($this->config->get('megjelenit_product_netto_ar') == 1 && $discount['price'] != $discount['price_netto']) { ?>
                                            <td><span class="price-tax" style="padding-left: 10px;"><?php echo $text_tax;  echo " "; echo $discount['price_netto'] ?></span></td>
                                        <?php } ?>

                                    <?php } elseif ($this->config->get('megjelenit_product_netto_ar') == 1 && $discount['price'] != $discount['price_netto']) {?>
                                        <td><?php echo sprintf($text_discount, $discount['quantity'], $discount['price_netto']); ?><span class="puszafa">+ ÁFA</span></td>

                                    <?php } elseif ($this->config->get('megjelenit_product_netto_ar') == 1 && $discount['price'] == $discount['price_netto']) {?>
                                        <td><?php echo sprintf($text_discount, $discount['quantity'], $discount['price_netto']); ?></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>



    <?php } else {?>
        <?php if ($price) {    //eredeti  ?>

            <?php
            $class="";
            $class_szazalek="";
            if ($utalvany == 1) {
                $class="utalvany";
            }
            if ($szazalek == 1) {
                $class_szazalek="szazalekos_ar";
            }
            ?>


            <div class="price <?php echo $class." ".$class_szazalek?>">
                <?php if (!$special && $this->config->get('megjelenit_product_netto_ar') == 1) { ?>
                    <span class=price_egyedul><?php echo $price_netto; ?>
                        <?php if ($price_netto != $price) { ?>
                            <span class="puszafa">+ ÁFA</span>
                        <?php }?>
                              </span>
                <?php } elseif ($this->config->get('megjelenit_product_netto_ar') == 1) { ?>
                    <div class="price_ujarral">
                        <span class="price-old"><?php echo $price_netto; ?></span> <span class="price-new"><?php echo $special_netto; ?></span>
                        <?php if ($price_netto != $price) { ?>
                            <span class="puszafa">+ ÁFA</span>
                        <?php }?>
                    </div>
                <?php } ?>

                <?php if ($price_netto && $this->config->get('megjelenit_product_brutto_ar') == 1 && $price != $price_netto ) { ?>

                    <?php if ($this->config->get('megjelenit_product_netto_ar') == 0) { ?>
                        <?php if (!$special) {?>
                            <span class="price_egyedul"><?php echo $price; ?></span>
                        <?php } else {?>
                            <div class="price_ujarral">
                                <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
                            </div>
                        <?php }?>
                    <?php } else {?>
                        <span class="price-tax"><?php echo "Brutto: "; ?> <?php echo $price; ?></span>
                    <?php } ?>

                <?php } elseif ($price_netto && $this->config->get('megjelenit_product_brutto_ar') == 1){?>
                    <?php if ($this->config->get('megjelenit_product_netto_ar') == 0) { ?>
                        <?php if (!$special) { ?>
                            <div class=price_egyedul><?php echo $price; ?></div>
                        <?php } else {?>
                            <div class="price_ujarral">
                                <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
                            </div>
                        <?php }?>
                    <?php }?>
                <?php }?>

                <?php if ($points) { ?>
                    <span class="reward"><small><?php echo $text_points; ?> <?php echo $points; ?></small></span><br />
                <?php } ?>
                <?php if ($discounts) { ?>
                    <br />
                    <div class="discount">
                        <table>
                            <?php foreach ($discounts as $discount) { ?>
                                <tr>
                                    <?php if ($this->config->get('megjelenit_product_netto_ar') == 1) { ?>
                                        <td><?php echo sprintf($text_discount, $discount['quantity'], $discount['price_netto']); ?>
                                            <?php foreach($discount['tax_name'] as $tax_name) { ?>
                                                <?php echo "+".$tax_name?>
                                            <?php } ?>
                                        </td>

                                        <?php if ($this->config->get('megjelenit_product_brutto_ar') == 1 && $discount['price'] != $discount['price_netto']) { ?>
                                            <td><span class="price-tax1" style="padding-left: 10px;">(<?php echo "Brutto: ";  echo $discount['price'] ?>)</span></td>
                                        <?php } elseif($discount['price'] != $discount['price_netto']) { ?>
                                            <td><span class="puszafa">+ ÁFA</span></td>
                                        <?php }?>

                                    <?php } elseif ($this->config->get('megjelenit_product_brutto_ar') == 1) {?>
                                        <td><?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?></td>
                                    <?php } ?>

                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if ($price) { ?>
        <div class="arhoz_megjegyzesek">
            <?php if ($magyarazat1) { ?>
                <span><?php echo $magyarazat1?></span>
            <?php } ?>
        </div>
    <?php } ?>


    <?php if ($options) { ?>
        <div class="options">
            <h2><?php echo $text_option; ?></h2>
            <br />
            <?php foreach ($options as $option) { ?>
                <?php if ($option['type'] == 'select') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <select name="option[<?php echo $option['product_option_id']; ?>]">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($option['option_value'] as $option_value) { ?>
                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                    <?php if ($option_value['price']) { ?>
                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                    <?php } ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <br />
                <?php } ?>
                <?php if ($option['type'] == 'radio') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <?php foreach ($option['option_value'] as $option_value) { ?>
                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                            <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                <?php if ($option_value['price']) { ?>
                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                <?php } ?>
                            </label>
                            <br />
                        <?php } ?>
                    </div>
                    <br />
                <?php } ?>
                <?php if ($option['type'] == 'checkbox') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <?php foreach ($option['option_value'] as $option_value) { ?>
                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                            <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                <?php if ($option_value['price']) { ?>
                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                <?php } ?>
                            </label>
                            <br />
                        <?php } ?>
                    </div>
                    <br />
                <?php } ?>
                <?php if ($option['type'] == 'image') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <table class="option-image">
                            <?php foreach ($option['option_value'] as $option_value) { ?>
                                <tr>
                                    <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
                                    <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
                                    <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                        </label></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <br />
                <?php } ?>
                <?php if ($option['type'] == 'text') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
                    </div>
                    <br />
                <?php } ?>
                <?php if ($option['type'] == 'textarea') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
                    </div>
                    <br />
                <?php } ?>
                <?php if ($option['type'] == 'file') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <a id="button-option-<?php echo $option['product_option_id']; ?>" class="button"><?php echo $button_upload; ?></a>
                        <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
                    </div>
                    <br />
                <?php } ?>
                <?php if ($option['type'] == 'date') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
                    </div>
                    <br />
                <?php } ?>
                <?php if ($option['type'] == 'datetime') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
                    </div>
                    <br />
                <?php } ?>
                <?php if ($option['type'] == 'time') { ?>
                    <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                            <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
                    </div>
                    <br />
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>

    <?php if ($this->config->get('megjelenit_product_social') == 1) { ?>
        <div class="review">
                    <span class="share"><!-- AddThis Button BEGIN -->
                        <span class="addthis_default_style">
                            <a class="addthis_button_email"> <a class="addthis_button_facebook"></a> <a class="addthis_button_googleplus"></a>
                        </span>
                        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53675cea59952f34"></script>

                        <!-- AddThis Button END -->
                        <?php $valami = selfURL();

                        ?>
                        <div class="fb-like" data-href="<?php echo $valami; ?>" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>

                    </span>
        </div>

    <?php } ?>
    <div class="cart">
        <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
        <?php if ($this->config->get('megjelenit_product_quantity') == 1) { ?>
            <input class="mennyit_vasarol" type="text"  id="product-quantity_<?php echo $product_id; ?>" name="quantity" size="2" value="<?php echo $minimum; ?>" />
        <?php } else { ?>
            <input class="mennyit_vasarol" type="hidden"  id="product-quantity_<?php echo $product_id; ?>" name="quantity" size="2" value="<?php echo $minimum; ?>" />
        <?php } ?>
        <?if ($csomagolasi_mennyiseg > 1){ ?>
            <input type="hidden" name="quantity2" />
            <span><?echo 'x '.$csomagolasi_mennyiseg.' '. $megyseg.'/'.$csomagolasi_egyseg?></span>
            <span class="atmenet-hatter">
                          <input type="button" class="kosar button" value="<?php echo $button_cart; ?>"
                                 onclick="addToCart('<?php echo $product_id; ?>',
                                     $('#product-quantity_<?php echo $product_id; ?>').val()*<? echo $csomagolasi_mennyiseg?>,
                                     '<?php echo $this->config->get('config_template')?>',
                                     '<?php echo $module_name.$product_id?>',
                                     true)" />
                      </span>
        <?} else { $csomagolasi_mennyiseg = 1 ?>
            <span class="atmenet-hatter">
                      <input type="button" class="kosar button" value="<?php echo $button_cart; ?>"
                             onclick="addToCart('<?php echo $product_id; ?>',
                                 $('#product-quantity_<?php echo $product_id; ?>').val()*<? echo $csomagolasi_mennyiseg?>,
                                 '<?php echo $this->config->get('config_template')?>',
                                 '<?php echo $module_name.$product_id?>',
                                 true)" />
                  </span>
        <?php }?>
        <?php if ($minimum > 1) { ?>
            <div class="minimum"><?php echo $text_minimum; ?></div>
        <?php } ?>
        <?php if ($this->config->get('megjelenit_kosar_kivansag') == 1 ) {?>
            <span class="or">
                      <?php echo $text_or; ?>
                  </span>
            <span class="compare">
                    <a class="hasonlit" onclick="addToCompare('<?php echo $product_id; ?>');">

                    </a>
                </span>
            <span class="wishlist">
                    <a class="kivansag" onclick="addToWishList('<?php echo $product_id; ?>');">

                    </a>
                </span>
        <?php } ?>
    </div>


    <?php if ($megjelenit_product['product_velemeny_megosztas_doboz']) { ?>
        <?php if ($review_status) { ?>
            <div class="review">
                <div><img src="catalog/view/theme/default/image/stars-<?php echo $rating; ?>.png" alt="<?php echo $reviews; ?>" />&nbsp;&nbsp;<a onclick="$('a[href=\'#tab-review\']').trigger('click'); $('html,body').animate({scrollTop: $('#tab-review').offset().top - $('#tabs').height()},'slow');"><?php echo $reviews; ?></a><a onclick="$('a[href=\'#tab-review\']').trigger('click'); $('html,body').animate({scrollTop: $('#tab-review').offset().top - $('#tabs').height()},'slow');"><?php echo $text_write; ?></a></div>
                <div class="share"><!-- AddThis Button BEGIN -->
                    <div class="addthis_default_style"><a class="addthis_button_compact"><?php echo $text_share; ?></a> <a class="addthis_button_email"></a><a class="addthis_button_print"></a> <a class="addthis_button_facebook"></a> <a class="addthis_button_twitter"></a></div>
                    <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js"></script>
                    <!-- AddThis Button END -->
                </div>
            </div>
        <?php } ?>
    <?php } ?>
    </div>

    <?php if ($this->config->get('megjelenit_product_jobb_jobb_elso') == 1 || $this->config->get('megjelenit_product_jobb_jobb_masodik') == 1 || $this->config->get('megjelenit_product_jobb_jobb_harmadik') == 1) { ?>
        <div class="jobb">
            <?php if ($this->config->get('megjelenit_product_jobb_jobb_elso') == 1 ) {?>
                <span class="wishlist">
                <a class="kivansag" onclick="addToWishList('<?php echo $product_id; ?>');">
                    <?php echo $button_wishlist; ?>
                </a>
            </span>

                <span class="compare">
                <a class="hasonlit" onclick="addToCompare('<?php echo $product_id; ?>');">
                    <?php echo $button_compare; ?>
                </a>
            </span>
            <?php } ?>
            <?php if ($this->config->get('megjelenit_product_jobb_jobb_masodik') == 1 ) {?>
                <!--<div class="separator">-->
                <br>
                <div class="fb-like" data-send="false" data-layout="button_count" data-width="130" data-show-faces="true"></div>
                <br>
                <a href="https://twitter.com/share" class="twitter-share-button" data-via="kpanyik" data-count="none">Tweet</a>
                <script>!function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (!d.getElementById(id)) {
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//platform.twitter.com/widgets.js";
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                    }(document, "script", "twitter-wjs");
                </script>
            <?php } ?>
            <?php if ($this->config->get('megjelenit_product_jobb_jobb_harmadik') == 1 ) {?>
                <div style="margin-top: 7px;">
                    <a href="javascript:window.print()" class="xbutton small">Print
                        <span class="ico-print"></span>
                    </a>
                    <!--<a href="<?php echo $email; ?>" class="xbutton small">Email
                            <span class="ico-emailshare"></span>
                        </a>-->
                </div>
            <?php } ?>
        </div>
    <?php } ?>
    </div>
    </div>
    </div>

    <?php if (isset($megjelenit_product['tab_description']) && $megjelenit_product['tab_description'] != 1 ) { ?>
        <div class="leiras"><?php echo $description; ?></div>
    <?php }?>




    <?php if ($megjelenit_product['product_koztes'] && false) { ?>

        <div class="termek_koztes">

            <?php if ($megjelenit_product['product_garancia'] || $megjelenit_product['product_szallitas'] || $megjelenit_product['product_velemeny'] ) { ?>
                <div class="koztes-bal">
                    <?php if ($megjelenit_product['product_garancia']) { ?>
                        <div class="garancia">
                            <span><?php echo $product_id;?></span> <span><?php echo $text_garancia ?></span>
                        </div>
                    <?php } ?>
                    <?php if ($megjelenit_product['product_szallitas']) { ?>
                        <div class="szallitas">
                            <span><?php echo $text_szallitas  ;?></span> <span><?php echo $szallitasi_koltseg ?></span>
                        </div>
                    <?php } ?>
                    <?php if ($megjelenit_product['product_velemeny']) { ?>
                        <div class="velemeny">
                            <div>
                                <img src="catalog/view/theme/default/image/stars-<?php echo $rating; ?>.png" alt="<?php echo $reviews; ?>" />&nbsp;&nbsp;
                                <a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $reviews; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $text_write; ?></a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if ($megjelenit_product['product_osszehasonlit_kivansag'] || $megjelenit_product['product_addthis']) { ?>
                <div class="koztes-jobb">
                    <?php if ($megjelenit_product['product_osszehasonlit_kivansag']) { ?>
                        <div class="osszehasonlit_kivansag">
                            <span class="wishlist">
                                <a class="kivansag" onclick="addToWishList('<?php echo $product_id; ?>');">
                                    <?php echo $button_wishlist; ?>
                                </a>
                            </span>

                            <span class="compare">
                                <a class="hasonlit" onclick="addToCompare('<?php echo $product_id; ?>');">
                                    <?php echo $button_compare; ?>
                                </a>
                            </span>
                        </div>
                    <?php } ?>
                    <?php if ($megjelenit_product['product_addthis']) { ?>
                        <div class="addthis">
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_sharing_toolbox"></div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53aa6db534fb9330"></script>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

        </div>





    <?php } ?>

    <?php $termek_tabok = $this->config->get('megjelenit_termek_tabok'); ?>

    <?php if ($termek_tabok) {?>
        <?php $oszlop = -1;?>
        <?php $sor = -1;?>
        <?php $nyitott = false;?>
        <?php $nyitott_sor = false;?>

        <?php foreach($termek_tabok as $key=>$beallitas_termek_aloldal) {?>
            <?php if ($beallitas_termek_aloldal['status'] == 1 ) {?>

                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product/'. $key .'.tpl')) {
                    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/product/product/'.$key . '.tpl';
                } else {
                    $arkiir = DIR_TEMPLATE.'default/template/product/product/'.$key.'.tpl';
                } ?>

                <?php if ($oszlop != $beallitas_termek_aloldal['sort_order_sor']) { ?>
                    <?php $sor = -1;?>
                    <?php $oszlop = $beallitas_termek_aloldal['sort_order_sor']?>
                    <?php if ($nyitott) {?>
                        <?php $nyitott = false;?>
                        </div>
                    <?php }?>
                    <?php if ($nyitott_sor) {?>
                        <?php $nyitott_sor = false;?>
                        </div>
                    <?php }?>

                    <div class="product-sor-<?php echo $beallitas_termek_aloldal['sort_order_sor']?>">

                    <?php $nyitott = true;?>
                <?php } ?>

                <?php if ($sor != $beallitas_termek_aloldal['sort_order_oszlop']) { ?>
                    <?php $sor = $beallitas_termek_aloldal['sort_order_oszlop']?>
                    <?php if ($nyitott_sor) {?>
                        <?php $nyitott_sor = false;?>
                        </div>
                    <?php } ?>

                    <div class="product-oszlop-<?php echo $beallitas_termek_aloldal['sort_order_oszlop']?>">
                    <?php $nyitott_sor = true;?>

                <?php } ?>

                <?php include($arkiir); ?>

            <?php } ?>

        <?php }?>

        <?php if ($nyitott) {?>
            </div>
        <?php }?>
        <?php if ($nyitott_sor) {?>
            </div>
        <?php }?>
    <?php }?>


    <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product/altalanos_tab.tpl')) {
        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/product/product/altalanos_tab.tpl';
    } else {
        $arkiir = DIR_TEMPLATE.'default/template/product/product/altalanos_tab.tpl';
    } ?>
    <?php //include($arkiir); ?>


    <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product/package.tpl')) {
        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/product/product/package.tpl';
    } else {
        $arkiir = DIR_TEMPLATE.'default/template/product/product/package.tpl';
    } ?>
    <?php //include($arkiir); ?>



    <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product/tartozekok_tab.tpl')) {
        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/product/product/tartozekok_tab.tpl';
    } else {
        $arkiir = DIR_TEMPLATE.'default/template/product/product/tartozekok_tab.tpl';
    } ?>
    <?php //include($arkiir); ?>



    <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product/termekek_tab.tpl')) {
        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/product/product/termekek_tab.tpl';
    } else {
        $arkiir = DIR_TEMPLATE.'default/template/product/product/termekek_tab.tpl';
    } ?>
    <?php //include($arkiir); ?>



    <?php if ($tags) { ?>
        <div class="tags"><b><?php echo $text_tags; ?></b>
            <?php foreach ($tags as $tag) { ?>
                <a href="<?php echo $tag['href']; ?>"><?php echo $tag['tag']; ?></a>,
            <?php } ?>
        </div>
    <?php } ?>

<?php } else {?>
    <?php
    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product_modularis.tpl')) {
        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/product/product_modularis.tpl';
        include($arkiir);

    } elseif (file_exists(DIR_TEMPLATE . 'default/template/product/product_modularis.tpl')) {
        $arkiir = DIR_TEMPLATE.'default/template/product/product_modularis.tpl';
        include($arkiir);

    } ?>
<?php } ?>



<?php echo $content_bottom; ?></div>


<?php if ($options) { ?>
    <script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
    <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'file') { ?>
            <script type="text/javascript"><!--
                new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
                    action: 'index.php?route=product/product/upload',
                    name: 'file',
                    autoSubmit: true,
                    responseType: 'json',
                    onSubmit: function(file, extension) {
                        $('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
                    },
                    onComplete: function(file, json) {
                        $('.error').remove();

                        if (json.success) {
                            $('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json.file);
                        }

                        if (json.error) {
                            $('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json.error + '</span>');
                        }

                        $('.loading').remove();
                    }
                });
                //--></script>
        <?php } ?>
    <?php } ?>
<?php } ?>
    <script type="text/javascript"><!--
        $('#review .pagination a').on('click', function() {
            $('#review').slideUp('slow');

            $('#review').load(this.href);

            $('#review').slideDown('slow');

            return false;
        });

        $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

        $('#button-review').bind('click', function() {
            $.ajax({
                url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
                type: 'post',
                dataType: 'json',
                data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
                beforeSend: function() {
                    $('.success, .warning').remove();
                    $('#button-review').attr('disabled', true);
                    $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
                },
                complete: function() {
                    $('#button-review').attr('disabled', false);
                    $('.attention').remove();
                },
                success: function(data) {

                    if (data.error) {
                        $('#review-title').after('<div class="warning">' + data.error + '</div>');
                    }

                    if (data.success) {
                        $('#review-title').after('<div class="success">' + data.success + '</div>');

                        $('input[name=\'name\']').val('');
                        $('textarea[name=\'text\']').val('');
                        $('input[name=\'rating\']:checked').attr('checked', '');
                        $('input[name=\'captcha\']').val('');
                    }
                }
            });
        });
        //--></script>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <script type="text/javascript"><!--


        function uzenet_kikapcsol(){
            $('#notification').slideUp(500);
        }
        //--></script>

    <!--<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>-->
    <script type="text/javascript"><!--





        /*$('.date').datepicker({dateFormat: 'yy-mm-dd'});
        $('.datetime').datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'h:m'
        });
        $('.time').timepicker({timeFormat: 'h:m'});*/

$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

        function magassag(){
            var left_szelesseg  = $("#product_left").width();
            var szulo_szelesseg = $("#product_right").parent().width();
            var kulonbseg = szulo_szelesseg -left_szelesseg - 30;

            if(kulonbseg>155)
                $("#product_right").width(kulonbseg);
            else
                $("#product_right").width(szulo_szelesseg);
        }

        /* oldal görgetése id-ra */
        $(".scroll").click(function(event){
            debugger;
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
        });

        $('#button-cart-product').bind('click', function () {
            if ("<?echo $csomagolasi_mennyiseg?>" > 1){
                var mennyiseg=$('.cart input[type=\'text\']').val()*<?echo $csomagolasi_mennyiseg?>;
                $('.cart input[name=\'quantity2\']').val(mennyiseg);
            }

            $.ajax({
                url:'index.php?route=checkout/cart/add',
                type:'post',
                data:$('.cart input[type=\'text\'], .cart input[type=\'hidden\'], .cart input[type=\'radio\']:checked, .cart input[type=\'checkbox\']:checked,' +
                    ' .cart select, .cart textarea, 		.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),

                dataType:'json',

                success:function (json) {
                    $('.success, .warning, .attention, .error').remove();

                    if (json['error']) {
                        if (json['error']['warning']) {
                            $('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                            $('.warning').fadeIn('slow');
                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                        }

                        if (json['error']['option']) {

                            for (i in json['error']['option']) {
                                $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
                            }
                        }
                    }

                    if (json['success']) {

                        $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                        $('.success').fadeIn('slow');

                        $('#cart-total').html(json['total']);

                        $('html, body').animate({ scrollTop:0 }, 'slow');

                        $("#slidecontent").load("index.php #slidecontent");
                    }
                }
            });
        });

        //--></script>
<?php echo $footer; ?>