
<div id="tab-backend-top">

    <div id="ttab" class="htabs">
        <a href="#ttab-altalanosadmin"><?php echo $tab_altalanosadmin; ?></a>
        <a href="#ttab-nomodul"><?php echo $tab_nomodul; ?></a>
    </div>


    <div id="ttab-altalanosadmin">

        <div id="fttab" class="htabs">
            <a href="#fttab-admin"><?php echo "Általlános"; ?></a>
            <a href="#fttab-product_lista"><?php echo "Termék lista"; ?></a>
            <a href="#fttab-termekful"><?php echo $tab_termekful; ?></a>
            <a href="#fttab-termektulajdonsag"><?php echo $tab_termektulajdonsag; ?></a>
            <a href="#fttab-rendeles"><?php echo $tab_rendeles; ?></a>
            <a href="#fttab-vevok"><?php echo $tab_vevok; ?></a>
            <a href="#fttab-export"><?php echo $tab_export; ?></a>
            <a href="#fttab-fizetes"><?php echo "Fizetési mód"; ?></a>
        </div>


        <table class="form" id="fttab-admin">
            <tr>
                <td title="megjelenit_admin_nyelvi_ellenorzes"><?php echo $entry_admin_nyelv_ellenorzes; ?></td>
                <td colspan="4"><?php if ($megjelenit_admin_nyelvi_ellenorzes) { ?>
                        <input type="radio" name="megjelenit_admin_nyelvi_ellenorzes" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_nyelvi_ellenorzes" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_nyelvi_ellenorzes" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_nyelvi_ellenorzes" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_ar_ellenorzes"><?php echo $entry_admin_ar_ellenorzes; ?></td>
                <td colspan="4"><?php if ($megjelenit_admin_ar_ellenorzes) { ?>
                        <input type="radio" name="megjelenit_admin_ar_ellenorzes" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_ar_ellenorzes" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_ar_ellenorzes" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_ar_ellenorzes" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_ketszintu_szuro_ellenorzes"><?php echo $entry_admin_ketszintu_szuro_ellenorzes; ?></td>
                <td colspan="4"><?php if ($megjelenit_admin_ketszintu_szuro_ellenorzes) { ?>
                        <input type="radio" name="megjelenit_admin_ketszintu_szuro_ellenorzes" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_ketszintu_szuro_ellenorzes" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_ketszintu_szuro_ellenorzes" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_ketszintu_szuro_ellenorzes" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_ervenyes_ig_ellenorzes"><?php echo $entry_admin_ervenyes_ig_ellenorzes; ?></td>
                <td colspan="4"><?php if ($megjelenit_admin_ervenyes_ig_ellenorzes) { ?>
                        <input type="radio" name="megjelenit_admin_ervenyes_ig_ellenorzes" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_ervenyes_ig_ellenorzes" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_ervenyes_ig_ellenorzes" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_ervenyes_ig_ellenorzes" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_korhinta"><?php echo $entry_gorgetosav; ?></td>
                <td colspan="4"><?php if ($megjelenit_korhinta) { ?>
                        <input type="radio" name="megjelenit_korhinta" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_korhinta" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_korhinta" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_korhinta" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>


            <tr>
                <td title="megjelenit_admin_altalanos[ketszintu_szuro_admin]" style="width: 244px;"><?php echo $entry_form_ketszintu_szuro; ?></td>
                <td style="width: 144px;">
                    <?php if ($ketszintu_szuro_admin['ketszintu_szuro_admin']) { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[ketszintu_szuro_admin]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[ketszintu_szuro_admin]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[ketszintu_szuro_admin]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[ketszintu_szuro_admin]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_altalanos[termek_elhelyezkedes]" style="width: 244px;"><?php echo $entry_form_termek_elhelyezkedes; ?></td>
                <td style="width: 144px;">
                    <?php if ($termek_elhelyezkedes['termek_elhelyezkedes']) { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[termek_elhelyezkedes]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[termek_elhelyezkedes]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[termek_elhelyezkedes]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[termek_elhelyezkedes]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_altalanos[termek_kiemeles_modositasa]" style="width: 244px;"><?php echo $entry_termek_kiemeles_modositasa; ?></td>
                <td style="width: 144px;">
                    <?php if ($termek_kiemeles_modositasa['termek_kiemeles_modositasa']) { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[termek_kiemeles_modositasa]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[termek_kiemeles_modositasa]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[termek_kiemeles_modositasa]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[termek_kiemeles_modositasa]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_altalanos[csv_export_import]" style="width: 244px;"><?php echo "CSV Export-Import engedélyezése"; ?></td>
                <td style="width: 144px;">
                    <?php if ($csv_export_import['csv_export_import']) { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[csv_export_import]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[csv_export_import]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[csv_export_import]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[csv_export_import]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_altalanos[fogalom_magyarazat]" style="width: 244px;"><?php echo "Fogalom magyarázat engedélyezése"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($fogalom_magyarazat['fogalom_magyarazat']) && $fogalom_magyarazat['fogalom_magyarazat']) { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[fogalom_magyarazat]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[fogalom_magyarazat]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[fogalom_magyarazat]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[fogalom_magyarazat]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_altalanos[felhasznalo_atiranyitas]" style="width: 244px;"><?php echo "Felhasználó átirányítás engedélyezése"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($felhasznalo_atiranyitas['felhasznalo_atiranyitas']) && $felhasznalo_atiranyitas['felhasznalo_atiranyitas']) { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[felhasznalo_atiranyitas]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[felhasznalo_atiranyitas]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_altalanos[felhasznalo_atiranyitas]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_altalanos[felhasznalo_atiranyitas]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
        </table>


        <table class="form" id="fttab-product_lista">
            <tr>
                <td title="megjelenit_list_admin_model"><?php echo $entry_admin_model; ?></td>
                <td><?php if ($megjelenit_list_admin_model) { ?>
                        <input type="radio" name="megjelenit_list_admin_model" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_model" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_model" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_model" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_list_admin_cikkszam"><?php echo $entry_admin_cikkszam; ?></td>
                <td><?php if ($megjelenit_list_admin_cikkszam) { ?>
                        <input type="radio" name="megjelenit_list_admin_cikkszam" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_cikkszam" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_cikkszam" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_cikkszam" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_list_admin_cikkszam2"><?php echo $entry_admin_cikkszam2; ?></td>
                <td><?php if ($megjelenit_list_admin_cikkszam2) { ?>
                        <input type="radio" name="megjelenit_list_admin_cikkszam2" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_cikkszam2" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_cikkszam2" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_cikkszam2" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_list_admin_modositas"><?php echo "Módosítás dátuma"; ?></td>
                <td><?php if ($megjelenit_list_admin_modositas) { ?>
                        <input type="radio" name="megjelenit_list_admin_modositas" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_modositas" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_modositas" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_modositas" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_list_admin_hozzaadas"><?php echo "Létrehozás dátuma"; ?></td>
                <td><?php if ($megjelenit_list_admin_hozzaadas) { ?>
                        <input type="radio" name="megjelenit_list_admin_hozzaadas" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_hozzaadas" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_hozzaadas" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_hozzaadas" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_list_admin_kifuto"><?php echo "Kifutó termékek"; ?></td>
                <td><?php if (isset($megjelenit_list_admin_kifuto) && $megjelenit_list_admin_kifuto) { ?>
                        <input type="radio" name="megjelenit_list_admin_kifuto" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_kifuto" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_kifuto" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_kifuto" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_list_admin_ujdonsag"><?php echo "Újdonság"; ?></td>
                <td><?php if (isset($megjelenit_list_admin_ujdonsag) && $megjelenit_list_admin_ujdonsag) { ?>
                        <input type="radio" name="megjelenit_list_admin_ujdonsag" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_ujdonsag" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_ujdonsag" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_ujdonsag" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_list_admin_extra_garancia"><?php echo "Extra garancia"; ?></td>
                <td><?php if (isset($megjelenit_list_admin_extra_garancia) && $megjelenit_list_admin_extra_garancia) { ?>
                        <input type="radio" name="megjelenit_list_admin_extra_garancia" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_extra_garancia" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_extra_garancia" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_extra_garancia" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_list_admin_elorendeles"><?php echo "Csak rendelhető termékek"; ?></td>
                <td><?php if (isset($megjelenit_list_admin_elorendeles) && $megjelenit_list_admin_elorendeles) { ?>
                        <input type="radio" name="megjelenit_list_admin_elorendeles" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_elorendeles" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_elorendeles" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_elorendeles" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_list_admin_ar_tiltasa"><?php echo "Ár megjelenítés tiltása"; ?></td>
                <td><?php if (!empty($megjelenit_list_admin_ar_tiltasa)) { ?>
                        <input type="radio" name="megjelenit_list_admin_ar_tiltasa" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_ar_tiltasa" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_ar_tiltasa" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_ar_tiltasa" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>


            <tr>
                <td title="megjelenit_list_admin_megrendelem"><?php echo "Megrendelem gomb <span class='help'>A terméket rögtön az egy oldalas kifizetéshez küldi</span>"; ?></td>
                <td><?php if (!empty($megjelenit_list_admin_megrendelem) ) { ?>
                        <input type="radio" name="megjelenit_list_admin_megrendelem" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_megrendelem" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_list_admin_megrendelem" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_list_admin_megrendelem" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
        </table>


        <table class="form" id="fttab-termekful">
            <tr>
                <td title="megjelenit_admin_termekful[tulajdonsag_ful]" style="width: 244px;"><?php echo $entry_tulajdonsag_ful; ?></td>
                <td style="width: 144px;">
                    <?php if ($tulajdonsag_ful['tulajdonsag_ful']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[tulajdonsag_ful]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[tulajdonsag_ful]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[tulajdonsag_ful]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[tulajdonsag_ful]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[valasztek_ful]" style="width: 244px;"><?php echo $entry_valasztek_ful; ?></td>
                <td style="width: 144px;">
                    <?php if ($valasztek_ful['valasztek_ful']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[valasztek_ful]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[valasztek_ful]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[valasztek_ful]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[valasztek_ful]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[mennyisegi_ful]" style="width: 244px;"><?php echo $entry_mennyisegi_ful; ?></td>
                <td style="width: 144px;">
                    <?php if ($mennyisegi_ful['mennyisegi_ful']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[mennyisegi_ful]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[mennyisegi_ful]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[mennyisegi_ful]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[mennyisegi_ful]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[vevo_ful]" style="width: 244px;"><?php echo $entry_vevo_ful; ?></td>
                <td style="width: 144px;">
                    <?php if ($vevo_ful['vevo_ful']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[vevo_ful]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[vevo_ful]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[vevo_ful]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[vevo_ful]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[akcios_ful]" style="width: 244px;"><?php echo $entry_akcios_ful; ?></td>
                <td style="width: 144px;">
                    <?php if ($akcios_ful['akcios_ful']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[akcios_ful]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[akcios_ful]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[akcios_ful]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[akcios_ful]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[kep_ful]" style="width: 244px;"><?php echo $entry_kep_ful; ?></td>
                <td style="width: 144px;">
                    <?php if ($kep_ful['kep_ful']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[kep_ful]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[kep_ful]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[kep_ful]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[kep_ful]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[jutalom_ful]" style="width: 244px;"><?php echo $entry_jutalom_ful; ?></td>
                <td style="width: 144px;">
                    <?php if ($jutalom_ful['jutalom_ful']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[jutalom_ful]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[jutalom_ful]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[jutalom_ful]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[jutalom_ful]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[elhelyezkedes]" style="width: 244px;"><?php echo $entry_elhelyezkedes; ?></td>
                <td style="width: 144px;">
                    <?php if ($elhelyezkedes['elhelyezkedes']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[elhelyezkedes]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[elhelyezkedes]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[elhelyezkedes]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[elhelyezkedes]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[kepek_helyszinenkent]" style="width: 244px;"><?php echo $entry_kepek_helyszinenkent; ?></td>
                <td style="width: 144px;">
                    <?php if ($kepek_helyszinenkent['kepek_helyszinenkent']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[kepek_helyszinenkent]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[kepek_helyszinenkent]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[kepek_helyszinenkent]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[kepek_helyszinenkent]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[tab_videok]" style="width: 244px;"><?php echo $entry_tab_videok; ?></td>
                <td style="width: 144px;">
                    <?php if ($tab_videok['tab_videok']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[tab_videok]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[tab_videok]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[tab_videok]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[tab_videok]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[tab_pdf]" style="width: 244px;"><?php echo $entry_tab_pdf; ?></td>
                <td style="width: 144px;">
                    <?php if ($tab_pdf['tab_pdf']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[tab_pdf]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[tab_pdf]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[tab_pdf]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[tab_pdf]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekful[crm_kapcsolat]" style="width: 244px;"><?php echo 'Crm kapcsolat engedélyezése:'; ?></td>
                <td style="width: 144px;">
                    <?php if ($crm_kapcsolat['crm_kapcsolat']) { ?>
                        <input type="radio" name="megjelenit_admin_termekful[crm_kapcsolat]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[crm_kapcsolat]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekful[crm_kapcsolat]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekful[crm_kapcsolat]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
        </table>

        <table class="form" id="fttab-termektulajdonsag">


            <tr>
                <td title="megjelenit_form_admin_model"><?php echo $entry_form_model; ?></td>
                <td><?php if ($megjelenit_form_admin_model) { ?>
                        <input type="radio" name="megjelenit_form_admin_model" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_model" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_model" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_model" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>

                <td style="width: 10%" title="megjelenit_form_admin_model"><?php echo "Csak olvasható:"; ?></td>

                <td><?php if ($megjelenit_form_admin_model_readonly) { ?>
                        <input type="radio" name="megjelenit_form_admin_model_readonly" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_model_readonly" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_model_readonly" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_model_readonly" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_form_admin_cikkszam"><?php echo $entry_form_cikkszam; ?></td>
                <td><?php if ($megjelenit_form_admin_cikkszam) { ?>
                        <input type="radio" name="megjelenit_form_admin_cikkszam" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_cikkszam" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_cikkszam" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_cikkszam" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_form_admin_cikkszam2"><?php echo $entry_form_cikkszam2; ?></td>
                <td><?php if ($megjelenit_form_admin_cikkszam2) { ?>
                        <input type="radio" name="megjelenit_form_admin_cikkszam2" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_cikkszam2" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_cikkszam2" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_cikkszam2" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[egyedi_azonosito]" style="width: 244px;"><?php echo "Egyedi azonosító megjelenítése:"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['egyedi_azonosito']) && $megjelenit_admin_termekadatok['egyedi_azonosito']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyedi_azonosito]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyedi_azonosito]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyedi_azonosito]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyedi_azonosito]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[location]" style="width: 244px;"><?php echo "Location (egyéb) megjelenítése:"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['location']) && $megjelenit_admin_termekadatok['location']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[location]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[location]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[location]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[location]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[vonalkod]" style="width: 244px;"><?php echo "Vonalkód megjelenítése:"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['vonalkod']) && $megjelenit_admin_termekadatok['vonalkod']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[vonalkod]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[vonalkod]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[vonalkod]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[vonalkod]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[egyeb]" style="width: 244px;"><?php echo "Egyéb megjelenítése:"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['egyeb']) && $megjelenit_admin_termekadatok['egyeb']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyeb]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyeb]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyeb]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyeb]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[securitycode]" style="width: 244px;"><?php echo "Ellenőrző kód (frontend feltöltéhez):"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['securitycode']) && $megjelenit_admin_termekadatok['securitycode']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[securitycode]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[securitycode]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[securitycode]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[securitycode]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[max_szazalek_kedvezmeny]" style="width: 244px;"><?php echo "Maximum adható százalékos kedvezmény:"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['max_szazalek_kedvezmeny']) && $megjelenit_admin_termekadatok['max_szazalek_kedvezmeny']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[max_szazalek_kedvezmeny]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[max_szazalek_kedvezmeny]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[max_szazalek_kedvezmeny]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[max_szazalek_kedvezmeny]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>







            <tr>
                <td title="megjelenit_form_admin_eredeti_ar"><?php echo $entry_form_eredeti_ar; ?></td>
                <td colspan="4"><?php if ($megjelenit_form_admin_eredeti_ar) { ?>
                        <input type="radio" name="megjelenit_form_admin_eredeti_ar" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_eredeti_ar" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_eredeti_ar" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_eredeti_ar" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_form_admin_szazalek_ar"><?php echo 'Százalékban értendő mező megjelenítése'; ?></td>
                <td colspan="4"><?php if ($megjelenit_form_admin_szazalek_ar) { ?>
                        <input type="radio" name="megjelenit_form_admin_szazalek_ar" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_szazalek_ar" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_szazalek_ar" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_szazalek_ar" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_form_admin_kifuto"><?php echo 'Kifutó termék mező megjelenítése'; ?></td>
                <td colspan="4"><?php if ($megjelenit_form_admin_kifuto) { ?>
                        <input type="radio" name="megjelenit_form_admin_kifuto" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_kifuto" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_kifuto" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_kifuto" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_form_admin_ujdonsag"><?php echo $text_ujdonsag; ?></td>
                <td colspan="4"><?php if ($megjelenit_form_admin_ujdonsag) { ?>
                        <input type="radio" name="megjelenit_form_admin_ujdonsag" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_ujdonsag" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_ujdonsag" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_ujdonsag" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_form_admin_elerendeles"><?php echo 'Csak előrendelhető termék' ?></td>
                <td colspan="4"><?php if ( isset($megjelenit_form_admin_elerendeles) && $megjelenit_form_admin_elerendeles) { ?>
                        <input type="radio" name="megjelenit_form_admin_elerendeles" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_elerendeles" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_elerendeles" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_elerendeles" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_form_admin_ar_tiltasa"><?php echo 'Ár megjelenítés tiltása' ?></td>
                <td colspan="4"><?php if ( isset($megjelenit_form_admin_ar_tiltasa) && $megjelenit_form_admin_ar_tiltasa) { ?>
                        <input type="radio" name="megjelenit_form_admin_ar_tiltasa" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_ar_tiltasa" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_ar_tiltasa" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_ar_tiltasa" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <td title="megjelenit_form_admin_megrendelem"><?php echo "Megrendelem gomb <span class='help'>A terméket rögtön az egy oldalas kifizetéshez küldi</span>"; ?></td>
            <td colspan="4"><?php if ( !empty($megjelenit_form_admin_megrendelem)) { ?>
                    <input type="radio" name="megjelenit_form_admin_megrendelem" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_form_admin_megrendelem" value="0" />
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="megjelenit_form_admin_megrendelem" value="1" />
                    <?php echo $text_yes; ?>
                    <input type="radio" name="megjelenit_form_admin_megrendelem" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                <?php } ?>
            </td>
            </tr>


            <tr>
                <td title="megjelenit_form_admin_utalvany"><?php echo $entry_form_utalvany; ?></td>
                <td colspan="4"><?php if ($megjelenit_form_admin_utalvany) { ?>
                        <input type="radio" name="megjelenit_form_admin_utalvany" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_utalvany" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_form_admin_utalvany" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_form_admin_utalvany" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_meddig_kaphato"><?php echo $entry_meddig_kaphato; ?></td>
                <td><?php if ($megjelenit_meddig_kaphato) { ?>
                        <input type="radio" name="megjelenit_meddig_kaphato" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_meddig_kaphato" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_meddig_kaphato" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_meddig_kaphato" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_mikortol_kaphato"><?php echo $entry_mikortol_kaphato; ?></td>
                <td><?php if ($megjelenit_mikortol_kaphato) { ?>
                        <input type="radio" name="megjelenit_mikortol_kaphato" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_mikortol_kaphato" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_mikortol_kaphato" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_mikortol_kaphato" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_csomagolas_admin"><?php echo $entry_csomagolas_admin; ?></td>
                <td colspan="4"><?php if ($megjelenit_csomagolas_admin) { ?>
                        <input type="radio" name="megjelenit_csomagolas_admin" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csomagolas_admin" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csomagolas_admin" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csomagolas_admin" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[form_min_mennyiseg]" style="width: 244px;"><?php echo $entry_form_min_mennyiseg; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['form_min_mennyiseg']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_min_mennyiseg]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_min_mennyiseg]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_min_mennyiseg]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_min_mennyiseg]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekadatok[form_szallitando]" style="width: 244px;"><?php echo $entry_form_szallitando; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['form_szallitando']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_szallitando]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_szallitando]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_szallitando]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_szallitando]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[stock_date]" style="width: 244px;"><?php echo $entry_stock_date; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['stock_date']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[stock_date]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[stock_date]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[stock_date]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[stock_date]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[form_meret]" style="width: 244px;"><?php echo $entry_form_meret; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['form_meret']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_meret]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_meret]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_meret]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_meret]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekadatok[form_hosszmertek]" style="width: 244px;"><?php echo $entry_form_hosszmertek; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['form_hosszmertek']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_hosszmertek]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_hosszmertek]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_hosszmertek]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_hosszmertek]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekadatok[form_suly]" style="width: 244px;"><?php echo $entry_form_suly; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['form_suly']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_suly]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_suly]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_suly]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_suly]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekadatok[form_sulymertek]" style="width: 244px;"><?php echo $entry_form_sulymertek; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['form_sulymertek']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_sulymertek]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_sulymertek]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_sulymertek]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[form_sulymertek]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[kapcsolodo_partnerek]" style="width: 244px;"><?php echo $entry_form_kapcsolodo_partnerek; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['kapcsolodo_partnerek']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kapcsolodo_partnerek]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kapcsolodo_partnerek]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kapcsolodo_partnerek]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kapcsolodo_partnerek]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[kep_megjelenites_kapcsolas]" style="width: 244px;"><?php echo $entry_kep_megjelenites_tiltasa; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['kep_megjelenites_kapcsolas']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kep_megjelenites_kapcsolas]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kep_megjelenites_kapcsolas]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kep_megjelenites_kapcsolas]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kep_megjelenites_kapcsolas]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[nincs_negativ_ar]" style="width: 244px;"><?php echo $entry_admin_nincs_negativ_ar; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['nincs_negativ_ar']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[nincs_negativ_ar]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[nincs_negativ_ar]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[nincs_negativ_ar]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[nincs_negativ_ar]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[seourl_generalas]" style="width: 244px;"><?php echo $entry_admin_seourl_generalas; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['seourl_generalas']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[seourl_generalas]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[seourl_generalas]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[seourl_generalas]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[seourl_generalas]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[termek_cimkek]" style="width: 244px;"><?php echo $entry_admin_termek_cimkek; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['termek_cimkek']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[termek_cimkek]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[termek_cimkek]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[termek_cimkek]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[termek_cimkek]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[letoltheto_kep]" style="width: 244px;"><?php echo $entry_admin_letoltheto_kep; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['letoltheto_kep']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[letoltheto_kep]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[letoltheto_kep]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[letoltheto_kep]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[letoltheto_kep]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>

                <td><?php echo $entry_ellenorzes; ?>
                    <?php if ($megjelenit_admin_termekadatok['letoltheto_kep_ellenorzes']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[letoltheto_kep_ellenorzes]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[letoltheto_kep_ellenorzes]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[letoltheto_kep_ellenorzes]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[letoltheto_kep_ellenorzes]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>

            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[csomag_ajanlat]" style="width: 244px;"><?php echo $entry_admin_csomag_ajanlat; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['csomag_ajanlat']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[csomag_ajanlat]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[csomag_ajanlat]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[csomag_ajanlat]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[csomag_ajanlat]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[rovid_leiras]" style="width: 244px;"><?php echo $entry_admin_rovid_leiras; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['rovid_leiras']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[rovid_leiras]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[rovid_leiras]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[rovid_leiras]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[rovid_leiras]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[tesztek]" style="width: 244px;"><?php echo $entry_admin_tesztek; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['tesztek']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[tesztek]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[tesztek]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[tesztek]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[tesztek]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[tartozekok]" style="width: 244px;"><?php echo $entry_admin_tartozekok; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['tartozekok']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[tartozekok]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[tartozekok]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[tartozekok]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[tartozekok]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[altalanos_tartozekok]" style="width: 244px;"><?php echo $entry_admin_altalanos_tartozekok; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['altalanos_tartozekok']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[altalanos_tartozekok]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[altalanos_tartozekok]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[altalanos_tartozekok]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[altalanos_tartozekok]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekadatok[ajandek]" style="width: 244px;"><?php echo "Ajándék engedélyezése"; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['ajandek']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[ajandek]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[ajandek]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[ajandek]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[ajandek]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[helyettesito]" style="width: 244px;"><?php echo "Helyettesítő termékek engedélyezése"; ?></td>
                <td style="width: 144px;">
                    <?php if ($megjelenit_admin_termekadatok['helyettesito']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[helyettesito]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[helyettesito]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[helyettesito]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[helyettesito]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[termekenkenti_szallitasi_dij]" style="width: 244px;"><?php echo "Termékenkénti szállítási díj:"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['termekenkenti_szallitasi_dij']) &&  $megjelenit_admin_termekadatok['termekenkenti_szallitasi_dij']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[termekenkenti_szallitasi_dij]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[termekenkenti_szallitasi_dij]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[termekenkenti_szallitasi_dij]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[termekenkenti_szallitasi_dij]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[extra_garancia]" style="width: 244px;"><?php echo "Extra garancia"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['extra_garancia']) && $megjelenit_admin_termekadatok['extra_garancia']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[extra_garancia]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[extra_garancia]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[extra_garancia]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[extra_garancia]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[video]" style="width: 244px;"><?php echo $entry_video; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['video']) && $megjelenit_admin_termekadatok['video']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[video]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[video]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[video]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[video]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_termekadatok[lizing]" style="width: 244px;"><?php echo $entry_lizing; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['lizing']) && $megjelenit_admin_termekadatok['lizing']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[lizing]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[lizing]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[lizing]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[lizing]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekadatok[kiszereles]" style="width: 244px;"><?php echo $entry_kiszereles; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['kiszereles']) && $megjelenit_admin_termekadatok['kiszereles']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kiszereles]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kiszereles]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kiszereles]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kiszereles]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekadatok[kiszerelesmertek]" style="width: 244px;"><?php echo $entry_kiszerelesmertek; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['kiszerelesmertek']) && $megjelenit_admin_termekadatok['kiszerelesmertek']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kiszerelesmertek]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kiszerelesmertek]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kiszerelesmertek]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[kiszerelesmertek]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_termekadatok[egyszer_kosarba]" style="width: 244px;"><?php echo "A termék csak egyszer rakható kosárba"; ?></td>
                <td style="width: 144px;">
                    <?php if (isset($megjelenit_admin_termekadatok['egyszer_kosarba']) && $megjelenit_admin_termekadatok['egyszer_kosarba']) { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyszer_kosarba]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyszer_kosarba]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyszer_kosarba]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_termekadatok[egyszer_kosarba]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

        </table>

        <table class="form" id="fttab-rendeles">
            <tr>
                <td title="megjelenit_admin_rendeles[ajandekutalvanyok_ful]" style="width: 244px;"><?php echo $entry_ajandekutalvanyok_ful; ?></td>
                <td style="width: 144px;">
                    <?php if ($ajandekutalvanyok_ful['ajandekutalvanyok_ful']) { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[ajandekutalvanyok_ful]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[ajandekutalvanyok_ful]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[ajandekutalvanyok_ful]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[ajandekutalvanyok_ful]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_rendeles[kupon]" style="width: 244px;"><?php echo $entry_kupon; ?></td>
                <td style="width: 144px;">
                    <?php if ($kupon['kupon']) { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[kupon]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[kupon]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[kupon]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[kupon]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_rendeles[ajandekutalvany]" style="width: 244px;"><?php echo $entry_ajandekutalvany; ?></td>
                <td style="width: 144px;">
                    <?php if ($ajandekutalvany['ajandekutalvany']) { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[ajandekutalvany]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[ajandekutalvany]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[ajandekutalvany]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[ajandekutalvany]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_rendeles[jutalom]" style="width: 244px;"><?php echo $entry_jutalom; ?></td>
                <td style="width: 144px;">
                    <?php if ($jutalom['jutalom']) { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[jutalom]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[jutalom]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[jutalom]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[jutalom]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_rendeles[sap_adatkuldes_hungarosack]" style="width: 244px;"><?php echo "SAP adatküldés (Hungarosack)" ?></td>
                <td style="width: 144px;">
                    <?php if ($sap_adatkuldes_hungarosack['sap_adatkuldes_hungarosack']) { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[sap_adatkuldes_hungarosack]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[sap_adatkuldes_hungarosack]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[sap_adatkuldes_hungarosack]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[sap_adatkuldes_hungarosack]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_rendeles[csv_letolt]" style="width: 244px;"><?php echo "CSV letöltés engedélyezése" ?>
                    <span style="margin-left: 100px">Csoport neve:
                    <input type="text" name="megjelenit_admin_rendeles[csv_letolt_csoportnev]" value="<?php echo $csv_letolt_csoportnev['csv_letolt_csoportnev'] ?>" />
                </span></td>
                <td style="width: 144px;">

                    <?php if ($csv_letolt['csv_letolt']) { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_letolt]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_letolt]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_letolt]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_letolt]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>

            </tr>
            <tr>
                <td title="megjelenit_admin_rendeles[csv_gls_pont]" style="width: 244px;"><?php echo "GLS csomagpont CSV letöltés engedélyezése" ?>
                    <span style="margin-left: 100px">Csoport neve:
                    <input type="text" name="megjelenit_admin_rendeles[csv_gls_pont_csoportnev]" value="<?php echo $csv_gls_pont_csoportnev['csv_gls_pont_csoportnev'] ?>" />
                </span></td>
                <td style="width: 144px;">

                    <?php if ($csv_gls_pont['csv_gls_pont']) { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_gls_pont]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_gls_pont]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_gls_pont]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_gls_pont]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>

            </tr>
            <tr>
                <td title="megjelenit_admin_rendeles[csv_integracio_csoportnev]" style="width: 244px;"><?php echo "CSV integráció a vásárlásról" ?>
                    <span style="margin-left: 100px">Csoport neve
                    <input type="text" name="megjelenit_admin_rendeles[csv_integracio_csoportnev]" value="<?php echo $csv_integracio_csoportnev['csv_integracio_csoportnev'] ?>" />
                </span></td>
                <td style="width: 144px;">

                    <?php if ($csv_integracio['csv_integracio']) { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_integracio]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_integracio]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_integracio]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[csv_integracio]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="megjelenit_admin_rendeles[xml_szamlazz]" style="width: 244px;"><?php echo "XML vásárlásról szamlazz.hu" ?>
                <td style="width: 144px;">
                    <?php if ($xml_szamlazz['xml_szamlazz']) { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[xml_szamlazz]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[xml_szamlazz]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_rendeles[xml_szamlazz]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_rendeles[xml_szamlazz]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>


        </table>


        <table class="form" id="fttab-vevok">
            <thead>
            <tr>
                <td>
                    <?php echo $text_csv_vevok_title; ?>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td title="megjelenit_admin_vevok[csv_vevok]" style="width: 244px;"><?php echo $text_csv_vevok_setting; ?>
                    <span style="margin-left: 100px"><?php echo $text_group_name; ?>
                        <input type="text" name="megjelenit_admin_vevok[csv_vevok_csoportnev]" value="<?php echo $csv_vevok_csoportnev['csv_vevok_csoportnev'] ?>" />
                    </span>
                </td>
                <td style="width: 144px;">

                    <?php if ($csv_vevok['csv_vevok']) { ?>
                        <input type="radio" name="megjelenit_admin_vevok[csv_vevok]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_vevok[csv_vevok]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_vevok[csv_vevok]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_vevok[csv_vevok]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>

            </tr>


            <tr>
                <td title="megjelenit_admin_vevok[partner_kod]" style="width: 244px;"><?php echo $text_partnerkod_vevok_setting; ?></td>
                <td>

                    <?php if ($partner_kod) { ?>
                        <input type="radio" name="megjelenit_admin_vevok[partner_kod]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_vevok[partner_kod]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_vevok[partner_kod]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_vevok[partner_kod]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_admin_vevok[szazalek_read_only]" style="width: 244px;"><?php echo "Százalékos kedvezmény csak olvasható"; ?></td>
                <td>

                    <?php if ($szazalek_read_only) { ?>
                        <input type="radio" name="megjelenit_admin_vevok[szazalek_read_only]" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_vevok[szazalek_read_only]" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_admin_vevok[szazalek_read_only]" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_admin_vevok[szazalek_read_only]" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            </tbody>
        </table>


        <table class="form" id="fttab-export">
            <tr>
                <td title="megjelenit_csv_export_import"><?php echo "Csv export-import engedélyezése:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import) { ?>
                        <input type="radio" name="megjelenit_csv_export_import" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>

            <tr>
                <td title="megjelenit_csv_export_import_termek"><?php echo "Csv export-import termékek:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_termek) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_termek_termekek"><?php echo "Termékek:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_termek_termekek) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_termekek" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_termekek" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_termekek" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_termekek" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_termek_kapcsolatok"><?php echo "Általános kapcsolatok:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_termek_kapcsolatok) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_kapcsolatok" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_kapcsolatok" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_kapcsolatok" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_kapcsolatok" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_termek_valasztek"><?php echo "Kapcsolódó választék:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_termek_valasztek) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_valasztek" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_valasztek" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_valasztek" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_valasztek" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_termek_akcios"><?php echo "Akciós termékek:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_termek_akcios) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_akcios" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_akcios" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_akcios" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_akcios" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_termek_mennyisegi"><?php echo "Mennyiségi kedvezmények:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_termek_mennyisegi) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_mennyisegi" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_mennyisegi" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_mennyisegi" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_mennyisegi" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_termek_vevo_arak"><?php echo "Vevő egyedi árak:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_termek_vevo_arak) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_vevo_arak" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_vevo_arak" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_vevo_arak" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_termek_vevo_arak" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>


            <tr>
                <td title="megjelenit_csv_export_import_kategoriak"><?php echo "Csv export-import kategóriák:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_kategoriak) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_kategoriak" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_kategoriak" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_kategoriak" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_kategoriak" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>




            <tr>
                <td title="megjelenit_csv_export_import_jellemzo"><?php echo "Csv export-import jellemzok:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_jellemzo) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_jellemzo_jellemzok"><?php echo "Tulajdonságok:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_jellemzo_jellemzok) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo_jellemzok" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo_jellemzok" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo_jellemzok" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo_jellemzok" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_jellemzo_csoportok"><?php echo "Tulajdonság csoportok:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_jellemzo_csoportok) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo_csoportok" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo_csoportok" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo_csoportok" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_jellemzo_csoportok" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>


            <tr>
                <td title="megjelenit_csv_export_import_valasztek"><?php echo "Csv export-import választékok:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_valasztek) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_valasztek_valasztek"><?php echo "Választék:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_valasztek_valasztek) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_valasztek" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_valasztek" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_valasztek" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_valasztek" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_valasztek_ertek"><?php echo "Választék érték:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_valasztek_ertek) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_ertek" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_ertek" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_ertek" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_ertek" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_valasztek_szintablazat"><?php echo "Színtáblázat:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_valasztek_szintablazat) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szintablazat" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szintablazat" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szintablazat" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szintablazat" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_valasztek_szincsoport"><?php echo "Színcsoport:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_valasztek_szincsoport) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szincsoport" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szincsoport" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szincsoport" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szincsoport" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_valasztek_szinkapcsolat"><?php echo "Színcsoport-szín kapcsolat:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_valasztek_szinkapcsolat) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szinkapcsolat" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szinkapcsolat" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szinkapcsolat" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_valasztek_szinkapcsolat" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>



            <tr>
                <td title="megjelenit_csv_export_import_vevo"><?php echo "Csv export-import vevők:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_vevo) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_vevo_vevok"><?php echo "Vevők:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_vevo_vevok) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo_vevok" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo_vevok" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo_vevok" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo_vevok" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td title="megjelenit_csv_export_import_vevo_cimek"><?php echo "Címek:"; ?></td>
                <td><?php if ($megjelenit_csv_export_import_vevo_cimek) { ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo_cimek" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo_cimek" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo_cimek" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_csv_export_import_vevo_cimek" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>



        </table>
        <table class="form" id="fttab-fizetes">
            <tr>
                <td title="megjelenit_megrendeles_crm_integracio"><?php echo "Rendelés integrálás CRM-be engedélyezése:"; ?></td>
                <td><?php if ($megjelenit_megrendeles_crm_integracio) { ?>
                        <input type="radio" name="megjelenit_megrendeles_crm_integracio" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_megrendeles_crm_integracio" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_megrendeles_crm_integracio" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_megrendeles_crm_integracio" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
        </table>

    </div>


    <div id="ttab-nomodul">

        <div id="fttab" class="htabs">
            <a href="#fttab-alapmodulok"><?php echo $tab_alapmodulok; ?></a>
            <a href="#fttab-szallitas"><?php echo $tab_szallitas; ?></a>
            <a href="#fttab-fizetes-back"><?php echo $tab_fizetes; ?></a>
            <a href="#fttab-integraciok">Integrációk</a>
        </div>

        <table class="form" id="fttab-alapmodulok">
            <?php foreach ($extensions as $value) { ?>
                <tr>
                    <td title="<?php echo $value['filename']?>"><?php echo $value['name']; ?></td>
                    <td><?php if ($value['action'] == 1) { ?>
                            <input type="radio" name="<?php echo $value['filename']?>" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="<?php echo $value['filename']?>" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="<?php echo $value['filename']?>" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="<?php echo $value['filename']?>" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>



            <tr>
                <td title="megjelenit_vatera"><?php echo $entry_vatera; ?></td>
                <td><?php if ($megjelenit_vatera) { ?>
                        <input type="radio" name="megjelenit_vatera" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_vatera" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_vatera" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_vatera" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td title="megjelenit_sap"><?php echo $entry_sap; ?></td>
                <td><?php if ($megjelenit_sap) { ?>
                        <input type="radio" name="megjelenit_sap" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_sap" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_sap" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_sap" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

        </table>

        <table class="form" id="fttab-szallitas">
            <thead>
                <tr>
                    <td><?php echo $text_altalanos; ?></td>
                </tr>
            </thead>

            <tbody>
            <tr>
                <td title="megjelenit_gls"><?php echo $entry_gls; ?></td>
                <td><?php if ($megjelenit_gls) { ?>
                        <input type="radio" name="megjelenit_gls" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_gls" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="megjelenit_gls" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="megjelenit_gls" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            </tbody>

            <thead>
                <tr>
                    <td><?php echo $text_modulok; ?></td>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($extensions_shipping as $value) {?>
                    <tr>
                        <td title="<?php echo $value['filename']?>"><?php echo $value['name']; ?></td>
                        <td><?php if ($value['action'] == 1) { ?>
                                <input type="radio" name="<?php echo $value['filename']?>" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="<?php echo $value['filename']?>" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="<?php echo $value['filename']?>" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="<?php echo $value['filename']?>" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                    </tr>
                <?php }?>
            </tbody>
        </table>

        <table class="form" id="fttab-fizetes-back">
            <?php foreach ($extensions_payment as $value) {?>
                <tr>
                    <td title="<?php echo $value['filename']?>"><?php echo $value['name']; ?></td>
                    <td><?php if ($value['action'] == 1) { ?>
                            <input type="radio" name="<?php echo $value['filename']?>" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="<?php echo $value['filename']?>" value="0" />
                            <?php echo $text_no; ?>
                        <?php } else { ?>
                            <input type="radio" name="<?php echo $value['filename']?>" value="1" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="<?php echo $value['filename']?>" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php }?>
        </table>
        <table class="form" id="fttab-integraciok">
            <tr>
                <td title="engedelyez_integracio_argep">Árgép</td>
                <td><?php if ($engedelyez_integracio_argep) { ?>
                        <input type="radio" name="engedelyez_integracio_argep" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_argep" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="engedelyez_integracio_argep" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_argep" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="engedelyez_integracio_olcso">Olcsó</td>
                <td><?php if ($engedelyez_integracio_olcso) { ?>
                        <input type="radio" name="engedelyez_integracio_olcso" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_olcso" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="engedelyez_integracio_olcso" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_olcso" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="engedelyez_integracio_kirakat">Kirakat</td>
                <td><?php if ($engedelyez_integracio_kirakat) { ?>
                        <input type="radio" name="engedelyez_integracio_kirakat" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_kirakat" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="engedelyez_integracio_kirakat" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_kirakat" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="engedelyez_integracio_joaron">Jó áron</td>
                <td><?php if ($engedelyez_integracio_joaron) { ?>
                        <input type="radio" name="engedelyez_integracio_joaron" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_joaron" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="engedelyez_integracio_joaron" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_joaron" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="engedelyez_integracio_arukereso">Árúkereső</td>
                <td><?php if ($engedelyez_integracio_arukereso) { ?>
                        <input type="radio" name="engedelyez_integracio_arukereso" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_arukereso" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="engedelyez_integracio_arukereso" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_arukereso" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>


            <tr>
                <td title="engedelyez_integracio_arkozpont">Árközpont</td>
                <td><?php if ($engedelyez_integracio_arkozpont) { ?>
                        <input type="radio" name="engedelyez_integracio_arkozpont" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_arkozpont" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="engedelyez_integracio_arkozpont" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_arkozpont" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="engedelyez_integracio_osszehasonlitom">Összehasonlítom</td>
                <td><?php if ($engedelyez_integracio_osszehasonlitom) { ?>
                        <input type="radio" name="engedelyez_integracio_osszehasonlitom" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_osszehasonlitom" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="engedelyez_integracio_osszehasonlitom" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_osszehasonlitom" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td title="engedelyez_integracio_olcsobbat">Olcsóbbat</td>
                <td><?php if ($engedelyez_integracio_olcsobbat) { ?>
                        <input type="radio" name="engedelyez_integracio_olcsobbat" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_olcsobbat" value="0" />
                        <?php echo $text_no; ?>
                    <?php } else { ?>
                        <input type="radio" name="engedelyez_integracio_olcsobbat" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" name="engedelyez_integracio_olcsobbat" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                    <?php } ?>
                </td>
            </tr>

        </table>

    </div>