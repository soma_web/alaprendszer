<?php  echo $header;?>

<?php
    function adatbazisTabla($data=array()){ ?>


            <select name="<?php echo $data['select_name_tabla']?>" class="<?php echo $data['class_taba']?>" onchange="tablaValaszt(<?php echo$data['key_aktualis']?>)">
                <option>--- Válasszon ---</option>

                <?php foreach($data['szerkezet'] as $szerkezet_key=>$szerkezet_tabla) {?>
                    <?php if($data['tabla'] == $szerkezet_key){ ?>
                        <option selected="selected" value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>
                    <?php } else { ?>
                        <option value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>
                    <?php } ?>
                <?php }?>
            </select>

    <?php } ?>

<?php
function adatbazisMezo($data=array()){ ?>
        <select name="<?php echo $data['select_name_mezo']?>" class="<?php echo $data['class_mezo']?>">
            <option>--- Válasszon ---</option>
            <?php foreach($data['szerkezet'][$data['tabla']] as $szerkezet_mezok) {?>
                <?php if($szerkezet_mezok['Field'] == $data['mezo']){ ?>
                    <option selected="selected" value="<?php echo $szerkezet_mezok['Field']?>"><?php echo $szerkezet_mezok['Field']?></option>
                <?php } else { ?>
                    <option value="<?php echo $szerkezet_mezok['Field']?>"><?php echo $szerkezet_mezok['Field']?></option>
                <?php } ?>
            <?php }?>
        </select>
<?php } ?>


<div id="content" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
       <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div id="header_altalanos">
                    <a onclick="addCsoport();" class="button">Csoport hozzáadása</a>

                    <?php if (isset($csv_beallitasok_import) && $csv_beallitasok_import) {?>
                        <?php $elemszam = 0?>
                        <?php foreach($csv_beallitasok_import as $key_aktualis=>$csv_import) {?>
                            <div class="csoportok" id="csoport_<?php echo $key_aktualis?>" >

                                <div class="csoportok_nyito" style="padding: 5px 0 30px 20px;" onclick="csikiCsuki(this,<?php echo $key_aktualis;?>)">
                                    <a onclick="$('#csoport<?php echo "_".$key_aktualis;?>').remove();" class="button"><?php echo $button_remove; ?></a>
                                    <a style="text-decoration: none; margin-right: 40px; float: left">
                                        Csoport neve: <input type="text" name="csv_beallitasok_import[<?php echo $key_aktualis?>][import_name]" value="<?php echo $csv_import['import_name'];?>" size="25">
                                    </a>
                                </div>

                                <div class="csoport_beallitasok">
                                    <div class="csoport_<?php echo $key_aktualis;?>">



                                        <?php $tablasor = isset($csv_oszlop['tabla']) && !empty($csv_oszlop['tabla']) ? count($csv_oszlop['tabla']) : 0;?>
                                        <table class="fotabla">
                                            <tr>
                                                <td ><a onclick="addOszlop('<?php echo $key_aktualis?>');" class="button top">Új oszlop</a></td>
                                                <td ><span>
                                                        CSV file neve:
                                                        <input type="text" name="csv_beallitasok_import[<?php echo $key_aktualis?>][csv_name]" value="<?php echo $csv_import['csv_name'];?>" size="25">
                                                    </span>
                                                    <span>
                                                        Fejléces:
                                                        <?php if ($csv_import['fejleces'] == 1) {?>
                                                            <input type="radio" value=1  name="csv_beallitasok_import[<?php echo $key_aktualis?>][fejleces]" checked="checked"><?php echo $text_yes;?>
                                                            <input type="radio" value=0  name="csv_beallitasok_import[<?php echo $key_aktualis?>][fejleces]"><?php echo $text_no;?>
                                                        <?php } else { ?>
                                                            <input type="radio" value=1  name="csv_beallitasok_import[<?php echo $key_aktualis?>][fejleces]" ><?php echo $text_yes;?>
                                                            <input type="radio" value=0  name="csv_beallitasok_import[<?php echo $key_aktualis?>][fejleces]" checked="checked"><?php echo $text_no;?>                                                        <?php } ?>
                                                    </span>
                                                </td>
                                                <td>
                                                    <span>Karakterkódolás
                                                        <input type="text" name="csv_beallitasok_import[<?php echo $key_aktualis?>][kodolas_rol]" value="<?php echo $csv_import['kodolas_rol'];?>" size="10">
                                                        -->
                                                        <input type="text" name="csv_beallitasok_import[<?php echo $key_aktualis?>][kodolas_ra]" value="<?php echo $csv_import['kodolas_ra'];?>" size="10">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr><td><br><br></td></tr>


                                            <tr>
                                                <td colspan="3" id="csv_oszlop_sorok_<?php echo $key_aktualis?>">
                                                    <?php if (isset($csv_import['oszlopok'])) { ?>
                                                        <?php $utolso_sor = count($csv_import['oszlopok'])-1?>
                                                        <?php foreach ($csv_import['oszlopok'] as $oszlop_key=>$csv_oszlop) { ?>
                                                            <table style="margin:0 60px 30px 0;" class="csv_oszlopok_<?php echo $key_aktualis?>_<?php echo $oszlop_key?>">
                                                                <tr>
                                                                    <td class="oszlop_key_<?php echo $key_aktualis?>" id="oszlop_key_<?php echo $key_aktualis?>_<?php echo $oszlop_key?>" style="min-width: 83px">
                                                                        <span>Oszlop: <?php echo $oszlop_key?>
                                                                            <input type="text" value="<?php echo $csv_oszlop['oszlop_neve']?>"  name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][oszlop_neve]" size="15">
                                                                        </span>

                                                                        <?php if ($utolso_sor == $oszlop_key) { ?>
                                                                                <img class="oszlop_torol_<?php echo $key_aktualis?>" style="position: relative; bottom: -3px; padding-right: 10px;" src="view/image/delete.png" alt="" onclick="oszlopTorol(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>)" />
                                                                        <?php } else { ?>
                                                                            <span style="padding-right: 33px;"></span>
                                                                        <?php }  ?>
                                                                    </td>


                                                                    <td><span>Elöfeldolgozást igényel:
                                                                        <?php if ($csv_oszlop['elofeldolgozas'] == 1) {?>
                                                                            <input type="radio" value=0  onclick="eloFeldolgozas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][elofeldolgozas]"><?php echo $text_no;?>
                                                                            <input type="radio" value=1  onclick="eloFeldolgozas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][elofeldolgozas]" checked="checked"><?php echo $text_yes;?>
                                                                        <?php } else { ?>
                                                                            <input type="radio" value=0  onclick="eloFeldolgozas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][elofeldolgozas]" checked="checked" ><?php echo $text_no;?>
                                                                            <input type="radio" value=1  onclick="eloFeldolgozas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][elofeldolgozas]" ><?php echo $text_yes;?>
                                                                        <?php } ?>
                                                                        </span>
                                                                    </td>

                                                                    <?php $display = "";?>
                                                                    <?php if ($csv_oszlop['elofeldolgozas'] == 0) {?>
                                                                        <?php $display = "display: none"?>
                                                                    <?php } ?>
                                                                    <td id="elofeldolgozas_<?php echo $key_aktualis?>_<?php echo $oszlop_key?>" style="<?php echo $display?>">
                                                                        <span>Elválasztó karakter: <input type="text" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][elvalaszto]" value="<?php echo isset($csv_oszlop['elvalaszto']) ? $csv_oszlop['elvalaszto'] : ''?>" size="1" style="height: 11px"></span>
                                                                    </td>
                                                                    <td></td>

                                                                </tr>

                                                                <?php $kijelez = isset($csv_oszlop['hova']) ? "display: none" : "";?>
                                                                <tr class="tabla_mezo_hozzaadasa_<?php echo $key_aktualis.'_'.$oszlop_key?>" style="<?php echo $kijelez;?>">
                                                                    <td align=right colspan="3"><a onclick="tablaBeszuras(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,0);" class="button top">Tábla és mező hozzáadása</a></td>
                                                                </tr>

                                                                <?php if (isset($csv_oszlop['hova'])) {?>
                                                                    <?php foreach(($csv_oszlop['hova']) as $hova_key=>$csv_oszlop_hova) {?>
                                                                        <tr id="tabla_tabla_<?php echo $key_aktualis?>_<?php echo $oszlop_key?>_<?php echo $hova_key?>" class="tabla_tabla_<?php echo $key_aktualis?>_<?php echo $oszlop_key?> tabla_tabla_<?php echo $key_aktualis?>_<?php echo $oszlop_key?>_<?php echo $hova_key?>">
                                                                            <td></td>

                                                                            <?php
                                                                            $data=array(
                                                                                'key_aktualis'      =>  $key_aktualis,
                                                                                'select_name_tabla' =>  'csv_beallitasok_import['.$key_aktualis.'][oszlopok]['.$oszlop_key.'][hova]['.$hova_key.'][tabla]',
                                                                                'class_taba'        =>  'tablak_'.$key_aktualis,
                                                                                'tabla'             =>  $csv_oszlop_hova['tabla'],
                                                                                'szerkezet'         =>  $szerkezet
                                                                            );
                                                                            echo "<td>Tábla:";
                                                                            adatbazisTabla($data);
                                                                            echo "</td>"
                                                                            ?>


                                                                            <td>
                                                                                <span>Létezés vizsgálat:
                                                                                    <?php if (isset($csv_oszlop_hova['letezes_vizsgalat']) && $csv_oszlop_hova['letezes_vizsgalat']  == 1) {?>
                                                                                        <input type="radio" value=0  onclick="letezesVizsgalat(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][letezes_vizsgalat]"><?php echo $text_no;?>
                                                                                        <input type="radio" value=1  onclick="letezesVizsgalat(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][letezes_vizsgalat]" checked="checked"><?php echo $text_yes;?>
                                                                                    <?php } else { ?>
                                                                                        <input type="radio" value=0  onclick="letezesVizsgalat(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][letezes_vizsgalat]" checked="checked" ><?php echo $text_no;?>
                                                                                        <input type="radio" value=1  onclick="letezesVizsgalat(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][letezes_vizsgalat]" ><?php echo $text_yes;?>
                                                                                    <?php } ?>
                                                                                </span>
                                                                                <span><br>
                                                                                    Nemlétezés esetén felvitel:<br>
                                                                                    <?php
                                                                                    $data=array(
                                                                                        'key_aktualis'      =>  $key_aktualis,
                                                                                        'select_name_tabla' =>  'csv_beallitasok_import['.$key_aktualis.'][oszlopok]['.$oszlop_key.'][hova]['.$hova_key.'][nemletezes_tabla]',
                                                                                        'class_taba'        =>  'tablak_'.$key_aktualis,
                                                                                        'tabla'             =>  $csv_oszlop_hova['nemletezes_tabla'],
                                                                                        'szerkezet'         =>  $szerkezet
                                                                                    );
                                                                                    echo 'Tábla:';
                                                                                    adatbazisTabla($data);
                                                                                    echo "<br>";

                                                                                    $data=array(
                                                                                        'key_aktualis'      =>  $key_aktualis,
                                                                                        'select_name_mezo'  =>  'csv_beallitasok_import['.$key_aktualis.'][oszlopok]['.$oszlop_key.'][hova]['.$hova_key.'][nemletezes_mezo]',
                                                                                        'class_mezo'        =>  "",
                                                                                        'tabla'             =>  $csv_oszlop_hova['nemletezes_tabla'],
                                                                                        'mezo'              =>  $csv_oszlop_hova['nemletezes_mezo'],
                                                                                        'szerkezet'         =>  $szerkezet
                                                                                    );
                                                                                    echo "Mező : ";
                                                                                    adatbazisMezo($data)
                                                                                    ?><br><br><br>

                                                                                </span>
                                                                            </td>
                                                                            <td>
                                                                                <img style="float: right;" src="view/image/add.png" alt="" onclick="mezoBeszuras(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,<?php echo $hova_key?>,0)">
                                                                                <img style="float: right;" src="view/image/delete.png" alt="" onclick="tablaMezoTorol(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,<?php echo $hova_key?>) " >
                                                                            </td>
                                                                        </tr>


                                                                        <?php foreach($csv_oszlop_hova['mezo'] as $mezo_key=>$tabla_mezoi) {?>
                                                                            <tr id="tabla_mezo_<?php echo $key_aktualis?>_<?php echo $oszlop_key?>_<?php echo $hova_key?>" class="tabla_mezo_<?php echo $key_aktualis?>_<?php echo $oszlop_key?> tabla_mezo_<?php echo $key_aktualis?>_<?php echo $oszlop_key?>_<?php echo $hova_key?>_<?php echo $mezo_key?>">
                                                                                <td></td>
                                                                                <?php
                                                                                $data=array(
                                                                                    'key_aktualis'      =>  $key_aktualis,
                                                                                    'select_name_mezo'  =>  'csv_beallitasok_import['.$key_aktualis.'][oszlopok]['.$oszlop_key.'][hova]['.$hova_key.'][mezo]['.$mezo_key.'][name]',
                                                                                    'class_mezo'        =>  "",
                                                                                    'tabla'             =>  $csv_oszlop_hova['tabla'],
                                                                                    'mezo'              =>  $csv_oszlop_hova['mezo'][$mezo_key]['name'],
                                                                                    'szerkezet'         =>  $szerkezet
                                                                                );?>
                                                                                <td >Mező:
                                                                                    <?php adatbazisMezo($data)?><br>
                                                                                    <span>
                                                                                        Vizsgálat:
                                                                                        <?php if (isset($csv_oszlop_hova['mezo'][$mezo_key]['relacio']) && $csv_oszlop_hova['mezo'][$mezo_key]['relacio']  == "LIKE" ) {?>
                                                                                            <input type="radio" value='LIKE'    name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][relacio]" checked="checked">Szövegre
                                                                                            <input type="radio" value='='       name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][relacio]">Számra
                                                                                        <?php } else { ?>
                                                                                            <input type="radio" value='LIKE'    name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][relacio]">Szövegre
                                                                                            <input type="radio" value='='       name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][relacio]"  checked="checked">Számra
                                                                                        <?php } ?>
                                                                                    </span>
                                                                                </td>

                                                                                <td>Érték:
                                                                                    <?php $checked =  (isset($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']) && $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']  == 0) ? 'checked="checked"' : ""; ?>
                                                                                    <div>
                                                                                        <input type="radio" value=0  onclick="ertekValasztas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][box]" <?php echo $checked?> >Aktuális oszlop értéke
                                                                                        <input type="hidden" value="<?php echo $oszlop_key?>"  name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][oszlop]" <?php echo $checked?> >

                                                                                    </div>
                                                                                    <?php $checked =  (isset($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']) && $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']  == 1) ? 'checked="checked"' : ""; ?>
                                                                                    <div>
                                                                                        <input type="radio" value=1  onclick="ertekValasztas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][box]" <?php echo $checked?> >Választott oszlop értéke
                                                                                        <select name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][oszlop]">
                                                                                            <option> Válasszon </option>
                                                                                        <?php foreach($csv_import['oszlopok'] as $select_key=>$select_oszlop) { ?>
                                                                                            <?php if($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['oszlop'] === $select_key."") { ?>
                                                                                                <option value=<?php echo $select_key?> selected="selected"><?php echo $select_key?></option>
                                                                                            <?php } else { ?>
                                                                                                <option value=<?php echo $select_key?>><?php echo $select_key?></option>
                                                                                            <?php }  ?>
                                                                                        <?php } ?>
                                                                                        </select>
                                                                                    </div>
                                                                                    <?php $checked =  (isset($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']) && $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']  == 2) ? 'checked="checked"' : ""; ?>
                                                                                    <div>
                                                                                        <input type="radio" value=2  onclick="ertekValasztas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][box]" <?php echo $checked?> >Választott oszlop mentett értéke
                                                                                    </div>
                                                                                    <?php $checked =  (isset($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']) && $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']  == 3) ? 'checked="checked"' : ""; ?>
                                                                                    <div>
                                                                                        <input type="radio" value=3  onclick="ertekValasztas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][box]" <?php echo $checked?> >Standart érték
                                                                                        <input type="text" value="<?php echo $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['standard']?>" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][standard]" <?php echo $checked?> size="3">
                                                                                    </div>

                                                                                </td>

                                                                                <td>
                                                                                    <img style="float: right;" src="view/image/add.png" alt="" onclick="mezoBeszuras(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,<?php echo $hova_key?>,0)">
                                                                                    <img style="float: right;" src="view/image/delete.png" alt="" onclick="tablaMezoTorol(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,<?php echo $hova_key?>) " >
                                                                                </td>
                                                                            </tr>
                                                                        <?php } ?>






                                                                <?php $mezo_key =1?>

                                                                <!--<tr id="tabla_mezo_<?php echo $key_aktualis?>_<?php echo $oszlop_key?>_<?php echo $hova_key?>" class="tabla_mezo_<?php echo $key_aktualis?>_<?php echo $oszlop_key?> tabla_mezo_<?php echo $key_aktualis?>_<?php echo $oszlop_key?>_<?php echo $hova_key?>_<?php echo $mezo_key?>">
                                                                    <td></td>




                                                                    <?php
                                                                    $data=array(
                                                                        'key_aktualis'      =>  $key_aktualis,
                                                                        'select_name_mezo'  =>  'csv_beallitasok_import['.$key_aktualis.'][oszlopok]['.$oszlop_key.'][hova]['.$hova_key.'][mezo]['.$mezo_key.'][name]',
                                                                        'class_mezo'        =>  "",
                                                                        'tabla'             =>  $csv_oszlop_hova['tabla'],
                                                                        'mezo'              =>  $csv_oszlop_hova['mezo'][$mezo_key]['name'],
                                                                        'szerkezet'         =>  $szerkezet
                                                                    );?>
                                                                    <td >Mező:
                                                                        <?php adatbazisMezo($data)?><br>
                                                                                <span>
                                                                                    Vizsgálat:
                                                                                    <?php if (isset($csv_oszlop_hova['mezo'][$mezo_key]['relacio']) && $csv_oszlop_hova['mezo'][$mezo_key]['relacio']  == "LIKE" ) {?>
                                                                                        <input type="radio" value='LIKE'    name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][relacio]" checked="checked">Szövegre
                                                                                        <input type="radio" value='='       name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][relacio]">Számra
                                                                                    <?php } else { ?>
                                                                                        <input type="radio" value='LIKE'    name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][relacio]">Szövegre
                                                                                        <input type="radio" value='='       name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][relacio]"  checked="checked">Számra
                                                                                    <?php } ?>
                                                                                </span>
                                                                    </td>

                                                                    <td>Érték:
                                                                        <?php $checked =  (isset($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']) && $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']  == 0) ? 'checked="checked"' : ""; ?>
                                                                        <div>
                                                                            <input type="radio" value=0  onclick="ertekValasztas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][box]" <?php echo $checked?> >Aktuális oszlop értéke
                                                                            <input type="hidden" value="<?php echo $oszlop_key?>"  name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][oszlop]" <?php echo $checked?> >

                                                                        </div>
                                                                        <?php $checked =  (isset($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']) && $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']  == 1) ? 'checked="checked"' : ""; ?>
                                                                        <div>
                                                                            <input type="radio" value=1  onclick="ertekValasztas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][box]" <?php echo $checked?> >Választott oszlop értéke
                                                                            <select name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][oszlop]">
                                                                                <option> Válasszon </option>
                                                                                <?php foreach($csv_import['oszlopok'] as $select_key=>$select_oszlop) { ?>
                                                                                    <?php if($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['oszlop'] == $select_key) { ?>
                                                                                        <option value="<?php echo $select_key?>" selected="selected"><?php echo $select_key?></option>
                                                                                    <?php } else { ?>
                                                                                        <option value="<?php echo $select_key?>"><?php echo $select_key?></option>
                                                                                    <?php }  ?>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                        <?php $checked =  (isset($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']) && $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']  == 2) ? 'checked="checked"' : ""; ?>
                                                                        <div>
                                                                            <input type="radio" value=2  onclick="ertekValasztas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][box]" <?php echo $checked?> >Választott oszlop mentett értéke
                                                                        </div>
                                                                        <?php $checked =  (isset($csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']) && $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['box']  == 3) ? 'checked="checked"' : ""; ?>
                                                                        <div>
                                                                            <input type="radio" value=3  onclick="ertekValasztas(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,this)" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][box]" <?php echo $checked?> >Standart érték
                                                                            <input type="text" value="<?php echo $csv_oszlop_hova['mezo'][$mezo_key]['ertek']['standard']?>" name="csv_beallitasok_import[<?php echo $key_aktualis?>][oszlopok][<?php echo $oszlop_key?>][hova][<?php echo $hova_key?>][mezo][<?php echo $mezo_key?>][ertek][standard]" <?php echo $checked?> size="3">
                                                                        </div>

                                                                    </td>

                                                                    <td>
                                                                        <img style="float: right;" src="view/image/add.png" alt="" onclick="mezoBeszuras(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,<?php echo $hova_key?>,0)">
                                                                        <img style="float: right;" src="view/image/delete.png" alt="" onclick="tablaMezoTorol(<?php echo $key_aktualis?>,<?php echo $oszlop_key?>,<?php echo $hova_key?>) " >
                                                                    </td>
                                                                </tr>-->








                                                                <tr><td><br></td><tr>




                                                                    <?php } ?>

                                                                <?php } ?>
                                                            </table>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>


                            </div>
                        <?php }?>
                    <?php } ?>
                </div>
                <input type="hidden" name="nyitott_sor" value="" id="nyitott_sor">

            </form>
    </div>
</div>


<script>
    var csv_row = <?php echo isset($key_aktualis) ? $key_aktualis+1 : 0?>;

    <?php if ($nyitott_sor !== "") { ?>
        $('#csoport_<?php echo $nyitott_sor;?> .csoport_beallitasok').css('display','block');
    <?php } ?>

    function addCsoport(){
        $('.csoport_beallitasok').hide();
        $('.csoportok_nyito').removeClass("active");

        var html = "";

        html += '<div class="csoportok" id="csoport_'+csv_row+'" >';

            html += '<div class="csoportok_nyito" style="padding: 5px 0 30px 20px;" onclick="csikiCsuki(this,'+csv_row+')">';
                html +='<a onclick="$(\'#csoport_'+csv_row+'\').remove();" class="button"><?php echo $button_remove; ?></a>';
                html +='<a style="text-decoration: none; margin-right: 40px; float: left">';
                    html +='Csoport neve: <input type="text" name="csv_beallitasok_import['+csv_row+'][import_name]" value="" size="25">';
                html +='</a>';
            html +='</div>';

            html += '<div class="csoport_beallitasok">'
                html +='<div class="csoport_'+csv_row+'">';

                    html +='<table class="fotabla">';
                        html +='<tr>';
                            html +='<td ><a onclick="addOszlop('+csv_row+');" class="button top">Új oszlop</a></td>';
                        html +='</tr>';
                            html +='<tr><td><br><br></td></tr>';
                        html +='<tr>';
                            html +='<td>';
                                html +='<table>';
                                    html +='<tr>';
                                        html +='<td>Külső azonosító:</td>';
                                        html +='<td style="position: relative; top: -5px;">';
                                            html +='<input type="text" name="csv_beallitasok_import['+csv_row+'][kulso_azonosito][kulcs]>" value="">';
                                        html +='</td>';
                                    html +='</tr>';
                                    html +='<tr>';
                                        html +='<td>Oszlop száma:</td>';
                                        html +='<td style="position: relative; top: -5px;">';
                                            html +='<input type="text" name="csv_beallitasok_import['+csv_row+'][kulso_azonosito][oszlop]>" value="" size="1">';
                                        html +='</td>';
                                    html +='</tr>';
                                html +='</table>';
                            html +='</td>';

                            html +='<td >';
                                html +='<table>';
                                    html +='<tr>';
                                        html +='<td>Belső azonosító:</td>';
                                        html +='<td>Tábla: </td>';
                                        html +='<td>';
                                            html +='<select name="csv_beallitasok_import['+csv_row+'][belso_azonosito][tabla]" class="tablak_'+csv_row+'" onchange="tablaValaszt('+csv_row+')">';
                                                html +='<option>--- Válasszon ---</option>';
                                                <?php foreach($szerkezet as $szerkezet_key=>$szerkezet_tabla) { ?>
                                                    html +='<option value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>';
                                                <?php } ?>
                                            html +='</select>';
                                        html +='</td>';
                                    html +='</tr>';
                                    html +='<tr>';
                                        html +='<td></td>';
                                        html +='<td>Mező:</td>';
                                        html +='<td>';
                                            html +='<select name="csv_beallitasok_import['+csv_row+'][belso_azonosito][mezo]" >';
                                                html +='<option>--- Válasszon ---</option>';
                                            html +='</select>';
                                        html +='</td>';
                                    html +='</tr>';
                                html +='</table>';
                            html +='</td>';

                        html +='<tr><td><br></td></tr>';


                        html +='<tr>';
                            html +='<td colspan="2" id="csv_oszlop_sorok_'+csv_row+'">';
                            html +='</td>';
                        html +='</tr>';
                    html +='</table>';
                html +='</div>';
            html +='</div>';
        html +='</div>';


        $("#header_altalanos").append(html);
        $('#csoport_'+csv_row+' .csoport_beallitasok').slideDown(500);
        $('#csoport_'+csv_row+' .csoportok_nyito').addClass("active");


        csv_row++;

    }



    function addOszlop(sor){

        var oszlop_key = $(".oszlop_key_"+sor).length;
        $(".oszlop_torol_"+sor).remove();

        var html = '';

        html += '<table style="margin:0 60px 30px 0;" class="csv_oszlopok_'+sor+'_'+oszlop_key+'">';
            html +='<tr>';
                html += '<td class="oszlop_key_'+sor+'" style="min-width: 83px" id="oszlop_key_'+sor+'_'+oszlop_key+'">Oszlop:'+oszlop_key;
                    html += '<img class="oszlop_torol_'+sor+'" style="position: relative; bottom: 2px; left: 5px;" src="view/image/delete.png" alt="" onclick="oszlopTorol('+sor+','+oszlop_key+')">';
                html += '</td>';
                /*html += '<td><span>Azonosító oszlop:';
                    html += '<input type="radio" value=0  name="csv_beallitasok_import['+sor+'][oszlopok]['+oszlop_key+'][azonosito]" checked="checked"> <?php echo $text_no;?>';
                    html += '<input type="radio" value=0  name="csv_beallitasok_import['+sor+'][oszlopok]['+oszlop_key+'][azonosito]" > <?php echo $text_yes;?>';
                    html += '</span>';
                html += '</td>';*/
                html += '<td><span>Elöfeldolgozást igényel:';
                        html += '<input type="radio" value=0  onclick="eloFeldolgozas('+sor+','+oszlop_key+',this)" name="csv_beallitasok_import['+sor+'][oszlopok]['+oszlop_key+'][elofeldolgozas]" checked="checked" ><?php echo $text_no;?>';
                        html += '<input type="radio" value=1  onclick="eloFeldolgozas('+sor+','+oszlop_key+',this)" name="csv_beallitasok_import['+sor+'][oszlopok]['+oszlop_key+'][elofeldolgozas]" ><?php echo $text_yes;?>';
                    html += '</span>';
                html += '</td>';
                html += '<td id="elofeldolgozas_'+sor+'_'+oszlop_key+'" style="display: none">';
                    html += '<span>Elválasztó karakter: <input type="text" name="csv_beallitasok_import['+sor+'][oszlopok]['+oszlop_key+'][elvalaszto]" value="" size="1" style="height: 11px"></span>';
                html += '</td>';
            html += '</tr>';


        html += addTabla(sor,oszlop_key,0);


        html += '</table>';

        debugger;
        $('#csv_oszlop_sorok_' +sor).append(html);
    }

    function oszlopTorol(sor,oszlop_key) {
        $('.csv_oszlopok_'+sor+'_'+oszlop_key).remove();

        var elozo_oszlop_key = oszlop_key -1;
        var html = 'Oszlop:'+elozo_oszlop_key;
        html += '<img class="oszlop_torol_'+sor+'" style="position: relative; bottom: 2px; left: 5px;" src="view/image/delete.png" alt="" onclick="oszlopTorol('+sor+','+elozo_oszlop_key+')">';
        $('#oszlop_key_'+sor+'_'+(oszlop_key-1)).html(html);

        return false;
    }


    function tablaBeszuras(sor,oszlop_key,hova_key){

        var html = addTabla(sor,oszlop_key,hova_key);

        if ($('#tabla_mezo_'+sor+'_'+oszlop_key+'_'+hova_key).length > 0) {
            $('#tabla_mezo_'+sor+'_'+oszlop_key+'_'+hova_key).after(html);
        } else {
            $('.tabla_mezo_hozzaadasa_'+sor+'_'+oszlop_key).after(html);
            $('.tabla_mezo_hozzaadasa_'+sor+'_'+oszlop_key).fadeOut(400);
        }
    }

    function mezoBeszuras(sor,oszlop_key,hova_key,utolso_mezo){

        var html = "";
        var hanyadik_mezo =utolso_mezo+1;

        html += '<tr class="tabla_mezo_'+sor+'_'+oszlop_key+'_'+hova_key+'_'+hanyadik_mezo+'">';
            html += '<td>';
                html += '<select name="csv_beallitasok_import['+sor+'][oszlopok]['+oszlop_key+'][hova]['+hova_key+'][mezo]['+hanyadik_mezo+']">';
                    html += '<option>--- Válasszon ---</option>';
                html += '</select>';
                html += '<img style="float: right;" src="view/image/add.png" alt="" onclick="mezoBeszuras('+sor+','+oszlop_key+','+hova_key+')">';
                html += '<img style="float: right;" src="view/image/delete.png" alt="" onclick="tablaMezoTorol('+sor+','+oszlop_key+','+hova_key+')" >';
            html += '</td>';
        html += '</tr>';

        debugger;
        $('#tabla_mezo_'+sor+'_'+oszlop_key+'_'+hova_key+'_'+utolso_mezo+':last-child').after(html);


    }

    function addTabla(sor,oszlop_key){

        var hova_key = 0;

        if ($(".tabla_mezo_"+sor+"_"+oszlop_key).length > 0) {
            debugger;
            $(".tabla_mezo_"+sor+"_"+oszlop_key).each(function(element) {
                kulcs = this.id.split("_");
                kulcs = (kulcs[kulcs.length-1]*1);

                if (hova_key < kulcs) {
                    hova_key = kulcs;
                }
            });
            hova_key ++;
        }
        var html = "";
        html += '<tr class="tabla_mezo_hozzaadasa_'+sor+'_'+oszlop_key+'" style="display: none">';
            html += '<td align=right colspan="3"><a onclick="tablaBeszuras('+sor+','+oszlop_key+',0);" class="button top">Tábla és mező hozzáadása</a></td>';
        html += '</tr>';

        html += '<tr id="tabla_mezo_'+sor+'_'+oszlop_key+'_'+hova_key+'" class="tabla_mezo_'+sor+'_'+oszlop_key+' tabla_mezo_'+sor+'_'+oszlop_key+'_'+hova_key+'">';
            html += '<td></td>';
            html += '<td>Tábla:';
                html += '<select name="csv_beallitasok_import['+sor+'][oszlopok]['+oszlop_key+'][hova]['+hova_key+'][tabla]" class="tablak_'+sor+'" onchange="tablaValaszt('+sor+')">';
                    html += '<option>--- Válasszon ---</option>';
                    <?php foreach($szerkezet as $szerkezet_key=>$szerkezet_tabla) {?>
                        html += '<option value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>';
                    <?php } ?>
                html += '</select>';
            html += '</td>';
            html += '<td>';
                html += '<select name="csv_beallitasok_import['+sor+'][oszlopok]['+oszlop_key+'][hova]['+hova_key+'][mezo]">';
                    html += '<option>--- Válasszon ---</option>';
                html += '</select>';
                html += '<img style="float: right;" src="view/image/add.png" alt="" onclick="tablaBeszuras('+sor+','+oszlop_key+','+hova_key+')">';
                html += '<img style="float: right;" src="view/image/delete.png" alt="" onclick="tablaMezoTorol('+sor+','+oszlop_key+','+hova_key+')" >';
            html += '</td>';
        html += '</tr>';

        return html;
    }

    function csikiCsuki(para,sor){
        //if (this.event.target == para || this.event.target.localName == "span") {
        if (window.event.target == para || window.event.target.localName == "span") {
            if ($(para).next().css("display") != "none") {
                $(para).next().slideUp(500);
                $(para).removeClass("active");

            } else {
                $('.csoportok_nyito').removeClass("active");
                $('.csoport_beallitasok').hide();
                $(para).next().slideDown(500);
                $(para).addClass("active");

            }
        }
    }


    function checkEllenoriz(aktualis_csv,aktualis_tabla,aktualis_elem) {
        $("#csoport_"+aktualis_csv+" input[type=checkbox]").prop("checked",false);
        $(aktualis_elem).prop("checked",true);


        $(".select_"+aktualis_csv).attr("disabled",false).css("opacity","1");
        $("#select_"+aktualis_csv+"_"+aktualis_tabla).attr("disabled",true).css("opacity","0.4");

    }


    function tablaValaszt(sor){
        $('#nyitott_sor').val(sor);
        $('#form').submit();
    }

    function eloFeldolgozas(sor,oszlop,para) {
        if (para.value == 0)
            $('#elofeldolgozas_'+sor+'_'+oszlop).fadeOut(600);
        else
            $('#elofeldolgozas_'+sor+'_'+oszlop).fadeIn(600);
    }

    function tablaMezoTorol(sor,oszlop_key,hova_key){
        $('.tabla_mezo_'+sor+'_'+oszlop_key+'_'+hova_key).remove();
        if($('.tabla_mezo_'+sor+'_'+oszlop_key).length < 1){
            $('.tabla_mezo_hozzaadasa_'+sor+'_'+oszlop_key).fadeIn(600);
        }
        return false;

    }
</script>







<?php echo $footer; ?>