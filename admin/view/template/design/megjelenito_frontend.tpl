<?php  echo $header; ?>

<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
        <div class="content">
            <div id="tabs" class="htabs">
                <a href="#fttab-fiok">Fiók megjelenítések</a>
                <a href="#fttab-adatbekeres"><?php echo $tab_frontend_adat_bekeres; ?></a>
                <a href="#fttab-listamegjelenites"><?php echo $tab_frontend_lista_megjeleni; ?></a>
            </div>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

                <div id="fttab-fiok">
                    <table class="form1">
                        <tr>
                            <td style="padding: 0 0 20px 0" title="megjelenit_modul_frontend"><b>Frontend modul engedélyezése:</b> </td>
                            <td style="padding: 0 0 21px 0;"><?php if ($megjelenit_modul_frontend) { ?>
                                    <input type="radio" name="megjelenit_modul_frontend" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_modul_frontend" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_modul_frontend" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_modul_frontend" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_sajat_termek">Saját termékeim:</td>
                            <td><?php if ($megjelenit_frontend_sajat_termek) { ?>
                                    <input type="radio" name="megjelenit_frontend_sajat_termek" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_sajat_termek" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_sajat_termek" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_sajat_termek" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_termek_felvitel">Új termék felvitele:</td>
                            <td><?php if ($megjelenit_frontend_termek_felvitel) { ?>
                                    <input type="radio" name="megjelenit_frontend_termek_felvitel" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_termek_felvitel" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_termek_felvitel" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_termek_felvitel" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_jelentesek">Jelentések - statisztikák</td>
                            <td><?php if ($megjelenit_frontend_jelentesek) { ?>
                                    <input type="radio" name="megjelenit_frontend_jelentesek" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_jelentesek" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_jelentesek" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_jelentesek" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_ertesitesi_jelentes">Értékesítési jelentés</td>
                            <td><?php if ($megjelenit_frontend_ertesitesi_jelentes) { ?>
                                    <input type="radio" name="megjelenit_frontend_ertesitesi_jelentes" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_ertesitesi_jelentes" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_ertesitesi_jelentes" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_ertesitesi_jelentes" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_letöltések">Letöltések csoportosítva</td>
                            <td><?php if ($megjelenit_frontend_letöltések) { ?>
                                    <input type="radio" name="megjelenit_frontend_letöltések" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_letöltések" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_letöltések" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_letöltések" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_megnezett">Megnézett termékeim</td>
                            <td><?php if ($megjelenit_frontend_megnezett) { ?>
                                    <input type="radio" name="megjelenit_frontend_megnezett" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_megnezett" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_megnezett" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_megnezett" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_letoltott">Letöltött</td>
                            <td><?php if ($megjelenit_frontend_letoltott) { ?>
                                    <input type="radio" name="megjelenit_frontend_letoltott" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_letoltott" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_letoltott" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_letoltott" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_hatteradatok">Háttéradatok</td>
                            <td><?php if ($megjelenit_frontend_hatteradatok) { ?>
                                    <input type="radio" name="megjelenit_frontend_hatteradatok" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_hatteradatok" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_hatteradatok" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_hatteradatok" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_utalvany">Utalvány ellenőrzése</td>
                            <td><?php if ($megjelenit_frontend_utalvany) { ?>
                                    <input type="radio" name="megjelenit_frontend_utalvany" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_utalvany" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_utalvany" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_utalvany" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_frontend_arajanlat">Arajánlat készítés</td>
                            <td><?php if ($megjelenit_frontend_arajanlat) { ?>
                                    <input type="radio" name="megjelenit_frontend_arajanlat" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_arajanlat" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_frontend_arajanlat" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_frontend_arajanlat" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                    </table>
                </div>

                <div id="fttab-adatbekeres">
                    <table class="form1">
                        <tr>
                            <td title="megjelenit_admin_product[megnevezes]" style="width: 244px;"><?php echo $entry_megnevezes; ?></td>
                            <td style="width: 144px;">
                                <?php if ($megnevezes['megnevezes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[megnevezes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[megnevezes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[megnevezes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[megnevezes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[megnevezes_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[megnevezes_sorrend]" value="<?php echo $megnevezes['megnevezes_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[megnevezes_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[megnevezes_neve]" value="<?php echo $megnevezes['megnevezes_neve']?>" size=22 /></td>
                            <td><?php echo $entry_ellenorzes; ?>
                                <?php if ($megnevezes['megnevezes_ellenorzes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[megnevezes_ellenorzes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[megnevezes_ellenorzes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[megnevezes_ellenorzes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[megnevezes_ellenorzes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td  title="megjelenit_admin_product[description]" style="width: 244px;"><?php echo $entry_description; ?></td>
                            <td style="width: 144px;">
                                <?php if ($description['description']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[description]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[description]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[description]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[description]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[description_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[description_sorrend]" value="<?php echo $description['description_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[description_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[description_neve]" value="<?php echo $description['description_neve']?>" size=22 /></td>

                            <td><?php echo $entry_ellenorzes; ?>
                                <?php if ($description['description_ellenorzes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[description_ellenorzes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[description_ellenorzes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[description_ellenorzes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[description_ellenorzes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[keyword]" style="width: 244px;"><?php echo $entry_keyword; ?></td>
                            <td style="width: 144px;">
                                <?php if ($keyword['keyword']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[keyword]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[keyword]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[keyword]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[keyword]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[keyword_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[keyword_sorrend]" value="<?php echo $keyword['keyword_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[keyword_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[keyword_neve]" value="<?php echo $keyword['keyword_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[meta_description]" style="width: 244px;"><?php echo $entry_meta_description; ?></td>
                            <td style="width: 144px;">
                                <?php if ($meta_description['meta_description']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[meta_description]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[meta_description]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[meta_description]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[meta_description]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[meta_description_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[meta_description_sorrend]" value="<?php echo $meta_description['meta_description_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[meta_description_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[meta_description_neve]" value="<?php echo $meta_description['meta_description_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[tag]" style="width: 244px;"><?php echo $entry_tag; ?></td>
                            <td style="width: 144px;">
                                <?php if ($tag['tag']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[tag]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[tag]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[tag]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[tag]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[tag_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[tag_sorrend]" value="<?php echo $tag['tag_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[tag_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[tag_neve]" value="<?php echo $tag['tag_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[category]" style="width: 244px;"><?php echo $entry_category; ?></td>
                            <td style="width: 144px;">
                                <?php if ($category['category']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[category]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[category]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[category]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[categoryag]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[category_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[category_sorrend]" value="<?php echo $category['category_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[category_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[category_neve]" value="<?php echo $category['category_neve']?>" size=22 /></td>

                            <td><?php echo $entry_ellenorzes; ?>
                                <?php if ($category['category_ellenorzes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[category_ellenorzes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[category_ellenorzes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[category_ellenorzes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[category_ellenorzes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[filter_csoport]" style="width: 244px;"><?php echo $entry_termek_filter_csoport; ?></td>
                            <td style="width: 144px;">
                                <?php if ($filter_csoport['filter_csoport']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[filter_csoport]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[filter_csoport]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[filter_csoport]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[filter_csoport]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[filter_csoport_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[filter_csoport_sorrend]" value="<?php echo $filter_csoport['filter_csoport_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[filter_csoport_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[filter_csoport_neve]" value="<?php echo $filter_csoport['filter_csoport_neve']?>" size=22 /></td>

                            <td><?php echo $entry_ellenorzes; ?>
                                <?php if ($filter_csoport['filter_csoport_ellenorzes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[filter_csoport_ellenorzes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[filter_csoport_ellenorzes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[filter_csoport_ellenorzes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[filter_csoport_ellenorzes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[filter]" style="width: 244px;"><?php echo $entry_filter; ?></td>
                            <td style="width: 144px;">
                                <?php if ($filter['filter']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[filter]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[filter]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[filter]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[filter]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[filter_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[filter_sorrend]" value="<?php echo $filter['filter_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[filter_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[filter_neve]" value="<?php echo $filter['filter_neve']?>" size=22 /></td>
                        </tr>


                        <tr>
                            <td title="megjelenit_admin_product[model]" style="width: 244px;"><?php echo 'Modell:'; ?></td>
                            <td style="width: 144px;">
                                <?php if ($model['model']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[model]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[model]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[model]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[model]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[model_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[model_sorrend]" value="<?php echo $model['model_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[model_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[model_neve]" value="<?php echo $model['model_neve']?>" size=22 /></td>
                        </tr>


                        <tr>
                            <td title="megjelenit_admin_product[cikkszam]"><?php echo $entry_cikkszam; ?></td>
                            <td><?php if ($cikkszam['cikkszam']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[cikkszam]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[cikkszam]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[cikkszam]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[cikkszam]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[cikkszam_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[cikkszam_sorrend]" value="<?php echo $cikkszam['cikkszam_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[cikkszam_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[cikkszam_neve]" value="<?php echo $cikkszam['cikkszam_neve']?>" size=22 /></td>
                        </tr>


                        <tr>
                            <td title="megjelenit_admin_product[cikkszam2]"><?php echo $entry_cikkszam2; ?></td>
                            <td><?php if ($cikkszam2['cikkszam2']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[cikkszam2]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[cikkszam2]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[cikkszam2]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[cikkszam2]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[cikkszam2_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[cikkszam2_sorrend]" value="<?php echo $cikkszam2['cikkszam2_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[cikkszam2_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[cikkszam2_neve]" value="<?php echo $cikkszam2['cikkszam2_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[sku]" style="width: 244px;"><?php echo $entry_sku; ?></td>
                            <td style="width: 144px;">
                                <?php if ($sku['sku']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[sku]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[sku]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[sku]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[sku]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[sku_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[sku_sorrend]" value="<?php echo $sku['sku_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[sku_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[sku_neve]" value="<?php echo $sku['sku_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[upc]" style="width: 244px;"><?php echo $entry_upc; ?></td>
                            <td style="width: 144px;">
                                <?php if ($upc['upc']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[upc]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[upc]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[upc]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[upc]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[upc_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[upc_sorrend]" value="<?php echo $upc['upc_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[upc_neve]" style="width: 63px;"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[upc_neve]" value="<?php echo $upc['upc_neve']?>" size=22 /></td>
                        </tr>





                        <tr>
                            <td title="megjelenit_admin_product[location]"><?php echo $entry_location; ?></td>
                            <td><?php if ($location['location']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[location]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[location]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[location]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[location]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[location_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[location_sorrend]" value="<?php echo $location['location_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[location_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[location_neve]" value="<?php echo $location['location_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[quantity]"><?php echo $entry_quantity; ?></td>
                            <td><?php if ($quantity['quantity']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[quantity]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[quantity]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[quantity]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[quantity]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[quantity_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[quantity_sorrend]" value="<?php echo $quantity['quantity_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[quantity_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[quantity_neve]" value="<?php echo $quantity['quantity_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[stock_status_id]"><?php echo $entry_stock_status_id; ?></td>
                            <td><?php if ($stock_status_id['stock_status_id']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[stock_status_id]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[stock_status_id]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[stock_status_id]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[stock_status_id]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[stock_status_id_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[stock_status_id_sorrend]" value="<?php echo $stock_status_id['stock_status_id_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[stock_status_id_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[stock_status_id_neve]" value="<?php echo $stock_status_id['stock_status_id_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[image]"><?php echo $entry_image; ?></td>
                            <td><?php if ($image['image']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[image]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[image]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[image]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[image]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[image_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[image_sorrend]" value="<?php echo $image['image_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[image_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[image_neve]" value="<?php echo $image['image_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[images]"><?php echo $entry_images; ?></td>
                            <td><?php if ($images['images']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[images]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[images]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[images]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[images]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[images_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[images_sorrend]" value="<?php echo $images['images_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[images_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[images_neve]" value="<?php echo $images['images_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[manufacturer_id]"><?php echo $entry_manufacturer_id; ?></td>
                            <td><?php if ($manufacturer_id['manufacturer_id']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[manufacturer_id]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[manufacturer_id]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[manufacturer_id]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[manufacturer_id]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[manufacturer_id_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[manufacturer_id_sorrend]" value="<?php echo $manufacturer_id['manufacturer_id_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[manufacturer_id_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[manufacturer_id_neve]" value="<?php echo $manufacturer_id['manufacturer_id_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[shipping]"><?php echo $entry_shipping; ?></td>
                            <td><?php if ($shipping['shipping']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[shipping]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[shipping]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[shipping]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[shipping]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[shipping_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[shipping_sorrend]" value="<?php echo $shipping['shipping_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[shipping_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[shipping_neve]" value="<?php echo $shipping['shipping_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[price]"><?php echo $entry_price; ?></td>
                            <td><?php if ($price['price']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[price]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[price]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[price]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[price]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[price_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[price_sorrend]" value="<?php echo $price['price_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[price_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[price_neve]" value="<?php echo $price['price_neve']?>" size=22 /></td>

                            <td><?php echo $entry_ellenorzes; ?>
                                <?php if ($price['price_ellenorzes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[price_ellenorzes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[price_ellenorzes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[price_ellenorzes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[price_ellenorzes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[szazalek]"><?php echo $entry_admin_szazalek; ?></td>
                            <td><?php if ($szazalek['szazalek']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[szazalek]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[szazalek]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[szazalek]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[szazalek]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[szazalek_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[szazalek_sorrend]" value="<?php echo $szazalek['szazalek_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[szazalek_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[szazalek_neve]" value="<?php echo $szazalek['szazalek_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[eredeti_ar]"><?php echo $entry_admin_eredeti_ar; ?></td>
                            <td><?php if ($eredeti_ar['eredeti_ar']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[eredeti_ar]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[eredeti_ar]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[eredeti_ar]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[eredeti_ar]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[eredeti_ar_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[eredeti_ar_sorrend]" value="<?php echo $eredeti_ar['eredeti_ar_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[eredeti_ar_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[eredeti_ar_neve]" value="<?php echo $eredeti_ar['eredeti_ar_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[points]"><?php echo $entry_points; ?></td>
                            <td><?php if ($points['points']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[points]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[points]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[points]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[points]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[points_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[points_sorrend]" value="<?php echo $points['points_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[points_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[points_neve]" value="<?php echo $points['points_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[tax_class_id]"><?php echo $entry_tax_class_id; ?></td>
                            <td><?php if ($tax_class_id['tax_class_id']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[tax_class_id]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[tax_class_id]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[tax_class_id]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[tax_class_id]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[tax_class_id_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[tax_class_id_sorrend]" value="<?php echo $tax_class_id['tax_class_id_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[tax_class_id_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[tax_class_id_neve]" value="<?php echo $tax_class_id['tax_class_id_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[date_available]"><?php echo $entry_date_available; ?></td>
                            <td><?php if ($date_available['date_available']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_available]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_available]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_available]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_available]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[date_available_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[date_available_sorrend]" value="<?php echo $date_available['date_available_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[date_available_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[date_available_neve]" value="<?php echo $date_available['date_available_neve']?>" size=22 /></td>
                        </tr>


                        <tr>
                            <td title="megjelenit_admin_product[date_ervenyes_ig]"><?php echo $entry_date_ervenyes_ig; ?></td>
                            <td><?php if ($date_ervenyes_ig['date_ervenyes_ig']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_ervenyes_ig]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_ervenyes_ig]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_ervenyes_ig]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_ervenyes_ig]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[date_ervenyes_ig_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[date_ervenyes_ig_sorrend]" value="<?php echo $date_ervenyes_ig['date_ervenyes_ig_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[date_ervenyes_ig_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[date_ervenyes_ig_neve]" value="<?php echo $date_ervenyes_ig['date_ervenyes_ig_neve']?>" size=22 /></td>

                            <td><?php echo $entry_ellenorzes; ?>
                                <?php if ($date_ervenyes_ig['date_ervenyes_ig_ellenorzes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_ervenyes_ig_ellenorzes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_ervenyes_ig_ellenorzes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_ervenyes_ig_ellenorzes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_ervenyes_ig_ellenorzes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[lejarat_nelkuli]"><?php echo 'Lejárat nélküli kapcsoló'; ?></td>
                            <td><?php if ($lejarat_nelkuli['lejarat_nelkuli']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[lejarat_nelkuli]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[lejarat_nelkuli]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[lejarat_nelkuli]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[lejarat_nelkuli]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td></td>
                            <td></td>
                            <td title="megjelenit_admin_product[lejarat_nelkuli_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[lejarat_nelkuli_neve]" value="<?php echo $lejarat_nelkuli['lejarat_nelkuli_neve']?>" size=22 /></td>

                            <td><?php echo $entry_default; ?>
                                <?php if ($lejarat_nelkuli['lejarat_nelkuli_default']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[lejarat_nelkuli_default]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[lejarat_nelkuli_default]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[lejarat_nelkuli_default]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[lejarat_nelkuli_default]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[elolnezet]"><?php echo $entry_elolnezet; ?></td>
                            <td><?php if ($elolnezet['elolnezet']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[elolnezet]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[elolnezet]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[elolnezet]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[elolnezet]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[elolnezet_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[elolnezet_sorrend]" value="<?php echo $elolnezet['elolnezet_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[elolnezet_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[elolnezet_neve]" value="<?php echo $elolnezet['elolnezet_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[weight]"><?php echo 'Súly'; ?></td>
                            <td><?php if ($weight['weight']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[weight]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[weight]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[weight]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[weight]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[weight_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[weight_sorrend]" value="<?php echo $weight['weight_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[weight_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[weight_neve]" value="<?php echo $weight['weight_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[weight_class_id]"><?php echo $entry_weight_class_id; ?></td>
                            <td><?php if ($weight_class_id['weight_class_id']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[weight_class_id]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[weight_class_id]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[weight_class_id]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[weight_class_id]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[weight_class_id_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[weight_class_id_sorrend]" value="<?php echo $weight_class_id['weight_class_id_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[weight_class_id_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[weight_class_id_neve]" value="<?php echo $weight_class_id['weight_class_id_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[length]"><?php echo $entry_length; ?></td>
                            <td><?php if ($length['length']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[length]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[length]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[length]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[length]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[length_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[length_sorrend]" value="<?php echo $length['length_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[length_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[length_neve]" value="<?php echo $length['length_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[width]"><?php echo $entry_width; ?></td>
                            <td><?php if ($width['width']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[width]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[width]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[width]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[width]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[width_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[width_sorrend]" value="<?php echo $width['width_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[width_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[width_neve]" value="<?php echo $width['width_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[height]"><?php echo $entry_height; ?></td>
                            <td><?php if ($height['height']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[height]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[height]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[height]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[height]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[height_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[height_sorrend]" value="<?php echo $height['height_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[height_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[height_neve]" value="<?php echo $height['height_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[length_class_id]"><?php echo $entry_length_class_id; ?></td>
                            <td><?php if ($length_class_id['length_class_id']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[length_class_id]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[length_class_id]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[length_class_id]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[length_class_id]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[length_class_id_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[length_class_id_sorrend]" value="<?php echo $length_class_id['length_class_id_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[length_class_id_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[length_class_id_neve]" value="<?php echo $length_class_id['length_class_id_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[subtract]"><?php echo $entry_subtract; ?></td>
                            <td><?php if ($subtract['subtract']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[subtract]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[subtract]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[subtract]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[subtract]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[subtract_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[subtract_sorrend]" value="<?php echo $subtract['subtract_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[subtract_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[subtract_neve]" value="<?php echo $subtract['subtract_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[minimum]"><?php echo $entry_minimum; ?></td>
                            <td><?php if ($minimum['minimum']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[minimum]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[minimum]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[minimum]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[minimum]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[minimum_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[minimum_sorrend]" value="<?php echo $minimum['minimum_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[minimum_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[minimum_neve]" value="<?php echo $minimum['minimum_neve']?>" size=22 /></td>

                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[sort_order]"><?php echo $entry_sort_order; ?></td>
                            <td><?php if ($sort_order['sort_order']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[sort_order]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[sort_order]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[sort_order]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[sort_order]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[sort_order_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[sort_order_sorrend]" value="<?php echo $sort_order['sort_order_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[sort_order_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[sort_order_neve]" value="<?php echo $sort_order['sort_order_neve']?>" size=22 /></td>

                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[status]"><?php echo $entry_status; ?></td>
                            <td><?php if ($status['status']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[status]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[status]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[status]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[status]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[status_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[status_sorrend]" value="<?php echo $status['status_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[status_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[status_neve]" value="<?php echo $status['status_neve']?>" size=22 /></td>

                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[date_added]"><?php echo $entry_date_added; ?></td>
                            <td><?php if ($date_added['date_added']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_added]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_added]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_added]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_added]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[date_added_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[date_added_sorrend]" value="<?php echo $date_added['date_added_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[date_added_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[date_added_neve]" value="<?php echo $date_added['date_added_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[date_modified]"><?php echo $entry_date_modified; ?></td>
                            <td><?php if ($date_modified['date_modified']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_modified]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_modified]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[date_modified]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[date_modified]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[date_modified_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[date_modified_sorrend]" value="<?php echo $date_modified['date_modified_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[date_modified_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[date_modified_neve]" value="<?php echo $date_modified['date_modified_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[viewed]"><?php echo $entry_viewed; ?></td>
                            <td><?php if ($viewed['viewed']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[viewed]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[viewed]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[viewed]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[viewed]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[viewed_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[viewed_sorrend]" value="<?php echo $viewed['viewed_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[viewed_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[viewed_neve]" value="<?php echo $viewed['viewed_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[megyseg]"><?php echo $entry_megyseg; ?></td>
                            <td><?php if ($megyseg['megyseg']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[megyseg]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[megyseg]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[megyseg]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[megyseg]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[megyseg_sorrend"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[megyseg_sorrend]" value="<?php echo $megyseg['megyseg_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[megyseg_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[megyseg_neve]" value="<?php echo $megyseg['megyseg_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[csomagolasi_mennyiseg]"><?php echo $entry_csomagolasi_mennyiseg; ?></td>
                            <td><?php if ($csomagolasi_mennyiseg['csomagolasi_mennyiseg']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[csomagolasi_mennyiseg]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[csomagolasi_mennyiseg]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[csomagolasi_mennyiseg]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[csomagolasi_mennyiseg]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[csomagolasi_mennyiseg_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[csomagolasi_mennyiseg_sorrend]" value="<?php echo $csomagolasi_mennyiseg['csomagolasi_mennyiseg_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[csomagolasi_mennyiseg_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[csomagolasi_mennyiseg_neve]" value="<?php echo $csomagolasi_mennyiseg['csomagolasi_mennyiseg_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[csomagolasi_egyseg]"><?php echo $entry_csomagolasi_egyseg; ?></td>
                            <td><?php if ($csomagolasi_egyseg['csomagolasi_egyseg']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[csomagolasi_egyseg]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[csomagolasi_egyseg]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[csomagolasi_egyseg]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[csomagolasi_egyseg]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[csomagolasi_egyseg_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[csomagolasi_egyseg_sorrend]" value="<?php echo $csomagolasi_egyseg['csomagolasi_egyseg_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[csomagolasi_egyseg_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[csomagolasi_egyseg_neve]" value="<?php echo $csomagolasi_egyseg['csomagolasi_egyseg_neve']?>" size=22 /></td>
                        </tr>


                        <tr>
                            <td title="megjelenit_admin_product[darabaru_meter]"><?php echo $entry_darabaru_meter; ?></td>
                            <td><?php if ($darabaru_meter['darabaru_meter']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[darabaru_meter]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[darabaru_meter]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[darabaru_meter]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[darabaru_meter]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[darabaru_meter_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[darabaru_meter_sorrend]" value="<?php echo $darabaru_meter['darabaru_meter_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[darabaru_meter_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[darabaru_meter_neve]" value="<?php echo $darabaru_meter['darabaru_meter_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[letoltheto]"><?php echo $entry_admin_letoltheto; ?></td>
                            <td><?php if ($letoltheto['letoltheto']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[letoltheto]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[letoltheto]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[letoltheto]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[letoltheto]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[letoltheto_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[letoltheto_sorrend]" value="<?php echo $letoltheto['letoltheto_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[letoltheto_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[letoltheto_neve]" value="<?php echo $letoltheto['letoltheto_neve']?>" size=22 /></td>

                            <td><?php echo $entry_ellenorzes; ?>
                                <?php if ($letoltheto['letoltheto_ellenorzes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[letoltheto_ellenorzes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[letoltheto_ellenorzes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[letoltheto_ellenorzes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[letoltheto_ellenorzes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[elhelyezkedes]"><?php echo $entry_admin_elhelyezkedes; ?></td>
                            <td><?php if ($elhelyezkedes_kiemeles['elhelyezkedes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[elhelyezkedes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[elhelyezkedes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[elhelyezkedes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[elhelyezkedes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[elhelyezkedes_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[elhelyezkedes_sorrend]" value="<?php echo $elhelyezkedes_kiemeles['elhelyezkedes_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[elhelyezkedes_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[elhelyezkedes_neve]" value="<?php echo $elhelyezkedes_kiemeles['elhelyezkedes_neve']?>" size=22 /></td>
                        </tr>

                        <tr>
                            <td title="megjelenit_admin_product[biztonsagi_kod]"><?php echo $entry_admin_biztonsagi_kod; ?></td>
                            <td><?php if ($biztonsagi_kod['biztonsagi_kod']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[biztonsagi_kod]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[biztonsagi_kod]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[biztonsagi_kod]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[biztonsagi_kod]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[biztonsagi_kod_sorrend]"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[biztonsagi_kod_sorrend]" value="<?php echo $biztonsagi_kod['biztonsagi_kod_sorrend']?>" size=2 /></td>
                            <td title="megjelenit_admin_product[biztonsagi_kod_neve]"><?php echo $entry_frontend_input_neve; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[biztonsagi_kod_neve]" value="<?php echo $biztonsagi_kod['biztonsagi_kod_neve']?>" size=22 /></td>
                            <td><?php echo $entry_ellenorzes; ?>
                                <?php if ($biztonsagi_kod['biztonsagi_kod_ellenorzes']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[biztonsagi_kod_ellenorzes]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[biztonsagi_kod_ellenorzes]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[biztonsagi_kod_ellenorzes]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[biztonsagi_kod_ellenorzes]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                        </tr>

                    </table>
                </div>

                <div id="fttab-listamegjelenites">

                    <table class="form1">
                        <tr>
                            <td title="megjelenit_admin_product[lista_model]" style="width: 244px;"><?php echo 'Modell'; ?></td>
                            <td style="width: 144px;">
                                <?php if ($lista_model['lista_model']) { ?>
                                    <input type="radio" name="megjelenit_admin_product[lista_model]" value="1" checked="checked" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[lista_model]" value="0" />
                                    <?php echo $text_no; ?>
                                <?php } else { ?>
                                    <input type="radio" name="megjelenit_admin_product[lista_model]" value="1" />
                                    <?php echo $text_yes; ?>
                                    <input type="radio" name="megjelenit_admin_product[lista_model]" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                <?php } ?>
                            </td>
                            <td title="megjelenit_admin_product[lista_model_sorrend]" style="width: 63px;"><?php echo $entry_admin_sorrend; ?></td>
                            <td><input type="text" name="megjelenit_admin_product[lista_model_sorrend]" value="<?php echo $lista_model['lista_model_sorrend']?>" size=2 /></td>
                        </tr>
                    </table>
                </div>

            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
   $('#tabs a').tabs();
</script>



<?php echo $footer; ?>