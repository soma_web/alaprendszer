<?php  
class ControllerProductCompare extends Controller {
	public function index() { 
		$this->language->load('product/compare');
		
		$this->load->model('catalog/product');

		$this->load->model('tool/image');
        $this->data['module_name'] = "compore";

        if (!isset($this->session->data['compare'])) {
			$this->session->data['compare'] = array();
		}	
				
		if (isset($this->request->get['remove'])) {
			$key = array_search($this->request->get['remove'], $this->session->data['compare']);
				
			if ($key !== false) {
				unset($this->session->data['compare'][$key]);
			}
		
			$this->session->data['success'] = $this->language->get('text_remove');
		
			$this->redirect($this->url->link('product/compare'));
		}
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),			
			'separator' => false
		);
				
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('product/compare'),			
			'separator' => $this->language->get('text_separator')
		);	
				
		$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
        $this->data['kaphato'] = $this->language->get('kaphato');
		$this->data['text_product'] = $this->language->get('text_product');
		$this->data['text_name'] = $this->language->get('text_name');
		$this->data['text_image'] = $this->language->get('text_image');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_model'] = $this->language->get('text_model');
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$this->data['text_availability'] = $this->language->get('text_availability');
		$this->data['text_rating'] = $this->language->get('text_rating');
		$this->data['text_summary'] = $this->language->get('text_summary');
		$this->data['text_weight'] = $this->language->get('text_weight');
		$this->data['text_dimension'] = $this->language->get('text_dimension');
		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['text_netto'] = $this->language->get('text_netto');

		
		$this->data['button_continue'] = $this->language->get('button_continue');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
								
		$this->data['products'] = array();
		
		$this->data['attribute_groups'] = array();
        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => 20
        );
        $this->load->model('catalog/product');

        $results1 = $this->model_catalog_product->getProductsNew($data1);
		$setting['image_width'] = $this->config->get('config_image_compare_width');
		$setting['image_height'] = $this->config->get('config_image_compare_height');


		foreach ($this->session->data['compare'] as $key => $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);



			if ($product_info) {

				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_compare_width'), $this->config->get('config_image_compare_height'));
				} else {
					$image = false;
				}

                $elojel = "";
                if ( (isset($product_info['utalvany']) &&  $product_info['utalvany'] == 1) && $this->config->get('megjelenit_negativ_ar') == 1) {
                    $elojel = "-";
                }
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $elojel.$this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }
				
				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                } else {
                    $tax = false;
                }
				if ($product_info['quantity'] <= 0) {
					$availability = $product_info['stock_status'];
				} elseif ($this->config->get('config_stock_display')) {
					$availability = $product_info['quantity'];
				} else {
					$availability = $this->language->get('text_instock');
				}
                $uj=false;
                if (array_key_exists($product_info['product_id'],$results1)){
                    $uj=1;
                }
				$attribute_data = array();
				
				$attribute_groups = $this->model_catalog_product->getProductAttributes($product_id);
				
				foreach ($attribute_groups as $attribute_group) {
					foreach ($attribute_group['attribute'] as $attribute) {
						$attribute_data[$attribute['attribute_id']] = $attribute['text'];
					}
				}
                $discount_query = $this->model_catalog_product->getProductDiscounts($product_info['product_id']);
                $discounts = array();
                $legolcsobb_kulcs=0;
                if($product_info['price'])  {
                    foreach ($discount_query as $discount) {
                        $discounts[] = array(
                            'quantity' => $discount['quantity'],
                            'netto'    => $this->currency->format($discount['price']),
                            'brutto'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))),
                            'ara'    => $this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))
                        );
                    }
                    if (count($discounts) > 0){
                        $legolcsobb=$discounts[0]['ara'];
                        foreach ($discounts as $key => $value) {
                            if ($value['ara'] < $legolcsobb ){
                                $legolcsobb_kulcs=$key;
                                $legolcsobb=$value['ara'];
                            }
                        }
                    }
                }
                if (count($discounts) == 0) $discounts[$legolcsobb_kulcs]=false;

                if ($product_info['quantity'] > 0 && $product_info['quantity'] <= 4) {
                    $pardarab = true;
                } else {
                    $pardarab = false;
                }

                $descr= strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8'));
                $descr=str_replace("&nbsp;","",$descr);
                $descr=str_replace("\r","",$descr);
                $descr=str_replace("\t","",$descr);
                $descr=str_replace("\n","",$descr);
                $descr=trim($descr);
                $descr=mb_substr($descr, 0, 1000, "UTF-8");

                $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
                $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');

				$this->data['products'][$product_id] = array(
					'product_id'    => $product_info['product_id'],
                    'szazalek'      => $product_info['szazalek'],
                    'utalvany'      => $product_info['utalvany'],
					'name'          => $product_info['name'],
					'elorendeles'   => $product_info['elorendeles'],
					'megrendelem'   => $product_info['megrendelem'],
					'thumb'         => $image,
					'price'         => $price,
                    'price_netto'   => $this->currency->format($product_info['price']),
                    'special_netto' => $product_info['special'] > 0 ? $this->currency->format($product_info['special']) : false,
                    'special' 	    => $special,
                    'date_ervenyes_ig' => $product_info['date_ervenyes_ig'],
                    'tax'           => $tax,
                    'imagedesabled' => $product_info['imagedesabled'],
					'description'   => $descr,
					'model'         => $product_info['model'],
					'cikkszam'      => $product_info['cikkszam'],
					'manufacturer' => $product_info['manufacturer'],
					'availability' => $availability,
					'rating'       => (int)$product_info['rating'],
					'reviews'      => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'weight'       => $this->weight->format($product_info['weight'], $product_info['weight_class_id']),
					'length'       => $this->length->format($product_info['length'], $product_info['length_class_id']),
					'width'        => $this->length->format($product_info['width'], $product_info['length_class_id']),
					'height'       => $this->length->format($product_info['height'], $product_info['length_class_id']),
					'attribute'    => $attribute_data,
					'href'         => $this->url->link('product/product', 'product_id=' . $product_id),
					'remove'       => $this->url->link('product/compare', 'remove=' . $product_id),
                    'uj'         => $uj,
                    'discounts' => $discounts[$legolcsobb_kulcs],
                    'pardarab'	 => $pardarab,
                    'csomagolasi_egyseg' => $product_info['csomagolasi_egyseg'],
                    'megyseg'    => $product_info['megyseg'],
                    'description' => mb_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 60) . '...',
                    'csomagolasi_mennyiseg' => $product_info['csomagolasi_mennyiseg'] > 1 ? $product_info['csomagolasi_mennyiseg'] : 1
				);
				
				foreach ($attribute_groups as $attribute_group) {
					$this->data['attribute_groups'][$attribute_group['attribute_group_id']]['name'] = $attribute_group['name'];
					
					foreach ($attribute_group['attribute'] as $attribute) {
						$this->data['attribute_groups'][$attribute_group['attribute_group_id']]['attribute'][$attribute['attribute_id']]['name'] = $attribute['name'];
					}
				}
			} else {
				unset($this->session->data['compare'][$key]);
			}
		}

        $this->data['continue'] = $this->url->link('common/home');
        $this->data['megjelenit_product'] = $this->config->get('megjelenit_product');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/compare.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/product/compare.tpl';
        } else {
            $this->template = 'default/template/product/compare.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );
		
		$this->response->setOutput($this->render());
  	}
	
	public function add() {
		$this->language->load('product/compare');

        $ikon = false;
        if (is_array($this->config->get('header_beallitas_header_csoportok')) ) {
            foreach( $this->config->get('header_beallitas_header_csoportok') as $value) {
                if (isset($value['templates'])) {
                    foreach( $value['templates'] as $key=>$template) {
                        if ($key == 'compare') {
                            if (isset($template['compare_icon'])) {
                                $ikon = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

		$json = array();

		if (!isset($this->session->data['compare'])) {
			$this->session->data['compare'] = array();
		}
				
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
		
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info) {
			if (!in_array($this->request->post['product_id'], $this->session->data['compare'])) {	
				if (count($this->session->data['compare']) >= 20) {
					array_shift($this->session->data['compare']);
				}
				
				$this->session->data['compare'][] = $this->request->post['product_id'];
			}
			 
			$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('product/compare'));



			$json['total'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));


            if($ikon) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/compare.png')) {
                    $compare = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/compare.png';
                } else {
                    $compare = DIR_TEMPLATE_IMAGE.'default/image/compare.png';
                }
                $json['total'] = ("<img class='icon_meret' title='$json[total]' src='$compare' >");

            }
		}


        $this->response->setOutput(json_encode($json));
	}
}
?>