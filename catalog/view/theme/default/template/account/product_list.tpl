<?php echo $header; ?>
<?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>

    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

<?php echo $column_left; ?><?php echo $column_right; ?>
    <div id="content"><?php echo $content_top; ?>

        <h1><img src="catalog/view/theme/default/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>

    <?php if ($kosarban_van > 0) { ?>
        <div id="kiemelesek_kosar">
            <?php echo $cart_elhelyezkedes; ?>
        </div>
    <?php } ?>

        <div class="button"><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>

        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="list frontend">
                <thead>
                <tr>
                    <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                    <td class="center"><?php echo $column_image; ?></td>
                    <td class="left elipsify"><?php if ($sort == 'pd.name') { ?>
                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                        <?php } ?></td>


                    <?php if ($mezok['lista_model'] == 1) {?>
                        <td class="left elipsify"><?php if ($sort == 'p.model') { ?>
                                <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>



                    <td class="right elipsify"><?php if ($sort == 'p.price') { ?>
                            <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                        <?php } ?></td>
                    <td class="right elipsify"><?php if ($sort == 'p.quantity') { ?>
                            <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                        <?php } ?></td>
                    <td class="left elipsify"><?php if ($sort == 'p.status') { ?>
                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                        <?php } ?></td>
                    <td class="right" nowrap><?php echo $column_action; ?></td>
                </tr>
                </thead>
                <tbody>
                <tr class="filter">
                    <td></td>
                    <td></td>
                    <td><input type="text" name="filter_name" class="productfilter" value="<?php echo $filter_name; ?>" /></td>
                    <?php if ($mezok['lista_model'] == 1) {?>
                        <td><input type="text" name="filter_model" value="<?php echo $filter_model; ?>" /></td>
                    <?php } ?>
                    <td align="right"><input type="text" name="filter_price" class="productfilter" value="<?php echo $filter_price; ?>" size="8"/></td>
                    <td align="right"><input type="text" name="filter_quantity" class="productfilter" value="<?php echo $filter_quantity; ?>" style="text-align: right;" /></td>
                    <td><select name="filter_status" class="productfilter">
                            <option value="*"></option>
                            <?php if ($filter_status) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                            <?php } ?>
                            <?php if (!is_null($filter_status) && !$filter_status) { ?>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                                <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                        </select></td>
                    <td align="right" nowrap><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
                </tr>
                <?php if ($products) { ?>
                    <?php foreach ($products as $product) { ?>
                        <tr>
                            <td style="text-align: center;"><?php if ($product['selected']) { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                                <?php } else { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                                <?php } ?></td>
                            <td class="center elipsify"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
                            <td class="left elipsify"><?php echo $product['name']; ?></td>
                            <?php if ($mezok['lista_model'] == 1) {?>
                                <td class="left elipsify"><?php echo $product['model']; ?></td>
                            <?php } ?>

                            <td class="right elipsify">
                                <?php if ($product['special']) { ?>
                                    <?php if($this->config->get('megjelenit_negativ_ar') == 1){ ?>
                                        <span style="text-decoration: line-through;"><?php echo '-'.$product['price']; ?></span><br/>
                                        <span style="color: #b00;"><?php echo '-'.$product['special']; ?></span>
                                    <?php } else { ?>
                                        <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                                        <span style="color: #b00;"><?php echo $product['special']; ?></span>                    <?php } ?>
                                <?php } else { ?>
                                    <?php if($this->config->get('megjelenit_negativ_ar') == 1){ ?>
                                        <?php echo '-'.$product['price']; ?>
                                    <?php } else { ?>
                                        <?php echo $product['price']; ?>
                                    <?php } ?>

                                <?php } ?>
                            </td>

                            <td class="right elipsify"><?php if ($product['quantity'] <= 0) { ?>
                                    <span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
                                <?php } elseif ($product['quantity'] <= 5) { ?>
                                    <span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
                                <?php } else { ?>
                                    <span style="color: #008000;"><?php echo $product['quantity']; ?></span>
                                <?php } ?></td>
                            <td class="left elipsify"><?php echo $product['status']; ?></td>
                            <td class="right elipsify" nowrap><?php foreach ($product['action'] as $action) { ?>
                                    [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                <?php } ?></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td class="center elipsify" colspan="8"><?php echo $text_no_results; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </form>
        <div class="pagination"><?php echo $pagination; ?></div>
        <?php echo $content_bottom; ?></div>


    <script type="text/javascript"><!--
        function filter() {
            url = 'index.php?route=account/product';
            var filter_name = $('input[name=\'filter_name\']').attr('value');

            if (filter_name) {
                url += '&filter_name=' + encodeURIComponent(filter_name);
            }

            var filter_model = $('input[name=\'filter_model\']').attr('value');

            if (filter_model) {
                url += '&filter_model=' + encodeURIComponent(filter_model);
            }

            var filter_price = $('input[name=\'filter_price\']').attr('value');

            if (filter_price) {
                url += '&filter_price=' + encodeURIComponent(filter_price);
            }

            var filter_quantity = $('input[name=\'filter_quantity\']').attr('value');

            if (filter_quantity) {
                url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
            }

            var filter_status = $('select[name=\'filter_status\']').attr('value');

            if (filter_status != '*') {
                url += '&filter_status=' + encodeURIComponent(filter_status);
            }

            location = url;
        }
        //--></script>
    <script type="text/javascript"><!--
        $('#form input').keydown(function(e) {
            if (e.keyCode == 13) {
                filter();
            }
        });
        //--></script>
    <script type="text/javascript"><!--
        $('input[name=\'filter_name\']').autocomplete({
            delay: 500,
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=account/product/autocomplete&filter_name=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item.name,
                                value: item.product_id
                            }
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $('input[name=\'filter_name\']').val(ui.item.label);

                return false;
            },
            focus: function(event, ui) {
                return false;
            }
        });

        $('input[name=\'filter_model\']').autocomplete({
            delay: 500,
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=account/product/autocomplete&filter_model=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item.model,
                                value: item.product_id
                            }
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $('input[name=\'filter_model\']').val(ui.item.label);

                return false;
            },
            focus: function(event, ui) {
                return false;
            }
        });
        //--></script>
<?php echo $footer; ?>