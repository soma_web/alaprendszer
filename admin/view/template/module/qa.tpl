<?php echo $header; ?>
<div class="modal fade" id="legal_text" tabindex="-1" role="dialog" aria-labelledby="legal_text_label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="legal_text_label"><?php echo $text_terms; ?></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default cancel" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $button_close; ?></button>
      </div>
    </div>
  </div>
</div>

<div id="content" class="main-content">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li<?php echo ($breadcrumb['active']) ? ' class="active bread"' : ''; ?>><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <div class="alerts">
    <div class="container" id="alerts">
      <?php foreach ($alerts as $type => $_alerts) { ?>
        <?php foreach ((array)$_alerts as $alert) { ?>
          <?php if ($alert) { ?>
      <div class="alert alert-<?php echo ($type == "error") ? "danger" : $type; ?> fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $alert; ?>
      </div>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    </div>
  </div>

  <div class="navbar-placeholder">
    <nav class="navbar navbar-default" role="navigation" id="bull5i-navbar">
      <div class="nav-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
            <span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand"><i class="fa fa-question-circle fa-fw ext-icon"></i> <?php echo $heading_title; ?></span>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#settings" data-toggle="tab"><?php echo $tab_settings; ?></a></li>
            <li><a href="#qa_form" data-toggle="tab"><?php echo $tab_form; ?></a></li>
            <li><a href="#questions" data-toggle="tab"><?php echo $tab_qa; ?></a></li>
            <!--<li><a href="#support" data-toggle="tab"><?php echo $tab_support; ?></a></li>
            <li><a href="#about" data-toggle="tab"><?php echo $tab_about; ?></a></li>-->
          </ul>
          <div class="nav navbar-nav btn-group navbar-btn navbar-right">
            <?php if ($update_pending) { ?><button type="button" class="btn btn-primary" id="upgrade" action="<?php echo $upgrade; ?>"><i class="fa fa-arrow-circle-up"></i> <?php echo $button_upgrade; ?></button><?php } ?>
            <button type="button" class="btn btn-default" id="apply" action="<?php echo $save; ?>"<?php echo $update_pending ? ' disabled': ''; ?>><?php echo $button_apply; ?></button>
            <button type="button" class="btn btn-default" id="save" action="<?php echo $save; ?>"<?php echo $update_pending ? ' disabled': ''; ?>><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
            <a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-ban"></i> <?php echo $button_cancel; ?></a>
          </div>
        </div>
      </div>
    </nav>
  </div>

  <div class="bull5i-content bull5i-container">
    <div id="page-overlay" class="bull5i-overlay fade">
      <div class="page-overlay-progress"><i class="fa fa-refresh fa-spin fa-5x text-muted"></i></div>
    </div>

    <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="beForm" class="form-horizontal" role="form">
      <div class="tab-content">
        <div class="tab-pane active" id="settings">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-cog fa-fw"></i> <?php echo $tab_settings; ?></h3></div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-12">
                  <fieldset>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_status"><?php echo $entry_extension_status; ?></label>
                      <div class="col-sm-2 fc-auto-width">
                        <select name="qa_status" id="qa_status" class="form-control">
                          <option value="1"<?php echo ((int)$qa_status) ? ' selected' : ''; ?>><?php echo $text_enabled; ?></option>
                          <option value="0"<?php echo (!(int)$qa_status) ? ' selected' : ''; ?>><?php echo $text_disabled; ?></option>
                        </select>
                        <input type="hidden" name="qa_installed" value="1" />
                        <input type="hidden" name="qa_installed_version" value="<?php echo $installed_version; ?>" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_new_question_notification1"><?php echo $entry_new_question_notification; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_new_question_notification" id="qa_new_question_notification1" value="1"<?php echo ((int)$qa_new_question_notification) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_new_question_notification" id="qa_new_question_notification0" value="0"<?php echo (!(int)$qa_new_question_notification) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
                        <span class="help-block help-text"><?php echo $help_new_question_notification; ?></span>
                      </div>
                    </div>
                    <div class="form-group<?php echo (isset($errors['qa_notification_emails'])) ? ' has-error' : ''; ?>">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_notification_emails<?php echo $this->config->get('config_language_id'); ?>"><?php echo $entry_notificatin_emails; ?></label>
                      <?php $count = 0; foreach ($languages as $language_id => $language) { ?>
                      <div class="<?php echo ($count) ? 'multi-row col-sm-offset-3 col-md-offset-2 ' : '' ; ?>col-sm-6 col-md-6 col-lg-5">
                        <div class="input-group">
                          <input type="text" name="qa_notification_emails[<?php echo $language_id; ?>]" id="qa_notification_emails<?php echo $language_id; ?>" value="<?php echo isset($qa_notification_emails[$language_id]) ? $qa_notification_emails[$language_id] : ''; ?>" class="form-control" />
                          <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                        </div>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 error-container" data-error-target="qa_notification_emails<?php echo $language_id; ?>">
                        <?php if (isset($errors['qa_notification_emails'][$language_id])) { ?>
                        <span class="help-block error-text"><?php echo $errors['qa_notification_emails'][$language_id]; ?></span>
                        <?php } ?>
                      </div>
                      <?php $count++; } ?>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
                        <span class="help-block help-text"><?php echo $help_notificatin_emails; ?></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_notification_from"><?php echo $entry_notificatin_email_from; ?></label>
                      <div class="col-sm-2 fc-auto-width">
                        <select name="qa_notification_from" id="qa_notification_from" class="form-control">
                          <option value="0"<?php echo ((int)$qa_notification_from != 1) ? ' selected' : ''; ?>><?php echo $text_store_email_address; ?></option>
                          <option value="1"<?php echo ((int)$qa_notification_from == 1) ? ' selected' : ''; ?>><?php echo $text_customer_email_address; ?></option>
                        </select>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
                        <span class="help-block help-text"><?php echo $help_notificatin_email_from; ?></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_question_reply_notification1"><?php echo $entry_question_reply_notification; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_question_reply_notification" id="qa_question_reply_notification1" value="1"<?php echo ((int)$qa_question_reply_notification) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_question_reply_notification" id="qa_question_reply_notification0" value="0"<?php echo (!(int)$qa_question_reply_notification) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
                        <span class="help-block help-text"><?php echo $help_question_reply_notification; ?></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_remove_sql_changes0"><?php echo $entry_remove_sql_changes; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_remove_sql_changes" id="qa_remove_sql_changes1" value="1"<?php echo ((int)$qa_remove_sql_changes) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_remove_sql_changes" id="qa_remove_sql_changes0" value="0"<?php echo (!(int)$qa_remove_sql_changes) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
                        <span class="help-block help-text"><?php echo $help_remove_sql_changes; ?></span>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="qa_form">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-cog fa-fw"></i> <?php echo $tab_form; ?></h3></div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-12">
                  <fieldset>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_display_name1"><?php echo $entry_form_display_name; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_name" id="qa_form_display_name1" value="1"<?php echo ((int)$qa_form_display_name) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_name" id="qa_form_display_name0" value="0"<?php echo (!(int)$qa_form_display_name) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_require_name1"><?php echo $entry_form_require_name; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_name" id="qa_form_require_name1" value="1"<?php echo ((int)$qa_form_require_name) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_name" id="qa_form_require_name0" value="0"<?php echo (!(int)$qa_form_require_name) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_display_email1"><?php echo $entry_form_display_email; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_email" id="qa_form_display_email1" value="1"<?php echo ((int)$qa_form_display_email) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_email" id="qa_form_display_email0" value="0"<?php echo (!(int)$qa_form_display_email) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_require_email1"><?php echo $entry_form_require_email; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_email" id="qa_form_require_email1" value="1"<?php echo ((int)$qa_form_require_email) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_email" id="qa_form_require_email0" value="0"<?php echo (!(int)$qa_form_require_email) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_display_phone1"><?php echo $entry_form_display_phone; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_phone" id="qa_form_display_phone1" value="1"<?php echo ((int)$qa_form_display_phone) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_phone" id="qa_form_display_phone0" value="0"<?php echo (!(int)$qa_form_display_phone) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_require_phone1"><?php echo $entry_form_require_phone; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_phone" id="qa_form_require_phone1" value="1"<?php echo ((int)$qa_form_require_phone) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_phone" id="qa_form_require_phone0" value="0"<?php echo (!(int)$qa_form_require_phone) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_display_phone1"><?php echo $entry_form_display_custom; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_custom_field" id="qa_form_display_custom_field1" value="1"<?php echo ((int)$qa_form_display_custom_field) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_custom_field" id="qa_form_display_custom_field0" value="0"<?php echo (!(int)$qa_form_display_custom_field) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_require_custom_field1"><?php echo $entry_form_require_custom; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_custom_field" id="qa_form_require_custom_field1" value="1"<?php echo ((int)$qa_form_require_custom_field) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_custom_field" id="qa_form_require_custom_field0" value="0"<?php echo (!(int)$qa_form_require_custom_field) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group<?php echo (isset($errors['qa_form_custom_field_name'])) ? ' has-error' : ''; ?>">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_custom_field_name<?php echo $this->config->get('config_language_id'); ?>"><?php echo $entry_form_custom_field_name; ?></label>
                      <?php $count = 0; foreach ($languages as $language_id => $language) { ?>
                      <div class="<?php echo ($count) ? 'multi-row col-sm-offset-3 col-md-offset-2 ' : '' ; ?>col-sm-5 col-md-5 col-lg-4">
                        <div class="input-group">
                          <input type="text" name="qa_form_custom_field_name[<?php echo $language_id; ?>]" id="qa_form_custom_field_name<?php echo $language_id; ?>" value="<?php echo isset($qa_form_custom_field_name[$language_id]) ? $qa_form_custom_field_name[$language_id] : ''; ?>" class="form-control" />
                          <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                        </div>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 error-container" data-error-target="qa_form_custom_field_name<?php echo $language_id; ?>">
                        <?php if (isset($errors['qa_form_custom_field_name'][$language_id])) { ?>
                        <span class="help-block error-text"><?php echo $errors['qa_form_custom_field_name'][$language_id]; ?></span>
                        <?php } ?>
                      </div>
                      <?php $count++; } ?>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_display_captcha1"><?php echo $entry_form_display_captcha; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_captcha" id="qa_form_display_captcha1" value="1"<?php echo ((int)$qa_form_display_captcha) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_display_captcha" id="qa_form_display_captcha0" value="0"<?php echo (!(int)$qa_form_display_captcha) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_form_require_captcha1"><?php echo $entry_form_require_captcha; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_captcha" id="qa_form_require_captcha1" value="1"<?php echo ((int)$qa_form_require_captcha) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_form_require_captcha" id="qa_form_require_captcha0" value="0"<?php echo (!(int)$qa_form_require_captcha) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="questions">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-cog fa-fw"></i> <?php echo $tab_qa; ?></h3></div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-12">
                  <fieldset>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_display_questions1"><?php echo $entry_display_questions; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_questions" id="qa_display_questions1" value="1"<?php echo ((int)$qa_display_questions) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_questions" id="qa_display_questions0" value="0"<?php echo (!(int)$qa_display_questions) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
                        <span class="help-block help-text"><?php echo $help_display_questions; ?></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_display_all_languages1"><?php echo $entry_display_all_languages; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_all_languages" id="qa_display_all_languages1" value="1"<?php echo ((int)$qa_display_all_languages) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_all_languages" id="qa_display_all_languages0" value="0"<?php echo (!(int)$qa_display_all_languages) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
                        <span class="help-block help-text"><?php echo $help_display_all_languages; ?></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_new_question_status"><?php echo $entry_new_questions_status; ?></label>
                      <div class="col-sm-2 fc-auto-width">
                        <select name="qa_new_question_status" id="qa_new_question_status" class="form-control">
                          <option value="1"<?php echo ((int)$qa_new_question_status) ? ' selected' : ''; ?>><?php echo $text_enabled; ?></option>
                          <option value="0"<?php echo (!(int)$qa_new_question_status) ? ' selected' : ''; ?>><?php echo $text_disabled; ?></option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group<?php echo (isset($errors['qa_items_per_page'])) ? ' has-error' : ''; ?>">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_items_per_page"><?php echo $entry_items_per_page; ?></label>
                      <div class="col-sm-2 col-md-2 col-lg-1">
                        <input type="text" name="qa_items_per_page" id="qa_items_per_page" value="<?php echo $qa_items_per_page; ?>" class="form-control" />
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 error-container">
                        <?php if (isset($errors['qa_items_per_page'])) { ?>
                        <span class="help-block error-text"><?php echo $errors['qa_items_per_page']; ?></span>
                        <?php } ?>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
                        <span class="help-block help-text"><?php echo $help_items_per_page; ?></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_preload"><?php echo $entry_preload; ?></label>
                      <div class="col-sm-2 fc-auto-width">
                        <select name="qa_preload" id="qa_preload" class="form-control">
                          <option value="0"<?php echo ((int)$qa_preload != 1 && (int)$qa_preload != 2) ? ' selected' : ''; ?>><?php echo $text_none; ?></option>
                          <option value="1"<?php echo ((int)$qa_preload == 1) ? ' selected' : ''; ?>><?php echo $text_first_page; ?></option>
                          <option value="2"<?php echo ((int)$qa_preload == 2) ? ' selected' : ''; ?>><?php echo $text_all; ?></option>
                        </select>
                      </div>
                      <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
                        <span class="help-block help-text"><?php echo $help_preload; ?></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_display_question_author1"><?php echo $entry_display_question_author; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_question_author" id="qa_display_question_author1" value="1"<?php echo ((int)$qa_display_question_author) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_question_author" id="qa_display_question_author0" value="0"<?php echo (!(int)$qa_display_question_author) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_display_question_date1"><?php echo $entry_display_question_date; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_question_date" id="qa_display_question_date1" value="1"<?php echo ((int)$qa_display_question_date) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_question_date" id="qa_display_question_date0" value="0"<?php echo (!(int)$qa_display_question_date) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_display_answer_author1"><?php echo $entry_display_answer_author; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_answer_author" id="qa_display_answer_author1" value="1"<?php echo ((int)$qa_display_answer_author) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_answer_author" id="qa_display_answer_author0" value="0"<?php echo (!(int)$qa_display_answer_author) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-md-2 control-label" for="qa_display_answer_date1"><?php echo $entry_display_answer_date; ?></label>
                      <div class="col-sm-9 col-md-10">
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_answer_date" id="qa_display_answer_date1" value="1"<?php echo ((int)$qa_display_answer_date) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="qa_display_answer_date" id="qa_display_answer_date0" value="0"<?php echo (!(int)$qa_display_answer_date) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                        </label>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="support">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#support-navbar-collapse">
                  <span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <h3 class="panel-title"><i class="fa fa-phone fa-fw"></i> <?php echo $tab_support; ?></h3>
              </div>
              <div class="collapse navbar-collapse" id="support-navbar-collapse">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="#general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                  <li><a href="#faq" data-toggle="tab" title="<?php echo $text_faq; ?>"><?php echo $tab_faq; ?></a></li>
                  <li><a href="#services" data-toggle="tab"><?php echo $tab_services; ?></a></li>
                </ul>
              </div>
            </div>
            <div class="panel-body">
              <div class="tab-content">
                <div class="tab-pane active" id="general">
                  <div class="row">
                    <div class="col-sm-12">
                      <h3>Getting support</h3>
                      <p>I consider support a priority of mine, so if you need any help with your purchase you can contact me in one of the following ways:</p>
                      <ul>
                        <li>Send an email to <a href="mailto:<?php echo $ext_support_email; ?>?subject='<?php echo $text_support_subject; ?>'"><?php echo $ext_support_email; ?></a></li>
                        <li>Post in the <a href="<?php echo $ext_support_forum; ?>" target="_blank">extension forum thread</a> or send me a <a href="http://forum.opencart.com/ucp.php?i=pm&mode=compose&u=17771">private message</a></li>
                        <li><a href="<?php echo $ext_store_url; ?>" target="_blank">Leave a comment</a> in the extension store comments section</li>
                      </ul>
                      <p>I usually reply within a few hours, but can take up to 24 hours.</p>
                      <p>Please note that all support is free if it is an issue with the product. Only issues due conflicts with other third party extensions/modules or custom front end theme are the exception to free support. Resolving such conflicts, customizing the extension or doing additional bespoke work will be provided with the hourly rate of <span id="hourly_rate">USD 50 / EUR 40</span>.</p>

                      <h4>Things to note when asking for help</h4>
                      <p>Please describe your problem in as much detail as possible. When contacting, please provide the following information:</p>
                      <ul>
                        <li>The OpenCart version you are using. <small>This can be found at the bottom of any admin page.</small></li>
                        <li>The extension name and version. <small>You can find this information under the About tab.</small></li>
                        <li>If you got any error messages, please include them in the message.</li>
                        <li>In case the error message is generated by a vQmod cached file, please also attach that file.</li>
                      </ul>
                      <p>Any additional information that you can provide about the issue is greatly appreciated and will make problem solving much faster.</p>

                      <h3 class="page-header">Happy with <?php echo $ext_name; ?>?</h3>
                      <p>I would appreciate it very much if you could <a href="<?php echo $ext_store_url; ?>" target="_blank">rate the extension</a> once you've had a chance to try it out. Why not tell everybody how great this extension is by <a href="<?php echo $ext_store_url; ?>" target="_blank">leaving a comment</a> as well.</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="alert alert-info">
                        <p><?php echo $text_other_extensions; ?></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="faq">
                  <h3><?php echo $text_faq; ?></h3>
                  <ul class="media-list" id="faqs">
                    <li class="media">
                      <div class="pull-left">
                        <i class="fa fa-question-circle fa-4x media-object"></i>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading">How to translate the extension to another language?</h4>

                        <p class="short-answer">The store front end translatable strings can be found in the <em>vqmod/xml/questions_and_answers.xml</em> vQmod script file and also from <em>catalog/language/english/mail/new_question.php</em> translation file.</p>

                        <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#translation" data-parent="#faqs">Show the full answer</button>
                        <div class="collapse full-answer" id="translation">
                          <ol>
                            <li>
                              <p><strong>Copy</strong> the following language files <strong>to YOUR_LANGUAGE folder</strong> under the appropriate location as shown below:</p>
                              <div class="btm-mgn">
                                <em class="text-muted"><small>FROM:</small></em>
                                <ul class="list-unstyled">
                                  <li>admin/language/english/catalog/qa.php</li>
                                  <li>admin/language/english/mail/question_reply.php</li>
                                  <li>admin/language/english/module/qa.php</li>
                                  <li>catalog/language/english/mail/new_question.php</li>
                                </ul>
                                <em class="text-muted"><small>TO:</small></em>
                                <ul class="list-unstyled">
                                  <li>admin/language/YOUR_LANGUAGE/catalog/qa.php</li>
                                  <li>admin/language/YOUR_LANGUAGE/mail/question_reply.php</li>
                                  <li>admin/language/YOUR_LANGUAGE/module/qa.php</li>
                                  <li>catalog/language/YOUR_LANGUAGE/mail/new_question.php</li>
                                </ul>
                              </div>
                            </li>

                            <li>
                              <p><strong>Open</strong> each of the copied <strong>language files</strong> with a text editor such as <a href="http://www.sublimetext.com/">Sublime Text</a> or <a href="http://notepad-plus-plus.org/">Notepad++</a> and <strong>make the required translations</strong>. You can also leave the files in English.</p>
                              <p><span class="label label-info">Note</span> You only need to translate the parts that are to the right of the equal sign.</p>
                              <p><span class="label label-danger">Important</span> The <em>admin/language/english/mail/question_reply.php</em> language file needs to be translated to each custom language your store front end uses, because this is used for sending question answer notifications to customers.</p>
                            </li>

                            <li>
                              <p>Some of the translatable strings are located inside the vQmod script file <em>vqmod/xml/questions_and_answers.xml</em>, so <strong>open the XML file</strong> with a text editor (<strong>not</strong> with a word processor application such as MS Word) and <strong>search</strong> for a <em>file</em> block that edits the <em>admin/language/english/common/header.php</em> language file. It should look similar to the following:</p>
                              <pre class="prettyprint linenums"><code class="language-xml">    &lt;file name=&quot;admin/language/english/common/header.php&quot;&gt;
        &lt;operation&gt;
            &lt;search position=&quot;after&quot;&gt;&lt;![CDATA[
            ?&gt;
            ]]&gt;&lt;/search&gt;
            &lt;add&gt;&lt;![CDATA[
$_[&#039;text_qa&#039;]                          = &#039;Q &amp;amp; A&#039;;
            ]]&gt;&lt;/add&gt;
        &lt;/operation&gt;
    &lt;/file&gt;</code></pre>
                            </li>

                            <li>
                              <p>Make a <strong>copy</strong> of the whole <em>file</em> block, <strong>replace</strong> <em>english</em> with <em>YOUR_LANGUAGE</em> in the file path and <strong>translate the string(s)</strong>. You can also leave the strings in English.</p>

                              <p><span class="label label-info">Note</span> If you want to quickly familiarize yourself with the simple <a href="http://code.google.com/p/vqmod/" target="_blank">vQmod</a> script syntax, please check out the <a href="http://code.google.com/p/vqmod/wiki/Scripting" target="_blank">official Wiki page</a></p>

                              <p>The end result would look similar to the following example:</p>

                              <pre class="prettyprint linenums"><code class="language-xml">    &lt;file name=&quot;admin/language/english/common/header.php&quot;&gt;
        &lt;operation&gt;
            &lt;search position=&quot;after&quot;&gt;&lt;![CDATA[
            ?&gt;
            ]]&gt;&lt;/search&gt;
            &lt;add&gt;&lt;![CDATA[
$_[&#039;text_qa&#039;]                          = &#039;Q &amp;amp; A&#039;;
            ]]&gt;&lt;/add&gt;
        &lt;/operation&gt;
    &lt;/file&gt;

    &lt;file name=&quot;admin/language/YOUR_LANGUAGE/common/header.php&quot;&gt;
        &lt;operation&gt;
            &lt;search position=&quot;after&quot;&gt;&lt;![CDATA[
            ?&gt;
            ]]&gt;&lt;/search&gt;
            &lt;add&gt;&lt;![CDATA[
$_[&#039;text_qa&#039;]                          = &#039;YOUR_LANGUAGE_TRANSLATION&#039;;
            ]]&gt;&lt;/add&gt;
        &lt;/operation&gt;
    &lt;/file&gt;</code></pre>
                            </li>

                            <li>
                              <p>Now, repeat steps <strong>3</strong> and <strong>4</strong> for the blocks that edit the following language files:</p>
                              <ul class="list-unstyled">
                                <li>admin/language/english/common/home.php</li>
                                <li>catalog/language/english/product/product.php</li>
                              </ul>
                            </li>
                          </ol>
                        </div>
                      </div>
                    </li>
                    <li class="media">
                      <div class="pull-left">
                        <i class="fa fa-question-circle fa-4x media-object"></i>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading">How to integrate the extension with a custom theme?</h4>

                        <p class="short-answer">If you are using a custom theme and the extension is not working out of the box then the first thing to do is to change the theme name in the <em>vqmod/xml/questions_and_answers.xml</em> vQmod script file to point to your custom theme.</p>

                        <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#theme_integration" data-parent="#faqs">Show the full answer</button>
                        <div class="collapse full-answer" id="theme_integration">
                          <p>In order to integrate the <?php echo $ext_name; ?> extension with your custom theme you need to modify the <em>vqmod/xml/questions_and_answers.xml</em> file. Find the block that edit the default theme <em>product.tpl</em> template file (near the end of the file) and change the theme name from 'default' to your custom theme (folder name).</p>
                          <p>If changing the theme name does not make it work, then your custom theme structure must differ in some way from the default theme. In this case you need to further tailor the vqmod search &amp; replace/insert patterns for the <em>product.tpl</em> template file to deal with the structural peculiarities of your custom theme.</p>
                          <p>As due to the very nature of a custom theme there does not exist a universal solution. A custom theme may have a different way of displaying things. Take a look at the changes made to the default theme and work out adjustments to the search &amp; replace patterns to suit your theme.</p>
                          <p>If you do not know how the vqmod script works, I kindly suggest you read about it from the vqmod <a href="https://code.google.com/p/vqmod/w/list" target="_blank">wiki pages</a>. vQmod log files (<em>vqmod/logs/*.log</em>) are helpful for debugging. They will tell you where the script fails (meaning which vqmod search line it does not find in the referenced file), so you need to adjust that part of the script.</p>
                          <p>If you would like to change the way questions and answers are displayed on the product page you should copy <em>catalog/view/theme/default/template/product/qa.tpl</em> template file to <em>catalog/view/theme/<strong>YOUR_CUSTOM_THEME_FOLDER_NAME</strong>/template/product/qa.tpl</em> and make changes to that file.</p>
                          <p>Should you find yourself in trouble with the changes I can offer commercial custom theme integration service. Please refer to the <a href="#" class="external-tab-link" data-target="#services">Services</a> section.</p>
                        </div>
                      </div>
                    </li>
                    <li class="media">
                      <div class="pull-left">
                        <i class="fa fa-question-circle fa-4x media-object"></i>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading">How to upgrade the extension?</h4>
                        <p class="short-answer">Back up your system, disable the extension, overwrite the current extension files with new ones and click Upgrade on the extension settings page. After upgrade is complete enable the extension again.</p>

                        <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#upgrade" data-parent="#faqs">Show the full answer</button>
                        <div class="collapse full-answer" id="upgrade">
                          <ol>
                            <li>
                              <p><strong>Back up your system</strong> before making any upgrades or changes.</p>
                              <p><span class="label label-info">Note</span> Although <?php echo $ext_name; ?> does not overwrite any OpenCart core files, it is always a good practice to create a system backup before making any changes to the system.</p>
                            </li>
                            <li><strong>Disable</strong> <?php echo $ext_name; ?> <strong>extension</strong> on the module settings page (<em>Extensions > Modules > <?php echo $ext_name; ?></em>) by changing <em>Extension status</em> setting to "Disabled".</li>

                            <li>
                              <p><strong>Copy</strong> the <strong>new files</strong> from the <em>FILES TO UPLOAD</em> directory <strong>to the root directory you have installed OpenCart in</strong> overwriting any files that already exist.</p>
                              <p><span class="label label-info">Note</span> Do not worry, no core files will be replaced! Only the previously installed <?php echo $ext_name; ?> files will be overwritten.</p>
                              <p><span class="label label-danger">Important</span> If you have done custom modifications to the extension (for example customized it for your theme) and you don't want to overwrite all of the extension files, please take a look at the changelog file. You can copy only these files which have been changed since your last update and merge the files you have made custom modifications to.</p>
                            </li>

                            <li>
                              <p><strong>Open</strong> the <?php echo $ext_name; ?> <strong>module settings page</strong> <small>(<em>Extensions > Modules > <?php echo $ext_name; ?></em>)</small> and <strong>refresh the page</strong> by pressing <em>Ctrl + F5</em> twice to force the browser to update the css changes.</p>
                            </li>

                            <li><p>You should see a notice stating that new version of extension files have been found. <strong>Upgrade the extension</strong> by clicking on the 'Upgrade' button.</p></li>

                            <li>After the extension has been successfully upgraded <strong>enable the extension</strong> by changing <em>Extension status</em> setting to "Enabled".</li>
                          </ol>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="tab-pane" id="services">
                  <h3>Premium Services</h3>
                  <div id="service-container">
                    <p data-bind="visible: services().length == 0">There are currently no available services for this extension.</p>
                    <table class="table table-hover">
                      <tbody data-bind="foreach: services">
                        <tr class="srvc">
                          <td>
                            <h4 class="service" data-bind="html: name"></h4>
                            <span class="help-block">
                              <p class="description" data-bind="visible: description != '', html: description"></p>
                              <p data-bind="visible: turnaround != ''"><strong>Turnaround time</strong>: <span class="turnaround" data-bind="html: turnaround"></span></p>
                              <span class="hidden code" data-bind="html: code"></span>
                            </span>
                          </td>
                          <td class="nowrap text-right top-pad"><span class="currency" data-bind="html: currency"></span> <span class="price" data-bind="html: price"></span></td>
                          <td class="text-right"><button type="button" class="btn btn-sm btn-primary purchase"><i class="fa fa-shopping-cart"></i> Buy Now</button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="about">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#about-navbar-collapse">
                  <span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <h3 class="panel-title"><i class="fa fa-info fa-fw"></i> <?php echo $tab_about; ?></h3>
              </div>
              <div class="collapse navbar-collapse" id="about-navbar-collapse">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="#ext_info" data-toggle="tab"><?php echo $tab_extension; ?></a></li>
                  <li><a href="#changelog" data-toggle="tab"><?php echo $tab_changelog; ?></a></li>
                </ul>
              </div>
            </div>
            <div class="panel-body">
              <div class="tab-content">
                <div class="tab-pane active" id="ext_info">
                  <div class="row">
                    <div class="col-sm-12">
                      <h3><?php echo $text_extension_information; ?></h3>

                      <div class="form-group">
                        <label class="col-sm-3 col-md-2 control-label label-normal"><?php echo $entry_extension_name; ?></label>
                        <div class="col-sm-9 col-md-10">
                          <p class="form-control-static"><?php echo $ext_name; ?></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 col-md-2 control-label label-normal"><?php echo $entry_installed_version; ?></label>
                        <div class="col-sm-9 col-md-10">
                          <p class="form-control-static"><strong><?php echo $installed_version; ?></strong></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 col-md-2 control-label label-normal"><?php echo $entry_extension_compatibility; ?></label>
                        <div class="col-sm-9 col-md-10">
                          <p class="form-control-static"><?php echo $ext_compatibility; ?></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 col-md-2 control-label label-normal"><?php echo $entry_extension_store_url; ?></label>
                        <div class="col-sm-9 col-md-10">
                          <p class="form-control-static"><a href="<?php echo $ext_store_url; ?>" target="_blank"><?php echo htmlspecialchars($ext_store_url); ?></a></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 col-md-2 control-label label-normal"><?php echo $entry_copyright_notice; ?></label>
                        <div class="col-sm-9 col-md-10">
                          <p class="form-control-static">&copy; 2011 - <?php echo date("Y"); ?> Romi Agar</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-10">
                          <p class="form-control-static"><a href="view/static/bull5i_qa_extension_terms.htm" id="legal_notice" data-modal="#legal_text"><?php echo $text_terms; ?></a></p>
                        </div>
                      </div>

                      <h3 class="page-header"><?php echo $text_license; ?></h3>
                      <p><?php echo $text_license_text; ?></p>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="changelog">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="release">
                        <h3>Version 1.7.5 <small class="release-date text-muted">03 Oct 2014</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-success">Fixed:</em> Question filtering in the admin panel</li>
                          </ul>

                          <h4>Files changed:</h4>

                          <ul>
                            <li>admin/controller/module/qa.php</li>
                            <li>admin/model/catalog/qa.php</li>
                            <li>admin/view/template/module/qa.tpl</li>
                            <li>vqmod/xml/questions_and_answers.xml</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.7.4 <small class="release-date text-muted">15 Apr 2014</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-success">Fixed:</em> Popup display on admin Q &amp; A list page</li>
                            <li><em class="text-primary">New:</em> Changelog on module settings page under About tab</li>
                          </ul>

                          <h4>Files changed:</h4>

                          <ul>
                            <li>admin/controller/catalog/qa.php</li>
                            <li>admin/controller/module/qa.php</li>
                            <li>admin/language/english/module/qa.php</li>
                            <li>admin/model/module/qa.php</li>
                            <li>admin/view/template/catalog/qa_form.tpl</li>
                            <li>admin/view/template/catalog/qa_list.tpl</li>
                            <li>admin/view/template/module/qa.tpl</li>
                            <li>vqmod/xml/questions_and_answers.xml</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.7.3 <small class="release-date text-muted">04 Feb 2014</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-success">Fixed:</em> Autocomplete not working on PHP versions &lt; 5.4.0</li>
                          </ul>

                          <h4>Files changed:</h4>

                          <ul>
                            <li>admin/controller/module/qa.php</li>
                            <li>system/helper/qa.php</li>
                            <li>vqmod/xml/questions_and_answers.xml</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.7.2 <small class="release-date text-muted">01 Feb 2014</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-success">Fixed:</em> Captcha validation fails even if not required</li>
                          </ul>

                          <h4>Files changed:</h4>

                          <ul>
                            <li>admin/controller/module/qa.php</li>
                            <li>vqmod/xml/questions_and_answers.xml</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.7.1 <small class="release-date text-muted">31 Jan 2014</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-success">Fixed:</em> Multiple identical alerts displayed in admin</li>
                            <li><em class="text-success">Fixed:</em> Database upgrade when upgrading from version &lt; 1.7.0</li>
                          </ul>

                          <h4>Files changed:</h4>

                          <ul>
                            <li>admin/controller/catalog/qa.php</li>
                            <li>admin/controller/module/qa.php</li>
                            <li>admin/model/catalog/qa.php</li>
                            <li>vqmod/xml/questions_and_answers.xml</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.7.0 <small class="release-date text-muted">28 Jan 2014</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-primary">New:</em> Revamped module admin interface</li>
                            <li><em class="text-primary">New:</em> Configurable question fields</li>
                            <li><em class="text-primary">New:</em> Optional phone and a custom field</li>
                            <li><em class="text-primary">New:</em> Multi-store support</li>
                            <li><em class="text-primary">New:</em> Customer language based notification addresses</li>
                            <li><em class="text-primary">New:</em> Option to receive new question notification from customer email address</li>
                          </ul>

                          <h4>Files changed:</h4>

                          <ul>
                            <li>admin/controller/catalog/qa.php</li>
                            <li>admin/controller/module/qa.php</li>
                            <li>admin/language/english/catalog/qa.php</li>
                            <li>admin/language/english/mail/question_reply.php</li>
                            <li>admin/language/english/module/qa.php</li>
                            <li>admin/model/catalog/qa.php</li>
                            <li>admin/view/static/bull5i_qa_extension_terms.htm</li>
                            <li>admin/view/template/catalog/qa_form.tpl</li>
                            <li>admin/view/template/catalog/qa_list.tpl</li>
                            <li>admin/view/template/module/qa.tpl</li>
                            <li>catalog/language/english/mail/new_question.php</li>
                            <li>catalog/model/catalog/qa.php</li>
                            <li>catalog/view/theme/default/template/mail/new_question.tpl</li>
                            <li>catalog/view/theme/default/template/product/qa.tpl</li>
                            <li>vqmod/xml/questions_and_answers.xml</li>
                          </ul>

                          <h4>Files added:</h4>

                          <ul>
                            <li>admin/model/module/qa.php</li>
                            <li>admin/view/javascript/qa/*</li>
                            <li>admin/view/stylesheet/qa/*</li>
                            <li>catalog/view/theme/default/stylesheet/qa.css</li>
                            <li>system/helper/qa.php</li>
                          </ul>

                          <h4>Files removed:</h4>

                          <ul>
                            <li>admin/view/image/qa/*</li>
                            <li>admin/view/static/bull5i_qa_extension_help.htm</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.6.0 <small class="release-date text-muted">07 Apr 2013</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-primary">New:</em> Option to display questions in all languages or only current language</li>
                            <li><em class="text-success">Fixed:</em> HTML support in customer notification emails</li>
                            <li><em class="text-success">Fixed:</em> Store logo display in emails</li>
                          </ul>

                          <h4>Files changed:</h4>

                          <ul>
                            <li>admin/controller/catalog/qa.php</li>
                            <li>admin/controller/module/qa.php</li>
                            <li>admin/language/english/catalog/qa.php</li>
                            <li>admin/language/english/module/qa.php</li>
                            <li>admin/model/catalog/qa.php</li>
                            <li>admin/view/template/catalog/qa_form.tpl</li>
                            <li>admin/view/template/catalog/qa_list.tpl</li>
                            <li>admin/view/template/module/qa.tpl</li>
                            <li>catalog/model/catalog/qa.php</li>
                            <li>vqmod/xml/questions_and_answers.xml</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.5.0 <small class="release-date text-muted">14 Mar 2012</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-primary">New:</em> HTML is allowed in question answers</li>
                          </ul>

                          <h4>Files changed:</h4>

                          <ul>
                            <li>admin/controller/module/qa.php</li>
                            <li>admin/model/catalog/qa.php</li>
                            <li>vqmod/xml/questions_and_answers.xml</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.4.2 <small class="release-date text-muted">14 Mar 2012</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-success">Fixed:</em> Email validation when no reply was needed</li>
                          </ul>

                          <h4>Files changed:</h4>

                          <ul>
                            <li>admin/controller/module/qa.php</li>
                            <li>vqmod/xml/questions_and_answers.xml</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.4.1 <small class="release-date text-muted">17 Nov 2011</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-success">Fixed:</em> Links to the shop in customer notification emails</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.4.0 <small class="release-date text-muted">15 Nov 2011</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-primary">New:</em> Option to configure the number of questions displayed per page</li>
                            <li><em class="text-primary">New:</em> Option to improve SEO by preloading first page or all questions without AJAX</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.3.2 <small class="release-date text-muted">30 Sep 2011</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-success">Fixed:</em> Minor bugs</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.3.1 <small class="release-date text-muted">16 Sep 2011</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-success">Fixed:</em> Error messages when extension was not installed, but core files were modified</li>
                            <li><em class="text-primary">New:</em> First vQmod release</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.3.0 <small class="release-date text-muted">28 Jul 2011</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-primary">New:</em> Customer notification email when question has been answered</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.2.0 <small class="release-date text-muted">22 Apr 2011</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-primary">New:</em> New question notification email</li>
                            <li><em class="text-primary">New:</em> Easier installation</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.1.0 <small class="release-date text-muted">23 Jan 2011</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li><em class="text-primary">New:</em> Configuration options</li>
                          </ul>
                        </blockquote>
                      </div>

                      <div class="release">
                        <h3>Version 1.0.0 <small class="release-date text-muted">22 Jan 2011</small></h3>

                        <blockquote>
                          <ul class="list-unstyled">
                            <li>Initial release</li>
                          </ul>
                        </blockquote>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript"><!--
!function(e,t){e.texts=t.extend({},e.texts,{error_ajax_request:"<?php echo addslashes($error_ajax_request); ?>"}),t("#legal_text .modal-body").load("view/static/bull5i_qa_extension_terms.htm"),window.prettyPrint&&prettyPrint();var n=function(){o.service_list_loaded()||t.when(t.ajax({url:"http<?php echo $ssl; ?>://www.opencart.ee/services/",data:{eid:"<?php echo $ext_id; ?>",info:!0,general:!0},dataType:"jsonp"})).then(function(e){o.service_list_loaded(!0),e.services&&t.each(e.services,function(e,t){var n=t.code,i=t.name,r=t.description||"",s=t.currency,c=t.price,a=t.turnaround;o.addService(n,i,r,s,c,a)}),e.rate&&t("#hourly_rate").html(e.rate)},function(e,t,n){window.console&&window.console.log&&window.console.log("Failed to load services list: "+n)})};t("body").on("click","a.external-tab-link",function(e){var n=t(this).attr("data-target");e.preventDefault(),t("[href="+n+"]").trigger("click")}).on("click","button.purchase",function(){var e=t(this).closest("tr.srvc"),n=e.find(".service").text(),i=e.find(".code").text(),r=e.find(".currency").text(),o=parseFloat(e.find(".price").text());n&&i&&r&&o&&window.open("https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=support@opencart.ee&item_name="+n+"&item_number="+i+"&amount="+o+"&currency_code="+r+"&button_subtype=services&no_note=0&bn=OCEE_BuyNow_WPS_EE")}).on("shown.bs.tab","a[data-toggle='tab'][href='#support']",function(){n()});var i=function(e,t,n,i,r,o){this.code=e,this.name=t,this.description=n,this.currency=i,this.price=r,this.turnaround=o},r=function(){var e=this;e.service_list_loaded=ko.observable(!1),e.services=ko.observableArray([]),e.addService=function(t,n,r,o,s,c){e.services.push(new i(t,n,r,o,s,c))}},o=new r;ko.applyBindings(o,t("#service-container")[0])}(window.bull5i=window.bull5i||{},jQuery);
//--></script>
<?php echo $footer; ?>
