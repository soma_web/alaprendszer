<?php
// Text
$_['text_subject']              = '[%s] Kosárban maradt termékek';
$_['text_hi']                   = 'Kedves <strong>%s</strong>!';
$_['text_info']                 = 'Észrevettük, hogy a következő termékeket helyezted kosaradba, de a vásárlás folyamata megakadt:';
$_['text_cart_content']         = '<strong>Vásárlókosarad tartalmát itt láthatod:</strong>';
$_['text_view_product_detail']  = 'részletek megtekintése';
$_['text_coupon']               = 'Hogy lásd mennyire értékeljük vásárlásod, adunk neked egy különleges kupont, <strong><u>amennyiben rendelsz a következő %s napban:</u></strong>';
$_['text_coupon_code']          = 'Kupon kód: <strong>%s</strong>';
$_['text_coupon_notice']        = '** A kupon felhasználásához kérjük, jelentkezz be!';
$_['text_order_now']            = 'Kosarad újbóli megtekintéséhez <a href="%s" target="blank">jelentkezz be itt</a>!';
$_['text_thanks']               = '<strong>Köszönettel</strong>, <br />%s<br />%s<br />%s<br />%s';

// part mail 2
$_['text_info2']                = 'Nem vagyunk benne biztosak, hogy megkaptad előző üzenetünket. <br /><br /> Néhány nappal ezelőtt, elindítottál egy megrendelést, de valahogy nem tudtad befejezni, nem jutottál el a megerősítésig.<br />';
$_['text_coupon2']              = 'Úgy döntöttünk, hogy adunk Neked egy másik különleges kupon, <strong><u>amennyiben rendelsz a következő %s napban:</u></strong>';
$_['text_shop_methods']         = 'Két módja van annak, hogy teljesítsd megrendelésed kevesebb, mint két perc alatt:<br /><ul><li>Kattints erre a linkre, jelentkezz be és folytasd a vásárlást: <a href="%s">Bejelentkezés itt!</a></li><li> Vedd fel a kapcsolatot velünk telefonon: %s. Szívesen válaszolunk minden kérdésedre.</li></ul>';
$_['text_need_help']            = 'Kérjük, tudasd velünk, ha más módon rendelkezésedre állhatunk.';


// summary log for admin

$_['text_log_subject']              = 'Elhagyott kosár - jelentés';
$_['text_log_mail_sent_to']         = 'Emlékeztetőt küldtünk a következő vásárlóknak: ';
$_['text_log_mail_no_coupon']       = 'KUPON NÉLKÜL (Ok: Eddig %s kupont küldtünk ki.)';
$_['text_log_no_customers']         = 'Egy vásárlónak sincs elhagyott kosara.';
$_['text_reminder_sent_to']         = 'Emlékeztetőt küldünk: ';
$_['text_without_coupon']           = ' KUPON NÉLKÜL (Ok: Eddig ';
$_['text_email_sender']             = 'Elhagyott Kosár Emlékeztető';
$_['text_forgot_secret_code']       = 'Ön elfelejtette megadni a titkos kódját';
$_['text_access_denied_secret_code']= 'Hozzáférés megtagadva: Rossz titkos kód';
$_['text_sent_coupon']              = ' kupont küldtünk ki.) <br />';
$_['text_no_customer_to_alert']     = 'Jelenleg nincs szükség egy vásárló értesítésére sem. <br />';

?>