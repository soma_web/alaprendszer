<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">

<div style="width: 680px;"><a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-bottom: 20px; border: none;" /></a>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_hi; ?></p>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_info2; ?></p>
  <p style="margin-top: 20px; margin-bottom: 5px;"><?php echo $text_cart_content; ?></p>
  
  <table style="border-collapse: collapse; width: 100%; border-top: 1px dotted #DDDDDD;  margin-bottom: 20px;">
    <tbody>
      <?php foreach ($products as $product) { ?>
      <tr>
        <td style="font-size: 12px; text-align: left; border-bottom: 1px dotted #DDDDDD; padding: 7px; width: 55px;"><a href="<?php echo $product['href']; ?>" target="blank"><img src="<?php echo $product['image']; ?>" /></a></td>
        <td style="font-size: 12px;	text-align: left; border-bottom: 1px dotted #DDDDDD; padding: 7px;"><?php echo $product['quantity']; ?> X <a href="<?php echo $product['href']; ?>" target="blank"><?php echo $product['name']; ?></a></td>
        <td style="font-size: 12px;	text-align: left; border-bottom: 1px dotted #DDDDDD; padding: 7px;">[ <a href="<?php echo $product['href']; ?>" target="blank"><?php echo $text_view_product_detail; ?></a> ]</td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  
  <?php if ($allow_coupon) { ?>
  <p style="margin-top: 30px;"><?php echo $text_coupon2; ?></p> 
  <p style="margin-top: 10px; font-size: 14px;"><?php echo $text_coupon_code; ?></p> 
  <p style="margin-top: 3px; font-size: 10px;"><?php echo $text_coupon_notice; ?></p> 
  <?php } ?>

  <p style="margin-top: 20px; margin-bottom: 20px;"><?php echo $text_shop_methods; ?></p>
  <p style="margin-top: 10px; margin-bottom: 20px;"><?php echo $text_need_help; ?></p>
  
  <p style="margin-top: 10px;"><?php echo $text_thanks; ?></p>
</div>
</body>
</html>
