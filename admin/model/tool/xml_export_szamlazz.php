<?php
class ModeltoolXmlExportSzamlazz extends Model {

    public function szamlazzellenor(){
        $sql = "SELECT * FROM ".DB_PREFIX."szamlazzhu WHERE order_id='".$this->request->get['order_id']."'";
        $query = $this->db->query($sql);

        if($query->num_rows > 0){
            $this->update_szamlazz($this->request->get['order_id']) ;
        }
        else{
            $this->insert_szamlazz($this->request->get['order_id']);
        }
    }

    public function update_szamlazz($order_id){
        $sql = "UPDATE ".DB_PREFIX."szamlazzhu SET ";
        $sql .= "order_id='".$this->request->get['order_id']."',";
        foreach($this->request->get["xml_szamlazz"] as $nev => $value){
            $sql .= $nev."='".$value."',";
        }
        $sql = substr($sql, 0, -1);
        $sql .= " WHERE order_id='".$order_id."'";
        $this->db->query($sql);
    }

    public function insert_szamlazz($order_id){
        $sql = "INSERT INTO ".DB_PREFIX."szamlazzhu SET ";
        $sql .= "order_id='".$this->request->get['order_id']."',";
        foreach($this->request->get["xml_szamlazz"] as $nev => $value){
            if($value != ""){
                $sql .= $nev."='".$value."',";
            }
        }
        $sql = substr($sql, 0, -1);
        $this->db->query($sql);
    }
}
?>