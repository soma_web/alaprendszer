<?php
class ControllerCheckoutShippingMethod extends Controller {
    public function index() {
        $this->language->load('checkout/checkout');

        $this->load->model('account/address');

        if ($this->customer->isLogged() && isset($this->session->data['shipping_address_id'])) {
            $shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);
        } elseif (isset($this->session->data['guest'])) {
            $shipping_address = $this->session->data['guest']['shipping'];
        }

        if (!empty($shipping_address)) {

            // Totals
            $total_data = array();
            $total = 0;
            $taxes = $this->cart->getTaxes();

            $this->load->model('setting/extension');

            $sort_order = array();

            $results = $this->model_setting_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                }
            }

            // Shipping Methods


            $this->load->model('setting/extension');

            $fizetesek = $this->model_setting_extension->getExtensions('payment');


            $quote_data = array();

            $this->load->model('setting/extension');

            $results = $this->model_setting_extension->getExtensions('shipping');

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('shipping/' . $result['code']);

                    $quote = $this->{'model_shipping_' . $result['code']}->getQuote($shipping_address, $fizetesek);

                    if ($quote) {
                        if ($result['code'] == "weight" || $result['code'] == "weight_kp" ){
                            $query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "biztositas WHERE biztositas_tol <= $total AND biztositas_ig >= $total");

                            foreach($quote['quote'] as $key=>$value){
                                if ($quote['quote'][$key]['biztositas'] == 1) {
                                    $quote['quote'][$key]['cost']+=!empty($query1->row['biztositas_ertek']) ? $query1->row['biztositas_ertek'] : 0;

                                    $quote['quote'][$key]['text'] = $this->currency->format($this->tax->calculate($quote['quote'][$key]['cost'], $this->config->get('weight_tax_class_id'), $this->config->get('config_tax')));
                                    $quote['quote'][$key]['netto_text'] = $this->currency->format($quote['quote'][$key]['cost']);
                                }

                            }
                        }

                        $quote_data[$result['code']] = array(
                            'title'             => $quote['title'],
                            'quote'             => $quote['quote'],
                            'fizetesi_mod'      => $quote['fizetesi_mod'],
                            'szallitasi_mod'    => isset($quote['szallitasi_mod']) ? $quote['szallitasi_mod'] : "",
                            'sort_order'        => $quote['sort_order'],
                            'warehouse'         => isset($quote['warehouse_quote']) ? $quote['warehouse_quote'] : false,
                            'error'             => $quote['error']
                        );
                    }
                }
            }

            if (isset($quote_data['item_product'])) {
                if ( isset($quote_data['item_product']['szallitasi_mod']) ) {
                    $szallitasi = array();
                    foreach($quote_data['item_product']['szallitasi_mod'] as $szallitas) {
                        $szallitasi[] = $szallitas['szallitasi_mod'];
                    }
                    foreach($quote_data as $key=>$szallitasok) {
                        if ( !in_array($key,$szallitasi) && $key != "item_product") {
                            unset($quote_data[$key]);
                        }
                    }
                }
            }

            if (isset($quote_data['free'])) {
                if ( isset($quote_data['free']['szallitasi_mod']) ) {
                    $szallitasi = array();
                    foreach($quote_data['free']['szallitasi_mod'] as $szallitas) {
                        $szallitasi[] = $szallitas['szallitasi_mod'];
                    }
                    foreach($quote_data as $key=>$szallitasok) {
                        if ( !in_array($key,$szallitasi) && $key != "free") {
                            unset($quote_data[$key]);
                        }
                    }
                }
            }

            if (isset($quote_data['item_product_gls'])) {
                if ( isset($quote_data['item_product_gls']['szallitasi_mod']) ) {
                    $szallitasi = array();
                    foreach($quote_data['item_product_gls']['szallitasi_mod'] as $szallitas) {
                        $szallitasi[] = $szallitas['szallitasi_mod'];
                    }
                    foreach($quote_data as $key=>$szallitasok) {
                        if ( !in_array($key,$szallitasi) && $key != "item_product_gls") {
                            unset($quote_data[$key]);
                        }
                    }
                }
            }

            $sort_order = array();

            foreach ($quote_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $quote_data);

            $this->session->data['shipping_methods'] = $quote_data;
        }

        $this->data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $this->data['text_gls_csomagpontok'] = $this->language->get('text_gls_csomagpontok');
        if ($this->config->get('megjelenit_penztar_fizetesi_mod') == 0){
            $this->data['text_shipping_method'] = $this->language->get('text_shipping_method_no_payment');
        }
        $this->data['text_comments'] = $this->language->get('text_comments');
        $this->data['text_brutto'] = $this->language->get('text_brutto');
        $this->data['text_netto'] = $this->language->get('text_brutto');

        $this->data['button_continue'] = $this->language->get('button_continue');

        if (empty($this->session->data['shipping_methods'])) {
            $this->data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
        } else {
            $this->data['error_warning'] = '';
        }

        if(isset($this->session->data['shipping_address']['city_new']) && $this->session->data['shipping_address']['city_new']) {
            $this->data['shipping_address_city'] = $this->session->data['shipping_address']['city_new'];
        } else if(!empty($this->session->data['shipping_address']['city'])) {
            $this->data['shipping_address_city'] = $this->session->data['shipping_address']['city'];
        }

        if (isset($this->session->data['shipping_methods'])) {
            $this->data['shipping_methods'] = $this->session->data['shipping_methods'];
        } else {
            $this->data['shipping_methods'] = array();
        }

        if (isset($this->session->data['shipping_method']['warehouse_id'])) {
            $this->data['melyik_uzlet'] = $this->session->data['shipping_method']['warehouse_id'];
        } else {
            $this->data['melyik_uzlet'] = '';
        }

        if (isset($this->session->data['shipping_method']['code'])) {
            $this->data['code'] = $this->session->data['shipping_method']['code'];
        } else {
            $this->data['code'] = '';
        }


        if (isset($this->session->data['comment'])) {
            $this->data['comment'] = $this->session->data['comment'];
        } else {
            $this->data['comment'] = '';
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/shipping_method.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/checkout/shipping_method.tpl';
        } else {
            $this->template = 'default/template/checkout/shipping_method.tpl';
        }

        $this->response->setOutput($this->render());
    }

    public function storeGLS() {
        $gls['gls_shop_id']     = json_encode($_POST['shopid']);
        $gls['gls_zipcode']     = json_encode($_POST['zipcode']);
        $gls['gls_name']        = json_encode($_POST['name']);
        $gls['gls_city']        = json_encode($_POST['city']);
        $gls['gls_address']     = json_encode($_POST['address']);

        if(isset($gls['gls_shop_id']) && !empty($gls['gls_shop_id'])) {
            $this->session->data['shipping_method']['gls']['gls_shop_id'] = $gls['gls_shop_id'];
        }

        if(isset($gls['gls_zipcode']) && !empty($gls['gls_zipcode'])) {
            $this->session->data['shipping_method']['gls']['gls_zipcode'] = $gls['gls_zipcode'];
        }

        if(isset($gls['gls_name']) && !empty($gls['gls_name'])) {
            $this->session->data['shipping_method']['gls']['gls_name'] = $gls['gls_name'];
        }

        if(isset($gls['gls_city']) && !empty($gls['gls_city'])) {
            $this->session->data['shipping_method']['gls']['gls_city'] = $gls['gls_city'];
        }

        if(isset($gls['gls_address']) && !empty($gls['gls_address'])) {
            $this->session->data['shipping_method']['gls']['gls_address'] = $gls['gls_address'];
        }
    }

    public function validate() {
        $this->language->load('checkout/checkout');
        $gls_pont = false;
        if (isset($this->request->request['gls_pont']) && $this->request->request['gls_pont']) {
            $gls_pont = explode(",",$this->request->request['gls_pont']);
        }

        $json = array();

        // Validate if shipping is required. If not the customer should not have reached this page.
        if (!$this->cart->hasShipping()) {
            $json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
        }

        // Validate if shipping address has been set.
        $this->load->model('account/address');

        if ($this->customer->isLogged() && isset($this->session->data['shipping_address_id'])) {
            $shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);
        } elseif (isset($this->session->data['guest'])) {
            $shipping_address = $this->session->data['guest']['shipping'];
        }

        if (empty($shipping_address)) {
            $json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
        }

        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            $json['redirect'] = $this->url->link('checkout/cart');
        }

        // Validate minimum quantity requirments.
        $products = $this->cart->getProducts();

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $json['redirect'] = $this->url->link('checkout/cart');

                break;
            }
        }

        if (!$json) {
            if (!isset($this->request->post['shipping_method'])) {
                $json['error']['warning'] = $this->language->get('error_shipping');
            } else {
                $shipping = explode('.', $this->request->post['shipping_method']);

                if ($this->config->get($shipping[0] . '_status')) {
                    $this->load->model('shipping/' . $shipping[0]);
                    if(method_exists($this->{'model_shipping_' . $shipping[0]}, 'validate')) {
                        $valid = $this->{'model_shipping_' . $shipping[0]}->validate();
                        if($valid)
                            $json['error']['warning'] = $valid;
                    }
                }


                if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
                    $json['error']['warning'] = $this->language->get('error_shipping');
                }
            }

            if (isset($this->request->post['shipping_method']) && !empty($this->request->post['shipping_method'])) {
                if ($this->gls_vizsgalat($gls_pont,$this->request->post)) { // ha true, akkor jöhet a warning
                    $json['error']['warning'] = $this->language->get('error_gls_nincs');
                }
            }

            if (!$json) {
                $shipping = explode('.', $this->request->post['shipping_method']);

                $this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
                if ($this->session->data['shipping_method']['code'] == "pickup.pickup") {
                    if (isset($this->request->request['uzlet']) && $this->request->request['uzlet']) {
                        $this->session->data['shipping_method']['warehouse_id'] = $this->request->request['uzlet'];
                    }
                }
                if ($gls_pont) {
                    $this->session->data['shipping_method']['gls_pont'] = $gls_pont;
                }
                $this->session->data['comment'] = strip_tags($this->request->post['comment']);
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    public function validateMegrendelem() {
        $this->language->load('checkout/checkout');

        $gls_pont = false;
        if (isset($this->request->request['gls_pont']) && $this->request->request['gls_pont']) {
            $gls_pont = explode(",",$this->request->request['gls_pont']);
        }

        $json = array();

        // Validate if shipping is required. If not the customer should not have reached this page.
        if (!$this->cart->hasShipping("megrendelem")) {
            $json['redirect'] = $this->url->link('checkout/checkout_megrendelem', '', 'SSL');
        }

        // Validate if shipping address has been set.
        $this->load->model('account/address');

        if ($this->customer->isLogged() && isset($this->session->data['shipping_address_id'])) {
            $shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);
        } elseif (isset($this->session->data['guest'])) {
            $shipping_address = $this->session->data['guest']['shipping'];
        }

        if (empty($shipping_address)) {
            $json['redirect'] = $this->url->link('checkout/checkout_megrendelem', '', 'SSL');
        }

        // Validate cart has products and has stock.
        if ( empty($this->session->data['megrendelem']) )  {
            $json['redirect'] = $this->url->link('checkout/cart');
        }

        // Validate minimum quantity requirments.
        $products = $this->cart->getProducts(false,false,false,"megrendelem");

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $json['redirect'] = $this->url->link('checkout/cart');

                break;
            }
        }

        if (!$json) {
            if (!isset($this->request->post['shipping_method'])) {
                $json['error']['warning'] = $this->language->get('error_shipping');
            } else {
                $shipping = explode('.', $this->request->post['shipping_method']);

                if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
                    $json['error']['warning'] = $this->language->get('error_shipping');
                }
            }

            if (isset($this->request->post['shipping_method']) && !empty($this->request->post['shipping_method'])) {
                if ($this->gls_vizsgalat($gls_pont,$this->request->post)) { // ha true, akkor jöhet a warning
                    $json['error']['warning'] = $this->language->get('error_gls_nincs');
                }
            }

            if (!$json) {
                $shipping = explode('.', $this->request->post['shipping_method']);

                $this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
                if ($this->session->data['shipping_method']['code'] == "pickup.pickup") {
                    if (isset($this->request->request['uzlet']) && $this->request->request['uzlet']) {
                        $this->session->data['shipping_method']['warehouse_id'] = $this->request->request['uzlet'];
                    }
                }
                if ($gls_pont) {
                    $this->session->data['shipping_method']['gls_pont'] = $gls_pont;
                }
                $this->session->data['comment'] = strip_tags($this->request->post['comment']);
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    public function gls_vizsgalat($gls_pont, $post) {
        $shipping_m_array = explode('.',$post['shipping_method']);
        $shipping_method = $shipping_m_array[0]."_gls_csomagpont";
        $gls_status = $this->config->get($shipping_method);
        if(!isset($gls_status) || $gls_status != 1) {
            return false;
        } else if(!$gls_pont) {
            return true;
        } else {
            return false;
        }
    }
}
?>