<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>

    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

<style>
    #content > form td {
        min-width: inherit;
    }

    #content > form table.termek {
        width: 100%;
        border-bottom: 1px solid #ccc;
    }

    #content > form table.termek thead td {
        padding: 7px;
    }

    #content > form table.termek tr td {
        text-align: right;
    }
    table.list td {
        border-bottom: none;
    }

    #content > form {
        padding: 0;
    }

    .fejlec {
        display: table;
        width: 100%;
        background: #eee;
        padding: 10px;
        margin: 0 0 10px 0;
    }
    .fejlec_oszlop {
        display: table-cell;
    }

    .cimke {
        font-size: 14px;
        color: #555;
    }
    .ertek {
        font-size: 15px;
        color: #e60000;
    }
    .right {
        text-align: right;
    }
    .odd {
        background: #E2E9E6;
    }

    .arajanlat_modosit input {
        height: 20px;
        background: #f7f7f7;
        margin: 0 0 3px 0;

    }

    .arajanlat_modosit td img {
        margin: 10px 0 0 0;
    }

    .arajanlat_modosit td.listaar {
        font-weight: bold;
    }
    .small {
        font-size: 11px;
    }
    .arajanlat_modosit td input.product_price_color {
        background: #A5F3A8;
    }
    #customer_name {
        text-decoration: underline;
        cursor: pointer;
    }


    #kozepe_hatternek .kozepe #content a:hover {
        color: #010;
    }

    table.list .right {
        padding: 0;
    }

    .ertek.ajanlat-adatok {
        color: #333;
        text-decoration: underline;
        cursor: pointer;
        font-weight: bold;
        font-size: 14px;
    }
    .ertek.ajanlat-adatok:hover {
        color: #010;
    }
    .ajanlat_fejlec_adatai{
        display: table;
        width: 100%;
    }

    .arajanlat_szallito {
        border: 1px solid #ddd;
        padding: 10px;
    }

    .arajanlat_szallito input {
        width: 250px;
        background: #fdfdfd;
    }


    .arajanlat_szallito.szallito {
        margin-right: 20px;
    }


    .tabla_sor {
        display: table-row;
    }
    .tabla_oszlop {
        display: table-cell;
        border-top: 1px solid #ddd;
    }
    .szam_kelt_mod {
        margin: 15px 0;
    }
    #ajanlat_fejlec {
        background: #F9FFFD;
        padding: 10px 10px;
        margin-bottom: 10px;
    }
    .bekezdes {
        padding-left: 15px;
    }

    .szelesseg {
        width: 25%;
        display: inline-table;
    }
    .ceg_neve {
        font-weight: bold;
    }


</style>

<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

    <h1><img style="width: 20px"  src="catalog/view/theme/default/image/arajanlat_icon.png" alt="" /> <?php echo $heading_modosit; ?></h1>

    <div style="text-align:right; margin-bottom:10px;">
        <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>&nbsp;
        <!--<a onclick="generalPdf()" class="button"><?php echo $button_pdf; ?></a>&nbsp;-->
        <!--<a href="<?php echo $action_pdf; ?>" class="button"><?php echo $button_pdf; ?></a>&nbsp;-->
        <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
    </div>

    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="arajanlat_modosit">



        <div class="fejlec">
            <div class="fejlec_oszlop">
                <span class="cimke"><?php echo $column_azonosito;?>:</span>
                <span class="ertek"><?php echo $arajanlat_info['arajanlat_id']?></span>
            </div>

            <div class="fejlec_oszlop">
                <span class="cimke"><?php echo $column_name?>:</span>
                <input type="text" name="ajanlat_neve" class="ertek" style="height: 25px; text-align: center;" value="<?php echo $arajanlat_info['name']?>">
            </div>

            <div class="fejlec_oszlop">
                <span class="cimke"><?php echo $column_date_added?>:</span>
                <span class="ertek"><?php echo $arajanlat_info['date_added']?></span>
            </div>

            <div class="fejlec_oszlop">
                <span class="cimke"><?php echo $column_date_modified?>:</span>
                <span class="ertek"><?php echo $arajanlat_info['date_modified']?></span>
            </div>
        </div>

        <div class="fejlec">
            <div class="fejlec_oszlop">
                <span onclick="arajanlatFejlec()" class="ertek ajanlat-adatok"><?php echo $column_arajanlat_keszito_adatai;?></span>
            </div>

        </div>

        <div id="ajanlat_fejlec" style="display: none">
            <div class="ajanlat_fejlec_adatai">
                <div class="fejlec_oszlop">
                    <div><b><?php echo $fejlec_szallito?></b></div>
                    <div class="arajanlat_szallito szallito">
                        <div class="ceg_neve"><input name="szallito_neve" type="text" value="<?php echo $arajanlat_info['szallito_neve'];?>"></div>
                        <div><?php echo $fejlec_szekhely?></div>
                        <div class="bekezdes"><input name="szallito_varos" type="text" value="<?php echo $arajanlat_info['szallito_varos'];?>"></div>
                        <div class="bekezdes"><input name="szallito_utca" type="text" value="<?php echo $arajanlat_info['szallito_utca'];?>"></div>
                        <div class="bekezdes"><input name="szallito_orszag" type="text" value="<?php echo $arajanlat_info['szallito_orszag'];?>"></div>
                        <div>
                            <span>&nbsp</span>
                            <span>&nbsp</span>
                        </div>
                        <div>
                            <span class="szelesseg"><?php echo $fejlec_adoszam?></span>
                            <input  name="szallito_adoszam" type="text" value="<?php echo $arajanlat_info['szallito_adoszam'];?>">
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_bank?></span>
                            <input  name="szallito_bank" type="text" value="<?php echo $arajanlat_info['szallito_bank'];?>">
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_bank_szamla?></span>
                            <input  name="szallito_bankszamla_szam" type="text" value="<?php echo $arajanlat_info['szallito_bankszamla_szam'];?>">
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_swift_kod?></span>
                            <input  name="szallito_bankszamla_swift" type="text" value="<?php echo $arajanlat_info['szallito_bankszamla_swift'];?>">
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_iban_kod?></span>
                            <input  name="szallito_bankszamla_iban" type="text" value="<?php echo $arajanlat_info['szallito_bankszamla_iban'];?>">
                        </div>
                    </div>
                </div>

                <div class="fejlec_oszlop">
                    <div><b><?php echo $fejlec_vevo?></b></div>
                    <div class="arajanlat_szallito">
                        <div  class="ceg_neve"><input  name="vevo_neve" type="text" value="<?php echo $arajanlat_info['vevo_neve'];?>"></div>
                        <div><?php echo $fejlec_szekhely?></div>
                        <div class="bekezdes"><input  name="vevo_varos" type="text" value="<?php echo $arajanlat_info['vevo_varos'];?>"></div>
                        <div class="bekezdes"><input  name="vevo_utca" type="text" value="<?php echo $arajanlat_info['vevo_utca'];?>"></div>
                        <div class="bekezdes"><input  name="vevo_orszag" type="text" value="<?php echo $arajanlat_info['vevo_orszag'];?>"></div>
                        <div>
                            <span>&nbsp</span>
                            <span>&nbsp</span>
                        </div>

                        <div>
                            <span class="szelesseg"><?php echo $fejlec_adoszam?></span>
                            <input  name="vevo_adoszam" type="text" value="<?php echo $arajanlat_info['vevo_adoszam'];?>">
                        </div>
                         <div>
                            <span class="szelesseg"><?php echo $fejlec_bank?></span>
                            <input  name="vevo_bank" type="text" value="<?php echo $arajanlat_info['vevo_bank'];?>">
                         </div>

                         <div>
                            <span class="szelesseg"><?php echo $fejlec_bank_szamla?></span>
                            <input  name="vevo_bankszamla_szam" type="text" value="<?php echo $arajanlat_info['vevo_bankszamla_szam'];?>">
                        </div>
                        <div>
                            <span>&nbsp</span>
                            <span>&nbsp</span>
                        </div>
                        <div>
                            <span>&nbsp</span>
                            <span>&nbsp</span>
                        </div>
                    </div>
                </div>

            </div>

            <div class="ajanlat_fejlec_adatai szam_kelt_mod">
                <div class="tabla_sor">
                    <div class="tabla_oszlop"><?php echo $fejlec_ajanlat_szam?>

                    </div>
                    <div class="tabla_oszlop"><?php echo $fejlec_ajanlat_kelt?>

                    </div>
                    <div class="tabla_oszlop"><?php echo $fejlec_szallitasi_hatarido?>

                    </div>
                    <div class="tabla_oszlop"><?php echo $fejlec_fizetesi_hatarido?>

                    </div>
                    <div class="tabla_oszlop"><?php echo $fejlec_fizetesi_mod?>

                    </div>
                </div>
                <div class="tabla_sor">
                    <div class="tabla_oszlop" style="font-weight: bold">
                        <?php echo 'AJ'.$arajanlat_info['arajanlat_id'];?>
                    </div>
                    <div class="tabla_oszlop">
                        <input  name="ajanlat_kelte" type="text" size="8" class="date" value="<?php echo $arajanlat_info['ajanlat_kelte'];?>">
                    </div>
                    <div class="tabla_oszlop">
                        <input  name="ajanlat_szallitasi_hatarido" type="text" value="<?php echo $arajanlat_info['ajanlat_szallitasi_hatarido'];?>">
                    </div>
                    <div class="tabla_oszlop">
                        <input  name="ajanlat_fizetesi_hatarido" type="text" value="<?php echo $arajanlat_info['ajanlat_fizetesi_hatarido'];?>">
                    </div>
                    <div class="tabla_oszlop">
                        <input  name="ajanlat_fizetesi_mod" type="text" value="<?php echo $arajanlat_info['ajanlat_fizetesi_mod'];?>">
                    </div>
                </div>
            </div>

            <div class="ajanlat_fejlec_adatai szam_kelt_mod">
                <div class="tabla_sor">
                    <div class="tabla_oszlop"><?php echo $fejlec_kuldo?>

                    </div>
                    <div class="tabla_oszlop"><?php echo $fejlec_telefon?>

                    </div>
                    <div class="tabla_oszlop"><?php echo $fejlec_email?>

                    </div>
                </div>
                <div class="tabla_sor">
                    <div class="tabla_oszlop">
                        <input  name="ajanlat_kuldo_neve" type="text" value="<?php echo $arajanlat_info['ajanlat_kuldo_neve'];?>">
                    </div>
                    <div class="tabla_oszlop">
                        <input  name="ajanlat_kuldo_telefon" type="text" value="<?php echo $arajanlat_info['ajanlat_kuldo_telefon'];?>">
                    </div>
                    <div class="tabla_oszlop">
                        <input  name="ajanlat_kuldo_email" type="text" value="<?php echo $arajanlat_info['ajanlat_kuldo_email'];?>">
                    </div>
                </div>
            </div>

            <div>
                <input  name="ajanlat_megszolitas" style="width:100%; margin-top: 10px ; height: 28px" type="text" value="<?php echo $arajanlat_info['ajanlat_megszolitas'];?>">
            </div>
            <div>
                <input  name="ajanlat_altalanos_szoveg_felul" style="width:100%; margin: 5px 0 0 0; height: 28px" type="text" value="<?php echo $arajanlat_info['ajanlat_altalanos_szoveg_felul'];?>">
            </div>

            <div style="margin-top: 30px; font-weight: bold">Lábléc</div>
            <textarea style="width: 100%; min-height: 100px" name="ajanlat_altalanos_szoveg_alul"><?php echo $arajanlat_info['ajanlat_altalanos_szoveg_alul'];?></textarea>
        </div>




        <div class="fejlec">
            <div class="fejlec_oszlop">
                <span class="cimke"><?php echo $column_customer?>:</span>
                <span id="customer_name" class="ertek ajanlat-adatok" style="" onclick="vevoCserehet()"><?php echo $arajanlat_info['customer_name']?></span>
                <input type="hidden" name="vevo_azonosito" value="<?php echo $arajanlat_info['customer_id']?>">
                <div id="vevo_cserel_span" style="display: none">
                    <input type="text" id="vevo_cserel"  value="" >
                </div>
            </div>

            <div class="fejlec_oszlop right">
                <span class="cimke"><?php echo $column_customer_email?></span>
                <span id="customer_email" class="ertek"><?php echo $arajanlat_info['email']?></span>
            </div>
        </div>


        <?php if (isset($arajanlat_info['products']) && $arajanlat_info['products']) { ?>

            <table class="termek list">
                <thead>
                    <tr>
                        <td><?php echo $column_kep?></td>
                        <td><?php echo $column_nev?></td>
                        <td><?php echo $column_model?></td>
                        <td><?php echo $column_listaar?></td>
                        <td style="width: 1%; min-width: 70px;">
                            <a id="altalanos_szazalek_felirat" style="text-decoration: underline; cursor: pointer"
                               onclick="altalanosSzazalek()">
                                <?php echo $column_engedmeny_szazalek?>
                            </a><br>
                            <span id="altalanos_szazalek_ertek" style="display: none">
                                <input type="text"  value="" class="right" style="width: 58px" oninput="$('.szazalek_ertek').val(this.value); teljesAtszamol()">
                            </span>

                        </td>
                        <td><?php echo $column_engedmeny_ertek?></td>
                        <td><?php echo $column_mennyiseg?></td>

                        <td><?php echo $column_ertek?></td>
                    </tr>
                </thead>
                <tbody>
                <?php $class = 'even'; ?>

                <?php foreach($arajanlat_info['products'] as $product) { ?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>

                        <tr class="<?php echo $class?>">
                            <td><img src="<?php echo $product['image']?>" ></td>

                            <td><b><?php echo $product['name']?></b>
                                <?php $van_option = false;?>
                                <?php $van_valamilyen_option = false;?>

                                <?php if (isset($product['options']) && $product['options'] ) { ?>
                                    <?php foreach($product['options'] as $option) { ?>

                                        <?php if (isset($option['value']) && $option['value']) { ?>
                                            <?php $van_valamilyen_option = true;; ?>

                                            <?php foreach($option['value'] as $value) { ?>
                                                <?php if (!$value['mennyiseg']) { ?>
                                                    <?php $van_option = true;?>

                                                    <br>
                                                    <?php echo $option['option_name'].': '.$value['option_value_name']?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($van_option) { ?>
                                    <br>
                                    <br>
                                    <br>
                                <?php } ?>
                            </td>

                            <td><?php echo $product['model']?></td>

                            <td><b><?php echo $this->currency->format($product['price'])?></b>
                                <?php $ar=(int)$product['price']?>

                                <?php if (isset($product['options']) && $product['options'] ) { ?>
                                    <?php foreach($product['options'] as $option) { ?>

                                        <?php if (isset($option['value']) && $option['value']) { ?>
                                            <?php foreach($option['value'] as $value) { ?>
                                                <?php if (!$value['mennyiseg']) { ?>
                                                    <br>
                                                    <?php echo $value['price'] ? $value['price_prefix'].' '.$this->currency->format($value['price']) : ''?>
                                                    <?php $ar +=$value['price']?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($van_option) { ?>
                                    <br>
                                    <br>
                                    <br>
                                <?php } ?>

                            </td>

                            <td>
                                <input id="szazalek-<?php echo $product['product_id']?>" class="right szazalek_ertek" style="width: 60px" type="text" name="product_szazalek-<?php echo $product['product_id']?>"
                                       value="<?php echo $product['szazalek']?>"
                                       oninput="productAtszamol('szazalek','<?php echo $product['product_id']?>','<?php echo $ar?>',$('#mennyiseg-<?php echo $product['product_id']?>').val())">
                            </td>
                            <td><input id="egysegar-<?php echo $product['product_id']?>"  class="right" style="width: 70%"
                                        type="text" name="product_egysegar-<?php echo $product['product_id']?>"
                                        value="<?php echo round($product['egysegar'],1)?>"
                                        oninput="productAtszamol('egysegar','<?php echo $product['product_id']?>','<?php echo $ar?>',$('#mennyiseg-<?php echo $product['product_id']?>').val())">
                            </td>
                            <td><input type="text"  id='mennyiseg-<?php echo $product['product_id']?>' style="width: 40px" class="right" name="product_mennyiseg-<?php echo $product['product_id']?>"
                                       value="<?php echo $product['mennyiseg']?>"
                                       oninput="productAtszamol('egysegar','<?php echo $product['product_id']?>','<?php echo $ar?>',$('#mennyiseg-<?php echo $product['product_id']?>').val())">
                                <?php echo $product['mennyiseg'] ? ' '.$product['megyseg'] : ''?>
                            </td>

                            <td>
                                <input id="ertek-<?php echo $product['product_id']?>" class="right <?php echo !$van_valamilyen_option ? 'product_price_color' : ''?> ertek-<?php echo $product['product_id']?>"
                                       readonly style="width: 70%" type="text"
                                       name="product_ertek-<?php echo $product['product_id']?>" value="<?php echo $this->currency->format($product['ertek'])?>">
                            </td>
                        </tr>

                        <?php $van_option = false;?>

                        <?php if (isset($product['options']) && $product['options'] ) { ?>
                            <?php foreach($product['options'] as $option) { ?>

                                <?php if (isset($option['value']) && $option['value']) { ?>
                                    <?php foreach($option['value'] as $value) { ?>
                                        <?php if ($value['mennyiseg']) { ?>
                                            <?php $van_option = true;?>

                                            <tr class="<?php echo $class?>">
                                                <td></td>
                                                <td><?php echo $option['option_name'].': '.$value['option_value_name']?></td>
                                                <td></td>

                                                <?php if ($value['price'] > 0) { ?>
                                                    <td><?php echo $value['price_prefix'].' '.$this->currency->format($value['price'])?></td>
                                                    <td><input id="option_szazalek-<?php echo $value['arajanlat_option_value_id']?>" class="right szazalek_ertek" style="width: 60px" type="text" name="option_value_szazalek-<?php echo $value['arajanlat_option_value_id']?>"
                                                               value="<?php echo $value['szazalek']?>"
                                                               oninput="productAtszamol('option_szazalek',
                                                                                       '<?php echo $product['product_id']?>',
                                                                                       '<?php echo $value['price']?>',
                                                                                       $('#mennyiseg_option-<?php echo $value['arajanlat_option_value_id']?>').val(),
                                                                                       '<?php echo $value['price_prefix']?>',
                                                                                       '<?php echo $value['arajanlat_option_value_id']?>')">
                                                    </td>
                                                    <td><input id="option_egysegar-<?php echo $value['arajanlat_option_value_id']?>"  class="right" style="width: 70%" type="text" name="option_value_egysegar-<?php echo $value['arajanlat_option_value_id']?>"
                                                               value="<?php echo round($value['egysegar'],1)?>"
                                                               oninput="productAtszamol('option_egysegar',
                                                                                        '<?php echo $product['product_id']?>',
                                                                                        '<?php echo $value['price']?>',
                                                                                        $('#mennyiseg_option-<?php echo $value['arajanlat_option_value_id']?>').val(),
                                                                                        '<?php echo $value['price_prefix']?>',
                                                                                        '<?php echo $value['arajanlat_option_value_id']?>')">

                                                    </td>
                                                <?php } else { ?>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                <?php } ?>

                                                <td><span class="small">
                                                        <input type="text"  id='mennyiseg_option-<?php echo $value['arajanlat_option_value_id']?>' style="width: 40px" class="right"
                                                               name="option_mennyiseg-<?php echo $value['arajanlat_option_value_id']?>"
                                                               value="<?php echo $value['mennyiseg']?>"
                                                               oninput="productAtszamol('option_szazalek',
                                                                   '<?php echo $product['product_id']?>',
                                                                   '<?php echo $value['price']?>',
                                                                   $('#mennyiseg_option-<?php echo $value['arajanlat_option_value_id']?>').val(),
                                                                   '<?php echo $value['price_prefix']?>',
                                                                   '<?php echo $value['arajanlat_option_value_id']?>')">

                                                        <span ><?php echo ' '.$product['megyseg']; ?></span>
                                                    </span>
                                                </td>
                                                <?php if ($value['price'] > 0) { ?>
                                                    <td><input id="option_ertek-<?php echo $value['arajanlat_option_value_id']?>" class="right ertek-<?php echo $product['product_id']?>" readonly
                                                               style="width: 70%"
                                                               type="text"
                                                               name="option_value_ertek-<?php echo $value['arajanlat_option_value_id']?>"
                                                               value="<?php echo $this->currency->format($value['ertek'])?>"></td>
                                                <?php } else { ?>
                                                    <td></td>
                                                <?php } ?>
                                            </tr>

                                    <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>

                        <?php if ($van_option) { ?>
                            <tr class="<?php echo $class?>">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <div style="margin-right: 30px"><?php echo $column_ar_osszesen?>:</div>
                                    <div style="margin-bottom: 5px"><input readonly  class="right product_price_color product-ertek-<?php echo $product['product_id']?>"
                                                                      style="width: 70%"
                                                                      type="text" name="" value="<?php echo $this->currency->format($product['product_ertek_osszesen'])?>"></div>
                                </td>
                            </tr>
                        <?php } ?>
                        <input  class="right rejtett_ertek rejtett-product-ertek-<?php echo $product['product_id']?>" type="hidden" name="product_arosszesen-<?php echo $product['product_id']?>" value="<?php echo $product['product_ertek_osszesen']?>">

                <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <div style="margin-right: 30px"><?php echo $column_total_osszesen?>:</div>
                            <input class="right rejtett-total-ertek" type="hidden" name="total-ertek_osszesen" value="<?php echo $arajanlat_info['total']?>">
                            <div><input readonly class="right total-ertek product_price_color" style="width: 70%" type="text" name="" value="<?php echo $this->currency->format($arajanlat_info['total'])?>"></div>
                        </td>
                    </tr>
            </table>
        <?php } ?>

    </form>

    <div style="text-align:right; margin-top:10px;">
        <a onclick="$('#form').submit();;" class="button"><?php echo $button_save; ?></a>&nbsp;
        <!--<a onclick="generalPdf()" class="button"><?php echo $button_pdf; ?></a>&nbsp;-->
        <!--<a href="<?php echo $action_pdf; ?>" class="button"><?php echo $button_pdf; ?></a>&nbsp;-->
        <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
    </div>
    <?php echo $content_bottom; ?>
</div>


<script>
    function productAtszamol(para,product_id,listaar,mennyiseg,prefix,option_value_id) {
        if (para == 'egysegar') {
            var szazalek = (1-($('#egysegar-'+product_id).val()/listaar))*100;
            var ertek = $('#egysegar-'+product_id).val()*mennyiseg;

            $('#szazalek-'+product_id).val(Math.round(szazalek*10) /10);
            $('#ertek-'+product_id).val(price_format(ertek));
        }

        if (para == 'szazalek') {
            var egysegar = listaar * (100-$('#szazalek-'+product_id).val()) /100;

            $('#egysegar-'+product_id).val(Math.round(egysegar*100) /100);
            $('#ertek-'+product_id).val(price_format(Math.round($('#egysegar-'+product_id).val()*mennyiseg)));
        }


        if (para == 'option_szazalek') {
            debugger;
            if (prefix=="+") {
                var egysegar = listaar * (100-$('#option_szazalek-'+option_value_id).val()) /100;
            } else {
                var egysegar = (listaar * $('#option_szazalek-'+option_value_id).val()/100 + listaar*1)*-1;
            }

            $('#option_egysegar-'+option_value_id).val(Math.round(egysegar*100) /100);
            $('#option_ertek-'+option_value_id).val(price_format($('#option_egysegar-'+option_value_id).val()*mennyiseg));
        }

        if (para == 'option_egysegar') {
            if (prefix=="+") {
                var szazalek = (1-($('#option_egysegar-'+option_value_id).val()/listaar))*100;
            } else {
                var kulonbseg = -1*$('#option_egysegar-'+option_value_id).val() - listaar;
                var szazalek = kulonbseg /listaar*100;
            }
            var ertek = $('#option_egysegar-'+option_value_id).val()*mennyiseg;

            $('#option_szazalek-'+option_value_id).val(Math.round(szazalek*10) /10);
            $('#option_ertek-'+option_value_id).val(price_format(ertek));
        }

        product_osszesen = 0;
        $('.ertek-'+product_id).each(function(){
            debugger;
            product_osszesen +=(this.value.replace(/[^0-9,-]/g,'')*1);
        });

        $('.product-ertek-'+product_id).val(price_format(product_osszesen));
        $('.rejtett-product-ertek-'+product_id).val(product_osszesen);
        total = 0;
        $('.rejtett_ertek').each(function(){
            total += this.value*1;
        });
        $('.rejtett-total-ertek').val(total);
        $('.total-ertek').val(price_format(total));


    }

    function altalanosSzazalek() {
        if ($('#altalanos_szazalek_ertek').css('display') == 'none') {
            $('#altalanos_szazalek_ertek').show(1000,function(){
                $('#altalanos_szazalek_ertek input').focus();
            });
        } else {
            $('#altalanos_szazalek_ertek').hide(1000);
        }
    }

    function teljesAtszamol() {
        $('.szazalek_ertek').each(function(e,b){
            var jellemzok = $(this).attr("oninput");
            var jellemzok = jellemzok.substr(jellemzok.indexOf('(')+1);


            var jellemzok = jellemzok.replace(/[^A-Za-z0-9.,-:+_]/g, "");



            var jellemzok = jellemzok.split(',');
            debugger;
            var mennyiseg = jellemzok[3].replace('.val','');

            if (jellemzok[0] == 'szazalek') {
                productAtszamol('szazalek',jellemzok[1],jellemzok[2],$('#'+mennyiseg).val());
            } else {
                productAtszamol(jellemzok[0],jellemzok[1],jellemzok[2],$('#'+mennyiseg).val(),jellemzok[4],jellemzok[5]);

            }
        });
    }

    function vevoCserehet() {
        if ($('#vevo_cserel_span').css("display") == "none") {
            $('#vevo_cserel_span').show('1000',function(){
                $('#vevo_cserel').focus();
            });
        } else {
            $('#vevo_cserel_span').hide('1000');
        }

    }


    function generalPdf() {
        debugger;
        $.ajax({
            url: 'index.php?route=account/arajanlat/toPdf&arajanlat_id=<?php echo $arajanlat_info['arajanlat_id']?>',
            dataType: 'json',
            success: function(json) {

            }
        });
    }

    $('#vevo_cserel').autocomplete({
        delay: 0,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=account/arajanlat/autocompleteCustomer&customer_name=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.name+' :  - '+item.email,
                            value: item.name,
                            customer_id: item.customer_id,
                            email: item.email,
                            vevo_varos:   item.vevo_varos,
                            vevo_utca:    item.vevo_utca,
                            vevo_orszag:  item.vevo_orszag,
                            vevo_adoszam: item.vevo_adoszam,
                            ajanlat_megszolitas: item.ajanlat_megszolitas
                        }
                    }));
                }
            });
        },
        select: function(event, ui) {
            $('#vevo_cserel').val('');
            $('input[name=\'vevo_azonosito\']').val(ui.item.customer_id);
            $('#customer_name').html(ui.item.value);
            $('#customer_email').html(ui.item.email);
            $('#vevo_cserel_span').hide('1000');
            $('input[name="vevo_neve"]').val(ui.item.value);
            $('input[name="vevo_varos"').val(ui.item.vevo_varos);
            $('input[name="vevo_utca"').val(ui.item.vevo_utca);
            $('input[name="vevo_orszag"').val(ui.item.vevo_orszag);
            $('input[name="vevo_adoszam"').val(ui.item.vevo_adoszam);
            $('input[name="ajanlat_megszolitas"').val(ui.item.ajanlat_megszolitas);

            return false;
        }
    });

    function arajanlatFejlec() {
        if ($('#ajanlat_fejlec').css('display') == "none") {
            $('#ajanlat_fejlec').show("slow");
        } else {
            $('#ajanlat_fejlec').hide("slow");
        }
    }
</script>

    <script type="text/javascript" src="catalog/view/javascript/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace('ajanlat_altalanos_szoveg_alul', {
        filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
    });
    $('.date').datepicker({dateFormat: 'yy-mm-dd'});

</script>


<?php echo $footer; ?>