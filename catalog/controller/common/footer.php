<?php  
class ControllerCommonFooter extends Controller {
	protected function index() {
        $this->log->setMicroTime();

        $this->language->load('module/category');
        $this->data['megjelenit_altalanos'] = $this->config->get("megjelenit_altalanos");
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $this->data['footer_popup'] = (isset($this->data['megjelenit_altalanos']['footer_popup']) && $this->data['megjelenit_altalanos']['footer_popup'] == 1) ? $this->data['megjelenit_altalanos']['footer_popup'] : false;

        $aruhaz_id = $this->config->get('config_store_id') ? $this->config->get('config_store_id') : '';
        $nyelv_kod = $this->config->get('config_language_id');
        $nyelv_kod .= $aruhaz_id ? '_'.$aruhaz_id : '';
        $this->data['categories'] = array();


        if (isset($_SESSION['categoryAll']) && empty($_GET['csere'])) {
            $this->data['categories'] = $_SESSION['categoryAll'];
        } else {
            $this->data['categories'] = $this->cache->get('category_all.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));

            if (!$this->data['categories'] || !empty($_GET['csere'])) {
                $this->data['categories'] = array();
                $this->load->model('module/multi_category');

                $arr2 = $this->model_module_multi_category->getCategoriesAll(0);
                $categoria_tomb = $this->model_module_multi_category->buildTree($arr2, 0, "");
                $categoria_tomb = $this->model_module_multi_category->onlyFindProduct($categoria_tomb);

                $this->data['categories'] = $this->model_module_multi_category->categorySort($categoria_tomb);
                $this->cache->set('category_all.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $this->data['categories']);
            }
            $_SESSION['categoryAll'] = $this->data['categories'];
        }

        /*if ($this->config->get('megjelenit_footer_kategoria')) {
            $categories = $this->model_catalog_category->getCategories(0);

            foreach ($categories as $category) {
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {
                    $data = array(
                        'filter_category_id'  => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name'        => $child['name'],
                        'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }

                $data = array(
                    'filter_category_id'  => $category['category_id'],
                    'filter_sub_category' => true
                );

                $this->data['categories'][] = array(
                        'category_id' => $category['category_id'],
                        'name'        => $category['name'],
                        'children'    => $children_data,
                        'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );
            }
        }*/

		$this->language->load('common/footer');
		
		$this->data['text_information'] = $this->language->get('text_information');
		$this->data['text_service'] = $this->language->get('text_service');
		$this->data['text_message'] = $this->language->get('text_message');
		$this->data['text_extra'] = $this->language->get('text_extra');
		$this->data['text_submit'] = $this->language->get('text_submit');
		$this->data['text_email'] = $this->language->get('text_email');
		$this->data['text_content'] = $this->language->get('text_content');
		$this->data['text_contact'] = $this->language->get('text_contact');
		$this->data['text_address'] = $this->language->get('text_address');
		$this->data['text_return'] = $this->language->get('text_return');
    	$this->data['text_sitemap'] = $this->language->get('text_sitemap');
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$this->data['text_voucher'] = $this->language->get('text_voucher');
		$this->data['text_affiliate'] = $this->language->get('text_affiliate');
		$this->data['text_special'] = $this->language->get('text_special');
		$this->data['text_account'] = $this->language->get('text_account');
		$this->data['text_order'] = $this->language->get('text_order');
		$this->data['text_wishlist'] = $this->language->get('text_wishlist');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');
        $this->data['text_config_name'] = $this->config->get('config_name');
        $this->data['text_config_cim'] = $this->config->get('config_address');
        $this->data['text_config_owner'] = $this->config->get('config_owner');
        $this->data['text_config_telefon'] = $this->config->get('config_telephone');
        $this->data['text_kedvezmeny'] = $this->language->get('text_kedvezmeny');
        $this->data['text_name'] = $this->language->get('text_name');
        $this->data['text_cookie_figyelmeztetes'] = $this->language->get('text_cookie_figyelmeztetes');
        $this->data['text_pricelist'] = $this->language->get('text_pricelist');


        $this->data['pricelist'] = $this->url->link('product/pricelist', '', 'SSL');

        $this->load->model('catalog/information');

        $this->data['terkep']                           = html_entity_decode($this->config->get('config_terkep'.$aruhaz_id));
        $this->data['tg_helios_footer_terkep_szelesseg']= $this->config->get("tg_helios_footer_terkep_szelesseg".$aruhaz_id);
        $this->data['tg_helios_footer_terkep_magassag'] = $this->config->get("tg_helios_footer_terkep_magassag".$aruhaz_id);
        $this->data['tg_helios_footer_facebook_gomb']   = html_entity_decode($this->config->get('tg_helios_footer_facebook_gomb'.$aruhaz_id));
        $this->data['tg_helios_footer_facebook_box']    = html_entity_decode($this->config->get('tg_helios_footer_facebook_box'.$aruhaz_id));
        if($this->config->get('sidebar_code_status'.$aruhaz_id) == '1') {
            $this->data['sidebar_code'] = html_entity_decode($this->config->get('sidebar_code'.$nyelv_kod));
        }


        $van_facebook = $this->config->get('config_facebook'.$aruhaz_id);
        if (!empty($van_facebook))
    		$this->data['facebook_doboz'] = html_entity_decode($van_facebook);
        else
            $this->data['facebook_doboz'] = false;

		$this->data['contact'] = $this->url->link('information/contact');
		$this->data['return'] = $this->url->link('account/return/insert', '', 'SSL');
    	$this->data['sitemap'] = $this->url->link('information/sitemap');
		$this->data['manufacturer'] = $this->url->link('product/manufacturer');
		$this->data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$this->data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$this->data['special'] = $this->url->link('product/special');
		$this->data['account'] = $this->url->link('account/account', '', 'SSL');
		$this->data['order'] = $this->url->link('account/order', '', 'SSL');
		$this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		$this->data['kedvezmeny'] = $this->url->link('information/information','information_id=9', 'SSL');

        $this->data['powered'] = sprintf($this->language->get('text_powered'),  date('Y', time()), $this->config->get('config_name'));


        /*if ( !in_array($this->config->get('config_store_id'),$this->config->get("tg_helios_footer_stores")) ) {
            $this->config->set('tg_helios_footer_status',0);
        }*/
        $this->data['tg_helios_footer_status'] = $this->config->get('tg_helios_footer_status'.$aruhaz_id);


        require_once(DIR_APPLICATION . 'controller/module/tg_helios_footer.php');

        $helios_footer = new ControllerModuletgheliosfooter($this);
        $helios_footer->index($this);

        $this->data['footer_beallitasok'] = $helios_footer->footer_beallitasok;
        $this->data['footer_default'] = $helios_footer->footer_default;

        $sorrend = array();

        if ($this->config->get('tg_helios_footer_informaciok_status'.$aruhaz_id) == 1) {
            $informations = array(
                'title' => $this->language->get('text_information')
            );
            foreach ($this->model_catalog_information->getInformations() as $result) {
                $informations['adatok'][] = array(
                    'title' => $result['title'],
                    'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
            $sorrend[] = array(
                'fuggveny' => 'alap_informaciok',
                'sorrend' => $this->config->get('tg_helios_footer_informaciok_status_sorrend'.$aruhaz_id),
                'adatok' => $informations
            );
        }


        if ($this->config->get('tg_helios_footer_ugyfelszol_status'.$aruhaz_id) == 1) {
            $informations = array();
            $informations['adatok'] = array(
                'title' => $this->config->get('ugyfelszol_title' . $nyelv_kod),
                'href' => $this->config->get('ugyfelszol_code' . $nyelv_kod)
            );
            $sorrend[] = array(
                'fuggveny' => 'alap_ugyfelszol',
                'sorrend' => $this->config->get('tg_helios_footer_ugyfelszol_status_sorrend'.$aruhaz_id),
                'adatok' => $informations

            );
        }

        if ($this->config->get('tg_helios_footer_extrak_status'.$aruhaz_id) == 1) {
            $informations = array();
            $informations['adatok'] = array(
                'title' => $this->config->get('extrak_title' . $nyelv_kod),
                'href' => $this->config->get('extrak_code' . $nyelv_kod)
            );
            $sorrend[] = array(
                'fuggveny' => 'alap_extrak',
                'sorrend' => $this->config->get('tg_helios_footer_extrak_status_sorrend'.$aruhaz_id),
                'adatok' => $informations
            );
        }

        if ($this->config->get('tg_helios_footer_fiok_status'.$aruhaz_id) == 1) {
            $informations = array();
            $informations['adatok'] = array(
                'title' => $this->config->get('fiok_title' . $nyelv_kod),
                'href' => $this->config->get('fiok_code' . $nyelv_kod)
            );
            $sorrend[] = array(
                'fuggveny' => 'alap_fiok',
                'sorrend' => $this->config->get('tg_helios_footer_fiok_status_sorrend'.$aruhaz_id),
                'adatok' => $informations

            );
        }

        if ($this->config->get('tg_helios_footer_uzenet_status'.$aruhaz_id) == 1) {
            $informations = array();
            $informations['adatok'] = array(
                'text_message' => $this->data['text_message'],
                'text_email' => $this->data['text_email'],
                'text_content' => $this->data['text_content'],
                'text_submit' => $this->data['text_submit']
            );

            $sorrend[] = array(
                'fuggveny' => 'alap_uzenet',
                'sorrend' => $this->config->get('tg_helios_footer_uzenet_status_sorrend'.$aruhaz_id),
                'adatok' => $informations

            );
        }

        if ($this->config->get('tg_helios_footer_hirlevel_status'.$aruhaz_id) == 1) {
            $informations = array();
            $informations['adatok'] = array(
                'text_name' => $this->data['text_name'],
                'text_email' => $this->data['text_email'],
                'text_submit' => $this->data['text_submit'],
                'text_newsletter' => $this->data['text_newsletter']
            );

            $sorrend[] = array(
                'fuggveny' => 'alap_hirlevel',
                'sorrend' => $this->config->get('tg_helios_footer_hirlevel_status_sorrend'.$aruhaz_id),
                'adatok' => $informations

            );
        }

        if ($this->config->get('tg_helios_footer_likebox_status'.$aruhaz_id) == 1) {
            $informations = array();
            $informations['adatok'] = array(
                'likebox_code' => $this->config->get('likebox_code')

            );

            $sorrend[] = array(
                'fuggveny' => 'alap_likebox',
                'sorrend' => $this->config->get('tg_helios_footer_likebox_status_sorrend'.$aruhaz_id),
                'adatok' => $informations

            );
        }

        if ($this->config->get('tg_helios_footer_elerhetoseg_status'.$aruhaz_id) == 1) {
            $informations = array();
            $informations['adatok'] = array(
                'title' => $this->config->get('elerhetoseg_title' . $nyelv_kod),
                'href' => $this->config->get('elerhetoseg_code' . $nyelv_kod)
            );
            $sorrend[] = array(
                'fuggveny' => 'alap_egyeb',
                'sorrend' => $this->config->get('tg_helios_footer_elerhetoseg_status_sorrend'.$aruhaz_id),
                'adatok' => $informations

            );
        }

        if ($this->config->get('tg_helios_footer_kategoriak_status'.$aruhaz_id) == 1) {
            $informations = array(
                'title' => $this->language->get('text_kategoriak'),
                'categories' => $this->data['categories']
            );
            foreach ($this->model_catalog_information->getInformations() as $result) {
                $informations['adatok'][] = array(
                    'title' => $result['title'],
                    'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
            $sorrend[] = array(
                'fuggveny' => 'alap_kategoriak',
                'sorrend' => $this->config->get('tg_helios_footer_kategoriak_status_sorrend'.$aruhaz_id),
                'adatok' => $informations
            );
        }

        /* SIDEBAR */
        /*if ($this->config->get('tg_helios_footer_sidebar_status'.$aruhaz_id) == 1) {
            $informations = array();
            $informations['adatok'] = array(
                'title' => $this->config->get('sidebar_title' . $nyelv_kod),
                'href' => $this->config->get('sidebar_code' . $nyelv_kod)
            );
        }*/

        $sortingSettings = array(
            0 => array(
                'orderby' => 'sorrend',
                'sortorder' => 'ASC'
            )
        );

        $rendezo = new ArrayOfArrays($sorrend);
        $rendezo->multiSorting($sortingSettings, true);
        $this->data['alap_footer'] = $rendezo->getArrayCopy();

        $this->data['cookie_domain'] = $this->rename_file(HTTP_SERVER);

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/footer.tpl';
		} else {
			$this->template = 'default/template/common/footer.tpl';
		}
        $this->log->GetMicroTime('Action - common/footer');


        $this->render();
	}
    public function sendemail(){
      $emailaddress = $this->request->get->emailcim;
      $message      = $this->request->get->message;


        $mail = new Mail();
        $mail->protocol  = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->hostname  = $this->config->get('config_smtp_host');
        $mail->username  = $this->config->get('config_smtp_username');
        $mail->password  = $this->config->get('config_smtp_password');
        $mail->port      = $this->config->get('config_smtp_port');
        $mail->timeout   = $this->config->get('config_smtp_timeout');
        $mail->setTo("sz.lehel@ideasol.hu");
        //$mail->setTo($this->config->get('config_email'));
        $mail->setFrom($emailaddress);
        $mail->setSender($this->config->get($emailaddress));
        $mail->setSubject("Kapcsolatfelvétel"); //html_entity_decode($subject, ENT_QUOTES, 'UTF-8')
        $mail->setText($message);
        $mail->send();
        return true;
    }

    public function rename_file($name){
        $name = str_replace('http','',$name);
        $name = str_replace('www','',$name);
        $name = preg_replace('/[^a-z0-9_ ]/i', '', $name);
        return $name;
    }


}

?>