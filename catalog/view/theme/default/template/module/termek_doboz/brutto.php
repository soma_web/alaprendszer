<?php
$_['brutto']          = array(
    'type'              => 'checkbox',
    'name'              => 'brutto',
    'default'           => "0",
    'heading'           => '"Bruttó" szöveg megjelenítés'
);

$_['null_off_brutto']          = array(
    'type'              => 'checkbox',
    'name'              => 'null_off_brutto',
    'default'           => "1",
    'heading'           => 'Nulla ne jelenjen meg'
);

?>