<?php
class Termek_Import {               /* SAP termékek segédtábla feldolgozása a:
                                        product
                                        product_description
                                        product_to_category
                                        category
                                        category_description
                                        manufacturer
                                        product_to_store
                                        category_to_store
                                        manufacturer_to_store
                                             táblákba.
                                    */

    public $csvadatok;
    public $product_azonosito;
    private $query;

    public function __construct($query) {
        $this->query = $query;
        $this->csvadatok=$this->query->query("SELECT * FROM sap_termekek");
    }

    public function product($sorok){
        $sap_cikkszam       = $sorok['cikkszam'];
        $m_datum            = $sorok['letrehozas_datuma'];
        $m_minimum          = $sorok['ertekesitesi_mennyiseg_min'];
        $m_megyseg          = $sorok['ertekesitesi_egyseg'];
        $m_keszlet          = $sorok['raktarkeszlet'];
        $m_gyartokod        = $sorok['gyarto_kodja'];

        $m_hossz            = $sorok['szallitasi_hossz'];
        $m_szelesseg	    = $sorok['szallitasi_szelesseg'];
        $m_magassag         = $sorok['szallitasi_magassag'];
        $m_meret_megyseg    = $sorok['szallitasi_meret_megyseg'];  // 1-náluk a cm kódja, nálunk is 1 a cm kódja

        $m_suly             = $sorok['szallitasi_suly'];
        $m_suly_megyseg     = 1;

        $m_kaphato          = $sorok['letrehozas_datuma'];
        $m_ara              = $sorok['ara'];
        $m_status           = $sorok['statusz'];

        $m_csom_egyseg      = $sorok['csomagolasi_egyseg'];     // varchar
        $m_csom_mennyiseg   = $sorok['csomagolasi_mennyiseg'];  // integer
        $m_darabaru_meret   = $sorok['darabaru_merete'];        // text



        $sql="SELECT * FROM ".DB_PREFIX."product WHERE cikkszam LIKE '$sap_cikkszam'";
        $query= $this->query->query($sql);

        if (isset($query->num_rows) && $query->num_rows > 0){
            $modosit = "UPDATE ".DB_PREFIX."product set
                          date_added            = '$m_datum',
                          minimum               = '$m_minimum',
                          megyseg               = '$m_megyseg',
                          quantity              = '$m_keszlet',
                          manufacturer_id       = '$m_gyartokod',
                          length                = '$m_hossz',
                          width                 = '$m_szelesseg',
                          height                = '$m_magassag',
                          length_class_id       = '$m_meret_megyseg',
                          weight                = '$m_suly',
                          weight_class_id       = '$m_suly_megyseg',
                          date_available        = '$m_kaphato',
                          status                = '$m_status',
                          price                 = '$m_ara',
                          csomagolasi_egyseg    = '$m_csom_egyseg',
                          stock_status_id       = '6',
                          csomagolasi_mennyiseg = '$m_csom_mennyiseg',
                          darabaru_meret        = '$m_darabaru_meret'
            WHERE cikkszam LIKE '$sap_cikkszam'";


        } else{
            $modosit="INSERT INTO ".DB_PREFIX."product set product_id = '',
                                                cikkszam            = '$sap_cikkszam',
                                                date_added          = '$m_datum',
                                                minimum             = '$m_minimum',
                                                megyseg             = '$m_megyseg',
                                                quantity            = '$m_keszlet',
                                                manufacturer_id     = '$m_gyartokod',
                                                length              = '$m_hossz',
                                                width               = '$m_szelesseg',
                                                height              = '$m_magassag',
                                                weight              = $m_suly,
                                                weight_class_id     = '$m_suly_megyseg',
                                                date_available      = '$m_kaphato',
                                                status              = '$m_status',
                                                price               = '$m_ara',
                                                subtract            = '0',
                                                stock_status_id       = '6',
                                                tax_class_id        = '11' ";

        }

        mysql_query($modosit) or Hibalista("Termékek import", "product", "felvitel", $sap_cikkszam);

        $sql="SELECT * FROM ".DB_PREFIX."product WHERE cikkszam LIKE '$sap_cikkszam'";
        $query= $this->query->query($sql);

        $this->product_azonosito= $query->row['product_id'];
        $this->product_description($sorok);
        $this->product_category($sorok);
        $this->manufacturer($sorok);
        $this->store_category($sorok);
        $this->store_product($sorok);
        $this->store_manufacturer($sorok);
    }


    public function product_description($sorok){
        $m_cikk_megnevezese   = $sorok['cikk_megnevezese'];
        $m_megjegyzes         = $sorok['megjegyzes'];

        $azonosito=$this->product_azonosito;

        for ($i=1; $i< 5; $i++ ){
            if ($i==1){
                $m_cikk_megnevezese = $sorok['cikk_idegen_neve_1'];
                $m_megjegyzes = $sorok['megjegyzes_idegen_neve_1'];
            }
            elseif ($i==2){
                $m_cikk_megnevezese = addslashes($sorok['cikk_megnevezese']);
                $m_megjegyzes = $sorok['megjegyzes'];
            }
            elseif ($i==3){
                $m_cikk_megnevezese = $sorok['cikk_idegen_neve_2'];
                $m_megjegyzes = $sorok['megjegyzes_idegen_neve_2'];
            }
            elseif ($i==4){
                $m_cikk_megnevezese = $sorok['cikk_idegen_neve_3'];
                $m_megjegyzes = $sorok['megjegyzes_idegen_neve_3'];
            }


            $sql="SELECT * FROM ".DB_PREFIX."product_description  WHERE product_id=$azonosito AND language_id=$i";
            $query=$this->query->query($sql);

            if (isset($query->num_rows) && $query->num_rows > 0){
                 $modosit = "UPDATE ".DB_PREFIX."product_description set
                                `name`      = '$m_cikk_megnevezese',
                                description = '$m_megjegyzes'
                    WHERE product_id=$this->product_azonosito AND language_id=$i";
                //mysql_query($modosit) or die("Nem sikerült a product_description tábla módosítása");
                mysql_query($modosit) or Hibalista("Termékek import", "product_description", "modositas", $azonosito);

             } else {

                $modosit="INSERT INTO ".DB_PREFIX."product_description SET
                                `product_id`    =   '$azonosito',
                                `language_id`   =   '$i',
                                `name`          =   '$m_cikk_megnevezese',
                                `description`   =   '$m_megjegyzes' ";
                mysql_query($modosit) or Hibalista("Termékek import", "product_description", "felvitel", $azonosito);
            }
        }
    }

    public function product_category($sorok){
        $m_cikkcsoport_kodja   = $sorok['cikkcsoport_kodja'];
        $m_cikkcsoport_neve    = $sorok['cikkcsoport_neve'];

        $azonosito=$this->product_azonosito;


        $sql="SELECT * FROM ".DB_PREFIX."category  WHERE category_id=$m_cikkcsoport_kodja";
        $query=$this->query->query($sql);

        $sql="SELECT * FROM ".DB_PREFIX."category_description  WHERE category_id=$m_cikkcsoport_kodja AND language_id=2";
        $query_category=$this->query->query($sql);

        $sql="SELECT * FROM ".DB_PREFIX."product_to_category  WHERE product_id=$azonosito AND category_id=$m_cikkcsoport_kodja";
        $query_procuct_category=$this->query->query($sql);

        if (isset($query->num_rows) && $query->num_rows > 0){
           if (isset($query_category->num_rows) && $query_category->num_rows > 0){
               $modosit = "UPDATE ".DB_PREFIX."category_description set
                             `name`      = '$m_cikkcsoport_neve'
               WHERE category_id=$m_cikkcsoport_kodja AND language_id=2";
               mysql_query($modosit) or Hibalista("Termékek import", "category_description", "módosítás", $m_cikkcsoport_kodja);
           } else {
               $modosit="INSERT INTO ".DB_PREFIX."category_description (category_id, language_id, name)
	                    VALUES( $m_cikkcsoport_kodja,
	                            2,
	                            '$m_cikkcsoport_neve' )";

               mysql_query($modosit) or Hibalista("Termékek import", "category_description", "felvitel", $m_cikkcsoport_kodja);
           }

        } else {
            $modosit="INSERT INTO ".DB_PREFIX."category (category_id, status)
	                VALUES( $m_cikkcsoport_kodja, 1 )";
            mysql_query($modosit) or Hibalista("Termékek import", "category", "felvitel", $m_cikkcsoport_kodja);

            if (isset($query_category->num_rows) && $query_category->num_rows > 0){
                $modosit = "UPDATE ".DB_PREFIX."category_description set
                                `name`      = '$m_cikkcsoport_neve'
                WHERE category_id=$m_cikkcsoport_kodja AND language_id=2";
                mysql_query($modosit) or  Hibalista("Termékek import", "category_description", "módosítás", $m_cikkcsoport_kodja);
            } else {
                $modosit="INSERT INTO ".DB_PREFIX."category_description (category_id, language_id, name)
	                  VALUES( $m_cikkcsoport_kodja,
	                          2,
	                        '$m_cikkcsoport_neve' )";

                mysql_query($modosit) or  Hibalista("Termékek import", "category_description", "felvitel", $m_cikkcsoport_kodja);
            }
        }

        //if (!isset($query_procuct_category->num_rows) || $query_procuct_category->num_rows == 0){
        if (!empty($m_cikkcsoport_kodja) ) {
            $torol = "DELETE FROM ".DB_PREFIX."product_to_category WHERE  product_id      = '$azonosito'";
            mysql_query($torol) or Hibalista("Termékek import", "product_to_category", "törlés", $azonosito, $m_cikkcsoport_kodja);

            $modosit="INSERT INTO ".DB_PREFIX."product_to_category SET
                        product_id      = '$azonosito',
                        category_id     = '$m_cikkcsoport_kodja' ";

            mysql_query($modosit) or Hibalista("Termékek import", "product_to_category", "felvitel", $azonosito, $m_cikkcsoport_kodja);
        //}
        }
    }


    public function manufacturer($sorok){
        $m_gyarto_kodja   = $sorok['gyarto_kodja'];
        $m_gyarto_neve    = $sorok['gyarto_neve'];
        $sql="SELECT * FROM ".DB_PREFIX."manufacturer  WHERE manufacturer_id=$m_gyarto_kodja";
        $query=$this->query->query($sql);
        if (isset($query->num_rows) && $query->num_rows > 0){
            $modosit = "UPDATE ".DB_PREFIX."manufacturer set
                                `name`      = '$m_gyarto_neve'
                                WHERE manufacturer_id=$m_gyarto_kodja";
            mysql_query($modosit) or Hibalista("Termékek import", "manufacturer", "módosítás", $m_gyarto_kodja);

        } else {
             $modosit="INSERT INTO ".DB_PREFIX."manufacturer SET
                         `manufacturer_id`  = '$m_gyarto_kodja',
                         `name`             = '$m_gyarto_neve'  ";

        }
        //mysql_query($modosit) or die("Nem sikerült a manufacturer insert");
        mysql_query($modosit) or Hibalista("Termékek import", "manufacturer", "felvitel", $m_gyarto_kodja);
    }

    /*---------------- Áruház beazonosítás --------------------*/
    public function store_category($sorok){

        $m_category_id	  = $sorok['cikkcsoport_kodja'];
	    $m_store_id       = 0;

        $sql="SELECT * FROM ".DB_PREFIX."category_to_store  WHERE category_id=$m_category_id AND store_id=0";
        $query=$this->query->query($sql);
        if (!isset($query->num_rows) || $query->num_rows == 0){
            $modosit="INSERT INTO ".DB_PREFIX."category_to_store SET
                                category_id = '$m_category_id',
                                store_id    = '$m_store_id' ";

            //mysql_query($modosit) or die("Nem sikerült a category_to_store insert");
            mysql_query($modosit) or Hibalista("Termékek import", "category_to_store", "felvitel", $m_category_id);
        }
    }
    public function store_product($sorok){
        $azonosito=$this->product_azonosito;
        $m_store_id       = 0;

        $sql="SELECT * FROM ".DB_PREFIX."product_to_store  WHERE product_id=$azonosito AND store_id=0";
        $query=$this->query->query($sql);
        if (!isset($query->num_rows) || $query->num_rows == 0){
            $modosit="INSERT INTO ".DB_PREFIX."product_to_store SET
                                product_id = '$azonosito',
                                store_id    = '$m_store_id' ";
            //mysql_query($modosit) or die("Nem sikerült a product_to_store insert");
            mysql_query($modosit) or Hibalista("Termékek import", "product_to_store", "felvitel", $azonosito);
        }
    }

    public function store_manufacturer($sorok){

        $m_manufacturer_id	  = $sorok['gyarto_kodja'];
        $m_store_id       = 0;

        $sql="SELECT * FROM ".DB_PREFIX."manufacturer_to_store  WHERE manufacturer_id=$m_manufacturer_id AND store_id=0";
        $query=$this->query->query($sql);
        if (!isset($query->num_rows) || $query->num_rows == 0){
            $modosit="INSERT INTO ".DB_PREFIX."manufacturer_to_store SET
                                manufacturer_id = '$m_manufacturer_id',
                                store_id    = '0' ";

            //mysql_query($modosit) or die("Nem sikerült a manufacturer_to_store insert");
            mysql_query($modosit) or Hibalista("Termékek import", "manufacturer_to_store", "felvitel", "cikkszám:".$sorok['cikkszam']. "  Hiba:".$m_manufacturer_id);
        }
    }


}