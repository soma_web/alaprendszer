<?php
class ControllerMyocPricelist extends Controller {
	public function index() {
        if($this->config->get('myocwpl_login') && !$this->customer->isLogged()) {
   			$this->session->data['redirect'] = $this->url->link('myoc/pricelist', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
        }
        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
        $this->data['kaphato'] = $this->language->get('kaphato');
        $this->language->load('myoc/pricelist');
        $this->data['module_name'] = "pricelist";

        $this->document->setTitle($this->language->get('heading_title'));

		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('myoc/pricelist'),
        	'separator' => $this->language->get('text_separator')
      	);

		$this->data['heading_title'] = $this->language->get('heading_title');

	    $this->data['text_empty'] = $this->language->get('text_empty');
	    $this->data['button_continue'] = $this->language->get('button_continue');

	    $this->data['continue'] = $this->url->link('common/home', '', 'SSL');

        $myocwpl_picture = $this->config->get('myocwpl_picture');
        $myocwpl_model = $this->config->get('myocwpl_model');
        $myocwpl_rate = $this->config->get('myocwpl_rate');
        $myocwpl_buy = $this->config->get('myocwpl_buy');

        $this->data['myocwpl_picture'] = isset($myocwpl_picture) ? $myocwpl_picture : 0;
        $this->data['myocwpl_model'] = isset($myocwpl_model) ? $myocwpl_model : 0;
        $this->data['myocwpl_rate'] = isset($myocwpl_rate) ? $myocwpl_rate : 0;
        $this->data['myocwpl_buy'] = isset($myocwpl_buy) ? $myocwpl_buy : 0;

        if($this->config->get('myocwpl_login') && $this->customer->isLogged() && (!$this->config->get('myocwpl_customer_group') || !in_array($this->customer->getCustomerGroupId(), $this->config->get('myocwpl_customer_group')))) {
        	$this->data['error_customer_group'] = $this->language->get('error_customer_group');
        } else {
	        $this->load->model('catalog/product');
	        $this->load->model('catalog/category');
			$this->load->model('tool/image');

	        $default_limits = array(5,10,25,50,100);

	        if (isset($this->request->get['catid'])) {
				$catid = $this->request->get['catid'];
			} else {
				$catid = 0;
			}

	        if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'pd.name';
			}

	        if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'ASC';
			}

	        if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = $default_limits[1];
			}

	        if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			$this->data['text_limit'] = $this->language->get('text_limit');
			$this->data['text_category'] = $this->language->get('text_category');
			$this->data['text_print'] = $this->language->get('text_print');
			$this->data['text_sku'] = $this->language->get('text_sku');
			$this->data['text_upc'] = $this->language->get('text_upc');
			$this->data['text_discount'] = $this->language->get('text_discount');
            $this->data['text_netto'] = $this->language->get('text_netto');

			$this->data['column_image'] = $this->language->get('column_image');
			$this->data['column_name'] = $this->language->get('column_name');
			$this->data['column_model'] = $this->language->get('column_model');
			$this->data['column_rating'] = $this->language->get('column_rating');
			$this->data['column_price'] = $this->language->get('column_price');
			$this->data['column_stock'] = $this->language->get('column_stock');
			$this->data['column_qty'] = $this->language->get('column_qty');
			$this->data['column_action'] = $this->language->get('column_action');

            $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
            $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
			$this->data['button_cart'] = $this->language->get('button_cart');
			$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
			$this->data['button_bovebben'] = $this->language->get('button_bovebben');

	        $url = ''; //column header sort href

	        if ($order == 'ASC') {
				$url .= '&order=DESC';
			} else {
				$url .= '&order=ASC';
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			if (isset($this->request->get['catid'])) {
				$url .= '&catid=' . $this->request->get['catid'];
			}

			$this->data['sort_name'] = $this->url->link('myoc/pricelist', '&sort=pd.name' . $url, 'SSL');
			$this->data['sort_model'] = $this->url->link('myoc/pricelist', '&sort=p.model' . $url, 'SSL');
			$this->data['sort_rating'] = $this->url->link('myoc/pricelist', '&sort=rating' . $url, 'SSL');
			$this->data['sort_price'] = $this->url->link('myoc/pricelist', '&sort=p.price' . $url, 'SSL');
			$this->data['sort_quantity'] = $this->url->link('myoc/pricelist', '&sort=p.quantity' . $url, 'SSL');

	        $url = ''; //limits

	        if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

	        if (isset($this->request->get['catid'])) {
				$url .= '&catid=' . $this->request->get['catid'];
			}

	        $this->data['limits'] = array();

	        foreach($default_limits as $default_limit) {
	            $this->data['limits'][] = array(
	    			'value' => $default_limit,
	    			'href'  => $this->url->link('myoc/pricelist', $url . '&limit='. $default_limit, 'SSL')
	    		);
	        }

	        $url = ''; //categories

	        if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

	        if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

	        $this->data['categories'] = array();

	        $categories_id = $this->model_catalog_category->getCategoriesByParentId(0);
	        foreach($categories_id as $category_id)
	        {
	            $path = $this->getPath($category_id);
	            if($path) {
		            $this->data['categories'][$path] = array(
		                'category_id' => $category_id,
		                'path' => $path,
		                'href' => $this->url->link('myoc/pricelist', $url . '&catid='. $category_id, 'SSL'),
		            );
	        	}	
	            ksort($this->data['categories']);
	        }
	        array_unshift($this->data['categories'], array(
	            'category_id' => 0,
	            'path' => $this->language->get('text_all_products'),
	            'href' => $this->url->link('myoc/pricelist', $url, 'SSL'),
	        ));

	        $url = ''; //redirect, pagination href

	        if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

	        if (isset($this->request->get['catid'])) {
				$url .= '&catid=' . $this->request->get['catid'];
			}

	        $data = array(
				'sort'  => $sort,
				'order' => $order,
				'start' => ($page - 1) * $limit,
				'limit' => $limit,
			);

	        if($catid) {
	            $data['filter_category_id'] = $catid;
	        }
	        $this->data['catid'] = $catid;

	        $product_total = $this->model_catalog_product->getTotalProducts($data);

	        if((($page - 1) * $limit) > $product_total) {
	            $this->redirect($this->url->link('myoc/pricelist', $url, 'SSL'));
	        }

	        $this->data['products'] = array();

	        $products = $this->model_catalog_product->getProducts($data);



            $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
            $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');
	        foreach($products as $product_info) {
				if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
					$product_info['price'] = false;
	        	}

	            $discount_query = $this->model_catalog_product->getProductDiscounts($product_info['product_id']);

	    		$discounts = array();

	    		if($product_info['price']) { 
	    			foreach ($discount_query as $discount) {
		    			$discounts[] = array(
		    				'quantity' => $discount['quantity'],
		    				'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
		    			);
		    		}
	    		}

                $descr= strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8'));
                $descr=str_replace("&nbsp;","",$descr);
                $descr=str_replace("\r","",$descr);
                $descr=str_replace("\t","",$descr);
                $descr=str_replace("\n","",$descr);
                $descr=trim($descr);
                $descr=mb_substr($descr, 0, 1000, "UTF-8");

	            $this->data['products'][$product_info['product_id']] = array(
	                'image' => $product_info['image'] ? $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')) : $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')),
	                'popup' => $product_info['image'] ? $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')) : false,
	                'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id'], 'SSL'),
	                'name' => $product_info['name'],
	                'description' => nl2br($this->word_trim(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 25, TRUE)),
	                'model' => $product_info['model'],
	                'sku' => $product_info['sku'] == "" ? false : $product_info['sku'],
	                'upc' => $product_info['upc'] == "" ? false : $product_info['upc'],
	                'rating' => $product_info['rating'],
                    'date_ervenyes_ig' => $product_info['date_ervenyes_ig'],
	                'price' => $product_info['price'] ? $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))) : false,
	                'special' => $product_info['special'] ? $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'))) : false,
	                'discounts' => $discounts,
	                'quantity' => $product_info['quantity'],
	                'minimum' => $product_info['minimum'],
                    'csomagolasi_egyseg' => $product_info['csomagolasi_egyseg'],
                    'szazalek'    => $product_info['szazalek'],
                    'utalvany'    => $product_info['utalvany'],
                    'imagedesabled' => $product_info['imagedesabled'],
                    'megyseg'    => $product_info['megyseg'],
                    'csomagolasi_mennyiseg' => $product_info['csomagolasi_mennyiseg'] > 1 ? $product_info['csomagolasi_mennyiseg'] : 0
	            );
	        }

	        $pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('myoc/pricelist', $url . '&page={page}', 'SSL');

			$this->data['pagination'] = $pagination->render();

			//print button
	        if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

	        $this->data['print'] = $this->url->link('myoc/pricelist/printable', $url, 'SSL');

			$this->data['sort'] = $sort;
			$this->data['order'] = $order;
			$this->data['limit'] = $limit;

	        /*$this->document->addStyle(DIR_APPLICATION . 'view/javascript/jquery/fancybox/jquery.fancybox-1.3.4.css');
	        $this->document->addScript(DIR_APPLICATION . 'view/javascript/jquery/fancybox/jquery.fancybox-1.3.4.pack.js');
	        $this->document->addStyle(DIR_APPLICATION . 'view/javascript/jquery/colorbox/colorbox.css');
	        $this->document->addScript(DIR_APPLICATION . 'view/javascript/jquery/colorbox/jquery.colorbox-min.js');*/
	    }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/myoc/pricelist.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/myoc/pricelist.tpl';
		} else {
			$this->template = 'default/template/myoc/pricelist.tpl';
		}

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
    }

    public function printable() {
    	if($this->config->get('myocwpl_login') && !$this->customer->isLogged()) {
   			$this->session->data['redirect'] = $this->url->link('myoc/pricelist', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
        }
        if($this->config->get('myocwpl_login') && $this->customer->isLogged() && (!$this->config->get('myocwpl_customer_group') || !in_array($this->customer->getCustomerGroupId(), $this->config->get('myocwpl_customer_group')))) {
        	$this->redirect($this->url->link('myoc/pricelist', '', 'SSL'));
        }
        //header info
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = $this->config->get('config_ssl');
		} else {
			$this->data['base'] = $this->config->get('config_url');
		}
		
		$this->data['links'] = $this->document->getLinks();	 
		$this->data['lang'] = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');
		$this->data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = HTTPS_IMAGE;
		} else {
			$server = HTTP_IMAGE;
		}	
				
		if ($this->config->get('config_icon') && file_exists(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->data['icon'] = $server . $this->config->get('config_icon');
		} else {
			$this->data['icon'] = '';
		}
		
		$this->data['name'] = $this->config->get('config_name');
				
		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
			$this->data['logo'] = $server . $this->config->get('config_logo');
		} else {
			$this->data['logo'] = '';
		}

		$this->data['store_url'] = $this->data['base'];
		$this->data['store_title'] = $this->config->get('config_title');
		$this->data['store_address'] = nl2br($this->config->get('config_address'));
		$this->data['store_email'] = $this->config->get('config_email');
		$this->data['store_telephone'] = $this->config->get('config_telephone');

		//pricelist
        $this->language->load('myoc/pricelist');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['title'] = $this->document->getTitle();

        $this->data['heading_title'] = $this->language->get('heading_title');

	    $this->data['text_empty'] = $this->language->get('text_empty');

	    $this->load->model('catalog/product');
        $this->load->model('catalog/category');
		$this->load->model('tool/image');

		$default_limits = array(5,10,25,50,100);

        if (isset($this->request->get['catid'])) {
			$catid = $this->request->get['catid'];
		} else {
			$catid = 0;
		}

        if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

        if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

        if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $default_limits[0];
		}

        if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['text_sku'] = $this->language->get('text_sku');
		$this->data['text_upc'] = $this->language->get('text_upc');
		$this->data['text_discount'] = $this->language->get('text_discount');

		$this->data['column_number'] = $this->language->get('column_number');
		$this->data['column_image'] = $this->language->get('column_image');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_model'] = $this->language->get('column_model');
		$this->data['column_rating'] = $this->language->get('column_rating');
		$this->data['column_price'] = $this->language->get('column_price');
		$this->data['column_stock'] = $this->language->get('column_stock');
		$this->data['column_qty'] = $this->language->get('column_qty');

        $myocwpl_picture = $this->config->get('myocwpl_picture');
        $myocwpl_model = $this->config->get('myocwpl_model');
        $myocwpl_rate = $this->config->get('myocwpl_rate');
        $myocwpl_buy = $this->config->get('myocwpl_buy');

        $this->data['myocwpl_picture'] = isset($myocwpl_picture) ? $myocwpl_picture : 0;
        $this->data['myocwpl_model'] = isset($myocwpl_model) ? $myocwpl_model : 0;
        $this->data['myocwpl_rate'] = isset($myocwpl_rate) ? $myocwpl_rate : 0;
        $this->data['myocwpl_buy'] = isset($myocwpl_buy) ? $myocwpl_buy : 0;

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit,
			'limit' => $limit,
		);

        if($catid) {
            $data['filter_category_id'] = $catid;
        }

        $product_total = $this->model_catalog_product->getTotalProducts($data);

        if((($page - 1) * $limit) > $product_total) {
            $this->redirect($this->url->link('myoc/pricelist', '', 'SSL'));
        }

        $this->data['products'] = array();

        $products = $this->model_catalog_product->getProducts($data);

        foreach($products as $product_info) {
            $discount_query = $this->model_catalog_product->getProductDiscounts($product_info['product_id']);

    		$discounts = array();

    		foreach ($discount_query as $discount) {
    			$discounts[] = array(
    				'quantity' => $discount['quantity'],
    				'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
    			);
    		}

            $this->data['products'][$product_info['product_id']] = array(
                'image' => $product_info['image'] ? $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')) : $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')),
                //'popup' => $product_info['image'] ? $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')) : false,
                //'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id'], 'SSL'),
                'name' => $product_info['name'],
                'description' => nl2br($this->word_trim(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 20, TRUE)),
                'model' => $product_info['model'],
                'sku' => $product_info['sku'] == "" ? false : $product_info['sku'],
                'upc' => $product_info['upc'] == "" ? false : $product_info['upc'],
                'rating' => $product_info['rating'],
                'price' => $product_info['price'] ? $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))) : false,
                'special' => $product_info['special'] ? $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'))) : false,
                'date_ervenyes_ig' => $product_info['date_ervenyes_ig'],
                'discounts' => $discounts,
                'quantity' => $product_info['quantity'],
                'minimum' => $product_info['minimum'],
            );
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/myoc/pricelist_print.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/myoc/pricelist_print.tpl';
		} else {
			$this->template = 'default/template/myoc/pricelist_print.tpl';
		}

		$this->response->setOutput($this->render());
    }

    private function getPath($category_id) {
        $this->load->model('catalog/category');
        $category_info = $this->model_catalog_category->getCategory($category_id);
        if(!$category_info) {
        	return false;
        }
		if ($category_info['parent_id']) {
			return $this->getPath($category_info['parent_id']) . $this->language->get('text_separator') . $category_info['name'];
		}
		return $category_info['name'];
	}

    private function word_trim($string, $count, $ellipsis = FALSE) {
        $words = explode(' ', $string);
        if (count($words) > $count){
            array_splice($words, $count);
            $string = implode(' ', $words);
            if (is_string($ellipsis)){
                $string .= $ellipsis;
            } elseif ($ellipsis){
                $string .= '&hellip;';
            }
        }
        return $string;
    }
}
?>