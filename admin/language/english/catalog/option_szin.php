<?php
// Heading
$_['heading_title']       = 'Colors';

// Text
$_['text_success']        = 'Success: You have modified colors!';
$

// Column
$_['column_name']         = 'Option Name';
$_['column_szinkod']      = 'Color code';
$_['column_sort_order']   = 'Sort Order';
$_['column_action']       = 'Action';

// Entry
$_['entry_name']         = 'Option Name:';
$_['entry_szinkod']      = 'Hexadecimal color code:<br/><small>If there are multiple color / symbol, select a</small>';
$_['entry_azonosito']    = 'Identification:';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_szin_group']   = 'Color group:';


// Error
$_['error_permission']   = 'Warning: You do not have permission to modify color table!';
$_['error_name']         = 'Option Name must be between 1 and 128 characters!';
$_['error_type']         = 'Warning: Color code values required!';
$_['error_option_value'] = 'Option Value Name must be between 1 and 128 characters!';
$_['error_product']      = 'Warning: This option cannot be deleted as it is currently assigned to %s products!';
?>