<script type="text/javascript" src="catalog/view/javascript/jquery/nivo-slider/jquery.nivo.slider.pack.js"></script>
<div class="slideshow">
    <? if ($banners) { ?>
    <div id="slideshow<?php echo $module; ?>" class="nivoSlider" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;">
        <?php foreach ($banners as $banner) { ?>
            <?php if ($banner['link']) { ?>
                <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
            <?php } else { ?>
                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
            <?php } ?>
        <?php } ?>
    </div>
    <?php } ?>

</div>

<script type="text/javascript"><!--
    $(document).ready(function() {

        setTimeout("cserelget('#slideshow<?php echo $module; ?>')",900*<?php echo $module; ?>);

    });

function cserelget(para){
    $(para).nivoSlider();
    //$('#slideshow<?php echo $module; ?>').nivoSlider()

}
    --></script>
