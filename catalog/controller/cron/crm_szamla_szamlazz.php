<?php   
class ControllerCronCrmSzamlaSzamlazz extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;

        $this->load->model("cron/altalanos");
        $this->load->model('cron/crm_szamla_szamlazz');

        echo 'Elindítva<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $orders = $this->model_cron_crm_szamla_szamlazz->getOrders();

        echo '<br><br>Rendelések összesen: '.count($orders).' db';
        $this->model_cron_altalanos->addFuttatasEredmeny('Számlák összesen: '.count($orders).' db');


        if ($orders) {

            $data['fizmod']         = "Banki átutalás";
            $data['szamlaNyelve']   = "hu";
            $data['arfolyam']       = 1;

            foreach($orders as $key=>$value) {
                $szamlazz[$key] = $this->model_cron_crm_szamla_szamlazz->getOrder($value["invoiceid"]);


                if ($szamlazz[$key]) {
                    $szamlazz[$key]['salesorderid'] = $szamlazz[$key]['invoiceid'];
                    $szamlazz[$key]['salesorder_no'] = $szamlazz[$key]['rendeles_szam'];

                    $szamlazz[$key]['payment'] = $this->model_cron_crm_szamla_szamlazz->getOrderPayment($value["invoiceid"]);
                    $szamlazz[$key]['account']  = $this->model_cron_crm_szamla_szamlazz->getOrderAccount($szamlazz[$key]['accountid']);
                    $szamlazz[$key]['shipping']= $this->model_cron_crm_szamla_szamlazz->getOrderShipping($value["invoiceid"]);
                    $szamlazz[$key]['products'] = $this->model_cron_crm_szamla_szamlazz->getOrderProducts($value["invoiceid"]);
                    $szamlazz[$key]['contact'] = $this->model_cron_crm_szamla_szamlazz->getOrderContact($szamlazz[$key]["contactid"]);
                    if ($szamlazz[$key]['products']) {

                        $szla_tipus = 0;
                        if ($value['invoicestatus'] == "Számla") {
                            $szla_tipus = 1;
                        } elseif ($value['invoicestatus'] == "Előlegszámla") {
                            $szla_tipus = 3;
                        } elseif ($value['invoicestatus'] == "Végszámla") {
                            $szla_tipus = 4;
                        } /*elseif ($value['invoicestatus'] == "Webshopból bankkártyával kifizetett rendelés") {
                            $szla_tipus = 6;
                        }*/

                        if ($szla_tipus) {
                            if ($value['szamlaszam'] = $this->model_cron_altalanos->sendCrmToSzamlazz($szamlazz[$key],$szla_tipus,$data)) {
                                $this->model_cron_crm_szamla_szamlazz->deleteTriggerOrder($value);
                                $value['siker'] = 1;
                                $this->model_cron_crm_szamla_szamlazz->updateAllapotStatus($value);
                            } else {
                                $value['siker'] = '';
                                echo '<br>Sikertelen továbbítás:'.$szamlazz[$key]['salesorder_no'].' ID: '.$szamlazz[$key]['salesorderid'];
                                $this->model_cron_crm_szamla_szamlazz->updateAllapotStatus($value);
                            }
                        }
                    }
                }
            }
        }


        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}

