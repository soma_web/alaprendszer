
<?php
function children($para,$parts){
    if (isset($para['children']) && $para['children']) { ?>
        <ul>
            <?php foreach ($para['children'] as $child) { ?>
                    <?php if (in_array($child['category_id'],$parts)){ ?>
                    <li class="menu-text-image-1 active-li">
                       <a href="<?php echo $child['href']; ?>" class="active"><span><?php echo $child['name']; ?></span></a>
                    <?php } else { ?>
                    <li class="menu-text-image-1">
                        <a href="<?php echo $child['href']; ?>"><span><?php echo $child['name']; ?></span></a>
                    <?php } ?>
                   <?php if (isset($child["children"]) && $child["children"]){
                        children($child,$parts);
                    } ?>
                </li>
            <?php } ?>
        </ul>
        <?php
        return true;
    }
}
?>

<div class="box" style="">

    <div class="box-category" style="">
        <?php if ($heading_latszik){ ?>
            <div class="box-heading"><?php echo $heading_title; ?></div>
        <?php }?>

        <ul style="margin: 0;">
            <?php foreach ($categories as $category) { ?>

                    <?php if ($category['category_id'] == $category_id) { ?>
                    <li  class="menu-text-image-2 active-li">
                        <a href="<?php echo $category['href']; ?>" class="active"><span><?php echo $category['name']; ?></span></a>
                    <?php } else { ?>
                    <li  class="menu-text-image-2">
                        <a href="<?php echo $category['href']; ?>" > <span><?php echo $category['name']; ?></span></a>
                    <?php }
                    children($category,$parts);
                    ?>

                </li>
            <?php } ?>
        </ul>

    </div>
</div>
