<div class="product_icons">

    <?php if (isset($beallitas_termek_aloldal['uj']) && $beallitas_termek_aloldal['uj']) { ?>
        <?php if(!(isset($product_info['ujdonsag']) && $product_info['ujdonsag'])) { ?>
            <?php if (isset($uj) && $uj == 1) { ?>
                <img class="icon_meret" title="<? if (isset($text_uj_title)) { echo $text_uj_title; }?>" src="<? echo $ujtermek?>" >
            <?php } ?>
        <?php } ?>

        <?php if (isset($beallitas_termek_aloldal['kifuto']) && $beallitas_termek_aloldal['kifuto']) { ?>
            <?php if (isset($kifuto) && $kifuto == 1) { ?>
                <img class="icon_meret" title="<? if (isset($text_kifuto_title)) { echo $text_kifuto_title; }?>" src="<? echo $kifutokep?>" >
            <?php } ?>
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas_termek_aloldal['pardarab']) && $beallitas_termek_aloldal['pardarab']) { ?>
        <?php if ($this->config->get('config_pardarab') ) { ?>
            <?php $pardarab_kell =  ($product_info['quantity'] <= $this->config->get('config_pardarab') && $product_info['quantity'] > 0) ? true : false;?>
        <?php } else { ?>
            <?php $pardarab_kell =  ($product_info['quantity'] <= 4 && $product_info['quantity'] > 0) ? true : false;?>
        <?php } ?>

        <?php if($pardarab_kell) { ?>
            <img class="icon_meret" title="<? if (isset($text_pardarab_title)) { echo $text_pardarab_title; }?>" src="<? echo $pardarabkep?>" >
        <?php } ?>

    <?php } ?>

    <?php if (isset($beallitas_termek_aloldal['szazalek']) && $beallitas_termek_aloldal['szazalek']) { ?>
        <?php if($product_info['special']) {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/sale.png')) {
                $szazalek = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/sale.png';
            } else {
                $szazalek = DIR_TEMPLATE_IMAGE.'default/image/sale.png';
            }
            ?>
            <img class="icon_meret" title="<? if (isset($text_akcio_title)) { echo $text_akcio_title; }?>"  src="<? echo $szazalek?>" >
        <?php } ?>
    <?php } ?>

    <?php if (!empty($beallitas_termek_aloldal['szazalek_ertek']) ) { ?>
        <?php if(!empty($product_info['arengedmeny_szazalek'])) { ?>
            <span class="icon_meret szazalek_ertek" title="<? echo (isset($text_akcio_title)) ? $text_akcio_title : ''?>"><?php echo '- '.$product_info['arengedmeny_szazalek'].' %'?></span>
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas_termek_aloldal['ujdonsag']) && $beallitas_termek_aloldal['ujdonsag']) { ?>
        <?php if($product_info['ujdonsag']) {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/ujdonsag.png')) {
                $ujdonsag = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/ujdonsag.png';
            } else {
                $ujdonsag = DIR_TEMPLATE_IMAGE.'default/image/ujdonsag.png';
            }
            ?>
            <img class="icon_meret" title="<? if (isset($text_ujdonsag_title)) { echo $text_ujdonsag_title; }?>"  src="<?php echo $ujdonsag; ?>" >
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas_termek_aloldal['mennyisegi']) && $beallitas_termek_aloldal['mennyisegi']) { ?>
        <?php if (isset($discounts) && count($discounts) > 0 ) { ?>
            <img class="icon_meret ico_mennyisegi"  src="<? echo $mennyisegikep?>" >
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas_termek_aloldal['ajandek']) && $beallitas_termek_aloldal['ajandek']) { ?>
        <?php if($product_info['ajandek']) {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/ajandek.jpg')) {
                $ajandek = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/ajandek.jpg';
            } else {
                $ajandek = DIR_TEMPLATE_IMAGE.'default/image/ajandek.jpg';
            }
            ?>
            <img class="icon_meret" title="<? if (isset($text_ajandek_title)) { echo $text_ajandek_title; }?>"  src="<?php echo $ajandek; ?>" >
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas_termek_aloldal['raktar']) && $beallitas_termek_aloldal['raktar']) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/raktaron.png')) {
                    $raktaron = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/raktaron.png';
                } else {
                    $raktaron = DIR_TEMPLATE_IMAGE.'default/image/raktaron.png';
                }

                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/nincs_raktaron.png')) {
                    $nincs_raktaron = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/nincs_raktaron.png';
                } else {
                    $nincs_raktaron = DIR_TEMPLATE_IMAGE.'default/image/nincs_raktaron.png';
                }
        ?>
        <?php if ($product_info['quantity'] > 0 ) { ?>
            <img class="icon_raktaron" title="<? if (isset($text_raktaron)) { echo $text_raktaron; }?>"  src="<? echo $raktaron?>" >
        <?php } else { ?>
            <img class="icon_raktaron" title="<? if (isset($text_nincs_raktaron)) { echo $text_nincs_raktaron; }?>"  src="<? echo $nincs_raktaron?>" >
        <?php } ?>
    <?php } ?>
    <?php if (isset($beallitas_termek_aloldal['kamera']) && $beallitas_termek_aloldal['kamera']) { ?>
        <div class="van_video"></div>
    <?php } ?>
    <?php if (isset($beallitas_termek_aloldal['csapo']) && $beallitas_termek_aloldal['csapo']) { ?>
        <div class="van_csapo"></div>
    <?php } ?>
    <?php if (isset($beallitas_termek_aloldal['compare']) && $beallitas_termek_aloldal['compare']) {

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/compare_hover.png')) {
            $compare_hover = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/compare_hover.png';
        } elseif(file_exists(DIR_TEMPLATE_IMAGE.'default/image/compare_hover.png')) {
            $compare_hover = DIR_TEMPLATE_IMAGE.'default/image/compare_hover.png';
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/compare.png')) {
            $compare = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/compare.png';
        } else {
            $compare = DIR_TEMPLATE_IMAGE.'default/image/compare.png';
        }

        if(isset($compare_hover)){
            ?>
            <script>
                function hover_compare(element) {
                    element.setAttribute('src', '<?php echo $compare_hover; ?>');
                }
                function unhover_compare(element) {
                    element.setAttribute('src', '<?php echo $compare; ?>');
                }
            </script>
        <?php } ?>


        <a class="" onclick="addToCompare('<?php echo $product_id; ?>');">
            <img class="icon_compare" <?php if(isset($compare_hover)){ ?> onmouseover="hover_compare(this);" onmouseout="unhover_compare(this);" <?php } ?> title="<? if (isset($text_compare)) { echo $text_compare; }?>"  src="<? echo $compare?>" >

        </a>
    <?php } ?>
    <?php if (isset($beallitas_termek_aloldal['wishlist']) && $beallitas_termek_aloldal['wishlist']) {
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/kedvencekhez_ad_hover.png')) {
            $kedvencekhez_ad_hover = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/kedvencekhez_ad_hover.png';
        } elseif(file_exists(DIR_TEMPLATE_IMAGE.'default/image/compare_hover.png')) {
            $kedvencekhez_ad_hover = DIR_TEMPLATE_IMAGE.'default/image/compare_hover.png';
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/kedvencekhez_ad.png')) {
            $kedvencekhez_ad = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/kedvencekhez_ad.png';
        } else {
            $kedvencekhez_ad = DIR_TEMPLATE_IMAGE.'default/image/kedvencekhez_ad.png';
        }

        if(isset($kedvencekhez_ad_hover)){
            ?>
            <script>
                function hover_wishlist(element) {
                    element.setAttribute('src', '<?php echo $kedvencekhez_ad_hover; ?>');
                }
                function unhover_wishlist(element) {
                    element.setAttribute('src', '<?php echo $kedvencekhez_ad; ?>');
                }
            </script>
        <?php } ?>
        <a class="" onclick="addToWishList('<?php echo $product_id; ?>');">
            <img class="icon_compare" <?php if(isset($kedvencekhez_ad_hover)){ ?> onmouseover="hover_wishlist(this);" onmouseout="unhover_wishlist(this);" <?php } ?> title="<? if (isset($text_wishlist)) { echo $text_wishlist; }?>"  src="<? echo $kedvencekhez_ad?>" >

        </a>
    <?php } ?>
</div>