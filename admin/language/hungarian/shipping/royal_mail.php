<?php

// Heading
$_['heading_title']             = 'Angol állami postaszolgálat';

// Text
$_['text_shipping']             = 'Szállítás';
$_['text_success']              = 'Siker: Az angol állami postaszolgálat szállítási mód módosítása megtörtént!';
$_['text_1st_class_standard']   = 'Első osztályú szabványos postázás';
$_['text_1st_class_recorded']   = 'Első osztályú feljegyszett postázás';
$_['text_2nd_class_standard']   = 'Másod osztályú szabványos postázás';
$_['text_2nd_class_recorded']   = 'Másod osztályú feljegyszett postázás';
$_['text_standard_parcels']     = 'Szabványos csomag';
$_['text_airmail']              = 'Airmail';
$_['text_international_signed'] = 'Nemzetközileg megjelölt';
$_['text_airsure']              = 'Airsure';
$_['text_surface']              = 'Felület';

// Entry
$_['entry_service']             = 'Szolgáltatások:';
$_['entry_display_weight']      = 'Szállítási súly kijelzése:<br /><span class="help">Akarja, hogy ki legyen jelezve a szállítási súly? (pl. Szállítási súly : 2.7674 Kg)</span>';
$_['entry_display_insurance']   = 'Biztosítás kijelzése:<br /><span class="help">Akarja, hogy ki legyen jelezve a szállítás biztosítása? (pl. Biztosítva 500-ig)</span>';
$_['entry_display_time']        = 'Szállítási idő kijelzése:<br /><span class="help">Akarja, hogy ki legyen jelezve a szállítási idő? (pl. Szállítás 3-5 napon belül)</span>';
$_['entry_weight_class']        = 'Súly osztály:';
$_['entry_tax']                 = 'Adóosztály:';
$_['entry_geo_zone']            = 'Földrajzi zóna:';
$_['entry_status']              = 'Állapot:';
$_['entry_sort_order']          = 'Sorrend:';
$_['entry_title_heading'] = 'Fejléc megnevezése:';
$_['entry_title_text'] = 'Szállítási mód megnevezése:';
$_['entry_mail']        = 'Egyéb figyelmeztető e-mailek:<br /><span class="help">Egyéb figyelmeztető e-mailek, amiket kapni szeretne, a közponzi áruház  e-mailjei mellett. (vesszővel elválasztva)</span>';

// Error
$_['error_permission']          = 'Figyelmeztetés: Az angol állami postaszolgálat szállítási mód módosítása az Ön számára nem engedélyezett!';
?>
