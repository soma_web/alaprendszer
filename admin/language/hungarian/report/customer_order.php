<?php

// Heading
$_['heading_title']         = 'Ügyfél rendelések jelentés';

// Text
$_['text_all_status']       = 'Minden állapot';
$_['select_ferfi']          = 'Férfi';
$_['select_holgy']          = 'Nő';
$_['select_kupon']          = 'Kupon';
$_['select_utalvany']       = 'Utalvany';


// Column
$_['column_customer']       = 'Ügyfélnév';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Ügyfélcsoport';
$_['column_status']         = 'Állapot';
$_['column_orders']         = 'Rendelések száma';
$_['column_products']       = 'Termékek száma';
$_['column_total']          = 'Összesen';
$_['column_action']         = 'Művelet';

// Entry
$_['entry_date_start']      = 'Kezdete:';
$_['entry_date_end']        = 'Vége:';
$_['entry_status']          = 'Rendelés állapota:';
$_['entry_korosztaly']      = 'Korosztály:';
$_['entry_nem']             = 'Nemek:';
$_['entry_ingyenes']        = 'Termék:';

$_['entry_iskolai_vegzettseg'] = 'Iskolai végzettség';
$_['column_iskolai_vegzettseg'] = 'Iskolai végzettség';

?>