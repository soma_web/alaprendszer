<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Hírlevél Statisztika';

// Text 
$_['text_success']							= 'Siker: A kijelölt elemek eltávolítva!';
$_['text_sent']								= 'Elküldve';
$_['text_time']								= 'Idő';
$_['text_views']							= 'Megnézve';
$_['text_no_data']							= 'Üres';
$_['text_default']							= 'Alapértelmezett';

// Entry 
$_['entry_text_queued']						= 'Küldve %s ból %oknak';
$_['entry_heading']							= 'Hírlevél &laquo;%s&raquo;';
$_['entry_track']							= 'Trackelt linkek';
$_['entry_recipients']						= 'Címzettek';
$_['entry_total_recipients']				= 'Összes címzett';
$_['entry_total_views']						= 'Összes megtekintés';
$_['entry_total_failed']					= 'Sikertelen Hírlevelek';
$_['entry_total_bounced']					= 'Nem kézbesített Hírlevelek';
$_['entry_sent']							= 'Elküldött Hírlevelek';
$_['entry_read']							= 'Olvasott Hírlevelek';
$_['entry_unsubscribe_clicks']				= 'Leiratkozó kattintások';
$_['entry_url']								= 'Url';
$_['entry_clicks']							= 'Kattintások';
$_['entry_attachements']					= 'Csatolmányok';
$_['entry_timeline']						= 'Idővonal';
$_['entry_yes']								= 'Igen';
$_['entry_no']								= 'Nem';

// Button 
$_['button_details']						= 'Részletek';
$_['button_hide_details']					= 'Részletek elrejtése';

// Column 
$_['column_actions']						= 'Akciók';
$_['column_subject']						= 'Tárgy';
$_['column_recipients']						= 'Címzettek';
$_['column_date']							= 'Dátum';
$_['column_views']							= 'Megnézve';
$_['column_name']							= 'Név';
$_['column_email']							= 'Email';
$_['column_clicks']							= 'Meglátogatott hivatkozások';
$_['column_store']							= 'Áruház';
$_['column_success']						= 'Sikeresen elküldve';
$_['column_bounced']						= 'Kézbesítetlen';

?>
