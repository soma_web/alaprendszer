<?php
class Customer {
	private $customer_id;
	private $firstname;
	private $lastname;
	private $email;
	private $telephone;
	private $fax;
	private $newsletter;
	private $customer_group_id;
	private $address_id;
	private $adoszam;
	private $nem;
	private $eletkor;
    private $iskolai_vegzettseg;
    private $feltolto;
    private $vallalkozasi_forma;
    private $szekhely;
    private $ugyvezeto_neve;
    private $ugyvezeto_telefonszama;
    private $weblap;
    private $szazalek;
    private $company;

  	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');
				
		if (isset($this->session->data['customer_id'])) { 
			$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE
			    customer_id = '" . (int)$this->session->data['customer_id'] . "' AND status = '1'");
			
			if ($customer_query->num_rows) {
				$this->customer_id = $customer_query->row['customer_id'];
				$this->firstname = $customer_query->row['firstname'];
				$this->lastname = $customer_query->row['lastname'];
				$this->email = $customer_query->row['email'];
				$this->telephone = $customer_query->row['telephone'];
				$this->fax = $customer_query->row['fax'];
				$this->newsletter = $customer_query->row['newsletter'];
				$this->customer_group_id = $customer_query->row['customer_group_id'];
				$this->address_id = $customer_query->row['address_id'];
				$this->adoszam = $customer_query->row['adoszam'];
				$this->nem = $customer_query->row['nem'];
				$this->eletkor = $customer_query->row['eletkor'];
                $this->iskolai_vegzettseg = $customer_query->row['iskolai_vegzettseg'];
                $this->feltolto = $customer_query->row['feltolto'];
                $this->vallalkozasi_forma = $customer_query->row['vallalkozasi_forma'];
                $this->szekhely = $customer_query->row['szekhely'];
                $this->ugyvezeto_neve = $customer_query->row['ugyvezeto_neve'];
                $this->ugyvezeto_telefonszama = $customer_query->row['ugyvezeto_telefonszama'];
                $this->weblap = isset($customer_query->row['weblap']) ? $customer_query->row['weblap'] : '' ;
                $this->szazalek = $customer_query->row['szazalek'];
                $this->company = $customer_query->row['company'];

      		/*	$this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "', wishlist = '" . $this->db->escape(isset($this->session->data['wishlist']) ? serialize($this->session->data['wishlist']) : '') . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");

                $this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "', wishlist = '" . $this->db->escape(isset($this->session->data['wishlist']) ? serialize($this->session->data['wishlist']) : '') . "', date_last_action = NOW(), number_reminder_sent = 0, ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");*/

                $this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "', wishlist = '" . $this->db->escape(isset($this->session->data['wishlist']) ? serialize($this->session->data['wishlist']) : '') . "', date_last_action = NOW(), number_reminder_sent = 0, ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->session->data['customer_id'] . "'");
			
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$this->session->data['customer_id'] . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");
				
				if (!$query->num_rows) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "customer_ip SET customer_id = '" . (int)$this->session->data['customer_id'] . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', date_added = NOW()");
				}
			} else {
				$this->logout();
			}
  		}
	}
		
  	public function login($email, $password, $override = false) {
		if ($override) {
			$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer where LOWER(email) = '" . $this->db->escape(strtolower($email)) . "' AND status = '1'");
		} elseif (!$this->config->get('config_customer_approval')) {
			$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "' AND password = '" . $this->db->escape(md5($password)) . "' AND status = '1'");
		} else {
			$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "' AND password = '" . $this->db->escape(md5($password)) . "' AND status = '1' AND approved = '1'");
		}
		
		if ($customer_query->num_rows) {
			$this->session->data['customer_id'] = $customer_query->row['customer_id'];
            if ($customer_query->row['feltolto'] == 1) {
                $this->session->data['feltolto'] = 1;
            }
		    
			if ($customer_query->row['cart'] && is_string($customer_query->row['cart'])) {
				$cart = unserialize($customer_query->row['cart']);
				
				foreach ($cart as $key => $value) {
					if (!array_key_exists($key, $this->session->data['cart'])) {
						$this->session->data['cart'][$key] = $value;
					} else {
						$this->session->data['cart'][$key] += $value;
					}
				}			
			}

			if ($customer_query->row['wishlist'] && is_string($customer_query->row['wishlist'])) {
				if (!isset($this->session->data['wishlist'])) {
					$this->session->data['wishlist'] = array();
				}
								
				$wishlist = unserialize($customer_query->row['wishlist']);
			
				foreach ($wishlist as $product_id) {
					if (!in_array($product_id, $this->session->data['wishlist'])) {
						$this->session->data['wishlist'][] = $product_id;
					}
				}			
			}
									
			$this->customer_id = $customer_query->row['customer_id'];
			$this->firstname = $customer_query->row['firstname'];
			$this->lastname = $customer_query->row['lastname'];
			$this->email = $customer_query->row['email'];
			$this->telephone = $customer_query->row['telephone'];
			$this->fax = $customer_query->row['fax'];
			$this->newsletter = $customer_query->row['newsletter'];
			$this->customer_group_id = $customer_query->row['customer_group_id'];
			$this->address_id = $customer_query->row['address_id'];
			$this->adoszam = $customer_query->row['adoszam'];
            $this->iskolai_vegzettseg = $customer_query->row['iskolai_vegzettseg'];
            $this->vallalkozasi_forma = $customer_query->row['vallalkozasi_forma'];
            $this->szekhely = $customer_query->row['szekhely'];
            $this->ugyvezeto_neve = $customer_query->row['ugyvezeto_neve'];
            $this->ugyvezeto_telefonszama = $customer_query->row['ugyvezeto_telefonszama'];
            $this->weblap = isset($customer_query->row['weblap']) ? $customer_query->row['weblap'] : '' ;
            $this->szazalek = $customer_query->row['szazalek'];
            $this->company = $customer_query->row['company'];


			$this->db->query("UPDATE " . DB_PREFIX . "customer SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");
			
	  		return true;
    	} else {
      		return false;
    	}
  	}
  	
	public function logout() {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "', wishlist = '" . $this->db->escape(isset($this->session->data['wishlist']) ? serialize($this->session->data['wishlist']) : '') . "' WHERE customer_id = '" . (int)$this->customer_id . "'");
		
		unset($this->session->data['customer_id']);
        if (isset($this->session->data['feltolto'])) {
            unset ($this->session->data['feltolto']);
        }

		$this->customer_id = '';
		$this->firstname = '';
		$this->lastname = '';
		$this->email = '';
		$this->telephone = '';
		$this->fax = '';
		$this->newsletter = '';
		$this->customer_group_id = '';
		$this->address_id = '';
		$this->adoszam = '';
        $this->iskolai_vegzettseg = '';
        $this->vallalkozasi_forma = '';
        $this->szekhely = '';
        $this->ugyvezeto_neve = '';
        $this->ugyvezeto_telefonszama = '';
        $this->weblap = '';
        $this->szazalek = '';
        $this->company = '';
        if( array_key_exists('userid', $_COOKIE) && array_key_exists('token', $_COOKIE)) {
            $this->db->query("DELETE FROM ".DB_PREFIX."remembered_logins WHERE customer_id=".$_COOKIE['userid']." AND token='".$_COOKIE['token']."';");
            setcookie('userid', null, time()-3600, '/', $_SERVER['SERVER_NAME'], false, true);
            setcookie('token', null, time()-3600, '/', $_SERVER['SERVER_NAME'], false, true);
        }
  	}
  
  	public function isLogged() {
    	return $this->customer_id;
  	}

  	public function getId() {
    	return $this->customer_id;
  	}
      
  	public function getFirstName() {
		return $this->firstname;
  	}
    public function getAdoszam() {
        return $this->adoszam;
    }
  
  	public function getLastName() {
		return $this->lastname;
  	}
  
  	public function getEmail() {
		return $this->email;
  	}

    public function getCompany() {
		return $this->company;
  	}
  
  	public function getTelephone() {
		return $this->telephone;
  	}
  
  	public function getFax() {
		return $this->fax;
  	}

    public function getNem() {
        return $this->nem;
    }

    public function getEletkor() {
        return $this->eletkor;
    }

    public function getIskolaiVegzettseg() {
        return $this->iskolai_vegzettseg;
    }

    public function getFeltolto() {
        return $this->feltolto;
    }

    public function getSzazalek() {
        return $this->szazalek;
    }

    /**
     * @return mixed
     */
    public function getSzekhely() {
        return $this->szekhely;
    }

    /**
     * @return mixed
     */
    public function getUgyvezetoNeve() {
        return $this->ugyvezeto_neve;
    }

    /**
     * @return mixed
     */
    public function getUgyvezetoTelefonszama() {
        return $this->ugyvezeto_telefonszama;
    }
    public function getWeblap() {
        return $this->weblap;
    }

    /**
     * @return mixed
     */
    public function getVallalkozasiForma() {
        return $this->vallalkozasi_forma;
    }
	
  	public function getNewsletter() {
		return $this->newsletter;	
  	}

  	public function getCustomerGroupId() {
		return $this->customer_group_id;	
  	}
	
  	public function getAddressId() {
		return $this->address_id;	
  	}
	
  	public function getBalance() {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$this->customer_id . "'");
	
		return $query->row['total'];
  	}	
		
  	public function getRewardPoints() {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$this->customer_id . "'");
	
		return $query->row['total'];	
  	}

    public function getAddress() {
        $vissza = array(
            'iranyitoszam'   => '',
            'varos'         => '',
            'utca'          => ''
        );
        if (!empty($this->address_id)) {
            $sql = "SELECT address_1, city, postcode FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$this->address_id . "'";
            $query = $this->db->query($sql);

            if (!empty($query->row)) {
                $vissza = array(
                    'iranyitoszam'  => $query->row['postcode'],
                    'varos'         => $query->row['city'],
                    'utca'          => $query->row['address_1']
                );
            }
        }

        return $vissza;
    }
}
?>