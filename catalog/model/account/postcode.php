<?php
class ModelAccountPostcode extends Model {
    private $maxItem=10;

    public function getPostcodes() {
        $sql = "SELECT * FROM ".DB_PREFIX."postcode LIMIT 0,".$this->maxItem;
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function searchPostcode($part) {
        if($part == null || $part == "") {
            return $this->getPostcodes();
        }
        $sql = "SELECT * FROM ".DB_PREFIX."postcode WHERE postcode LIKE '".$part."%' LIMIT 0,".$this->maxItem;
        $query = $this->db->query($sql);

        return  $query->rows;
    }

    public function getPostcode($id) {
        $sql = "SELECT * FROM ".DB_PREFIX."postcode WHERE postcodeid=".$id;
        $query = $this->db->query($sql);

        return $query->row;
    }
}
