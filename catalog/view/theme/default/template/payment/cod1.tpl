<div class="buttons">
    <div class="right">
        <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="button" />
    </div>
</div>
<script type="text/javascript"><!--
    $('#button-confirm').bind('click', function() {
        if (typeof(megrendelem_folyamat) == "boolean") {
            $.ajax({
                url: 'index.php?route=checkout/confirm_megrendelem/validate',
                type: 'post',
                data: $('#payment-address :input'),
                dataType: 'json',

                success: function(json) {
                    $(".error").remove();

                    if (json['error']) {
                        if (json['error']['firstname']) {
                            $('#payment-address input[name=\'firstname\'] + br').after('<span class="error">' + json['error']['firstname'] + '</span>');
                        }

                        if (json['error']['lastname']) {
                            $('#payment-address input[name=\'lastname\'] + br').after('<span class="error">' + json['error']['lastname'] + '</span>');
                        }

                        if (json['error']['email']) {
                            $('#payment-address input[name=\'email\'] + br').after('<span class="error">' + json['error']['email'] + '</span>');
                        }

                        if (json['error']['telephone']) {
                            $('#payment-address input[name=\'telephone\'] + br').after('<span class="error">' + json['error']['telephone'] + '</span>');
                        }

                        if (json['error']['address_1']) {
                            $('#payment-address input[name=\'address_1\'] + br').after('<span class="error">' + json['error']['address_1'] + '</span>');
                        }

                        if (json['error']['city']) {
                            $('#payment-address input[name=\'city\'] + br').after('<span class="error">' + json['error']['city'] + '</span>');
                        }

                        if (json['error']['postcode']) {
                            $('#payment-address input[name=\'postcode\'] + br').after('<span class="error">' + json['error']['postcode'] + '</span>');
                        }

                        if (json['error']['country']) {
                            $('#payment-address select[name=\'country_id\'] + br').after('<span class="error">' + json['error']['country'] + '</span>');
                        }

                        if (json['error']['nem']) {
                            $('#payment-address select[name=\'nem\'] + br').after('<span class="error">' + json['error']['nem'] + '</span>');
                        }
                        if (json['error']['eletkor']) {
                            $('#payment-address select[name=\'eletkor\'] + br').after('<span class="error">' + json['error']['eletkor'] + '</span>');
                        }

                        if (json['error']['zone']) {
                            $('#payment-address select[name=\'zone_id\'] + br').after('<span class="error">' + json['error']['zone'] + '</span>');
                        }
                        $('html, body').animate({ scrollTop: 0 }, 'slow');

                    } else {
                        $.ajax({
                            type: 'GET',
                            url: 'index.php?route=payment/cod1/confirm',
                            beforeSend: function() {
                                $('#button-confirm').attr('disabled', true);
                                $('#button-confirm').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
                            },
                            success: function() {
                                location = '<?php echo $continue; ?>';
                            },
                            complete: function() {
                                $('#button-confirm').attr('disabled', false);
                                $('.wait').remove();
                            }
                        });
                    }


                },
                error: function(xhr, ajaxOptions, thrownError) {

                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }

            });

        } else {
            $.ajax({
                type: 'GET',
                url: 'index.php?route=payment/cod1/confirm',
                beforeSend: function() {
                    $('#button-confirm').attr('disabled', true);
                    $('#button-confirm').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
                },
                success: function() {
                    $.ajax({
                        url: 'index.php?route=checkout/guest_history',
                        type: 'post',
                        data: 'Megrendelem=Tovább',
                        dataType: 'html',

                        success: function(json) {
                            location = '<?php echo $continue; ?>';

                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            debugger;

                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                },
                complete: function() {
                    $('#button-confirm').attr('disabled', false);
                    $('.wait').remove();
                }
            });
        }
    });
    //--></script>
