<?php if ( isset($szinek) && $szinek){ ?>

    <style>
        .szintablazat {
            display: inline-table;
            height: 14px;
            width: 14px;
            padding: 0;
        }



        .squaredTwo {
            width: 33px;
            height: 28px;


            -webkit-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.1);
            -moz-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.1);
            box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.1);
            position: relative;
            display: inline-block;
            margin: 0 4px 10px 0;

        }

        .squaredTwo label {
            cursor: pointer;
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0px;
            top: 0px;
        }

        .squaredTwo label:after {
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            filter: alpha(opacity=0);
            opacity: 0;
            content: '';
            position: absolute;
            width: 9px;
            height: 5px;
            background: transparent;
            top: 4px;
            left: 4px;
            border: 3px solid #fcfff4;
            border-top: none;
            border-right: none;

            -webkit-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            -o-transform: rotate(-45deg);
            -ms-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }

        .squaredTwo label:hover::after {
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30";
            filter: alpha(opacity=30);
            opacity: 0.3;
        }

        .squaredTwo input[type=checkbox]:checked + label:after {
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
            filter: alpha(opacity=100);
            opacity: 1;
        }

    </style>

    <div class="box">
        <div class="box-heading reszek harmadik-filter"><?php echo $heading_title_szin; ?><img style="display: none"  src="catalog/view/theme/default/image/responsive/pos_r_opened.png"></div>
        <?php if ($beallitasok['szin']['teljes'] != 1) { ?>
            <div class="box-content">

                <?php foreach ($szinek as $szin) { ?>

                    <?php if ($beallitasok['szin']['szinnev'] != 1) { ?>
                        <?php $szinek = str_replace(' ','',$szin['szinkod']);?>
                        <?php $szinek = str_replace('#','',$szinek);?>
                        <?php $szinek = explode('/',$szinek); ?>
                    <?php } ?>

                    <?php if (!empty($szin['name'])) { ?>
                        <?php if (in_array($szin['option_szin_id'], $filter_szin)) { ?>
                            <div class="squaredTwo box-filter box-filter-szin">
                                <input type="checkbox" value="<?php echo $szin['option_szin_id']; ?>" id="szin<?php echo $szin['option_szin_id']; ?>" name="check" checked="checked"/>

                                <label style=""  class="" for="szin<?php echo $szin['option_szin_id']; ?>">
                                    <?php foreach($szinek as $szinkod) { ?>
                                        <div style="background: #<?php echo $szinkod; ?>; height:<?php echo round(100/count($szinek),0)?>%"></div>
                                    <?php } ?>
                                </label>
                            </div>

                        <?php }  else {?>
                            <div class="squaredTwo box-filter box-filter-szin">
                                <input type="checkbox" value="<?php echo $szin['option_szin_id']; ?>" id="szin<?php echo $szin['option_szin_id']; ?>" name="check" />

                                <label style=""  class="" for="szin<?php echo $szin['option_szin_id']; ?>">
                                    <?php foreach($szinek as $szinkod) { ?>
                                        <div style="background: #<?php echo $szinkod; ?>; height:<?php echo round(100/count($szinek),0)?>%"></div>
                                    <?php } ?>
                                </label>
                            </div>

                        <?php } ?>

                    <?php } ?>
                <?php } ?>
            </div>
        <?php } else {?>
            <div class="box-content">
                <ul class="box-filter box-filter-szin">
                    <?php foreach ($szinek_teljes as $szin_teljes) { ?>

                        <?php if ($beallitasok['szin']['szinnev'] != 1) { ?>
                            <?php $szinek = str_replace(' ','',$szin_teljes['szinkod']);?>
                            <?php $szinek = str_replace('#','',$szinek);?>
                            <?php $szinek = explode('/',$szinek); ?>
                        <?php } ?>

                        <?php if (in_array($szin_teljes, $szinek)) { ?>
                            <?php if (in_array($szin_teljes['option_szin_id'], $filter_szin)) { ?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $szin_teljes['option_szin_id']; ?>" id="szin<?php echo $szin_teljes['option_szin_id']; ?>"  checked="checked"/>
                                    <?php if ($beallitasok['szin']['szinnev'] == 1) { ?>
                                        <label class="css-label mac-style" for="szin<?php echo $szin_teljes['option_szin_id']; ?>"><?php echo $szin_teljes['name']; ?></label>
                                    <?php } else {?>
                                        <label class="css-label mac-style szintablazat" for="szin<?php echo $szin_teljes['option_szin_id']; ?>">
                                            <?php foreach($szinek as $szinkod) { ?>
                                                <?php echo $szin_teljes['name']; ?><div style="background: #<?php echo $szinkod; ?>; height:<?php echo round(100/count($szinek),0)-1?>%"></div>
                                            <?php } ?>
                                        </label>
                                    <?php } ?>
                                </li>
                            <?php } else {?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $szin_teljes['option_szin_id']; ?>" id="szin<?php echo $szin_teljes['option_szin_id']; ?>" />
                                    <?php if ($beallitasok['szin']['szinnev'] == 1) { ?>
                                        <label class="css-label mac-style" for="szin<?php echo $szin_teljes['option_szin_id']; ?>"><?php echo $szin_teljes['name']; ?></label>
                                    <?php } else {?>
                                        <label class="css-label mac-style szintablazat" for="szin<?php echo $szin_teljes['option_szin_id']; ?>">
                                            <?php foreach($szinek as $szinkod) { ?>
                                                <?php echo $szin_teljes['name']; ?><div style="background: #<?php echo $szinkod; ?>; height:<?php echo round(100/count($szinek),0)-1?>%"></div>
                                            <?php } ?>
                                        </label>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        <?php } else {?>
                            <li><input class="css-checkbox" type="checkbox" value="<?php echo $szin_teljes['option_szin_id']; ?>" id="szin<?php echo $szin_teljes['option_szin_id']; ?>" disabled="disabled"/>
                                <?php if ($beallitasok['szin']['szinnev'] == 1) { ?>
                                    <label class="css-label mac-style halvany" for="szin<?php echo $szin_teljes['option_szin_id']; ?>" ><?php echo $szin_teljes['name']; ?></label>
                                <?php } else {?>
                                    <label class="css-label mac-style halvany szintablazat" for="szin<?php echo $szin_teljes['option_szin_id']; ?>" >
                                        <?php foreach($szinek as $szinkod) { ?>
                                            <div style="background: #<?php echo $szinkod; ?>; height:<?php echo round(100/count($szinek),0)-1?>%"></div>
                                        <?php } ?>
                                    </label>
                                <?php } ?>
                            </li>
                        <?php } ?>

                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
    </div>
<?php }?>