<?php
$_['hide_lastseen']          = array(
    'type'              => 'checkbox',
    'name'              => 'hide_lastseen',
    'default'           => "1",
    'heading'           => 'Utoljára megtekintettek eltűntetése'
);

$_['hide_featured']          = array(
    'type'              => 'checkbox',
    'name'              => 'hide_featured',
    'default'           => "1",
    'heading'           => 'Kiemelt akciók eltűntetése'
);

$_['hide_webshop']          = array(
    'type'              => 'checkbox',
    'name'              => 'hide_webshop',
    'default'           => "1",
    'heading'           => 'Webshop eltűntetése'
);

?>