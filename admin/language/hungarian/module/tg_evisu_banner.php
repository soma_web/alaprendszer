<?php
// Heading
$_['heading_title']       = 'NMS fejléc banner';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Siker: Banner sikeresen módosítva!';
$_['text_content_top']    = 'Tartalom Teteje';
$_['text_content_bottom'] = 'Tartalom Alja';
$_['text_column_left']    = 'Bal Oszlop';
$_['text_column_right']   = 'Jobb Oszlop';

// Entry
$_['entry_banner']        = 'Banner:';
$_['entry_dimension']     = 'Felbontás (SZ x M):';
$_['entry_layout']        = 'Kiosztás:';
$_['entry_position']      = 'Pozició:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';

// Error
$_['error_permission']    = 'Figyelem nincs jogosultsága módosítani!';
$_['error_dimension']     = 'Szélesség &amp; Magasság szükséges!';
?>