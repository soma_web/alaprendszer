<?php
$_['extension_name']                    = 'Kérdezz felelek (Termékekkel kapcsolatban)';

// Heading
$_['heading_title']                     = '<i class="fa" style="font-size:14px;color:#F7951D;"></i> <strong>' . $_['extension_name'] . '</strong>';

// Buttons
$_['button_apply']                      = 'Alkalmaz';
$_['button_upgrade']                    = 'Frissítés';

// Tabs
$_['tab_settings']                      = 'Beállítások';
$_['tab_form']                          = 'Űrlap';
$_['tab_qa']                            = 'Kérdezz felelek';
$_['tab_support']                       = 'Támogatás';
$_['tab_about']                         = 'Rólunk';
$_['tab_general']                       = 'Általános';
$_['tab_faq']                           = 'FAQ';
$_['tab_services']                      = 'Szolgáltatások';
$_['tab_changelog']                     = 'Log';
$_['tab_extension']                     = 'Modul';

// Text
$_['text_success_upgrade']              = '<strong>Siker!</strong> Sikeresen frissült a ' . $_['extension_name'] . ' a modul a következő verzióra: <strong>%s</strong>!';
$_['text_success_update']               = '<strong>Sikeres módosítás!</strong> A változtatások mentésre kerültek';
$_['text_toggle_navigation']            = 'Toggle navigation';
$_['text_license']                      = 'License';
$_['text_extension_information']        = 'Extension information';
$_['text_legal_notice']                 = 'Legal notice';
$_['text_terms']                        = 'Terms &amp; Conditions';
$_['text_support_subject']              = $_['extension_name'] . ' support needed';
$_['text_license_text']                 = 'Please be aware that this product has a <strong>per-domain license</strong>, meaning you can use it <em>only on a single domain</em> (sub-domains count as separate domains). <strong>You will need to purchase a separate license for each domain you wish to use this extension on.</strong>';
$_['text_other_extensions']             = 'If you like this extension you might also be interested in <a href="%s" class="alert-link" target="_blank">my other extensions</a>.';
$_['text_module']                       = 'Modulok';
$_['text_faq']                          = 'Frequently Asked Questions';
$_['text_first_page']                   = 'Kezdőlap';
$_['text_all']                          = 'Minden';
$_['text_store_email_address']          = 'Áruház e-mail címe';
$_['text_customer_email_address']       = 'Vásárló e-mail címe';

// Help texts
$_['help_new_question_notification']    = 'Értesítő e-mail küldése a meghatározott e-mail cím(ek)re, amikor új kérdés érkezik.';
$_['help_question_reply_notification']  = 'Értesítő e-mail küldése a kérdezőnek miután a kérdése megválaszolásra került az admin felüleleten.';
$_['help_notificatin_emails']           = 'Több e-mail címre történő értesítő üzenet küldéséhez vesszővel elválasztva sorolja fel a címzetteket. Többnyelvű áruház esetén nyelvenként eltérő e-mail címet is megadhat.';
$_['help_notificatin_email_from']       = 'Az e-mail cím, amelyről az értesítő üzenet kiküldésre kerül. Abban az esetben, ha a vásárló e-mail címe van kiválasztva, de a vásárló nem adott meg e-mail címet, az áruház e-mail címéről megy ki az üzenet.';
$_['help_display_questions']            = 'Kikapcsolt állapotban nem jelennek meg a kérdések a termék aloldalon.';
$_['help_items_per_page']               = '0 érték esetén az összes kérdés és válasz megjelenik';
$_['help_preload']                      = 'Kérdések és válaszok betöltése AJAX hívás nélkül a SEO láthatóság érdekében.';
$_['help_display_all_languages']        = 'Kikapcsolt állapotban az áruházban csak a kiválasztott nyelven jelennek meg a kérdések és feleletek.';
$_['help_remove_sql_changes']           = 'SQL változások törlése a modul <strong>eltávolításakor</strong>.';

// Entry
$_['entry_installed_version']           = 'Verzió:';
$_['entry_extension_status']            = 'Modul állapota:';
$_['entry_new_question_notification']   = 'Új kérdés érkezett értesítő:';
$_['entry_notificatin_emails']          = 'Értesítő e-mail címek:';
$_['entry_notificatin_email_from']      = 'Értesítő üzenet kiküldése erről az e-mail címről:';
$_['entry_question_reply_notification'] = 'Megválaszolt kérdés értesítő:';
$_['entry_display_all_languages']       = 'Kérdések megjelenítése minden nyelven:';
$_['entry_remove_sql_changes']          = 'SQL változtatások eltávolítása:';
$_['entry_form_display_name']           = 'Név mező megjelenítése:';
$_['entry_form_require_name']           = 'Kötelező a név mező kitöltése?';
$_['entry_form_display_email']          = 'E-mail mező megjelenítése:';
$_['entry_form_require_email']          = 'Kötelező az e-mail mező kitöltése?';
$_['entry_form_display_phone']          = 'Telefonszám mező megjelenítése:';
$_['entry_form_require_phone']          = 'Kötelező a telefonszám mező kitöltése?';
$_['entry_form_display_custom']         = 'Egyéb mező megjelenítése:';
$_['entry_form_require_custom']         = 'Kötelező az egyéb mező kitöltése?';
$_['entry_form_custom_field_name']      = 'Egyéb mező címe:';
$_['entry_form_display_captcha']        = 'Captcha megjelenítése:';
$_['entry_form_require_captcha']        = 'Kötelező a captcha mező kitöltése?';
$_['entry_display_questions']           = 'Kérdezz felelek megjelenítése:';
$_['entry_new_questions_status']        = 'Új kérdés állapota:';
$_['entry_items_per_page']              = 'Kérdések száma oldalanként:';
$_['entry_preload']                     = 'Kérdések betöltése előre:';
$_['entry_display_question_author']     = 'Kérdező nevének megjelenítése:';
$_['entry_display_question_date']       = 'Kérdés létrehozásának időpontjának megjelenítése:';
$_['entry_display_answer_author']       = 'Válaszadó nevének megjelenítése:';
$_['entry_display_answer_date']         = 'Válasz létrehozásának időpontjának megjelenítése:';
$_['entry_extension_name']              = 'Név:';
$_['entry_extension_compatibility']     = 'Kompatibilitás:';
$_['entry_extension_store_url']         = 'Áruház URL:';
$_['entry_copyright_notice']            = 'Szerzői jog megjegyzés:';

// Error
$_['error_permission']                  = '<strong>Hiba!</strong> Önnek nincs jogosultsága a következő modul módosítására: ' . $_['extension_name'] . '!';
$_['error_warning']                     = '<strong>Figyelem!</strong> Kérem, nézze át az űrlapot újra!';
$_['error_vqmod']                       = '<strong>Error!</strong> vQmod does not seem to be installed. <a href="http://code.google.com/p/vqmod/" class="alert-link">Get vQmod!</a>';
$_['error_missing_table']               = '<strong>Hiba!</strong> Az SQL adatbázisból hiányzik a következő tábla: \'%s\'!';
$_['error_missing_column']              = '<strong>Hiba!</strong> A(z) \'%s\' SQL táblából hiányzik a következő oszlop: \'%s\'!';
$_['error_unsaved_settings']            = '<strong>Figyelem!</strong> Önnek nem mentett beállításai vannak! Kérem, mentse el őket.';
$_['error_version']                     = '<strong>Információ!</strong> ' . $_['extension_name'] . ' verzióhoz tartozó <strong>%s</strong> fájlokat találtunk. Frissítse a ' . $_['extension_name'] . '-et!';
$_['error_upgrade_database']            = '<strong>Hiba!</strong> Az adatbázis szerkezetet nem tudtuk frissíteni!';
$_['error_ajax_request']                = 'AJAX hiba történt!';
$_['error_items_per_page']              = 'Ez az érték csak pozitív szám lehet, nagyobb vagy egyenlő mint 0.';
$_['error_custom_field_name']           = 'Kérem, adja meg az egyéb mező nevét!';
$_['error_missing_email']               = 'Legalább egy e-mail címet meg kell adnia!';
$_['error_email']                       = 'A mező érvénytelen e-mail cím(ek)et tartalmaz!';
?>
