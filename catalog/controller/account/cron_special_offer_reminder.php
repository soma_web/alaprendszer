<?php   
class ControllerAccountCronSpecialOfferReminder extends Controller {
	public function index() {
	
		$this->language->load('mail/special_offer_reminder');
		
		$this->load->model('module/special_offer_reminder');	
		$this->load->model('catalog/product');	
		
		if (!isset($this->request->get['secret_code'])){
			echo "You forgot secret code";
			exit;
		}
		
		if ($this->request->get['secret_code'] != $this->config->get('special_offer_reminder_secret_code')){
			echo "Access Denied: Wrong secret code";
			exit;
		}
		
		$subscribers = $this->model_module_special_offer_reminder->getSubscribers();
		
		$subject = $this->language->get('text_subject'); 
		
		foreach($subscribers as $subscriber){
			$message = sprintf($this->language->get('text_hi'), $subscriber['name']) . "\n\n";
			$message .= $this->language->get('text_info') . "\n\n";
			
			$reminders = $this->model_module_special_offer_reminder->getSubscriberReminders($subscriber['email']);
			$sendEmail = false;
			
			foreach($reminders as $reminder){
				$product_info = $this->model_catalog_product->getProduct($reminder['product_id']);
				
				if ($product_info){
					
					if ((float)$product_info['special']) {
						$price = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					}
					
					$expire_date = date($this->language->get('date_format_short'), strtotime($reminder['date_end']));	
					
					$message .= sprintf($this->language->get('text_offer_info'), $this->url->link('product/product', 'product_id=' . $product_info['product_id'], 'SSL'),  $product_info['name'], $price, $expire_date);
					$message .= "\n";
					
					$this->model_module_special_offer_reminder->deleteReminder($reminder['reminder_id']);
					
					$sendEmail = true;
					
				}
			}
			
			$message .= "\n\n" . $this->language->get('text_thanks') . "\n";
			$message .= $this->config->get('config_name');
			$message = nl2br($message);
			
			if ($sendEmail){
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');				
				$mail->setTo($subscriber['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject($subject);
				$mail->setHtml($message);
				$mail->send();
				
				echo "Mail sent to: " . $subscriber['email'] . "<br />";
			}
		}
	} 	
}
?>