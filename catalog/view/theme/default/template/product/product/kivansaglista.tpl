<div class="osszehasonlit_kivansag">
    <span class="wishlist">
        <a class="kivansag" onclick="addToWishList('<?php echo $product_id; ?>');">
            <?php if(isset($beallitas_termek_aloldal['wishlist_icon']) && $beallitas_termek_aloldal['wishlist_icon']) { ?>
                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/wishlist.png')) {
                    $icon = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/wishlist.png';
                } else {
                    $icon = DIR_TEMPLATE_IMAGE.'default/image/wishlist.png';
                } ?>
                <img class="product-icon-wishlist" title="<? if (isset($button_wishlist)) { echo $button_wishlist; }?>"  src="<? echo $icon?>" >
            <?php } ?>
            <?php if(isset($beallitas_termek_aloldal['wishlist_text']) && $beallitas_termek_aloldal['wishlist_text']) { ?>
                <span><?php echo $button_wishlist; ?></span>
            <?php } ?>
        </a>
    </span>
</div>
