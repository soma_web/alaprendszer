<?php
class ControllerModulePackage extends Controller {
	protected function index($setting) {
		$this->language->load('module/package',$this->config->get('config_template'));
        $this->data['package_heading_title'] = $this->language->get('heading_title');
        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
		$this->data['kaphato']          = $this->language->get('kaphato');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
		$this->data['button_cart']      = $this->language->get('button_cart');
		$this->data['button_set_valos_ar']      = $this->language->get('button_set_valos_ar');
        $this->data['button_bovebben']  = $this->language->get('button_bovebben');
        $this->data['text_tax']         = $this->language->get('text_tax');
        $this->data['text_netto']       = $this->language->get('text_netto');
        $this->data['module_name']      = "package";
        $this->data['button_wishlist']  = $this->language->get('button_wishlist');
        $this->data['button_compare']   = $this->language->get('button_compare');
        $this->data['heading_title']    = $this->language->get('heading_title');

        $this->data['text_akcio_title']         = $this->language->get('text_akcio_title');
        $this->data['text_pardarab_title']      = $this->language->get('text_pardarab_title');
        $this->data['text_uj_title']            = $this->language->get('text_uj_title');
        $this->data['text_eredeti_ar']          = $this->language->get('text_eredeti_ar');

        if ($this->config->get('package_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

        $this->load->model('catalog/product');
		
		$this->load->model('tool/image');

		$this->data['packages'] = array();

		$results = $this->model_catalog_product->getPackages(0,$setting['limit']);



        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );

        $results1 = $this->model_catalog_product->getProductsNew($data1);
        $this->data['position'] = $setting['position'];
        if ($this->config->get('megjelenit_korhinta') == 1) {
            if (isset($setting['show']) ) {
                $this->data['show'] = $setting['show'];
            } else {
                $this->data['show'] = 0;
            }
        } else {
            $this->data['show'] = 0;
        }
        $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
        $this->data['leiras_karakter_racs']     = $this->config->get('megjelenit_description_karakterek');

        $i = 0;
		foreach ($results as $result) {

            $result = $this->model_catalog_product->getProduct($result['product_id']);

            $this->data['packages'][$i]['package'] = $this->product->productPreparation($result,$setting,$results1);

            $products = $this->model_catalog_product->getProductsPackage($result['product_id']);
            $eredeti_ar = 0;
            foreach($products as $product) {

                $product = $this->model_catalog_product->getProduct($product['package_id']);

                $this->data['packages'][$i]['products'][] = $this->product->productPreparation($product,$setting,$results1);

                if (isset($product['special']) && $product['special'] <  $product['price'] && $product['special'] > 0) {
                    $eredeti_ar += $product['special'];
                } else {
                    $eredeti_ar += $product['price'];
                }
            }
            $this->data['packages'][$i]['package']['eredeti_ar'] =   $this->currency->format($this->tax->calculate($eredeti_ar, $product['tax_class_id'], $this->config->get('config_tax')));

            $i++;
        }


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/package.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/package.tpl';
        } else {
            $this->template = 'default/template/module/package.tpl';
        }


        $this->render();
	}
}
?>