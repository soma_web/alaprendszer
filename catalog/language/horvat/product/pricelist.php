<?php
// Heading
$_['heading_title']     = 'Árlista';

// Header Bar Title
$_['text_all_products'] = 'Összes termék';

// Text
$_['text_limit']        = 'Termékek oldalanként';
$_['text_empty']        = 'nincsenek kilistázandó termékek.';
$_['text_stars']        = '%s 5 csillagból';
$_['text_catagory']     = 'Kategória:';


$_['text_sort']         = 'Rendezés:';
$_['text_default']      = 'Alapértelmezett';
$_['text_name_asc']     = 'Név szerint növekvő';
$_['text_name_desc']    = 'Név szerint csökkenő';
$_['text_price_asc']    = 'Olcsóbbtól a drágábbig';
$_['text_price_desc']   = 'Drágábbtól az olcsóbbig';
$_['text_rating_asc']   = 'Értékelés (Legrosszabbak elől)';
$_['text_rating_desc']  = 'Értékelés (Legjobbak elől)';
$_['text_model_asc']    = 'Cikkszám szerint növekvő';
$_['text_model_desc']   = 'Cikkszám szerint csökkenő';

$_['text_addcart']      = 'Kosárba tesz';
$_['text_print']        = 'Nyomtatás';
$_['text_notfound']     = 'Termék nem található!';
$_['text_pricelist']    = 'Árlista';
?>