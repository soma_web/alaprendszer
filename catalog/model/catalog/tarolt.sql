


SELECT p.product_id, (SELECT AVG(rating) AS total FROM hungaro_review r1 WHERE r1.product_id = p.product_id AND r1.status = '1'
                    GROUP BY r1.product_id) AS rating
                    FROM hungaro_product p
                    LEFT JOIN ( SELECT * FROM
                            (SELECT price, product_id, customer_group_id FROM hungaro_product_special
                                  WHERE  (date_start <= curdate() OR date_start = '0000-00-00') AND (date_end >= curdate()  OR date_end = '0000-00-00' AND customer_group_id='2')
                                 ORDER BY priority DESC ) AS ps2 WHERE customer_group_id='2'  GROUP BY ps2.product_id ) AS special ON (p.product_id=special.product_id)

                LEFT JOIN hungaro_product_customer_special vevo ON (p.product_id = vevo.product_id AND customer_id=0)
                LEFT JOIN hungaro_product_description pd ON (p.product_id = pd.product_id)
                LEFT JOIN hungaro_product_to_store p2s ON (p.product_id = p2s.product_id)

                LEFT JOIN hungaro_tax_rule tr ON (p.tax_class_id = tr.tax_class_id AND tr.based='store')
                LEFT JOIN hungaro_tax_rate trate ON (tr.tax_rate_id=trate.tax_rate_id)
                LEFT JOIN hungaro_product_to_category p2c ON (p.product_id = p2c.product_id)
                LEFT JOIN hungaro_product_attribute pa ON (p.product_id = pa.product_id)
                WHERE pd.language_id = '2'
                AND p.status = '1'
                AND p.date_available <= curdate()
                AND p2s.store_id = '0'
                AND (
                      (   (special.price <= vevo.price OR vevo.price IS NULL)
                          AND  special.price > 0
                          AND ( (special.price < p.price
                                  AND abs(special.price) <= abs(p.price)*1
                                  AND ( p.tax_class_id > 0
                                      AND ( (trate.type='P' AND  round(special.price*(trate.rate/100+1)*1) >= '260700') OR  (trate.type='F' AND  (round(special.price+(trate.rate)*1)) >= '260700') )
                                      OR   (p.tax_class_id = 0  AND abs(special.price)*1 >= '260700')
                                      )
                                )
                                OR
                                (
                                    ( p.tax_class_id > 0
                                      AND ( (trate.type='P' AND  round(p.price*(trate.rate/100+1)*1) >= '260700') OR  (trate.type='F' AND  (round(p.price+(trate.rate)*1)) >= '260700') )
                                      OR   (p.tax_class_id = 0  AND abs(p.price)*1 >= '260700')
                                      )
                                )
                              )
                      )

                      OR
                      (   (vevo.price < special.price OR special.price IS NULL)
                          AND  vevo.price > 0
                          AND vevo.price < p.price
                          AND abs(vevo.price) <= abs(p.price)*1
                          AND ( p.tax_class_id > 0
                              AND ( (trate.type='P' AND  round(vevo.price*(trate.rate/100+1)*1) >= '260700') OR  (trate.type='F' AND  (round(vevo.price+(trate.rate)*1)) >= '260700') )
                              OR   (p.tax_class_id = 0  AND abs(vevo.price)*1 >= '260700')
                              )
                      )

                      OR
                      ( (abs(p.price)*1 < special.price OR special.price IS NULL)
                          AND (abs(p.price)*1 < vevo.price OR vevo.price IS NULL)
                          AND ( p.tax_class_id > 0 AND ( (trate.type='P' AND  round(p.price*(trate.rate/100+1)*1) >= '260700') OR  (trate.type='F' AND  (round(p.price+(trate.rate)*1)) >= '260700') )
                              OR   (p.tax_class_id = 0  AND abs(p.price)*1 >= '260700')
                              )
                      )
                    )

                AND (
                      ( (special.price <= vevo.price OR vevo.price IS NULL)
                        AND  special.price > 0
                        AND special.price < p.price
                        AND abs(special.price) <= abs(p.price)*1
                        AND ( p.tax_class_id > 0
                              AND ( (trate.type='P' AND  round(special.price*(trate.rate/100+1)*1) <= '528320') OR  (trate.type='F' AND  (round(special.price+(trate.rate)*1)) <= '528320'))
                              OR   (p.tax_class_id = 0  AND abs(special.price)*1 <= '528320')
                            )
                      )
                      OR (vevo.price < special.price OR special.price IS NULL) AND  vevo.price > 0 AND vevo.price < p.price AND abs(vevo.price) <= abs(p.price)*1  AND ( p.tax_class_id > 0  AND ( (trate.type='P' AND  round(vevo.price*(trate.rate/100+1)*1) <= '528320')
                                                  OR  (trate.type='F' AND  (round(vevo.price+(trate.rate)*1)) <= '528320')
                                                )
                    OR   (p.tax_class_id = 0  AND abs(vevo.price)*1 <= '528320')
                      )

                      OR (abs(p.price)*1 < special.price OR special.price IS NULL) AND (abs(p.price)*1 < vevo.price OR vevo.price IS NULL)  AND ( p.tax_class_id > 0  AND ( (trate.type='P' AND  round(p.price*(trate.rate/100+1)*1) <= '528320')
                                                  OR  (trate.type='F' AND  (round(p.price+(trate.rate)*1)) <= '528320')
                                                )
                    OR   (p.tax_class_id = 0  AND abs(p.price)*1 <= '528320')
                      )
                    )
                AND p2c.category_id IN(26) AND (  (  ( replace(pa.text,'.','')  >=500 AND pa.attribute_id=115)  )  )
                AND pa.language_id='2'
                GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT 0,10





SELECT p.product_id, (SELECT AVG(rating) AS total FROM hungaro_review r1 WHERE r1.product_id = p.product_id AND r1.status = '1'
                    GROUP BY r1.product_id) AS rating
                    FROM hungaro_product p
                    LEFT JOIN ( SELECT * FROM
                            (SELECT price, product_id, customer_group_id FROM hungaro_product_special
                                  WHERE  (date_start <= curdate() OR date_start = '0000-00-00') AND (date_end >= curdate()  OR date_end = '0000-00-00' AND customer_group_id='2')
                                 ORDER BY priority DESC ) AS ps2 WHERE customer_group_id='2'  GROUP BY ps2.product_id ) AS special ON (p.product_id=special.product_id)

                LEFT JOIN hungaro_product_customer_special vevo ON (p.product_id = vevo.product_id AND customer_id=0)
                LEFT JOIN hungaro_product_description pd ON (p.product_id = pd.product_id)
                LEFT JOIN hungaro_product_to_store p2s ON (p.product_id = p2s.product_id)

                LEFT JOIN hungaro_tax_rule tr ON (p.tax_class_id = tr.tax_class_id AND tr.based='store')
                LEFT JOIN hungaro_tax_rate trate ON (tr.tax_rate_id=trate.tax_rate_id)
                LEFT JOIN hungaro_product_to_category p2c ON (p.product_id = p2c.product_id)
                LEFT JOIN hungaro_product_attribute pa ON (p.product_id = pa.product_id)
                WHERE pd.language_id = '2'
                AND p.status = '1'
                AND p.date_available <= curdate()
                AND p2s.store_id = '0'
                AND (
                      ( (special.price <= vevo.price OR vevo.price IS NULL) AND  special.price > 0 AND special.price < p.price AND abs(special.price) <= abs(p.price)*1  AND
                        ( p.tax_class_id > 0  AND ( (trate.type='P' AND  round(special.price*(trate.rate/100+1)*1) >= '380619') OR  (trate.type='F' AND  (round(special.price+(trate.rate)*1)) >= '380619') )
                        OR   (p.tax_class_id = 0  AND abs(special.price)*1 >= '380619') )
                      )

                      OR (vevo.price < special.price OR special.price IS NULL)
                      AND  vevo.price > 0
                      AND vevo.price < p.price
                      AND abs(vevo.price) <= abs(p.price)*1
                      AND ( p.tax_class_id > 0
                            AND ( (trate.type='P' AND  round(vevo.price*(trate.rate/100+1)*1) >= '380619') OR  (trate.type='F' AND  (round(vevo.price+(trate.rate)*1)) >= '380619') )
                            OR   (p.tax_class_id = 0  AND abs(vevo.price)*1 >= '380619')
                          )

                      OR (abs(p.price)*1 < special.price OR special.price IS NULL)
                      AND (abs(p.price)*1 < vevo.price OR vevo.price IS NULL)
                      AND ( p.tax_class_id > 0  AND ( (trate.type='P' AND  round(p.price*(trate.rate/100+1)*1) >= '380619')
                                                  OR  (trate.type='F' AND  (round(p.price+(trate.rate)*1)) >= '380619') )
                          OR   (p.tax_class_id = 0  AND abs(p.price)*1 >= '380619') )
                    )

                AND (
                      ( (special.price <= vevo.price OR vevo.price IS NULL)
                        AND  special.price > 0
                        AND special.price < p.price
                        AND abs(special.price) <= abs(p.price)*1
                        AND ( p.tax_class_id > 0
                            AND (
                                  (trate.type='P' AND  round(special.price*(trate.rate/100+1)*1) <= '528320')
                                  OR  (trate.type='F' AND  (round(special.price+(trate.rate)*1)) <= '528320')
                                )
                            OR   (p.tax_class_id = 0  AND abs(special.price)*1 <= '528320')
                            )
                      )
                      OR (vevo.price < special.price OR special.price IS NULL) AND  vevo.price > 0 AND vevo.price < p.price AND abs(vevo.price) <= abs(p.price)*1  AND ( p.tax_class_id > 0  AND ( (trate.type='P' AND  round(vevo.price*(trate.rate/100+1)*1) <= '528320')
                                                  OR  (trate.type='F' AND  (round(vevo.price+(trate.rate)*1)) <= '528320')
                                                )
                    OR   (p.tax_class_id = 0  AND abs(vevo.price)*1 <= '528320')
                      )  OR (abs(p.price)*1 < special.price OR special.price IS NULL) AND (abs(p.price)*1 < vevo.price OR vevo.price IS NULL)  AND ( p.tax_class_id > 0  AND ( (trate.type='P' AND  round(p.price*(trate.rate/100+1)*1) <= '528320')
                                                  OR  (trate.type='F' AND  (round(p.price+(trate.rate)*1)) <= '528320')
                                                )
                    OR   (p.tax_class_id = 0  AND abs(p.price)*1 <= '528320')
                      )
                    )
                AND p2c.category_id IN(26) AND (  (  ( replace(pa.text,'.','')  >=500 AND pa.attribute_id=115)  )  )
                AND pa.language_id='2'
                GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT 0,10