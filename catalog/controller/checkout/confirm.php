<?php 
class ControllerCheckoutConfirm extends Controller { 
	public function index() {
		$redirect = '';
		
		if ($this->cart->hasShipping()) {
			// Validate if shipping address has been set.		
			$this->load->model('account/address');
	
			if ($this->customer->isLogged() && isset($this->session->data['shipping_address_id'])) {					
				$shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);		
			} elseif (isset($this->session->data['guest'])) {
				$shipping_address = $this->session->data['guest']['shipping'];
			}
			
			if (empty($shipping_address)) {								
				$redirect = $this->url->link('checkout/checkout', '', 'SSL');
			}
			
			// Validate if shipping method has been set.	
			if (!isset($this->session->data['shipping_method'])) {
				$redirect = $this->url->link('checkout/checkout', '', 'SSL');
			}
		} else {
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
		}
		
		// Validate if payment address has been set.
		$this->load->model('account/address');
		
		if ($this->customer->isLogged() && isset($this->session->data['payment_address_id'])) {
			$payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);		
		} elseif (isset($this->session->data['guest'])) {
			$payment_address = $this->session->data['guest']['payment'];
		}	
				
		if (empty($payment_address)) {
			$redirect = $this->url->link('checkout/checkout', '', 'SSL');
		}			
		
		// Validate if payment method has been set.	
		if (!isset($this->session->data['payment_method'])) {
            if ($this->config->get('megjelenit_penztar_fizetesi_mod') == 1) {
			    $redirect = $this->url->link('checkout/checkout', '', 'SSL');
            } else {


                // Payment Methods

              /*  $this->session->data['payment_method'] = $this->session->data['shipping_method'];
                $this->session->data['payment_method']['sort_order'] = 1;*/


                // totál kiszámítása

                $total_data = array();
                $total = 0;
                $taxes = $this->cart->getTaxes();

                $this->load->model('setting/extension');

                $sort_order = array();

                $results = $this->model_setting_extension->getExtensions('total');

                foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                }

                array_multisort($sort_order, SORT_ASC, $results);

                foreach ($results as $result) {
                    if ($this->config->get($result['code'] . '_status')) {
                        $this->load->model('total/' . $result['code']);

                        $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                    }
                }

                $this->load->model('payment/free_checkout');
                $method = $this->{'model_payment_free_checkout'}->getMethod($payment_address, $total);
                $this->session->data['payment_methods'] = $method;
                $this->session->data['payment_method'] = $method;

                // Fizetési módok

              /*  $method_data = array();

                $this->load->model('setting/extension');

                $results = $this->model_setting_extension->getExtensions('payment');

                foreach ($results as $result) {
                    if ($this->config->get($result['code'] . '_status') ) {
                        $this->load->model('payment/' . $result['code']);

                        $method = $this->{'model_payment_' . $result['code']}->getMethod($payment_address, $total);

                        if ($method) {
                            $method_data[$result['code']] = $method;
                        }
                    }

                }

                $sort_order = array();

                foreach ($method_data as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }

                array_multisort($sort_order, SORT_ASC, $method_data);

                $this->session->data['payment_methods'] = $method_data;
            }

            if (empty($this->session->data['payment_methods'])) {
                $this->data['error_warning'] = sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'));
            } else {
                $this->data['error_warning'] = '';
            }

            if (isset($this->session->data['payment_methods'])) {
                $this->data['payment_methods'] = $this->session->data['payment_methods'];
            } else {
                $this->data['payment_methods'] = array();
            }

            if (isset($this->session->data['payment_method']['code'])) {
                $this->data['code'] = $this->session->data['payment_method']['code'];
            } else {
                $this->data['code'] = '';
            }*/
		}
		}

		// Validate cart has products and has stock.	
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$redirect = $this->url->link('checkout/cart');				
		}	
		
		// Validate minimum quantity requirments.			
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
            if ($product['utalvany'] != 1) {
                $product_total = 0;

                foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $product['product_id']) {
                        $product_total += $product_2['quantity'];
                    }
                }

                if ($product['minimum'] > $product_total) {
                    $redirect = $this->url->link('checkout/cart');

                    break;
                }
			}
		}
						
		if (!$redirect) {
			$total_data = array();
			$total = 0;
			$taxes = $this->cart->getTaxes();
			 
			$this->load->model('setting/extension');
			
			$sort_order = array(); 
			
			$results = $this->model_setting_extension->getExtensions('total');
			
			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}
			
			array_multisort($sort_order, SORT_ASC, $results);
			
			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);
		
					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}
			
			$sort_order = array(); 
		  
			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}
	
			array_multisort($sort_order, SORT_ASC, $total_data);
	
			$this->language->load('checkout/checkout');
			
			$data = array();
			
			$data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
			$data['store_id'] = $this->config->get('config_store_id');
			$data['store_name'] = $this->config->get('config_name');
			
			if ($data['store_id']) {
				$data['store_url'] = $this->config->get('config_url');		
			} else {
				$data['store_url'] = HTTP_SERVER;	
			}
			
			if ($this->customer->isLogged()) {
				$data['customer_id'] = $this->customer->getId();
				$data['customer_group_id'] = $this->customer->getCustomerGroupId();
				$data['firstname'] = $this->customer->getFirstName();
				$data['lastname'] = $this->customer->getLastName();
				$data['email'] = $this->customer->getEmail();
				$data['telephone'] = $this->customer->getTelephone();
				$data['fax'] = $this->customer->getFax();
				$data['nem'] = $this->customer->getNem();
				$data['eletkor'] = $this->customer->getEletkor();
                $data['iskolai_vegzettseg'] = $this->customer->getIskolaiVegzettseg();
                $data['vallalkozasi_forma'] = $this->customer->getVallalkozasiForma();
                $data['szekhely'] = $this->customer->getSzekhely();
                $data['ugyvezeto_neve'] = $this->customer->getUgyvezetoNeve();
                $data['ugyvezeto_telefonszama'] = $this->customer->getUgyvezetoTelefonszama();
                $data['weblap'] = $this->customer->getWeblap();

				$this->load->model('account/address');
				
				$payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
			} elseif (isset($this->session->data['guest'])) {
				$data['customer_id'] = 0;
				$data['customer_group_id'] = $this->config->get('config_customer_group_id');
				$data['firstname'] = isset($this->session->data['guest']['firstname']) ? $this->session->data['guest']['firstname'] : "";
				$data['lastname'] = isset($this->session->data['guest']['lastname']) ? $this->session->data['guest']['lastname'] : "";
				$data['email'] = isset($this->session->data['guest']['email']) ? $this->session->data['guest']['email'] : "";
				$data['telephone'] = isset($this->session->data['guest']['telephone']) ? $this->session->data['guest']['telephone'] : "";
				$data['fax'] = isset($this->session->data['guest']['fax']) ? $this->session->data['guest']['fax'] : "";
                $data['nem'] = isset($this->session->data['guest']['nem']) ? $this->session->data['guest']['nem'] : "";
                $data['eletkor'] = isset($this->session->data['guest']['eletkor']) ? $this->session->data['guest']['eletkor'] : "";
                $data['iskolai_vegzettseg'] = isset($this->session->data['guest']['iskolai_vegzettseg']) ? $this->session->data['guest']['iskolai_vegzettseg'] : "";
                $data['vallalkozasi_forma'] = isset($this->session->data['guest']['vallalkozasi_forma']) ? $this->session->data['guest']['vallalkozasi_forma'] : "";
                $data['szekhely'] = isset($this->session->data['guest']['szekhely']) ? $this->session->data['guest']['szekhely'] : "";
                $data['ugyvezeto_neve'] = isset($this->session->data['guest']['ugyvezeto_neve']) ?  $this->session->data['guest']['ugyvezeto_neve'] : "";
                $data['ugyvezeto_telefonszama'] = isset($this->session->data['guest']['ugyvezeto_telefonszama']) ? $this->session->data['guest']['ugyvezeto_telefonszama'] : "";
                $data['weblap'] = isset($this->session->data['guest']['weblap']) ? $this->session->data['guest']['weblap'] : "";

                $payment_address = isset($this->session->data['guest']['payment']) ? $this->session->data['guest']['payment'] : "";
			}
			
			$data['payment_firstname'] = isset($payment_address['firstname']) ? $payment_address['firstname'] : "";
			$data['payment_lastname'] = isset($payment_address['lastname']) ? $payment_address['lastname'] : "";
			$data['payment_company'] = isset($payment_address['company']) ? $payment_address['company'] : "";
			$data['payment_adoszam'] = isset($payment_address['adoszam']) ? $payment_address['adoszam'] : "";
			$data['payment_address_1'] = isset($payment_address['address_1']) ? $payment_address['address_1'] : "";
			$data['payment_address_2'] = isset($payment_address['address_2']) ? $payment_address['address_2'] : "";
			$data['payment_city'] = isset($payment_address['city']) ? $payment_address['city'] : "";
			$data['payment_postcode'] = isset($payment_address['postcode']) ? $payment_address['postcode'] : "";
			$data['payment_zone'] = isset($payment_address['zone']) ? $payment_address['zone'] : "";
			$data['payment_zone_id'] = isset($payment_address['zone_id']) ? $payment_address['zone_id'] : "";
			$data['payment_country'] = isset($payment_address['country']) ? $payment_address['country'] : "";
			$data['payment_country_id'] = isset($payment_address['country_id']) ? $payment_address['country_id'] : "";
			$data['payment_address_format'] = isset($payment_address['address_format']) ? $payment_address['address_format'] : "";

            $data['payment_vallalkozasi_forma'] = isset($payment_address['vallalkozasi_forma']) ? $payment_address['vallalkozasi_forma'] : "";
            $data['payment_szekhely'] = isset($payment_address['szekhely']) ? $payment_address['szekhely'] : "";
            $data['payment_ugyvezeto_neve'] = isset($payment_address['ugyvezeto_neve']) ? $payment_address['ugyvezeto_neve'] : "";
            $data['payment_ugyvezeto_telefonszama'] = isset($payment_address['ugyvezeto_telefonszama']) ? $payment_address['ugyvezeto_telefonszama'] : "";
		
			if (isset($this->session->data['payment_method']['title'])) {
				$data['payment_method'] = $this->session->data['payment_method']['title'];
			} else {
				$data['payment_method'] = '';
			}


            if (isset($payment_address)) {
                $this->session->data['payment_address'] = $payment_address;
            } else {
                $this->session->data['payment_address'] = '';
            }
            if (isset($shipping_address)) {
                $this->session->data['shipping_address'] = $shipping_address;
            } else {
                $this->session->data['shipping_address'] = '';
            }

			if (isset($this->session->data['payment_method']['code'])) {
				$data['payment_code'] = $this->session->data['payment_method']['code'];
			} else {
				$data['payment_code'] = '';
			}
						
			if ($this->cart->hasShipping()) {
				if ($this->customer->isLogged()) {
					$this->load->model('account/address');
					
					$shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);	
				} elseif (isset($this->session->data['guest'])) {
					$shipping_address = $this->session->data['guest']['shipping'];
				}			
				
				$data['shipping_firstname'] = isset($shipping_address['firstname']) ? $shipping_address['firstname'] : "";
				$data['shipping_lastname'] = isset($shipping_address['lastname']) ? $shipping_address['lastname'] : "";
				$data['shipping_company'] = isset($shipping_address['company']) ? $shipping_address['company'] : "";
				$data['shipping_adoszam'] = isset($shipping_address['adoszam']) ? $shipping_address['adoszam'] : "";
				$data['shipping_address_1'] = isset($shipping_address['address_1']) ? $shipping_address['address_1'] : "";
				$data['shipping_address_2'] = isset($shipping_address['address_2']) ? $shipping_address['address_2'] : "";
				$data['shipping_city'] = isset($shipping_address['city']) ? $shipping_address['city'] : "";
				$data['shipping_postcode'] = isset($shipping_address['postcode']) ? $shipping_address['postcode'] : "";
				$data['shipping_zone'] = isset($shipping_address['zone']) ? $shipping_address['zone'] : "";
				$data['shipping_zone_id'] = isset($shipping_address['zone_id']) ? $shipping_address['zone_id'] : "";
				$data['shipping_country'] = isset($shipping_address['country']) ? $shipping_address['country'] : "";
				$data['shipping_country_id'] = isset($shipping_address['country_id']) ? $shipping_address['country_id'] : "";
				$data['shipping_address_format'] = isset($shipping_address['address_format']) ? $shipping_address['address_format'] : "";

                $data['shipping_vallalkozasi_forma'] = isset($shipping_address['vallalkozasi_forma']) ? $shipping_address['vallalkozasi_forma'] : "";
                $data['shipping_szekhely'] = isset($shipping_address['szekhely']) ? $shipping_address['szekhely'] : "";
                $data['shipping_ugyvezeto_neve'] = isset($shipping_address['ugyvezeto_neve']) ? $shipping_address['ugyvezeto_neve'] : "";
                $data['shipping_ugyvezeto_telefonszama'] = isset($shipping_address['ugyvezeto_telefonszama']) ? $shipping_address['ugyvezeto_telefonszama'] : "";
			
				if (isset($this->session->data['shipping_method']['title'])) {
					$data['shipping_method'] = $this->session->data['shipping_method']['title'];
				} else {
					$data['shipping_method'] = '';
				}

                $data['shipping_warehouse_name'] = "";
                $data['shipping_warehouse_email'] = "";
                $data['shipping_warehouse_id'] = 0;
                if (isset($this->session->data['shipping_method']['code']) && $this->session->data['shipping_method']['code'] == "pickup.pickup") {
                    if (isset($this->session->data['shipping_method']['warehouse_id'])) {
                        $this->load->model("warehouse/warehouse");
                        $warehouse = $this->model_warehouse_warehouse->getWarehouses($this->session->data['shipping_method']['warehouse_id']);
                        $data['shipping_warehouse_id']      = $this->session->data['shipping_method']['warehouse_id'];
                        $data['shipping_warehouse_name']    = $warehouse[0]['address'];
                        $data['shipping_warehouse_email']   = $warehouse[0]['warehouse_email'];
                    }
                }

                $data['shipping_gls_pont_name'] = "";
                $data['shipping_gls_pont_city'] = "";
                $data['shipping_gls_pont_address'] = "";
                $data['shipping_gls_pont_shopid'] = "";
                $data['shipping_gls_pont_zipcode'] = "";
                $gls_statuses = explode('.',$this->session->data['shipping_method']['code']);
                $gls_status = $gls_statuses[0];
                if (isset($this->session->data['shipping_method']['code']) && isset($this->session->data['shipping_method']['gls'][$gls_status.'_status']) && $this->session->data['shipping_method']['gls'][$gls_status.'_status'] == "1" ) {
                    if (isset($this->session->data['shipping_method']['gls_pont'])) {
                        $data['shipping_gls_pont_name']      = $this->session->data['shipping_method']['gls_pont'][2];
                        $data['shipping_gls_pont_city']      = $this->session->data['shipping_method']['gls_pont'][3];
                        $data['shipping_gls_pont_address']   = $this->session->data['shipping_method']['gls_pont'][4];
                        $data['shipping_gls_pont_shopid']    = $this->session->data['shipping_method']['gls_pont'][0];
                        $data['shipping_gls_pont_zipcode']   = $this->session->data['shipping_method']['gls_pont'][1];
                    }
                } else {
                    $this->session->data['shipping_method']['gls_pont'] = null;
                }


				if (isset($this->session->data['shipping_method']['code'])) {
					$data['shipping_code'] = $this->session->data['shipping_method']['code'];
				} else {
					$data['shipping_code'] = '';
				}				
			} else {
				$data['shipping_firstname'] = '';
				$data['shipping_lastname'] = '';	
				$data['shipping_company'] = '';	
				$data['shipping_adoszam'] = '';
				$data['shipping_address_1'] = '';
				$data['shipping_address_2'] = '';
				$data['shipping_city'] = '';
				$data['shipping_postcode'] = '';
				$data['shipping_zone'] = '';
				$data['shipping_zone_id'] = '';
				$data['shipping_country'] = '';
				$data['shipping_country_id'] = '';
				$data['shipping_address_format'] = '';
				$data['shipping_method'] = '';
				$data['shipping_code'] = '';
                $data['shipping_vallalkozasi_forma'] = '';
                $data['shipping_szekhely'] = '';
                $data['shipping_ugyvezeto_neve'] = '';
                $data['shipping_ugyvezeto_telefonszama'] = '';
                $data['shipping_warehouse'] = '';
			}





			$product_data = array();

			foreach ( $this->cart->getProducts() as $product) {
                if ($product['utalvany'] != 1 || true) {
                    $option_data = array();

                    foreach ($product['option'] as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['option_value'];
                        } else {
                            $value = $this->encryption->decrypt($option['option_value']);
                        }

                        $option_data[] = array(
                            'product_option_id'       => $option['product_option_id'],
                            'product_option_value_id' => $option['product_option_value_id'],
                            'option_id'               => $option['option_id'],
                            'subtract'                => $option['subtract'],
                            'option_value_id'         => $option['option_value_id'],
                            'name'                    => $option['name'],
                            'value'                   => $value,
                            'type'                    => $option['type'],
                            'price'                   => $option['price']
                        );
                    }

                    if ($this->config->get('megjelenit_form_admin_model') == 1) {
                        $model = $product['model'];
                    } elseif ($this->config->get('megjelenit_form_admin_cikkszam') == 1) {
                        $model = $product['cikkszam'];
                    } elseif ( $this->config->get('megjelenit_form_admin_cikkszam2') == 1) {
                        $model = $product['cikkszam2'];
                    } else {
                        $model = "";
                    }

                    $this->load->model("catalog/manufacturer");
                    $manufacturer = $this->model_catalog_manufacturer->getManufacturer($product['manufacturer_id']);
                    $manufacturer = isset($manufacturer['name']) && $manufacturer['name'] ? $manufacturer['name'] : "";

                    $product_data[] = array(
                        'product_id'    => $product['product_id'],
                        'name'          => $product['name'],
                        'model'         => $model,
                        'cikkszam'      => $product['cikkszam'],
                        'manufacturer'  => $manufacturer,
                        'option'        => $option_data,
                        'download'      => $product['download'],
                        'quantity'      => $product['quantity'],
                        'subtract'      => $product['subtract'],
                        'price'         => $product['price'],
                        'total'         => $product['total'],
                        'tax'           => $this->tax->getTax($product['total'], $product['tax_class_id']),
                        'reward'        => $product['reward'],
                        'ingyenes'      => $product['utalvany']
                    );
                }
			}
			
			// Gift Voucher
			$voucher_data = array();
			
			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $voucher) {
					$voucher_data[] = array(
						'description'      => $voucher['description'],
						'code'             => substr(md5(rand()), 0, 7),
						'to_name'          => $voucher['to_name'],
						'to_email'         => $voucher['to_email'],
						'from_name'        => $voucher['from_name'],
						'from_email'       => $voucher['from_email'],
						'voucher_theme_id' => $voucher['voucher_theme_id'],
						'message'          => $voucher['message'],						
						'amount'           => $voucher['amount']
					);
				}
			}  
						
			$data['products'] = $product_data;
			$data['vouchers'] = $voucher_data;
			$data['totals'] = $total_data;
			$data['comment'] = $this->session->data['comment'];
			$data['total'] = $total;
			
			if (isset($this->request->cookie['tracking'])) {
				$this->load->model('affiliate/affiliate');
				
				$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);
				
				if ($affiliate_info) {
					$data['affiliate_id'] = $affiliate_info['affiliate_id']; 
					$data['commission'] = ($total / 100) * $affiliate_info['commission']; 
				} else {
					$data['affiliate_id'] = 0;
					$data['commission'] = 0;
				}
			} else {
				$data['affiliate_id'] = 0;
				$data['commission'] = 0;
			}
			
			$data['language_id'] = $this->config->get('config_language_id');
			$data['currency_id'] = $this->currency->getId();
			$data['currency_code'] = $this->currency->getCode();
			$data['currency_value'] = $this->currency->getValue($this->currency->getCode());
			$data['ip'] = $this->request->server['REMOTE_ADDR'];
			
			if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
				$data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];	
			} elseif(!empty($this->request->server['HTTP_CLIENT_IP'])) {
				$data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];	
			} else {
				$data['forwarded_ip'] = '';
			}
			
			if (isset($this->request->server['HTTP_USER_AGENT'])) {
				$data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];	
			} else {
				$data['user_agent'] = '';
			}
			
			if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
				$data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];	
			} else {
				$data['accept_language'] = '';
			}
						
			$this->load->model('checkout/order');
			
			$this->session->data['order_id'] = $this->model_checkout_order->addOrder($data);


            $this->data['entry_szamlazasi_cim'] = $this->language->get('entry_szamlazasi_cim');
            $this->data['entry_szalitasi_cim']  = $this->language->get('entry_szalitasi_cim');
            $this->data['entry_megjegyzes']     = $this->language->get('entry_megjegyzes');
            $this->data['entry_fizetesi_mod']   = $this->language->get('entry_fizetesi_mod');
            $this->data['entry_adatok']         = $this->language->get('entry_adatok');


            $this->data['column_name'] = $this->language->get('column_name');
			$this->data['column_netto'] = $this->language->get('column_netto');
			$this->data['termek_ar_osszes'] = $this->language->get('termek_ar_osszes');
			$this->data['column_netto_total_price'] = $this->language->get('column_netto_total_price');

            $this->data['megjelenit_kosar'] = $this->config->get('megjelenit_admin_kosar');


            if ($this->config->get('megjelenit_form_admin_model') == 1) {
                $this->data['column_model'] = $this->language->get('column_model');
            } elseif ($this->config->get('megjelenit_form_admin_cikkszam') == 1) {
                $this->data['column_model'] = $this->language->get('column_cikkszam');
            } elseif ( $this->config->get('megjelenit_form_admin_cikkszam2') == 1) {
                $this->data['column_model'] = $this->language->get('column_cikkszam');
            } else {
                $this->data['column_model'] = "";
            }


			$this->data['column_quantity'] = $this->language->get('column_quantity');
			$this->data['column_price'] = $this->language->get('column_price');
			$this->data['column_price_netto_ar'] = $this->language->get('column_price_netto_ar');
			$this->data['column_total_netto_ar'] = $this->language->get('column_total_netto_ar');

            $this->data['fizetendo_osszes'] = $this->language->get('fizetendo_osszes');
            $this->data['afa_osszes'] = $this->language->get('afa_osszes');
            $this->data['netto_osszes'] = $this->language->get('netto_osszes');

			$this->data['column_total'] = $this->language->get('column_total');
			$this->data['szallitas'] = $this->language->get('szallitas');
			$this->data['szallitas_netto_ar'] = $this->language->get('szallitas_netto_ar');
			$this->data['szallitas_brutto_ar'] = $this->language->get('szallitas_brutto_ar');


			$this->data['products'] = array();


            $products_brutto = 0;
            $products_netto = 0;

            foreach ($this->cart->getProducts() as $product) {
                if ($product['utalvany'] != 1) {
                    $option_data = array();

                    foreach ($product['option'] as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['option_value'];
                        } else {
                            $filename = $this->encryption->decrypt($option['option_value']);

                            $value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
                        }

                        $option_data[] = array(
                            'name'  => $option['name'],
                            'value' => (utf8_strlen($value) > 40 ? utf8_substr($value, 0, 40) . '...' : $value),
                            'price' => $option['price']
                        );
                    }

                    $this->load->model("catalog/manufacturer");
                    $manufacturer = $this->model_catalog_manufacturer->getManufacturer($product['manufacturer_id']);
                    $manufacturer = isset($manufacturer['name']) && $manufacturer['name'] ? $manufacturer['name'] : "";

                    $this->data['products'][] = array(
                        'product_id'        => $product['product_id'],
                        'name'              => $product['name'],
                        'model'             => $product['model'],
                        'cikkszam'          => $product['cikkszam'],
                        'manufacturer'      => $manufacturer,
                        'option'            => $option_data,
                        'quantity'          => $product['quantity'],
                        'subtract'          => $product['subtract'],
                        'netto_price'       => $this->currency->format(($product['price'])),
                        'netto_total_price' => $this->currency->format(($product['total'])),
                        'price'             => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
                        'total'             => $this->currency->format($this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'))),
                        'href'              => $this->url->link('product/product', 'product_id=' . $product['product_id'])

                    );
                    $products_brutto += $this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'));
                    $products_netto += $product['total'];
                }
			}

            $this->data['products_brutto'] = $this->currency->format(($products_brutto));
            $this->data['products_netto'] = $this->currency->format(($products_netto));

			// Gift Voucher
			$this->data['vouchers'] = array();
			
			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $voucher) {
					$this->data['vouchers'][] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'])
					);
				}
			}  
						
			$this->data['totals'] = $total_data;
           // if ($this->config->get('megjelenit_penztar_fizetesi_mod') == 1) {
                $this->data['payment'] = $this->getChild('payment/' . $this->session->data['payment_method']['code']);
            //} else {
//                $this->data['payment'] = $this->getChild('payment/free_checkout');
  //          }

			$uzlet_adatai = false;
            if (isset($_SESSION['shipping_method']['warehouse_id'])) {
				if (isset($_SESSION['shipping_method']['warehouse_quote'][0]['warehouse']) && $_SESSION['shipping_method']['warehouse_quote'][0]['warehouse']) {
					foreach($_SESSION['shipping_method']['warehouse_quote'][0]['warehouse'] as $value) {
						if ($value['warehouse_id'] == $_SESSION['shipping_method']['warehouse_id']) {
							$uzlet_adatai ['city'] = $value['city'];
							$uzlet_adatai ['address'] = $value['address'];
							break;
						}
					}
				}
			}

            $gls_adatai = false;
            if (isset($_SESSION['shipping_method']['gls_pont']) && $_SESSION['shipping_method']['gls_pont']) {
                $gls_adatai ['zipcode']     = $_SESSION['shipping_method']['gls_pont'][1];
                $gls_adatai ['name']        = $_SESSION['shipping_method']['gls_pont'][2];
                $gls_adatai ['city']        = $_SESSION['shipping_method']['gls_pont'][3];
                $gls_adatai ['address']     = $_SESSION['shipping_method']['gls_pont'][4];
            }

            if(isset($_SESSION['shipping_method'])) {
                $this->data['shipping_arak'] = array(
                    "netto"     => $_SESSION['shipping_method']['netto_text'],
                    "brutto"    => $_SESSION['shipping_method']['text'],
                    "title"     => $_SESSION['shipping_method']['title'],
                    "warehouse" => $uzlet_adatai,
                    "gls_pont"  => $gls_adatai

                );
            }

            $total_data_segitseg = array();
            foreach ($total_data as $value) {
                if ($value['code'] == "shipping") {
                    $total_data_segitseg[] = array(
                        'code'          => $value['code'],
                        'title'         => $value['title'],
                        'text'          => $value['text'],
                        'value'         => $value['value'],
                        'sort_order'    => $value['sort_order'],
                        'text_brutto'   => isset($_SESSION['shipping_method']['text']) ? $_SESSION['shipping_method']['text'] : ""
                    );
                } else {
                    $total_data_segitseg[] = $value;
                }
            }
            $this->data['totals'] = $total_data_segitseg;

		} else {
			$this->data['redirect'] = $redirect;
		}



		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/confirm.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/checkout/confirm.tpl';
		} else {
			$this->template = 'default/template/checkout/confirm.tpl';
		}
		
		$this->response->setOutput($this->render());	
  	}
}
?>