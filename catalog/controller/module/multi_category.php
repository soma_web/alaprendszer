<?php
class ControllerModuleMultiCategory extends Controller {
    public  $children_data = array();

    protected function index($setting) {
        $this->language->load('module/category');

        $this->data['heading_title'] = $this->language->get('heading_title');

        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string)$this->request->get['path']);
        } else {
            $parts = array();
        }

        if ($this->config->get('multi_category_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

        $this->data['parts'] = $parts;
        if (isset($parts[0])) {
            $this->data['category_id'] = $parts[0];
        } else {
            $this->data['category_id'] = 0;
        }

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('module/multi_category');

        $this->data['categories'] = array();
        $this->log->setMicroTime();

        if (isset($_SESSION['categoryAll'])) {
            $this->data['categories'] = $_SESSION['categoryAll'];
        } else {
            $this->data['categories'] = $this->cache->get('category_all.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));
            if (!$this->data['categories']) {
                $arr2 = $this->model_module_multi_category->getCategoriesAll(0);
                $this->data['categories'] = $this->model_module_multi_category->buildTree($arr2, 0, "");
                $categoria_tomb = $this->data['categories'];
                $this->data['categories'] = $this->model_module_multi_category->categorySort($categoria_tomb);
                $this->cache->set('category_all.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $this->data['categories']);
            }
            $_SESSION['categoryAll'] = $this->data['categories'];

        }
        $this->log->GetMicroTime('multi_category - 37 buildTree');

        $this->load->model('tool/image');

        $this->data['kategoria_lista'] = false;

        if ($this->config->get('multi_category_module_lista') == 1 ) {
            $this->data['kategoria_lista'] = true;
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/multi_category.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/multi_category.tpl';
        } else {
            $this->template = 'default/template/module/multi_category.tpl';
        }

        $this->render();

    }

}
?>