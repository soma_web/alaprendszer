    <?php echo $header; ?>
    <?php if ($attention) { ?>
        <div class="attention"><?php echo $attention; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
    <?php } ?>

    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>

    <?php echo $column_left; ?><?php echo $column_right; ?>
    <div id="content"><?php echo $content_top; ?>

        <h1><?php echo $heading_title; ?>
            <?php if ($weight && $this->config->get("megjelenit_kosar_suly") == 1) { ?>
                &nbsp;(<?php echo $weight; ?>)
            <?php } ?>
        </h1>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart_info.tpl')) {
                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/checkout/cart_info.tpl';
            } else {
                $arkiir = DIR_TEMPLATE.'default/template/checkout/cart_info.tpl';
            } ?>
            <?php include($arkiir); ?>
        </form>


        <?php if ( ($coupon_status || $voucher_status || $reward_status || $shipping_status) && ($this->config->get("megjelenit_kosar_kupon") == 1) ) { ?>
            <h2><?php echo $text_next; ?></h2>
            <div class="content">
                <p><?php echo $text_next_choice; ?></p>
                <table class="radio">
                    <?php if ($coupon_status) { ?>
                        <tr class="highlight">
                            <td><input type="radio" name="next" value="coupon" id="use_coupon" /></td>
                            <td><label for="use_coupon"><?php echo $text_use_coupon; ?></label></td>
                        </tr>
                    <?php } ?>
                    <?php if ($voucher_status) { ?>
                        <tr class="highlight">
                            <td><input type="radio" name="next" value="voucher" id="use_voucher" /></td>
                            <td><label for="use_voucher"><?php echo $text_use_voucher; ?></label></td>
                        </tr>
                    <?php } ?>
                    <?php if ($reward_status) { ?>
                        <tr class="highlight">
                            <td><input type="radio" name="next" value="reward" id="use_reward" /></td>
                            <td><label for="use_reward"><?php echo $text_use_reward; ?></label></td>
                        </tr>
                    <?php } ?>
                    <?php if ($shipping_status) { ?>
                        <?php if( (!$coupon_status) && (!$voucher_status) && (!$reward_status) ) $shipping_checked = true; ?>
                        <?php if($shipping_checked) { ?>
                            <tr class="highlight">
                        <?php } else { ?>
                            <tr class="highlight">
                        <?php } ?>
                        <td><input type="radio" name="next" value="shipping" id="shipping_estimate" <?php if($shipping_checked) { ?>  <?php } ?>  /></td>
                        <td><label for="shipping_estimate"><?php echo $text_shipping_estimate; ?></label></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php } ?>

            <div class="cart-module">
                <div id="coupon" class="content">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                        <?php echo $entry_coupon; ?>&nbsp;
                        <input type="text" name="coupon" value="<?php echo $coupon; ?>" />
                        <input type="hidden" name="next" value="coupon" />
                        &nbsp;
                        <input type="submit" value="<?php echo $button_coupon; ?>" class="button" />
                    </form>
                </div>
                <div id="voucher" class="content">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                        <?php echo $entry_voucher; ?>&nbsp;
                        <input type="text" name="voucher" value="<?php echo $voucher; ?>" />
                        <input type="hidden" name="next" value="voucher" />
                        &nbsp;
                        <input type="submit" value="<?php echo $button_voucher; ?>" class="button" />
                    </form>
                </div>
                <div id="reward" class="content">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                        <?php echo $entry_reward; ?>&nbsp;
                        <input type="text" name="reward" value="<?php echo $reward; ?>" />
                        <input type="hidden" name="next" value="reward" />
                        &nbsp;
                        <input type="submit" value="<?php echo $button_reward; ?>" class="button" />
                    </form>
                </div>
                <div id="shipping" class="content">
                    <p><?php echo $text_shipping_detail; ?></p>
                    <table>
                        <tr>
                            <td><span class="required">*</span> <?php echo $entry_country; ?></td>
                            <td><select name="country_id" onchange="$('select[name=\'zone_id\']').load('index.php?route=checkout/cart/zone&country_id=' + this.value + '&zone_id=<?php echo $zone_id; ?>');">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($countries as $country) { ?>
                                        <?php if ($country['country_id'] == $country_id) { ?>
                                            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select></td>
                        </tr>
                        <?php if ($megjelenit_kosar['cart_shipping_region']) {?>
                            <tr>
                                <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
                                <td><select name="zone_id">
                                    </select></td>
                            </tr>
                        <?php }?>

                        <?php if ($megjelenit_kosar['cart_shipping_postcode']) {?>
                            <tr class="postcode">
                                <td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
                                <td><input type="text" name="postcode" value="<?php echo $postcode; ?>" /></td>
                            </tr>
                        <?php }?>

                    </table>
                    <input type="button" value="<?php echo $button_quote; ?>" id="button-quote" class="button" />
                </div>
            </div>
        <?php if ( $megjelenit_kosar['netto_adat'] == 1) {?>
            <?php foreach ($totals as $total) { ?>
                <?php     if ($total['code'] == "shipping") { ?>
                    <table class="szallitas_tabla">
                        <thead>
                        <tr>
                            <td colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 5; } else {echo 4;}?>"><?php echo $szallitas ?></td>
                            <td class="price"><?php echo $szallitas_netto_ar ?></td>
                            <td class="price"><?php echo $szallitas_brutto_ar ?></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 5; } else {echo 4;}?>"><?php echo $shipping_arak['title']?></td>
                            <td class="price"><?php echo $shipping_arak['netto']?></td>
                            <td class="price"><?php echo $shipping_arak['brutto']?></td>
                        </tr>

                        </tbody>
                    </table>
                    <style>
                        .szallitas_tabla{
                            border-collapse: collapse;
                            width: 100%;
                            margin-bottom: 15px;
                            border-collapse: collapse;
                            border-top: 1px solid #DDDDDD;
                            border-left: 1px solid #DDDDDD;
                            border-right: 1px solid #DDDDDD;
                        }
                        .szallitas_tabla thead td{
                            color: #4D4D4D;
                            font-weight: bold;
                            background-color: #F7F7F7;
                            border-bottom: 1px solid #DDDDDD;
                            padding: 7px;
                            text-align: left;

                        }
                        .szallitas_tabla thead .price{
                            text-align: right;
                        }
                        .szallitas_tabla tbody .price{
                            text-align: right;
                        }
                        .szallitas_tabla tbody td {
                            padding: 7px;
                            border-bottom: 1px solid #DDDDDD;
                        }
                    </style>
                <?php } ?>
            <?php } ?>
        <?php } ?>
            <div class="cart-total">

                <table id="total">
                    <tr style="border:none"><td style="border:none" colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 7; } else {echo 6;}?>"></td></tr>

                    <?php foreach ($totals as $total) { ?>
                        <?php if ( $megjelenit_kosar['netto_adat'] == 1) {   ?>
                            <tr>
                                <?php  if ($total['code'] != "shipping" ) { ?>
                                    <td colspan="5" class="right"><b><?php echo $total['title']; ?>:</b></td>
                                    <td class="right"><?php echo $total['text']; ?></td>
                                <?php } ?>

                            </tr>
                        <?php } else if ( $megjelenit_kosar['netto_adat'] == 0) { ?>
                            <tr>
                                <td colspan="5" class="right"><b><?php echo $total['title']; ?>:</b></td>
                                <td class="right"><?php echo $total['text']; ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </table>
            </div>
            <div class="buttons">
                <?php if ($kosarvan) { ?>
                    <div class="right"><a href="<?php echo $checkout; ?>" class="button"><?php echo $button_checkout; ?></a></div>
                <?php } ?>
                <div class="center"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_shopping; ?></a></div>
            </div>

        <?php echo $content_bottom; ?>
    </div>

    <script type="text/javascript"><!--
        $('input[name=\'next\']').bind('change', function() {
            $('.cart-module > div').hide();

            $('#' + this.value).show();
        });

        <?php if ($next == 'coupon') { ?>
        $('#use_coupon').trigger('click');
        <?php } ?>
        <?php if ($next == 'voucher') { ?>
        $('#use_voucher').trigger('click');
        <?php } ?>
        <?php if ($next == 'reward') { ?>
        $('#use_reward').trigger('click');
        <?php } ?>
        <?php if ($next == 'shipping') { ?>
        $('#shipping_estimate').trigger('click');
        <?php } ?>
        //--></script>

    <?php if ($shipping_status) { ?>
        <script type="text/javascript"><!--
            $('#button-quote').on('click', function() {
                $.ajax({
                    url: 'index.php?route=checkout/cart/quote',
                    type: 'post',
                    data: 'country_id=' + $('select[name=\'country_id\']').val() + '&zone_id=' + $('select[name=\'zone_id\']').val() + '&postcode=' + encodeURIComponent($('input[name=\'postcode\']').val()),
                    dataType: 'json',
                    beforeSend: function() {
                        $('#button-quote').attr('disabled', true);
                        $('#button-quote').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
                    },
                    complete: function() {
                        $('#button-quote').attr('disabled', false);
                        $('.wait').remove();
                    },
                    success: function(json) {
                        $('.success, .warning, .attention, .error').remove();

                        if (json['error']) {
                            if (json['error']['warning']) {
                                $('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                                $('.warning').fadeIn('slow');

                                $('html, body').animate({ scrollTop: 0 }, 'slow');
                            }

                            if (json['error']['country']) {
                                $('select[name=\'country_id\']').after('<span class="error">' + json['error']['country'] + '</span>');
                            }

                            if (json['error']['zone']) {
                                $('select[name=\'zone_id\']').after('<span class="error">' + json['error']['zone'] + '</span>');
                            }

                            if (json['error']['postcode']) {
                                $('input[name=\'postcode\']').after('<span class="error">' + json['error']['postcode'] + '</span>');
                            }
                        }

                        if (json['shipping_method']) {
                            html = '<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">';
                            html += '  <table class="radio">';
                            html += '<tr><td colspan=2 class="left" width: 80%"><h2><?php echo $text_shipping_method; ?></h2></td>';
                            html += '<td style="text-align:right"><?php echo "Bruttó"; ?></td></tr>';

                            for (i in json['shipping_method']) {
                                html += '<tr>';
                                html += '  <td colspan="3"><b>' + json['shipping_method'][i]['title'] + '</b></td>';
                                html += '</tr>';

                                if (!json['shipping_method'][i]['error']) {
                                    for (j in json['shipping_method'][i]['quote']) {
                                        html += '<tr class="highlight">';

                                        if (json['shipping_method'][i]['quote'][j]['code'] == '<?php echo $shipping_method; ?>') {
                                            html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" checked="checked" /></td>';
                                        } else {
                                            html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" /></td>';
                                        }

                                        html += '  <td style="width: 60%"><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['title'] + '</label></td>';
                                        if (json['shipping_method'][i]['quote'][j]['tax_class_id'] > 0) {
                                            html += '  <td style="text-align: right;">';
                                            html += '<label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">';
                                            html += '<span style="margin-right: 20px;">'+json['shipping_method'][i]['quote'][j]['netto_text'] + "+" + json['shipping_method'][i]['quote'][j]['ado_title'] + "</span>";
                                            html += '<span>' + json['shipping_method'][i]['quote'][j]['text']+ '</span>';
                                            html += '</label></td>';

                                        } else {
                                            html += '  <td style="text-align: right;"><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['text'] + '</label></td>';
                                        }
                                        html += '</tr>';
                                    }
                                } else {
                                    html += '<tr>';
                                    html += '  <td colspan="3"><div class="error">' + json['shipping_method'][i]['error'] + '</div></td>';
                                    html += '</tr>';
                                }
                            }

                            html += '  </table>';
                            html += '  <br />';
                            html += '  <input type="hidden" name="next" value="shipping" />';
                            html += '  <input type="submit" value="<?php echo $button_shipping; ?>" class="button" />';
                            html += '</form>';

                            /*$.colorbox({
                                overlayClose: true,
                                opacity: 0.5,
                                width: '600px',
                                height: '400px',
                                href: false,
                                html: html
                            });*/
                        }
                    },
                    error: function(e) {

                    }

                    });
            });
            //--></script>

        <script type="text/javascript"><!--
            $('select[name=\'zone_id\']').load('index.php?route=checkout/cart/zone&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
            //--></script>
    <?php } ?>

    <?php echo $footer; ?>
