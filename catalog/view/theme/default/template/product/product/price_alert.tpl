<?php if ($price_alert_status) { ?>
    <div class="pricealert-attention" id="price-alert-trigger"><?php echo $text_price_alert_set_alert; ?></div>
    <div id="dialog-price-alert" class="pricealert-modal">
        <a class="close-pricealert-modal">&#215;</a>
        <div id="pricealert-explain-info"><?php echo $text_price_alert_short_explanation; ?></div>

        <div id="pricealert-info-form">
            <table class="form">
                <tbody>
                <tr>
                    <td><span class="required">* </span><?php echo $entry_price_alert_name; ?></td>
                    <td><input type="text" name="price_alert_name" value="<?php echo $price_alert_name; ?>" size="35"></td>
                </tr>
                <tr>
                    <td><span class="required">* </span><?php echo $entry_price_alert_email; ?></td>
                    <td><input type="text" name="price_alert_email" value="<?php echo $price_alert_email; ?>" size="35"></td>
                </tr>
                <tr>
                    <td><span class="required">* </span><?php echo $entry_price_alert_desired_price; ?></td>
                    <td><input type="text" name="price_alert_desired_price" size="10"></td>
                </tr>
                </tbody>
            </table>
            <input type="hidden" name="price_alert_product_id" value="<?php echo $product_id; ?>" />
            <input type="hidden" name="price_alert_product_price" value="<?php echo $price_alert_price; ?>" />
            <input type="hidden" name="price_alert_currency_code" value="<?php echo $price_alert_currency_code; ?>" />
            <input type="hidden" name="price_alert_customer_group_id" value="<?php echo $price_alert_customer_group_id; ?>" />
        </div>

        <div class="buttons">
            <div class="right"><a class="pricealert-button" id="price-alert-send"><?php echo $button_price_alert; ?></a></div>
        </div>

    </div>
<?php } ?>

<script type="text/javascript"><!--
    $('#price-alert-trigger').bind('click', function(){
        $('#dialog-price-alert').pricealert();
    });

    $('#dialog-price-alert input[name=\'price_alert_desired_price\']').bind('change', function() {
        $(this).val($(this).val().replace(",", "."));
    });

    $('#price-alert-send').bind('click', function(){
        $.ajax({
            type: 'POST',
            url: 'index.php?route=module/price_alert',
            data: $('#pricealert-info-form input[type=\'text\'], #pricealert-info-form input[type=\'hidden\']'),
            dataType: 'json',
            success: function(json){
                $('#dialog-price-alert .warning, #dialog-price-alert .success').remove();

                if (json['error']){
                    if (json['error']['warning']){
                        $('#pricealert-info-form').before('<div class="warning">' + json['error']['warning'] + '</div>');
                        $('#dialog-price-alert .warning').fadeIn('slow');
                    }
                }

                if (json['success']){
                    $('#pricealert-info-form').before('<div class="success">' + json['success'] + '</div>');
                    $('#dialog-price-alert .success').fadeIn('slow');

                    setTimeout(function() {
                        $('#dialog-price-alert .warning, #dialog-price-alert .success').remove();
                        $('.close-pricealert-modal').trigger('click');
                    }, 3000);

                }
            }
        });
    });
    //--></script>
