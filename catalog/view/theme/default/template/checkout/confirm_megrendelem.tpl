<?php if (!isset($redirect)) { ?>
    <?php $megjelenit_product = $this->config->get('megjelenit_product'); ?>


    <?php if ( $megjelenit_kosar['netto_adat'] == 1) {   ?>

        <div class="checkout-product">
          <table>
            <thead>
              <tr>
                <td class="name"><?php echo $column_name; ?></td>
                  <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                    <td class="model"><?php echo $column_model; ?></td>
                  <?php } ?>
                <td class="price"><?php echo $column_netto; ?></td>
                <td class="price"><?php echo $column_price_netto_ar; ?></td>
                <td class="quantity"><?php echo $column_quantity; ?></td>
                <td class="netto"><?php echo $column_netto_total_price; ?></td>
                <td class="total"><?php echo $column_total_netto_ar; ?></td>
              </tr>
            </thead>
            <!--<tbody>-->
              <?php foreach ($products as $product) { ?>
              <tr>
                <td class="name"><a href="<?php echo $product['href']; ?>">
                        <?php if ($megjelenit_product['product_model_termeknevben']) { ?>
                            <?php if (isset($product['model']) && $product['model']) { ?>
                                <span class="model_cart"><?php echo $product['model'];?></span> <?php echo " - ";?>
                            <?php }?>
                        <?php }?>

                        <?php if ($megjelenit_product['product_cikkszam_termeknevben']) { ?>
                            <?php if (isset($product['cikkszam']) && $product['cikkszam']) { ?>
                                <span class="model_cart"><?php echo $product['cikkszam'];?></span> <?php echo " - ";?>
                            <?php }?>
                        <?php }?>

                        <?php if ($megjelenit_product['product_gyarto_termeknevben']) { ?>
                            <?php if (isset($product['manufacturer']) && $product['manufacturer']) { ?>
                                <span class="manufacturer_cart"><?php echo $product['manufacturer'];?></span> <?php echo " - ";?>
                            <?php }?>
                        <?php }?>

                        <?php echo $product['name']; ?>
                    </a>
                  <?php foreach ($product['option'] as $option) { ?>
                    <br />
                    &nbsp;<small> - <?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                  <?php } ?>
                </td>
                  <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                <td class="model"><?php echo $product['model']; ?></td>
                  <?php } ?>
                  <td class="price"><?php echo $product['netto_price']; ?></td>
                  <td class="price"><?php echo $product['price']; ?></td>
                <td class="quantity"><?php echo $product['quantity']; ?></td>
                  <td class="total"><?php echo $product['netto_total_price']; ?></td>
                <td class="total"><?php echo $product['total']; ?></td>
              </tr>

              <?php } ?>
              <tr>
                  <td  colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 5; } else {echo 4;}?>"><?php echo $termek_ar_osszes; ?></td>
                  <td class="price" style="font-weight: bold;">
                      <?php echo $products_netto; ?>
                  </td>
                  <td class="price" style="font-weight: bold;">
                      <?php echo $products_brutto; ?>
                  </td>
              </tr>
              <?php foreach ($vouchers as $voucher) { ?>
              <tr>
                <td class="name"><?php echo $voucher['description']; ?></td>
                  <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                      <td class="model"><?php echo $product['model']; ?></td>
                  <?php } ?>
                  <td class="price"><?php echo $voucher['amount']; ?></td>
                  <td class="price"><?php echo $voucher['amount']; ?></td>
                <td class="quantity">1</td>
                <td class="price"><?php echo $voucher['amount']; ?></td>
                <td class="total"><?php echo $voucher['amount']; ?></td>
              </tr>
              <?php } ?>
              <tr style="border:none"><td style="border:none" colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 7; } else {echo 6;}?>"></td></tr>
              <?php foreach ($totals as $total) { ?>

              <?php     if ($total['code'] == "shipping") { ?>



                      <tr>
                          <td colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 5; } else {echo 4;}?>"><?php echo $szallitas ?></td>
                          <td class="price"><?php echo $szallitas_netto_ar ?></td>
                          <td class="price"><?php echo $szallitas_brutto_ar ?></td>
                      </tr>

                      <tr>
                          <td colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 5; } else {echo 4;}?>"><?php echo $shipping_arak['title'];?>
                              <?php echo $shipping_arak['warehouse'] ? " - <b> " .$shipping_arak['warehouse']['city']. ", " .$shipping_arak['warehouse']['address']."</b>"  : ""?>
                              <?php echo $shipping_arak['gls_pont'] ? ":<br> <b> ".$shipping_arak['gls_pont']['name'].": ".$shipping_arak['gls_pont']['zipcode']." " .$shipping_arak['gls_pont']['city']. ", " .$shipping_arak['gls_pont']['address']."</b>"  : ""?>
                          </td>
                          <td class="price"><?php echo $shipping_arak['netto']?></td>
                          <td class="price"><?php echo $shipping_arak['brutto']?></td>
                      </tr>



                      <tr style="border:none">
                          <td style="border:none" colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 7; } else {echo 6;}?>">&nbsp;</td>
                      </tr>
              <?php } else {?>
                <tr>
                    <td class="name"><b><?php echo $total['title']; ?></b></td>
                    <td colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 5; } else {echo 4;}?>"></td>
                    <td class="total"><b><?php echo $total['text']; ?></b></td>
                  </tr>
                <?php } ?>
              <?php } ?>
          </table>
        </div>

        <div class="payment"><?php echo $payment; ?></div>
        <style>
            .checkout-product tfoot td{
                text-align: left;
            }
            .checkout-product tfoot .total {
                text-align: right;
            }
            .checkout-product thead .netto {
                text-align: right;
            }
        </style>
    <?php } else if ( $megjelenit_kosar['netto_adat'] == 0) { ?>
        <div class="checkout-product">
          <table>
            <thead>
              <tr>
                <td colspan="2" class="name"><?php echo $column_name; ?></td>
                  <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                      <td class="model"><?php echo $column_model; ?></td>
                  <?php } ?>
                <td class="quantity"><?php echo $column_quantity; ?></td>
                <td class="price"><?php echo $column_price; ?></td>
                <td class="total"><?php echo $column_total; ?></td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($products as $product) { ?>
                    <tr>
                        <td colspan="2" class="name">
                            <a href="<?php echo $product['href']; ?>">
                                <?php if ($megjelenit_product['product_model_termeknevben']) { ?>
                                    <?php if (isset($product['model']) && $product['model']) { ?>
                                        <span class="model_cart"><?php echo $product['model'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>

                                <?php if ($megjelenit_product['product_cikkszam_termeknevben']) { ?>
                                    <?php if (isset($product['cikkszam']) && $product['cikkszam']) { ?>
                                        <span class="model_cart"><?php echo $product['cikkszam'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>

                                <?php if ($megjelenit_product['product_gyarto_termeknevben']) { ?>
                                    <?php if (isset($product['manufacturer']) && $product['manufacturer']) { ?>
                                        <span class="manufacturer_cart"><?php echo $product['manufacturer'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>

                                <?php echo $product['name']; ?>
                            </a>
                            <?php foreach ($product['option'] as $option) { ?>
                                <br />
                                <? if ($option['name'] == "Termékdíj") {
                                    $hossz=strlen($option['price']);
                                    $price_levag=0;
                                    for($i=$hossz; $i>0; $i--){
                                        if (substr($option['price'],$i-1,1) == "0"){
                                            $price_levag++;
                                        } elseif (substr($option['price'],$i-1,1) == "."){
                                            $price_levag++;
                                            break;
                                        } else break;
                                    }
                                    $termekdij= substr($option['price'],0,$hossz-$price_levag);
                                    ?>

                                    &nbsp;<small> - <?php echo $option['name']."  (". $termekdij."Ft)"; ?></small>
                                <? } else {?>
                                    &nbsp;<small> - <?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                                <?php } ?>
                            <?php } ?></td>
                        <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                            <td class="model"><?php echo $product['model']; ?></td>
                        <?php } ?>
                        <td class="quantity"><?php echo $product['quantity']; ?></td>
                        <td class="price"><?php echo $product['price']; ?></td>
                        <td class="total"><?php echo $product['total']; ?></td>
                    </tr>
                <?php } ?>
                <?php foreach ($vouchers as $voucher) { ?>
                    <tr>
                        <td class="name"><?php echo $voucher['description']; ?></td>
                        <td class="model"></td>
                        <td class="quantity">1</td>
                        <td class="price"><?php echo $voucher['amount']; ?></td>
                        <td class="total"><?php echo $voucher['amount']; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
              <tr style="border:none"><td style="border:none" colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 6; } else {echo 5;}?>"></td></tr>

              <tfoot>
                <?php foreach ($totals as $total) { ?>
                    <tr>
                        <td colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 5; } else {echo 4;}?>" class="price"><b><?php echo $total['title']; ?></b></td>
                        <td class="total"><?php echo $total['text']; ?></td>
                    </tr>
                <?php } ?>
                </tfoot>
                </table>
                </div>
                <div class="payment"><?php echo $payment; ?></div>
                <style>
                    .checkout-product tfoot td{
                        text-align: right;
                    }

                </style>
    <?php } ?>
<?php } else { ?>
    <script type="text/javascript"><!--
    location = '<?php echo $redirect; ?>';
    //--></script>
<?php } ?>