<?php
class ControllerSalePreOrder extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('sale/pre_order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/pre_order');

        $this->getList();
    }

    public function insert() {
        $this->load->language('sale/pre_order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/pre_order');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_sale_pre_order->addPreOorder($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_pre_order_id'])) {
                $url .= '&filter_pre_order_id=' . $this->request->get['filter_pre_order_id'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . $this->request->get['filter_customer'];
            }


            if (isset($this->request->get['filter_date_added'])) {
                $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
            }

            if (isset($this->request->get['filter_date_modified'])) {
                $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
            }


            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function update() {
        $this->load->language('sale/pre_order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/pre_order');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_sale_pre_order->editPreOrder($this->request->get['pre_order_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';
            $url .= isset($this->request->get['filter_pre_order_id']) ? '&filter_pre_order_id=' . $this->request->get['filter_pre_order_id'] : '';
            $url .= isset($this->request->get['filter_customer']) ? '&filter_customer=' . $this->request->get['filter_customer'] : '';
            $url .= isset($this->request->get['filter_product_name']) ? '&filter_product_name=' . $this->request->get['filter_product_name'] : '';
            $url .= isset($this->request->get['filter_model']) ? '&filter_model=' . $this->request->get['filter_model'] : '';
            $url .= isset($this->request->get['filter_email']) ? '&filter_email=' . $this->request->get['filter_email'] : '';
            $url .= isset($this->request->get['filter_date_added']) ? '&filter_date_added=' . $this->request->get['filter_date_added'] : '';
            $url .= isset($this->request->get['filter_date_modified']) ? '&filter_date_modified=' . $this->request->get['filter_date_modified'] : '';
            $url .= isset($this->request->get['sort']) ? '&sort=' . $this->request->get['sort'] : '';
            $url .= isset($this->request->get['order']) ? '&order=' . $this->request->get['order'] : '';
            $url .= isset($this->request->get['page']) ? '&page=' . $this->request->get['page'] : '';

            $this->redirect($this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('sale/pre_order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/pre_order');

        if (isset($this->request->post['selected']) && ($this->validateDelete())) {
            foreach ($this->request->post['selected'] as $pre_order_id) {
                $this->model_sale_pre_order->deletePreOrder($pre_order_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';
            $url .= isset($this->request->get['filter_pre_order_id']) ? '&filter_pre_order_id=' . $this->request->get['filter_pre_order_id'] : '';
            $url .= isset($this->request->get['filter_customer']) ? '&filter_customer=' . $this->request->get['filter_customer'] : '';
            $url .= isset($this->request->get['filter_product_name']) ? '&filter_product_name=' . $this->request->get['filter_product_name'] : '';
            $url .= isset($this->request->get['filter_model']) ? '&filter_model=' . $this->request->get['filter_model'] : '';
            $url .= isset($this->request->get['filter_email']) ? '&filter_email=' . $this->request->get['filter_email'] : '';
            $url .= isset($this->request->get['filter_date_added']) ? '&filter_date_added=' . $this->request->get['filter_date_added'] : '';
            $url .= isset($this->request->get['filter_date_modified']) ? '&filter_date_modified=' . $this->request->get['filter_date_modified'] : '';
            $url .= isset($this->request->get['sort']) ? '&sort=' . $this->request->get['sort'] : '';
            $url .= isset($this->request->get['order']) ? '&order=' . $this->request->get['order'] : '';
            $url .= isset($this->request->get['page']) ? '&page=' . $this->request->get['page'] : '';

            $this->redirect($this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    private function getList() {
        $filter_pre_order_id    = isset($this->request->get['filter_pre_order_id']) ? $this->request->get['filter_pre_order_id'] : null;
        $filter_product_name    = isset($this->request->get['filter_product_name']) ? $this->request->get['filter_product_name'] : null;
        $filter_model           = isset($this->request->get['filter_model'])        ? $this->request->get['filter_model'] : null;
        $filter_customer        = isset($this->request->get['filter_customer'])     ? $this->request->get['filter_customer'] : null;
        $filter_email           = isset($this->request->get['filter_email'])        ? $this->request->get['filter_email'] : null;
        $filter_date_added      = isset($this->request->get['filter_date_added'])   ? $this->request->get['filter_date_added'] : null;
        $filter_date_modified   = isset($this->request->get['filter_date_modified']) ? $this->request->get['filter_date_modified'] : null;
        $sort                   = isset($this->request->get['sort'])                ? $this->request->get['sort'] : 'o.pre_order_id';
        $order                  = isset($this->request->get['order'])               ? $this->request->get['order'] : 'DESC';
        $page                   = isset($this->request->get['page'])                ? $this->request->get['page'] : 1;

        $url = '';
        $url .= isset($this->request->get['filter_pre_order_id']) ? '&filter_pre_order_id=' . $this->request->get['filter_pre_order_id'] : '';
        $url .= isset($this->request->get['filter_customer']) ? '&filter_customer=' . $this->request->get['filter_customer'] : '';
        $url .= isset($this->request->get['filter_product_name']) ? '&filter_product_name=' . $this->request->get['filter_product_name'] : '';
        $url .= isset($this->request->get['filter_model']) ? '&filter_model=' . $this->request->get['filter_model'] : '';
        $url .= isset($this->request->get['filter_email']) ? '&filter_email=' . $this->request->get['filter_email'] : '';
        $url .= isset($this->request->get['filter_date_added']) ? '&filter_date_added=' . $this->request->get['filter_date_added'] : '';
        $url .= isset($this->request->get['filter_date_modified']) ? '&filter_date_modified=' . $this->request->get['filter_date_modified'] : '';
        $url .= isset($this->request->get['sort']) ? '&sort=' . $this->request->get['sort'] : '';
        $url .= isset($this->request->get['order']) ? '&order=' . $this->request->get['order'] : '';
        $url .= isset($this->request->get['page']) ? '&page=' . $this->request->get['page'] : '';

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        //$this->data['insert']       = $this->url->link('sale/pre_order/insert', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['delete']       = $this->url->link('sale/pre_order/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['pre_orders'] = array();

        $data = array(
            'filter_pre_order_id'    => $filter_pre_order_id,
            'filter_customer'	     => $filter_customer,
            'filter_product_name'	 => $filter_product_name,
            'filter_model'	         => $filter_model,
            'filter_email'	         => $filter_email,
            'filter_date_added'	     => $filter_date_added,
            'filter_date_modified'	 => $filter_date_modified,
            'sort'                   => $sort,
            'order'                  => $order,
            'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'                  => $this->config->get('config_admin_limit')
        );

        $pre_order_total = $this->model_sale_pre_order->getTotalPreOrders($data);

        $results = $this->model_sale_pre_order->getPreOrders($data);

        foreach ($results as $result) {
            $action = array(
                'text' => $this->language->get('text_edit'),
                'href' => $this->url->link('sale/pre_order/update', 'token=' . $this->session->data['token'] . '&pre_order_id=' . $result['pre_order_id'] . $url, 'SSL')
            );
            $this->data['pre_orders'][] = array(
                'pre_order_id'	    => $result['pre_order_id'],
                'product_id'	    => $result['product_id'],
                'model'           	=> $result['model'],
                'cikkszam'          => $result['cikkszam'],
                'product_name'      => $result['product_name'],
                'gyarto'           	=> $result['gyarto'],
                'megrendelo_name'   => $result['megrendelo_name'],
                'megrendelo_email'  => $result['megrendelo_email'],
                'megrendelo_uzenet' => $result['megrendelo_uzenet'],
                'date_added'        => $result['date_added'],
                'selected'          => isset($this->request->post['selected']) && in_array($result['pre_order_id'], $this->request->post['selected']),
                'action'            => $action
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['column_pre_order_id']  = $this->language->get('column_pre_order_id');
        $this->data['column_customer']      = $this->language->get('column_customer');
        $this->data['column_email']         = $this->language->get('column_email');
        $this->data['column_product_name']  = $this->language->get('column_product_name');
        $this->data['column_model']         = $this->language->get('column_model');
        $this->data['column_date_added']    = $this->language->get('column_date_added');
        $this->data['column_date_modified'] = $this->language->get('column_date_modified');
        $this->data['column_action']        = $this->language->get('column_action');

        $this->data['button_insert']        = $this->language->get('button_insert');
        $this->data['button_delete']        = $this->language->get('button_delete');
        $this->data['button_filter']        = $this->language->get('button_filter');
        $this->data['text_no_results']      = $this->language->get('text_no_results');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';
        $url .= isset($this->request->get['filter_pre_order_id']) ? '&filter_pre_order_id=' . $this->request->get['filter_pre_order_id'] : '';
        $url .= isset($this->request->get['filter_customer']) ? '&filter_customer=' . $this->request->get['filter_customer'] : '';
        $url .= isset($this->request->get['filter_product_name']) ? '&filter_product_name=' . $this->request->get['filter_product_name'] : '';
        $url .= isset($this->request->get['filter_model']) ? '&filter_model=' . $this->request->get['filter_model'] : '';
        $url .= isset($this->request->get['filter_email']) ? '&filter_email=' . $this->request->get['filter_email'] : '';
        $url .= isset($this->request->get['filter_date_added']) ? '&filter_date_added=' . $this->request->get['filter_date_added'] : '';
        $url .= isset($this->request->get['filter_date_modified']) ? '&filter_date_modified=' . $this->request->get['filter_date_modified'] : '';
        $url .= $order == 'ASC' ? '&order=DESC' : '&order=ASC';
        $url .= isset($this->request->get['page']) ? '&page=' . $this->request->get['page'] : '';

        $this->data['sort_pre_order']       = $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . '&sort=o.pre_order_id' . $url, 'SSL');
        $this->data['sort_product_name']    = $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . '&sort=o.product_name' . $url, 'SSL');
        $this->data['sort_model']           = $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . '&sort=o.model' . $url, 'SSL');
        $this->data['sort_customer']        = $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . '&sort=o.megrendelo_name' . $url, 'SSL');
        $this->data['sort_email']           = $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . '&sort=o.megrendelo_email' . $url, 'SSL');
        $this->data['sort_date_added']      = $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . '&sort=o.date_added' . $url, 'SSL');
        $this->data['sort_date_modified']   = $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . '&sort=o.date_modified' . $url, 'SSL');

        $url = '';
        $url .= isset($this->request->get['filter_pre_order_id']) ? '&filter_pre_order_id=' . $this->request->get['filter_pre_order_id'] : '';
        $url .= isset($this->request->get['filter_customer']) ? '&filter_customer=' . $this->request->get['filter_customer'] : '';
        $url .= isset($this->request->get['filter_product_name']) ? '&filter_product_name=' . $this->request->get['filter_product_name'] : '';
        $url .= isset($this->request->get['filter_model']) ? '&filter_model=' . $this->request->get['filter_model'] : '';
        $url .= isset($this->request->get['filter_email']) ? '&filter_email=' . $this->request->get['filter_email'] : '';
        $url .= isset($this->request->get['filter_date_added']) ? '&filter_date_added=' . $this->request->get['filter_date_added'] : '';
        $url .= isset($this->request->get['filter_date_modified']) ? '&filter_date_modified=' . $this->request->get['filter_date_modified'] : '';

        $url .= isset($this->request->get['sort']) ? '&sort=' . $this->request->get['sort'] : '';
        $url .= isset($this->request->get['order']) ? '&order=' . $this->request->get['order'] : '';


        $pagination = new Pagination();
        $pagination->total = $pre_order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['filter_pre_order_id']  = $filter_pre_order_id;
        $this->data['filter_customer']      = $filter_customer;
        $this->data['filter_product_name']  = $filter_product_name;
        $this->data['filter_model']         = $filter_model;
        $this->data['filter_email']         = $filter_email;
        $this->data['filter_date_added']    = $filter_date_added;
        $this->data['filter_date_modified'] = $filter_date_modified;

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;

        $this->template = 'sale/pre_order_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    public function getForm() {
        $this->load->model('sale/customer');

        $this->data['heading_title'] = $this->language->get('heading_title');


        $this->data['entry_pre_order_id']       = $this->language->get('entry_pre_order_id');
        $this->data['entry_product_id']         = $this->language->get('entry_product_id');
        $this->data['entry_model']              = $this->language->get('entry_model');
        $this->data['entry_cikkszam']           = $this->language->get('entry_cikkszam');
        $this->data['entry_product_name']       = $this->language->get('entry_product_name');
        $this->data['entry_gyarto']             = $this->language->get('entry_gyarto');
        $this->data['entry_megrendelo_name']    = $this->language->get('entry_megrendelo_name');
        $this->data['entry_megrendelo_email']   = $this->language->get('entry_megrendelo_email');
        $this->data['entry_megrendelo_uzenet']  = $this->language->get('entry_megrendelo_uzenet');
        $this->data['entry_telefon']            = $this->language->get('entry_telefon');

        $this->data['entry_iranyitoszam']       = $this->language->get('entry_iranyitoszam');
        $this->data['entry_varos']              = $this->language->get('entry_varos');
        $this->data['entry_utca']               = $this->language->get('entry_utca');
        $this->data['entry_ceg']                = $this->language->get('entry_ceg');
        $this->data['entry_adoszam']            = $this->language->get('entry_adoszam');

        $this->data['entry_date_added']         = $this->language->get('entry_date_added');
        $this->data['entry_date_modified']      = $this->language->get('entry_date_modified');
        $this->data['entry_sajat_megjegyzes']   = $this->language->get('entry_sajat_megjegyzes');



        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');


        $this->data['token'] = $this->session->data['token'];

        if (isset($this->request->get['pre_order_id'])) {
            $this->data['pre_order_id'] = $this->request->get['pre_order_id'];
        } else {
            $this->data['pre_order_id'] = 0;
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $url = '';
        $url .= isset($this->request->get['filter_pre_order_id'])   ? '&filter_pre_order_id=' . $this->request->get['filter_pre_order_id'] : '';
        $url .= isset($this->request->get['filter_customer'])       ? '&filter_customer=' . $this->request->get['filter_customer'] : '';
        $url .= isset($this->request->get['filter_product_name'])   ? '&filter_product_name=' . $this->request->get['filter_product_name'] : '';
        $url .= isset($this->request->get['filter_model'])          ? '&filter_model=' . $this->request->get['filter_model'] : '';
        $url .= isset($this->request->get['filter_email'])          ? '&filter_email=' . $this->request->get['filter_email'] : '';
        $url .= isset($this->request->get['filter_date_added'])     ? '&filter_date_added=' . $this->request->get['filter_date_added'] : '';
        $url .= isset($this->request->get['filter_date_modified'])  ? '&filter_date_modified=' . $this->request->get['filter_date_modified'] : '';
        $url .= isset($this->request->get['sort'])                  ? '&sort=' . $this->request->get['sort'] : '';
        $url .= isset($this->request->get['order'])                 ? '&sort=' . $this->request->get['order'] : '';
        $url .= isset($this->request->get['page'])                  ? '&page=' . $this->request->get['page'] : '';

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        if (!isset($this->request->get['pre_order_id'])) {
            $this->data['action'] = $this->url->link('sale/pre_order/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $this->data['action'] = $this->url->link('sale/pre_order/update', 'token=' . $this->session->data['token'] . '&pre_order_id=' . $this->request->get['pre_order_id'] . $url, 'SSL');
        }

        $this->data['cancel'] = $this->url->link('sale/pre_order', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['pre_order_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $pre_order_info = $this->model_sale_pre_order->getPreOrder($this->request->get['pre_order_id']);
        }

        $this->data['pre_order_id']     = isset($this->request->post['pre_order_id'])       ? $this->request->post['pre_order_id']      : (!empty($pre_order_info) ? $pre_order_info['pre_order_id'] : '');
        $this->data['product_id']       = isset($this->request->post['product_id'])         ? $this->request->post['product_id']        : (!empty($pre_order_info) ? $pre_order_info['product_id'] : '');
        $this->data['model']            = isset($this->request->post['model'])              ? $this->request->post['model']             : (!empty($pre_order_info) ? $pre_order_info['model'] : '');
        $this->data['cikkszam']         = isset($this->request->post['cikkszam'])           ? $this->request->post['cikkszam']          : (!empty($pre_order_info) ? $pre_order_info['cikkszam'] : '');
        $this->data['product_name']     = isset($this->request->post['product_name'])       ? $this->request->post['product_name']      : (!empty($pre_order_info) ? $pre_order_info['product_name'] : '');
        $this->data['gyarto']           = isset($this->request->post['gyarto'])             ? $this->request->post['gyarto']            : (!empty($pre_order_info) ? $pre_order_info['gyarto'] : '');
        $this->data['megrendelo_name']  = isset($this->request->post['megrendelo_name'])    ? $this->request->post['megrendelo_name']   : (!empty($pre_order_info) ? $pre_order_info['megrendelo_name'] : '');
        $this->data['megrendelo_email'] = isset($this->request->post['megrendelo_email'])   ? $this->request->post['megrendelo_email']  : (!empty($pre_order_info) ? $pre_order_info['megrendelo_email'] : '');
        $this->data['megrendelo_uzenet']= isset($this->request->post['megrendelo_uzenet'])  ? $this->request->post['megrendelo_uzenet'] : (!empty($pre_order_info) ? $pre_order_info['megrendelo_uzenet'] : '');

        $this->data['megrendelo_telefon'] = isset($this->request->post['megrendelo_telefon']) ? $this->request->post['megrendelo_telefon'] : (!empty($pre_order_info) ? $pre_order_info['megrendelo_telefon'] : '');
        $this->data['megrendelo_iranyitoszam'] = isset($this->request->post['megrendelo_telefon']) ? $this->request->post['megrendelo_iranyitoszam'] : (!empty($pre_order_info) ? $pre_order_info['megrendelo_iranyitoszam'] : '');
        $this->data['megrendelo_varos'] = isset($this->request->post['megrendelo_varos']) ? $this->request->post['megrendelo_varos'] : (!empty($pre_order_info) ? $pre_order_info['megrendelo_varos'] : '');
        $this->data['megrendelo_utca']  = isset($this->request->post['megrendelo_utca']) ? $this->request->post['megrendelo_utca'] : (!empty($pre_order_info) ? $pre_order_info['megrendelo_utca'] : '');
        $this->data['megrendelo_ceg']   = isset($this->request->post['megrendelo_ceg']) ? $this->request->post['megrendelo_ceg'] : (!empty($pre_order_info) ? $pre_order_info['megrendelo_ceg'] : '');
        $this->data['megrendelo_adoszam'] = isset($this->request->post['megrendelo_telefon']) ? $this->request->post['megrendelo_adoszam'] : (!empty($pre_order_info) ? $pre_order_info['megrendelo_adoszam'] : '');

        $this->data['date_added']       = isset($this->request->post['date_added'])         ? $this->request->post['date_added']        : (!empty($pre_order_info) ? $pre_order_info['date_added'] : '');
        $this->data['date_modified']    = isset($this->request->post['date_modified'])      ? $this->request->post['date_modified']     : (!empty($pre_order_info) ? $pre_order_info['date_modified'] : '');
        $this->data['megrendelo_uzenet']= isset($this->request->post['megrendelo_uzenet'])  ? $this->request->post['megrendelo_uzenet'] : (!empty($pre_order_info) ? $pre_order_info['megrendelo_uzenet'] : '');
        $this->data['sajat_megjegyzes'] = isset($this->request->post['sajat_megjegyzes'])   ? $this->request->post['sajat_megjegyzes']  : (!empty($pre_order_info) ? $pre_order_info['sajat_megjegyzes'] : '');


        $this->template = 'sale/pre_order_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validateForm() {
        if (!$this->user->hasPermission('modify', 'sale/pre_order')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    private function validateDelete() {
        if (!$this->user->hasPermission('modify', 'sale/pre_order')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
?>