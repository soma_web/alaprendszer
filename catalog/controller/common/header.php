<?php   
class ControllerCommonHeader extends Controller {
    protected function index() {
        $this->log->setMicroTime();

        $alt = $this->config->get("megjelenit_altalanos");

        if(isset($alt) && array_key_exists('emlekezz_ram_login', $alt) && $alt['emlekezz_ram_login'] == '1') {
            if( (!$this->customer->isLogged()) && (array_key_exists('userid', $_COOKIE) && array_key_exists('token', $_COOKIE))) {
                $this->load->model('account/rememberme');
                $this->model_account_rememberme->autoLogin();
            }
        }

		$this->data['title'] = $this->document->getTitle();
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = $this->config->get('config_ssl');
		} else {
			$this->data['base'] = $this->config->get('config_url');
		}
        //$this->load->model('mobile_store/mobile_activator');

        //$this->data['mobile_browser'] = $this->model_mobile_store_mobile_activator->index();

        $this->data['description'] = $this->document->getDescription();
		$this->data['keywords'] = $this->document->getKeywords();
		$this->data['links'] = $this->document->getLinks();	 
		$this->data['styles'] = $this->document->getStyles();
		$this->data['scripts'] = $this->document->getScripts();
		$this->data['lang'] = $this->language->get('code');
		$this->data['google_webmaster'] = $this->config->get('config_google_webmaster');

		$this->data['direction'] = $this->language->get('direction');
		$this->data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');

        $this->language->load('module/lastseen');
        $this->data['lastseen_heading_title']   = $this->language->get('heading_title');

        $this->language->load('module/special_header');

        $this->data['special_heading_title']    = $this->language->get('heading_title');


		$this->language->load('common/header');

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = HTTPS_IMAGE;
		} else {
			$server = HTTP_IMAGE;
		}	
				


		if ($this->config->get('config_icon') && file_exists(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->data['icon'] = $server . $this->config->get('config_icon');
		} else {
			$this->data['icon'] = '';
		}

		$this->data['name'] = $this->config->get('config_name');
		$this->data['address'] = $this->config->get('config_address');
		$this->data['telephone'] = $this->config->get('config_telephone');
		$this->data['store_description'] = nl2br(html_entity_decode($this->config->get('config_other_description'), ENT_QUOTES, 'UTF-8'));

        $logo =  $this->config->get('base_config_language') == $_SESSION['language']  ? $this->config->get('config_logo') :
            ($this->config->get('config_logo_'.$_SESSION['language']) ? $this->config->get('config_logo_'.$_SESSION['language']) : $this->config->get('config_logo'));

        if (!empty($logo) && file_exists(DIR_IMAGE . $logo)) {
            $this->data['logo'] = $server . $logo;
        } else {
            $this->data['logo'] = '';
        }


		/*if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
			$this->data['logo'] = $server . $this->config->get('config_logo');
		} else {
			$this->data['logo'] = '';
		}*/

        $this->data['text_partner'] = $this->language->get('text_partner');
        $this->data['text_ajanlat'] = sprintf($this->language->get('text_ajanlat'), $this->url->link('account/login&feltolto=1', '', 'SSL'));
        $this->data['text_ajanlat_kicsi'] = sprintf('<a href="%s"><img src="catalog/view/theme/kupon/image/responsive/pos_r_creg.png" /></a>', $this->url->link('account/login&feltolto=1', '', 'SSL'));



        $this->data['text_maganszemelyknek']    = $this->language->get('text_maganszemelyknek');
        $this->data['text_termekek']            = $this->language->get('text_termekek');
        $this->data['text_termek']            = $this->language->get('text_termek');
		$this->data['text_kapcsolat']           = $this->language->get('text_kapcsolat');
        $this->data['text_oldalterkep']         = $this->language->get('text_oldalterkep');
		$this->data['text_arlista']             = $this->language->get('text_arlista');
		$this->data['text_belep']               = $this->language->get('text_belep');
		$this->data['text_facebook_belep']      = $this->language->get('text_facebook_belep');
		$this->data['text_kilep']               = $this->language->get('text_kilep');
		$this->data['text_meg_nem_reg']         = $this->language->get('text_meg_nem_reg');
		$this->data['text_motto']               = $this->language->get('text_motto');
		$this->data['text_reg']                 = $this->language->get('text_reg');
		$this->data['text_home']                = $this->language->get('text_home');
		$this->data['text_wishlist']            = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		$this->data['text_wishlist_short']      = $this->language->get('text_wishlist_short');
        $this->data['text_shopping_cart']       = $this->language->get('text_shopping_cart');
    	$this->data['text_search']              = $this->language->get('text_search');
    	$this->data['text_information']         = $this->language->get('text_information');
    	$this->data['text_manufacturer']        = $this->language->get('text_manufacturer');
    	$this->data['text_voucher']             = $this->language->get('text_voucher');
    	$this->data['text_currency']            = $this->language->get('text_currency');
    	$this->data['text_news']                = $this->language->get('text_news');
    	$this->data['text_services']            = $this->language->get('text_services');
    	$this->data['text_kedvezmeny']          = $this->language->get('text_kedvezmeny');

    	$this->data['text_blogreceptek']        = $this->language->get('text_blogreceptek');
    	$this->data['text_gyik']                = $this->language->get('text_gyik');

        $this->data['text_hirlevel']            =  $this->language->get('text_hirlevel');
        $this->data['text_markak']              =  $this->language->get('text_markak');
        $this->data['text_szerviz']             =  $this->language->get('text_szerviz');
        $this->data['text_uzlet']               =  $this->language->get('text_uzlet');
        $this->data['action']                   =  $this->language->get('action');


        $this->data['text_welcome_helios'] = sprintf($this->language->get('text_welcome_helios'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
        if (isset($_SESSION['language']) &&  $_SESSION['language']== "hu"){
            $this->data['text_logged_helios'] = sprintf($this->language->get('text_logged_helios'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName()." ".$this->customer->getLastName(), $this->url->link('account/logout', '', 'SSL'));
        } else {
            $this->data['text_logged_helios'] = sprintf($this->language->get('text_logged_helios'), $this->url->link('account/account', '', 'SSL'), $this->customer->getLastName()." ".$this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
        }

        $honlap_cime=$this->config->get('config_honlap_cim');
        if (!empty($honlap_cime) ) {
            if (substr($this->config->get('config_honlap_cim'),0,4) != "http")
                $this->data['text_honlapom'] = sprintf($this->language->get('text_honlapom'), 'http://'. $this->config->get('config_honlap_cim'));
            else
                $this->data['text_honlapom'] = sprintf($this->language->get('text_honlapom'), $this->config->get('config_honlap_cim'));
        } else
            $this->data['text_honlapom'] = false;

        $this->data['text_welcome']             = sprintf($this->language->get('text_welcome'), $this->url->link('account/login', '', 'SSL'));
        $this->data['text_welcome_belep']       = sprintf($this->language->get('text_welcome_belep'), $this->url->link('account/login', '', 'SSL'));
        $this->data['text_welcome_kicsi']       = sprintf($this->language->get('text_welcome_kicsi'), $this->url->link('account/login', '', 'SSL'));
		$this->data['text_kijelentkezes']       = sprintf($this->language->get('text_kijelentkezes'), $this->url->link('account/logout', '', 'SSL'));
        $this->data['text_kijelentkezes_kicsi'] = sprintf($this->language->get('text_kijelentkezes_kicsi'), $this->url->link('account/logout', '', 'SSL'));
		$this->data['text_regisztral']          = sprintf($this->language->get('text_regisztral'), $this->url->link('account/register', '', 'SSL'));
		$this->data['text_regisztral_csalogat'] = sprintf($this->language->get('text_regisztral_csalogat'), $this->url->link('account/register', '', 'SSL'));
        $this->data['text_regisztral_kicsi']    = sprintf($this->language->get('text_regisztral_kicsi'), $this->url->link('account/register', '', 'SSL'));
        $this->data['text_information_kicsi']   = $this->language->get('text_information_kicsi');


        $this->data['text_regisztral_smallest'] = sprintf($this->language->get('text_regisztral_smallest'), $this->url->link('account/register', '', 'SSL'));
        $this->data['text_welcome_kicsi_smallest'] = sprintf($this->language->get('text_welcome_smallest'), $this->url->link('account/login', '', 'SSL'));
        $this->data['text_kijelentkezes_kicsi_smallest'] = sprintf($this->language->get('text_kijelentkezes_smallest'), $this->url->link('account/logout', '', 'SSL'));
        $this->data['text_information_kicsi_smallest'] = $this->language->get('text_information_kicsi_smallest');
        $this->data['text_szuro_kicsi_smallest'] = $this->language->get('text_szuro_kicsi_smallest');
        $this->data['text_ajanlat_kicsi_smallest'] = sprintf($this->language->get('text_ajanlat_kicsi_smallest'), $this->url->link('account/login&feltolto=1', '', 'SSL'));
        $this->data['text_cart_kicsi_smallest'] = $this->language->get('text_cart_kicsi_smallest');

        $this->data['text_ajanlat_keres']       = $this->language->get('text_ajanlat_keres');
        $this->data['van_ajanlat'] = false;
        if (isset($this->session->data['arajanlat']) && $this->session->data['arajanlat']) {
            $this->data['van_ajanlat'] = true;
        }


		$this->data['text_logged']              = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
		$this->data['text_logged_minosegbe']    = sprintf($this->language->get('text_logged_minosegbe'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
		$this->data['text_account']             = $this->language->get('text_account');
    	$this->data['text_checkout']            = $this->language->get('text_checkout');

        $this->data['facebook_login'] = $this->language->get('facebook_login');
        $this->data['logging_text'] = $this->language->get('logging_text');
        $this->data['text_fbconnect'] = $this->language->get('text_fbconnect');
        $this->data['canclled_text'] = $this->language->get('canclled_text');

    	$this->data['text_tv']                  = $this->language->get('text_tv');
    	$this->data['text_audiohifi']           = $this->language->get('text_audiohifi');
    	$this->data['text_kamera']              = $this->language->get('text_kamera');
    	$this->data['text_fenykepezogep']       = $this->language->get('text_fenykepezogep');
    	$this->data['text_mobil']               = $this->language->get('text_mobil');
    	$this->data['text_monitorprojektor']    = $this->language->get('text_monitorprojektor');
    	$this->data['text_computer']            = $this->language->get('text_computer');
    	$this->data['text_telekomiroda']        = $this->language->get('text_telekomiroda');
    	$this->data['text_haztartasigep']       = $this->language->get('text_haztartasigep');


        $this->data['text_fbconnect']       = $this->language->get('text_fbconnect');
        $this->data['facebook_login']       = $this->language->get('facebook_login');


    	$this->data['text_profkamera']          = $this->language->get('text_profkamera');
    	$this->data['text_protartozekok']       = $this->language->get('text_protartozekok');
    	$this->data['text_cctv']                = $this->language->get('text_cctv');

    	$this->data['text_webshop']             = $this->language->get('text_webshop');
    	$this->data['text_hasznalt']            = $this->language->get('text_hasznalt');
    	$this->data['text_berelheto']           = $this->language->get('text_berelheto');
    	$this->data['text_hirek']               = $this->language->get('text_hirek');


        $this->data['text_pricelist']           = $this->language->get('text_pricelist');
        $this->data['pricelist']                = $this->url->link('myoc/pricelist', '', 'SSL');
        //$this->data['pricelist'] = $this->url->link('product/pricelist', '', 'SSL');

        $this->load->model('setting/store');
        $stores = $this->model_setting_store->getStores();

        if ($stores) {
		    $this->data['home']                 = $this->url->link('common/home','sessionba=0');
        } else {
            $this->data['home']                 = $this->url->link('common/home');
        }


		$this->data['product']              = $this->url->link('myoc/termekek', '', 'SSL');
		$this->data['wishlist']             = $this->url->link('account/wishlist');
		$this->data['logged']               = $this->customer->isLogged();
		$this->data['account']              = $this->url->link('account/account', '', 'SSL');
		$this->data['shopping_cart']        = $this->url->link('checkout/cart');
		$this->data['checkout']             = $this->url->link('checkout/checkout', '', 'SSL');
		$this->data['kapcsolat']            = $this->url->link('information/contact', '', 'SSL');
		$this->data['facebook']             = "https://www.facebook.com/reklamtaska.hu?fref=ts";
        $this->data['sitemap']              = $this->url->link('information/sitemap', '', 'SSL');
        $this->data['news']                 =  $this->url->link('information/news', '', 'SSL');
        $this->data['gyik']                 = $this->url->link('faq/faq', '', 'SSL');

        $this->data['affiliate']            = $this->url->link('affiliate/account', '', 'SSL');
        $this->data['return']               = $this->url->link('account/return/insert', '', 'SSL');
        $this->data['manufacturer']         = $this->url->link('product/manufacturer');
        $this->data['voucher']              = $this->url->link('account/voucher', '', 'SSL');

        $this->data['menu_introduction']    = $this->language->get('menu_introduction');
        $this->data['menu_services']        = $this->language->get('menu_services');
        $this->data['menu_applications']    = $this->language->get('menu_applications');
        $this->data['menu_laser_knowledge'] = $this->language->get('menu_laser_knowledge');
        $this->data['menu_contact']         = $this->language->get('menu_contact');
        $this->data['text_compare'] =   (isset($this->session->data['compare']) && count($this->session->data['compare']) > 0) ? (sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0))) : "";

        $this->data['link_introduction']    = $this->url->link('information/information', 'information_id=7');
        $this->data['link_applications']    = $this->url->link('information/information', 'information_id=9');
        $this->data['link_laser_knowledge'] = $this->url->link('information/information', 'information_id=10');
        $this->data['link_contact']         = $this->url->link('information/contact', '');
        $this->data['link_services']        = $this->url->link('information/information', 'information_id=8');

        $this->data['link_szoltaltatas']    = $this->url->link('information/information', 'information_id=10');
        $this->data['megjelenit_altalanos'] = $this->config->get('megjelenit_altalanos');


        $this->data['link_uzleteink']       = $this->url->link('information/information', 'information_id=11');
        $this->data['link_szervizkozpont']  = $this->url->link('information/information', 'information_id=10');
        $this->data['compare'] = $this->url->link('product/compare');


        $this->data['hatterkep_body'] = $this->config->get('config_hatterkep') ? 'image/'.$this->config->get('config_hatterkep') : '';
        $this->data['hatterkep_body_link']  = $this->config->get('config_hatterkep_link');

        if (isset($this->request->get['filter_name'])) {
			$this->data['filter_name'] = $this->request->get['filter_name'];
		} else {
			$this->data['filter_name'] = '';
		}
		
		// Menu
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('module/multi_category');

        $this->data['latottak'] = $this->model_catalog_product->isLastSeenProducts();

        $akciosak   = $this->model_catalog_product->getTotalProductSpecials();
        $this->data['akciosak'] = $akciosak > 0 ? true : false;



        if (isset($_SESSION['categoryAll']) && empty($_GET['csere'])) {
            $this->data['categories'] = $_SESSION['categoryAll'];
        } else {
            $this->data['categories'] = $this->cache->get('category_all.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));

            if (!$this->data['categories'] || !empty($_GET['csere'])) {
                $this->data['categories'] = array();
                $arr2 = $this->model_module_multi_category->getCategoriesAll(0);
                $categoria_tomb = $this->model_module_multi_category->buildTree($arr2, 0, "");
                $categoria_tomb = $this->model_module_multi_category->onlyFindProduct($categoria_tomb);

                $this->data['categories'] = $this->model_module_multi_category->categorySort($categoria_tomb);
                $this->cache->set('category_all.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $this->data['categories']);
            }
            $_SESSION['categoryAll'] = $this->data['categories'];
        }

        $module_data = array();

        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getExtensions('module');


        foreach ($extensions as $extension) {
            if (substr($extension['code'],0,14) == "kiemeltmenusor"){
                $modules = $this->config->get($extension['code'] . '_module_menusor');

                if ($modules && $modules['status'] == 1) {
                    $this->language->load('module/'.$extension['code']);

                    $module_data[] = array(
                        'megjelenik' => $modules['status'],
                        'sorrend' => $modules['sorrend'],
                        'module_name' => $extension['code'],
                        'link' =>  $this->url->link('module/'.$extension['code'], '', 'SSL'),
                        'name_text' =>  html_entity_decode($modules['fejlec'], ENT_QUOTES, 'UTF-8'),
                    );
                }

                //'name_text' =>  $modules['fejlec']

            }
        }

       $this->language->load('module/pavblog');
        $this->load->model('pavblog/category');
        $this->data['blog_categories'] = $this->model_pavblog_category->getChild();

        $this->load->model('catalog/information');
        $informaciok = $this->model_catalog_information->getInformations();
        $informaciok_teljes = $this->model_catalog_information->getInformationsAll();

        $this->data['informaciok_teljes'] = $informaciok_teljes;
        $this->data['informations'] = array();

        foreach ($informaciok as $result) {
            if ($result['tofooter'] == 1){
                $this->data['informations'][] = array(
                'title' => $result['title'],
                'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }

        $this->data['informations'][] = array(
            'title' => $this->data['text_oldalterkep'],
            'href'  => $this->url->link('information/sitemap')
        );

        $this->data['informations_menu'] = array();
        foreach ($informaciok as $result) {
            if ($result['toheader'] == 1){
                $this->data['informations_menu'][] = array(
                    'title' => $result['title'],
                    'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }

        $this->load->model('account/attribute');
        $attribute_data['kiemelt'] = 1;
        $attribute_data['sort'] = "a.sort_order";

        $this->data['home_active']        = "";
        $this->data['lastseen_active']    = "";
        $this->data['special_active']     = "";
        $this->data['news_active']     = "";

        if (isset($this->request->request['route']) && $this->request->request['route'] == "module/lastseen") {
            $this->data['lastseen_active']    = "active";
        }
        if (isset($this->request->request['route']) && $this->request->request['route'] == "information/news") {
            $this->data['news_active']    = "active";
        }
        if (isset($this->request->request['route']) && $this->request->request['route'] == "common/home" || !isset($this->request->request['route']) ) {
            $this->data['home_active']        = "active";
        }
        if (isset($this->request->request['route']) && $this->request->request['route'] == "module/special_header") {
            $this->data['special_active']     = "active";
        }
        $attributes = $this->model_account_attribute->getAttributes($attribute_data);
        foreach($attributes as $attribute) {
            $active = "";
            if (isset($this->request->request['filter_tulajdonsagok']) &&  $attribute['attribute_id'] == $this->request->request['filter_tulajdonsagok']) {
                $active = "active";
                $this->data['home_active']        = "";
                $this->data['lastseen_active']    = "";
                $this->data['special_active']     = "";
            }

            $this->data['attributes'][] = array(
                'attribute_id'  =>$attribute['attribute_id'],
                'name'          =>$attribute['name'],
                'href'          =>$this->url->link('product/multifilter', 'filter_tulajdonsagok=' . $attribute['attribute_id']),
                'active'        =>$active
            );
        }

        if ($module_data != null) {
            foreach ($module_data as $key => $value) {
                $sorrend[$key] = $value['sorrend'];
            }

            array_multisort($sorrend, SORT_ASC, $module_data);
        }


        $this->data['kiemeltmenusorok'] = $module_data;
        $this->data['lastseen']         = $this->url->link('module/lastseen');
        $this->data['special_header']   = $this->url->link('module/special_header');

        $megjelenit_header                          = $this->config->get('header_beallitas_header');
        $megjelenit_header_csoportok                = $this->config->get('header_beallitas_header_csoportok');
        $this->data['megjelenit_header_csoportok']  = array();

        $vanstatus = false;
        if ($megjelenit_header_csoportok) {
            foreach($megjelenit_header_csoportok as $key=>$value) {
                if ($value['default']['status'] == 1) {
                    $vanstatus = true;
                    $this->data['megjelenit_header_csoportok'][$key] = $value;
                }
            }
        }

        //$this->data['megjelenit_header_csoportok'] = $megjelenit_header_csoportok; // most a teljes van

        $this->data['megjelenit_header']['modularis'] = 0;
        if ($vanstatus) {
            $this->data['megjelenit_header']['modularis'] = ($megjelenit_header['modularis'] == 1) ? 1 : 0;
        }




        /* ---------------     Kosár     ---------------*/



        $this->language->load('module/cart');

        /*if (isset($this->request->get['remove'])) {
            $this->cart->remove($this->request->get['remove']);

            unset($this->session->data['vouchers'][$this->request->get['remove']]);
        }*/

        // Totals
        $this->load->model('setting/extension');

        $total_data = array();
        $total = 0;
        $taxes = $this->cart->getTaxes();

        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get($result['code'] . '_status')) {
                $this->load->model('total/' . $result['code']);

                $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
            }

            $sort_order = array();

            foreach ($total_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $total_data);
        }

        $this->data['totals'] = $total_data;

        $this->data['cart_heading_title'] = $this->language->get('heading_title');

        $mini_cart_price = 0;
        if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
            foreach ($this->session->data['vouchers'] as $voucher) {
                $mini_cart_price += $voucher['amount'];
            }
        }
        $mini_cart_price += $total;
        $darab = $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0);

        if ($this->config->get('megjelenit_mini_cart') == 1 ){
            $this->data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($mini_cart_price));
        } else{
            $this->data['text_items'] = "(" . $darab . ")";
        }

        $this->data['text_items_responsive'] = "(" . $darab . ")";


        /* ---------------     Kosár vége     ---------------*/




        $this->children = array(
			'module/language',
			'module/currency',
			'module/cart',
			'module/sidecart',
			'module/fbjsconnect',
            'module/manufacturer',
            'module/icon_doboz',
            'module/manufacturer_kep',
            'module/newslettersubscribe',
            'common/content_header'

		);
				
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/header.tpl';
		} else {
			$this->template = 'default/template/common/header.tpl';
		}

        /*if(isset($this->data['categories'])){
            $this->data['categories'][] = array(
                'name'     => $this->language->get("text_blogs"),
                'children' => array(),
                'column'   => 1,
                'href'     => $this->url->link('pavblog/blogs', '')
            );
        }*/
        $this->log->GetMicroTime('Action - common/header');

        $this->render();
	} 	
}
?>