<?php
// Text

$_['text_refine']       = 'Refine Search';
$_['text_product']      = 'Termékek';
$_['text_error']        = 'Nem található a kategória!';
$_['text_empty']        = 'Ebben a kategóriában nincsenek termékek.';
$_['text_quantity']     = 'Menny.:';
$_['text_manufacturer'] = 'Márka:';
$_['text_model']        = 'Termékkód:';
$_['text_points']       = 'Jutalom pontok:';
$_['text_price']        = 'Ár:';
$_['text_tax']          = 'Nettó:';
$_['text_reviews']      = '%s vélemény alapján.';
$_['text_compare']      = 'Összehasonlítás (%s)';
$_['text_display']      = 'Nézet:';
$_['text_list']         = 'Lista';
$_['text_grid']         = 'Rács';
$_['text_sort']         = 'Rendezés:';
$_['text_default']      = 'Alapértelmezett';
$_['text_name_asc']     = 'Név szerint növekvő';
$_['text_name_desc']    = 'Név szerint csökkenő';
$_['text_price_asc']    = 'Olcsóbbtól a drágábbig';
$_['text_price_desc']   = 'Drágábbtól az olcsóbbig';
$_['text_rating_asc']   = 'Értékelés (Legrosszabbak elől)';
$_['text_rating_desc']  = 'Értékelés (Legjobbak elől)';
$_['text_model_asc']    = 'Modell szerint növekvő';
$_['text_model_desc']   = 'Modell szerint csökkenő';
$_['text_limit']        = 'Mutat:';
?>