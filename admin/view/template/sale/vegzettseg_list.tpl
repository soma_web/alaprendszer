<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
         <!-- <a onclick="$('form').attr('action', 'delete'); document.getElementById('form').submit();" class="button"><?php echo $button_delete; ?></a>-->
          <a onclick="$('form').attr('action', '<?php echo $delete; ?>'); $('form').submit();" class="button"><?php echo $button_delete; ?></a>

      </div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

                <td class="left"><?php echo $column_vegzettseg; ?></a></td>
              <td class="left"><?php echo $column_sort_order; ?></a></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>

          <tbody>
            <?php if ($vegzettsegek) { ?>
                <?php foreach ($vegzettsegek as $vegzettseg) { ?>
                    <tr>
                        <td style="text-align: center;">
                            <?php if ($vegzettseg['selected']) { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $vegzettseg['eletkor_id']; ?>" checked="checked" />
                            <?php } else { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $vegzettseg['eletkor_id']; ?>" />
                            <?php } ?>
                        </td>

                        <td class="left"><?php echo $vegzettseg['megnevezes']; ?></td>
                        <td class="left"><?php echo $vegzettseg['sort_order']; ?></td>
                        <td class="right"><a href="<?php echo $vegzettseg['action']['href']; ?>"><?php echo $vegzettseg['action']['text']; ?></a></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td class="center" colspan="3"><?php echo $text_no_results; ?></td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function sendVoucher(voucher_id) {
	$.ajax({
		url: 'index.php?route=sale/voucher/send&token=<?php echo $token; ?>&voucher_id=' + voucher_id,
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('.success, .warning').remove();
			$('.box').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('.attention').remove();
		},
		success: function(json) {
			if (json['error']) {
				$('.box').before('<div class="warning">' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('.box').before('<div class="success">' + json['success'] + '</div>');
			}
		}
	});
}
//--></script> 
<?php echo $footer; ?>