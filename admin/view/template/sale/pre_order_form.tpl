<?php echo $header; ?>

<style>
    .olvashato {
        background-color: #eee;
    }

    .uzenetek {
        min-width: 400px;
        min-height: 100px;
    }

</style>

<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
          <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <table class="form">

              <tr>
                <td><?php echo $entry_pre_order_id ?></td>
                <td><input class="olvashato" type="text" name="pre_order_id" value="<?php echo $pre_order_id; ?>" size="8" readonly /></td>
              </tr>
              <tr>
                <td><?php echo $entry_product_id ?></td>
                <td><input class="olvashato" type="text" name="product_id" value="<?php echo $product_id; ?>" size="8" readonly /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_product_name ?></td>
                  <td><input class="olvashato"  type="text" name="product_name" value="<?php echo $product_name; ?>" readonly size="25"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_model ?></td>
                  <td><input class="olvashato"  type="text" name="model" value="<?php echo $model; ?>" readonly size="11"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_cikkszam ?></td>
                  <td><input class="olvashato"  type="text" name="cikkszam" value="<?php echo $cikkszam; ?>" readonly size="11"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_gyarto ?></td>
                  <td><input class="olvashato"  type="text" name="gyarto" value="<?php echo $gyarto; ?>" readonly size="25"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_megrendelo_name ?></td>
                  <td><input type="text" name="megrendelo_name" value="<?php echo $megrendelo_name; ?>" size="25"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_megrendelo_email ?></td>
                  <td><input type="text" name="megrendelo_email" value="<?php echo $megrendelo_email; ?>" size="25"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_telefon ?></td>
                  <td><input type="text" name="megrendelo_telefon" value="<?php echo $megrendelo_telefon; ?>" size="25"  /></td>
              </tr>



              <tr>
                  <td><?php echo $entry_iranyitoszam ?></td>
                  <td><input type="text" name="megrendelo_iranyitoszam" value="<?php echo $megrendelo_iranyitoszam; ?>" size="5"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_varos ?></td>
                  <td><input type="text" name="megrendelo_varos" value="<?php echo $megrendelo_varos; ?>" size="25"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_utca ?></td>
                  <td><input type="text" name="megrendelo_utca" value="<?php echo $megrendelo_utca; ?>" size="25"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_ceg ?></td>
                  <td><input type="text" name="megrendelo_ceg" value="<?php echo $megrendelo_ceg; ?>" size="25"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_adoszam ?></td>
                  <td><input type="text" name="megrendelo_adoszam" value="<?php echo $megrendelo_adoszam; ?>" size="25"  /></td>
              </tr>




              <tr>
                  <td><?php echo $entry_date_added ?></td>
                  <td><input  class="olvashato"  type="text" name="date_added" value="<?php echo $date_added; ?>" readonly size="8"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_date_modified ?></td>
                  <td><input  class="olvashato"  type="text" name="date_modified" value="<?php echo $date_modified; ?>" readonly size="8"  /></td>
              </tr>
              <tr>
                  <td><?php echo $entry_megrendelo_uzenet ?></td>
                  <td><textarea class='uzenetek' name="megrendelo_uzenet"><?php echo $megrendelo_uzenet; ?></textarea></td>
              </tr>

              <tr>
                  <td><?php echo $entry_sajat_megjegyzes ?></td>
                  <td><textarea class='uzenetek'  name="sajat_megjegyzes"><?php echo $sajat_megjegyzes; ?></textarea></td>
              </tr>

          </table>

      </form>
    </div>
  </div>
</div>


<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript">
    $('.date').datepicker({dateFormat: 'yy-mm-dd'});

    $('.datetime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'h:m'
    });

</script>


<?php echo $footer; ?>