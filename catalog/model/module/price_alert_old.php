<?php
class ModelModulePriceAlert extends Model {
	public function addAlert($data){
		$sql = "INSERT INTO " . DB_PREFIX . "price_alert 
		        SET name            ='" . $this->db->escape($data['pa_fullname']) . "', 
		            email           ='" . $this->db->escape($data['pa_email']) . "', 
					product_id      ='" . (int)$data['pa_product_id'] . "',
		            price           ='" . (float)$data['pa_desired_price'] . "',
					currency_code   ='" . $this->session->data['currency'] . "'";
		
		$this->db->query($sql);
	}
	
	public function editAlert($price_alert_id, $data){
		$sql = "UPDATE " . DB_PREFIX . "price_alert 
		        SET name            ='" . $this->db->escape($data['pa_fullname']) . "', 
		            email           ='" . $this->db->escape($data['pa_email']) . "', 
					product_id      ='" . (int)$data['pa_product_id'] . "',
		            price           ='" . (float)$data['pa_desired_price'] . "',
					currency_code   ='" . $this->session->data['currency'] . "'
				WHERE price_alert_id='" . (int)$price_alert_id . "'";
		
		$this->db->query($sql);
	}
		
	public function existAlert($data){
		$sql = "SELECT price_alert_id FROM " . DB_PREFIX . "price_alert WHERE LOWER(email)='" . strtolower($this->db->escape($data['pa_email'])) . "' AND product_id ='" . (int)$data['pa_product_id'] . "'";
		$query = $this->db->query($sql);
		
		if ($query->num_rows){
			return $query->row['price_alert_id'];
		} else {
			return 0;
		}
	}
	
	public function deleteAlert($alert_id){
		$this->db->query("DELETE FROM " . DB_PREFIX . "price_alert WHERE price_alert_id='" . (int)$alert_id . "'" );
	}
	
	public function getSubscribers(){
		$query = $this->db->query("SELECT DISTINCT name, email FROM " . DB_PREFIX . "price_alert");
		
		return $query->rows;
	}
	
	public function getSubscriberAlerts($email){
		$sql = "SELECT * FROM " . DB_PREFIX . "price_alert WHERE LOWER(email)='" . strtolower($email) . "'";
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
}
?>