<?php
// Heading
$_['heading_title']        = 'Fiók regisztráció';

// Text
$_['text_account']         = 'Fiók';
$_['text_register']        = 'Regisztráció';
$_['text_account_already'] = 'Ha már van nálunk fiókja, kérem jelenkezzen be a <a href="%s">bejelentkező oldalon</a>.';
$_['text_your_details']    = 'Az Ön személyes adatai';
$_['text_your_address']    = 'Cím';
$_['text_newsletter']      = 'Hírlevél';
$_['text_your_password']   = 'Jelszó';
$_['error_fb_email_error']   = 'Kérem engedélyezze az email cím megadását a facebook belépéshez vagy regisztráljon új fiókot!';

$_['text_agree']           = 'Elolvastam és megértettem az <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>-et és az <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>ot!';

// Entry
$_['entry_adoszam']        = 'Adószám:';
$_['entry_firstname']      = 'Vezetéknév:';
$_['entry_lastname']       = 'Keresztnév:';
$_['entry_email']          = 'E-Mail:';
$_['entry_email_again']    = 'E-Mail cím megerősítése:';
$_['entry_telephone']      = 'Telefon:';
$_['entry_fax']            = 'Fax:';
$_['entry_company']        = 'Cégnév / Egyéni vállalkozó:';
$_['entry_adoszam']        = 'Adószám:';
$_['entry_nem']            = 'Neme:';
$_['entry_eletkor']        = 'Életkor:';

$_['entry_address_1']      = 'Utca, házszám:';
$_['entry_address_2']      = 'Kiegészítő címadatok:';
$_['entry_postcode']       = 'Irányítószám:';
$_['entry_city']           = 'Város:';
$_['entry_country']        = 'Ország:';
$_['entry_zone']           = 'Állam / Megye:';
$_['entry_newsletter']     = 'Megrendelem:';
$_['entry_password']       = 'Jelszó:';
$_['entry_confirm']        = 'Jelszó megerősítése:';

// Error
$_['error_exists']         = 'Figyelem: AZ E-Mail cím már regisztrálva van!';
$_['error_firstname']      = 'A vezetéknevet meg kell adni (max 32 karakter)!';
$_['error_lastname']       = 'A keresztnevet meg kell adni (max 32 karakter)!';
$_['error_email']          = 'Érvénytelen E-Mail cím!';
$_['error_telephone']      = 'A telefonszámnak min. 3, max. 32 számjegyből kell állnia!';
$_['error_password']       = 'A jelszónak min. 4, max. 20 karakterből kell állnia!';
$_['error_confirm']        = 'A jelszó és annak megerőssítése nem egyeznek!';
$_['error_address_1']      = 'Az utca, házszám mezőnek min. 3, max. 128 karakterből kell állnia!';
$_['error_city']           = 'A város mezőnek min. 2, max. 128 karakterből kell állnia!';
$_['error_postcode']       = 'Az irányítószámnak min. 2, max. 10 karakterből kell állnia!';
$_['error_country']        = 'Kérem válasszon országot!';
$_['error_zone']           = 'Kérem válasszon megyét!';
$_['error_agree']          = 'Figyelem: El kell fogadnia a %s!';
$_['error_eletkor']        = 'Kérem válassza ki az életkorát';
$_['error_nem']            = 'Kérem válassza ki a nemét';
$_['error_company']         = 'A cégnév megadása kötelező!';
$_['error_adoszam']         = 'Az adószám megadása kötelező';
$_['error_vallalkozasi_forma']  = 'A vállalkozási forma megadása kötelező!';
$_['error_szekhely']            = 'A székhely megadása kötelező!';
$_['error_ugyvezeto_neve']      = 'Az ügyvezető neve kötelező mező!';
$_['error_ugyvezeto_telefonszama']  = 'Az ügyvezető telefonszáma kötelező mező!';
$_['entry_weblap']  = 'Weblap címe:';

$_['text_paypaltitle']     = 'Paypal számla részletek';
$_['text_paypallable']     = 'Paypal számla Email Id';

$_['select_ferfi']         = 'Férfi';
$_['select_no']            = 'Nő';

$_['text_your_cegadatok']           = 'Cégadatok';
$_['entry_iskolai_vegzettseg']      = 'Iskolai végzettség';
$_['entry_vallalkozasi_forma']      = 'Vállalkozási forma:';
$_['entry_szekhely']                = 'Székhely:';
$_['entry_ugyvezeto_neve']          = 'Ügyvezető neve:';
$_['entry_ugyvezeto_telefonszama']  = 'Ügyvezető telefonszáma:';

?>