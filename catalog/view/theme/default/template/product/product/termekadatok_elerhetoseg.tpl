<?php if (isset($termekadatok['megjelenit_product_elerhetoseg']) && !empty($termekadatok['megjelenit_product_elerhetoseg'])) { ?>
    <div class="termekadatok termekadatok-elerhetoseg" >
        <span class="termekadatok-elerhetoseg-szoveg"><?php echo $termekadatok['megjelenit_product_elerhetoseg']['text_name']; ?></span>
        <span class="termekadatok-elerhetoseg-ertek"><?php echo $termekadatok['megjelenit_product_elerhetoseg']['value']; ?></span>
        <?php if (!empty($product_info['quantity'])) { ?>
            <span class="termekadatok-elerhetoseg-ertek"><?php echo $termekadatok['megjelenit_product_mennyisegi_egyseg']['value']; ?></span>
        <?php } ?>
    </div>
<?php } ?>