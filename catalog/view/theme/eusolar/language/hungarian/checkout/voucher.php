<?php
// Heading
$_['heading_title']    = 'Vásároljon Ajándékutalványt!';

// Text
$_['text_voucher']     = 'Ajándékutalvány';
$_['text_description'] = 'Az ajándékutalvány kifizetés után el lesz küldve e-mailben a címzettnek!';
$_['text_agree']       = 'Tudomásul veszem, hogy ajándékutalványok ellenértéke nem téríthető vissza.';
$_['text_message']     = '<p>Köszönjük, hogy ajándékutalványt vásárolt! A rendelés befejeztével az ajándékutalvány beváltásának módjáról küldünk egy e-mailt az utalvány címzettjének.</p>';
$_['text_for']         = '%s Ajándékutalvány %s részére';

// Entry
$_['entry_to_name']    = 'Címzett neve:';
$_['entry_to_email']   = 'Címzett e-mail címe:';
$_['entry_from_name']  = 'Ön neve:';
$_['entry_from_email'] = 'Ön e-mail címe:';
$_['entry_message']    = 'Üzenet:<br /><span class="help">(Választható)</span>';
$_['entry_amount']     = 'Mennyiség:<br /><span class="help">(Az értéknek %s és %s közt kell lennie!)</span>';
$_['entry_theme']      = 'Ajándékutalvány téma:';

// Error
$_['error_to_name']    = 'A címzett neve legalább 1 és legfeljebb 64 karakterből állhat!';
$_['error_from_name']  = 'Az Ön neve legalább 1 és legfeljebb 64 karakterből állhat!';
$_['error_email']      = 'Nem valós e-mail cím!';
$_['error_amount']     = 'A mennyiség legalább %s és legfeljebb %s karakterből állhat!';
$_['error_theme']      = 'Témát kell választania!';
$_['error_agree']      = 'Figyelem: El kell fogadnia, hogy ajándékutalványok ellenértéke nem téríthető vissza!';
?>