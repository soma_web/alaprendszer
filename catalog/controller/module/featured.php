<?php
class ControllerModuleFeatured extends Controller {
    protected function index($setting) {
		$this->language->load('module/featured',$this->config->get('config_template'));

      	$this->data['heading_title'] = $this->language->get('heading_title');
      	$this->data['osszes_gomb_title'] = $this->language->get('osszes_gomb_title');

        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
        $this->data['kaphato'] = $this->language->get('kaphato');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
        $this->data['button_bovebben'] = $this->language->get('button_bovebben');
        $this->data['text_tax'] = $this->language->get('text_tax');
        $this->data['text_netto'] = $this->language->get('text_netto');
        $this->data['module_name'] = "featured";
        $this->data['button_wishlist'] = $this->language->get('button_wishlist');
        $this->data['button_compare'] = $this->language->get('button_compare');


        $this->data['text_akcio_title']         = $this->language->get('text_akcio_title');
        $this->data['text_cikkszam']            = $this->language->get('text_cikkszam');
        $this->data['text_pardarab_title']      = $this->language->get('text_pardarab_title');
        $this->data['text_uj_title']            = $this->language->get('text_uj_title');
        $this->data['text_compare']             = $this->language->get('text_compare');
        $this->data['text_raktaron']            = $this->language->get('text_raktaron');
        $this->data['text_nincs_raktaron']      = $this->language->get('text_nincs_raktaron');


        if ($this->config->get('featured_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

        if ($this->config->get('featured_module_osszes') == 1) {
            $this->data['osszes_gomb'] = true;
        } else {
            $this->data['osszes_gomb'] = false;
        }

        $this->data['featured_osszes_nmb']  = $this->config->get('featured_osszes_nmb');

        $this->load->model('catalog/product');
		
		$this->load->model('tool/image');

		$this->data['products'] = array();

		$products = explode(',', $this->config->get('featured_product'));


        //ThemeGlobal.com - OpenCart Templates Club. Unlimited access to all of our themes for only $49
        if (isset($setting['position'])) {
            $this->data['position'] = $setting['position'];
        }
        $this->document->addScript('catalog/view/javascript/jquery/jquery.jcarousel.min22.js');

        if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/carousel22.css')) {
            $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/carousel22.css');
        } else {
            $this->document->addStyle('catalog/view/theme/default/stylesheet/carousel22.css');
        }
        //ThemeGlobal.com - OpenCart Templates Club. Unlimited access to all of our themes for only $49

		if (isset($setting['limit']) &&  empty($setting['limit'])) {
			$setting['limit'] = 15;
		}

        if (isset($setting['limit'])) {
		    $products = array_slice($products, 0, (int)$setting['limit']);
        }
        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => isset($setting['limit']) ? $setting['limit'] : 10000
        );

        $result1 = $this->model_catalog_product->getProductsNew($data1);
        $this->data['position'] = isset($setting['position']) ? $setting['position'] : 0 ;
        if ($this->config->get('megjelenit_korhinta') == 1) {
            if (isset($setting['show']) ) {
                $this->data['show'] = $setting['show'];
            } else {
                $this->data['show'] = 0;
            }

            if (isset($setting['megjelenit']) ) {
                $this->data['megjelenit'] = $setting['megjelenit'];
            } else {
                $this->data['megjelenit'] = 0;
            }

            if (isset($setting['auto']) ) {
                $this->data['auto'] = $setting['auto'];
            } else {
                $this->data['auto'] = 0;
            }

            if (isset($setting['infinity']) ) {
                $this->data['infinity'] = $setting['infinity'];
            } else {
                $this->data['infinity'] = 0;
            }

        } else {
            $this->data['show'] = 0;
        }



        $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
        $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');

        foreach ($products as $product_id) {
			$result = $this->model_catalog_product->getProduct($product_id);
            if ($result['status'] == 1){
                $this->data['products'][] = $this->product->productPreparation($result,$setting,$result1);
            }
		}

        $this->data['products'] = $this->config->rendezes( $this->data['products'], "kiemelt_sorrend","DESC","kiemelt_al_sorrend","DESC");


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/featured.tpl';
		} else {
			$this->template = 'default/template/module/featured.tpl';
		}

		$this->render();

	}
}
?>