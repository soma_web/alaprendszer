<div class="wishlist">
<a title="<?php echo isset($beallitas['kivansaglista']) ? $button_wishlist : ''; ?>" onclick="addToWishList('<?php echo $product['product_id']; ?>');">
    <?php if (isset($beallitas['kivansaglista_ikon']) && $beallitas['kivansaglista_ikon']) { ?>
        <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/wishlist.png')) {
            $icon = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/wishlist.png';
        } else if(file_exists(DIR_TEMPLATE_IMAGE.'default/image/wishlist.png')) {
            $icon = DIR_TEMPLATE_IMAGE.'default/image/wishlist.png';
        } else {
            $icon = "";
        }?>
        <img class="product-icon-wishlist" title="<? if (isset($button_wishlist)) { echo $button_wishlist; }?>"  src="<? echo $icon?>" >
    <?php } ?>
    <?php if (!isset($beallitas['kivansaglista'])) { ?>
        <?php echo $button_wishlist;  ?>
    <?php }  ?>
</a>
</div>
