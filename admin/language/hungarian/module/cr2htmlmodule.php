<?php
// Heading
$_['heading_title']    = 'HTML Modul';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeres módosítás!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

// Entry
$_['entry_product']       = 'Termékek:';
$_['entry_area_id']       = 'HTML Terület:';
$_['entry_layout']        = 'Kiosztás:';
$_['entry_position']      = 'Pozició:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_borderless']    = 'Keret nélküli design:<br /><span class="help">Teljesen kikapcsolja az alap html modul wrappingját (beleértve a header és a kereteket).</span>';
$_['entry_borderlesswarn'] = 'Óvatosan használja ezt a lehetőséget, mert tönkreteheti az oldal kiosztását és kinézetét, attól függően hogy milyen kódot használ.';
$_['entry_classname']    = 'CSS Osztály:';
$_['entry_area']      = 'HTML Terület';
$_['entry_code']       = 'HTML Tartalom:<br /><span class="help">Használja a "forrás" gombot hogy kerüljön minden hasonló dolgot amikor Komplex HTML kódot használ.</span>';
$_['entry_header']     = 'Cím mutatása:<br /><span class="help">Mutassa a címet a modul fejlécében.</span>';
$_['entry_title']      = 'Cím:<br /><span class="help">Ha ez a sáv üres de a Cím mutatása mód be van kapcsolva akkor az alapértelmezett cím kerül mutatásra.</span>';
$_['entry_yes']	       = 'Igen';
$_['entry_no']	       = 'Nem';

// Error
$_['error_permission'] = 'Nincs módosítási engedélye!';
$_['error_code']       = 'Kód szükséges';
?>
