<?php
$_['tab_vatera']              = 'Vatera';

$_['vat_vazonosito']             = 'Vatera azonosító';
$_['vat_termeknev']              = 'Termék neve';
$_['vat_alcim']                  = 'Alcím';
$_['vat_alcim_help']             = 'Nem kötelező, sőt fizetős! Ha nem kéri ezt a Vatera szolgáltatást, hagyja az oszlopot üresen!';
$_['vat_darabszam']              = 'Darabszám';
$_['vat_darabszam_help']         = 'A 0 darabszámú termékeket törli a Vatera rendszere! Maximum érték: 9999';
$_['vat_kikialtasiar']           = 'Kikiáltási ár';
$_['vat_minimalar']              = 'Minimál ár';
$_['vat_ar_help']                = 'Az ár oszlopokban megadott értékek csak egész számok lehetnek (Nem helyes: 11,2; 34 Ft)';
$_['vat_villamar']               = 'Villám ár';
$_['vat_villamar_help']          = 'Az ár oszlopokban megadott értékek csak egész számok lehetnek (Nem helyes: 11,2; 34 Ft) Amennyiben termékeit fixáron kívánja értékesíteni, a három ár mezőnek azonosnak kell lennie; A minimálár és a villámár nem lehet kisebb a kikiáltási árnál; A villámár nem lehet kisebb a minimálárnál; Az ár minden esetben bruttó ár!';
$_['vat_licitlepcso']            = 'Licitlépcső';
$_['vat_licitlepcso_help']       = 'Kötelező kitölteni. Csak egész számot adhat meg. Ha "0"-t ad meg, a Vatera rendszere automatikusan állítja be a licitlépcsőt. Fix áras termék esetén "0"-t kell megadni!';
$_['vat_aukcio_inditasa']        = 'Aukció indítása';
$_['vat_aukcio_inditasa_help']   = 'Időzítési formátumra példa:2014-06-09 10:00:00';
$_['vat_aukcio_idotartam']       = 'Aukció időtartama';
$_['vat_aukcio_idotartam_help']  = 'Az aukció hossza napokban megadva. Lehetséges értékek: 1,2,3,4,5,6,7,8,9,10,11,14,21';
$_['vat_vatera_kategoria_kod']   = 'Vatera kategóriakód';
$_['vat_vatera_kategoria_kod_help']   = 'Itt azoknak a kategóriáknak a kódja szerepel, amelyekben az aukcióját el kívánja indítani. A kategóriakódokat <a href="http://www.vatera.hu/listings/map.php?action=code">ezen az oldalon megtalálja</a>! Figyelem aukciót mindig csak a legalsó kategóriában indíthat!';
$_['vat_megye']                  = 'Megye';
$_['vat_orszag']                 = 'Ország';
$_['vat_sablon_azonosito']       = 'Szállítási sablon azonosító';
$_['vat_sablon_azonosito_help']  = 'Először a Vaterás fiókba belépve a Beállítások>>Eladási beállítások>>Szállítási és fizetési sablon menüpont alatt létre kell hozni egy (vagy több) sablont, majd a sablon mentése után az oldal forráskódjából ki kell keresni a sablon 7 jegyű azonosítószámát.';
$_['vat_ujrainditasok_szama']    = 'Újraindítások száma';
$_['vat_ujrainditasok_szama_help']  = 'Lehetséges értékek: 0, 1, 2, 3, 999.
Amennyiben azt szeretné, hogy a lejárt aukciók, azon termékek, amelyek nem keltek el, ne induljanak automatikusan újra, 0- át adjon meg ebben a mezőben.';
$_['vat_kiemelesek']             = 'Kiemelések (Feláras szolgáltatások) ';
$_['vat_kiemelesek_help']        = 'Lehetséges értékek: Félkövér: 1; Listázások elejére: 2; Bekeretezés: 4; Színes háttér: 8; 5 perces hosszabítás: 32 Kiemelés a főoldalra: 64; Alcím a listázóban: 128. Ha ön több termékkiemelést is igénybe kíván venni, a kódok összegét kell a mezőnek értékül adni.';
$_['vat_termek_jellemzok']       = 'Termékjellemzők';
$_['vat_termek_jellemzok_help']  = 'Minimálár látható: 2; Eladó állja a szállítási költséget: 4; Vásárló fizeti a szállítási költséget: 8; Új termék: 16 (ha használt, akkor 0); Van számlaadás: 128; Én ajánlatom automatikus elfogadása: 256; Csak a Vaterára töltse fel a terméket: 512. Amennyiben Ön több termékjellemzőt kíván alkalmazni, a kódok összegét kell a mezőnek értékül adni.';
$_['vat_garancia']               = 'Garancia';
$_['vat_garancia_help']          = 'Lehetséges értékek: Nincs: 1; Egy hét: 3; Egy hónap: 4; Három hónap: 5; Hat hónap: 6; Egy év: 7; Két év: 8; Több, mint két év: 9; Élettartam garancia: 10';
$_['vat_elfogadas_szazalek']     = 'Én ajánlatom funkció elfogadásának feltétele';
$_['vat_elfogadas_szazalek_help']= 'Az én ajánlatom opció százalékos értéke % jel nélkül. Értéke 75 és 99 között lehet. Az én ajánlatom opció csak fixáras értékesítés esetén vehető igénybe.';
$_['vat_kepek1']                 = 'Képek I.';
$_['vat_kepek2']                 = 'Képek II.';
$_['vat_kepek3']                 = 'Képek III.';
$_['vat_kepek4']                 = 'Képek IV.';
$_['vat_kinezeti_sablon']        = 'Termékkinézeti sablon azonosító';
$_['vat_kinezeti_sablon_help']   = '0 ha nem kell, vagy valamelyik kinézeti sablon sorszáma!';
$_['vat_product_id']             = 'Termék azonosító kód';
$_['vat_kategoria_specifikus']   = 'Kategória specifikus tulajdonságok';
$_['vat_kategoria_specifikus_help']   = 'Az egyes termékkategóriákra jellemző tulajdonságok egyetlen szövegdarabként, melyben a tulajdonság azonosító szám és a hozzá tartozó tulajdonság értéke "_|_" karaktersorral van elválasztva egymástól, és az ilyen párok egymástól "_%_" karaktersorral vannak elválasztva.';
$_['vat_kepek5']                 = 'Képek V.';
$_['vat_kepek6']                 = 'Képek VI.';
$_['vat_kepek7']                 = 'Képek VII.';
$_['vat_kepek8']                 = 'Képek VIII.';
$_['vat_leiras']                 = 'Leírás';
$_['vat_leiras_help']            = 'Maximum 3900, minimum 50 karakter lehet.';
$_['vat_automatikus_kuldes']     = 'Automatikus küldés';



?>
