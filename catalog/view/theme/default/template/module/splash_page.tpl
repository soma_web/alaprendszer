<?php if ($show_splash) { ?>
<style>
#splash-content{
	position: fixed;
    top:  50%;
    left: 50%;
	margin-top: -<?php echo $css_move_up; ?>px;
	margin-left:-<?php echo $css_move_left; ?>px;
}
</style>

<div id="splash<?php echo $module; ?>" class="splash">
    <a href="#" id="close-splash<?php echo $module; ?>" class="close-splash">[<?php echo $text_close;?>]</a>
	<a class="dont-show" id="dont-show<?php echo $module; ?>">[<?php echo $text_dont_show_again; ?>]</a>
	<div id="splash-content">
		<img id="enter<?php echo $module; ?>" src="<?php echo $image; ?>" alt="<?php echo $module; ?>" />
	</div>
</div>


<script type="text/javascript"><!--
$(function(){
	$('#splash<?php echo $module; ?>').meerkat({
		<?php if ($color != "" && $pattern != ""){ ?>
		background: '#<?php echo $color; ?> url(\'<?php echo $pattern?>\') repeat left top', 
		<?php } elseif ($pattern != "" && $color == "") { ?>
		background: url(\'<?php echo $pattern?>\') repeat left top',
		<?php } else { ?>
		background: '#<?php echo $color; ?>',
		<?php } ?>
		height: '100%',
		width: '100%',
		position: 'left',
		close: '#close-splash<?php echo $module; ?>',
		dontShowAgain: '#dont-show<?php echo $module; ?>',
		animationIn: 'none',
		animationOut: 'fade',
		animationSpeed: 500,
		timer: <?php echo $timer?>,
		removeCookie: '.reset'
	});
});
--></script>

<?php } ?>