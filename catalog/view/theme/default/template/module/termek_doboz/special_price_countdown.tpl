<?php if (isset($product['countdown']['show_special_price_countdown']) && $product['countdown']['show_special_price_countdown']) { ?>

    <style type="text/css">
        .counter_desc_grid .cntSeparator {
            font-size: <?php echo $product['countdown']['new_font_size'];?>px;
            margin: 5px auto;
            height: <?php echo $product['countdown']['digit_height'];?>;
            color: #000;
        }
        .counter_desc_grid .desc { margin: 7px 3px; }
        .counter_desc_grid .desc div {
            float: left;
            font-family: Arial;
            width: 70px;
            margin-right: 65px;
            font-size: 13px;
            font-weight: bold;
            color: #000;
        }
    </style>

    <div class="counter-box">

        <div class="counter_desc_grid" id="counter_<?php echo $module_name.$product['product_id']; ?>" style="width:auto;"></div>

    </div>

    <script type="text/javascript">
        $('#counter_<?php echo $module_name.$product['product_id']; ?>').countdown({
            image: '<?php echo $product['countdown']['digit_image']; ?>',
            digitWidth: <?php echo $product['countdown']['digit_width']; ?>,
            digitHeight: <?php echo $product['countdown']['digit_height']; ?>,
            format: '<?php echo $product['countdown']['countdown_format']; ?>',
            <?php if ($product['countdown']['show_days']) { ?>
            startTime: '<?php echo ($product['countdown']['days'] < 10)? "0" . $product['countdown']['days'] : $product['countdown']['days']; ?>:<?php echo ($product['countdown']['hours'] < 10)? "0" . $product['countdown']['hours'] : $product['countdown']['hours']; ?>:<?php echo ($product['countdown']['mins'] < 10)? "0" . $product['countdown']['mins'] : $product['countdown']['mins']; ?>:<?php echo ($product['countdown']['secs'] < 10)? "0" . $product['countdown']['secs'] : $product['countdown']['secs']; ?>'
            <?php } else { ?>
            startTime: '<?php echo ($product['countdown']['hours'] < 10)? "0" . $product['countdown']['hours'] : $product['countdown']['hours']; ?>:<?php echo ($product['countdown']['mins'] < 10)? "0" . $product['countdown']['mins'] : $product['countdown']['mins']; ?>:<?php echo ($product['countdown']['secs'] < 10)? "0" . $product['countdown']['secs'] : $product['countdown']['secs']; ?>'
            <?php }?>
        });

    </script>

<?php } ?>
