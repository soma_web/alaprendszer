<?php
class ControllerPaymentOtp extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('payment/otp');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            /*if(!empty($this->request->files['otp_key_file']['name'])) {
                if($this->saveFile($this->request->files['otp_key_file'])){
                    $this->error['warning'] = $this->saveFile($this->request->files['otp_key_file']);
                    $this->request->post['otp_key_file'] = $this->config->get('otp_key_file');
                }else{
                    $this->request->post['otp_key_file'] = $this->rename_file($this->request->files['otp_key_file']['name']);
                }
            } else {
                $this->request->post['otp_key_file'] = $this->config->get('otp_key_file');
            }*/

            $this->model_setting_setting->editSetting('otp', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');
            if(empty($this->error['warning'])){
                $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
            }
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_all_zones'] = $this->language->get('text_all_zones');
        $this->data['text_select']      = $this->language->get('text_select');

        $this->data['entry_order_status'] = $this->language->get('entry_order_status');
        $this->data['entry_total'] = $this->language->get('entry_total');
        $this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $this->data['entry_privkey'] = $this->language->get('entry_privkey');
        $this->data['entry_haromszereplos'] = $this->language->get('entry_haromszereplos');
        $this->data['entry_otp_webshop_client'] = $this->language->get('entry_otp_webshop_client');
        $this->data['entry_crm_fizetesi_mod'] = $this->language->get('entry_crm_fizetesi_mod');
        $this->data['entry_crm_fizetesi_hatarido'] = $this->language->get('entry_crm_fizetesi_hatarido');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['tab_general'] = $this->language->get('tab_general');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_payment'),
            'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('payment/otp', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('payment/otp', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['otp_total'])) {
            $this->data['otp_total'] = $this->request->post['otp_total'];
        } else {
            $this->data['otp_total'] = $this->config->get('otp_total');
        }

        if (isset($this->request->post['otp_privkey'])) {
            $this->data['otp_privkey'] = $this->request->post['otp_privkey'];
        } else {
            $this->data['otp_privkey'] = $this->config->get('otp_privkey');
        }
        if (isset($this->request->post['otp_haromszereplos'])) {
            $this->data['otp_haromszereplos'] = $this->request->post['otp_haromszereplos'];
        } else {
            $this->data['otp_haromszereplos'] = $this->config->get('otp_haromszereplos');
        }
        if (isset($this->request->post['otp_webshop_client'])) {
            $this->data['otp_webshop_client'] = $this->request->post['otp_webshop_client'];
        } else {
            $this->data['otp_webshop_client'] = $this->config->get('otp_webshop_client');
        }

        /*if (isset($this->request->post['otp_key_file'])) {
            $this->data['otp_key_file'] = $this->request->post['otp_key_file'];
        } else {
            $this->data['otp_key_file'] = $this->config->get('otp_key_file');
        }*/

        if (isset($this->request->post['otp_order_status_id'])) {
            $this->data['otp_order_status_id'] = $this->request->post['otp_order_status_id'];
        } else {
            $this->data['otp_order_status_id'] = $this->config->get('otp_order_status_id');
        }

        $this->load->model('localisation/order_status');

        $this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['otp_geo_zone_id'])) {
            $this->data['otp_geo_zone_id'] = $this->request->post['otp_geo_zone_id'];
        } else {
            $this->data['otp_geo_zone_id'] = $this->config->get('otp_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        if (isset($this->request->post['otp_status'])) {
            $this->data['otp_status'] = $this->request->post['otp_status'];
        } else {
            $this->data['otp_status'] = $this->config->get('otp_status');
        }

        if (isset($this->request->post['otp_sort_order'])) {
            $this->data['otp_sort_order'] = $this->request->post['otp_sort_order'];
        } else {
            $this->data['otp_sort_order'] = $this->config->get('otp_sort_order');
        }

        $this->data['crm_kapcsolat'] = $this->config->get('megjelenit_megrendeles_crm_integracio');
        if ($this->data['crm_kapcsolat']) {
            $this->load->model("payment/crm_kapcsolat");
            $this->data['crm_fizetesi_modok'] = $this->model_payment_crm_kapcsolat->getFizetesiMod();

            if (isset($this->request->post['otp_crm_fizetesi_mod'])) {
                $this->data['otp_crm_fizetesi_mod'] = $this->request->post['otp_crm_fizetesi_mod'];
            } else {
                $this->data['otp_crm_fizetesi_mod'] = $this->config->get('otp_crm_fizetesi_mod');
            }

            if (isset($this->request->post['otp_crm_fizetesi_hatarido'])) {
                $this->data['otp_crm_fizetesi_hatarido'] = $this->request->post['otp_crm_fizetesi_hatarido'];
            } else {
                $this->data['otp_crm_fizetesi_hatarido'] = $this->config->get('otp_crm_fizetesi_hatarido');
            }

        }


        $this->template = 'payment/otp.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'payment/otp')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
    private function saveFile($file)
    {

        $target_dir = DIR_SYSTEM . "config/";
        $target_file = $target_dir . basename($file["name"]);
        $filetype = pathinfo($target_file,PATHINFO_EXTENSION);
        $error = false;

        $filesize = $file["size"];
        if($filesize <= 2097152){
            if($filetype == "pem"){
                move_uploaded_file($file["tmp_name"], $target_file);
                rename($target_file,$this->rename_file($target_file));
            }else{
                $error = $this->language->get('error_filetype');
            }
        }else{
            $error = $this->language->get('error_filesize');
        }
        return $error;
    }
    private function rename_file($name){
        $CHARMAP = array(
            'ö' => 'o',
            'Ö' => 'O',
            'ó' => 'o',
            'Ó' => 'O',
            'ő' => 'o',
            'Ő' => 'O',
            'ú' => 'u',
            'Ú' => 'U',
            'ű' => 'u',
            'Ű' => 'U',
            'ü' => 'u',
            'Ü' => 'U',
            'á' => 'a',
            'Á' => 'A',
            'é' => 'e',
            'É' => 'E',
            'í' => 'i',
            'Í' => 'I',
            ' ' => '_'
        );

        $name = strtr($name, $CHARMAP);
        return $name;
    }
}
?>