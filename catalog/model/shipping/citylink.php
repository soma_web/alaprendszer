<?php
class ModelShippingCitylink extends Model {
    function getQuote($address, $fizetesek="") {
		$this->load->language('shipping/citylink');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('citylink_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if (!$this->config->get('citylink_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();
	
		if ($status) {
            $fizetesi_mod = array();

            if ($fizetesek){
                foreach($fizetesek as $value) {
                    if ( $this->config->get("citylink_payment_".$value['code']) &&  $this->config->get("citylink_payment_".$value['code']) == 1){
                        $fizetesi_mod[] = array(
                            'fizetesi_mod' => $value['code']
                        );
                    }
                }
            }

			$cost = 0;
			$weight = $this->cart->getWeight();
			
			$rates = explode(',', $this->config->get('citylink_rate'));
			
			foreach ($rates as $rate) {
  				$data = explode(':', $rate);
  					
				if ($data[0] >= $weight) {
					if (isset($data[1])) {
    					$cost = $data[1];
					}
					
   					break;
  				}
			}
			
			$quote_data = array();
            if ($this->config->get('citylink_tax_class_id') > 0) {
                $query_tax = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$this->config->get('flat_tax_class_id') . "'");
            }
            $ado_title = isset($query_tax->row['title']) ? $query_tax->row['title'] :"";

			if ((float)$cost) {
				$quote_data['citylink'] = array(
        			'code'         => 'citylink.citylink',
                    'title'        => $this->config->get('citylink_text') != null ? $this->config->get('citylink_text') : $this->language->get('text_description') . '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')',
        			'cost'         => $cost,
                    'fizetesi_mod'  => $fizetesi_mod,
        			'tax_class_id' => $this->config->get('citylink_tax_class_id'),
					'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('citylink_tax_class_id'), $this->config->get('config_tax'))),
                    'netto_text'   => $this->currency->format($cost),
                    'ado_title'    => $ado_title,
					'mail'         => $this->config->get('citylink_mail')

				);
				
      			$method_data = array(
        			'code'       => 'citylink',
        			'title'      => $this->config->get('citylink_header') != null ? $this->config->get('citylink_header') : $this->language->get('text_description'),
        			'quote'      => $quote_data,
                    'fizetesi_mod'  => $fizetesi_mod,
                    'sort_order' => $this->config->get('citylink_sort_order'),
        			'error'      => false
      			);
			}
		}
	
		return $method_data;
	}
}
?>