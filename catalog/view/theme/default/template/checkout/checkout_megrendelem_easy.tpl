<?php echo $header; ?>

<div id="content" style="margin-top: 20px;">
    <h1><?php echo $heading_title; ?></h1>

    <input type="hidden" name="fizetos" value="<?php echo $fizetos;?>" >

    <div id="payment-address">
        <div class="checkout-heading"><span class="checkout-label" ><?php echo $text_checkout_payment_address; ?></span></div>
        <div class="checkout-content"></div>
    </div>



<?php if ($shipping_required) { ?>

    <div id="shipping-method">
        <div class="checkout-heading"><span class="checkout-label" ><?php echo $text_checkout_shipping_method; ?></span></div>
        <div class="checkout-content"></div>
    </div>

<?php } ?>



<?php if ($fizetos || (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) ) {?>

    <?php if ($this->config->get('megjelenit_penztar_fizetesi_mod') == 1) {?>
        <div id="payment-method">
            <div class="checkout-heading"><span class="checkout-label" ><?php echo $text_checkout_payment_method; ?></span></div>
            <div class="checkout-content"></div>
        </div>
    <? }?>
    <div id="confirm">
        <div class="checkout-heading"><span class="checkout-label" ><?php echo $text_checkout_confirm; ?></span></div>
        <div class="checkout-content"></div>
    </div>
    </div>
    <?php echo $content_bottom; ?></div>
<?php } ?>

<script type="text/javascript"><!--
    $("body").css("background-image","none");
    $("#header_hatternek").css("display","none");

    megrendelem_folyamat = true;

$(document).ready(function() {

    $.ajax({
        url: 'index.php?route=checkout/guest_megrendelem',
        dataType: 'html',

        success: function(html) {
            $('#payment-address .checkout-content').html(html);
            $('#payment-address .checkout-content').slideDown('slow');

            <?php if ($shipping_required) { ?>
            $.ajax({
                url: 'index.php?route=checkout/shipping_method_megrendelem',
                dataType: 'html',
                success: function(html) {
                    $('#shipping-method .checkout-content').html(html);
                    $('#shipping-method .checkout-content').slideDown('slow');

                    $.ajax({
                        url: 'index.php?route=checkout/payment_method_megrendelem',
                        dataType: 'html',
                        success: function(html) {
                            $('#payment-method .checkout-content').html(html);
                            $('#payment-method .checkout-content').slideDown('slow');

                            $.ajax({
                                url: 'index.php?route=checkout/confirm_megrendelem',
                                dataType: 'html',
                                success: function(html) {
                                    $('#confirm .checkout-content').html(html);

                                    $('#confirm .checkout-content').slideDown('slow');

                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });

                },
                error: function(xhr, ajaxOptions, thrownError) {

                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
            <?php } ?>



        },
        error: function(xhr, ajaxOptions, thrownError) {


            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});









    $('input[name=shipping_method]').on('click', function() {

            $.ajax({
                url: 'index.php?route=checkout/shipping_method_megrendelem/validate',
                type: 'post',
                data: $('#shipping-method input[type=\'radio\']:checked, #shipping-method textarea'),
                dataType: 'json',

                success: function(json) {

                    $('.warning').remove();

                    $.ajax({
                        url: 'index.php?route=checkout/payment_method_megrendelem',
                        dataType: 'html',
                        success: function(html) {
                            $('#payment-method .checkout-content').html(html);
                            $('#payment-method .checkout-content').slideDown('slow');

                            $.ajax({
                                url: 'index.php?route=checkout/payment_method_megrendelem',
                                dataType: 'html',
                                success: function(html) {
                                    $('#payment-method .checkout-content').html(html);
                                    $('#payment-method .checkout-content').slideDown('slow');

                                    $.ajax({
                                        url: 'index.php?route=checkout/confirm_megrendelem',
                                        dataType: 'html',
                                        success: function(html) {
                                            $('#confirm .checkout-content').html(html);

                                            $('#confirm .checkout-content').slideDown('slow');

                                        },
                                        error: function(xhr, ajaxOptions, thrownError) {
                                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                        }
                                    });
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });

                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            debugger;
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });



                },
                error: function(xhr, ajaxOptions, thrownError) {
                    debugger;

                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });



    });

    $('input[name=payment_method]').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/payment_method_megrendelem/validate',
            type: 'post',
            data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea'),
            dataType: 'json',

            success: function(json) {


                if (json['error']) {
                    if (json['error']['warning']) {
                        if (json['error']['payment']) {
                            $('#payment-method .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
                        } else {
                            $('#payment-method .checkout-content .buttons').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
                        }

                        $('.warning').fadeIn('slow');
                    }
                } else  {
                    $.ajax({
                        url: 'index.php?route=checkout/confirm_megrendelem',
                        dataType: 'html',
                        success: function(html) {
                            $('#confirm .checkout-content').html(html);

                            $('#confirm .checkout-content').slideDown('slow');

                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });



    //--></script>
<?php //echo $footer; ?>