<?php
class ControllerModuleIconDoboz extends Controller {
    public function index() {
        $this->data['categories'] = array();

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        if (isset($this->request->request['filter_tulajdonsagok'])) {
            //$this->tulajdonsag($this->request->request['filter_tulajdonsagok']);
            $this->teljesKatagoriak();

        /*} elseif (isset($this->request->request['route']) && $this->request->request['route'] == "module/lastseen") {
        } elseif (isset($this->request->request['route']) && $this->request->request['route'] == "module/special_header") {*/

        } else {
            $this->teljesKatagoriak();
        }

    }

    public function teljesKatagoriak() {
        $categories = $this->model_catalog_category->getCategories(0);
        $this->katagoriak($categories);
    }

    public function katagoriak($categories,$attribute_id=0) {
        $this->data['categories'] = array();

        foreach ($categories as $category) {
            if ($category['top']) {
                $children_data = array();

                if ($attribute_id)
                    $children = $this->model_catalog_category->getCategories($category['category_id'],true,$attribute_id);
                else
                    $children = $this->model_catalog_category->getCategories($category['category_id']);

                $megjelenit_category_header_null = $this->config->get('megjelenit_category_header_null');

                foreach ($children as $child) {
                    $data = array(
                        'filter_category_id'  => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $product_total = $this->model_catalog_product->getTotalProducts($data);

                    $ha_piacter=$this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']. ($attribute_id && false ? '&filter_tulajdonsagok='.$attribute_id : '') );

                    if ($child['piacter'] == 1){
                        $ha_piacter.="&piacter=piacter";
                    }

                    if (isset($megjelenit_category_header_null) && $megjelenit_category_header_null == 1) {
                        $children_data[] = array(
                            'name'  => $child['name'] . ' (' . $product_total . ')',
                            'href'  => $ha_piacter
                        );
                    } else {
                        if($product_total > 0 ) {
                            $children_data[] = array(
                                'name'  => $child['name'] . ' (' . $product_total . ')',
                                'href'  => $ha_piacter
                            );
                        }
                    }
                }

                // Level 1
                $ha_piacter=$this->url->link('product/category', 'path=' . $category['category_id']);
                //$ha_piacter=$this->url->link('product/multifilter', 'path=' . $category['category_id']);
                if ($category['piacter'] == 1){
                    $ha_piacter .="&piacter=piacter";
                }

                if (isset($this->request->request['filter_tulajdonsagok'])) {
                    //$ha_piacter .="&filter_tulajdonsagok=".$this->request->request['filter_tulajdonsagok'];

                }

                $this->load->model('tool/image');
                if ($category['image_icon']) {
                    $image_icon = $this->model_tool_image->resize($category['image_icon'], $this->config->get('config_image_category_icon_width'), $this->config->get('config_image_category_icon_height'),false);
                } else {
                    $image_icon =  $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_category_icon_width'), $this->config->get('config_image_category_icon_height'),false);
                }

                $this->data['categories'][] = array(
                    'name'       => $category['name'],
                    'image_icon' => $image_icon,
                    'children'   => $children_data,
                    'column'     => $category['column'] ? $category['column'] : 1,
                    'href'       => $ha_piacter
                );
            }
        }

        $this->data['icon_width'] = $this->config->get('config_image_category_icon_width');


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icon_doboz.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/icon_doboz.tpl';
        } else {
            $this->template = 'default/template/module/icon_doboz.tpl';
        }

        $this->response->setOutput($this->render());
    }

    public function tulajdonsag($attribute_id) {
        $this->load->model('catalog/category');

        //$attribute_id = $this->request->request['attribute_id'];
        $categories = $this->model_catalog_category->getCategories(0,true,$attribute_id);

        $this->katagoriak($categories,$attribute_id);


    }

}
?>