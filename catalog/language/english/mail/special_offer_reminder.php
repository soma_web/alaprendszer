<?php
// Text
$_['text_subject']      = 'Special Offer Reminder';
$_['text_hi']           = 'Hi %s,';
$_['text_info']         = 'Please note that special offers from list below expire soon:';
$_['text_offer_info']   = '<a href="%s">%s</a> - %s expire on:<strong>%s</strong>';
$_['text_thanks']       = 'Thanks,';
?>