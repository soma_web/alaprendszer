<?php
// Text
$_['text_new_subject']          = '%s - Rendelés %s';
$_['text_new_greeting']         = '';
$_['text_new_received']         = 'Új megrendelése érkezett.';
$_['text_new_link']             = 'A megrendelés megtekintéséhez kattintson a linkre:';
$_['text_new_order_detail']     = 'Megrendelés részletei';
$_['text_new_instruction']      = 'Utasítások';
$_['text_new_order_id']         = 'Rendelésszám:';
$_['text_new_date_added']       = 'Rendelés dátuma:';
$_['text_new_order_status']     = 'Rendelés állapota:';
$_['text_new_payment_method']   = 'Fizetési mód:';
$_['text_new_shipping_method']  = 'Szállítási mód:';
$_['text_shipping_warehouse']   = 'Átvétel helye:';
$_['text_shipping_gls_pont']    = 'Választott GLS Csomagpont:';
$_['text_shipping_gls_pont_shopid']    = 'A választott GLS Csomagpont azonosító:';
$_['text_megjegyzes']           = 'Megjegyzés:';
$_['text_new_email']  			= 'Email:';
$_['text_new_telephone']  		= 'Telefon:';
$_['text_new_ip']  				= 'IP cím:';
$_['text_order']  				= 'Rendelés:';
$_['text_new_payment_address']  = 'Számlázási cím';
$_['text_new_shipping_address'] = 'Szállítási cím';
$_['text_new_products']         = 'Termékek';
$_['text_new_product']          = 'Termék';
$_['text_new_model']            = 'Modell';
$_['text_new_quantity']         = 'Mennyiség';
$_['text_new_price']            = 'Ár';
$_['text_new_order_total']      = 'Rendelés mindösszesen';
$_['text_new_total']            = 'Összesen';
$_['text_new_download']         = 'Miután a fizetés megerősítést nyert, kattintson az alábbi linkre, hogy hozzáférjen a letölthető termékekhez:';
$_['text_new_comment']          = 'A rendelése megjegyzései:';
$_['text_new_footer']           = 'Amennyiben kérdése merülne fel, kérem válaszoljon erre az e-mailre!
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"></br></br>
	<span id="docs-internal-guid-61851cfb-ff78-995f-545f-50d12cadb719"><span style="font-size: 15px; font-family: Arial; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;">Üdvözlettel: </span></span></p>
<p>
	<span id="docs-internal-guid-61851cfb-ff78-995f-545f-50d12cadb719">
	    <span style="font-size: 15px; font-family: Arial; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;">EU-SOLAR Kft.<br>Adószám: 24132608-2-02<br>Cégjegyzékszám: 02-09-079435<br>Székhely: 7635 Pécs, Abaligeti út 14.<br><br>
Ügyfélszolgálat:<br>
ertekesites@eu-solar.hu<br>
+36-70/907-0820
	    </span>
	</span>
	 </p>';
$_['text_new_powered']          = '';
$_['text_update_subject']       = '%s - Rendelés frissítése %s';
$_['text_update_order']         = 'Rendelésszám:';
$_['text_update_date_added']    = 'Rendelés dátuma:';
$_['text_update_order_status']  = 'A rendelésének állapota az alábbira módosult:';
$_['text_update_comment']       = 'A megrendelésének megjegyzései:';
$_['text_update_link']          = 'A megrendelés megtekintéséhez kattintson a linkre:';
$_['text_update_footer']        = 'Amennyiben kérdése merülne fel, kérem válaszoljon erre az e-mailre!';

$_['column_netto']                  = '<span>Egység ár </span></br><span style="font-size: 10px; ">(Nettó)</span>';
$_['column_netto_total_price']      = '<span>Összesen ár</span></br><span style="font-size: 10px; "> (Nettó)</span>';
$_['column_price_netto_ar']         = '<span>Egység ár</span></br><span style="font-size: 10px; "> (Bruttó)</span>';
$_['column_total_netto_ar']         = '<span>Összesen ár</span></br><span style="font-size: 10px; "> (Bruttó)</span>';
$_['termek_ar_osszes']              = '<span>Összesen</span>';
$_['szallitas']                     = '<span>Szállítás</span>';
$_['szallitas_netto_ar']            = '<span>Ár (Nettó)</span>';
$_['szallitas_brutto_ar']           = '<span>Ár (Bruttó)</span>';

$_['netto_osszes']                     = '<span>Nettó összesen</span>';
$_['afa_osszes']            = '<span>Áfa összesen</span>';
$_['fizetendo_osszes']           = '<span>Fizetendő összesen</span>';
?>