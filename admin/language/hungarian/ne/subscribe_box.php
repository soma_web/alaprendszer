<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Feliratkozó dobozok';

// Entry
$_['entry_name']							= 'Név:';
$_['entry_heading']							= 'Címsor:';
$_['entry_text']							= 'Szöveg:';
$_['entry_status']							= 'Állapot:';
$_['entry_store']							= 'Áruház:';
$_['entry_subscribe_box']					= 'Feliratkozó doboz:';
$_['entry_layout']							= 'Elrendezés:';
$_['entry_position']						= 'Pozíció:';
$_['entry_sort_order']						= 'Rendezési Sorrend:';
$_['entry_show_for']						= 'Mutat:';
$_['entry_fields']							= 'Mezők:';
$_['entry_type']							= 'Típus:';
$_['entry_modal_timeout']					= 'Modális felugró ablak időkorlát:<br/><span class="help">Idő másodpercben mielőtt a felugró ablak bezárul. Ha nem meghatározott vagy nulla, a bezárás gomb hozzáadja.</span>';
$_['entry_modal_repeat_time']				= 'Ismétlés ideje:<br/><span class="help">Idő órában, amely után a modális felugró ablak fog újra megjelenni annak az ügyfélnek, aki meglátogatja az oldalt. Ha nincs megadva, vagy nulla, modális felugró ablak jelenik meg minden oldal betöltésekor. Modális felugró ablak nem jelenik meg azon ügyfelek számára, akik benyújtották az adatokat.</span>';
$_['entry_modal_bg_color']					= 'Háttér szín:';
$_['entry_modal_heading_color']				= 'Címsor szövegének színe:';
$_['entry_modal_line_color']				= 'Elválasztó vonalak színe:';
$_['entry_list_type']						= 'Lista típusa:';

// Text 
$_['text_new_subscribe_box']				= 'Új Feliratkozó Doboz';
$_['text_enabled']							= 'Engedélyezve';
$_['text_disabled']							= 'Letiltva';
$_['text_all']								= 'Összes';
$_['text_guests']							= 'Vendégek';
$_['text_only_email']						= 'Csak Email';
$_['text_email_name']						= 'Email és Név';
$_['text_email_full']						= 'Email, Vezetéknév és Keresztnév';
$_['text_success']							= 'Siker: A kiválasztott elemek törölve!';
$_['text_success_save']						= 'Feliratkozó doboz sikeresen elmentve!';
$_['text_success_copy']						= 'Feliratkozó doboz sikeresen másolva!';
$_['text_settings']							= 'Általános feliratkozó doboz beállításai';
$_['text_new_template']						= 'Új feliratkozó doboz';
$_['text_content_box']						= 'Tartalom doboz';
$_['text_modal_popup']						= 'Modális felugró ablak';
$_['text_content_box_to_modal']				= 'Tartalom doboz gombbal a modális felugró ablak megnyitásához';
$_['text_modal_popup_settings']				= 'Modális felugró ablak beállításai';
$_['text_modal_popup_style']				= 'Modális felugró ablak stílusa';
$_['text_content_top']						= 'Tartalom teteje';
$_['text_content_bottom']					= 'Tartalom alja';
$_['text_column_left']						= 'Bal oszlop';
$_['text_column_right']						= 'Jobb oszlop';
$_['text_default']							= 'Alapértelmezett';
$_['text_checkboxes']						= 'Jelölőnégyzetek';
$_['text_radio_buttons']					= 'Rádió gombok';

// Button 
$_['button_edit']							= 'Szerkesztés';

// Column 
$_['column_actions']						= 'Műveletek';
$_['column_name']							= 'Név';
$_['column_status']							= 'Állapot';
$_['column_last_change']					= 'Legutóbbi változás';

// Error
$_['error_copy']							= 'Hiba a feliratkozó doboz másolása közben.';
$_['error_name']							= 'Név megadása kötelező';

?>
