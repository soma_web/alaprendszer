<?php if( (isset($filter_csoportok) && $filter_csoportok) || $beallitasok['szuro_csoport']['teljes'] == 1){ ?>

        <div class="box">
            <div class="box-heading reszek masodik-filter"><?php echo $heading_title_filter_group; ?>
                <img style="display: none"  src="catalog/view/theme/default/image/responsive/pos_r_opened.png">
            </div>
            <?php if ($beallitasok['szuro_csoport']['teljes'] != 1) { ?>
                <div id="mf-bevaltashelye" class="box-content">
                    <ul class="box-filter box-filter-varos">
                    <?php foreach ($filter_csoportok as $filter_csoport) { ?>
                        <?php if (in_array($filter_csoport['filter_group_id'], $filter_varos)) { ?>
                            <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_csoport['filter_group_id']; ?>" id="filter_group<?php echo $filter_csoport['filter_group_id']; ?>"  checked="checked"/>
                                <label class="css-label mac-style" for="filter_group<?php echo $filter_csoport['filter_group_id']; ?>"><?php echo $filter_csoport['name']; ?></label>
                            </li>
                        <?php } else {?>
                            <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_csoport['filter_group_id']; ?>" id="filter_group<?php echo $filter_csoport['filter_group_id']; ?>" />
                                <label class="css-label mac-style" for="filter_group<?php echo $filter_csoport['filter_group_id']; ?>"><?php echo $filter_csoport['name']; ?></label>
                            </li>
                        <?php } ?>
                    <?php } ?>
                    </ul>
                </div>
            <?php } else {?>

                <div id="mf-bevaltashelye" class="box-content">
                    <ul class="box-filter box-filter-varos">
                        <?php foreach ($filter_csoportok_teljes as $filter_csoport_teljes) { ?>
                            <?php if (in_array($filter_csoport_teljes, $filter_csoportok)) { ?>
                                <?php if (in_array($filter_csoport_teljes['filter_group_id'], $filter_varos)) { ?>
                                    <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_csoport_teljes['filter_group_id']; ?>" id="filter_group<?php echo $filter_csoport_teljes['filter_group_id']; ?>"  checked="checked"/>
                                        <label class="css-label mac-style" for="filter_group<?php echo $filter_csoport_teljes['filter_group_id']; ?>"><?php echo $filter_csoport_teljes['name']; ?></label>
                                    </li>
                                <?php } else {?>
                                    <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_csoport_teljes['filter_group_id']; ?>" id="filter_group<?php echo $filter_csoport_teljes['filter_group_id']; ?>" />
                                        <label class="css-label mac-style" for="filter_group<?php echo $filter_csoport_teljes['filter_group_id']; ?>"><?php echo $filter_csoport_teljes['name']; ?></label>
                                    </li>
                                <?php } ?>
                            <?php } else {?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_csoport_teljes['filter_group_id']; ?>" id="filter_group<?php echo $filter_csoport_teljes['filter_group_id']; ?>" disabled="disabled"/>
                                    <label class="css-label mac-style halvany" for="filter_group<?php echo $filter_csoport_teljes['filter_group_id']; ?>" ><?php echo $filter_csoport_teljes['name']; ?></label>
                                </li>
                            <?php } ?>

                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>

        </div>
    <?php }?>
