<?php

// Heading
$_['heading_title']    = 'Termékek leválogatása a menusorba X.';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Sikeresen módosította a kiemelt modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Taratalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_ures']          = 'Nincs animáció';
$_['text_mcs5']          = 'Görgető animáció';
$_['text_korhinta']      = 'Körhinta animáció';

// Entry
$_['entry_product']    = 'Termékek:<br /><span class="help">(Autocomplete)</span>';
$_['entry_image']      = 'Kép (Sz × M):';
$_['entry_layout']     = 'Elrendezés:';
$_['entry_position']   = 'Pozíció:';
$_['entry_status']     = 'Állapot:';
$_['entry_sort_order'] = 'Sorrend:';
$_['entry_show']       = 'Show animáció:';
$_['entry_menusorban'] = 'Menusorban megjelenik:';
$_['entry_menusor_neve'] = 'Menüpont neve:';


// Error
$_['error_permission'] = 'Figyelmeztetés: A kiemelt modul módosítása az Ön számára nem engedélyezett!';
$_['error_image']      = 'Meg kell adni a kép magasságát és szélességét!';
?>
