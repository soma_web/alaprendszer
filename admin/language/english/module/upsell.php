<?php

// Heading
$_['heading_title']    = 'Buy incentive (Upsell) module';

// Text
$_['text_module']                       = 'Modules';
$_['text_success']                      = 'Success: You have modified module Buy incentive Module!';
$_['text_szinten_vasarolt']             = 'Also purchased products';
$_['text_kapcsolodo']                   = 'Kapcsolódó termékek';
$_['text_tartozekok']                   = 'Related products';
$_['text_kiemelt']                      = 'Featured products';
$_['text_bestseller']                   = 'Bestseller products';
$_['text_latest']                       = 'Latest products';
$_['text_auto']                         = 'Automatic';
$_['text_kosar_nem_latszik']            = 'Nothing seems in cart:';
$_['text_kosarba_tett_termek_latszik']  = 'Actual product seems in cart:';
$_['text_kosar_tartalma_latszik']       = 'Full seems cart:';

// Entry
$_['entry_status']              = 'Status:';
$_['entry_fejlec_latszik']      = 'Display Header:';
$_['entry_megfelelo_termekek']  = 'Select product group:';
$_['entry_automata_bellitas']   = 'Automatic selection order:';
$_['entry_max_termek']          = 'Up to display product:';
$_['entry_design_beallitas']    = 'Design options:';
$_['entry_bought_relacio']      = 'Relational signal:';
$_['entry_bought_order_status'] = 'Order status:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Buy incentive Module!';

?>
