<?php
// Heading
$_['heading_title']    = 'TG Helios Kosár modul';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Sikerült: A modul módosítása megtörtént!';
$_['text_content_top']    = 'Oldal tetején';
$_['text_content_bottom'] = 'Oldal alján';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

// Entry
$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']   = 'Pozíció:';
$_['entry_status']     = 'Státusz:';
$_['entry_sort_order'] = 'Sorrend:';

// Error
$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága módosítani a kosár modul módosítására!';
?>