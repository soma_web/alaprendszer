<?php
class ControllerModulefbjsconnect extends Controller {
	protected function index($setting) {

		$this->language->load('module/fbjsconnect'); 
		$this->data['heading_title'] = $this->language->get('heading_title');
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = $this->config->get('config_ssl');
		} else {
			$this->data['base'] = $this->config->get('config_url');
		}
		$this->data['facebook_login'] = $this->language->get('facebook_login');
		$this->data['logging_text'] = $this->language->get('logging_text');
		$this->data['text_fbconnect'] = $this->language->get('text_fbconnect');
		$this->data['canclled_text'] = $this->language->get('canclled_text');
		if(!$this->customer->isLogged()){
			
			$this->data['fbjsconnect']['appid'] = $this->config->get('fbjsconnect_apikey');
			$this->data['fbjsconnect']['secret'] = $this->config->get('fbjsconnect_apisecret');
			$this->data['fbjsconnect']['scope'] = 'email,user_birthday,user_location,user_hometown';
			$this->data['fbjsconnect']['redirect_uri'] = $this->url->link('account/fbjsconnect', '', 'SSL');

			if(!isset($this->fbjsconnect)){			
				require_once(DIR_SYSTEM . 'vendor/facebook-sdk/facebook.php');
				$this->fbjsconnect = new Facebook(array(
					'appId'  => $this->data['fbjsconnect']['appid'],
					'secret' => $this->data['fbjsconnect']['secret'],
				));
			}
			
			$this->data['fbjsconnect_url'] = $this->fbjsconnect->getLoginUrl(
				array(
					'scope' => $this->data['fbjsconnect']['scope'],
					'redirect_uri'  => $this->data['fbjsconnect']['redirect_uri']
				)
			);

			if($this->config->get('fbjsconnect_button_' . $this->config->get('config_language_id'))){
				$this->data['fbjsconnect_button'] = html_entity_decode($this->config->get('fbjsconnect_button_' . $this->config->get('config_language_id')));
			}
			else {
                $this->data['fbjsconnect_button'] = "proba";//$this->language->get('heading_title');
            }

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/fbjsconnect.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/module/fbjsconnect.tpl';
			} else {
				$this->template = 'default/template/module/fbjsconnect.tpl';
			}

			$this->render();
		}				

	}
}
?>