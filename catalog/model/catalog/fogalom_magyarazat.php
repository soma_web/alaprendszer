<?php
class ModelCatalogFogalomMagyarazat extends Model {

    public function getFogalomMagyarazatok() {
            $sql = "SELECT fmd.name, fmd.description FROM " . DB_PREFIX . "fogalom_magyarazat fm
			 	LEFT JOIN " . DB_PREFIX . "fogalom_magyarazat_description fmd ON (fm.fogalom_magyarazat_id = fmd.fogalom_magyarazat_id)
			 	WHERE fmd.language_id = '" . (int)$this->config->get('config_language_id') . "'
			 	AND fm.status=1
			 	AND fmd.description > ''
			 	AND fmd.name > ''
			 	";

            $query = $this->db->query($sql);

            return $query->rows;

    }

    public function getFogalomMagyarazat($fogalom_magyarazat_id) {
        $sql = "SELECT fmd.description, fmd.name FROM " . DB_PREFIX . "fogalom_magyarazat fm
			 	LEFT JOIN " . DB_PREFIX . "fogalom_magyarazat_description fmd ON (fm.fogalom_magyarazat_id = fmd.fogalom_magyarazat_id)
			 	WHERE fmd.language_id = '" . (int)$this->config->get('config_language_id') . "'
			 	AND fm.fogalom_magyarazat_id = '" . (int)$fogalom_magyarazat_id . "'";

        $query = $this->db->query($sql);

        return $query->row;

    }
}
?>