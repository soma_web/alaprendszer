<?php  
class ControllerCommonHome extends Controller {
	public function index() {
        $this->log->setMicroTime();

        $this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));

		$this->data['heading_title'] = $this->config->get('config_title');


        $this->load->model('setting/store');
        $stores = $this->model_setting_store->getStores();

		if (isset($this->request->request['sessionba']) && $stores )  {
			if ($this->request->request['sessionba'] == 'valasztott') {
				$this->session->data['sessionba'] = 'valasztott';
            } else {
                $this->redirect($this->url->link('common/store_kulso','','SSL'));
            }

		} elseif (empty($this->session->data['sessionba'])) {
            if ($stores) {
                $this->redirect($this->url->link('common/store_kulso','','SSL'));
            }

        }


        $this->load->model('module/multifilter');
        $this->load->language('module/multifilter');

        $this->data['text_cikkszam'] = $this->language->get('text_cikkszam');
        $this->model_module_multifilter->sessionUrit();

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/home.tpl';
		} else {
			$this->template = 'default/template/common/home.tpl';
		}
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner_image ");
        $this->data['banner_kepek']=$query->rows;

        if ( $this->config->get('megjelenit_slideshow') == 1){
            $this->data['teljes_szelesseg'] = true;;
        } else {
            $this->data['teljes_szelesseg'] = false;;

        }
        $this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

        $this->log->GetMicroTime('Action - common/home');

        $this->response->setOutput($this->render());
	}

    function arUjraszamol() {
        if (isset($this->request->request['ar_atszamol']) &&  $this->request->request['ar_atszamol'] > 0) {

            $this->session->data['ar_atszamol'] = $this->request->request['ar_atszamol'];

        } elseif (isset($this->session->data['ar_atszamol'])) {
            unset($this->session->data['ar_atszamol']);
        }
    }
}
?>