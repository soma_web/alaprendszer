<?php
    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/close_upsell.png')) {
        $close = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/close_upsell.png';
    } else {
        $close = DIR_TEMPLATE_IMAGE.'default/image/close_upsell.png';
}
?>

<div class="close_upsell" >
    <img class="close_upsell_icon" title="<? if (isset($text_close_upsell)) { echo $text_close_upsell; }?>" alt="<? if (isset($text_close_upsell)) { echo $text_close_upsell; }?>"  src="<? echo $close?>" >
</div>

<script>
    $(".close_upsell").bind("click",function(){
        $('#upsell').fadeOut(500);
    });
</script>