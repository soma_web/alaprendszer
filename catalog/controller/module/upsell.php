<?php
class ControllerModuleUpsell extends Controller {

    private $upsell;
    private $key = 0;
    private $products = array();

    public function index() {
        $time_pre = microtime(true);

        $this->upsell = $this->config->get('upsell');

        if (  isset($this->upsell['status']) && $this->upsell['status'] == 1 ) {

            $this->language->load('module/upsell',$this->config->get('config_template'));

            $this->load->model('module/upsell');
            $this->load->model('catalog/product');

            $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
            $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
            $this->data['button_compare']   = $this->language->get('button_compare');
            $this->data['button_cart']          = $this->language->get('button_cart');
            $this->data['button_set_valos_ar']          = $this->language->get('button_set_valos_ar');
            $this->data['text_akcio_title']     = $this->language->get('text_akcio_title');
            $this->data['text_uj_title']        = $this->language->get('text_uj_title');
            $this->data['text_compare']         = $this->language->get('text_compare');
            $this->data['text_raktaron']        = $this->language->get('text_raktaron');
            $this->data['text_nincs_raktaron']  = $this->language->get('text_nincs_raktaron');
            $this->data['text_egypar']          = $this->language->get('text_egypar');
            $this->data['text_close_upsell']    = $this->language->get('text_close_upsell');
            $this->data['text_cikkszam']    = $this->language->get('text_cikkszam');

            asort($this->upsell['csoport_sorrend']);

            $this->key = $this->upsell['termekek'];

            if ($this->upsell['termekek'] == 0 ) {  // letiltva
            } elseif ($this->upsell['termekek'] == 1 ) {  // auto
                $this->key = 1;
                $this->termekKiegeszit();
            } else {
                $name = "termek_".$this->key;
                $results = $this->$name();
                if ($results) {
                    foreach ($results as $key=>$result) {
                        $this->products[$key] = $key;
                        if (count($this->products) >= $this->upsell['limit']) {
                            break;
                        }
                    }
                }
                if (count($this->products) < $this->upsell['limit']) {
                    $this->termekKiegeszit();
                }
            }


      	    $this->data['heading_title'] = $this->language->get('heading_title');

            $this->data['ajanlott_termekek'] = array();
            foreach ($this->products as $product) {
                $termek = $this->model_catalog_product->getProduct($product);
                $this->data['ajanlott_termekek'][] = $this->product->productPreparation($termek);
            }

            $termek = $this->model_catalog_product->getProduct($this->request->request['product_id']);

      	    $this->data['product']  = $this->product->productPreparation($termek);


      	    $this->data['kosar_netto']    = $this->currency->format(round($this->cart->getSubTotal()));
      	    $this->data['kosar_brutto']   = $this->currency->format(round($this->cart->getTotal()));
      	    $this->data['kosar_darab']    = $this->cart->countProducts();
      	    $this->data['kosar_shipping'] = $this->cart->getProductShipping();
      	    $this->data['module_name']    = "upsell";

		    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/upsell.tpl')) {
			    $this->template = $this->config->get('config_template') . '/template/module/upsell.tpl';
            } else {
                $this->template = 'default/template/module/upsell.tpl';
            }

            $this->response->setOutput($this->render());
        }
        $time_post = microtime(true);

        $exec_time = $time_post - $time_pre;
        $this->log->write('upsell idö: '.$exec_time);
    }

    private function termek_2() {
        $data = array(
            'filter_product_id'    => $this->request->request['product_id'],
            'order_status_operand' => htmlspecialchars_decode($this->upsell['also_bought']),
            'order_status_id'      => $this->upsell['order_status_id'],
            'start' 			   => 0,
            'limit' 			   => $this->upsell['limit']
        );

        $products = $this->model_module_upsell->getAlsoBoughtProducts($data);
        if ($products) {
            foreach($products as $key=>$value) {
                $vissza[$value] = $value;
            }
        } else {
            $vissza = false;
        }

        return $vissza;
    }

    private function termek_3() {
        $products =  $this->model_catalog_product->getProductRelated($this->request->request['product_id']);

        if ($products) {
            foreach($products as $key=>$value) {
                $vissza[$key] = $key;
            }
        } else {
            $vissza = false;
        }

        return $vissza;
    }

    private function termek_4() {
        $products = $this->model_catalog_product->getProductAccessory($this->request->request['product_id']);

        if ($products) {
            foreach($products as $key=>$value) {
                $vissza[$key] = $key;
            }
        } else {
            $vissza = false;
        }

        return $vissza;
    }

    private function termek_5() {
        $products = $this->model_module_upsell->getFeaturedProducts($this->config->get('featured_product'));


        if ($products) {
            foreach($products as $key=>$value) {
                $vissza[$value] = $value;
            }
        } else {
            $vissza = false;
        }

        return $vissza;
    }

    private function termek_6() {
        $products = $this->model_catalog_product->getBestSellerProducts($this->upsell['limit']);

        if ($products) {
            foreach($products as $key=>$value) {
                $vissza[$key] = $key;
            }
        } else {
            $vissza = false;
        }

        return $vissza;
    }

    private function termek_7() {
        $data = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $this->upsell['limit']
        );
        $products = $this->model_catalog_product->getProducts($data);
        if ($products) {
            foreach($products as $key=>$value) {
                $vissza[$key] = $key;
            }
        } else {
            $vissza = false;
        }

        return $vissza;
    }

    private function termekKiegeszit() {

        foreach($this->upsell['csoport_sorrend'] as $key=>$value) {
            if ($key != $this->key) {
                $name = "termek_".$key;
                $results = $this->$name();
                if ($results) {
                    foreach ($results as $key=>$result) {
                        $this->products[$key] = $key;
                        if (count($this->products) >= $this->upsell['limit']) {
                            break;
                        }
                    }
                }
            }
            if (count($this->products) >= $this->upsell['limit']) {
                break;
            }
        }

    }
}
?>