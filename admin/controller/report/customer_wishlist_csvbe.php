<?php
class ControllerReportCustomerWishlistCsvbe extends Controller {
	public function index() {     
		$this->load->language('report/customer_wishlist');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
				
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
						
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/customer_wishlist', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);

        $this->load->model('report/wishlist');
        $this->load->model('catalog/product');

		$this->data['products'] = array();
		
		$data = array(
			'filter_date_start'	=> $filter_date_start, 
			'filter_date_end'	=> $filter_date_end, 
			'start'             => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'             => $this->config->get('config_admin_limit')
		);
				
		$customer_total = $this->model_report_wishlist->getTotalWishlists($data);

		$results = $this->model_report_wishlist->getwishlists($data);

        foreach ($results as $product_id=>$result) {
            $product = $this->model_catalog_product->getProduct($product_id);


			$action = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('sale/customer/update', 'token=' . $this->session->data['token'] . '&product_id=' . $product_id . $url, 'SSL')
			);

            $customer = array();
            foreach($result as $value) {
                $customer[] = array(
                    'customer'       => $value['customer'],
				    'email'          => $value['email'],
				    'customer_group' => $value['customer_group']
                );
            }
			if (isset($product_id) && isset($product['name']) && isset($product['model']) && isset($product['price']) && isset($product['quantity']) ) {
				$this->data['products'][] = array(
					'product_name' => $product['name'],
					'product_model' => $product['model'],
					'product_price' => $this->currency->format($product['price']),
					'product_quantity' => $product['quantity'],
					'total_customer' => count($result),
				);
			}
		}

        $this->data['products'] = $this->config->rendezes($this->data['products'],'total_customer','DESC');

		 
 		$this->data['heading_title'] = $this->language->get('heading_title');
		 
		$this->data['text_no_results'] = $this->language->get('text_no_results');


        $this->data['column_product_name']      = $this->language->get('column_product_name');
        $this->data['column_product_model']     = $this->language->get('column_product_model');
        $this->data['column_product_price']     = $this->language->get('column_product_price');
        $this->data['column_product_quantity']  = $this->language->get('column_product_quantity');
        $this->data['column_total_customer']    = $this->language->get('column_total_customer');

        $this->data['column_customer'] = $this->language->get('column_customer');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_customer_group'] = $this->language->get('column_customer_group');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_total'] = $this->language->get('column_total');
		$this->data['column_action'] = $this->language->get('column_action');
        $this->data['button_nyomtatas'] = $this->language->get('button_nyomtatas');
        $this->data['button_exportalas'] = $this->language->get('button_exportalas');
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');

		$this->data['button_filter'] = $this->language->get('button_filter');
		
		$this->data['token'] = $this->session->data['token'];
		
		$url = '';
						
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}


		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;

		$this->load->model('tool/ExportCSV');
		$this->model_tool_ExportCSV->setHeader(array(
			'product_name' 		=>  $this->data['column_product_name'],
			'product_model' 	=>  $this->data['column_product_model'],
			'product_price' 	=>  $this->data['column_product_price'],
			'product_quantity' 	=>  $this->data['column_product_quantity'],
			'total_customer' 	=>  $this->data['column_total_customer'],
		));
		if(!empty($results)) {
			$this->model_tool_ExportCSV->setDatas($this->data['products']);
		} else {
			$this->model_tool_ExportCSV->setDatas(array());
		}
		$this->model_tool_ExportCSV->sendToOutput();
	}
}
?>