<?php
$_['osszehasonlitas']          = array(
    'type'              => 'checkbox',
    'name'              => 'osszehasonlitas',
    'default'           => "0",
    'heading'           => 'Felirat eltűntetése'
);

$_['osszehasonlitas_ikon']          = array(
    'type'              => 'checkbox',
    'name'              => 'osszehasonlitas_ikon',
    'default'           => "0",
    'heading'           => 'Ikon megjelenítése'
);
?>
