<div class="product-compare">
    <a href="<?php echo $compare; ?>" id="compare-total">
        <?php if(isset($template['compare_icon']) && $template['compare_icon']) { ?>
            <?php if($text_compare) { ?>
                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/compare.png')) {
                    $compare = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/compare.png';
                } else {
                    $compare = DIR_TEMPLATE_IMAGE.'default/image/compare.png';
                } ?>
                <img class="icon_meret" title="<? if (isset($text_compare)) { echo $text_compare; }?>"  src="<? echo $compare; ?>" >
            <?php } ?>
        <?php } else { ?>
            <?php echo $text_compare; ?>
        <?php } ?>
    </a>
</div>
