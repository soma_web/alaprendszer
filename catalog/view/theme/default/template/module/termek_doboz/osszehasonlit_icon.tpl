<?php

if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/compare_hover.png')) {
    $compare_hover = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/compare_hover.png';
} elseif(file_exists(DIR_TEMPLATE_IMAGE.'default/image/compare_hover.png')) {
    $compare_hover = DIR_TEMPLATE_IMAGE.'default/image/compare_hover.png';
}

if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/compare.png')) {
    $compare = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/compare.png';
} else {
    $compare = DIR_TEMPLATE_IMAGE.'default/image/compare.png';
}

if(isset($compare_hover)){


?>
    <script>
        function hover_compare(element) {
            element.setAttribute('src', '<?php echo $compare_hover; ?>');
        }
        function unhover_compare(element) {
            element.setAttribute('src', '<?php echo $compare; ?>');
        }
    </script>
<?php } ?>


    <?php  ?>
    <?php if (isset($beallitas['hover_compare'])) { ?>
<a onclick="addToCompare('<?php echo $product['product_id']; ?>');">
    <?php if (!isset($beallitas['kivansaglista'])) { ?>
        <img class="icon_compare" <?php if(isset($compare_hover)){ ?> onmouseover="hover_compare(this);" onmouseout="unhover_compare(this);" <?php } ?> title="<? if (isset($text_compare)) { echo $text_compare; }?>"  src="<? echo $compare?>" >
    <?php }  ?>
</a>
    <?php } else { ?>
    <a onclick="addToCompare('<?php echo $product['product_id']; ?>');">
        <?php if (!isset($beallitas['kivansaglista'])) { ?>
        <img class="icon_compare" title="<?php echo $this->config->get('config_honlap_cim'); ?>"  src="<? echo $compare?>" >
        <?php }  ?>
    </a>
    <?php } ?>

