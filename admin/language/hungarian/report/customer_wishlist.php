<?php

// Heading
$_['heading_title']         = 'Kívánság lista Jelentés';

// Column
$_['column_product_name']      = 'Temék neve';
$_['column_product_model']     = 'Modell';
$_['column_product_price']     = 'Alapár';
$_['column_product_quantity']  = 'Raktáron';
$_['column_total_customer']    = 'Vevő';



$_['column_customer']       = 'Ügyfél neve';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Ügyfélcsoport';

$_['column_status']         = 'Állapot';
$_['column_action']         = 'Művelet';

$_['text_edit']             = 'Vevők';

// Entry
$_['entry_date_start']      = 'Kezdete:';
$_['entry_date_end']        = 'Vége:';
?>