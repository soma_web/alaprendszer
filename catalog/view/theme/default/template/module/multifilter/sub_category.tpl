<?php if(isset($al_categoria_marad) && $al_categoria_marad){ ?>

    <div class="box" style="">

        <div class="box-heading reszek elso-filter" style="position: relative"><?php echo $heading_title_category; ?>
            <?php if( isset($teljes_kategoria) && $teljes_kategoria==1) { ?>
                <img id="alkategoria_heading" style="display: inline;" src="catalog/view/theme/default/image/pos_r_opened.png">
            <?php } ?>
        </div>

        <div class="box-content multi_alkategoria" style="<?php echo isset($teljes_kategoria) && $teljes_kategoria==1 ? 'display: none' : ''?>">
            <ul class="box-filter box-filter-sub-category">
                <?php foreach ($al_categoria_marad as $category) { ?>
                    <li  class="menu-text-image-2" >
                        <?php if (in_array($category['category_id'],$parts)) { ?>
                            <a style="position: relative" href="<?php echo $category['href']; ?>" class="active">  <?php echo $category['name']; ?> </a>
                            <?php if( isset($teljes_kategoria) && $teljes_kategoria==1) { ?>
                                <img class="alkategoria_fokategoriaja" style="display: inline;  margin-left: 10px" src="catalog/view/theme/default/image/desc.png">
                            <?php } ?>

                        <?php } else { ?>
                            <a style="position: relative" href="<?php echo $category['href']; ?>" >  <?php echo $category['name']; ?></a>
                            <?php if( isset($teljes_kategoria) && $teljes_kategoria==1 || true) { ?>
                                <img class="alkategoria_fokategoriaja" style="display: inline; margin-left: 10px" src="catalog/view/theme/default/image/desc.png">
                            <?php } ?>

                        <?php }
                        category_children($category,$parts);
                        ?>

                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <?php if( isset($teljes_kategoria) && $teljes_kategoria==1 || true) { ?>
        <script>
            $("#alkategoria_heading").bind("click",function(){
                if ( $(".multi_alkategoria").css("display") == "none") {
                    $(".multi_alkategoria").slideDown(1000);
                    $("#alkategoria_heading").attr("src","catalog/view/theme/default/image/pos_r_closed.png");

                } else {
                    $(".multi_alkategoria").slideUp(400);
                    $("#alkategoria_heading").attr("src","catalog/view/theme/default/image/pos_r_opened.png");

                }
            });

            $(".alkategoria_fokategoriaja").bind("click",function(){
                if ($(this).next().css("display") == "none") {
                    $(this).next().slideDown(400);
                    $(this).attr("src","catalog/view/theme/default/image/asc.png");
                } else {
                    $(this).next().slideUp(400);
                    $(this).attr("src","catalog/view/theme/default/image/desc.png");

                }
            });

        </script>

        <style>
            #alkategoria_heading {
                position: absolute;
                right: 10px;
            }
            #alkategoria_heading:hover, .alkategoria_fokategoriaja:hover {
                cursor: pointer;
            }



            .alkategoria_fokategoriaja + ul {
                display: none;
            }
        </style>
    <?php } ?>



<?php } ?>