<?php
class ModelWarehouseWarehouse extends Model {

    public function getWarehouses($warehouse_id="") {

        $warehouse_data = array();

        $sql  = "SELECT *, w.warehouse_id FROM " . DB_PREFIX . "warehouse w
          LEFT JOIN " . DB_PREFIX . "warehouse_description wd ON (w.warehouse_id = wd.warehouse_id AND wd.language_id = '" . (int)$this->config->get('config_language_id') . "')
          WHERE w.status = 1";

        if (!empty($warehouse_id)) {
            $sql .= " AND w.warehouse_id='".$warehouse_id."'";
        }

        $sql .= " GROUP BY w.warehouse_id ORDER BY w.sort_order, LCASE(wd.warehouse_name) ";

        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$result['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$result['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $code = $zone_query->row['code'];
            } else {
                $zone = '';
                $code = '';
            }


                $warehouse_data[] = array(
                    'warehouse_id'              => $result['warehouse_id'],
                    'warehouse_name'            => $result['warehouse_name'],
                    'description'               => $result['description'],
                    'country_id'                => $result['country_id'],
                    'country'                   => $country,
                    'iso_code_2'                => $iso_code_2,
                    'iso_code_3'                => $iso_code_3,
                    'zone_id'                   => $result['zone_id'],
                    'zone'                      => $zone,
                    'zone_code'                 => $code,
                    'city'                      => $result['city'],
                    'postcode'                  => $result['postcode'],
                    'address'                   => $result['address'],
                    'contact_name'              => $result['contact_name'],
                    'telephone'                 => $result['telephone'],
                    'position'                  => $result['position']
            );
        }

        return $warehouse_data;
    }



    public function getTotalWarehouseQuantity($warehouse_id,$product_id) {
        $query = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product_warehouse
        WHERE product_id = '" . $product_id . "'
        AND warehouse_id = '" .$warehouse_id."'");

        if ( $query->num_rows > 0) {
            return $query->row['quantity'];
        } else {
            return 0;
        }
    }

    public function getWarehousesProduct($product_id) {
        $query = $this->db->query("SELECT warehouse_id FROM " . DB_PREFIX . "product_warehouse
        WHERE product_id = '" . $product_id . "'");

        $vissza = array();

        foreach ($query->rows as $result) {
            $vissza[] = $result['warehouse_id'];
        }

        return $vissza;

    }
}
?>