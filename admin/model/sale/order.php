<?php
class ModelSaleOrder extends Model {
	public function addOrder($data) {
		$this->load->model('setting/store');
		
		$store_info = $this->model_setting_store->getStore($data['store_id']);
		
		if ($store_info) {
			$store_name = $store_info['name'];
			$store_url = $store_info['url'];
		} else {
			$store_name = $this->config->get('config_name');
			$store_url = HTTP_CATALOG;
		}
		
		$this->load->model('setting/setting');
		
		$setting_info = $this->model_setting_setting->getSetting('setting', $data['store_id']);
			
		if (isset($setting_info['invoice_prefix'])) {
			$invoice_prefix = $setting_info['invoice_prefix'];
		} else {
			$invoice_prefix = $this->config->get('config_invoice_prefix');
		}
		
		$this->load->model('localisation/country');
		
		$this->load->model('localisation/zone');
		
		$country_info = $this->model_localisation_country->getCountry($data['shipping_country_id']);
		
		if ($country_info) {
			$shipping_country = $country_info['name'];
			$shipping_address_format = $country_info['address_format'];
		} else {
			$shipping_country = '';	
			$shipping_address_format = '{firstname} {lastname}' . "\n" . '{company}' . '{adoszam}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}	
		
		$zone_info = $this->model_localisation_zone->getZone($data['shipping_zone_id']);
		
		if ($zone_info) {
			$shipping_zone = $zone_info['name'];
		} else {
			$shipping_zone = '';			
		}	
					
		$country_info = $this->model_localisation_country->getCountry($data['payment_country_id']);
		
		if ($country_info) {
			$payment_country = $country_info['name'];
			$payment_address_format = $country_info['address_format'];			
		} else {
			$payment_country = '';	
			$payment_address_format = '{firstname} {lastname}' . "\n" . '{company}' . '{adoszam}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}
	
		$zone_info = $this->model_localisation_zone->getZone($data['payment_zone_id']);
		
		if ($zone_info) {
			$payment_zone = $zone_info['name'];
		} else {
			$payment_zone = '';			
		}	

		$this->load->model('localisation/currency');

		$currency_info = $this->model_localisation_currency->getCurrencyByCode($this->config->get('config_currency'));
		
		if ($currency_info) {
			$currency_id = $currency_info['currency_id'];
			$currency_code = $currency_info['code'];
			$currency_value = $currency_info['value'];
		} else {
			$currency_id = 0;
			$currency_code = $this->config->get('config_currency');
			$currency_value = 1.00000;			
		}


        $nem     = isset($data['nem']) ? $this->db->escape($data['nem']) : "";
        $eletkor = isset($data['eletkor']) ? $this->db->escape($data['eletkor']) : "";
        $weblap  = isset($data['weblap']) ? $this->db->escape($data['weblap']) : "";

      	$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET
      	        invoice_prefix                      = '" . $this->db->escape($invoice_prefix) . "',
      	        store_id                            = '" . (int)$data['store_id'] . "',
      	        store_name                          = '" . $this->db->escape($store_name) . "',
      	        store_url                           = '" . $this->db->escape($store_url) . "',
      	        customer_id                         = '" . (int)$data['customer_id'] . "',
      	        customer_group_id                   = '" . (int)$data['customer_group_id'] . "',
      	        firstname                           = '" . $this->db->escape($data['firstname']) . "',
      	        lastname                            = '" . $this->db->escape($data['lastname']) . "',
      	        email                               = '" . $this->db->escape($data['email']) . "',
      	        telephone                           = '" . $this->db->escape($data['telephone']) . "',
      	        fax                                 = '" . $this->db->escape($data['fax']) . "',
      	        nem                                 = '" . $nem . "',
      	        eletkor                             = '" . $eletkor . "',
      	        shipping_firstname                  = '" . $this->db->escape($data['shipping_firstname']) . "',
      	        shipping_lastname                   = '" . $this->db->escape($data['shipping_lastname']) . "',
      	        shipping_company                    = '" . $this->db->escape($data['shipping_company']) . "',
      	        shipping_adoszam                    = '" . $this->db->escape($data['shipping_adoszam']) . "',
      	        shipping_vallalkozasi_forma         = '" . $this->db->escape($data['shipping_vallalkozasi_forma']) . "',
      	        shipping_szekhely                   = '" . $this->db->escape($data['shipping_szekhely']) . "',
      	        shipping_ugyvezeto_neve             = '" . $this->db->escape($data['shipping_ugyvezeto_neve']) . "',
      	        shipping_ugyvezeto_telefonszama     = '" . $this->db->escape($data['shipping_ugyvezeto_telefonszama']) . "',
      	        shipping_address_1                  = '" . $this->db->escape($data['shipping_address_1']) . "',
      	        shipping_address_2                  = '" . $this->db->escape($data['shipping_address_2']) . "',
      	        shipping_city                       = '" . $this->db->escape($data['shipping_city']) . "',
      	        shipping_postcode                   = '" . $this->db->escape($data['shipping_postcode']) . "',
      	        shipping_country                    = '" . $this->db->escape($shipping_country) . "',
      	        shipping_country_id                 = '" . (int)$data['shipping_country_id'] . "',
      	        shipping_zone                       = '" . $this->db->escape($shipping_zone) . "',
      	        shipping_zone_id                    = '" . (int)$data['shipping_zone_id'] . "',
      	        shipping_address_format             = '" . $this->db->escape($shipping_address_format) . "',
      	        shipping_method                     = '" . $this->db->escape($data['shipping_method']) . "',
      	        shipping_code                       = '" . $this->db->escape($data['shipping_code']) . "',
      	        payment_firstname                   = '" . $this->db->escape($data['payment_firstname']) . "',
      	        payment_lastname                    = '" . $this->db->escape($data['payment_lastname']) . "',
      	        payment_company                     = '" . $this->db->escape($data['payment_company']) . "',
      	        payment_adoszam                     = '" . $this->db->escape($data['payment_adoszam']) . "',
      	        payment_vallalkozasi_forma          = '" . $this->db->escape($data['payment_vallalkozasi_forma']) . "',
      	        payment_szekhely                    = '" . $this->db->escape($data['payment_szekhely']) . "',
      	        payment_ugyvezeto_neve              = '" . $this->db->escape($data['payment_ugyvezeto_neve']) . "',
      	        payment_ugyvezeto_telefonszama      = '" . $this->db->escape($data['payment_ugyvezeto_telefonszama']) . "',
      	        payment_address_1                   = '" . $this->db->escape($data['payment_address_1']) . "',
      	        payment_address_2                   = '" . $this->db->escape($data['payment_address_2']) . "',
      	        payment_city                        = '" . $this->db->escape($data['payment_city']) . "',
      	        payment_postcode                    = '" . $this->db->escape($data['payment_postcode']) . "',
      	        payment_country                     = '" . $this->db->escape($payment_country) . "',
      	        payment_country_id                  = '" . (int)$data['payment_country_id'] . "',
      	        payment_zone                        = '" . $this->db->escape($payment_zone) . "',
      	        payment_zone_id                     = '" . (int)$data['payment_zone_id'] . "',
      	        payment_address_format              = '" . $this->db->escape($payment_address_format) . "',
      	        payment_method                      = '" . $this->db->escape($data['payment_method']) . "',
      	        payment_code                        = '" . $this->db->escape($data['payment_code']) . "',
      	        comment                             = '" . $this->db->escape($data['comment']) . "',
      	        order_status_id                     = '" . (int)$data['order_status_id'] . "',
      	        affiliate_id                        = '" . (int)$data['affiliate_id'] . "',
      	        language_id                         = '" . (int)$this->config->get('config_language_id') . "',
      	        currency_id                         = '" . (int)$currency_id . "', currency_code = '" . $this->db->escape($currency_code) . "',
      	        currency_value                      = '" . (float)$currency_value . "',
      	        iskolai_vegzettseg                  = '" . $this->db->escape($data['iskolai_vegzettseg']) . "',
      	        vallalkozasi_forma                  = '" . $this->db->escape($data['vallalkozasi_forma']) . "',
      	        szekhely                            = '" . $this->db->escape($data['szekhely']) . "',
      	        ugyvezeto_neve                      = '" . $this->db->escape($data['ugyvezeto_neve']) . "',
      	        ugyvezeto_telefonszama              = '" . $this->db->escape($data['ugyvezeto_telefonszama']) . "',
      	        weblap                              = '" . $weblap . "',
      	        date_added                          = NOW(),
      	        date_modified                       = NOW()");
      	
      	$order_id = $this->db->getLastId();
		
      	if (isset($data['order_product'])) {		
      		foreach ($data['order_product'] as $order_product) {	
      			$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$order_product['product_id'] . "', name = '" . $this->db->escape($order_product['name']) . "', model = '" . $this->db->escape($order_product['model']) . "', quantity = '" . (int)$order_product['quantity'] . "', price = '" . (float)$order_product['price'] . "', total = '" . (float)$order_product['total'] . "', tax = '" . (float)$order_product['tax'] . "', reward = '" . (int)$order_product['reward'] . "'");
			
				$order_product_id = $this->db->getLastId();
	
				if (isset($order_product['order_option'])) {
					foreach ($order_product['order_option'] as $order_option) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$order_option['product_option_id'] . "', product_option_value_id = '" . (int)$order_option['product_option_value_id'] . "', name = '" . $this->db->escape($order_option['name']) . "', `value` = '" . $this->db->escape($order_option['value']) . "', `type` = '" . $this->db->escape($order_option['type']) . "'");
					}
				}
				
				if (isset($order_product['order_download'])) {
					foreach ($order_product['order_download'] as $order_download) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "order_download SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', name = '" . $this->db->escape($order_download['name']) . "', filename = '" . $this->db->escape($order_download['filename']) . "', mask = '" . $this->db->escape($order_download['mask']) . "', remaining = '" . (int)$order_download['remaining'] . "'");
					}
				}
			}
		}
		
		if (isset($data['order_voucher'])) {	
			foreach ($data['order_voucher'] as $order_voucher) {	
      			$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', voucher_id = '" . (int)$order_voucher['voucher_id'] . "', description = '" . $this->db->escape($order_voucher['description']) . "', code = '" . $this->db->escape($order_voucher['code']) . "', from_name = '" . $this->db->escape($order_voucher['from_name']) . "', from_email = '" . $this->db->escape($order_voucher['from_email']) . "', to_name = '" . $this->db->escape($order_voucher['to_name']) . "', to_email = '" . $this->db->escape($order_voucher['to_email']) . "', voucher_theme_id = '" . (int)$order_voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($order_voucher['message']) . "', amount = '" . (float)$order_voucher['amount'] . "'");
			
      			$this->db->query("UPDATE " . DB_PREFIX . "voucher SET order_id = '" . (int)$order_id . "' WHERE voucher_id = '" . (int)$order_voucher['voucher_id'] . "'");
			}
		}
		
		// Get the total
		$total = 0;
		
		if (isset($data['order_total'])) {		
      		foreach ($data['order_total'] as $order_total) {	
      			$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($order_total['code']) . "', title = '" . $this->db->escape($order_total['title']) . "', text = '" . $this->db->escape($order_total['text']) . "', `value` = '" . (float)$order_total['value'] . "', sort_order = '" . (int)$order_total['sort_order'] . "'");
			}
			
			$total += $order_total['value'];
		}


		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET total = '" . (float)$total . "' WHERE order_id = '" . (int)$order_id . "'"); 	
	}
	
	public function editOrder($order_id, $data) {
		$this->load->model('localisation/country');
		
		$this->load->model('localisation/zone');
		
		$country_info = $this->model_localisation_country->getCountry($data['shipping_country_id']);
		
		if ($country_info) {
			$shipping_country = $country_info['name'];
			$shipping_address_format = $country_info['address_format'];
		} else {
			$shipping_country = '';	
			$shipping_address_format = '{firstname} {lastname}' . "\n" . '{company}' . '{adoszam}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}	
		
		$zone_info = $this->model_localisation_zone->getZone($data['shipping_zone_id']);
		
		if ($zone_info) {
			$shipping_zone = $zone_info['name'];
		} else {
			$shipping_zone = '';			
		}	
					
		$country_info = $this->model_localisation_country->getCountry($data['payment_country_id']);
		
		if ($country_info) {
			$payment_country = $country_info['name'];
			$payment_address_format = $country_info['address_format'];			
		} else {
			$payment_country = '';	
			$payment_address_format = '{firstname} {lastname}' . "\n" . '{company}' . '{adoszam}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}
	
		$zone_info = $this->model_localisation_zone->getZone($data['payment_zone_id']);
		
		if ($zone_info) {
			$payment_zone = $zone_info['name'];
		} else {
			$payment_zone = '';			
		}


        $nem     = isset($data['nem'])      ? $this->db->escape($data['nem']) : "";
        $eletkor = isset($data['eletkor'])  ? $this->db->escape($data['eletkor']) : "";
        $weblap  = isset($data['weblap'])   ? $this->db->escape($data['weblap']) : "";


        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET
      	        firstname                           = '" . $this->db->escape($data['firstname']) . "',
      	        lastname                            = '" . $this->db->escape($data['lastname']) . "',
      	        email                               = '" . $this->db->escape($data['email']) . "',
      	        telephone                           = '" . $this->db->escape($data['telephone']) . "',
      	        fax                                 = '" . $this->db->escape($data['fax']) . "',
      	        nem                                 = '" . $nem . "',
      	        eletkor                             = '" . $eletkor . "',
      	        shipping_firstname                  = '" . $this->db->escape($data['shipping_firstname']) . "',
      	        shipping_lastname                   = '" . $this->db->escape($data['shipping_lastname']) . "',
      	        shipping_company                    = '" . $this->db->escape($data['shipping_company']) . "',
      	        shipping_adoszam                    = '" . $this->db->escape($data['shipping_adoszam']) . "',
      	        shipping_vallalkozasi_forma         = '" . $this->db->escape($data['shipping_vallalkozasi_forma']) . "',
      	        shipping_szekhely                   = '" . $this->db->escape($data['shipping_szekhely']) . "',
      	        shipping_ugyvezeto_neve             = '" . $this->db->escape($data['shipping_ugyvezeto_neve']) . "',
      	        shipping_ugyvezeto_telefonszama     = '" . $this->db->escape($data['shipping_ugyvezeto_telefonszama']) . "',
      	        shipping_address_1                  = '" . $this->db->escape($data['shipping_address_1']) . "',
      	        shipping_address_2                  = '" . $this->db->escape($data['shipping_address_2']) . "',
      	        shipping_city                       = '" . $this->db->escape($data['shipping_city']) . "',
      	        shipping_postcode                   = '" . $this->db->escape($data['shipping_postcode']) . "',
      	        shipping_country                    = '" . $this->db->escape($shipping_country) . "',
      	        shipping_country_id                 = '" . (int)$data['shipping_country_id'] . "',
      	        shipping_zone                       = '" . $this->db->escape($shipping_zone) . "',
      	        shipping_zone_id                    = '" . (int)$data['shipping_zone_id'] . "',
      	        shipping_address_format             = '" . $this->db->escape($shipping_address_format) . "',
      	        shipping_method                     = '" . $this->db->escape($data['shipping_method']) . "',
      	        shipping_code                       = '" . $this->db->escape($data['shipping_code']) . "',
      	        payment_firstname                   = '" . $this->db->escape($data['payment_firstname']) . "',
      	        payment_lastname                    = '" . $this->db->escape($data['payment_lastname']) . "',
      	        payment_company                     = '" . $this->db->escape($data['payment_company']) . "',
      	        payment_adoszam                     = '" . $this->db->escape($data['payment_adoszam']) . "',
      	        payment_vallalkozasi_forma          = '" . $this->db->escape($data['payment_vallalkozasi_forma']) . "',
      	        payment_szekhely                    = '" . $this->db->escape($data['payment_szekhely']) . "',
      	        payment_ugyvezeto_neve              = '" . $this->db->escape($data['payment_ugyvezeto_neve']) . "',
      	        payment_ugyvezeto_telefonszama      = '" . $this->db->escape($data['payment_ugyvezeto_telefonszama']) . "',
      	        payment_address_1                   = '" . $this->db->escape($data['payment_address_1']) . "',
      	        payment_address_2                   = '" . $this->db->escape($data['payment_address_2']) . "',
      	        payment_city                        = '" . $this->db->escape($data['payment_city']) . "',
      	        payment_postcode                    = '" . $this->db->escape($data['payment_postcode']) . "',
      	        payment_country                     = '" . $this->db->escape($payment_country) . "',
      	        payment_country_id                  = '" . (int)$data['payment_country_id'] . "',
      	        payment_zone                        = '" . $this->db->escape($payment_zone) . "',
      	        payment_zone_id                     = '" . (int)$data['payment_zone_id'] . "',
      	        payment_address_format              = '" . $this->db->escape($payment_address_format) . "',
      	        payment_method                      = '" . $this->db->escape($data['payment_method']) . "',
      	        payment_code                        = '" . $this->db->escape($data['payment_code']) . "',
      	        comment                             = '" . $this->db->escape($data['comment']) . "',
      	        order_status_id                     = '" . (int)$data['order_status_id'] . "',
      	        affiliate_id                        = '" . (int)$data['affiliate_id'] . "',
      	        iskolai_vegzettseg                  = '" . $this->db->escape($data['iskolai_vegzettseg']) . "',
      	        vallalkozasi_forma                  = '" . $this->db->escape($data['vallalkozasi_forma']) . "',
      	        szekhely                            = '" . $this->db->escape($data['szekhely']) . "',
      	        ugyvezeto_neve                      = '" . $this->db->escape($data['ugyvezeto_neve']) . "',
      	        ugyvezeto_telefonszama              = '" . $this->db->escape($data['ugyvezeto_telefonszama']) . "',
      	        weblap                              = '" . $weblap . "',
      	        date_modified                       = NOW()
      	        WHERE order_id = '" . (int)$order_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'"); 
       	$this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");


        if ($this->config->get("megjelenit_vonalkod_kuldes") == 1) {
            $vonalkodok = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_vonalkod WHERE order_id = '" . (int)$order_id . "'");
            foreach ($vonalkodok->rows as $vonalkod) {
                $utvonal= DIR_DOWNLOAD.$vonalkod['vonalkod'].".jpg";

                if (file_exists($utvonal)) {
                 unlink($utvonal);
                }
            }
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_vonalkod WHERE order_id = '" . (int)$order_id . "'");
        }


      	if (isset($data['order_product'])) {		
      		foreach ($data['order_product'] as $order_product) {	
      			$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET
      			    order_product_id = '" . (int)$order_product['order_product_id'] . "',
      			    order_id = '" . (int)$order_id . "',
      			    product_id = '" . (int)$order_product['product_id'] . "',
      			    `name` = '" . $this->db->escape($order_product['name']) . "',
      			    model = '" . $this->db->escape($order_product['model']) . "',
      			    quantity = '" . (int)$order_product['quantity'] . "',
      			    price = '" . (float)$order_product['price'] . "',
      			    total = '" . (float)$order_product['total'] . "',
      			    tax = '" . (float)$order_product['tax'] . "',
      			    reward = '" . (int)$order_product['reward'] . "'");
			
				$order_product_id = $this->db->getLastId();

                if ($this->config->get('megjelenit_vonalkod_kuldes') == 1) {

                    if (isset($order_product['vonalkod']) && is_array($order_product['vonalkod'])) {
                        if ( count($order_product['vonalkod']) > 0 ) {
                            foreach ($order_product['vonalkod'] as $vonalkod){
                                if (isset($order_product['letoltheto']) && in_array($vonalkod,$order_product['letoltheto']) ) {
                                    $status = 1;
                                } else {
                                    $status = 0;
                                }
                                $sql = "INSERT INTO " .DB_PREFIX. "product_vonalkod SET
                                    order_product_id    = '" . $order_product_id . "',
                                    order_id            = '" . $order_id. "',
                                    product_id          = '" . $order_product['product_id'] . "',
                                    vonalkod            = '" . $vonalkod . "',
                                    date_added          = NOW(),
                                    status              = '" . $status . "'";
                                $this->db->query($sql);
                                $this->utalvanyGenereal($order_product, $vonalkod);
                            }
                        }
                    }
                }
	
				if (isset($order_product['order_option'])) {
					foreach ($order_product['order_option'] as $order_option) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_option_id = '" . (int)$order_option['order_option_id'] . "', order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$order_option['product_option_id'] . "', product_option_value_id = '" . (int)$order_option['product_option_value_id'] . "', name = '" . $this->db->escape($order_option['name']) . "', `value` = '" . $this->db->escape($order_option['value']) . "', `type` = '" . $this->db->escape($order_option['type']) . "'");
					}
				}
				
				if (isset($order_product['order_download'])) {
					foreach ($order_product['order_download'] as $order_download) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "order_download SET order_download_id = '" . (int)$order_download['order_download_id'] . "', order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', name = '" . $this->db->escape($order_download['name']) . "', filename = '" . $this->db->escape($order_download['filename']) . "', mask = '" . $this->db->escape($order_download['mask']) . "', remaining = '" . (int)$order_download['remaining'] . "'");
					}
				}
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'"); 
		
		if (isset($data['order_voucher'])) {	
			foreach ($data['order_voucher'] as $order_voucher) {	
      			$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_voucher_id = '" . (int)$order_voucher['order_voucher_id'] . "', order_id = '" . (int)$order_id . "', voucher_id = '" . (int)$order_voucher['voucher_id'] . "', description = '" . $this->db->escape($order_voucher['description']) . "', code = '" . $this->db->escape($order_voucher['code']) . "', from_name = '" . $this->db->escape($order_voucher['from_name']) . "', from_email = '" . $this->db->escape($order_voucher['from_email']) . "', to_name = '" . $this->db->escape($order_voucher['to_name']) . "', to_email = '" . $this->db->escape($order_voucher['to_email']) . "', voucher_theme_id = '" . (int)$order_voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($order_voucher['message']) . "', amount = '" . (float)$order_voucher['amount'] . "'");
			
				$this->db->query("UPDATE " . DB_PREFIX . "voucher SET order_id = '" . (int)$order_id . "' WHERE voucher_id = '" . (int)$order_voucher['voucher_id'] . "'");
			}
		}
		
		// Get the total
		$total = 0;
				
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");
		
		if (isset($data['order_total'])) {		
      		foreach ($data['order_total'] as $order_total) {	
      			$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_total_id = '" . (int)$order_total['order_total_id'] . "', order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($order_total['code']) . "', title = '" . $this->db->escape($order_total['title']) . "', text = '" . $this->db->escape($order_total['text']) . "', `value` = '" . (float)$order_total['value'] . "', sort_order = '" . (int)$order_total['sort_order'] . "'");
			}
			
			$total += $order_total['value'];
		}
		 
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET total = '" . (float)$total . "' WHERE order_id = '" . (int)$order_id . "'"); 
	}
	
	public function deleteOrder($order_id) {
		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_status_id > '0' AND order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

			foreach($product_query->rows as $product) {
				$this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

				$option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

				foreach ($option_query->rows as $option) {
					$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
				}
			}
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
      	$this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
      	$this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_fraud WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id = '" . (int)$order_id . "'");
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$reward = 0;
			
			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		
			foreach ($order_product_query->rows as $product) {
				$reward += $product['reward'];
			}			
			
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}
		
			if ($order_query->row['affiliate_id']) {
				$affiliate_id = $order_query->row['affiliate_id'];
			} else {
				$affiliate_id = 0;
			}				
				
			$this->load->model('sale/affiliate');
				
			$affiliate_info = $this->model_sale_affiliate->getAffiliate($affiliate_id);
				
			if ($affiliate_info) {
				$affiliate_firstname = $affiliate_info['firstname'];
				$affiliate_lastname = $affiliate_info['lastname'];
			} else {
				$affiliate_firstname = '';
				$affiliate_lastname = '';				
			}

			$this->load->model('localisation/language');
			
			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);
			
			if ($language_info) {
				$language_code = $language_info['code'];
				$language_filename = $language_info['filename'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_filename = '';
				$language_directory = '';
			}
			
			return array(
				'order_id'                          => $order_query->row['order_id'],
				'invoice_no'                        => $order_query->row['invoice_no'],
				'invoice_prefix'                    => $order_query->row['invoice_prefix'],
				'store_id'                          => $order_query->row['store_id'],
				'store_name'                        => $order_query->row['store_name'],
				'store_url'                         => $order_query->row['store_url'],
				'customer_id'                       => $order_query->row['customer_id'],
				'customer'                          => $order_query->row['customer'],
				'customer_group_id'                 => $order_query->row['customer_group_id'],
				'firstname'                         => $order_query->row['firstname'],
				'lastname'                          => $order_query->row['lastname'],
				'telephone'                         => $order_query->row['telephone'],
				'fax'                               => $order_query->row['fax'],
				'nem'                               => $order_query->row['nem'],
				'eletkor'                           => $order_query->row['eletkor'],
				'email'                             => $order_query->row['email'],
				'shipping_firstname'                => $order_query->row['shipping_firstname'],
				'shipping_lastname'                 => $order_query->row['shipping_lastname'],
				'shipping_company'                  => $order_query->row['shipping_company'],
				'shipping_adoszam'                  => $order_query->row['shipping_adoszam'],
				'shipping_address_1'                => $order_query->row['shipping_address_1'],
				'shipping_address_2'                => $order_query->row['shipping_address_2'],
				'shipping_postcode'                 => $order_query->row['shipping_postcode'],
				'shipping_city'                     => $order_query->row['shipping_city'],
				'shipping_zone_id'                  => $order_query->row['shipping_zone_id'],
				'shipping_zone'                     => $order_query->row['shipping_zone'],
				'shipping_zone_code'                => $shipping_zone_code,
				'shipping_country_id'               => $order_query->row['shipping_country_id'],
				'shipping_country'                  => $order_query->row['shipping_country'],
				'shipping_iso_code_2'               => $shipping_iso_code_2,
				'shipping_iso_code_3'               => $shipping_iso_code_3,
				'shipping_address_format'           => $order_query->row['shipping_address_format'],
				'shipping_method'                   => $order_query->row['shipping_method'],
                'shipping_warehouse_id'             => $order_query->row['shipping_warehouse_id'],
                'shipping_warehouse_name'           => $order_query->row['shipping_warehouse_name'],
                'shipping_warehouse_email'          => $order_query->row['shipping_warehouse_email'],
				'shipping_code'                     => $order_query->row['shipping_code'],
                'shipping_vallalkozasi_forma'       => $order_query->row['shipping_vallalkozasi_forma'],
                'shipping_szekhely'                 => $order_query->row['shipping_szekhely'],
                'shipping_ugyvezeto_neve'           => $order_query->row['shipping_ugyvezeto_neve'],
                'shipping_ugyvezeto_telefonszama'   => $order_query->row['shipping_ugyvezeto_telefonszama'],
				'payment_firstname'                 => $order_query->row['payment_firstname'],
				'payment_lastname'                  => $order_query->row['payment_lastname'],
				'payment_company'                   => $order_query->row['payment_company'],
				'payment_adoszam'                   => $order_query->row['payment_adoszam'],
				'payment_address_1'                 => $order_query->row['payment_address_1'],
				'payment_address_2'                 => $order_query->row['payment_address_2'],
				'payment_postcode'                  => $order_query->row['payment_postcode'],
				'payment_city'                      => $order_query->row['payment_city'],
				'payment_zone_id'                   => $order_query->row['payment_zone_id'],
				'payment_zone'                      => $order_query->row['payment_zone'],
				'payment_zone_code'                 => $payment_zone_code,
				'payment_country_id'                => $order_query->row['payment_country_id'],
				'payment_country'                   => $order_query->row['payment_country'],
				'payment_iso_code_2'                => $payment_iso_code_2,
				'payment_iso_code_3'                => $payment_iso_code_3,
				'payment_address_format'            => $order_query->row['payment_address_format'],
				'payment_method'                    => $order_query->row['payment_method'],
				'payment_code'                      => $order_query->row['payment_code'],
                'payment_vallalkozasi_forma'        => $order_query->row['payment_vallalkozasi_forma'],
                'payment_szekhely'                  => $order_query->row['payment_szekhely'],
                'payment_ugyvezeto_neve'            => $order_query->row['payment_ugyvezeto_neve'],
                'payment_ugyvezeto_telefonszama'    => $order_query->row['payment_ugyvezeto_telefonszama'],
                'vallalkozasi_forma'                => $order_query->row['vallalkozasi_forma'],
      	        'szekhely'                          => $order_query->row['szekhely'],
      	        'ugyvezeto_neve'                    => $order_query->row['ugyvezeto_neve'],
      	        'ugyvezeto_telefonszama'            => $order_query->row['ugyvezeto_telefonszama'],
				'comment'                           => $order_query->row['comment'],
				'total'                             => $order_query->row['total'],
				'reward'                            => $reward,
				'order_status_id'                   => $order_query->row['order_status_id'],
				'affiliate_id'                      => $order_query->row['affiliate_id'],
				'affiliate_firstname'               => $affiliate_firstname,
				'affiliate_lastname'                => $affiliate_lastname,
				'commission'                        => $order_query->row['commission'],
				'language_id'                       => $order_query->row['language_id'],
				'language_code'                     => $language_code,
				'language_filename'                 => $language_filename,
				'language_directory'                => $language_directory,
				'currency_id'                       => $order_query->row['currency_id'],
				'currency_code'                     => $order_query->row['currency_code'],
				'currency_value'                    => $order_query->row['currency_value'],
				'ip'                                => $order_query->row['ip'],
				'forwarded_ip'                      => $order_query->row['forwarded_ip'],
				'user_agent'                        => $order_query->row['user_agent'],
				'accept_language'                   => $order_query->row['accept_language'],
                'iskolai_vegzettseg'                => $order_query->row['iskolai_vegzettseg'],
				'date_added'                        => $order_query->row['date_added'],
				'weblap'                            => (!empty($order_query->row['weblap']) ? $order_query->row['weblap'] : ''),
				'date_modified'                     => $order_query->row['date_modified']
			);
		} else {
			return false;
		}
	}
	
	public function getOrders($data = array()) {
		$sql = "SELECT o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified, o.order_status_id FROM `" . DB_PREFIX . "order` o";

		if (isset($data['filter_order_status_id']) && !is_null($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND LCASE(CONCAT(o.firstname, ' ', o.lastname)) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_customer'])) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND round(o.total) = '" . (float)$data['filter_total'] . "'";
		}

		$sort_data = array(
			'o.order_id',
			'customer',
			'status',
			'o.date_added',
			'o.date_modified',
			'o.total'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY o.order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getOrderProducts($order_id) {
		$query = $this->db->query("SELECT op.*, p.utalvany AS ingyenes FROM " . DB_PREFIX . "order_product op
            LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = op.product_id)
		    WHERE order_id = '" . (int)$order_id . "'");

       /* $sql = "SELECT *, (SELECT ingyenes FROM " . DB_PREFIX . "product WHERE product_id = op.product_id) AS ingyenes FROM " . DB_PREFIX . "order_product op
		    WHERE order_id = '" . (int)$order_id . "'";

        $query = $this->db->query($sql);*/


		return $query->rows;
	}

    public function getOrderLetoltheto($order_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_vonalkod WHERE order_product_id = '" . (int)$order_product_id . "'");

		return $query->rows;
	}
	
	public function getOrderOption($order_id, $order_option_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_option_id = '" . (int)$order_option_id . "'");

		return $query->row;
	}
	
	public function getOrderOptions($order_id, $order_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

		return $query->rows;
	}

	public function getOrderDownloads($order_id, $order_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

		return $query->rows;
	}
	
	public function getOrderVouchers($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
		
		return $query->rows;
	}
	
	public function getOrderVoucherByVoucherId($voucher_id) {
      	$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_voucher` WHERE voucher_id = '" . (int)$voucher_id . "'");

		return $query->row;
	}
				
	public function getOrderTotals($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

		return $query->rows;
	}

	public function getTotalOrders($data = array()) {
      	$sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order`";

		if (isset($data['filter_order_status_id']) && !is_null($data['filter_order_status_id'])) {
			$sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE order_status_id > '0'";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(firstname, ' ', lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND round(total) = '" . (float)$data['filter_total'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalOrdersByStoreId($store_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE store_id = '" . (int)$store_id . "'");

		return $query->row['total'];
	}

	public function getTotalOrdersByOrderStatusId($order_status_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id = '" . (int)$order_status_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalOrdersByLanguageId($language_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE language_id = '" . (int)$language_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalOrdersByCurrencyId($currency_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE currency_id = '" . (int)$currency_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}
	
	public function getTotalSales() {
      	$query = $this->db->query("SELECT SUM(total) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalSalesByYear($year) {
      	$query = $this->db->query("SELECT SUM(total) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id > '0' AND YEAR(date_added) = '" . (int)$year . "'");

		return $query->row['total'];
	}

	public function createInvoiceNo($order_id) {
		$order_info = $this->getOrder($this->request->get['order_id']);
			
		if ($order_info && !$order_info['invoice_no']) {
			$query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order` WHERE invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "'");
	
			if ($query->row['invoice_no']) {
				$invoice_no = $query->row['invoice_no'] + 1;
			} else {
				$invoice_no = 1;
			}
		
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "' WHERE order_id = '" . (int)$order_id . "'");
			
			return $order_info['invoice_prefix'] . $invoice_no;
		}
	}
	
	public function addOrderHistory($order_id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$data['order_status_id'] . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$data['order_status_id'] . "', notify = '" . (isset($data['notify']) ? (int)$data['notify'] : 0) . "', comment = '" . $this->db->escape(strip_tags($data['comment'])) . "', date_added = NOW()");

		$order_info = $this->getOrder($order_id);

		// Send out any gift voucher mails
		if ($this->config->get('config_complete_status_id') == $data['order_status_id']) {
			$this->load->model('sale/voucher');

			$results = $this->getOrderVouchers($order_id);
			
			foreach ($results as $result) {
				$this->model_sale_voucher->sendVoucher($result['voucher_id']);
			}
		}

      	if ($data['notify']) {
            $language = new Language($order_info['language_directory'],$this->config->get('config_template'));
			$language->load($order_info['language_filename']);
			$language->load('mail/order');

			$subject = sprintf($language->get('text_subject'), $order_info['store_name'], $order_id);

			$message  = $language->get('text_order') . ' ' . $order_id . "\n";
			$message .= $language->get('text_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";
			
			$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$data['order_status_id'] . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
				
			if ($order_status_query->num_rows) {
				$message .= $language->get('text_order_status') . "\n";
				$message .= $order_status_query->row['name'] . "\n\n";
			}
			
			if ($order_info['customer_id']) {
				$message .= $language->get('text_link') . "\n";
				$message .= html_entity_decode($order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id, ENT_QUOTES, 'UTF-8') . "\n\n";
			}
			
			if ($data['comment']) {
				$message .= $language->get('text_comment') . "\n\n";
				$message .= strip_tags(html_entity_decode($data['comment'], ENT_QUOTES, 'UTF-8')) . "\n\n";
			}

			$message .= $language->get('text_footer');

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo($order_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($order_info['store_name']);
			//$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
            $mail->setSubject($subject);

            $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}
	}
		
	public function getOrderHistories($order_id, $start = 0, $limit = 10) {
		$query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}
	
	public function getTotalOrderHistories($order_id) {
	  	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}	
		
	public function getTotalOrderHistoriesByOrderStatusId($order_status_id) {
	  	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_status_id = '" . (int)$order_status_id . "'");

		return $query->row['total'];
	}	
	
	public function getEmailsByProductsOrdered($products, $start=0, $end=0) {
		$implode = array();
		
		foreach ($products as $product_id) {
			$implode[] = "op.product_id = '" . $product_id . "'";
		}
		
		$query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'");
	
		return $query->rows;
	}
	
	public function getTotalEmailsByProductsOrdered($products) {
		$implode = array();
		
		foreach ($products as $product_id) {
			$implode[] = "op.product_id = '" . $product_id . "'";
		}
				
		$query = $this->db->query("SELECT COUNT(DISTINCT email) AS total FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'");
	
		return $query->row['total'];
	}

    private function utalvanyGenereal($termek, $vonalkod) {


        $utvonal1   = DIR_DOWNLOAD.$vonalkod."file.jpg";
        $utvonal    = DIR_DOWNLOAD.$vonalkod.".jpg";
        if (!file_exists($utvonal) ) {

            $bc = new barCode("jpeg",30);
            $bc->build($vonalkod,false,$utvonal1);

            //header("Content-Type: image/jpg");

            $this->load->model('catalog/product');
            $this->load->model('tool/image');

            $results = $this->model_catalog_product->getProduct($termek['product_id']);

            $this->language->load('catalog/product');
            $kaphato = $this->language->get('text_kaphato');
            $erteke = $this->language->get('text_utalvany_erteke');

            $src_width  = $this->config->get('config_image_product_width');
            $src_height = $this->config->get('config_image_product_height');

            $image = $this->model_tool_image->resize($results['image'], $src_width, $src_height);

            if ($image) {
                $src = imagecreatefromjpeg($image);
            }
            $vonalkod_src = imagecreatefromjpeg(DIR_DOWNLOAD.$vonalkod."file.jpg");


            $szovegek = strip_tags(html_entity_decode($results['description'], ENT_QUOTES, 'UTF-8'));
            $szovegek = str_replace("\r","",$szovegek);
            $szovegek = str_replace("\n","",$szovegek);
            $szovegek = str_replace("\t","",$szovegek);
            $szovegek = str_replace("&nbsp;","",$szovegek);


            $szovegek = explode(" ",$szovegek);

            $sorkiir = "";
            $i = 1;
            foreach ($szovegek as $szoveg) {
                $szoveg = substr($szoveg,0,50);
                if ( (strlen($szoveg)+strlen($sorkiir)) > 60 ) {
                    $sorkiir = "";
                    $i++;
                }
                $sorkiir .= $szoveg . " ";
            }

            if (!empty($sorkiir)) {
                $i++;
            }


            $width = $src_width + 350;
            $height = $src_height + (30*$i)+120;


            $im = @imagecreate($width, $height);
            $background_color = imagecolorallocate($im, 250, 250, 250);
            $text_color = imagecolorallocate($im, 0, 0, 0);
            $text_color1 = imagecolorallocate($im, 90, 90, 90);


            $font = DIR_CATALOG.'view/theme/default/stylesheet/impact.ttf';
            $font2 = DIR_CATALOG.'view/theme/default/stylesheet/arial.ttf';

            $nevek = explode(" ",$results['name']);
            $sorkiier_nev = "";
            $t = 0;
            foreach ($nevek as $nev) {
                $nev = substr($nev,0,20);
                if ( (strlen($nev)+strlen($sorkiier_nev)) > 30 ) {
                    imagettftext($im, 20, 0, $src_width+10, 30+($t*30), $text_color1, $font, $sorkiier_nev);
                    $sorkiier_nev = "";
                    $t++;
                }
                $sorkiier_nev .= $nev . " ";
            }
            if (!empty($sorkiier_nev) ) {
                imagettftext($im, 20, 0, $src_width+10, 30+($t*30), $text_color1, $font, $sorkiier_nev);
            }



            $ar = $this->currency->format($termek['price'],4);
            imagettftext($im, 12, 0, $src_width+10, 70+($t*30), $text_color, $font2, $erteke);
            imagettftext($im, 14, 0, $src_width+160, 70+($t*30), $text_color, $font, $ar);

            if (!empty($results['date_ervenyes_ig']) && $results['date_ervenyes_ig'] > "0000-00-00" ) {
                imagettftext($im, 12, 0, $src_width+10, 100+($t*30), $text_color1, $font2, $kaphato);
                imagettftext($im, 13, 0, $src_width+160, 100+($t*30), $text_color1, $font, $results['date_ervenyes_ig']."-ig");
            }
            imagettftext($im, 13, 0, 50, $src_height+75, $text_color1, $font, $vonalkod);

            $sorkiir = "";
            $i = 1;
            foreach ($szovegek as $szoveg) {
                $szoveg = substr($szoveg,0,50);
                if ( (strlen($szoveg)+strlen($sorkiir)) > 62 ) {
                    imagettftext($im, 12, 0, 10, $src_height+(30*$i)+100, $text_color1, $font2,$sorkiir);
                    $sorkiir = "";
                    $i++;
                }
                $sorkiir .= $szoveg . " ";
            }
            imagettftext($im, 12, 0, 10, $src_height+(30*$i)+100, $text_color1, $font2,$sorkiir);

            $utvonal= DIR_DOWNLOAD.$vonalkod.".jpg";
            if ($image) {
                imagecopymerge($im, $src, 0, 0, 0, 0, $src_width, $src_height, 100);
            }
            imagecopymerge($im, $vonalkod_src, 10, $src_height+20, 0, 0, 200, 30, 100);

            imagejpeg($im,$utvonal);
            unlink($utvonal1);
        }
    }

    public function get_szamlazzhu_config($order_id){
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."szamlazzhu WHERE order_id=".$order_id);
        return $query->rows;
    }
    public function get_product_settings($order_id){
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id=".$order_id);
        return $query->rows;
    }
    public function get_modul_status($modul_code){
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."extension WHERE code='".$modul_code."'");
        return $query->rows;
    }
    public function get_customer($customer_id){
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."customer WHERE customer_id='".$customer_id."'");
        return $query->rows;
    }
}

class barCode
{
    public $bcHeight, $bcThinWidth, $bcThickWidth, $bcFontSize, $mode;

    function __construct($mode='gif', $height=50, $thin=2, $thick=3, $fSize=2)
    {
        $this->bcHeight = $height;
        $this->bcThinWidth = $thin;
        $this->bcThickWidth = $this->bcThinWidth * $thick;
        $this->fontSize = $fSize;
        $this->mode = $mode;
        $this->outMode = array('gif'=>'gif', 'png'=>'png', 'jpeg'=>'jpeg', 'wbmp'=>'vnd.wap.wbmp');
        $this->codeMap = array(
            '0'=>'000110100',	'1'=>'100100001',	'2'=>'001100001',	'3'=>'101100000',
            '4'=>'000110001',	'5'=>'100110000',	'6'=>'001110000',	'7'=>'000100101',
            '8'=>'100100100',	'9'=>'001100100',	'A'=>'100001001',	'B'=>'001001001',
            'C'=>'101001000',	'D'=>'000011001',	'E'=>'100011000',	'F'=>'001011000',
            'G'=>'000001101',	'H'=>'100001100',	'I'=>'001001100',	'J'=>'000011100',
            'K'=>'100000011',	'L'=>'001000011',	'M'=>'101000010',	'N'=>'000010011',
            'O'=>'100010010',	'P'=>'001010010',	'Q'=>'000000111',	'R'=>'100000110',
            'S'=>'001000110',	'T'=>'000010110',	'U'=>'110000001',	'V'=>'011000001',
            'W'=>'111000000',	'X'=>'010010001',	'Y'=>'110010000',	'Z'=>'011010000',
            ' '=>'011000100',	'$'=>'010101000',	'%'=>'000101010',	'*'=>'010010100',
            '+'=>'010001010',	'-'=>'010000101',	'.'=>'110000100',	'/'=>'010100010'
        );
    }

    public function build($text='', $showText=false, $fileName=null)
    {
        if (trim($text) <= ' ')
            throw new exception('barCode::build - must be passed text to operate');
        if (!$fileType = $this->outMode[$this->mode])
            throw new exception("barCode::build - unrecognized output format ({$this->mode})");
        if (!function_exists("image{$this->mode}"))
            throw new exception("barCode::build - unsupported output format ({$this->mode} - check phpinfo)");

        $text  =  strtoupper($text);
        $dispText = "* $text *";
        $text = "*$text*"; // adds start and stop chars
        $textLen  =  strlen($text);
        $barcodeWidth  =  $textLen * (7 * $this->bcThinWidth + 3 * $this->bcThickWidth) - $this->bcThinWidth;
        $im = imagecreate($barcodeWidth, $this->bcHeight);
        $black = imagecolorallocate($im, 0, 0, 0);
        $white = imagecolorallocate($im, 255, 255, 255);
        imagefill($im, 0, 0, $white);

        $xpos = 0;
        for ($idx=0; $idx<$textLen; $idx++)
        {
            if (!$char = $text[$idx]) $char = '-';
            for ($ptr=0; $ptr<=8; $ptr++)
            {
                $elementWidth = ($this->codeMap[$char][$ptr]) ? $this->bcThickWidth : $this->bcThinWidth;
                if (($ptr + 1) % 2)
                    imagefilledrectangle($im, $xpos, 0, $xpos + $elementWidth-1, $this->bcHeight, $black);
                $xpos += $elementWidth;
            }
            $xpos += $this->bcThinWidth;
        }

        if ($showText)
        {
            $pxWid = imagefontwidth($this->fontSize) * strlen($dispText) + 10;
            $pxHt = imagefontheight($this->fontSize) + 2;
            $bigCenter = $barcodeWidth / 2;
            $textCenter = $pxWid / 2;
            imagefilledrectangle($im, $bigCenter - $textCenter, $this->bcHeight - $pxHt, $bigCenter + $textCenter, $this->bcHeight, $white);
            imagestring($im, $this->fontSize, ($bigCenter - $textCenter) + 5, ($this->bcHeight - $pxHt) + 1, $dispText, $black);
        }

        if (!$fileName) header("Content-type:  image/{$fileType}");
        switch($this->mode)
        {
            case 'gif':
                imagegif($im, $fileName);
                break;
            case 'png':
                imagepng($im, $fileName);
                break;
            case 'jpeg':
                imagejpeg($im, $fileName);
                break;
            case 'wbmp':
                imagewbmp($im, $fileName);
                break;
        }

        imagedestroy($im);
    }
}
?>