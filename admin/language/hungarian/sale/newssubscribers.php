<?php
// Heading
$_['heading_title']    = 'Hírlevélfeliratkozók';

// Text
$_['text_success']     = 'Sikeres frissítés!';
$_['text_export']     = 'Exportálás';

// Column
$_['column_name']      = 'Név';
$_['column_email']     = 'Email';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_name']       = 'Név';
$_['entry_code']       = 'Email';



// Error
$_['error_email_exist']= 'Email Id már létezik';
$_['error_email']      = 'Helytelen email formátum';
$_['error_permission'] = 'Nincs módosítási engedélye!';
?>