<?php
/**
 * Class Model
 *
 * @property Affiliate                          $affiliate
 * @property Cache                              $cache
 * @property Captcha                            $captcha
 * @property Cart                               $cart
 * @property Config                             $config
 * @property Currency                           $currency
 * @property Customer                           $customer
 * @property MySQL                              $db
 * @property Document                           $document
 * @property Encryption                         $encryption
 * @property Loader                             $load
 * @property Image                              $image
 * @property Language                           $language
 * @property Length                             $length
 * @property Log                                $log
 * @property Mail                               $mail
 * @property Pagination                         $pagination
 * @property Request                            $request
 * @property Response                           $response
 * @property Session                            $session
 * @property Tax                                $tax
 * @property Template                           $template
 * @property Url                                $url
 * @property User                               $user
 * @property Weight                             $weight
 * @property ModelLocalisationCountry           $model_localisation_country
 * @property ModelLocalisationZone              $model_localisation_zone
 * @property ModelAccountAddress                $model_account_address
 * @property ModelAccountCustomer               $model_account_customer
 * @property ModelSaleCustomer                  $model_sale_customer
 * @property ModelSaleOrder                     $model_sale_order
 * @property ModelLocalisationLanguage          $model_localisation_language
 * @property ModelCheckoutFraud                 $model_checkout_fraud
 * @property ModelCheckoutVoucher               $model_checkout_voucher
 * @property ModelAccountRendelesek             $model_account_rendelesek
 * @property ModelAccountIskolaiVegzettseg      $model_account_iskolaivegzettseg
 * @property ModelAccountProduct                $model_account_product
 *
 * @property ModelAccountNewsletterCategories   $model_account_newslettercategories
 * @property ModelSaleNewsletterCategories      $model_sale_newslettercategories
 * @property ModelSaleNewsSubscribers           $model_sale_newssubscribers
 * @property ModelSaleVegzettseg                $model_sale_vegzettseg
 * @property CartElhelyezkedes                  $cart_elhelyezkedes
 * @property ModelToolImage                     $resize
 * @property ModelCheckoutOrder                 $model_checkout_order
 * @property ModelCatalogProductImagePlaces     $model_catalog_productimageplaces
 * @property ModelSettingExtension              $model_setting_extension
 * @property ModelAccountElhelyezkedes          $model_account_elhelyezkedes
 * @property ModelAccountElhelyezkedesAlcsoport $model_account_elhelyezkedes_alcsoport
 * @property ModelAccountKiemelesek             $model_account_kiemelesek
 * @property ModelCatalogMultifilter            $model_catalog_multifilter
 * @property ModelAccountPostcode               $model_account_postcode
 * @property ModelAccountRememberMe             $model_account_rememberme
 * @property ModelCatalogInformation            $model_catalog_information
 * @property ModelAccountKategoriaJelentes      $getCategory
 * @property ModelModuleArajanlat               $model_module_arajanlat
 * @property ModelAccountArajanlat              $model_account_arajanlat
 * @property ModelCatalogMultiSzurok            $model_catalog_multi_szurok
 * @property ModelCatalogMultiKozosites         $model_catalog_multi_kozosites
 * @property ModelAccountNewslettersubscribe    $model_account_newslettersubscribe
 * @property ModelCsvProduct                    $model_csv_product
 * @property ModelCronAltalanos                 $model_cron_altalanos
 * @property ModelModuleCron                    $model_module_cron
 * @property ModelCronCrmVevojeloltToKapcsolat  $model_cron_crm_vevojelolt_to_kapcsolat
 * @property ModelCronEusolarVtigerProducts     $model_cron_eusolar_vtiger_products
 * @property ModelCronEusolarVtigerCsomag       $model_cron_eusolar_vtiger_csomag
 * @property ModelCronCrmSzamlaSzamlazzKiegyenlitve       $model_cron_crm_szamla_szamlazz_kiegyenlitve


 */

abstract class Model {
	protected $registry;
	
	public function __construct($registry) {
		$this->registry = $registry;
	}
	
	public function __get($key) {
		return $this->registry->get($key);
	}
	
	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}
?>