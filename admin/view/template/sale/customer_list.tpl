<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php }  ?>
 <div class="warning warning2" style="display:none"></div>
  <div class="box">



      <div class="customer_list_oszlop" style="display: none">
          <img  style="position: absolute; right: 4px; top: 5px;"   src="view/image/close.png" alt="" title="Bezár"
                onclick="$(this).parent().slideUp('slow');"  >

          <h3 style="margin-top: 16px">Jelölje be a megjeleníteni kívánt oszlopokat</h3>




          <input type="checkbox" name="sale_name" value="1" <?php echo $this->config->get('sale_name') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_name;?><br>
          <input type="checkbox" name="sale_email" value="1" <?php echo $this->config->get('sale_email') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_email;?><br>
          <input type="checkbox" name="sale_customer_group" value="1" <?php echo $this->config->get('sale_customer_group') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_customer_group;?><br>
          <input type="checkbox" name="sale_status" value="1" <?php echo $this->config->get('sale_status') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_status;?><br>
          <input type="checkbox" name="sale_approved" value="1" <?php echo $this->config->get('sale_approved') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_approved;?><br>
          <input type="checkbox" name="sale_ip" value="1" <?php echo $this->config->get('sale_ip') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_ip;?><br>
          <input type="checkbox" name="sale_date_added" value="1" <?php echo $this->config->get('sale_date_added') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_date_added;?><br>
          <?php if ($sap == 1) {?>
                <input type="checkbox" name="sale_atkuldve" value="1" <?php echo $this->config->get('sale_atkuldve') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_atkuldve;?><br>
          <?php } ?>

          <?php if (!empty($megjelenit_vevok['partner_kod'])) { ?>
                <input type="checkbox" name="sale_partner_kod" value="1" <?php echo $this->config->get('sale_partner_kod') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_partner_kod;?><br>
          <?php } ?>
          <input type="checkbox" name="sale_belepes" value="1" <?php echo $this->config->get('sale_belepes') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_login;?><br>
          <input type="checkbox" name="sale_company" value="1" <?php echo $this->config->get('sale_company') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_company;?><br>
          <input type="checkbox" name="sale_szazalek" value="1" <?php echo $this->config->get('sale_szazalek') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_szazalek;?><br>
          <input type="checkbox" name="sale_fizetesi_mod" value="1" <?php echo $this->config->get('sale_fizetesi_mod') == 1 ? 'checked="checked"' : ''?> ><?php echo $column_fizetesi_mod;?><br>



          <a  onclick="oszlopBealit()" style="margin-left: auto; display: table;" class="button">Beállít</a>
      </div>


    <div class="heading">
      <img style="position: relative; top: 6px; margin-right: 5px;" src="view/image/customer.png" alt="" /> <?php //echo $heading_title; ?>
        <a class="button" onclick="$('.customer_list_oszlop').css('display') == 'none' ? $('.customer_list_oszlop').slideDown('slow') : $('.customer_list_oszlop').slideUp('slow')"> <?php echo $heading_title; ?></a>

        <div class="buttons">
          <?php if(isset($megjelenit_vevok['csv_vevok']) &&  ($megjelenit_vevok['csv_vevok']) == 1 && !empty($megjelenit_vevok['csv_vevok_csoportnev'])) {?>
              <a onclick="csvSzures()" class="button" style="position: relative;"><?php echo $button_csv_export; ?></a>
              <span id="csv_beallitas" style="display: none; position: absolute; border-radius: 4px 4px 6px 6px; background: #efefef; border: 1px solid #bbbbbb; padding: 20px">
                      <tr>
                          <td><?php echo $text_rendeles_datum;?></td>
                          <td>
                              <input type="text"    name="data[]" class = "datadatumtol date" value="" size="8">
                          </td>
                          <td>
                              <input type="text"    name="data[]" class = "datadatumig date" value="" size="8">
                          </td>
                      </tr>
                      <tr>
                          <td colspan="3" style="text-align: right; padding-top: 20px">
                              <a onclick="csvIndit()" class="button" style="position: relative;"><?php echo $text_letoltes; ?></a>
                          </td>
                      </tr>
                  </table>
              </span>
          <?php } ?>
          <script>
              var kivalasztott = 'input[type="checkbox"]:checked';
          </script>
          <?php $klassz = "class = 'warning'"; ?>
          <a onclick="if ($(kivalasztott).is(':checked')) { $('form').attr('action', '<?php echo $approve; ?>'); $('form').submit(); } else { $('.warning').css('display','block');$('.warning2').html('<span>Kérlek jelölj ki valamit.</span>') }" class="button"><?php echo $button_approve; ?></a>
          <a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
          <a onclick="if ($(kivalasztott).is(':checked')) {$('form').attr('action', '<?php echo $delete; ?>'); $('form').submit(); } else { $('.warning').css('display','block');$('.warning2').html('<span>Kérlek jelölj ki valamit.</span>') }" class="button"><?php echo $button_delete; ?></a>
      </div>
    </div>
    <div class="content">
      <form action="" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

                <?php if ($this->config->get('sale_name')) { ?>
                    <td class="left">
                        <?php if ($sort == 'name') { ?>
                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('sale_email')) { ?>
                    <td class="left">
                        <?php if ($sort == 'c.email') { ?>
                            <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('sale_customer_group')) { ?>
                    <td class="left">
                        <?php if ($sort == 'customer_group') { ?>
                            <a href="<?php echo $sort_customer_group; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer_group; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_customer_group; ?>"><?php echo $column_customer_group; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('sale_status')) { ?>
                    <td class="left">
                        <?php if ($sort == 'c.status') { ?>
                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('sale_approved')) { ?>
                    <td class="left">
                        <?php if ($sort == 'c.approved') { ?>
                            <a href="<?php echo $sort_approved; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_approved; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_approved; ?>"><?php echo $column_approved; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('sale_ip')) { ?>
                    <td class="left">
                        <?php if ($sort == 'c.ip') { ?>
                            <a href="<?php echo $sort_ip; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_ip; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_ip; ?>"><?php echo $column_ip; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('sale_date_added')) { ?>
                    <td class="left">
                        <?php if ($sort == 'c.date_added') { ?>
                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>



                <?php if (!empty($megjelenit_vevok['partner_kod'])) { ?>
                    <?php if ($this->config->get('sale_partner_kod')) { ?>
                        <td class="left">
                            <?php if ($sort == 'c.partner_kod') { ?>
                              <a href="<?php echo $sort_partner_kod; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_partner_kod; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_partner_kod; ?>"><?php echo $column_partner_kod; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ($sap == 1) { ?>
                    <?php if ($this->config->get('sale_atkuldve')) { ?>
                        <td class="left"><?php if ($sort == 'c.atkuldve') { ?>
                                <a href="<?php echo $sort_atkuldve; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_atkuldve; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_atkuldve; ?>"><?php echo $column_atkuldve; ?></a>
                            <?php } ?>
                        </td>
                    <?php }?>
                <?php }?>

                <?php if ($this->config->get('sale_company')) { ?>
                    <td class="left">
                        <?php if ($sort == 'c.company') { ?>
                            <a href="<?php echo $sort_company; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_company; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_company; ?>"><?php echo $column_company; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>
                <?php if ($this->config->get('sale_szazalek')) { ?>
                    <td class="left">
                        <?php if ($sort == 'c.szazalek') { ?>
                            <a href="<?php echo $sort_szazalek; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_szazalek; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_szazalek; ?>"><?php echo $column_szazalek; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>
                <?php if ($this->config->get('sale_fizetesi_mod')) { ?>
                    <td class="left"><?php echo $column_fizetesi_mod; ?></td>
                <?php } ?>


                <!--<td class="left"><?php echo $column_payemail; ?></td>-->
                <?php if ($this->config->get('sale_belepes')) { ?>
                    <td class="left"><?php echo $column_login; ?></td>
                <?php } ?>

                <td class="right"><?php echo $column_action; ?></td>

            </tr>
          </thead>

          <tbody>
            <tr class="filter">
              <td></td>
                <?php if ($this->config->get('sale_name')) { ?>
                    <td><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" /></td>
                <?php } ?>

                <?php if ($this->config->get('sale_email')) { ?>
                    <td><input type="text" name="filter_email" value="<?php echo $filter_email; ?>" /></td>
                <?php } ?>

                <?php if ($this->config->get('sale_customer_group')) { ?>
                    <td>
                        <select name="filter_customer_group_id">
                            <option value="*"></option>
                            <?php foreach ($customer_groups as $customer_group) { ?>
                                <?php if ($customer_group['customer_group_id'] == $filter_customer_group_id) { ?>
                                    <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('sale_status')) { ?>
                    <td>
                        <select name="filter_status">
                            <option value="*"></option>
                            <?php if ($filter_status) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                            <?php } ?>
                            <?php if (!is_null($filter_status) && !$filter_status) { ?>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                                <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('sale_approved')) { ?>
                    <td>
                        <select name="filter_approved">
                            <option value="*"></option>
                            <?php if ($filter_approved) { ?>
                                <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                            <?php } else { ?>
                                <option value="1"><?php echo $text_yes; ?></option>
                            <?php } ?>
                            <?php if (!is_null($filter_approved) && !$filter_approved) { ?>
                                <option value="0" selected="selected"><?php echo $text_no; ?></option>
                            <?php } else { ?>
                                <option value="0"><?php echo $text_no; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('sale_ip')) { ?>
                    <td><input type="text" name="filter_ip" value="<?php echo $filter_ip; ?>" /></td>
                <?php } ?>

                <?php if ($this->config->get('sale_date_added')) { ?>
                    <td><input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" id="date" /></td>
                <?php } ?>

                <?php if ($this->config->get('sale_partner_kod')) { ?>
                    <td><input type="text" name="filter_partner_kod" value="<?php echo $filter_partner_kod; ?>" /></td>
                <?php } ?>


                <?php if ($sap == 1) {?>
                    <?php if ($this->config->get('sale_atkuldve')) { ?>
                        <td></td>
                    <?php } ?>
                <?php }?>

                <?php if ($this->config->get('sale_company')) { ?>
                    <td><input type="text" name="filter_company" value="<?php echo $filter_company; ?>" /></td>
                <?php } ?>

                <?php if ($this->config->get('sale_szazalek')) { ?>
                    <td><input type="text" name="filter_szazalek" value="<?php echo $filter_szazalek; ?>" /></td>
                <?php } ?>

                <?php if ($this->config->get('sale_fizetesi_mod')) { ?>
                    <td></td>
                <?php } ?>

                <?php if ($this->config->get('sale_belepes')) { ?>
                    <td></td>
                <?php } ?>

                <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
            </tr>


            <?php if ($customers) { ?>
                <?php foreach ($customers as $customer) { ?>
                    <tr>
                        <td style="text-align: center;">
                            <?php if ($customer['selected']) { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $customer['customer_id']; ?>" checked="checked" />
                            <?php } else { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $customer['customer_id']; ?>" />
                            <?php } ?>
                        </td>
                        <?php if ($this->config->get('sale_name')) { ?>
                            <td class="left"><?php echo $customer['name']; ?></td>
                        <?php } ?>

                        <?php if ($this->config->get('sale_email')) { ?>
                            <td class="left"><?php echo $customer['email']; ?></td>
                        <?php } ?>

                        <?php if ($this->config->get('sale_customer_group')) { ?>
                            <td class="left"><?php echo $customer['customer_group']; ?></td>
                        <?php } ?>

                        <?php if ($this->config->get('sale_status')) { ?>
                            <td class="left"><?php echo $customer['status']; ?></td>
                        <?php } ?>

                        <?php if ($this->config->get('sale_approved')) { ?>
                            <td class="left"><?php echo $customer['approved']; ?></td>
                        <?php } ?>

                        <?php if ($this->config->get('sale_ip')) { ?>
                            <td class="left"><?php echo $customer['ip']; ?></td>
                        <?php } ?>

                        <?php if ($this->config->get('sale_date_added')) { ?>
                            <td class="left"><?php echo $customer['date_added']; ?></td>
                        <?php } ?>

                        <!--<td class="left"><?php echo $customer['paypalemail']; ?></td>-->



                        <?php if (!empty($megjelenit_vevok['partner_kod'])) { ?>
                            <?php if ($this->config->get('sale_partner_kod')) { ?>
                                <td class="left">
                                    <span class="list_ertek" onclick="listElemValueModify(this)">
                                        <?php echo $customer['partner_kod']; ?>
                                    </span>
                                    <span class="list_modify" style="display: none">
                                        <input type="text" name="partner_kod" value="<?php echo $customer['partner_kod']; ?>" modositas="<?php echo $customer['partner_kod_event']?>">
                                    </span>
                                </td>
                            <?php } ?>
                        <?php }?>

                        <?php if ($sap == 1) {?>
                            <?php if ($this->config->get('sale_atkuldve')) { ?>
                                <td class="left"><?php echo $customer['atkuldve']; ?></td>
                            <?php } ?>
                        <?php }?>

                        <?php if ($this->config->get('sale_company')) { ?>
                            <td class="left"><?php echo $customer['company']; ?></td>
                        <?php } ?>
                        <?php if ($this->config->get('sale_szazalek')) { ?>
                            <td class="left"><?php echo $customer['szazalek']; ?></td>
                        <?php } ?>
                        <?php if ($this->config->get('sale_fizetesi_mod')) { ?>
                            <td class="left"><?php echo $customer['fizetesi_mod']; ?></td>
                        <?php } ?>

                        <?php if ($this->config->get('sale_belepes')) { ?>
                            <td class="left">
                                <select onchange="((this.value !== '') ? window.open('index.php?route=sale/customer/login&token=<?php echo $token; ?>&customer_id=<?php echo $customer['customer_id']; ?>&store_id=' + this.value) : null); this.value = '';">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <option value="0"><?php echo $text_default; ?></option>
                                    <?php foreach ($stores as $store) { ?>
                                        <option value="<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        <?php } ?>

                        <td class="right">
                            <?php foreach ($customer['action'] as $action) { ?>
                                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--

    function oszlopBealit() {
        var url = 'index.php?route=sale/customer/oszlopok&token=<?php echo $token?>';

        oszlopok = [];
        $('.customer_list_oszlop input:checked').each(function(element){
            oszlopok.push(this.name);
        });

        location='index.php?route=sale/customer/oszlopok&token=<?php echo $token?>'+'&oszlopok=' + oszlopok.join(',');

    }

function filter() {
	url = 'index.php?route=sale/customer&token=<?php echo $token; ?>';
	
	var filter_name = $('input[name=\'filter_name\']').attr('value');
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}
	
	var filter_email = $('input[name=\'filter_email\']').attr('value');
	if (filter_email) {
		url += '&filter_email=' + encodeURIComponent(filter_email);
	}

    if ($('select[name=\'filter_customer_group_id\']').length > 0) {
        var filter_customer_group_id = $('select[name=\'filter_customer_group_id\']').attr('value');
        if (filter_customer_group_id != '*') {
            url += '&filter_customer_group_id=' + encodeURIComponent(filter_customer_group_id);
        }
	}

    if ($('select[name=\'filter_status\']').length > 0) {
        var filter_status = $('select[name=\'filter_status\']').attr('value');
        if (filter_status != '*') {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }
    }

    if ($('select[name=\'filter_approved\']').length > 0) {
        var filter_approved = $('select[name=\'filter_approved\']').attr('value');
        if (filter_approved != '*') {
            url += '&filter_approved=' + encodeURIComponent(filter_approved);
        }
	}

	var filter_ip = $('input[name=\'filter_ip\']').attr('value');
	if (filter_ip) {
		url += '&filter_ip=' + encodeURIComponent(filter_ip);
	}
		
	var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}

    var filter_partner_kod = $('input[name=\'filter_partner_kod\']').attr('value');
    if (filter_partner_kod) {
        url += '&filter_partner_kod=' + encodeURIComponent(filter_partner_kod);
    }

    var filter_company = $('input[name=\'filter_company\']').attr('value');
    if (filter_company) {
        url += '&filter_company=' + encodeURIComponent(filter_company);
    }

    var filter_szazalek = $('input[name=\'filter_szazalek\']').attr('value');
    if (filter_szazalek) {
        url += '&filter_szazalek=' + encodeURIComponent(filter_szazalek);
    }
	location = url;
}
//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('#date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>

<script>
    function csvSzures() {
        if ($('#csv_beallitas').css("display") == "none") {
            $('#csv_beallitas').slideDown(500);
        } else {
            $('#csv_beallitas').slideUp(500);
        }
    }

    function csvIndit() {

        url = 'index.php?route=tool/csv_export/csvFeltetel&token=<?php echo $token; ?>';

        var datarendelestol = '';
        var datarendelesig  = '';
        var datadatumtol    = $('.datadatumtol').val();
        var datadatumig     = $('.datadatumig').val();

        var feltetel = "";
        var szuro = "";
        var van = false;

        $('#csv_beallitas').slideUp(500);
        van_vevo = false;
        van = false;

        if (datarendelestol.length > 0 || datarendelesig.length > 0 || datadatumtol.length > 0 || datadatumig.length > 0) {
            var feltetel = " WHERE ";
            if(datarendelestol.length > 0) {
                van_vevo = true;
                feltetel += "<?php echo DB_PREFIX?>customer.customer_id >= "+datarendelestol;
            }
            if(datarendelesig.length > 0) {
                if (van_vevo) {
                    feltetel += " AND ";
                }
                feltetel += "<?php echo DB_PREFIX?>customer.customer_id <= "+datarendelesig;
            }
            if(datadatumtol.length > 0) {
                van = true;
                if(van_vevo) {
                    feltetel += " AND ";
                }
                feltetel += "<?php echo DB_PREFIX?>customer.date_added >= '"+datadatumtol+"'";
            }
            if(datadatumig.length > 0) {
                if (van) {
                    feltetel += " AND ";
                }
                feltetel += "<?php echo DB_PREFIX?>customer.date_added <= '"+datadatumig+"'";
            }

        }

        location = url+'&feltetel='+feltetel+'&export_csoport=<?php echo $megjelenit_vevok['csv_vevok_csoportnev']?>';


    }


    key_escape = false;
    aktualis_kivalaztott = '';
    eredet_ertek = '';
    aktualis_list_elem = '';

    $(document).ready(function(){
        uzenofal_torles = setTimeout(" $('.warning, .success').slideUp('slow')",5000);
    });

    $('.list_modify input, .list_modify select').focusout(function() {
        clearTimeout(uzenofal_torles);

        if (key_escape) {
            $('.warning, .success').slideUp('slow',function(){
                $('.success, .warning, .error').remove();
            });

            $(this).val(eredet_ertek);

            $(this).parent().slideUp("slow",function(){
                $(this).prev().css('display','block');
            });


        } else {
            if ($(this).prop('nodeName') == "SELECT") {
                var html = $(this).find('option:selected').html();

            } else {
                var html = this.value;

            }
            $(this).parent().prev().html(html);


            $(this).parent().slideUp("slow",function(){
                $(this).prev().css('display','block');

            });


            var eljaras = $(this).attr('modositas');
            var ertek   = this.value;

            if (eredet_ertek != this.value) {
                $.ajax({
                    url: 'index.php?route=sale/customer/modosit&token=<?php echo $token; ?>',
                    type: "post",
                    data: 'eljaras='+ eljaras+'&ertek='+ertek,

                    dataType: 'json',

                    success: function(json) {
                        $('.success, .warning, .error').remove();
                        if (json['success']) {
                            var html = '<div class="success" style="display: none;">'+json['success']+'</div>';
                            $('.breadcrumb').after(html);
                            $('.success').slideDown('slow');

                        }

                        if (json['error']) {
                            var html = '<div class="warning" style="display: none;">'+json['error']+'</div>';
                            $('.breadcrumb').after(html);
                            $('.warning').slideDown('slow');
                            $(aktualis_kivalaztott).val(eredet_ertek);
                            $(aktualis_list_elem).html(aktualis_list_ertek);
                        }
                        uzenofal_torles = setTimeout(" $('.warning, .success').slideUp('slow')",5000);

                    },
                    error: function(e) {
                    }


                });
            }
        }
    });

    function listElemValueModify(aktualis) {
        $('body').off('click');
        var td_width = $(aktualis).closest('td').width();
        if ($('.list_modify[style*="display: block"]').length == 0) {
            key_escape = false;
            $(aktualis).closest('td').width(td_width);

            $(aktualis).css('display','none');
            $('.list_modify').css('display','none');

            $(aktualis).next().slideDown('slow',function(){
                $(aktualis).next().children().focus();
            }).css("display","block");

            eredet_ertek = $(aktualis).next().children().val();
            aktualis_kivalaztott = $(aktualis).next().children();
            aktualis_list_elem = $(aktualis);
            aktualis_list_ertek = $(aktualis).html();

        } else {
            $('.list_modify').slideUp('slow',function(){
                $(this).prev().css('display','block');
            });
        }
    }
    $('body').on('click', function(e){
        var element_neve = $(e.target).prop('nodeName');
        if (element_neve != "SELECT" && element_neve != "OPTION" && element_neve != "INPUT" && $(e.target).attr('class') != 'list_ertek') {
            $('.list_modify').slideUp('slow',function(){
                $(this).prev().css('display','block');
            });
        }
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            key_escape = true;
            $(aktualis_kivalaztott).blur();

        } else if (e.keyCode == 13) {
            key_escape = false;
            $(aktualis_kivalaztott).blur();
        }
    });
</script>

<?php echo $footer; ?> 