<?php

// Heading
$_['heading_title'] = 'Kívánságlistám';

// Text
$_['text_account']  = 'Fiók';
$_['text_instock']  = 'Raktáron';
$_['text_success']  = 'Sikeresen hozzáadva <a href="%s">%s</a> az Ön <a href="%s">kívánságlistájához</a>!';
$_['text_wishlist'] = 'Kívánságlista (%s)';
$_['text_login']    = 'Önnek <a href="%s">be kell lépnie</a> vagy létre kell hoznia egy <a href="%s">új fiókot</a>, hogy hozzá tudja adni a választott terméket ( <a href="%s">%s</a> ) a <a href="%s"> Kívánságlistájához</a>!';
$_['text_empty']    = 'A kívánságlistája üres!';
$_['text_remove']   = 'Sikeresen módosította kívánságlistáját!';

// Column
$_['column_remove'] = 'Eltávolítás';
$_['column_image']  = 'Kép';
$_['column_name']   = 'Terméknév';
$_['column_model']  = 'Modell';
$_['column_stock']  = 'Raktárkészlet';
$_['column_price']  = 'Darabár';
$_['column_cart']   = 'Megveszem';
$_['column_action']   = 'Művelet';
$_['column_cikkszam'] = 'Cikkszám';
?>
