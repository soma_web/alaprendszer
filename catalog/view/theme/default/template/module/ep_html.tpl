<?php if ($box_status) { ?>
<div class="box">
    <?php if (isset($heading) && $heading != null) {?>
        <div class="box-heading"><?php echo $heading; ?></div>
    <?php } ?>
    <div class="box-content"><?php echo $message; ?></div>
</div>
<?php } else { ?>
<div class="welcome"><?php echo $heading; ?></div>
<?php echo $message; ?>
<?php } ?>