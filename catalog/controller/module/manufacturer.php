<?php  
class ControllerModuleManufacturer extends Controller {
	protected function index($setting) {

        $this->language->load('product/manufacturer');

        $this->load->model('catalog/manufacturer');

        $this->data['heading_title'] = $this->language->get('heading_title');

        $results = $this->model_catalog_manufacturer->getManufacturers();
        $this->data['manufacturers'] = $this->config->rendezes($results,'sort_order');
        $this->data['fejlec'] = $this->config->get("manufacturer_module_fejlec") == 1 ? true : false;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/manufacturer.tpl';
		} else {
			$this->template = 'default/template/module/manufacturer.tpl';
		}
		
		$this->render();
	}
}
?>