<?php  
class ControllerModuletgheliossocial extends Controller {
	protected function index() {
		$this->language->load('module/tg_helios_social');
		
    	$this->data['heading_title'] = $this->language->get('heading_title');
    	
		$this->data['text_facebook'] 		= $this->language->get('text_facebook');
    	$this->data['text_twitter']		= $this->language->get('text_twitter');
		$this->data['text_vimeo']	= $this->language->get('text_vimeo');
		$this->data['text_googletalk']	= $this->language->get('text_googletalk');
		$this->data['text_yahoomessenger']	= $this->language->get('text_yahoomessenger');
		$this->data['text_skype']	= $this->language->get('text_skype');
		$this->data['text_flickr']	= $this->language->get('text_flickr');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/tg_helios_social.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/tg_helios_social.tpl';
		} else {
			$this->template = 'default/template/module/tg_helios_social.tpl';
		}
		
		$this->render();
	}
}
?>