<?php
// Heading
$_['heading_title']       = 'Felnőtt oldal';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeresen módosította a Felnőtt oldal modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Taratalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_or']             = 'VAGY';
$_['text_image']          = 'Kép';
$_['text_color']          = 'Szín';
$_['text_browse']         = 'Fájlok tallózása';
$_['text_clear']          = 'Kép törlése';
$_['text_image_manager']  = 'Kép kezelő';


// Entry
$_['entry_image']         	   = 'Kép:<br /><span class="help">(nincs átméretezés)</span>';
$_['entry_pattern']       	   = 'Minta (nincs átméretezés)<br /><span class="help">megjeleníti, ha a kép nem fedi le a teljes képernyőt</span>';
$_['entry_bgcolor']            = 'Háttér szín<br /><span class="help">megjeleníti, ha a kép kisebb mint a teljes képernyő, és átméretezés nincs megadva</span>';
$_['entry_timer']              = 'Visszaszámláló<br /><span class="help">ennyi másodpercig jelenik meg (alapértelmezett 2 másodperc)</span>';
$_['entry_url']                = 'URL<br /><span class="help">csak ezen az URL-en jelenjen meg<br /> Pl: product/product&product_id=50</span>';
$_['entry_layout']             = 'Elrendezés:';
$_['entry_position']           = 'Pozíció:';
$_['entry_status']             = 'Állapot:';
$_['entry_sort_order']         = 'Sorrend:';

// Error
$_['error_permission']         = 'Figyelem: Önnek nincs jogosultsága módosítani a fiók modult!';;
$_['error_image']              = 'Nincs kép feltöltve a Felnőtt oldal modulhoz!';
$_['error_url']                = 'URL megadott másik elrendezése!';
?>