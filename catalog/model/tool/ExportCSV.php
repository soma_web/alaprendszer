<?php

class ModeltoolExportCSV {
    /**
     * @var array $header
     */
    private $header;

    /**
     * @var array $datas
     */
    private $datas;

    /**
     * @var array $generatedCSV
     */
    private $generatedCSV;
    private $isGenerated = false;

    private $separator = ';';
    private $needQuotes = true;

    public function setHeader($header) {
        $this->header = $header;
    }

    public function setDatas($data) {
        $this->datas = $data;
    }

    public function generateCSV() {
        $generated = array();
        if($this->needQuotes) {
            $newHeader = array();
            foreach($this->header as $key=>$value) {
                $newHeader[$key] = '"'.$value.'"';
            }
            $header = implode($this->separator, $newHeader)."\n";
        } else {
            $header = implode($this->separator, $this->header)."\n";
        }

        $generated[] = $header;

        foreach($this->datas as $dataNum=>$data) {
            $line = array();
            foreach($this->header as $key=>$foo) {
                if(array_key_exists($key, $data)) {
                    if($this->needQuotes) {
                        $line[$key] = '"'.$data[$key].'"';
                    } else {
                        $line[$key] = $data[$key];
                    }
                } else {
                    if($this->needQuotes) {
                        $line[$key] = '""';
                    } else {
                        $line[$key] = '';
                    }
                }
            }
            $generated[] = implode($this->separator, $line)."\n";
        }

        $this->generatedCSV = $generated;
        $this->isGenerated = true;
    }

    public function sendToOutput() {
        if(!$this->isGenerated) {
            $this->generateCSV();
        }
        #ob_clean();
        #ob_flush();
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="report.csv"');
        foreach($this->generatedCSV as $line) {
            echo $line;
        }
        #ob_end_flush();
    }

    public function initialize($header, $body) {
        $this->setHeader($header);
        $this->setDatas($body);
        $this->sendToOutput();
    }
}

?>