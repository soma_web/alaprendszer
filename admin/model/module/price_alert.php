<?php
class ModelModulePriceAlert extends Model {
	public function createTables() {
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "product_price_alert` (
				  `alert_id` int(11) NOT NULL AUTO_INCREMENT,
				  `name` varchar(255) NOT NULL,
				  `email` varchar(255) NOT NULL,
				  `product_id` int(11) NOT NULL,
				  `product_price` decimal(15,4) NOT NULL,
				  `desired_price` decimal(15,4) NOT NULL,
				  `currency_code` varchar(3) NOT NULL,
				  `customer_group_id` int(11) NOT NULL,
				  `language_id` int(11) NOT NULL,
				  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				  PRIMARY KEY (`alert_id`)
				) DEFAULT CHARSET=utf8;";
		
		$this->db->query($sql);	
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "product_price_alert_history` (
				  `history_id` int(11) NOT NULL AUTO_INCREMENT,
				  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
				  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
				  `email_message` text NOT NULL,
				  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				  PRIMARY KEY (`history_id`)
				) DEFAULT CHARSET=utf8;";
						
		$this->db->query($sql);
	}
	
	public function removeTables() {
		
	}
	
	public function getPriceAlerts($data = array()) {
		$sql = "SELECT ppa.*, pd.name as product_name FROM " . DB_PREFIX . "product_price_alert ppa
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (ppa.product_id = pd.product_id)
				WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		if (!empty($data['filter_customer'])){
			$sql .= " AND upper(ppa.name) LIKE '%" . $this->db->escape(strtoupper($data['filter_customer'])) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND UPPER(ppa.email) LIKE '" . $this->db->escape(strtoupper($data['filter_email'])) . "%'";
		}
		
		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}
		
		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(ppa.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		$sort_data = array(
			'ppa.alert_id',
			'ppa.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY ppa.alert_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;	
	} 
	
	public function getTotalPriceAlerts($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_price_alert ppa
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (ppa.product_id = pd.product_id)
				WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		if (!empty($data['filter_customer'])){
			$sql .= " AND upper(ppa.name) LIKE '%" . $this->db->escape(strtoupper($data['filter_customer'])) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND UPPER(ppa.email) LIKE '" . $this->db->escape(strtoupper($data['filter_email'])) . "%'";
		}
		
		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}
		
		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(ppa.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];	
	}

	public function getPriceAlertHistory($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "product_price_alert_history WHERE 1"; 
		
		if (!empty($data['filter_customer'])){
			$sql .= " AND upper(name) LIKE '%" . $this->db->escape(strtoupper($data['filter_customer'])) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND UPPER(email) LIKE '" . $this->db->escape(strtoupper($data['filter_email'])) . "%'";
		}
		
		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		$sort_data = array(
			'history_id',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY history_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;	
	} 
	
	public function getTotalPriceAlertHistory($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_price_alert_history WHERE 1";
		
		if (!empty($data['filter_customer'])){
			$sql .= " AND upper(name) LIKE '%" . $this->db->escape(strtoupper($data['filter_customer'])) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND UPPER(email) LIKE '" . $this->db->escape(strtoupper($data['filter_email'])) . "%'";
		}
				
		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];	
	}	
	
	public function getHistory($history_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_price_alert_history WHERE history_id='" . (int)$history_id . "'");
	
		return $query->row;
	}
}
?>