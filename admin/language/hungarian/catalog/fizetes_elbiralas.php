<?php

// Heading
$_['heading_title']       = 'Fizetés elbírálás státusz';
$_['button_insert']       = 'Beszúrás';
$_['button_delete']       = 'Törlés';
$_['error_warning']       = '';
$_['success']             = '';
$_['column_megnevezes']   = 'Fizetés elbírálás státusz';
$_['column_sort_order']   = 'Sorrend';
$_['column_action']       = 'Művelet';
$_['column_status']       = 'Státusz';


$_['entry_status']          = 'Státusz:';
$_['entry_sorrend']         = 'Sorrend:';

$_['entry_teljesitett']     = 'Engedélyezett állapot:';
$_['entry_teljesitett_lista'] = ' [Engedélyezett állapot]';

$_['entry_alapertelmezett_lista'] = ' [Alapértelmezett állapot]';
$_['entry_alapertelmezett'] = 'Alapértelmezett állapot:';

$_['text_engedelyezett']    = 'Engedélyezett';
$_['text_letiltott']        = 'Letiltott';

?>