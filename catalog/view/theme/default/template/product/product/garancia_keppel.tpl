<?php if (isset($text_garancia) && $text_garancia && isset($garancia_kep) && $garancia_kep) { ?>

   <?php
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/'.$garancia_kep)) {
            $garancia_kep = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/'.$garancia_kep;
        } elseif(file_exists(DIR_TEMPLATE_IMAGE.'default/image/'.$garancia_kep)) {
            $garancia_kep = DIR_TEMPLATE_IMAGE.'default/image/'.$garancia_kep;
        }
    ?>

    <?php if ($garancia_description) { ?>
        <div class="garancia_kep">
            <a class="various " href="#garancia_product">
                <img style="width: 200px;" src="<?php echo $garancia_kep; ?>" title="<?php echo $text_garancia ?>" alt="<?php echo $text_garancia ?>" />
            </a>
        </div>

        <div id="garancia_product" class=""
             style="display: none; min-width: 320; min-height: 150px; background-color: white;">
            <?php echo $garancia_description?>
        </div>

    <?php } else { ?>
        <div class="garancia_kep">
            <a style="cursor: default">
                <img style="width: 200px;" src="<?php echo $garancia_kep; ?>" title="<?php echo $text_garancia ?>" alt="<?php echo $text_garancia ?>" />
            </a>
        </div>
    <?php } ?>


<?php } ?>