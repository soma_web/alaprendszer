<?php

// Heading
$_['heading_title']         = 'Árajánlatok';
$_['heading_modosit']       = 'Árajánlat';
$_['heading_arajanlat']     = 'ÁRAJÁNLAT';
$_['heading_azonosito']     = 'Azonositó:';
$_['heading_neve']          = 'Neve:';

$_['column_azonosito']      = 'ID';
$_['column_customer']       = 'Vevő';
$_['column_name']           = 'Ajánlat neve';
$_['column_action']         = 'Művelet';
$_['column_date_added']     = 'Létrehozás';
$_['column_date_modified']  = 'Utolsó módosítás';
$_['column_arajanlat_keszito_adatai']  = 'Árajánlat fejléc adatai';
$_['column_arajanlat_kero_adatai']  = 'Árajánlat kérő adatai';

$_['text_account']          = 'Fiók';
$_['text_page']             = 'Oldal';
$_['text_delete']           = 'A törlés nem vonható vissza! Biztos, hogy ezt akarja?';


$_['column_customer_email']     = 'Email';

$_['column_megyseg'] 	        = 'ME';
$_['column_listaar'] 	        = 'Listaár';
$_['column_engedmeny_szazalek'] = 'Eng.%';
$_['column_engedmeny_ertek'] 	= 'Eng. egységár';
$_['column_ertek'] 	= 'Érték';


$_['column_kep']            = 'Kép';
$_['column_nev']            = 'Név';
$_['column_model']          = 'Modell';
$_['column_mennyiseg']      = 'Mennyiség';
$_['column_egysegar']       = 'Egységár';
$_['column_ar_osszesen']    = 'Összesen';
$_['column_total_osszesen'] = 'Total összesen';

$_['button_filter']         = 'Szűrés';
$_['button_copy']           = 'Másolás';
$_['button_save']           = 'Mentés';
$_['button_cancel']         = 'Mégse';
$_['button_szerkeszt']      = 'Szerkeszt';
$_['button_edit']           = 'Módosít';
$_['button_pdf']            = 'PDF letöltése';




$_['fejlec_szekhely']           = 'Székhely';
$_['fejlec_adoszam']            = 'Adószám:';
$_['fejlec_bank']               = 'Bank:';
$_['fejlec_bank_szamla']        = 'Bankszámla:';
$_['fejlec_swift_kod']          = 'Bank SWIFT kódja:';
$_['fejlec_iban_kod']           = 'Bank IBAN kódja:';

$_['fejlec_ajanlat_szam']       = 'Árajánlat száma:';
$_['fejlec_ajanlat_kelt']       = 'Árajánlat kelte:';
$_['fejlec_szallitasi_hatarido']= 'Szállítási határidő:';
$_['fejlec_fizetesi_hatarido']  = 'Fizetési határidő:';
$_['fejlec_fizetesi_mod']       = 'Fizetési mód';
$_['fejlec_szallito']           = 'Szállító';
$_['fejlec_vevo']               = 'Vevő';

$_['fejlec_kuldo']              = 'Küldő:';
$_['fejlec_telefon']            = 'Telefon:';
$_['fejlec_email']              = 'Email:';





$_['error_uritsd_elobb']    = 'Árajánlat kérés folyamatban van. Szerkesztés előtt kérem, fejezze be! ';

// Text
$_['text_no_results']       = 'Önnek nincsenek árajánlat kérései!';
$_['text_success']          = 'Sikerült! Az árajánlat betöltve!  Módosíthatja az <span style="cursor: pointer; text-decoration: underline" onclick="ajanlatKeresMegtekintes()">Ajánlat kérésem</span> menüpontban.';
$_['text_success_update']   = 'Sikerült! A ajánlat módosítása megtörtént!';
$_['text_success_copy']   = 'Sikerült! A ajánlat másolása megtörtént!';
$_['text_success_delete']   = 'Sikerült! A kijelölt termék(ek) törlése megtörtént!';

?>
