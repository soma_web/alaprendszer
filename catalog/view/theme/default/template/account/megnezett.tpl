<?php echo $header; ?>


    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>


    <div class="box">


        <div class="account-heading">
            <h1><img src="catalog/view/theme/default/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="nyomtat">
                <a onclick="JelentesNyomtat();" class="button"><?php echo $button_nyomtatas; ?></a>
                <a href='<?php echo $csv_be; ?>' class="button"><?php echo $button_exportalas; ?></a>
                <a onclick="location = '<?php echo $reset; ?>';" class="button"><?php echo $button_reset; ?></a>
            </div>
        </div>


        <div class="">
            <table class="list">
                <thead>
                <tr>
                    <td class="left"><?php echo $column_name; ?></td>
                    <?php if ($this->config->get('megjelenit_form_admin_model') == 1) { ?>
                        <td class="left"><?php echo $column_model; ?></td>
                    <?php } ?>
                    <td class="right"><?php echo $column_viewed; ?></td>
                    <td class="right"><?php echo $column_percent; ?></td>
                </tr>
                </thead>
                <tbody>
                <?php if ($products) { ?>
                    <?php foreach ($products as $product) { ?>
                        <tr>
                            <td class="left"><?php echo $product['name']; ?></td>
                            <?php if ($this->config->get('megjelenit_form_admin_model') == 1) { ?>
                                <td class="left"><?php echo $product['model']; ?></td>
                            <?php } ?>
                            <td class="right"><?php echo $product['viewed']; ?></td>
                            <td class="right"><?php echo $product['percent']; ?></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
    </div>

</div>


<?php echo $footer; ?>