<?php
// Heading 
$_['modal_title'] = 'Price Alert';

// Text
$_['text_info']  = 'To be informed when product price will go below the desired price please fill form below.'; 
$_['text_set_alert']  = 'Set alert'; 
$_['text_success']    = 'Price alert has been saved.'; 

// Entry
$_['entry_name'] = 'Name:';
$_['entry_email'] = 'Email:';
$_['entry_desired_price'] = 'Desired price:';

// Error
$_['error_name']           = 'Name must be at least 3 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_desired_price']  = 'Incorect price!';
?>