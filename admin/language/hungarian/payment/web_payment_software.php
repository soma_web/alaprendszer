<?php

// Heading
$_['heading_title']             = 'Webes Fizetés Software';

// Text
$_['text_payment']              = 'Fizetés';
$_['text_success']              = 'Siker: A Webes Fizetés Software részleteinek módosítása megtörtént!';
$_['text_web_payment_software'] = '<a onclick="window.open(\'http://www.web-payment-software.com/\');"><img src="view/image/payment/wps-logo.jpg" alt="Web Payment Software" title="Web Payment Software" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test']                 = 'Teszt';
$_['text_live']                 = 'Élő';
$_['text_authorization']        = 'Meghatalmazás';
$_['text_capture']              = 'Elfogás';

// Entry
$_['entry_login']               = 'Kereskedői ID:';
$_['entry_key']                 = 'Kereskedői kulcs:';
$_['entry_mode']                = 'Tranzakcsiós mód:';
$_['entry_method']              = 'Tranzakciós metódus:';
$_['entry_total']               = 'Összeg:<br /><span class="help">A pénztárban a rendelés értékének el kell érnie ezt az összeget, hogy aktívvá váljon!</span>';
$_['entry_order_status']        = 'Rendelés állapota:';
$_['entry_geo_zone']            = 'Földrajzi zóna:';
$_['entry_status']              = 'Állapot:';
$_['entry_sort_order']          = 'Sorrend:';

// Error
$_['error_permission']          = 'Figyelmeztetés: Webes Fizetés Software módosítása az Ön számára nem engedélyezett!';
$_['error_login']               = 'Bejelentkezési ID szükséges!';
$_['error_key']                 = 'Tranzakciós kulcs szükséges!';
?>