<?php
class ControllerModuleLandingPage extends Controller {

    public function index() {

     //protected function index($setting) {
		 $this->load->model('localisation/language');
		 $this->language->load('module/landing_page');
		
		 $languages = $this->model_localisation_language->getLanguages();
		 $this->data['heading_title'] = $this->language->get('heading_title');

		 $this->data['code'] = html_entity_decode($this->config->get('landing_page_code_' . $this->config->get('config_language_id')));


		 if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/landing_page.tpl')) {
		 	 $this->template = $this->config->get('config_template') . '/template/module/landing_page.tpl';
		 } else {
		 	 $this->template = 'default/template/module/landing_page.tpl';
		 }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );
        $this->response->setOutput($this->render());
	}
}
?>
