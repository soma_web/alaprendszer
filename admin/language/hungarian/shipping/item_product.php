<?php

// Heading
$_['heading_title']         = 'Árucikk vezérelt szállítási díj';

// Text
$_['text_shipping']         = 'Szállítás';
$_['text_success']          = 'Siker: Az árucikk vezérelt szállítási díj módosítása megtörtént!';
$_['text_magas']            = 'Legmagasabb szállítási díj';
$_['text_alacsony']         = 'Legalacsonyabb  szállítási díj';
$_['text_gls_csomagpont']  = 'GLS Csomagpont megjelenítése';

// Entry
$_['entry_tax']             = 'Adóosztály:';
$_['entry_tax_class']       = 'Adóosztály:';

$_['entry_magas_alacsony']  = 'Legmagasabb / legalacsonyabb<br> termékenkénti szállítási díj:';
$_['entry_geo_zone']        = 'Földrajzi zóna:';
$_['entry_status']          = 'Állapot:';
$_['entry_sort_order']      = 'Sorrend:';
$_['entry_title_heading']   = 'Fejléc megnevezése:';
$_['entry_title_text']      = 'Szállítási mód megnevezése:';
$_['entry_mail']            = 'Egyéb figyelmeztető e-mailek:<br /><span class="help">Egyéb figyelmeztető e-mailek, amiket kapni szeretne, a közponzi áruház  e-mailjei mellett. (vesszővel elválasztva)</span>';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az árucikk vezérelt szállítási díj módosítása az Ön számára nem engedélyezett!';
?>
