<?php
$_['eloleg_afa']          = array(
    'type'              => 'input',
    'name'              => 'eloleg_afa',
    'default'           => "27",
    'heading'           => 'Előleg számla ÁFA-ja:'
);

$_['eloleg_termek']          = array(
    'type'              => 'input',
    'name'              => 'eloleg_termek',
    'default'           => "394507",
    'heading'           => 'Előleg CRM ID:'
);