<?php

// Heading
$_['heading_title']    = 'Kiszerelés mértékegység';

// Text
$_['text_success']     = 'Sikeresen módosította a kiszerelés mértékegységét!';

// Column
$_['column_title']     = 'Kiszerelés mértékegység';
$_['column_unit']      = 'Rövisítés';
$_['column_value']     = 'Érték';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_title']      = 'Kiszerelés mértékegység megnevezése:';
$_['entry_unit']       = 'Rövidítés:';
$_['entry_value']      = 'Érték:<br /><span class="help">Állítsa 1.00000 -re, ha ez az alapértelmezett kiszerelés mértékegység érték.</span>';

// Error
$_['error_permission'] = 'Figyelmeztetés: A kiszerelés mértékegység módosítása az Ön számára nem engedélyezett';
$_['error_title']      = 'Figyelmeztetés: A kiszerelés mértékegység megnevezése legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_unit']       = 'Figyelmeztetés: A iszerelés mértékegység rövidítése legalább 1, és legfeljebb 4 karakterből álljon!';
$_['error_default']    = 'Figyelmeztetés: Ezt a kiszerelés mértékegység nem törölheti, mert jelenleg ezt rendelte hozzá az üzleti alapértelmezett rendelési kiszerelés mértékegységként!';
$_['error_product']    = 'Figyelmeztetés: Ezt a kiszerelés mértékegység nem törölheti, mert jelenleg %s termékhez rendelték hozzá!';
?>
