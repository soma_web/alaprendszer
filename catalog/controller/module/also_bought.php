<?php
/*
+--------------------------------------------------------+
|                   ALSO BOUGHT MODULE					 |
|            Copyright to: www.oc-extensions.com 		 |
|														 |		
|     Become a premium member and you can download all   |
|				the modules with only $99           	 |
+--------------------------------------------------------+
*/

class ControllerModuleAlsoBought extends Controller {
	protected function index($setting=array()) {

        if (count($setting) == 0) {
            $setting          =  $this->config->get('also_bought_module');
            $setting          =  $setting[0];
        }
      	$this->data['heading_title'] = $this->config->get('also_bought_heading_title_'. $this->config->get('config_language_id'));
		
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');

		$this->load->model('module/also_bought');
		
		$this->load->model('tool/image');
		
		$this->data['show_add_to_cart_button'] = $setting['button_cart'];
		
		$this->data['products'] = array();
		
		if (isset($this->request->get['product_id'])){
		
			$data = array(
				'dc_period'            => $this->config->get('also_bought_dc_period') * 60 * 60,
				'filter_product_id'    => $this->request->get['product_id'],
				'order_status_operand' => htmlspecialchars_decode($setting['order_status_operand']),
				'order_status_id'      => $setting['order_status_id'],
				'sort'  			   => $setting['sort'],
				'order' 			   => $setting['sort_type'],
				'start' 			   => 0,
				'limit' 			   => $setting['limit']
			);

			$results = $this->model_module_also_bought->getAlsoBoughtProducts($data);
			
			if ($results){
				foreach ($results as $result) {
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']);
					} else {
						$image = false;
					}
								
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
							
					if ((float)$result['special']) {
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}
					
					if ($this->config->get('config_review_status')) {
						$rating = $result['rating'];
					} else {
						$rating = false;
					}
					
					$this->data['products'][] = array(
						'product_id' => $result['product_id'],
						'thumb'   	 => $image,
						'name'    	 => $result['name'],
						'price'   	 => $price,
						'special' 	 => $special,
						'rating'     => $rating,
						'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
						'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
					);
				}
			}
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/also_bought.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/also_bought.tpl';
		} else {
			$this->template = 'default/template/module/also_bought.tpl';
		}

		$this->render();
	}
}
?>