<?php
class ModelCsvTermekek {

    private $db;
    public $csv_tartalom;

    public function __construct($query) {
        $this->db = $query;
    }

    public function csvBeolvas($data) {

        $kodolas = false;
        if (!empty($data['kodolas_rol'])){
            $kodolas = true;
            if (empty($data['kodolas_ra'])){
                $data['kodolas_ra'] = "UTF-8";
            }
        }

        if ($kodolas) {
            $fc = iconv($data['kodolas_rol'], $data['kodolas_ra'], file_get_contents($data['filename']));
            file_put_contents('import.csv', $fc);
            $handle = fopen('import.csv', "r");
        } else {
            $handle = fopen($data['filename'], "r");
        }

        if ($data['filefejlec'] == 1) {
            $data = fgetcsv($handle, NULL, ";");
        }

        if ($handle){
            while (($sor = fgetcsv($handle, NULL, ";"))) {
                $this->csv_tartalom[] = $sor;
            }
        }
        fclose($handle);
        if ($kodolas) {
            unlink('import.csv');
        }

    }

    public function csvAdatbazisba($data) {

        $szamlalo = 0;
        $szamlalo_bedolgozas = 0;
        foreach($this->csv_tartalom as $csv_sor){
            $szamlalo ++;

            $product_id = $this->productLetezesVizsgalat($csv_sor[7]);
            if ($product_id) {
                $szamlalo_bedolgozas ++;
                    $sql = "UPDATE ".DB_PREFIX."product
                                        SET   cikkszam = '".$csv_sor[8]."',
                                              status = ". ($csv_sor[21]=='igen' ? 1 : 0). "
                                        WHERE   product_id  = '".$product_id."'" ;
                    $query = $this->db->query($sql);
            }
        }



        echo $szamlalo." sorból ".$szamlalo_bedolgozas. " sor bedolgozása megtörtént !";
        //unlink ($data['filename']);

    }



    public function productLetezesVizsgalat($csv_model){

        $vissza = 0;
        $sql = "SELECT product_id FROM ".DB_PREFIX."product WHERE model='".$csv_model."'" ;
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $vissza = $query->row['product_id'];
        }

        return $vissza;
    }

    public function nameLetezesVizsgalat($product_id, $language_id){

        $vissza = 0;
        $sql = "SELECT `name` FROM ".DB_PREFIX."product_description WHERE product_id='".$product_id."' AND language_id='".$language_id."'" ;
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $vissza = $query->row['name'];
        }

        return $vissza;
    }




}
?>
