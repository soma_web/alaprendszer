<?php if(isset($template['rovid']) && $template['rovid'] == 1) { ?>
<div class="pirosatmenet">
    <?php if (!$logged) { ?>
        <?php echo $text_regisztral; ?>
    <?php } else { ?>
        <?php echo $text_logged_minosegbe; ?>
    <?php } ?>

</div>
<?php } else { ?>
<div class="regist">
    <?php if (!$logged) { ?>
        <?php echo $text_meg_nem_reg; ?><?php echo $text_regisztral_csalogat; ?>
    <?php } else { ?>
        <?php echo $text_logged_minosegbe; ?>
    <?php } ?>
</div>
<?php } ?>