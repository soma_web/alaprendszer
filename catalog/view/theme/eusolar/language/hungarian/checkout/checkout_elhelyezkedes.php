<?php
// Heading
$_['heading_title']                             = 'Pénztár';
$_['heading_nyomtat']                           = 'Letöltés';

// Text
$_['text_cart']                                 = 'Bevásárlókosár';
$_['text_brutto']                               = 'Bruttó';
$_['text_netto']                                = 'Nettó';
$_['text_checkout_account']                     = '1. lépés: Fiók &amp; Számlázási név';
$_['text_checkout_payment_address']             = '1. lépés: Számlázási adatok';
$_['text_checkout_payment_method']              = '2. lépés: Fizetési mód';
$_['text_checkout_confirm']                     = '3. lépés: Megrendelés rögzítése';
$_['text_checkout_confirm_no_payment']          = '3. lépés: Megrendelés rögzítése';
$_['text_modify']                               = 'Módosítás &raquo;';
$_['text_new_customer']                         = 'Új vásárló';
$_['text_returning_customer']                   = 'Visszatérő vásárló';
$_['text_checkout']                             = 'Pénztár beállítások:';
$_['text_i_am_returning_customer']              = 'Visszatérő vásáló vagyok';
$_['text_register']                             = 'Fiók regisztrálása';
$_['text_guest']                                = 'Regisztráció nélküli vásárlás';
$_['text_register_account']                     = 'Fiók létrehozásával kényelmesebben és gyorsabban tud vásárolni, naprakész lehet a rendelés(ek) állapotával és nyomon követheti az előző megrendeléseit.';
$_['text_forgotten']                            = 'Elfelejtett jelszó';
$_['text_logged']                               = 'Ön be van jelentkezve, mint <a href="%s">%s</a> <b>(</b> <a href="%s">Kijelentkezés</a> <b>)</b>';
$_['text_items']                                = '%s tétel(ek) - %s';
$_['text_your_details']                         = 'Az Ön személyes adatai';
$_['text_your_address']                         = 'Az Ön címe';
$_['text_your_password']                        = 'Jelszava';
$_['text_agree']                                = 'Elolvastam és megértettem az <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>-et és az <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>ot!';
$_['text_agree_2']                              = 'Elolvastam és megértettem a <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>et';
$_['text_address_new']                          = 'Új címet szeretnék használni';
$_['text_address_existing']                     = 'A meglévő címet szeretném használni';
$_['text_shipping_method']                      = 'Kérem válassza ki a rendelés szállítási módját.';
$_['text_shipping_method_no_payment']           = 'Kérem válassza ki a rendelés szállítási és fizetési módját.';
$_['text_payment_method']                       = 'Kérem válassza ki a rendelés fizetési módját.';
$_['text_comments']                             = 'Megjegyzések hozzáadása';
$_['text_checkou_nyomtat']                      = '<span style="color: #df190a">Letöltés / nyomtatás</span>';
$_['total_title']                               = 'Összesen:';
$_['text_termekeim']                            = 'Termékeim';


// Column
$_['column_name']                    = 'Terméknév';
$_['column_model']                   = 'Modell';
$_['column_quantity']                = 'Mennyiség';
$_['column_price']                   = 'Ár';
$_['column_total']                   = 'Mindösszesen';
$_['column_cikkszam']	             = 'Cikkszam';

// Entry
$_['entry_email_address']            = 'E-Mail cím:';
$_['entry_email']                    = 'E-Mail:';
$_['entry_email_again']              = 'E-Mail cím megerősítése:';
$_['entry_password']                 = 'Jelszó:';
$_['entry_confirm']                  = 'Jelszó ellenőrzése:';
$_['entry_firstname']                = 'Vezetéknév:';
$_['entry_lastname']                 = 'Keresztnév:';
$_['entry_telephone']                = 'Telefon:';
$_['entry_fax']                      = 'Fax:';
$_['entry_company']                  = 'Cég:';
$_['entry_adoszam']     = 'Adószám:';

$_['entry_address_1']                = 'Utca, házszám:';
$_['entry_address_2']                = 'Kiegészítő címadatok::';
$_['entry_postcode']                 = 'Irányítószám:';
$_['entry_city']                     = 'Város:';
$_['entry_country']                  = 'Ország:';
$_['entry_zone']                     = 'Állam / Megye:';
$_['entry_newsletter']               = 'Kérem a %s hírlevelet.';
$_['entry_shipping'] 	             = 'A számlázási és a szállítási címek megegyeznek.';
$_['entry_adoszam']                  = 'Adószám:';

$_['entry_letoltesek']               = 'Kiválasztott kuponok letöltése / nyomtatása:';
$_['entry_letoltesek_kep']           = 'A nyomtatási kép méretén, beállításain kérjük ne változtasson.';
$_['button_letoltes']                = 'Letöltés PDF formátumban';
$_['button_nyomtatas']               = 'Nyomtatás';

// Error
$_['error_warning']                  = 'Hiba történt a rendelés felvétele közben! Ha a probléma továbbla is fennáll kérem válasszon másik fizetési módot, vagy <a href="%s">lépjen kapcsolatba velünk</a>.';
$_['error_login']                    = 'Figyelem: Az E-mail cím és/vagy a jelszó nem megfelelő.';
$_['error_exists']                   = 'Figyelem: Ez az E-Mail cím, már be van regisztrálva! Próbálja meg a jelszóemlékeztetőt!';
$_['error_firstname']                = 'A vezetéknév legalább 1 és legfeljebb 32 karakterből állhat!';
$_['error_lastname']                 = 'A keresztnév legalább 1 és legfeljebb 32 karakterből állhat';
$_['error_email']                    = 'Az E-Mail cím nem megfelelő!';
$_['error_telephone']                = 'Az telefonszám legalább 3 és legfeljebb 32 karakterből állhat!';
$_['error_password']                 = 'Az jelszó legalább 3 és legfeljebb 20 karakterből állhat!';
$_['error_confirm']                  = 'A jelszó megerősítése nem egyezik meg a jelszóval!';
$_['error_address_1']                = 'Az Utca, házszám legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_city']                     = 'A város legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_postcode']                 = 'Az irányítószám legalább 2 és legfeljebb 10 karakterből állhat!';
$_['error_country']                  = 'Válasszon országot!';
$_['error_zone']		             = 'Válasszon megyét!';
$_['error_agree']                    = 'Figyelem: El kell fogadnia a %s-t!';
$_['error_address']                  = 'Figyelem: Ki kell választania a címet!';
$_['error_shipping']                 = 'Figyelem: Ki kell választania a szállítási módot!';
$_['error_no_shipping']              = 'Nem állnak rendelkezésre szállítási módok. Kérem, a hiba javítása érdekében <a href="%s">lépjen kacsolatba velünk</a>!';
$_['error_payment']                  = 'Figyelem: Meg kell adni a fizetési módot!';
$_['error_no_payment']               = 'Nem állnak rendelkezésre fizetési módok. Kérem, a hiba javítása érdekében <a href="%s">lépjen kacsolatba velünk</a> for assistance!';

$_['select_ferfi']         = 'Férfi';
$_['select_no']            = 'Nő';
$_['entry_nem']            = 'Neme:';
$_['entry_eletkor']        = 'Életkor:';
$_['error_eletkor']        = 'Kérem válassza ki az életkorát';
$_['error_nem']            = 'Kérem válassza ki a nemét';
$_['entry_iskolai_vegzettseg']        = 'Iskolai végzettség:';
$_['error_iskolai_vegzettseg']            = 'Kérem válassza ki az iskolai végzettségét';
$_['entry_vallalkozasi_forma']      = 'Vállalkozási forma:';
$_['entry_szekhely']                = 'Székhely:';
$_['entry_ugyvezeto_neve']          = 'Ügyvezető neve:';
$_['entry_ugyvezeto_telefonszama']  = 'Ügyvezető telefonszáma:';

?>