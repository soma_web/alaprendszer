<?php
class ControllerCatalogProduct extends Controller {
	private $error = array();

  	public function index() {
		$this->load->language('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $this->model_catalog_category->categoryPath();

        $this->getList();
  	}

  	public function oszlopok() {

        if (isset($this->request->request['oszlopok']) ) {
            $oszlopok = explode(',',$this->request->request['oszlopok']);
            $beallit = array();
            foreach($oszlopok as $oszlop) {
                $beallit[$oszlop] = 1;
            }
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('product_list', $beallit);
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'], 'SSL'));

        }


        $this->load->language('catalog/product');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $this->model_catalog_category->categoryPath();

        $this->getList();

    }

  	public function insert() {
    	$this->load->language('catalog/product');

    	$this->document->setTitle($this->language->get('heading_title'));
        //$this->data['megjelenit_admin_ujdonsag'] = $this->config->get('megjelenit_admin_ujdonsag');

        $this->load->model('catalog/product');

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_product->addProduct($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . $this->request->get['filter_model'];
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    	}

    	$this->getForm();
  	}

    public function insertcustomer() {
        $this->load->language('catalog/product');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_product->addProduct($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . $this->request->get['filter_name'];
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . $this->request->get['filter_model'];
            }

            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . $this->request->get['filter_customer'];
            }

            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getFormCustomer();
    }


    public function customerproducts() {
        $this->language->load('catalog/customerproduct');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_model'])) {
            $filter_model = $this->request->get['filter_model'];
        } else {
            $filter_model = null;
        }

        if (isset($this->request->get['filter_price'])) {
            $filter_price = $this->request->get['filter_price'];
        } else {
            $filter_price = null;
        }

        if (isset($this->request->get['filter_customer'])) {
            $filter_customer = $this->request->get['filter_customer'];
        } else {
            $filter_customer = null;
        }

        if (isset($this->request->get['filter_quantity'])) {
            $filter_quantity = $this->request->get['filter_quantity'];
        } else {
            $filter_quantity = null;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
        } else {
            $filter_status = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'pd.name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_price'])) {
            $url .= '&filter_price=' . $this->request->get['filter_price'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . $this->request->get['filter_customer'];
        }

        if (isset($this->request->get['filter_quantity'])) {
            $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['insert'] = $this->url->link('catalog/product/insertcustomer', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['copy'] = $this->url->link('catalog/product/copycustomer', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['delete'] = $this->url->link('catalog/product/deletecustomer', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['products'] = array();

        $data = array(
            'filter_name'	  => $filter_name,
            'filter_model'	  => $filter_model,
            'filter_price'	  => $filter_price,
            'filter_customer' => $filter_customer,
            'filter_quantity' => $filter_quantity,
            'filter_status'   => $filter_status,
            'sort'            => $sort,
            'order'           => $order,
            'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'           => $this->config->get('config_admin_limit')
        );

        $this->load->model('tool/image');

        $product_total = $this->model_catalog_product->getTotalCustomerProducts($data);

        $results = $this->model_catalog_product->getCustomerProducts($data);

        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => $this->url->link('catalog/product/updatecustomer', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
            );

            if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
                $image = $this->model_tool_image->resize($result['image'], 40, 40);
            } else {
                $image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
            }

            $special = false;

            $product_specials = $this->model_catalog_product->getProductSpecials($result['product_id']);

            foreach ($product_specials  as $product_special) {
                if (($product_special['date_start'] == '0000-00-00' || $product_special['date_start'] < date('Y-m-d')) && ($product_special['date_end'] == '0000-00-00' || $product_special['date_end'] > date('Y-m-d'))) {
                    $special = $product_special['price'];

                    break;
                }
            }

            $this->data['products'][] = array(
                'product_id' => $result['product_id'],
                'name'       => $result['name'],
                'customer'   => $result['firstname']." ".$result['lastname'],
                'model'      => $result['model'],
                'price'      => $result['price'],
                'special'    => $special,
                'image'      => $image,
                'quantity'   => $result['quantity'],
                'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
                //'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
                'selected'   => isset($this->request->post['selected']) && in_array($result['product_id'], $this->request->post['selected']),
                'action'     => $action
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');

        $this->data['column_product_id']= $this->language->get('column_product_id');
        $this->data['column_image']     = $this->language->get('column_image');
        $this->data['column_name']      = $this->language->get('column_name');
        $this->data['column_model']     = $this->language->get('column_model');
        $this->data['column_price']     = $this->language->get('column_price');
        $this->data['column_customer']  = $this->language->get('column_customer');
        $this->data['column_quantity']  = $this->language->get('column_quantity');
        $this->data['column_status']    = $this->language->get('column_status');
        $this->data['column_action']    = $this->language->get('column_action');

        $this->data['button_copy'] = $this->language->get('button_copy');
        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['button_filter'] = $this->language->get('button_filter');
        $this->data['column_category'] = $this->language->get('column_category');


        $this->data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_price'])) {
            $url .= '&filter_price=' . $this->request->get['filter_price'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . $this->request->get['filter_customer'];
        }

        if (isset($this->request->get['filter_quantity'])) {
            $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['sort_name'] = $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
        $this->data['sort_model'] = $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, 'SSL');
        $this->data['sort_price'] = $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, 'SSL');
        $this->data['sort_customer'] = $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . '&sort=c.firstname' . $url, 'SSL');
        $this->data['sort_quantity'] = $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, 'SSL');
        $this->data['sort_status'] = $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
        $this->data['sort_order'] = $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_price'])) {
            $url .= '&filter_price=' . $this->request->get['filter_price'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . $this->request->get['filter_customer'];
        }

        if (isset($this->request->get['filter_quantity'])) {
            $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['filter_name']      = $filter_name;
        $this->data['filter_model']     = $filter_model;
        $this->data['filter_customer']  = $filter_customer;
        $this->data['filter_price']     = $filter_price;
        $this->data['filter_quantity']  = $filter_quantity;
        $this->data['filter_status']    = $filter_status;

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;

        $this->template = 'catalog/customer_product_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

  	public function update() {
    	$this->load->language('catalog/product');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');
        $this->data['megjelenit_admin_ujdonsag'] = $this->config->get('megjelenit_admin_ujdonsag');


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_product->editProduct($this->request->get['product_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . $this->request->get['filter_model'];
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

//filter category start//
            if (isset($this->request->get['filter_category'])) {
                $url .= '&filter_category=' . $this->request->get['filter_category'];
            }
//filter category end//

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

    	$this->getForm();
  	}

    public function updatecustomer() {
    	$this->load->language('catalog/customerproduct');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_product->editProduct($this->request->get['product_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . $this->request->get['filter_model'];
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

//filter category start//
            if (isset($this->request->get['filter_category'])) {
                $url .= '&filter_category=' . $this->request->get['filter_category'];
            }
//filter category end//

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

            if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . $this->request->get['filter_customer'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

    	$this->getFormcustomer();
  	}

  	public function delete() {
    	$this->load->language('catalog/product');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_catalog_product->deleteProduct($product_id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . $this->request->get['filter_model'];
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

    	$this->getList();
  	}

    public function deletecustomer() {
        $this->load->language('catalog/customerproduct');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $product_id) {
                $this->model_catalog_product->deleteProduct($product_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . $this->request->get['filter_name'];
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . $this->request->get['filter_model'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . $this->request->get['filter_customer'];
            }

            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->customerproducts();
    }

  	public function copy() {
    	$this->load->language('catalog/product');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');

		if (isset($this->request->post['selected']) && $this->validateCopy()) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_catalog_product->copyProduct($product_id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . $this->request->get['filter_model'];
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

    	$this->getList();
  	}

    public function copycustomer() {
        $this->load->language('catalog/customerproduct');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');

        if (isset($this->request->post['selected']) && $this->validateCopy()) {
            foreach ($this->request->post['selected'] as $product_id) {
                $this->model_catalog_product->copyProduct($product_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . $this->request->get['filter_name'];
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . $this->request->get['filter_model'];
            }

            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . $this->request->get['filter_customer'];
            }

            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->customerproducts();
    }

  	private function getList() {


        if (isset($_POST["kepekrogzit"])){
            $kepneve= $_FILES['userfile']['name'];
            $para1=basename($_FILES['userfile']['name']);
            $patad = explode ( "." , $para1 );
            for ($i=0; isset($patad[$i]) && !empty($patad[$i]); $i++){}
            if ($i>1)
                $kiter=$patad[$i-1];
            else
                $kiter= "";

            if (!empty($kiter) && strtoupper($kiter)=="CSV"){
                if ($_FILES['userfile']['size'] == 0){
                    $this->error['warning'] = 'Hiba! Üres file.';

                } else{
                    $felvesz="../csv/jellemzok_import";
                    if (!is_dir($felvesz))
                        mkdir($felvesz);

                    $str1 =$kepneve;
                    $maxfilesize = 12000000;
                    $filedir = $felvesz;

                    if($_FILES['userfile']['size'] < $maxfilesize){
                        if (is_uploaded_file($_FILES['userfile']['tmp_name'])){
                            if (!file_exists($filedir . '/' . $str1)){
                                if (move_uploaded_file($_FILES['userfile']['tmp_name'],$filedir.'/'.$str1)){
                                    chmod($filedir.'/'.$str1, 0777);
                                    $this->session->data['success'] = 'Sikerült! A file feltöltése befejeződött.';

                                } else{
                                    $this->error['warning'] = 'Hiba! A feltöltés nem sikerült.';
                                }
                            } else {
                                $this->error['warning'] = 'Hiba! Az adott néven már van feltöltött CSV file.';

                            }
                        } else{
                            $this->error['warning'] = 'Hiba! A feltöltés nem sikerült.';

                        }
                    } else {
                        $this->error['warning'] = 'Hiba! A file mérete nagyobb a megengedettnél.';
                    }
                }
            } else {
                $this->error['warning'] = 'Hiba! Nem CSV file.';

            }
        }

        $this->data['megjelenit_admin_altalanos'] = $this->config->get('megjelenit_admin_altalanos');

        $this->data['csv_files'] = array();
        if (is_dir(DIR_ARUHAZ. 'csv/jellemzok_import')) {
            $csv_files = glob(DIR_ARUHAZ . 'csv/jellemzok_import/*.csv');
        }

        if ( isset($csv_files) && count($csv_files) > 0) {
            $this->data['csv_files'] =  $csv_files;
        }

        if (count($this->data['csv_files']) == 0) {
            $this->data['csv_files'] = false;
        }

		$filter_product_id  = isset($this->request->get['filter_product_id'])   ? $this->request->get['filter_product_id'] : null;
		$filter_name        = isset($this->request->get['filter_name'])         ? $this->request->get['filter_name'] : null;
        $filter_modify_date = isset($this->request->get['filter_modify_date'])  ? $this->request->get['filter_modify_date'] : null;
		$filter_date_added  = isset($this->request->get['filter_date_added'])   ? $this->request->get['filter_date_added'] : null;
        $filter_model       = isset($this->request->get['filter_model'])        ? $this->request->get['filter_model'] : null;
        $filter_cikkszam    = isset($this->request->get['filter_cikkszam'])     ? $this->request->get['filter_cikkszam'] : null;
		$filter_cikkszam2   = isset($this->request->get['filter_cikkszam2'])    ? $this->request->get['filter_cikkszam2'] : null;
		$filter_kifuto      = isset($this->request->get['filter_kifuto'])       ? $this->request->get['filter_kifuto'] : null;
		$filter_ujdonsag    = isset($this->request->get['filter_ujdonsag'])     ? $this->request->get['filter_ujdonsag'] : null;
		$filter_elorendeles = isset($this->request->get['filter_elorendeles'])  ? $this->request->get['filter_elorendeles'] : null;
		$filter_megrendelem = isset($this->request->get['filter_megrendelem'])  ? $this->request->get['filter_megrendelem'] : null;
		$filter_ar_tiltasa  = isset($this->request->get['filter_ar_tiltasa'])   ? $this->request->get['filter_ar_tiltasa'] : null;
		$filter_garancia_ertek = isset($this->request->get['filter_garancia_ertek']) ? $this->request->get['filter_garancia_ertek'] : null;
		$filter_price       = isset($this->request->get['filter_price'])        ? $this->request->get['filter_price'] : null;
        $filter_category    = isset($this->request->get['filter_category'])     ? $this->request->get['filter_category'] : null;
        $filter_quantity    = isset($this->request->get['filter_quantity'])     ? $this->request->get['filter_quantity'] : null;
		$filter_status      = isset($this->request->get['filter_status'])       ? $this->request->get['filter_status'] : null;
        $sort               = isset($this->request->get['sort'])                ? $this->request->get['sort'] : 'pd.name';
        $order              = isset($this->request->get['order'])               ? $this->request->get['order'] : 'ASC';
        $page               = isset($this->request->get['page'])                ? $this->request->get['page'] : '1';


		$url = '';
		$url .=  isset($this->request->get['filter_product_id'])    ? '&filter_product_id=' . $this->request->get['filter_product_id'] : '';
		$url .=  isset($this->request->get['filter_name'])          ? '&filter_name=' . $this->request->get['filter_name'] : '';
		$url .=  isset($this->request->get['filter_modify_date'])   ? '&filter_modify_date=' . $this->request->get['filter_modify_date'] : '';
        $url .=  isset($this->request->get['filter_date_added'])    ? '&filter_date_added=' . $this->request->get['filter_date_added'] : '';
		$url .=  isset($this->request->get['filter_model'])         ? '&filter_model=' . $this->request->get['filter_model'] : '';
		$url .=  isset($this->request->get['filter_cikkszam'])      ? '&filter_cikkszam=' . $this->request->get['filter_cikkszam'] : '';
		$url .=  isset($this->request->get['filter_cikkszam2'])     ? '&filter_cikkszam2=' . $this->request->get['filter_cikkszam2'] : '';
		$url .=  isset($this->request->get['filter_kifuto'])        ? '&filter_kifuto=' . $this->request->get['filter_kifuto'] : '';
		$url .=  isset($this->request->get['filter_ujdonsag'])      ? '&filter_ujdonsag=' . $this->request->get['filter_ujdonsag'] : '';
		$url .=  isset($this->request->get['filter_elorendeles'])   ? '&filter_elorendeles=' . $this->request->get['filter_elorendeles'] : '';
		$url .=  isset($this->request->get['filter_megrendelem'])   ? '&filter_megrendelem=' . $this->request->get['filter_megrendelem'] : '';
		$url .=  isset($this->request->get['filter_ar_tiltasa'])    ? '&filter_ar_tiltasa=' . $this->request->get['filter_ar_tiltasa'] : '';
		$url .=  isset($this->request->get['filter_garancia_ertek'])   ? '&filter_garancia_ertek=' . $this->request->get['filter_garancia_ertek'] : '';
		$url .=  isset($this->request->get['filter_category'])      ? '&filter_category=' . $this->request->get['filter_category'] : '';
		$url .=  isset($this->request->get['filter_quantity'])      ? '&filter_quantity=' . $this->request->get['filter_quantity'] : '';
        $url .=  isset($this->request->get['filter_price'])         ? '&filter_price=' . $this->request->get['filter_price'] : '';
		$url .=  isset($this->request->get['filter_status'])        ? '&filter_status=' . $this->request->get['filter_status'] : '';
		$url .=  isset($this->request->get['sort'])                 ? '&sort=' . $this->request->get['sort'] : '';
		$url .=  isset($this->request->get['order'])                ? '&order=' . $this->request->get['order'] : '';
		$url .=  isset($this->request->get['page'])                 ? '&page=' . $this->request->get['page'] : '';



  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);

		$this->data['insert']       = $this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['copy']         = $this->url->link('catalog/product/copy', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete']       = $this->url->link('catalog/product/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['csv_export']   = $this->url->link('catalog/product/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['csv_import']   = $this->url->link('catalog/product/import', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['csvinsert']    = $this->url->link('catalog/product', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['products'] = array();

		$data = array(
			'filter_product_id'	    => $filter_product_id,
			'filter_name'	        => $filter_name,
			'filter_modify_date'	=> $filter_modify_date,
			'filter_date_added'	    => $filter_date_added,
			'filter_model'	        => $filter_model,
			'filter_cikkszam'	    => $filter_cikkszam,
			'filter_cikkszam2'	    => $filter_cikkszam2,
			'filter_kifuto'	        => $filter_kifuto,
			'filter_ujdonsag'	    => $filter_ujdonsag,
			'filter_elorendeles'	=> $filter_elorendeles,
			'filter_megrendelem'	=> $filter_megrendelem,
			'filter_ar_tiltasa'	    => $filter_ar_tiltasa,
			'filter_garancia_ertek'	=> $filter_garancia_ertek,
			'filter_price'	        => $filter_price,
			'filter_quantity'       => $filter_quantity,
			'filter_status'         => $filter_status,
            'filter_category'       => $filter_category,
			'sort'                  => $sort,
			'order'                 => $order,
			'start'                 => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                 => $this->config->get('config_admin_limit')
		);

		$this->load->model('tool/image');

		$product_total = $this->model_catalog_product->getTotalProducts($data);

		$results = $this->model_catalog_product->getProducts($data);

        //filter category start//
        $this->load->model('catalog/category');
        $this->data['categories'] = $this->model_catalog_category->getCategories(0);
        //filter category end//

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/product/update', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
			);
            $package_info = $this->model_catalog_product->getProductPackage($result['product_id']);
            $class = "";
            if (count($package_info) > 0){
                $class = "csomag";
            }


            //filter category start//
            $category =  $this->model_catalog_product->getProductCategories($result['product_id']);
            //filter category end//

			if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
			}

			$special = false;

			$product_specials = $this->model_catalog_product->getProductSpecials($result['product_id']);

			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || $product_special['date_start'] <= date('Y-m-d'))
                    && ($product_special['date_end'] == '0000-00-00' || $product_special['date_end'] >= date('Y-m-d'))) {
					$special = $product_special['price'];

					break;
				}
			}


            if ($result['garancia_egyseg'] == 2) {
                $result['garancia_ertek'] = $result['garancia_ertek'] /12;
            }

            $garancia_egyseg=   $result['garancia_egyseg'] == 1 ?   $this->language->get('entry_garancia_honap') : ($result['garancia_egyseg'] == 2 ? $this->language->get('entry_garancia_ev') : '');

      		$this->data['products'][] = array(
				'product_id'        => $result['product_id'],
				'name'              => $result['name'],
				'date_modified'     => $result['date_modified'],
				'date_added'        => $result['date_added'],
				'model'             => $result['model'],
				'cikkszam'          => $result['cikkszam'],
				'cikkszam2'         => $result['cikkszam2'],
				'kifuto'            => $result['kifuto'],
				'ujdonsag'          => $result['ujdonsag'],
				'elorendeles'       => $result['elorendeles'],
                'elorendeles_event'      => 'UPDATE '.DB_PREFIX.'product set elorendeles=\'modositas_ertek\' WHERE product_id='.$result['product_id'],
                'megrendelem'       => $result['megrendelem'],
				'ar_tiltasa'        => $result['ar_tiltasa'],
                'ar_tiltasa_event'      => 'UPDATE '.DB_PREFIX.'product set ar_tiltasa=\'modositas_ertek\' WHERE product_id='.$result['product_id'],
                'garancia_ertek'    => $result['garancia_ertek'] ? $result['garancia_ertek'] .' ' .$garancia_egyseg : '',
				'price'             => round($result['price'],4),
                'price_event'      => 'UPDATE '.DB_PREFIX.'product set price=\'modositas_ertek\' WHERE product_id='.$result['product_id'],
                'special'           => round($special,4),
                'category'          => $category,
				'image'             => $image,
				'quantity'          => $result['quantity'],
				'status'            => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
                'status_value'      => $result['status'],
                'status_event'      => 'UPDATE '.DB_PREFIX.'product set status=\'modositas_ertek\' WHERE product_id='.$result['product_id'],
				'selected'          => isset($this->request->post['selected']) && in_array($result['product_id'], $this->request->post['selected']),
				'action'            => $action,
				'class'             => $class
			);
    	}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['column_category'] = $this->language->get('column_category');

        $this->data['column_product_id']= $this->language->get('column_product_id');
        $this->data['column_image']         = $this->language->get('column_image');
		$this->data['column_name']          = $this->language->get('column_name');
		$this->data['column_date_modify']   = $this->language->get('column_date_modify');
		$this->data['column_date_added']    = $this->language->get('column_date_added');
		$this->data['column_model']         = $this->language->get('column_model');
		$this->data['column_cikkszam']      = $this->language->get('column_cikkszam');
		$this->data['column_cikkszam2']     = $this->language->get('column_cikkszam2');
		$this->data['column_ujdonsag']      = $this->language->get('column_ujdonsag');
		$this->data['column_elorendeles']   = $this->language->get('column_elorendeles');
		$this->data['column_megrendelem']   = $this->language->get('column_megrendelem');
		$this->data['column_ar_tiltasa']    = $this->language->get('column_ar_tiltasa');
		$this->data['column_kifuto']        = $this->language->get('column_kifuto');
		$this->data['column_garancia_ertek']= $this->language->get('column_garancia_ertek');
		$this->data['column_price']         = $this->language->get('column_price');
		$this->data['column_quantity']      = $this->language->get('column_quantity');
		$this->data['column_status']        = $this->language->get('column_status');
		$this->data['column_action']        = $this->language->get('column_action');

		$this->data['button_copy'] = $this->language->get('button_copy');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_csv_export'] = $this->language->get('button_csv_export');
		$this->data['button_csv_import'] = $this->language->get('button_csv_import');

		$this->data['text_valasszon'] = $this->language->get('text_valasszon');
		$this->data['text_termekjellemzok'] = $this->language->get('text_termekjellemzok');
		$this->data['text_kapcsolodotermekek'] = $this->language->get('text_kapcsolodotermekek');
		$this->data['text_ajanlott_tartozekok'] = $this->language->get('text_ajanlott_tartozekok');
		$this->data['text_altalanos_tartozekok'] = $this->language->get('text_altalanos_tartozekok');
		$this->data['text_engedelyezett'] = $this->language->get('text_engedelyezett');
		$this->data['text_letiltott'] = $this->language->get('text_letiltott');
		$this->data['text_import_valasszon'] = $this->language->get('text_import_valasszon');
		$this->data['text_file_feltoltes'] = $this->language->get('text_file_feltoltes');
		$this->data['text_file_importalas'] = $this->language->get('text_file_importalas');
		$this->data['text_kerem_toltsefel'] = $this->language->get('text_kerem_toltsefel');
		$this->data['text_select'] = $this->language->get('text_select');

		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');

 		$this->data['token'] = $this->session->data['token'];

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];

		} elseif  (isset($this->session->data['warning'])) {
            $this->data['error_warning'] = $this->session->data['warning'];
            unset($this->session->data['warning']);

        } else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

        $url .=  isset($this->request->get['filter_product_id'])    ? '&filter_name=' . $this->request->get['filter_product_id'] : '';
        $url .=  isset($this->request->get['filter_name'])          ? '&filter_name=' . $this->request->get['filter_name'] : '';
        $url .=  isset($this->request->get['filter_modify_date'])   ? '&filter_modify_date=' . $this->request->get['filter_modify_date'] : '';
        $url .=  isset($this->request->get['filter_date_added'])    ? '&filter_date_added=' . $this->request->get['filter_date_added'] : '';
        $url .=  isset($this->request->get['filter_model'])         ? '&filter_model=' . $this->request->get['filter_model'] : '';
        $url .=  isset($this->request->get['filter_cikkszam'])      ? '&filter_cikkszam=' . $this->request->get['filter_cikkszam'] : '';
        $url .=  isset($this->request->get['filter_cikkszam2'])     ? '&filter_cikkszam2=' . $this->request->get['filter_cikkszam2'] : '';
        $url .=  isset($this->request->get['filter_kifuto'])        ? '&filter_kifuto=' . $this->request->get['filter_kifuto'] : '';
        $url .=  isset($this->request->get['filter_ujdonsag'])      ? '&filter_ujdonsag=' . $this->request->get['filter_ujdonsag'] : '';
        $url .=  isset($this->request->get['filter_elorendeles'])   ? '&filter_elorendeles=' . $this->request->get['filter_elorendeles'] : '';
        $url .=  isset($this->request->get['filter_ar_tiltasa'])    ? '&filter_ar_tiltasa=' . $this->request->get['filter_ar_tiltasa'] : '';
        $url .=  isset($this->request->get['filter_garancia_ertek'])? '&filter_garancia_ertek=' . $this->request->get['filter_garancia_ertek'] : '';
        $url .=  isset($this->request->get['filter_category'])      ? '&filter_category=' . $this->request->get['filter_category'] : '';
        $url .=  isset($this->request->get['filter_price'])         ? '&filter_price=' . $this->request->get['filter_price'] : '';
        $url .=  isset($this->request->get['filter_quantity'])      ? '&filter_quantity=' . $this->request->get['filter_quantity'] : '';
        $url .=  isset($this->request->get['filter_status'])        ? '&filter_status=' . $this->request->get['filter_status'] : '';
        $url .=  $order == 'ASC'    ? '&order=DESC' : '&order=ASC';
        $url .=  isset($this->request->get['page'])                 ? '&page=' . $this->request->get['page'] : '';



		$this->data['sort_product_id']  = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.product_id' . $url, 'SSL');
		$this->data['sort_name']        = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$this->data['sort_date_modify'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.date_modified' . $url, 'SSL');
		$this->data['sort_date_added']  = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.date_added' . $url, 'SSL');
		$this->data['sort_model']       = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, 'SSL');
		$this->data['sort_cikkszam']    = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.cikkszam' . $url, 'SSL');
		$this->data['sort_cikkszam2']   = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.cikkszam2' . $url, 'SSL');
		$this->data['sort_kifuto']      = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.kifuto' . $url, 'SSL');
		$this->data['sort_ujdonsag']    = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.ujdonsag' . $url, 'SSL');
		$this->data['sort_elorendeles'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.elorendeles' . $url, 'SSL');
		$this->data['sort_megrendelem'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.megrendelem' . $url, 'SSL');
		$this->data['sort_ar_tiltasa']  = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.ar_tiltasa' . $url, 'SSL');
		$this->data['sort_garancia_ertek']= $this->url->link('catalog/product', 'token='.$this->session->data['token'] . '&sort=p.garancia_ertek' . $url, 'SSL');
		$this->data['sort_price']       = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, 'SSL');
		$this->data['sort_category']    = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p2c.category_id' . $url, 'SSL');
        $this->data['sort_quantity']    = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, 'SSL');
		$this->data['sort_status']      = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
		$this->data['sort_order']       = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');



        $url = '';


        $url .=  isset($this->request->get['filter_product_id'])    ? '&filter_product_id=' . $this->request->get['filter_product_id'] : '';
        $url .=  isset($this->request->get['filter_name'])          ? '&filter_name=' . $this->request->get['filter_name'] : '';
        $url .=  isset($this->request->get['filter_modify_date'])   ? '&filter_modify_date=' . $this->request->get['filter_modify_date'] : '';
        $url .=  isset($this->request->get['filter_date_added'])    ? '&filter_date_added=' . $this->request->get['filter_date_added'] : '';
        $url .=  isset($this->request->get['filter_model'])         ? '&filter_model=' . $this->request->get['filter_model'] : '';
        $url .=  isset($this->request->get['filter_cikkszam'])      ? '&filter_cikkszam=' . $this->request->get['filter_cikkszam'] : '';
        $url .=  isset($this->request->get['filter_cikkszam2'])     ? '&filter_cikkszam2=' . $this->request->get['filter_cikkszam2'] : '';
        $url .=  isset($this->request->get['filter_kifuto'])        ? '&filter_kifuto=' . $this->request->get['filter_kifuto'] : '';
        $url .=  isset($this->request->get['filter_ujdonsag'])      ? '&filter_ujdonsag=' . $this->request->get['filter_ujdonsag'] : '';
        $url .=  isset($this->request->get['filter_elorendeles'])   ? '&filter_elorendeles=' . $this->request->get['filter_elorendeles'] : '';
        $url .=  isset($this->request->get['filter_megrendelem'])   ? '&filter_megrendelem=' . $this->request->get['filter_megrendelem'] : '';
        $url .=  isset($this->request->get['filter_ar_tiltasa'])    ? '&filter_ar_tiltasa=' . $this->request->get['filter_ar_tiltasa'] : '';
        $url .=  isset($this->request->get['filter_garancia_ertek'])? '&filter_garancia_ertek=' . $this->request->get['filter_garancia_ertek'] : '';
        $url .=  isset($this->request->get['filter_category'])      ? '&filter_category=' . $this->request->get['filter_category'] : '';
        $url .=  isset($this->request->get['filter_price'])         ? '&filter_price=' . $this->request->get['filter_price'] : '';
        $url .=  isset($this->request->get['filter_quantity'])      ? '&filter_quantity=' . $this->request->get['filter_quantity'] : '';
        $url .=  isset($this->request->get['filter_status'])        ? '&filter_status=' . $this->request->get['filter_status'] : '';
        $url .=  isset($this->request->get['sort'])                 ? '&sort=' . $this->request->get['sort'] : '';
        $url .=  isset($this->request->get['order'])                ? '&order=' . $this->request->get['order'] : '';


		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_product_id']    = $filter_product_id;
		$this->data['filter_name']          = $filter_name;
		$this->data['filter_modify_date']   = $filter_modify_date;
		$this->data['filter_date_added']    = $filter_date_added;
		$this->data['filter_model']         = $filter_model;
		$this->data['filter_cikkszam']      = $filter_cikkszam;
		$this->data['filter_cikkszam2']     = $filter_cikkszam2;
		$this->data['filter_kifuto']        = $filter_kifuto;
		$this->data['filter_ujdonsag']      = $filter_ujdonsag;
		$this->data['filter_elorendeles']   = $filter_elorendeles;
		$this->data['filter_megrendelem']   = $filter_megrendelem;
		$this->data['filter_ar_tiltasa']    = $filter_ar_tiltasa;
		$this->data['filter_garancia_ertek']  = $filter_garancia_ertek;
        $this->data['filter_category']      = $filter_category;
        $this->data['filter_price']         = $filter_price;
		$this->data['filter_quantity']      = $filter_quantity;
		$this->data['filter_status']        = $filter_status;

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/product_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
  	}

  	private function getForm() {
    	$this->data['heading_title'] = $this->language->get('heading_title');

    	$this->data['text_enabled']         = $this->language->get('text_enabled');
    	$this->data['text_disabled']        = $this->language->get('text_disabled');
    	$this->data['text_none']            = $this->language->get('text_none');
    	$this->data['text_yes']             = $this->language->get('text_yes');
    	$this->data['text_no']              = $this->language->get('text_no');
		$this->data['text_select_all']      = $this->language->get('text_select_all');
		$this->data['text_unselect_all']    = $this->language->get('text_unselect_all');
		$this->data['text_plus']            = $this->language->get('text_plus');
		$this->data['text_minus']           = $this->language->get('text_minus');
		$this->data['text_default']         = $this->language->get('text_default');
		$this->data['text_image_manager']   = $this->language->get('text_image_manager');
		$this->data['text_browse']          = $this->language->get('text_browse');
		$this->data['text_clear']           = $this->language->get('text_clear');
		$this->data['text_option']          = $this->language->get('text_option');
		$this->data['text_option_value']    = $this->language->get('text_option_value');
		$this->data['text_select']          = $this->language->get('text_select');
		$this->data['text_none']            = $this->language->get('text_none');
		$this->data['text_percent']         = $this->language->get('text_percent');
		$this->data['text_amount']          = $this->language->get('text_amount');
        $this->data['entry_filter']         = $this->language->get('entry_filter');
        $this->data['text_kupon_keszit']    = $this->language->get('text_kupon_keszit');
        $this->data['text_general']         = $this->language->get('text_general');
        $this->data['entry_idoszak']        = $this->language->get('entry_idoszak');
        $this->data['text_fizetes_alatt']   = $this->language->get('text_fizetes_alatt');
        $this->data['text_kifizetve']       = $this->language->get('text_kifizetve');
        $this->data['text_crm_tipus']       = $this->language->get('text_crm_tipus');
        $this->data['text_crm_category']    = $this->language->get('text_crm_category');
        $this->data['text_crm_product']       = $this->language->get('text_crm_product');

        $this->data['entry_szazalek_kedvezmeny'] = $this->language->get('entry_szazalek_kedvezmeny');
        $this->data['entry_lizing']          = $this->language->get('entry_lizing');
        $this->data['entry_kifuto']          = $this->language->get('entry_kifuto');
        $this->data['entry_ujdonsag']        = $this->language->get('entry_ujdonsag');
        $this->data['entry_elorendeles']     = $this->language->get('entry_elorendeles');
        $this->data['entry_megrendelem']     = $this->language->get('entry_megrendelem');
        $this->data['entry_ar_tiltasa']      = $this->language->get('entry_ar_tiltasa');
        $this->data['entry_status']          = $this->language->get('entry_status');
		$this->data['entry_name']            = $this->language->get('entry_name');
		$this->data['entry_meta_description']= $this->language->get('entry_meta_description');
		$this->data['entry_meta_keyword']    = $this->language->get('entry_meta_keyword');
		$this->data['entry_description']     = $this->language->get('entry_description');
		$this->data['entry_store']           = $this->language->get('entry_store');
		$this->data['entry_stock_date']      = $this->language->get('entry_stock_date');
		$this->data['entry_keyword']         = $this->language->get('entry_keyword');
    	$this->data['entry_model']           = $this->language->get('entry_model');
    	$this->data['entry_cikkszam']        = $this->language->get('entry_cikkszam');
    	$this->data['entry_cikkszam2']       = $this->language->get('entry_cikkszam2');
		$this->data['entry_sku']             = $this->language->get('entry_sku');
		$this->data['entry_upc']             = $this->language->get('entry_upc');
		$this->data['entry_location']        = $this->language->get('entry_location');
		$this->data['entry_minimum']         = $this->language->get('entry_minimum');
		$this->data['entry_manufacturer']    = $this->language->get('entry_manufacturer');
    	$this->data['entry_shipping']        = $this->language->get('entry_shipping');
    	$this->data['entry_szallitas']       = $this->language->get('entry_szallitas');
    	$this->data['entry_date_available']  = $this->language->get('entry_date_available');
    	$this->data['entry_date_kaphato_ig'] = $this->language->get('entry_date_kaphato_ig');
    	$this->data['entry_quantity']        = $this->language->get('entry_quantity');
    	$this->data['mennyisegi_egyseg']     = $this->language->get('mennyisegi_egyseg');
    	$this->data['csomagolasi_egyseg']    = $this->language->get('csomagolasi_egyseg');
    	$this->data['csomagolasi_mennyiseg'] = $this->language->get('csomagolasi_mennyiseg');
		$this->data['entry_stock_status']    = $this->language->get('entry_stock_status');
    	$this->data['entry_price']           = $this->language->get('entry_price');
    	$this->data['entry_eredeti_ar']     = $this->language->get('entry_eredeti_ar');
    	$this->data['entry_utalvany']       = $this->language->get('entry_utalvany');
    	$this->data['entry_szazalek']       = $this->language->get('entry_szazalek');
		$this->data['entry_tax_class']      = $this->language->get('entry_tax_class');
		$this->data['entry_points']         = $this->language->get('entry_points');
		$this->data['entry_option_points']  = $this->language->get('entry_option_points');
		$this->data['entry_subtract']        = $this->language->get('entry_subtract');
		$this->data['entry_checked']        = $this->language->get('entry_checked');
    	$this->data['entry_weight_class']   = $this->language->get('entry_weight_class');
    	$this->data['entry_weight']         = $this->language->get('entry_weight');
        $this->data['entry_kiszereles']     = $this->language->get('entry_kiszereles');
        $this->data['entry_kiszereles_mertekegyseg'] = $this->language->get('entry_kiszereles_mertekegyseg');
		$this->data['entry_dimension']      = $this->language->get('entry_dimension');
		$this->data['entry_length']         = $this->language->get('entry_length');
    	$this->data['entry_image']          = $this->language->get('entry_image');
        $this->data['entry_video']              = $this->language->get('entry_video');
        $this->data['entry_pdf']              = $this->language->get('entry_pdf');
    	$this->data['entry_image_letoltheto'] = $this->language->get('entry_image_letoltheto');
    	$this->data['entry_download']       = $this->language->get('entry_download');
    	$this->data['entry_category']       = $this->language->get('entry_category');
		$this->data['entry_related']        = $this->language->get('entry_related');
		$this->data['entry_accessory']      = $this->language->get('entry_accessory');
		$this->data['entry_ajandek']        = $this->language->get('entry_ajandek');
		$this->data['entry_general_accessory']        = $this->language->get('entry_general_accessory');
		$this->data['entry_helyettesito']   = $this->language->get('entry_helyettesito');
		$this->data['entry_attribute']      = $this->language->get('entry_attribute');
		$this->data['entry_text']           = $this->language->get('entry_text');
		$this->data['entry_option']         = $this->language->get('entry_option');
		$this->data['entry_option_value']   = $this->language->get('entry_option_value');
		$this->data['entry_required']       = $this->language->get('entry_required');
        $this->data['entry_size_color_required'] = $this->language->get('entry_size_color_required');
        $this->data['entry_sort_order']     = $this->language->get('entry_sort_order');
		$this->data['entry_feldolgozas_status'] = $this->language->get('entry_feldolgozas_status');
        $this->data['entry_fizetendo']      = $this->language->get('entry_fizetendo');
		$this->data['entry_fizetes_status'] = $this->language->get('entry_fizetes_status');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_date_start']     = $this->language->get('entry_date_start');
		$this->data['entry_date_end']       = $this->language->get('entry_date_end');
		$this->data['entry_priority']       = $this->language->get('entry_priority');
		$this->data['entry_tag']            = $this->language->get('entry_tag');
		$this->data['vevo_kedvezmeny']      = $this->language->get('vevo_kedvezmeny');
		$this->data['entry_reward']         = $this->language->get('entry_reward');
		$this->data['entry_layout']         = $this->language->get('entry_layout');
        $this->data['text_piacter']         = $this->language->get('text_piacter');
        $this->data['entry_letoltes']       = $this->language->get('entry_letoltes');
        $this->data['entry_image_download'] = $this->language->get('entry_image_download');
        $this->data['entry_termek_vevo']    = $this->language->get('entry_termek_vevo');
        $this->data['entry_status']         = $this->language->get('entry_status');
        $this->data['entry_kep_tiltas']     = $this->language->get('entry_kep_tiltas');
        $this->data['entry_kiemelt']        = $this->language->get('entry_kiemelt');
        $this->data['entry_short_description']  = $this->language->get('entry_short_description');
        $this->data['entry_test_description']   = $this->language->get('entry_test_description');
        $this->data['entry_video_link']         = $this->language->get('entry_video_link');
        $this->data['entry_pdf_link']           = $this->language->get('entry_pdf_link');
        $this->data['entry_video_title']        = $this->language->get('entry_video_title');
        $this->data['entry_pdf_title']          = $this->language->get('entry_pdf_title');
        $this->data['entry_egyszer_kosarba']    = $this->language->get('entry_egyszer_kosarba');
        $this->data['entry_csomagar_ossz']    = $this->language->get('entry_csomagar_ossz');

        $this->data['entry_elhelyezkedes']  = $this->language->get('entry_elhelyezkedes');
        $this->data['entry_elhelyezkedes_alcsoport'] = $this->language->get('entry_elhelyezkedes_alcsoport');
        $this->data['entry_kiemelt_keret']  = $this->language->get('entry_kiemelt_keret');
        $this->data['entry_idoszak_darab']  = $this->language->get('entry_idoszak_darab');
        $this->data['entry_helyszin']       = $this->language->get('entry_helyszin');
        $this->data['entry_generalt_nev']   = $this->language->get('entry_generalt_nev');
        $this->data['entry_uj']             = $this->language->get('entry_uj');
        $this->data['entry_package']        = $this->language->get('entry_package');

        $this->data['button_save']          = $this->language->get('button_save');
    	$this->data['button_cancel']        = $this->language->get('button_cancel');
		$this->data['button_add_attribute'] = $this->language->get('button_add_attribute');
		$this->data['button_add_option']    = $this->language->get('button_add_option');
		$this->data['button_add_option_value'] = $this->language->get('button_add_option_value');
		$this->data['button_add_discount']  = $this->language->get('button_add_discount');
		$this->data['button_add_vevo']      = $this->language->get('button_add_vevo');
		$this->data['button_add_special']   = $this->language->get('button_add_special');
		$this->data['button_add_image']     = $this->language->get('button_add_image');
        $this->data['button_add_video']         = $this->language->get('button_add_video');
        $this->data['button_add_pdf']           = $this->language->get('button_add_pdf');
		$this->data['button_add_elhelyezkedes'] = $this->language->get('button_add_elhelyezkedes');
		$this->data['button_remove']            = $this->language->get('button_remove');
		$this->data['button_remove_pdf']        = $this->language->get('button_remove_pdf');

		$this->data['entry_garancia']               = $this->language->get('entry_garancia');
		$this->data['entry_garancia_honap']         = $this->language->get('entry_garancia_honap');
		$this->data['entry_garancia_ev']            = $this->language->get('entry_garancia_ev');
        $this->data['entry_garancia_description']   = $this->language->get('entry_garancia_description');
        $this->data['entry_package_alapar']         = $this->language->get('entry_package_alapar');
        $this->data['entry_package_csomagar']       = $this->language->get('entry_package_csomagar');
        $this->data['entry_package_csomagar_ossz']  = $this->language->get('entry_package_csomagar_ossz');
        $this->data['entry_csomagar_akutalizal']  = $this->language->get('entry_csomagar_akutalizal');

    	$this->data['tab_general']          = $this->language->get('tab_general');
    	$this->data['tab_data']             = $this->language->get('tab_data');
		$this->data['tab_attribute']        = $this->language->get('tab_attribute');
		$this->data['tab_option']           = $this->language->get('tab_option');
		$this->data['tab_discount']         = $this->language->get('tab_discount');
		$this->data['tab_vevok']            = $this->language->get('tab_vevok');
		$this->data['tab_special']          = $this->language->get('tab_special');
		$this->data['tab_elhelyezkedes']    = $this->language->get('tab_elhelyezkedes');
    	$this->data['tab_image']            = $this->language->get('tab_image');
        $this->data['tab_videok']           = $this->language->get('tab_videok');
        $this->data['tab_pdf']              = $this->language->get('tab_pdf');
        $this->data['tab_crm_kapcsolat']    = $this->language->get('tab_crm_kapcsolat');
		$this->data['tab_links']            = $this->language->get('tab_links');
		$this->data['tab_reward']           = $this->language->get('tab_reward');
		$this->data['tab_design']           = $this->language->get('tab_design');
        $this->data['tab_kepek_helyszinenkent'] = $this->language->get('tab_kepek_helyszinenkent');


		$this->data['megjelenit_termekadat'] = $this->config->get('megjelenit_admin_termekadatok');
		$this->data['megjelenit_termekful'] = $this->config->get('megjelenit_admin_termekful');
		$this->data['megjelenit_admin_altalanos'] = $this->config->get('megjelenit_admin_altalanos');


        $megjelenit_admin_termekadatok = $this->config->get("megjelenit_admin_termekadatok");
        $this->data['seourl_generalas'] = isset($megjelenit_admin_termekadatok['seourl_generalas']) ? $megjelenit_admin_termekadatok['seourl_generalas'] : 0;

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['name'])) {
            $this->load->model('localisation/language');
            $language_id = $this->model_localisation_language->getLanguageId($this->language->get('code'));
			$this->data['error_name'][$language_id] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}

 		if (isset($this->error['meta_description'])) {
			$this->data['error_meta_description'] = $this->error['meta_description'];
		} else {
			$this->data['error_meta_description'] = array();
		}

   		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = array();
		}

        if (isset($this->error['short_description'])) {
            $this->data['error_short_description'] = $this->error['short_description'];
        } else {
            $this->data['error_short_description'] = array();
        }
        if (isset($this->error['test_description'])) {
            $this->data['error_test_description'] = $this->error['test_description'];
        } else {
            $this->data['error_test_description'] = array();
        }
        if (isset($this->error['garancia_description'])) {
            $this->data['error_garancia_description'] = $this->error['garancia_description'];
        } else {
            $this->data['error_garancia_description'] = array();
        }



        if (isset($this->error['model'])) {
			$this->data['error_model'] = $this->error['model'];
		} else {
			$this->data['error_model'] = '';
		}
        if (isset($this->error['cikkszam'])) {
			$this->data['error_cikkszam'] = $this->error['cikkszam'];
		} else {
			$this->data['error_cikkszam'] = '';
		}
        if (isset($this->error['cikkszam2'])) {
			$this->data['error_cikkszam2'] = $this->error['cikkszam2'];
		} else {
			$this->data['error_cikkszam2'] = '';
		}





        if (isset($this->error['price'])) {
            $this->data['error_price'] = $this->error['price'];
        } else {
            $this->data['error_price'] = '';
        }

        if (isset($this->error['product_filter'])) {
            $this->data['error_product_filter'] = $this->error['product_filter'];
        } else {
            $this->data['error_product_filter'] = '';
        }

        if (isset($this->error['date_ervenyes_ig'])) {
            $this->data['error_date_ervenyes_ig'] = $this->error['date_ervenyes_ig'];
        } else {
            $this->data['error_date_ervenyes_ig'] = '';
        }

        if (isset($this->error['date_available'])) {
            $this->data['error_date_available'] = $this->error['date_available'];
        } else {
            $this->data['error_date_available'] = '';
        }

        $url = '';
        if (isset($this->request->get['filter_category'])) {
            $url .= '&filter_category=' . $this->request->get['filter_category'];
        }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

        if (isset($this->request->get['filter_product_id'])) {
            $url .= '&filter_product_id=' . $this->request->get['filter_product_id'];
        }

        if (isset($this->request->get['filter_modify_date'])) {
            $url .= '&filter_modify_date=' . $this->request->get['filter_modify_date'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . $this->request->get['filter_model'];
		}
        if (isset($this->request->get['filter_cikkszam'])) {
			$url .= '&filter_cikkszam=' . $this->request->get['filter_cikkszam'];
		}
        if (isset($this->request->get['filter_cikkszam2'])) {
			$url .= '&filter_cikkszam2=' . $this->request->get['filter_cikkszam2'];
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);

		if (!isset($this->request->get['product_id'])) {
			$this->data['action'] = $this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/product/update', 'token=' . $this->session->data['token'] . '&product_id=' . $this->request->get['product_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['token'] = $this->session->data['token'];

        $this->data['url'] = $url;
		if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
    	}

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['product_description'])) {
			$this->data['product_description'] = $this->request->post['product_description'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_description'] = $this->model_catalog_product->getProductDescriptions($this->request->get['product_id']);
		} else {
			$this->data['product_description'] = array();
		}
        $this->data['disabled'] = '';

		if (isset($this->request->post['model'])) {
      		$this->data['model'] = $this->request->post['model'];
    	} elseif (!empty($product_info)) {
			$this->data['model'] = $product_info['model'];
		} else {
      		$this->data['model'] = '';
    	}

        if (isset($this->request->post['cikkszam'])) {
            $this->data['cikkszam'] = $this->request->post['cikkszam'];
        } elseif (!empty($product_info)) {
            $this->data['cikkszam'] = $product_info['cikkszam'];
        } else {
            $this->data['cikkszam'] = '';
        }

        if (isset($this->request->post['cikkszam2'])) {
            $this->data['cikkszam2'] = $this->request->post['cikkszam2'];
        } elseif (!empty($product_info)) {
            $this->data['cikkszam2'] = $product_info['cikkszam2'];
        } else {
            $this->data['cikkszam2'] = '';
        }

        if (isset($this->request->post['kifuto'])) {
            $this->data['kifuto'] = $this->request->post['kifuto'];
        } elseif (!empty($product_info)) {
            $this->data['kifuto'] = isset($product_info['kifuto']) ? $product_info['kifuto'] : "";
        } else {
            $this->data['kifuto'] = '';
        }

        if (isset($this->request->post['ujdonsag'])) {
            $this->data['ujdonsag'] = $this->request->post['ujdonsag'];
        } elseif (!empty($product_info)) {
            $this->data['ujdonsag'] = isset($product_info['ujdonsag']) ? $product_info['ujdonsag'] : "";
        } else {
            $this->data['ujdonsag'] = '';
        }

        if (isset($this->request->post['elorendeles'])) {
            $this->data['elorendeles'] = $this->request->post['elorendeles'];
        } elseif (!empty($product_info)) {
            $this->data['elorendeles'] = isset($product_info['elorendeles']) ? $product_info['elorendeles'] : "";
        } else {
            $this->data['elorendeles'] = '';
        }

        if (isset($this->request->post['megrendelem'])) {
            $this->data['megrendelem'] = $this->request->post['megrendelem'];
        } elseif (!empty($product_info)) {
            $this->data['megrendelem'] = isset($product_info['megrendelem']) ? $product_info['megrendelem'] : "";
        } else {
            $this->data['megrendelem'] = '';
        }

        if (isset($this->request->post['ar_tiltasa'])) {
            $this->data['ar_tiltasa'] = $this->request->post['ar_tiltasa'];
        } elseif (!empty($product_info)) {
            $this->data['ar_tiltasa'] = isset($product_info['ar_tiltasa']) ? $product_info['ar_tiltasa'] : "";
        } else {
            $this->data['ar_tiltasa'] = '';
        }

		if (isset($this->request->post['sku'])) {
      		$this->data['sku'] = $this->request->post['sku'];
    	} elseif (!empty($product_info)) {
			$this->data['sku'] = $product_info['sku'];
		} else {
      		$this->data['sku'] = '';
    	}

		if (isset($this->request->post['upc'])) {
      		$this->data['upc'] = $this->request->post['upc'];
    	} elseif (!empty($product_info)) {
			$this->data['upc'] = $product_info['upc'];
		} else {
      		$this->data['upc'] = '';
    	}

		if (isset($this->request->post['location'])) {
      		$this->data['location'] = $this->request->post['location'];
    	} elseif (!empty($product_info)) {
			$this->data['location'] = $product_info['location'];
		} else {
      		$this->data['location'] = '';
    	}

		$this->load->model('setting/store');

		$this->data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['product_store'])) {
			$this->data['product_store'] = $this->request->post['product_store'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_store'] = $this->model_catalog_product->getProductStores($this->request->get['product_id']);
		} else {
			$this->data['product_store'] = array(0);
		}

		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($product_info)) {
			$this->data['keyword'] = $product_info['keyword'];
		} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['product_tag'])) {
			$this->data['product_tag'] = $this->request->post['product_tag'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_tag'] = $this->model_catalog_product->getProductTags($this->request->get['product_id']);
		} else {
			$this->data['product_tag'] = array();
		}

		if (isset($this->request->post['image'])) {
			$this->data['image'] = $this->request->post['image'];
		} elseif (!empty($product_info)) {
			$this->data['image'] = $product_info['image'];
		} else {
			$this->data['image'] = '';
		}

        if (isset($this->request->post['letoltheto_kep'])) {
			$this->data['letoltheto_kep'] = $this->request->post['letoltheto_kep'];
		} elseif (!empty($product_info)) {
			$this->data['letoltheto_kep'] = $product_info['letoltheto'];
		} else {
			$this->data['letoltheto_kep'] = '';
		}

		$this->load->model('tool/image');

		if (!empty($product_info) && $product_info['image'] && file_exists(DIR_IMAGE . $product_info['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
		} else {
			$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (!empty($product_info) && $product_info['letoltheto'] && file_exists(DIR_IMAGE . $product_info['letoltheto'])) {
            $this->data['letoltheto_thumb'] =  $this->model_tool_image->resize($product_info['letoltheto'], 100, 100);
		} else {
			$this->data['letoltheto_thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

		$this->load->model('catalog/manufacturer');

    	$this->data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();

    	if (isset($this->request->post['manufacturer_id'])) {
      		$this->data['manufacturer_id'] = $this->request->post['manufacturer_id'];
		} elseif (!empty($product_info)) {
			$this->data['manufacturer_id'] = $product_info['manufacturer_id'];
		} else {
      		$this->data['manufacturer_id'] = 0;
    	}


        if (isset($this->request->post['lizing'])) {
            $this->data['lizing'] = $this->request->post['lizing'];
        } elseif (!empty($product_info)) {
            $this->data['lizing'] = $product_info['lizing'];
        } else {
            $this->data['lizing'] = 1;
        }

    	if (isset($this->request->post['shipping'])) {
      		$this->data['shipping'] = $this->request->post['shipping'];
    	} elseif (!empty($product_info)) {
      		$this->data['shipping'] = $product_info['shipping'];
    	} else {
			$this->data['shipping'] = 1;
		}

        if (isset($this->request->post['egyedi_szallitas'])) {
      		$this->data['egyedi_szallitas'] = $this->request->post['egyedi_szallitas'];
    	} elseif (!empty($product_info)) {
      		$this->data['egyedi_szallitas'] = $product_info['egyedi_szallitas'];
    	} else {
			$this->data['egyedi_szallitas'] = 0;
		}

        if (isset($this->request->post['egyedi_szallitasi_dij'])) {
            $this->data['egyedi_szallitasi_dij'] = $this->request->post['egyedi_szallitasi_dij'];
        } elseif (!empty($product_info)) {
            $this->data['egyedi_szallitasi_dij'] = $product_info['egyedi_szallitasi_dij'];
        } else {
            $this->data['egyedi_szallitasi_dij'] = 0;
        }


        if (isset($this->request->post['garancia_egyseg'])) {
            $this->data['garancia_egyseg'] = $this->request->post['garancia_egyseg'];
        } elseif (!empty($product_info)) {
            $this->data['garancia_egyseg'] = $product_info['garancia_egyseg'];
        } else {
            $this->data['garancia_egyseg'] = $this->config->get('config_warranty_unit_id');
        }

        if (isset($this->request->post['garancia_ertek'])) {
      		$this->data['garancia_ertek'] = $this->request->post['garancia_ertek'];
    	} elseif (!empty($product_info)) {
      		$this->data['garancia_ertek'] =  $this->data['garancia_egyseg'] == 2 ? $product_info['garancia_ertek']/12 : $product_info['garancia_ertek'];
    	} else {
			$this->data['garancia_ertek'] = $this->config->get('config_warranty');
		}




    	if (isset($this->request->post['price'])) {
      		$this->data['price'] = $this->request->post['price'];
    	} elseif (!empty($product_info)) {
			$this->data['price'] = $product_info['price'];
		} else {
      		$this->data['price'] = '';
    	}

        if (isset($this->request->post['max_szazalek_kedvezmeny'])) {
            $this->data['max_szazalek_kedvezmeny'] = $this->request->post['max_szazalek_kedvezmeny'];
        } elseif (!empty($product_info)) {
            $this->data['max_szazalek_kedvezmeny'] = $product_info['max_szazalek_kedvezmeny'];
        } else {
            $this->data['max_szazalek_kedvezmeny'] = '';
        }

        if (isset($this->request->post['eredeti_ar'])) {
            $this->data['eredeti_ar'] = $this->request->post['eredeti_ar'];
        } elseif (!empty($product_info)) {
            $this->data['eredeti_ar'] = $product_info['eredeti_ar'];
        } else {
            $this->data['eredeti_ar'] = 0;
        }


        if (isset($this->request->post['egyszer_kosarba'])) {
            $this->data['egyszer_kosarba'] = $this->request->post['egyszer_kosarba'];
        } elseif (!empty($product_info)) {
            $this->data['egyszer_kosarba'] = $product_info['egyszer_kosarba'];
        } else {
            $this->data['egyszer_kosarba'] = 0;
        }

        if (isset($this->request->post['szazalek'])) {
            $this->data['szazalek'] = $this->request->post['szazalek'];
        } elseif (!empty($product_info)) {
            $this->data['szazalek'] = $product_info['szazalek'];
        } else {
            $this->data['szazalek'] = '';
        }


        if (isset($this->request->post['utalvany'])) {
            $this->data['utalvany'] = $this->request->post['utalvany'];
        } elseif (!empty($product_info)) {
            $this->data['utalvany'] = $product_info['utalvany'];
        } else {
            $this->data['utalvany'] = 0;
        }

        if (isset($this->request->post['imagedesabled'])) {
            $this->data['imagedesabled'] = $this->request->post['imagedesabled'];
        } elseif (!empty($product_info)) {
            $this->data['imagedesabled'] = $product_info['imagedesabled'];
        } else {
            $this->data['imagedesabled'] = false;
        }

		$this->load->model('localisation/tax_class');

		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		if (isset($this->request->post['tax_class_id'])) {
      		$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
    	} elseif (!empty($product_info)) {
			$this->data['tax_class_id'] = $product_info['tax_class_id'];
		} else {
      		$this->data['tax_class_id'] = 0;
    	}

		if (isset($this->request->post['date_available'])) {
       		$this->data['date_available'] = $this->request->post['date_available'];
		} elseif (!empty($product_info)) {
			$this->data['date_available'] = date('Y-m-d', strtotime($product_info['date_available']));
		} else {
			$this->data['date_available'] = date('Y-m-d', time() - 86400);
		}

		if (isset($this->request->post['date_ervenyes_ig'])) {
       		$this->data['date_ervenyes_ig'] = $this->request->post['date_ervenyes_ig'];
		} elseif (!empty($product_info)) {
			$this->data['date_ervenyes_ig'] = $product_info['date_ervenyes_ig'];
		} else {
			$this->data['date_ervenyes_ig'] = date('Y-m-d');
		}

    	if (isset($this->request->post['quantity'])) {
      		$this->data['quantity'] = $this->request->post['quantity'];
    	} elseif (!empty($product_info)) {
      		$this->data['quantity'] = $product_info['quantity'];
    	} else {
			$this->data['quantity'] = 1;
		}

        if (isset($this->request->post['megyseg'])) {
            $this->data['megyseg'] = $this->request->post['megyseg'];
        } elseif (!empty($product_info)) {
            $this->data['megyseg'] = $product_info['megyseg'];
        } else {
            $this->data['megyseg'] = "darab";
        }

        if (isset($this->request->post['stock_in_date'])) {
            $this->data['stock_in_date'] = $this->request->post['stock_in_date'];
        } elseif (isset($product_info['stock_in_date']) && $product_info['stock_in_date']) {
            $this->data['stock_in_date'] = $product_info['stock_in_date'];
        } else {
            $this->data['stock_in_date'] = "0000-00-00";
        }

        if (isset($this->request->post['csom_egyseg'])) {
            $this->data['csom_egyseg'] = $this->request->post['csom_egyseg'];
        } elseif (!empty($product_info)) {
            $this->data['csom_egyseg'] = $product_info['csomagolasi_egyseg'];
        } else {
            $this->data['csom_egyseg'] = 1;
        }

        if (isset($this->request->post['csom_mennyiseg'])) {
            $this->data['csom_mennyiseg'] = $this->request->post['csom_mennyiseg'];
        } elseif (!empty($product_info)) {
            $this->data['csom_mennyiseg'] = $product_info['csomagolasi_mennyiseg'];
        } else {
            $this->data['csom_mennyiseg'] = 1;
        }

		if (isset($this->request->post['minimum'])) {
      		$this->data['minimum'] = $this->request->post['minimum'];
    	} elseif (!empty($product_info)) {
      		$this->data['minimum'] = $product_info['minimum'];
    	} else {
			$this->data['minimum'] = 1;
		}

		if (isset($this->request->post['subtract'])) {
      		$this->data['subtract'] = $this->request->post['subtract'];
    	} elseif (!empty($product_info)) {
      		$this->data['subtract'] = $product_info['subtract'];
    	} else {
			$this->data['subtract'] = 1;
		}

        if (isset($this->request->post['checked'])) {
            $this->data['checked'] = $this->request->post['checked'];
        } elseif (!empty($product_info['checked'])) {
            $this->data['checked'] = $product_info['checked'];
        } else {
            $this->data['checked'] = 1;
        }

		if (isset($this->request->post['sort_order'])) {
      		$this->data['sort_order'] = $this->request->post['sort_order'];
    	} elseif (!empty($product_info)) {
      		$this->data['sort_order'] = $product_info['sort_order'];
    	} else {
			$this->data['sort_order'] = 1;
		}

		$this->load->model('localisation/stock_status');

		$this->data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

		if (isset($this->request->post['stock_status_id'])) {
      		$this->data['stock_status_id'] = $this->request->post['stock_status_id'];
    	} elseif (!empty($product_info)) {
      		$this->data['stock_status_id'] = $product_info['stock_status_id'];
    	} else {
			$this->data['stock_status_id'] = $this->config->get('config_stock_status_id');
		}

    	if (isset($this->request->post['status'])) {
      		$this->data['status'] = $this->request->post['status'];
    	} elseif (!empty($product_info)) {
			$this->data['status'] = $product_info['status'];
		} else {
      		$this->data['status'] = 1;
    	}

        if (isset($this->request->post['szin_meret_szukseges'])) {
      		$this->data['szin_meret_szukseges'] = $this->request->post['szin_meret_szukseges'];
    	} elseif (!empty($product_info)) {
			$this->data['szin_meret_szukseges'] = $product_info['szin_meret_szukseges'];
		} else {
      		$this->data['szin_meret_szukseges'] = 1;
    	}

    	if (isset($this->request->post['weight'])) {
      		$this->data['weight'] = $this->request->post['weight'];
		} elseif (!empty($product_info)) {
			$this->data['weight'] = $product_info['weight'];
    	} else {
      		$this->data['weight'] = '';
    	}

        if (isset($this->request->post['packaging'])) {
            $this->data['packaging'] = $this->request->post['packaging'];
        } elseif (!empty($product_info['packaging'])) {
            $this->data['packaging'] = $product_info['packaging'];
        } else {
            $this->data['packaging'] = '';
        }

        if (isset($this->request->post['video'])) {
            $this->data['video'] = $this->request->post['video'];
        } elseif (!empty($product_info)) {
            $this->data['video'] = $product_info['video'];
        } else {
            $this->data['video'] = '';
        }

		$this->load->model('localisation/weight_class');

		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

		if (isset($this->request->post['weight_class_id'])) {
      		$this->data['weight_class_id'] = $this->request->post['weight_class_id'];
    	} elseif (!empty($product_info)) {
      		$this->data['weight_class_id'] = $product_info['weight_class_id'];
		} else {
      		$this->data['weight_class_id'] = $this->config->get('config_weight_class_id');
    	}

        $this->load->model('localisation/packaging_class');

        $this->data['packaging_classes'] = $this->model_localisation_packaging_class->getPackagingClasses();

        if (isset($this->request->post['packaging_class_id'])) {
            $this->data['packaging_class_id'] = $this->request->post['packaging_class_id'];
        } elseif (!empty($product_info['packaging_class_id'])) {
            $this->data['packaging_class_id'] = $product_info['packaging_class_id'];
        } else {
            $this->data['packaging_class_id'] = $this->config->get('config_packaging_class_id');
        }

		if (isset($this->request->post['length'])) {
      		$this->data['length'] = $this->request->post['length'];
    	} elseif (!empty($product_info)) {
			$this->data['length'] = $product_info['length'];
		} else {
      		$this->data['length'] = '';
    	}

		if (isset($this->request->post['width'])) {
      		$this->data['width'] = $this->request->post['width'];
		} elseif (!empty($product_info)) {
			$this->data['width'] = $product_info['width'];
    	} else {
      		$this->data['width'] = '';
    	}

		if (isset($this->request->post['height'])) {
      		$this->data['height'] = $this->request->post['height'];
		} elseif (!empty($product_info)) {
			$this->data['height'] = $product_info['height'];
    	} else {
      		$this->data['height'] = '';
    	}

        if (!empty($this->data['megjelenit_termekful']['crm_kapcsolat'])) {
            if (isset($this->request->post['crm_tipus'])) {
                $this->data['crm_tipus'] = $this->request->post['crm_tipus'];
            } elseif (!empty($product_info['crm_tipus'])) {
                $this->data['crm_tipus'] = $product_info['crm_tipus'];
            } else {
                $this->data['crm_tipus'] = '';
            }
            if (isset($this->request->post['crm_category_id'])) {
                $this->data['crm_category_id'] = $this->request->post['crm_category_id'];
            } elseif (!empty($product_info['crm_category_id'])) {
                $this->data['crm_category_id'] = $product_info['crm_category_id'];
            } else {
                $this->data['crm_category_id'] = '';
            }
            if (isset($this->request->post['crm_product_id'])) {
                $this->data['crm_product_id'] = $this->request->post['crm_product_id'];
            } elseif (!empty($product_info['crm_product_id'])) {
                $this->data['crm_product_id'] = $product_info['crm_product_id'];
            } else {
                $this->data['crm_product_id'] = '';
            }
            $this->load->model('catalog/crm_kapcsolat');
            $this->data['crm_products'] = array();
            $this->data['crm_categoryes'] = array();

            if ($this->data['crm_tipus'] == 1) {
                if ($this->data['crm_category_id']) {
                    $this->data['crm_products'] = $this->model_catalog_crm_kapcsolat->getCrmProducts($this->data['crm_category_id']);
                }
                $this->data['crm_categoryes'] = $this->model_catalog_crm_kapcsolat->getCrmProductCategoryes();

            } elseif ($this->data['crm_tipus'] == 2) {
                if ($this->data['crm_category_id']) {
                    $this->data['crm_products'] = $this->model_catalog_crm_kapcsolat->getCrmServices($this->data['crm_category_id']);
                } else {
                    $this->data['crm_products'] = $this->model_catalog_crm_kapcsolat->getCrmServices();
                }
                $this->data['crm_categoryes'] = $this->model_catalog_crm_kapcsolat->getCrmServiceCategoryes();
            }
            elseif ($this->data['crm_tipus'] == 3) {
                    $this->data['crm_products'] = $this->model_catalog_crm_kapcsolat->getCrmCsomag($this->data['crm_category_id']);
            }
        }

		$this->load->model('localisation/length_class');

		$this->data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();

		if (isset($this->request->post['length_class_id'])) {
      		$this->data['length_class_id'] = $this->request->post['length_class_id'];
    	} elseif (!empty($product_info)) {
      		$this->data['length_class_id'] = $product_info['length_class_id'];
    	} else {
      		$this->data['length_class_id'] = $this->config->get('config_length_class_id');
		}

		if (isset($this->request->post['product_attribute'])) {
			$this->data['product_attributes'] = $this->request->post['product_attribute'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_attributes'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
		} else {
			$this->data['product_attributes'] = array();
		}

        $this->load->model('catalog/attribute');
        $results = $this->model_catalog_attribute->getGroupsAttributes();
        foreach($results as $value){
            $attributes_search[$value['attribute_group_id']] = array(
                'group_name'    => $value['attribute_group']
            );
        }
        foreach($results as $value){
            $attributes_search[$value['attribute_group_id']]['attributes'][] = array(
                'attribute_id'  => $value['attribute_id'],
                'name'          => $value['name']
            );
        }
        $this->data['attributes_search'] = isset($attributes_search) ? $attributes_search : '';



		$this->load->model('catalog/option');

		if (isset($this->request->post['product_option'])) {
			$product_options = $this->request->post['product_option'];
		} elseif (isset($this->request->get['product_id'])) {
			$product_options = $this->model_catalog_product->getProductOptions($this->request->get['product_id']);
		} else {
			$product_options = array();
		}

		$this->data['product_options'] = array();

		foreach ($product_options as $product_option) {
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox'
                            || $product_option['type'] == 'image' || $product_option['type'] == 'checkbox_qty') {
				$product_option_value_data = array();

				foreach ($product_option['product_option_value'] as $product_option_value) {
					$product_option_value_data[] = array(
						'product_option_value_id' => $product_option_value['product_option_value_id'],
						'option_value_id'         => $product_option_value['option_value_id'],
						'quantity'                => $product_option_value['quantity'],
						'subtract'                => $product_option_value['subtract'],
						'checked'                 => isset($product_option_value['checked']) ? $product_option_value['checked'] : '',
						'price'                   => $product_option_value['price'],
						'price_prefix'            => $product_option_value['price_prefix'],
						'points'                  => $product_option_value['points'],
						'points_prefix'           => $product_option_value['points_prefix'],
						'weight'                  => $product_option_value['weight'],
						'weight_prefix'           => $product_option_value['weight_prefix']
					);
				}

				$this->data['product_options'][] = array(
					'product_option_id'    => $product_option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $product_option['option_id'],
					'name'                 => $product_option['name'],
					'type'                 => $product_option['type'],
					'required'             => isset($product_option['required']) ? $product_option['required'] : ''
				);
			} else {
				$this->data['product_options'][] = array(
					'product_option_id' => $product_option['product_option_id'],
					'option_id'         => $product_option['option_id'],
					'name'              => $product_option['name'],
					'type'              => $product_option['type'],
					'option_value'      => $product_option['option_value'],
					'required'          => isset($product_option['required']) ? $product_option['required'] : ''
				);
			}
		}

		$this->data['option_values'] = array();

		foreach ($product_options as $product_option) {
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox'
                || $product_option['type'] == 'image' || $product_option['type'] == 'checkbox_qty') {
				if (!isset($this->data['option_values'][$product_option['option_id']])) {
					$this->data['option_values'][$product_option['option_id']] = $this->model_catalog_option->getOptionValues($product_option['option_id']);
				}
			}
		}

		$this->load->model('sale/customer_group');

		$this->data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups();
        $this->load->model('sale/customer');
		$this->data['vevo_groups'] = $this->model_sale_customer->getCustomers();

		if (isset($this->request->post['product_discount'])) {
			$this->data['product_discounts'] = $this->request->post['product_discount'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_discounts'] = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);
		} else {
			$this->data['product_discounts'] = array();
		}

        if (isset($this->request->post['product_vevo'])) {
            $this->data['product_vevok'] = $this->request->post['product_vevo'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_vevok'] = $this->model_catalog_product->getProductVevo($this->request->get['product_id']);
        } else {
            $this->data['product_vevok'] = array();
        }

		if (isset($this->request->post['product_special'])) {
			$this->data['product_specials'] = $this->request->post['product_special'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_specials'] = $this->model_catalog_product->getProductSpecials($this->request->get['product_id']);
		} else {
			$this->data['product_specials'] = array();
		}

        /* Elhelyezkedés -kiemelés -vége */


        $this->load->model('catalog/elhelyezkedes');
        $this->data['elhelyezkedesek_selects'] = $this->model_catalog_elhelyezkedes->getElhelyezkedesek();

        $this->load->model('catalog/elhelyezkedes_alcsoport');
        $this->data['elhelyezkedesek_alcsoport_selects'] = $this->model_catalog_elhelyezkedes_alcsoport->getAlcsoportElhelyezkedesek();

        $this->load->model('catalog/kiemelesek');
        $this->data['kiemelesek_selects'] = $this->model_catalog_kiemelesek->getKiemelesek();

        $this->load->model('catalog/penzugyi_status');
        $this->data['penzugyi_status_selects'] = $this->model_catalog_penzugyi_status->getPenzugyiStatus();

        $this->load->model('catalog/fizetes_elbiralas');
        $this->data['fizetes_elbiralas_selects'] = $this->model_catalog_fizetes_elbiralas->getFizetesElbiralas();


        $idoszakok = array();

        $idoszakok[] = array(
            'id'    => '1',
            'name'  => $this->language->get('text_nap')
        );
        $idoszakok[] = array(
            'id'    => '2',
            'name'  => $this->language->get('text_het')
        );
        $idoszakok[] = array(
            'id'    => '3',
            'name'  => $this->language->get('text_honap')
        );
        $idoszakok[] = array(
            'id'    => '4',
            'name'  => $this->language->get('text_ev')
        );

        $this->data['idoszakok'] = $idoszakok;

        $this->data['penznem'] =  " ". $this->currency->getFizetoeszkoz();

        if (isset($this->request->post['product_elhelyezkedes'])) {
			$this->data['product_elhelyezkedess'] = $this->request->post['product_elhelyezkedes'];
            if ( isset($this->data['product_elhelyezkedess']['NULL']) ) {
                unset($this->data['product_elhelyezkedess']['NULL']);
            }
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_elhelyezkedess'] = $this->model_catalog_product->getProductElhelyezkedes($this->request->get['product_id']);
		} else {
			$this->data['product_elhelyezkedess'] = array();
		}

        $sor = 0;

        if ($this->data['product_elhelyezkedess'] ) {
            $sort_order = array();
            foreach ($this->data['product_elhelyezkedess'] as $key => $value) {
                $sort_order[$key] = $value['priority'];
                if ($value['sor'] > $sor) {
                    $sor = $value['sor'];
                }
            }
            array_multisort($sort_order, SORT_ASC, $this->data['product_elhelyezkedess']);
        }
        $this->data['elhelyezkedes_row_max'] = $sor+1;

        /* Elhelyezkedés -kiemelés -vége */

        if (isset($this->request->post['product_video'])) {
            $product_videos = $this->request->post['product_video'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_videos = $this->model_catalog_product->getProductVideos($this->request->get['product_id']);
        } else {
            $product_videos = array();
        }

        $this->data['product_videos'] = $product_videos;

        if (isset($this->request->post['product_pdf'])) {
            $product_pdfs = $this->request->post['product_pdf'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_pdfs = $this->model_catalog_product->getProductPdfs($this->request->get['product_id']);
        } else {
            $product_pdfs = array();
        }

        $this->data['product_pdfs'] = array();

        foreach ($product_pdfs as $product_pdf) {
            if ($product_pdf['path'] && file_exists(DIR_IMAGE . $product_pdf['path'])) {
                $image = $product_pdf['path'];
            } else {
                $image = 'no_image.jpg';
            }

            $this->data['product_pdfs'][] = array(
                'image'       => $image,
                'thumb'       => $this->model_tool_image->resize($image, 100, 100),
                'pdf_title'   => $product_pdf['pdf_title'],
                'sort_order'  => $product_pdf['sort_order']
            );
        }


		if (isset($this->request->post['product_image'])) {
			$product_images = $this->request->post['product_image'];
		} elseif (isset($this->request->get['product_id'])) {
			$product_images = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
		} else {
			$product_images = array();
		}

		$this->data['product_images'] = array();

		foreach ($product_images as $product_image) {
			if ($product_image['image'] && file_exists(DIR_IMAGE . $product_image['image'])) {
				$image = $product_image['image'];
			} else {
				$image = 'no_image.jpg';
			}

			$this->data['product_images'][] = array(
				'image'      => $image,
				'thumb'      => $this->model_tool_image->resize($image, 100, 100),
				'sort_order' => $product_image['sort_order']
			);
		}

        if (isset($this->request->post['product_image_in_places'])) {
            $product_image_in_places = $this->request->post['product_image_in_places'];
            if(array_key_exists('NULL', $product_image_in_places)) {
                unset($product_image_in_places['NULL']);
            }
            $placeNew = array();
            foreach($product_image_in_places as $product_image_in_place) {
                if ($product_image_in_place['image'] && file_exists(DIR_IMAGE . $product_image_in_place['image'])) {
                    $image_in_place = $product_image_in_place['image'];
                } else {
                    $image_in_place = 'no_image.jpg';
                }

                $isImage = true;
                if(preg_match("/\.pdf$/", $image_in_place)) {
                    $isImage = false;
                }

                $newData = array(
                    'image'             => $image_in_place,
                    'thumb'             => ($isImage) ? $this->model_tool_image->resize($image_in_place, 100, 100) : '',
                    'sort_order'        => $product_image_in_place['sort_order'],
                    'locations'         => isset($product_image_in_place['locations']) ? $product_image_in_place['locations'] : array(),
                    'generated_text'    => 0,
                    'location_name'     => isset($product_image_in_place['name']) ? $product_image_in_place['name'] : ''
                );
                $placeNew[] = $newData;
            }
            $this->data['product_image_in_places'] = $placeNew;
        } elseif (isset($this->request->get['product_id'])) {
            $this->load->model('catalog/productimageplaces');
            $product_image_in_places = $this->model_catalog_productimageplaces->getImagesByProductID($this->request->get['product_id']);
            $product_image_in_locations = $this->model_catalog_productimageplaces->getPlacesByProductID($this->request->get['product_id']);

            foreach ($product_image_in_places as $product_image_in_place) {
                $imagebylocations_id = $product_image_in_place['imagebylocations_id'];
                if ($product_image_in_place['image'] && file_exists(DIR_IMAGE . $product_image_in_place['image'])) {
                    $image_in_place = $product_image_in_place['image'];
                } else {
                    $image_in_place = 'no_image.jpg';
                }

                $isImage = true;
                /*if(preg_match("/\.pdf$/", $image_in_place)) {
                    $isImage = false;
                }*/

                $newData = array(
                    'image'             => $image_in_place,
                    'thumb'             => ($isImage) ? $this->model_tool_image->resize($image_in_place, 100, 100) : '',
                    'sort_order'        => $product_image_in_place['sort_order'],
                    'locations'         => array(),
                    'generated_text'    => 0,
                    'location_name'     => isset($product_image_in_place['location_name']) ? $product_image_in_place['location_name'] : ''
                );
                foreach($product_image_in_locations as $product_image_in_location) {
                    if($product_image_in_location['imagebylocations_id'] == $imagebylocations_id) {
                        $newData['locations'][] = $product_image_in_location['filter_select_id'];
                    }
                }
                $this->data['product_image_in_places'][] = $newData;
            }
        } else {
            $this->data['product_image_in_places'] = array();
        }

		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

		$this->load->model('catalog/download');

		$this->data['downloads'] = $this->model_catalog_download->getDownloads();

		if (isset($this->request->post['product_download'])) {
			$this->data['product_download'] = $this->request->post['product_download'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_download'] = $this->model_catalog_product->getProductDownloads($this->request->get['product_id']);
		} else {
			$this->data['product_download'] = array();
		};






        if (isset($this->request->post['product_customer_utalvany'])) {
            $this->data['product_vevo_sajat'] = $this->request->post['product_customer_utalvany'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_customer_utalvany'] = $this->model_catalog_product->getProductVevoUtalvany($this->request->get['product_id']);
        } else {
            $this->data['product_customer_utalvany'] = array();
        }

        $data= array(
            'filter_feltolto' => 1
        );

        $this->load->model('sale/customer');
        $this->data['product_termek_vevok'] = $this->model_sale_customer->getCustomers($data);






        $this->load->model('catalog/category');

		$this->data['categories'] = $this->model_catalog_category->getCategories(0);

		if (isset($this->request->post['product_category'])) {
			$this->data['product_category'] = $this->request->post['product_category'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_category'] = $this->model_catalog_product->getProductCategories($this->request->get['product_id']);
		} else {
			$this->data['product_category'] = array();
		}



        // Categories
        $this->load->model('catalog/category');

        if (isset($this->request->post['product_category'])) {
            $categories = $this->request->post['product_category'];
        } elseif (isset($this->request->get['product_id'])) {
            $categories = $this->model_catalog_product->getProductCategories($this->request->get['product_id']);
        } else {
            $categories = array();
        }

        $this->data['product_categories'] = array();

        foreach ($categories as $category_id) {
            $category_info = $this->model_catalog_category->getCategory($category_id);

            if ($category_info) {
                $this->data['product_categories'][] = array(
                    'category_id' => $category_info['category_id'],
                    'name'        => ($category_info['path'] ? $category_info['path'] . ' &gt; ' : '') . $category_info['name']
                );
            }
        }


        // warehouse
        /*$this->load->model('warehouse/warehouse');


        $this->data['warehouses'] =  $this->model_warehouse_warehouse->getWarehouses();

        if (isset($this->request->post['product_warehouse'])) {
            $this->data['product_warehouse'] = $this->request->post['product_warehouse'];
        } elseif (isset($this->request->get['warehouse_id'])) {
            $this->data['product_warehouse'] = $this->model_warehouse_warehouse->getWarehousesProduct($this->request->get['product_id']);
        } else {
            $this->data['product_warehouse'] = array();
        }


        if (isset($this->request->post['product_warehouse'])) {
            $warehouses = $this->request->post['product_warehouse'];
        } elseif (isset($this->request->get['product_id'])) {
            $warehouses = $this->model_warehouse_warehouse->getWarehousesProduct($this->request->get['product_id']);
        } else {
            $warehouses = array();
        }

        $this->data['product_warehouse'] = array();

        foreach ($warehouses as $warehouse_id) {
            $warehouse_info = $this->model_warehouse_warehouse->getWarehouses($warehouse_id);

            if ($warehouse_info) {
                $this->data['product_warehouses'][] = array(
                    'warehouse_id' => $warehouse_info['warehouse_id'],
                    'address'         => $warehouse_info['city']. " ". $warehouse_info['address']
                );
            }
        }*/
        // end warehouse


        // Filters
        $this->load->model('catalog/filter');

        if (isset($this->request->post['product_filter'])) {
            $filters = $this->request->post['product_filter'];
        } elseif (isset($this->request->get['product_id'])) {
            $filters = $this->model_catalog_product->getProductFilters($this->request->get['product_id']);
        } else {
            $filters = array();
        }

        $this->data['product_filters'] = array();

        foreach ($filters as $filter_id) {
            $filter_info = $this->model_catalog_filter->getFilter($filter_id);

            if ($filter_info) {
                $this->data['product_filters'][] = array(
                    'filter_id' => $filter_info['filter_id'],
                    'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name'],
                    'filter_group_id' => $filter_info['filter_group_id'],
                    'filter_select_id' => $filter_info['filter_select_id'],
                    'filter_select_name' => $filter_info['name']

                );
            }
        }



// szűrők kigyüjtése teljes (pl. város->üzlet
		$this->load->model('catalog/filter');
		$data = array(
			'filter_name' => '',
			'start'       => 0,
			'limit'       => 550
		);
		$filters = $this->model_catalog_filter->getFilters($data);
		$szurok = array();
		foreach ($filters as $filter) {
			$szurok[] = array(
                'filter_id' => $filter['filter_id'],
                'name'      => $filter['group'] . ' &gt; ' . $filter['name'],
                'filter_group_id' => $filter['filter_group_id'],
                'filter_select_id' => $filter['filter_select_id'],
                'filter_select_name' => $filter['name']
			);
		}
		$sort_order = array();

		foreach ($szurok as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $szurok);
		$this->data['szurok'] = $szurok;



        $sort_order = array();
        foreach ($szurok as $key => $value) {
            $sort_order[$key] = $value['filter_select_name'];
        }

        array_multisort($sort_order, SORT_ASC, $szurok);
        $this->data['szurok2'] = $szurok;


//szűrők kigyüjtés vége

//filter select  (pl. Üzletek)

        $this->load->model('catalog/filter_group');
        $results = $this->model_catalog_filter_group->getFilterGroups($data);

        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => $this->url->link('catalog/filter_group/update', 'token=' . $this->session->data['token'] . '&filter_select_id=' . $result['filter_select_id'] . $url, 'SSL')
            );

            $this->data['filter_selects'][] = array(
                'filter_select_id' => $result['filter_select_id'],
                'name'               => $result['name'],
                'sort_order'         => $result['sort_order'],
                'selected'           => isset($this->request->post['selected']) && in_array($result['filter_select_id'], $this->request->post['selected']),
                'action'             => $action
            );
        }
// filter select  vége

// Kapcsolódó termékek
        if (isset($this->request->post['product_related'])) {
			$products = $this->request->post['product_related'];
		} elseif (isset($this->request->get['product_id'])) {
			$products = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
		} else {
			$products = array();
		}

		$this->data['product_related'] = array();

		foreach ($products as $product_id) {
			$related_info = $this->model_catalog_product->getProduct($product_id);

			if ($related_info) {
				$this->data['product_related'][] = array(
					'product_id' => $related_info['product_id'],
					'name'       => $related_info['name'],
					'model'      => $related_info['model']
				);
			}
		}
// Kapcsolódó termékek vége

// Ajándékok
        if (isset($this->request->post['product_ajandek'])) {
            $products = $this->request->post['product_ajandek'];
        } elseif (isset($this->request->get['product_id'])) {
            $products = $this->model_catalog_product->getProductAjandek($this->request->get['product_id']);
        } else {
            $products = array();
        }

        $this->data['product_ajandekok'] = array();

        foreach ($products as $product_id) {
            $ajandek_info = $this->model_catalog_product->getProduct($product_id);

            if ($ajandek_info) {
                $this->data['product_ajandekok'][] = array(
                    'product_id' => $ajandek_info['product_id'],
                    'name'       => $ajandek_info['name'],
                    'model'      => $ajandek_info['model']

                );
            }
        }
// Ajándékok

// Helyetesito termekek
        if (isset($this->request->post['product_helyettesito'])) {
            $products = $this->request->post['product_helyettesito'];
        } elseif (isset($this->request->get['product_id'])) {
            $products = $this->model_catalog_product->getProductHelyettesito($this->request->get['product_id']);
        } else {
            $products = array();
        }

        $this->data['product_helyettesitok'] = array();

        foreach ($products as $product_id) {
            $helyettesito_info = $this->model_catalog_product->getProduct($product_id);

            if ($helyettesito_info) {
                $this->data['product_helyettesitok'][] = array(
                    'product_id' => $helyettesito_info['product_id'],
                    'name'       => $helyettesito_info['name'],
                    'model'      => $helyettesito_info['model']

                );
            }
        }
// Helyetesito termekek


// Ajánlott tartozékok
        if (isset($this->request->post['product_accessory'])) {
            $products = $this->request->post['product_accessory'];
        } elseif (isset($this->request->get['product_id'])) {
            $products = $this->model_catalog_product->getProductAccessory($this->request->get['product_id']);
        } else {
            $products = array();
        }

        $this->data['product_accessories'] = array();

        foreach ($products as $product_id) {
            $accessory_info = $this->model_catalog_product->getProduct($product_id);

            if ($accessory_info) {
                $this->data['product_accessories'][] = array(
                    'product_id' => $accessory_info['product_id'],
                    'name'       => $accessory_info['name'],
                    'model'      => $accessory_info['model']

                );
            }
        }
// Ajánlott tartozékok vége


// Általlános tartozékok
        if (isset($this->request->post['product_general_accessory'])) {
            $products = $this->request->post['product_general_accessory'];
        } elseif (isset($this->request->get['product_id'])) {
            $products = $this->model_catalog_product->getProductGeneralAccessory($this->request->get['product_id']);
        } else {
            $products = array();
        }

        $this->data['product_general_accessories'] = array();

        foreach ($products as $product_id) {
            $general_accessory_info = $this->model_catalog_product->getProduct($product_id);

            if ($general_accessory_info) {
                $this->data['product_general_accessories'][] = array(
                    'product_id' => $general_accessory_info['product_id'],
                    'name'       => $general_accessory_info['name'],
                    'model'      => $general_accessory_info['model']

                );
            }
        }
// Általános tartozékok vége


// Csomag
        if (isset($this->request->post['product_package'])) {
            $products = $this->request->post['product_package'];
        } elseif (isset($this->request->get['product_id'])) {
            $products = $this->model_catalog_product->getProductPackage($this->request->get['product_id']);
        } else {
            $products = array();
        }

        $this->data['product_packages'] = array();
        $price_all = 0;
        $package_price_all = 0;

        foreach ($products as $product) {
            $package_info = $this->model_catalog_product->getProduct($product['package_id']);

            if ($package_info) {
                $name = strip_tags(html_entity_decode($package_info['name'], ENT_QUOTES, 'UTF-8'));
                $name=str_replace("&nbsp;","",$name);
                $name=str_replace("\r","",$name);
                $name=str_replace("\t","",$name);
                $name=str_replace("\n","",$name);
                $name=trim($name);
                $name=  strlen($name) > 30 ? mb_substr($name, 0, 30, "UTF-8").'...' : $name;

                $this->data['product_packages'][] = array(
                    'package_id'        => $product['package_id'],
                    'name'              => $name,
                    'quantity'          => $product['quantity'],
                    'model'             => $package_info['model'],

                    'price'             => $package_info['price'],
                    'price_format'      => $this->currency->format($package_info['price']),
                    //'price_row'         => $package_info['price']*$product['quantity'],
                    //'price_row_format'  => $this->currency->format($package_info['price']*$product['quantity']),

                    'package_price'             => round($product['price']),
                    'package_price_format'      => $this->currency->format($product['price']),
                    'package_price_row'         => $product['price']*$product['quantity'],
                    'package_price_row_format'  => $this->currency->format($product['price']*$product['quantity'])
                );
                $price_all +=$package_info['price']*$product['quantity'];
                $package_price_all +=$product['price']*$product['quantity'];
            }
        }
        $this->data['price_all'] = $price_all;
        $this->data['price_all_format'] = $this->currency->format($price_all);
        $this->data['package_price_all'] = $package_price_all;
        $this->data['package_price_all_format'] = $this->currency->format($package_price_all);
// Csomag vége




    	if (isset($this->request->post['points'])) {
      		$this->data['points'] = $this->request->post['points'];
    	} elseif (!empty($product_info)) {
			$this->data['points'] = $product_info['points'];
		} else {
      		$this->data['points'] = '';
    	}

		if (isset($this->request->post['product_reward'])) {
			$this->data['product_reward'] = $this->request->post['product_reward'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_reward'] = $this->model_catalog_product->getProductRewards($this->request->get['product_id']);
		} else {
			$this->data['product_reward'] = array();
		}

		if (isset($this->request->post['product_layout'])) {
			$this->data['product_layout'] = $this->request->post['product_layout'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_layout'] = $this->model_catalog_product->getProductLayouts($this->request->get['product_id']);
		} else {
			$this->data['product_layout'] = array();
		}

		$this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'catalog/product_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

        if ($this->config->get('megjelenit_vatera') == 1) {
            if (file_exists(DIR_APPLICATION."controller/catalog/vatera.php")){
                require_once(DIR_APPLICATION."controller/catalog/vatera.php");
            }
        }

		$this->response->setOutput($this->render());
  	}

    private function getFormcustomer() {
        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');



        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_select_all'] = $this->language->get('text_select_all');
        $this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
        $this->data['text_plus'] = $this->language->get('text_plus');
        $this->data['text_minus'] = $this->language->get('text_minus');
        $this->data['text_default'] = $this->language->get('text_default');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['text_browse'] = $this->language->get('text_browse');
        $this->data['text_clear'] = $this->language->get('text_clear');
        $this->data['text_option'] = $this->language->get('text_option');
        $this->data['text_option_value'] = $this->language->get('text_option_value');
        $this->data['text_select'] = $this->language->get('text_select');
        $this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_percent'] = $this->language->get('text_percent');
        $this->data['text_amount'] = $this->language->get('text_amount');
        $this->data['entry_filter'] = $this->language->get('entry_filter');
        $this->data['text_kupon_keszit'] = $this->language->get('text_kupon_keszit');

        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
        $this->data['entry_description'] = $this->language->get('entry_description');
        $this->data['entry_store'] = $this->language->get('entry_store');
        $this->data['entry_keyword'] = $this->language->get('entry_keyword');
        $this->data['entry_model'] = $this->language->get('entry_model');
        $this->data['entry_cikkszam'] = $this->language->get('entry_cikkszam');
        $this->data['entry_cikkszam2'] = $this->language->get('entry_cikkszam2');
        $this->data['entry_sku'] = $this->language->get('entry_sku');
        $this->data['entry_upc'] = $this->language->get('entry_upc');
        $this->data['entry_location'] = $this->language->get('entry_location');
        $this->data['entry_minimum'] = $this->language->get('entry_minimum');
        $this->data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
        $this->data['entry_shipping'] = $this->language->get('entry_shipping');
        $this->data['entry_date_available'] = $this->language->get('entry_date_available');
        $this->data['entry_date_kaphato_ig'] = $this->language->get('entry_date_kaphato_ig');
        $this->data['entry_quantity'] = $this->language->get('entry_quantity');
        $this->data['mennyisegi_egyseg'] = $this->language->get('mennyisegi_egyseg');
        $this->data['csomagolasi_egyseg'] = $this->language->get('csomagolasi_egyseg');
        $this->data['csomagolasi_mennyiseg'] = $this->language->get('csomagolasi_mennyiseg');
        $this->data['entry_stock_status'] = $this->language->get('entry_stock_status');
        $this->data['entry_price'] = $this->language->get('entry_price');
        $this->data['entry_eredeti_ar'] = $this->language->get('entry_eredeti_ar');
        $this->data['entry_utalvany'] = $this->language->get('entry_utalvany');
        $this->data['entry_szazalek'] = $this->language->get('entry_szazalek');
        $this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
        $this->data['entry_points'] = $this->language->get('entry_points');
        $this->data['entry_option_points'] = $this->language->get('entry_option_points');
        $this->data['entry_subtract'] = $this->language->get('entry_subtract');
        $this->data['entry_checked'] = $this->language->get('entry_checked');
        $this->data['entry_weight_class'] = $this->language->get('entry_weight_class');
        $this->data['entry_weight'] = $this->language->get('entry_weight');
        $this->data['entry_kiszereles']     = $this->language->get('entry_kiszereles');
        $this->data['entry_kiszereles_mertekegyseg'] = $this->language->get('entry_kiszereles_mertekegyseg');
        $this->data['entry_dimension'] = $this->language->get('entry_dimension');
        $this->data['entry_length'] = $this->language->get('entry_length');
        $this->data['entry_image'] = $this->language->get('entry_image');
        $this->data['entry_image_letoltheto'] = $this->language->get('entry_image_letoltheto');
        $this->data['entry_download'] = $this->language->get('entry_download');
        $this->data['entry_category'] = $this->language->get('entry_category');
        $this->data['entry_related'] = $this->language->get('entry_related');
        $this->data['entry_attribute'] = $this->language->get('entry_attribute');
        $this->data['entry_text'] = $this->language->get('entry_text');
        $this->data['entry_option'] = $this->language->get('entry_option');
        $this->data['entry_option_value'] = $this->language->get('entry_option_value');
        $this->data['entry_required'] = $this->language->get('entry_required');
        $this->data['entry_size_color_required'] = $this->language->get('entry_size_color_required');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
        $this->data['entry_date_start'] = $this->language->get('entry_date_start');
        $this->data['entry_date_end'] = $this->language->get('entry_date_end');
        $this->data['entry_priority'] = $this->language->get('entry_priority');
        $this->data['entry_tag'] = $this->language->get('entry_tag');
        $this->data['vevo_kedvezmeny'] = $this->language->get('vevo_kedvezmeny');
        $this->data['entry_reward'] = $this->language->get('entry_reward');
        $this->data['entry_layout'] = $this->language->get('entry_layout');
        $this->data['text_piacter'] = $this->language->get('text_piacter');
        $this->data['entry_letoltes'] = $this->language->get('entry_letoltes');
        $this->data['entry_elhelyezkedes'] = $this->language->get('entry_elhelyezkedes');
        $this->data['entry_elhelyezkedes_alcsoport'] = $this->language->get('entry_elhelyezkedes_alcsoport');
        $this->data['entry_kiemelt_keret'] = $this->language->get('entry_kiemelt_keret');
        $this->data['entry_idoszak_darab'] = $this->language->get('entry_idoszak_darab');
        $this->data['entry_idoszak'] = $this->language->get('entry_idoszak');
        $this->data['entry_feldolgozas_status'] = $this->language->get('entry_feldolgozas_status');
        $this->data['entry_fizetes_status'] = $this->language->get('entry_fizetes_status');
        $this->data['entry_fizetendo'] = $this->language->get('entry_fizetendo');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['button_add_attribute'] = $this->language->get('button_add_attribute');
        $this->data['button_add_option'] = $this->language->get('button_add_option');
        $this->data['button_add_option_value'] = $this->language->get('button_add_option_value');
        $this->data['button_add_discount'] = $this->language->get('button_add_discount');
        $this->data['button_add_vevo'] = $this->language->get('button_add_vevo');
        $this->data['button_add_special'] = $this->language->get('button_add_special');
        $this->data['button_add_image'] = $this->language->get('button_add_image');
        $this->data['button_add_video'] = $this->language->get('button_add_video');
        $this->data['button_add_pdf']   = $this->language->get('button_add_pdf');
        $this->data['button_add_elhelyezkedes'] = $this->language->get('button_add_elhelyezkedes');
        $this->data['button_remove'] = $this->language->get('button_remove');

        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_data'] = $this->language->get('tab_data');
        $this->data['tab_attribute'] = $this->language->get('tab_attribute');
        $this->data['tab_option'] = $this->language->get('tab_option');
        $this->data['tab_discount'] = $this->language->get('tab_discount');
        $this->data['tab_vevok'] = $this->language->get('tab_vevok');
        $this->data['tab_special'] = $this->language->get('tab_special');
        $this->data['tab_image'] = $this->language->get('tab_image');
        $this->data['tab_links'] = $this->language->get('tab_links');
        $this->data['tab_reward'] = $this->language->get('tab_reward');
        $this->data['tab_design'] = $this->language->get('tab_design');
        $this->data['tab_elhelyezkedes'] = $this->language->get('tab_elhelyezkedes');
        $this->data['entry_biztonsagi_kod'] = $this->language->get('entry_biztonsagi_kod');

        $this->data['megjelenit_termekadat'] = $this->config->get('megjelenit_admin_termekadatok');
        $this->data['megjelenit_termekful'] = $this->config->get('megjelenit_admin_termekful');
        $this->data['megjelenit_admin_altalanos'] = $this->config->get('megjelenit_admin_altalanos');


        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $this->data['error_name'] = $this->error['name'];
        } else {
            $this->data['error_name'] = array();
        }

        if (isset($this->error['meta_description'])) {
            $this->data['error_meta_description'] = $this->error['meta_description'];
        } else {
            $this->data['error_meta_description'] = array();
        }

        if (isset($this->error['description'])) {
            $this->data['error_description'] = $this->error['description'];
        } else {
            $this->data['error_description'] = array();
        }



        if (isset($this->error['model'])) {
            $this->data['error_model'] = $this->error['model'];
        } else {
            $this->data['error_model'] = '';
        }
        if (isset($this->error['cikkszam'])) {
            $this->data['error_cikkszam'] = $this->error['cikkszam'];
        } else {
            $this->data['error_cikkszam'] = '';
        }
        if (isset($this->error['cikkszam2'])) {
            $this->data['error_cikkszam2'] = $this->error['cikkszam2'];
        } else {
            $this->data['error_cikkszam2'] = '';
        }



        if (isset($this->error['date_ervenyes_ig'])) {
            $this->data['error_date_ervenyes_ig'] = $this->error['date_ervenyes_ig'];
        } else {
            $this->data['error_date_ervenyes_ig'] = '';
        }

        if (isset($this->error['date_available'])) {
            $this->data['error_date_available'] = $this->error['date_available'];
        } else {
            $this->data['error_date_available'] = '';
        }

        $url = '';


        //filter category start//
        if (isset($this->request->get['filter_category'])) {
            $url .= '&filter_category=' . $this->request->get['filter_category'];
        }
        //filter category end//

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . $this->request->get['filter_name'];
        }



        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model=' . $this->request->get['filter_model'];
        }
        if (isset($this->request->get['filter_cikkszam'])) {
            $url .= '&filter_cikkszam=' . $this->request->get['filter_cikkszam'];
        }
        if (isset($this->request->get['filter_cikkszam2'])) {
            $url .= '&filter_cikkszam2=' . $this->request->get['filter_cikkszam2'];
        }



        if (isset($this->request->get['filter_price'])) {
            $url .= '&filter_price=' . $this->request->get['filter_price'];
        }

        if (isset($this->request->get['filter_quantity'])) {
            $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        if (!isset($this->request->get['product_id'])) {
            $this->data['action'] = $this->url->link('catalog/product/insertcustomer', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $this->data['action'] = $this->url->link('catalog/product/updatecustomer', 'token=' . $this->session->data['token'] . '&product_id=' . $this->request->get['product_id'] . $url, 'SSL');
        }

        $this->data['cancel'] = $this->url->link('catalog/product/customerproducts', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
        }

        $this->load->model('localisation/language');

        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['product_description'])) {
            $this->data['product_description'] = $this->request->post['product_description'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_description'] = $this->model_catalog_product->getProductDescriptions($this->request->get['product_id']);
        } else {
            $this->data['product_description'] = array();
        }

        if (isset($this->request->post['model'])) {
            $this->data['model'] = $this->request->post['model'];
        } elseif (!empty($product_info)) {
            $this->data['model'] = $product_info['model'];
        } else {
            $this->data['model'] = '';
        }

        if (isset($this->request->post['cikkszam'])) {
            $this->data['cikkszam'] = $this->request->post['cikkszam'];
        } elseif (!empty($product_info)) {
            $this->data['cikkszam'] = $product_info['cikkszam'];
        } else {
            $this->data['cikkszam'] = '';
        }

        if (isset($this->request->post['cikkszam2'])) {
            $this->data['cikkszam2'] = $this->request->post['cikkszam2'];
        } elseif (!empty($product_info)) {
            $this->data['cikkszam2'] = $product_info['cikkszam2'];
        } else {
            $this->data['cikkszam2'] = '';
        }

        if (isset($this->request->post['sku'])) {
            $this->data['sku'] = $this->request->post['sku'];
        } elseif (!empty($product_info)) {
            $this->data['sku'] = $product_info['sku'];
        } else {
            $this->data['sku'] = '';
        }

        if (isset($this->request->post['upc'])) {
            $this->data['upc'] = $this->request->post['upc'];
        } elseif (!empty($product_info)) {
            $this->data['upc'] = $product_info['upc'];
        } else {
            $this->data['upc'] = '';
        }

        if (isset($this->request->post['location'])) {
            $this->data['location'] = $this->request->post['location'];
        } elseif (!empty($product_info)) {
            $this->data['location'] = $product_info['location'];
        } else {
            $this->data['location'] = '';
        }

        $this->load->model('setting/store');

        $this->data['stores'] = $this->model_setting_store->getStores();

        if (isset($this->request->post['product_store'])) {
            $this->data['product_store'] = $this->request->post['product_store'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_store'] = $this->model_catalog_product->getProductStores($this->request->get['product_id']);
        } else {
            $this->data['product_store'] = array(0);
        }

        if (isset($this->request->post['keyword'])) {
            $this->data['keyword'] = $this->request->post['keyword'];
        } elseif (!empty($product_info)) {
            $this->data['keyword'] = $product_info['keyword'];
        } else {
            $this->data['keyword'] = '';
        }

        if (isset($this->request->post['product_tag'])) {
            $this->data['product_tag'] = $this->request->post['product_tag'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_tag'] = $this->model_catalog_product->getProductTags($this->request->get['product_id']);
        } else {
            $this->data['product_tag'] = array();
        }

        if (isset($this->request->post['image'])) {
            $this->data['image'] = $this->request->post['image'];
        } elseif (!empty($product_info)) {
            $this->data['image'] = $product_info['image'];
        } else {
            $this->data['image'] = '';
        }

        if (isset($this->request->post['letoltheto_kep'])) {
            $this->data['letoltheto_kep'] = $this->request->post['letoltheto_kep'];
        } elseif (!empty($product_info)) {
            $this->data['letoltheto_kep'] = $product_info['letoltheto'];
        } else {
            $this->data['letoltheto_kep'] = '';
        }

        $this->load->model('tool/image');

        if (!empty($product_info) && $product_info['image'] && file_exists(DIR_IMAGE . $product_info['image'])) {
            $this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
        } else {
            $this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }

        if (!empty($product_info) && $product_info['letoltheto'] && file_exists(DIR_IMAGE . $product_info['letoltheto'])) {
            $this->data['letoltheto_thumb'] = $this->model_tool_image->resize($product_info['letoltheto'], 100, 100);
        } else {
            $this->data['letoltheto_thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }

        $this->load->model('catalog/manufacturer');

        $this->data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();

        if (isset($this->request->post['manufacturer_id'])) {
            $this->data['manufacturer_id'] = $this->request->post['manufacturer_id'];
        } elseif (!empty($product_info)) {
            $this->data['manufacturer_id'] = $product_info['manufacturer_id'];
        } else {
            $this->data['manufacturer_id'] = 0;
        }

        if (isset($this->request->post['shipping'])) {
            $this->data['shipping'] = $this->request->post['shipping'];
        } elseif (!empty($product_info)) {
            $this->data['shipping'] = $product_info['shipping'];
        } else {
            $this->data['shipping'] = 1;
        }

        if (isset($this->request->post['price'])) {
            $this->data['price'] = $this->request->post['price'];
        } elseif (!empty($product_info)) {
            $this->data['price'] = $product_info['price'];
        } else {
            $this->data['price'] = '';
        }

        if (isset($this->request->post['eredeti_ar'])) {
            $this->data['eredeti_ar'] = $this->request->post['eredeti_ar'];
        } elseif (!empty($product_info)) {
            $this->data['eredeti_ar'] = $product_info['eredeti_ar'];
        } else {
            $this->data['eredeti_ar'] = 0;
        }

        if (isset($this->request->post['szazalek'])) {
            $this->data['szazalek'] = $this->request->post['szazalek'];
        } elseif (!empty($product_info)) {
            $this->data['szazalek'] = $product_info['szazalek'];
        } else {
            $this->data['szazalek'] = '';
        }

        if (isset($this->request->post['utalvany'])) {
            $this->data['utalvany'] = $this->request->post['utalvany'];
        } elseif (!empty($product_info)) {
            $this->data['utalvany'] = $product_info['utalvany'];
        } else {
            $this->data['utalvany'] = 0;
        }

        $this->load->model('localisation/tax_class');

        $this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

        if (isset($this->request->post['tax_class_id'])) {
            $this->data['tax_class_id'] = $this->request->post['tax_class_id'];
        } elseif (!empty($product_info)) {
            $this->data['tax_class_id'] = $product_info['tax_class_id'];
        } else {
            $this->data['tax_class_id'] = 0;
        }

        if (isset($this->request->post['date_available'])) {
            $this->data['date_available'] = $this->request->post['date_available'];
        } elseif (!empty($product_info)) {
            $this->data['date_available'] = date('Y-m-d', strtotime($product_info['date_available']));
        } else {
            $this->data['date_available'] = date('Y-m-d', time() - 86400);
        }

        if (isset($this->request->post['date_ervenyes_ig'])) {
            $this->data['date_ervenyes_ig'] = $this->request->post['date_ervenyes_ig'];
        } elseif (!empty($product_info)) {
            $this->data['date_ervenyes_ig'] = date('Y-m-d', strtotime($product_info['date_ervenyes_ig']));
        } else {
            $this->data['date_ervenyes_ig'] = date('Y-m-d');
            //$this->data['date_ervenyes_ig'] = date('Y-m-d', time() - 86400);
        }

        if (isset($this->request->post['quantity'])) {
            $this->data['quantity'] = $this->request->post['quantity'];
        } elseif (!empty($product_info)) {
            $this->data['quantity'] = $product_info['quantity'];
        } else {
            $this->data['quantity'] = 1;
        }

        if (isset($this->request->post['megyseg'])) {
            $this->data['megyseg'] = $this->request->post['megyseg'];
        } elseif (!empty($product_info)) {
            $this->data['megyseg'] = $product_info['megyseg'];
        } else {
            $this->data['megyseg'] = "darab";
        }

        if (isset($this->request->post['stock_in_date'])) {
            $this->data['stock_in_date'] = $this->request->post['stock_in_date'];
        } elseif (isset($product_info['stock_in_date']) && $product_info['stock_in_date']) {
            $this->data['stock_in_date'] = $product_info['stock_in_date'];
        } else {
            $this->data['stock_in_date'] = "0000-00-00";
        }

        if (isset($this->request->post['csom_egyseg'])) {
            $this->data['csom_egyseg'] = $this->request->post['csom_egyseg'];
        } elseif (!empty($product_info)) {
            $this->data['csom_egyseg'] = $product_info['csomagolasi_egyseg'];
        } else {
            $this->data['csom_egyseg'] = 1;
        }

        if (isset($this->request->post['csom_mennyiseg'])) {
            $this->data['csom_mennyiseg'] = $this->request->post['csom_mennyiseg'];
        } elseif (!empty($product_info)) {
            $this->data['csom_mennyiseg'] = $product_info['csomagolasi_mennyiseg'];
        } else {
            $this->data['csom_mennyiseg'] = 1;
        }

        if (isset($this->request->post['minimum'])) {
            $this->data['minimum'] = $this->request->post['minimum'];
        } elseif (!empty($product_info)) {
            $this->data['minimum'] = $product_info['minimum'];
        } else {
            $this->data['minimum'] = 1;
        }

        if (isset($this->request->post['subtract'])) {
            $this->data['subtract'] = $this->request->post['subtract'];
        } elseif (!empty($product_info)) {
            $this->data['subtract'] = $product_info['subtract'];
        } else {
            $this->data['subtract'] = 1;
        }

        if (isset($this->request->post['checked'])) {
            $this->data['checked'] = $this->request->post['checked'];
        } elseif (!empty($product_info)) {
            $this->data['checked'] = $product_info['checked'];
        } else {
            $this->data['checked'] = 1;
        }

        if (isset($this->request->post['sort_order'])) {
            $this->data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($product_info)) {
            $this->data['sort_order'] = $product_info['sort_order'];
        } else {
            $this->data['sort_order'] = 1;
        }

        $this->load->model('localisation/stock_status');

        $this->data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

        if (isset($this->request->post['stock_status_id'])) {
            $this->data['stock_status_id'] = $this->request->post['stock_status_id'];
        } elseif (!empty($product_info)) {
            $this->data['stock_status_id'] = $product_info['stock_status_id'];
        } else {
            $this->data['stock_status_id'] = $this->config->get('config_stock_status_id');
        }

        if (isset($this->request->post['status'])) {
            $this->data['status'] = $this->request->post['status'];
        } elseif (!empty($product_info)) {
            $this->data['status'] = $product_info['status'];
        } else {
            $this->data['status'] = 1;
        }

        if (isset($this->request->post['weight'])) {
            $this->data['weight'] = $this->request->post['weight'];
        } elseif (!empty($product_info)) {
            $this->data['weight'] = $product_info['weight'];
        } else {
            $this->data['weight'] = '';
        }

        if (isset($this->request->post['packaging'])) {
            $this->data['packaging'] = $this->request->post['packaging'];
        } elseif (!empty($product_info['packaging'])) {
            $this->data['packaging'] = $product_info['packaging'];
        } else {
            $this->data['packaging'] = '';
        }

        if (isset($this->request->post['video'])) {
            $this->data['video'] = $this->request->post['video'];
        } elseif (!empty($product_info)) {
            $this->data['video'] = $product_info['video'];
        } else {
            $this->data['video'] = '';
        }

        $this->load->model('localisation/weight_class');

        $this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

        if (isset($this->request->post['weight_class_id'])) {
            $this->data['weight_class_id'] = $this->request->post['weight_class_id'];
        } elseif (!empty($product_info)) {
            $this->data['weight_class_id'] = $product_info['weight_class_id'];
        } else {
            $this->data['weight_class_id'] = $this->config->get('config_weight_class_id');
        }

        if (isset($this->request->post['length'])) {
            $this->data['length'] = $this->request->post['length'];
        } elseif (!empty($product_info)) {
            $this->data['length'] = $product_info['length'];
        } else {
            $this->data['length'] = '';
        }

        if (isset($this->request->post['width'])) {
            $this->data['width'] = $this->request->post['width'];
        } elseif (!empty($product_info)) {
            $this->data['width'] = $product_info['width'];
        } else {
            $this->data['width'] = '';
        }

        if (isset($this->request->post['height'])) {
            $this->data['height'] = $this->request->post['height'];
        } elseif (!empty($product_info)) {
            $this->data['height'] = $product_info['height'];
        } else {
            $this->data['height'] = '';
        }

        if (isset($this->request->post['correctsecuritycode'])) {
            $this->data['correctsecuritycode'] = $this->request->post['correctsecuritycode'];
        } elseif (!empty($product_info)) {
            $this->data['correctsecuritycode'] = $product_info['correctsecuritycode'];
        } else {
            $this->data['correctsecuritycode'] = '0';
        }

        $this->load->model('localisation/length_class');

        $this->data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();

        if (isset($this->request->post['length_class_id'])) {
            $this->data['length_class_id'] = $this->request->post['length_class_id'];
        } elseif (!empty($product_info)) {
            $this->data['length_class_id'] = $product_info['length_class_id'];
        } else {
            $this->data['length_class_id'] = $this->config->get('config_length_class_id');
        }

        if (isset($this->request->post['product_attribute'])) {
            $this->data['product_attributes'] = $this->request->post['product_attribute'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_attributes'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
        } else {
            $this->data['product_attributes'] = array();
        }

        $this->load->model('catalog/option');

        if (isset($this->request->post['product_option'])) {
            $product_options = $this->request->post['product_option'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_options = $this->model_catalog_product->getProductOptions($this->request->get['product_id']);
        } else {
            $product_options = array();
        }

        $this->data['product_options'] = array();

        foreach ($product_options as $product_option) {
            if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                $product_option_value_data = array();

                foreach ($product_option['product_option_value'] as $product_option_value) {
                    $product_option_value_data[] = array(
                        'product_option_value_id' => $product_option_value['product_option_value_id'],
                        'option_value_id'         => $product_option_value['option_value_id'],
                        'quantity'                => $product_option_value['quantity'],
                        'subtract'                => $product_option_value['subtract'],
                        'checked'                 => isset($product_option_value['checked']) ? $product_option_value['checked']  : '',
                        'price'                   => $product_option_value['price'],
                        'price_prefix'            => $product_option_value['price_prefix'],
                        'points'                  => $product_option_value['points'],
                        'points_prefix'           => $product_option_value['points_prefix'],
                        'weight'                  => $product_option_value['weight'],
                        'weight_prefix'           => $product_option_value['weight_prefix']
                    );
                }

                $this->data['product_options'][] = array(
                    'product_option_id'    => $product_option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id'            => $product_option['option_id'],
                    'name'                 => $product_option['name'],
                    'type'                 => $product_option['type'],
                    'required'             => isset($product_option['required']) ? $product_option['required'] : ''
                );
            } else {
                $this->data['product_options'][] = array(
                    'product_option_id' => $product_option['product_option_id'],
                    'option_id'         => $product_option['option_id'],
                    'name'              => $product_option['name'],
                    'type'              => $product_option['type'],
                    'option_value'      => $product_option['option_value'],
                    'required'          => isset($product_option['required']) ? $product_option['required'] : ''
                );
            }
        }

        $this->data['option_values'] = array();

        foreach ($product_options as $product_option) {
            if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                if (!isset($this->data['option_values'][$product_option['option_id']])) {
                    $this->data['option_values'][$product_option['option_id']] = $this->model_catalog_option->getOptionValues($product_option['option_id']);
                }
            }
        }

        $this->load->model('sale/customer_group');

        $this->data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups();
        $this->load->model('sale/customer');
        $this->data['vevo_groups'] = $this->model_sale_customer->getCustomers();

        if (isset($this->request->post['product_discount'])) {
            $this->data['product_discounts'] = $this->request->post['product_discount'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_discounts'] = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);
        } else {
            $this->data['product_discounts'] = array();
        }

        if (isset($this->request->post['product_vevo'])) {
            $this->data['product_vevok'] = $this->request->post['product_vevo'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_vevok'] = $this->model_catalog_product->getProductVevo($this->request->get['product_id']);
        } else {
            $this->data['product_vevok'] = array();
        }

        if (isset($this->request->post['product_special'])) {
            $this->data['product_specials'] = $this->request->post['product_special'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_specials'] = $this->model_catalog_product->getProductSpecials($this->request->get['product_id']);
        } else {
            $this->data['product_specials'] = array();
        }



        /* Elhelyezkedés -kiemelés */

        $this->load->model('catalog/elhelyezkedes');
        $this->data['elhelyezkedesek_selects'] = $this->model_catalog_elhelyezkedes->getElhelyezkedesek();

        $this->load->model('catalog/elhelyezkedes_alcsoport');
        $this->data['elhelyezkedesek_alcsoport_selects'] = $this->model_catalog_elhelyezkedes_alcsoport->getAlcsoportElhelyezkedesek();

        $this->load->model('catalog/kiemelesek');
        $this->data['kiemelesek_selects'] = $this->model_catalog_kiemelesek->getKiemelesek();

        $this->load->model('catalog/penzugyi_status');
        $this->data['penzugyi_status_selects'] = $this->model_catalog_penzugyi_status->getPenzugyiStatus();

        $this->load->model('catalog/fizetes_elbiralas');
        $this->data['fizetes_elbiralas_selects'] = $this->model_catalog_fizetes_elbiralas->getFizetesElbiralas();


        $idoszakok = array();

        $idoszakok[] = array(
            'id'    => '1',
            'name'  => $this->language->get('text_nap')
        );
        $idoszakok[] = array(
            'id'    => '2',
            'name'  => $this->language->get('text_het')
        );
        $idoszakok[] = array(
            'id'    => '3',
            'name'  => $this->language->get('text_honap')
        );
        $idoszakok[] = array(
            'id'    => '4',
            'name'  => $this->language->get('text_ev')
        );

        $this->data['idoszakok'] = $idoszakok;

        $this->data['penznem'] =  " ". $this->currency->getFizetoeszkoz();

        if (isset($this->request->post['product_elhelyezkedes'])) {
            $this->data['product_elhelyezkedess'] = $this->request->post['product_elhelyezkedes'];
            if ( isset($this->data['product_elhelyezkedess']['NULL']) ) {
                unset($this->data['product_elhelyezkedess']['NULL']);
            }
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_elhelyezkedess'] = $this->model_catalog_product->getProductElhelyezkedes($this->request->get['product_id']);
        } else {
            $this->data['product_elhelyezkedess'] = array();
        }

        $sor = 0;

        if ($this->data['product_elhelyezkedess'] ) {
            $sort_order = array();
            foreach ($this->data['product_elhelyezkedess'] as $key => $value) {
                $sort_order[$key] = $value['priority'];
                if ($value['sor'] > $sor) {
                    $sor = $value['sor'];
                }
            }
            array_multisort($sort_order, SORT_ASC, $this->data['product_elhelyezkedess']);
        }
        $this->data['elhelyezkedes_row_max'] = $sor+1;

        /* Elhelyezkedés -kiemelés -vége */

        if (isset($this->request->post['product_video'])) {
            $product_videos = $this->request->post['product_video'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_videos = $this->model_catalog_product->getProductVideos($this->request->get['product_id']);
        } else {
            $product_videos = array();
        }

        $this->data['product_videos'] = $product_videos;

        if (isset($this->request->post['product_pdf'])) {
            $product_pdfs = $this->request->post['product_pdf'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_pdfs = $this->model_catalog_product->getProductPdfs($this->request->get['product_id']);
        } else {
            $product_pdfs = array();
        }

        $this->data['product_pdfs'] = array();

        foreach ($product_pdfs as $product_pdf) {
            if ($product_pdf['path'] && file_exists(DIR_IMAGE . $product_pdf['path'])) {
                $image = $product_pdf['path'];
            } else {
                $image = 'no_image.jpg';
            }

            $this->data['product_pdfs'][] = array(
                'image'      => $image,
                'thumb'      => $this->model_tool_image->resize($image, 100, 100),
                'pdf_title'  => $product_pdf['pdf_title'],
                'sort_order' => $product_pdf['sort_order']
            );
        }


        if (isset($this->request->post['product_image'])) {
            $product_images = $this->request->post['product_image'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_images = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
        } else {
            $product_images = array();
        }

        $this->data['product_images'] = array();

        foreach ($product_images as $product_image) {
            if ($product_image['image'] && file_exists(DIR_IMAGE . $product_image['image'])) {
                $image = $product_image['image'];
            } else {
                $image = 'no_image.jpg';
            }

            $this->data['product_images'][] = array(
                'image'      => $image,
                'thumb'      => $this->model_tool_image->resize($image, 100, 100),
                'sort_order' => $product_image['sort_order']
            );
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

        $this->load->model('catalog/download');

        $this->data['downloads'] = $this->model_catalog_download->getDownloads();

        if (isset($this->request->post['product_download'])) {
            $this->data['product_download'] = $this->request->post['product_download'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_download'] = $this->model_catalog_product->getProductDownloads($this->request->get['product_id']);
        } else {
            $this->data['product_download'] = array();
        };

        $this->load->model('catalog/category');

        $this->data['categories'] = $this->model_catalog_category->getCategories(0);

        if (isset($this->request->post['product_category'])) {
            $this->data['product_category'] = $this->request->post['product_category'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_category'] = $this->model_catalog_product->getProductCategories($this->request->get['product_id']);
        } else {
            $this->data['product_category'] = array();
        }



        // Categories
        $this->load->model('catalog/category');

        if (isset($this->request->post['product_category'])) {
            $categories = $this->request->post['product_category'];
        } elseif (isset($this->request->get['product_id'])) {
            $categories = $this->model_catalog_product->getProductCategories($this->request->get['product_id']);
        } else {
            $categories = array();
        }

        $this->data['product_categories'] = array();

        foreach ($categories as $category_id) {
            $category_info = $this->model_catalog_category->getCategory($category_id);

            if ($category_info) {
                $this->data['product_categories'][] = array(
                    'category_id' => $category_info['category_id'],
                    'name'        => ($category_info['path'] ? $category_info['path'] . ' &gt; ' : '') . $category_info['name']
                );
            }
        }

        // Filters
        $this->load->model('catalog/filter');

        if (isset($this->request->post['product_filter'])) {
            $filters = $this->request->post['product_filter'];
        } elseif (isset($this->request->get['product_id'])) {
            $filters = $this->model_catalog_product->getProductFilters($this->request->get['product_id']);
        } else {
            $filters = array();
        }

        $this->data['product_filters'] = array();

        foreach ($filters as $filter_id) {
            $filter_info = $this->model_catalog_filter->getFilter($filter_id);

            if ($filter_info) {
                $this->data['product_filters'][] = array(
                    'filter_id' => $filter_info['filter_id'],
                    'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name'],
                    'filter_group_id' => $filter_info['filter_group_id'],
                    'filter_select_id' => $filter_info['filter_select_id'],
                    'filter_select_name' => $filter_info['name']

                );
            }
        }




// szűrők kigyüjtése teljes (pl. város->üzlet
        $this->load->model('catalog/filter');
        $data = array(
            'filter_name' => '',
            'start'       => 0,
            'limit'       => 550
        );
        $filters = $this->model_catalog_filter->getFilters($data);
        $szurok = array();
        foreach ($filters as $filter) {
            $szurok[] = array(
                'filter_id' => $filter['filter_id'],
                'name'      => $filter['group'] . ' &gt; ' . $filter['name'],
                'filter_group_id' => $filter['filter_group_id'],
                'filter_select_id' => $filter['filter_select_id'],
                'filter_select_name' => $filter['name']
            );
        }
        $sort_order = array();

        foreach ($szurok as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $szurok);
        $this->data['szurok'] = $szurok;



        $sort_order = array();
        foreach ($szurok as $key => $value) {
            $sort_order[$key] = $value['filter_select_name'];
        }

        array_multisort($sort_order, SORT_ASC, $szurok);
        $this->data['szurok2'] = $szurok;


//szűrők kigyüjtés vége

//filter select  (pl. Üzletek)

        $this->load->model('catalog/filter_group');
        $results = $this->model_catalog_filter_group->getFilterGroups($data);

        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => $this->url->link('catalog/filter_group/update', 'token=' . $this->session->data['token'] . '&filter_select_id=' . $result['filter_select_id'] . $url, 'SSL')
            );

            $this->data['filter_selects'][] = array(
                'filter_select_id' => $result['filter_select_id'],
                'name'               => $result['name'],
                'sort_order'         => $result['sort_order'],
                'selected'           => isset($this->request->post['selected']) && in_array($result['filter_select_id'], $this->request->post['selected']),
                'action'             => $action
            );
        }
// filter select  vége

        if (isset($this->request->post['product_related'])) {
            $products = $this->request->post['product_related'];
        } elseif (isset($this->request->get['product_id'])) {
            $products = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
        } else {
            $products = array();
        }

        $this->data['product_related'] = array();

        foreach ($products as $product_id) {
            $related_info = $this->model_catalog_product->getProduct($product_id);

            if ($related_info) {
                $this->data['product_related'][] = array(
                    'product_id' => $related_info['product_id'],
                    'name'       => $related_info['name']
                );
            }
        }

        if (isset($this->request->post['points'])) {
            $this->data['points'] = $this->request->post['points'];
        } elseif (!empty($product_info)) {
            $this->data['points'] = $product_info['points'];
        } else {
            $this->data['points'] = '';
        }

        if (isset($this->request->post['product_reward'])) {
            $this->data['product_reward'] = $this->request->post['product_reward'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_reward'] = $this->model_catalog_product->getProductRewards($this->request->get['product_id']);
        } else {
            $this->data['product_reward'] = array();
        }

        if (isset($this->request->post['product_layout'])) {
            $this->data['product_layout'] = $this->request->post['product_layout'];
        } elseif (isset($this->request->get['product_id'])) {
            $this->data['product_layout'] = $this->model_catalog_product->getProductLayouts($this->request->get['product_id']);
        } else {
            $this->data['product_layout'] = array();
        }

        $this->load->model('design/layout');

        $this->data['layouts'] = $this->model_design_layout->getLayouts();

        $this->template = 'catalog/customer_product_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        if ($this->config->get('megjelenit_vatera') == 1) {
            if (file_exists(DIR_APPLICATION."controller/catalog/vatera.php")){
                require_once(DIR_APPLICATION."controller/catalog/vatera.php");
            }
        }

        $this->response->setOutput($this->render());
    }


    private function validateForm() {
        $megjelenit_admin_altalanos = $this->config->get('megjelenit_admin_altalanos');
        $megjelenit_admin_termekadatok = $this->config->get('megjelenit_admin_termekadatok');


        if (!$this->user->hasPermission('modify', 'catalog/product')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

        if ($this->config->get("megjelenit_admin_nyelvi_ellenorzes") == 1){
            foreach ($this->request->post['product_description'] as $language_id => $value) {
                  if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 255)) {
                    $this->error['name'][$language_id] = $this->language->get('error_name');
                  }
            }
        } else {
            $this->load->model('localisation/language');


            $language_id = $this->model_localisation_language->getLanguageId($this->language->get('code'));

            if ( (utf8_strlen($this->request->post['product_description'][$language_id]['name']) < 3) ||  (utf8_strlen($this->request->post['product_description'][$language_id]['name']) > 255) ) {
                $this->error['name'] = $this->language->get('error_name');
            }
        }

        if ($this->config->get("megjelenit_admin_ar_ellenorzes") == 1){
            if ( $this->request->post['price'] == 0) {
                $this->error['price'] = $this->language->get('error_price');
            }
        }

        if ($megjelenit_admin_altalanos['ketszintu_szuro_admin'] == 1){
            if ($this->config->get("megjelenit_admin_ketszintu_szuro_ellenorzes") == 1){
                if (!isset($this->request->post['product_filter'])) {
                    //$this->error['product_filter'] = $this->language->get('error_product_filter');
                }
            }
        }
        if ($this->config->get('megjelenit_meddig_kaphato')) {
            if ($this->config->get("megjelenit_admin_ervenyes_ig_ellenorzes") == 1){
                if ( $this->request->post['date_ervenyes_ig'] == "0000-00-00" ) {
                    $this->error['date_ervenyes_ig'] = $this->language->get('error_date_ervenyes_ig');
                }
            }
        }


        if(isset($megjelenit_admin_termekadatok['nincs_negativ_ar']) && $megjelenit_admin_termekadatok['nincs_negativ_ar'] == 1) {
            $price = isset($this->request->request['price']) ? (int)$this->request->request['price'] : 0;
            $utalvany = isset($this->request->request['utalvany']) ? $this->request->request['utalvany'] : 0;
            if( ($utalvany == '0') && ($price <= 0) ) {
                $this->error['price'] = $this->language->get('error_negativ_ar');
            }
        }
            /*if ((utf8_strlen($this->request->post['model']) < 1) || (utf8_strlen($this->request->post['model']) > 64)) {
                  $this->error['model'] = $this->language->get('error_model');
            }*/

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}

  	private function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'catalog/product')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}

  	private function validateCopy() {
    	if (!$this->user->hasPermission('modify', 'catalog/product')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])  || isset($this->request->get['filter_category_id'])
            || isset($this->request->get['filter_cikkszam'])  || isset($this->request->get['filter_cikkszam2'])  ) {
			$this->load->model('catalog/product');

            $filter_kivetel = '';
            if (!empty($this->request->request['filter_kivetel'])) {
                $filter_kivetel = $this->request->request['filter_kivetel'];
            }

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			} else {
				$filter_model = '';
			}
            if (isset($this->request->get['filter_cikkszam'])) {
				$filter_cikkszam = $this->request->get['filter_cikkszam'];
			} else {
				$filter_cikkszam = '';
			}
            if (isset($this->request->get['filter_cikkszam2'])) {
				$filter_cikkszam2 = $this->request->get['filter_cikkszam2'];
			} else {
				$filter_cikkszam2 = '';
			}

			if (isset($this->request->get['filter_category_id'])) {
				$filter_category_id = $this->request->get['filter_category_id'];
			} else {
				$filter_category_id = '';
			}

			if (isset($this->request->get['filter_sub_category'])) {
				$filter_sub_category = $this->request->get['filter_sub_category'];
			} else {
				$filter_sub_category = '';
			}

            if (!empty($this->request->get['package_products'])) {
				$filter_package_products = $this->request->get['package_products'];
			} else {
                $filter_package_products = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 20;
			}

			$data = array(
				'filter_name'               => $filter_name,
				'filter_model'              => $filter_model,
				'filter_cikkszam'           => $filter_cikkszam,
				'filter_cikkszam2'          => $filter_cikkszam2,
				'filter_category_id'        => $filter_category_id,
				'filter_sub_category'       => $filter_sub_category,
                'filter_kivetel'            => $filter_kivetel,
                'filter_package_products'   => $filter_package_products,
                'start'                     => 0,
                'limit'                     => $limit
			);

			$results = $this->model_catalog_product->getProducts($data);

			foreach ($results as $key=>$result) {
                if ( isset($_REQUEST['product_id']) && $result['product_id'] == $_REQUEST['product_id']) {
                    false;
                } elseif ( isset($_REQUEST['product_id']) &&  count($this->model_catalog_product->getProductPackage($result['product_id'])) > 0) {
                    false;
                } else {
                    $option_data = array();

                    $product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

                    foreach ($product_options as $product_option) {
                        if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image' || $product_option['type'] == 'checkbox_qty') {
                            $option_value_data = array();

                            foreach ($product_option['product_option_value'] as $product_option_value) {
                                $option_value_data[] = array(
                                    'product_option_value_id' => $product_option_value['product_option_value_id'],
                                    'option_value_id'         => $product_option_value['option_value_id'],
                                    'name'                    => $product_option_value['name'],
                                    'price'                   => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
                                    'price_prefix'            => $product_option_value['price_prefix']
                                );
                            }

                            $option_data[] = array(
                                'product_option_id' => $product_option['product_option_id'],
                                'option_id'         => $product_option['option_id'],
                                'name'              => $product_option['name'],
                                'type'              => $product_option['type'],
                                'option_value'      => $option_value_data,
                                'required'          => isset($product_option['required']) ? $product_option['required'] : ''
                            );
                        } else {
                            $option_data[] = array(
                                'product_option_id' => $product_option['product_option_id'],
                                'option_id'         => $product_option['option_id'],
                                'name'              => $product_option['name'],
                                'type'              => $product_option['type'],
                                'option_value'      => $product_option['option_value'],
                                'required'          => isset($product_option['required']) ? $product_option['required'] : ''
                            );
                        }
                    }

                    $name = strlen($result['name']) > 100 ? substr($result['name'],0,100).'...' : $result['name'];



                    $name = strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'));
                    $name=str_replace("&nbsp;","",$name);
                    $name=str_replace("\r","",$name);
                    $name=str_replace("\t","",$name);
                    $name=str_replace("\n","",$name);
                    $name=trim($name);
                    $name=  strlen($name) > 30 ? mb_substr($name, 0, 30, "UTF-8").'...' : $name;


                    $json[] = array(
                        'product_id'    => $result['product_id'],
                        'name'          => $name,
                        'model'         => $result['model'],
                        'cikkszam'      => $result['cikkszam'],
                        'cikkszam2'     => $result['cikkszam2'],
                        'option'        => $option_data,
                        'price'         => $result['price'],

                        'price_format'      => $this->currency->format($result['price']),

                        'package_price'             => round($result['price']),
                        'package_price_format'      => $this->currency->format($result['price']),
                        'package_price_row'         => $result['price'],
                        'package_price_row_format'  => $this->currency->format($result['price']),

                        'status'        => $result['status'],
                        'price_format'  => $this->currency->format($result['price'], $this->config->get('config_currency'))
                    );
                }
			}
		}

		$this->response->setOutput(json_encode($json));
	}


    public function customer_autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])  || isset($this->request->get['filter_category_id'])
            || isset($this->request->get['filter_customer'])  || isset($this->request->get['filter_cikkszam2'])  ) {
            $this->load->model('catalog/product');

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['filter_model'])) {
                $filter_model = $this->request->get['filter_model'];
            } else {
                $filter_model = '';
            }
            if (isset($this->request->get['filter_customer'])) {
                $filter_customer = $this->request->get['filter_customer'];
            } else {
                $filter_customer = '';
            }


            if (isset($this->request->get['filter_category_id'])) {
                $filter_category_id = $this->request->get['filter_category_id'];
            } else {
                $filter_category_id = '';
            }

            if (isset($this->request->get['filter_sub_category'])) {
                $filter_sub_category = $this->request->get['filter_sub_category'];
            } else {
                $filter_sub_category = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 20;
            }

            $data = array(
                'filter_name'         => $filter_name,
                'filter_model'        => $filter_model,
                'filter_category_id'  => $filter_category_id,
                'filter_customer'     => $filter_customer,
                'filter_sub_category' => $filter_sub_category,
                'start'               => 0,
                'limit'               => $limit
            );

            $results = $this->model_catalog_product->getCustomerProducts($data);

            foreach ($results as $result) {
                $option_data = array();

                $product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

                foreach ($product_options as $product_option) {
                    if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                        $option_value_data = array();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $option_value_data[] = array(
                                'product_option_value_id' => $product_option_value['product_option_value_id'],
                                'option_value_id'         => $product_option_value['option_value_id'],
                                'name'                    => $product_option_value['name'],
                                'price'                   => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
                                'price_prefix'            => $product_option_value['price_prefix']
                            );
                        }

                        $option_data[] = array(
                            'product_option_id' => $product_option['product_option_id'],
                            'option_id'         => $product_option['option_id'],
                            'name'              => $product_option['name'],
                            'type'              => $product_option['type'],
                            'option_value'      => $option_value_data,
                            'required'          => isset($product_option['required']) ? $product_option['required'] : ''
                        );
                    } else {
                        $option_data[] = array(
                            'product_option_id' => $product_option['product_option_id'],
                            'option_id'         => $product_option['option_id'],
                            'name'              => $product_option['name'],
                            'type'              => $product_option['type'],
                            'option_value'      => $product_option['option_value'],
                            'required'          => isset($product_option['required']) ? $product_option['required'] : ''
                        );
                    }
                }

                $json[] = array(
                    'product_id' => $result['product_id'],
                    'name'       => html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'),
                    'model'      => $result['model'],
                    'customer'   => $result['firstname']." " .$result['lastname'],
                    'option'     => $option_data,
                    'price'      => $result['price']
                );
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    function attributesInGroup(){
        $this->load->model('catalog/attribute');
        $data=array(
            'filter_name'               => "",
            'filter_attribute_group_id' => $this->request->request['attribute_group_id'],
            'sort'                      => 'ad.name',
            'limit'                     => 1000,
            'start'                     => 0
        );
        $result = $this->model_catalog_attribute->getAttributes($data);
        $json = array();
        foreach($result as $value) {
            $json[] = array(
                'attribute_id'  => $value['attribute_id'],
                'name'          => $value['name']
            );
        }
        $this->response->setOutput(json_encode($json));
    }

    public function csvExportAttribute() {

        if (!isset($this->request->request['modell_szuro']) || empty($this->request->request['modell_szuro']) ) {
            $this->session->data['warning'] = 'Hiba! Kérem adja meg a termékek modell számát, vagy annak egy részét (elejétől).';
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
        }

        if ( isset($this->request->request['csv_status']) ){
            $csv_status = $this->request->request['csv_status'];
            $status = ($csv_status == "0") ? false : true;;
            $csv_status = ($csv_status == "2") ? "0" : $csv_status;
        }

        if ( isset($this->request->request['modell_szuro']) && !empty($this->request->request['modell_szuro']) ) {
            if ($status) {
                $data = array(
                    'filter_model'	  => $this->request->request['modell_szuro'],
                    'filter_status'   => $csv_status,
                    'sort'            => "p.name",
                    'start'           => 0,
                    'limit'           => 10000,
                    'kapcsolodas'     => "attributes"

                );
            } else {
                $data = array(
                    'filter_model'	  => $this->request->request['modell_szuro'],
                    'sort'            => "p.name",
                    'start'           => 0,
                    'limit'           => 10000,
                    'kapcsolodas'     => "attributes"

                );
            }

            $this->load->model('catalog/product');
            $products = $this->model_catalog_product->getProductsInAttributes($data);
            if ($products) {
                $this->load->model('catalog/attribute');
                $attributes =  $this->model_catalog_attribute->getAttributes();
                $this->model_catalog_product->addCsvProductsAttributes($products,$attributes);
            } else {
                $this->session->data['warning'] = 'Hiba! A megadott adatok alapján nincs kigyüjthető termék.';
                $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));

            }
        }

    }

    public function csvExportKapcsolodo() {

        if (!isset($this->request->request['modell_szuro']) || empty($this->request->request['modell_szuro']) ) {
            $this->session->data['warning'] = 'Hiba! Kérem adja meg a termékek modell számát, vagy annak egy részét (elejétől).';
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
        }

        if (!isset($this->request->request['modell_szuro_termekek'])  || empty($this->request->request['modell_szuro_termekek']) ) {
            $this->session->data['warning'] = 'Hiba! Kérem adja meg a kapcsolódó termékek modell számát, vagy annak egy részét (elejétől).';
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
        }


        if ( isset($this->request->request['csv_status']) ){
            $csv_status = $this->request->request['csv_status'];
            $status = ($csv_status == "0") ? false : true;;
            $csv_status = ($csv_status == "2") ? "0" : $csv_status;
        }

        if ( isset($this->request->request['csv_status_termekek']) ){
            $csv_status_termekek = $this->request->request['csv_status_termekek'];
            $status_termekek = ($csv_status_termekek == "0") ? false : true;;
            $csv_status_termekek = ($csv_status_termekek == "2") ? "0" : $csv_status_termekek;
        }

        if ( isset($this->request->request['modell_szuro']) && !empty($this->request->request['modell_szuro']) ) {
            if ($status) {
                if ($status_termekek) {
                    $data = array(
                        'filter_model'	            => $this->request->request['modell_szuro'],
                        'filter_model_termekek'	    => $this->request->request['modell_szuro_termekek'],
                        'filter_status'             => $csv_status,
                        'filter_status_termekek'    => $csv_status_termekek,
                        'sort'                      => "p.name",
                        'start'                     => 0,
                        'limit'                     => 10000,
                        'kapcsolodas'               => "kapcsolodo"
                    );

                } else {
                    $data = array(
                        'filter_model'	  => $this->request->request['modell_szuro'],
                        'filter_model_termekek'	    => $this->request->request['modell_szuro_termekek'],
                        'filter_status'   => $csv_status,
                        'sort'            => "p.name",
                        'start'           => 0,
                        'limit'           => 10000,
                        'kapcsolodas'     => "kapcsolodo"
                    );
                }
            } else {
                if ($status_termekek) {
                    $data = array(
                        'filter_model'	            => $this->request->request['modell_szuro'],
                        'filter_model_termekek'	    => $this->request->request['modell_szuro_termekek'],
                        'filter_status_termekek'    => $csv_status_termekek,
                        'sort'                      => "p.name",
                        'start'                     => 0,
                        'limit'                     => 10000,
                        'kapcsolodas'               => "kapcsolodo"

                    );
                } else {
                    $data = array(
                        'filter_model'	        => $this->request->request['modell_szuro'],
                        'filter_model_termekek'	=> $this->request->request['modell_szuro_termekek'],
                        'sort'                  => "p.name",
                        'start'                 => 0,
                        'limit'                 => 10000,
                        'kapcsolodas'           => "kapcsolodo"

                    );
                }
            }

            $this->load->model('catalog/product');
            $products = $this->model_catalog_product->getProductsInRelateds($data);
            if ($products) {
                $this->load->model('catalog/attribute');
                $this->model_catalog_product->addCsvProductsReleted($products['termekek'], $products['kapcsolodo'],"kapcsolodo");
            } else {
                $this->session->data['warning'] = 'Hiba! A megadott adatok alapján nincs kigyüjthető termék.';
                $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
            }
        }

    }

    public function csvExportAjanlott() {

        if (!isset($this->request->request['modell_szuro']) || empty($this->request->request['modell_szuro']) ) {
            $this->session->data['warning'] = 'Hiba! Kérem adja meg a termékek modell számát, vagy annak egy részét (elejétől).';
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
        }

        if (!isset($this->request->request['modell_szuro_ajanlott_termekek'])  || empty($this->request->request['modell_szuro_ajanlott_termekek']) ) {
            $this->session->data['warning'] = 'Hiba! Kérem adja meg a kapcsolódó termékek modell számát, vagy annak egy részét (elejétől).';
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
        }


        if ( isset($this->request->request['csv_status']) ){
            $csv_status = $this->request->request['csv_status'];
            $status = ($csv_status == "0") ? false : true;;
            $csv_status = ($csv_status == "2") ? "0" : $csv_status;
        }

        if ( isset($this->request->request['csv_status_ajanlott_termekek']) ){
            $csv_status_termekek = $this->request->request['csv_status_ajanlott_termekek'];
            $status_termekek = ($csv_status_termekek == "0") ? false : true;;
            $csv_status_termekek = ($csv_status_termekek == "2") ? "0" : $csv_status_termekek;
        }

        if ($status) {
            if ($status_termekek) {
                $data = array(
                    'filter_model'	            => $this->request->request['modell_szuro'],
                    'filter_model_termekek'	    => $this->request->request['modell_szuro_ajanlott_termekek'],
                    'filter_status'             => $csv_status,
                    'filter_status_termekek'    => $csv_status_termekek,
                    'sort'                      => "p.name",
                    'start'                     => 0,
                    'limit'                     => 10000,
                    'kapcsolodas'               => "kapcsolodo"
                );

            } else {
                $data = array(
                    'filter_model'	  => $this->request->request['modell_szuro'],
                    'filter_model_termekek'	    => $this->request->request['modell_szuro_ajanlott_termekek'],
                    'filter_status'   => $csv_status,
                    'sort'            => "p.name",
                    'start'           => 0,
                    'limit'           => 10000,
                    'kapcsolodas'     => "kapcsolodo"
                );
            }
        } else {
            if ($status_termekek) {
                $data = array(
                    'filter_model'	            => $this->request->request['modell_szuro'],
                    'filter_model_termekek'	    => $this->request->request['modell_szuro_ajanlott_termekek'],
                    'filter_status_termekek'    => $csv_status_termekek,
                    'sort'                      => "p.name",
                    'start'                     => 0,
                    'limit'                     => 10000,
                    'kapcsolodas'               => "kapcsolodo"

                );
            } else {
                $data = array(
                    'filter_model'	        => $this->request->request['modell_szuro'],
                    'filter_model_termekek'	=> $this->request->request['modell_szuro_ajanlott_termekek'],
                    'sort'                  => "p.name",
                    'start'                 => 0,
                    'limit'                 => 10000,
                    'kapcsolodas'           => "kapcsolodo"

                );
            }
        }

        $this->load->model('catalog/product');
        $products = $this->model_catalog_product->getProductsInAjanlott($data);
        if ($products) {
            $this->load->model('catalog/attribute');
            $this->model_catalog_product->addCsvProductsReleted($products['termekek'], $products['kapcsolodo'], "ajanlott_tartozekok");
        } else {
            $this->session->data['warning'] = 'Hiba! A megadott adatok alapján nincs kigyüjthető termék.';
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
        }
    }


    public function csvExportAltalanos() {

        if (!isset($this->request->request['modell_szuro']) || empty($this->request->request['modell_szuro']) ) {
            $this->session->data['warning'] = 'Hiba! Kérem adja meg a termékek modell számát, vagy annak egy részét (elejétől).';
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
        }

        if (!isset($this->request->request['modell_szuro_altalanos_termekek'])  || empty($this->request->request['modell_szuro_altalanos_termekek']) ) {
            $this->session->data['warning'] = 'Hiba! Kérem adja meg a kapcsolódó termékek modell számát, vagy annak egy részét (elejétől).';
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
        }


        if ( isset($this->request->request['csv_status']) ){
            $csv_status = $this->request->request['csv_status'];
            $status = ($csv_status == "0") ? false : true;;
            $csv_status = ($csv_status == "2") ? "0" : $csv_status;
        }

        if ( isset($this->request->request['csv_status_altalanos_termekek']) ){
            $csv_status_termekek = $this->request->request['csv_status_altalanos_termekek'];
            $status_termekek = ($csv_status_termekek == "0") ? false : true;;
            $csv_status_termekek = ($csv_status_termekek == "2") ? "0" : $csv_status_termekek;
        }

        if ($status) {
            if ($status_termekek) {
                $data = array(
                    'filter_model'	            => $this->request->request['modell_szuro'],
                    'filter_model_termekek'	    => $this->request->request['modell_szuro_altalanos_termekek'],
                    'filter_status'             => $csv_status,
                    'filter_status_termekek'    => $csv_status_termekek,
                    'sort'                      => "p.name",
                    'start'                     => 0,
                    'limit'                     => 10000,
                    'kapcsolodas'               => "kapcsolodo"
                );

            } else {
                $data = array(
                    'filter_model'	  => $this->request->request['modell_szuro'],
                    'filter_model_termekek'	    => $this->request->request['modell_szuro_altalanos_termekek'],
                    'filter_status'   => $csv_status,
                    'sort'            => "p.name",
                    'start'           => 0,
                    'limit'           => 10000,
                    'kapcsolodas'     => "kapcsolodo"
                );
            }
        } else {
            if ($status_termekek) {
                $data = array(
                    'filter_model'	            => $this->request->request['modell_szuro'],
                    'filter_model_termekek'	    => $this->request->request['modell_szuro_altalanos_termekek'],
                    'filter_status_termekek'    => $csv_status_termekek,
                    'sort'                      => "p.name",
                    'start'                     => 0,
                    'limit'                     => 10000,
                    'kapcsolodas'               => "kapcsolodo"

                );
            } else {
                $data = array(
                    'filter_model'	        => $this->request->request['modell_szuro'],
                    'filter_model_termekek'	=> $this->request->request['modell_szuro_altalanos_termekek'],
                    'sort'                  => "p.name",
                    'start'                 => 0,
                    'limit'                 => 10000,
                    'kapcsolodas'           => "kapcsolodo"

                );
            }
        }

        $this->load->model('catalog/product');
        $products = $this->model_catalog_product->getProductsInAltalanos($data);
        if ($products) {
            $this->load->model('catalog/attribute');
            $this->model_catalog_product->addCsvProductsReleted($products['termekek'], $products['kapcsolodo'], "altalanos_tartozekok");
        } else {
            $this->session->data['warning'] = 'Hiba! A megadott adatok alapján nincs kigyüjthető termék.';
            $this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] , 'SSL'));
        }
    }

    public function csvImport() {

        $json = array();

        $handle = fopen($this->request->request['csv_file'], "r");
        if ($handle) {
            if ( substr(basename($this->request->request['csv_file']),0,8) == "jellemzo" ) {
                $data = fgets($handle);
                $data = fgets($handle);
                $data = fgetcsv($handle, NULL, ";");

                $csv_tartalom = array();
                $l = 0;
                while (($sor = fgetcsv($handle, NULL, ";"))) {
                    for ($i=0; count($sor) > $i; $i++) {
                        if ($i == 0) {
                            $csv_tartalom[$l]['product_id'] = $sor[$i];
                        } elseif ($i == 1) {
                            $csv_tartalom[$l]['model'] = $sor[$i];
                        } elseif ($i == 2) {
                            $csv_tartalom[$l]['name'] = $sor[$i];
                        } else {
                            $csv_tartalom[$l][$data[$i]] = $sor[$i];
                        }
                    }

                    $l++;
                }
                fclose($handle);
                $this->load->model('catalog/product');
                $this->model_catalog_product->csvImportAttribute($csv_tartalom);
            } elseif (  substr(basename($this->request->request['csv_file']),0,8) == "kapcsolo" ||
                        substr(basename($this->request->request['csv_file']),0,8) == "altalano" ||
                        substr(basename($this->request->request['csv_file']),0,8) == "ajanlott") {
                $csv_tartalom = array();
                $l = 0;
                while (($sor = fgetcsv($handle, NULL, ";"))) {
                    for ($i=0; count($sor) > $i; $i++) {
                        if ($i == 0) {
                            $csv_tartalom[$l]['product_id'] = $sor[$i];
                        } elseif ($i == 1) {
                            $csv_tartalom[$l]['model'] = $sor[$i];
                        } elseif ($i == 2) {
                            $csv_tartalom[$l]['name'] = $sor[$i];
                        } else {
                            $csv_tartalom[$l][$i] = $sor[$i];
                        }
                    }

                    $l++;
                }
                fclose($handle);
                $this->load->model('catalog/product');

                switch( substr(basename($this->request->request['csv_file']),0,8) ){
                    case 'kapcsolo':
                        $this->model_catalog_product->csvImportRelated($csv_tartalom);
                        break;

                    case 'ajanlott':
                        $this->model_catalog_product->csvImportAjanlott($csv_tartalom);
                        break;

                    case 'altalano':
                        $this->model_catalog_product->csvImportAltalanos($csv_tartalom);
                        break;

                }
            }


            if ( count($csv_tartalom) > 0) {
                $json["success"] = "Sikerült! A CSV file importálása befelyeződött.";
            } else {
                $json["error"] = "Hiba történt! A CSV file importálása nem sikerült.";

            }

        }
        unlink($this->request->request['csv_file']);

        $json['csv_files'] = array();
        if (is_dir(DIR_ARUHAZ. 'csv/jellemzok_import')) {
            $csv_files = glob(DIR_ARUHAZ . 'csv/jellemzok_import/*.csv');
        }

        if ( isset($csv_files) && is_array($csv_files) && count($csv_files) > 0) {
            foreach($csv_files as $csv) {
                $json['csv_files'][] =  $csv;
                $json['csv_name'][] =  basename($csv,'.csv');
            }
        } else {
            $json['csv_files'] =  false;;
        }

        $this->response->setOutput(json_encode($json));

    }

    public function valasztek() {

        $json = array();
        if ($this->request->get['elso'] == "true") {
            if (isset($this->session->data['termek'])) {
                unset ($this->session->data['termek']);
            }
        }
        if (!isset($this->session->data['termek'])) {
            $this->session->data['termek'] = $this->request->post;
        } else {
            foreach($this->request->post as $key=>$option) {
                if (is_array($option)) {
                    foreach($option as $opt_key=>$value) {
                        if (is_array($value)) {
                            foreach($value as $po_key=>$p_option_value) {
                                if (is_array($p_option_value)) {
                                    foreach($p_option_value as $product_key=>$product_option_value) {
                                        if (is_array($product_option_value)) {
                                            foreach($product_option_value as $product_key_zaro=>$option_value_zaro) {
                                                $this->session->data['termek'][$key][$opt_key][$po_key][$product_key][$product_key_zaro] = $option_value_zaro;
                                            }
                                        } else { $this->session->data['termek'][$key][$opt_key][$po_key][$product_key] = $product_option_value; }
                                    }
                                } else { $this->session->data['termek'][$key][$opt_key][$po_key] = $p_option_value; }
                            }
                        } else { $this->session->data['termek'][$key][$opt_key] = $value; }
                    }
                } else { $this->session->data['termek'][$key] = $option; }
            }
        }

        if ($this->request->get['utolso'] == "true" ) {
            $this->load->model('catalog/product');
            $data = $this->session->data['termek'];
            unset ($this->session->data['termek']);

            if (!empty($this->request->get["product_id"])) {
                $this->model_catalog_product->editProduct($this->request->get['product_id'], $data);
            } else {
                $this->model_catalog_product->addProduct($data);
            }
        }

        $this->response->setOutput(json_encode($json));

    }

    public function CrmTipus() {
        $json = array();
        $json['categoryes'] = array();
        $json['products'] = array();

        $this->load->language("catalog/product");
        if (isset($this->request->request['crm_tipus'])) {
            $this->load->model('catalog/crm_kapcsolat');

            if ($this->request->request['crm_tipus'] == 1) {
                $json['categoryes'] = $this->model_catalog_crm_kapcsolat->getCrmProductCategoryes();
                $json['products'] = $this->model_catalog_crm_kapcsolat->getCrmProducts();

            } elseif ($this->request->request['crm_tipus'] == 2) {
                $json['categoryes'] = $this->model_catalog_crm_kapcsolat->getCrmServiceCategoryes();
                $json['products'] = $this->model_catalog_crm_kapcsolat->getCrmServices();

            } elseif ($this->request->request['crm_tipus'] == 3) {
                $json['products'] = $this->model_catalog_crm_kapcsolat->getCrmCsomag();
            }

        } else {
            $json['error']['crmtipus'] = $this->language->get('not_crmtipus');
        }
        $this->response->setOutput(json_encode($json));
    }

    public function CrmCategory() {
        $json = array();
        $json['products'] = array();

        $this->load->language("catalog/product");
        if (isset($this->request->request['crm_tipus']) && isset($this->request->request['crm_category_id'])) {
            $this->load->model('catalog/crm_kapcsolat');

            if ($this->request->request['crm_tipus'] == 1) {
                if (!empty($this->request->request['crm_category_id'])) {
                    $json['products'] = $this->model_catalog_crm_kapcsolat->getCrmProducts($this->request->request['crm_category_id']);
                }

            } elseif ($this->request->request['crm_tipus'] == 2) {
                if (!empty($this->request->request['crm_category_id'])) {
                    $json['products'] = $this->model_catalog_crm_kapcsolat->getCrmServices($this->request->request['crm_category_id']);
                }
            }

        } else {
            $json['error']['crmtipus'] = $this->language->get('not_crmtipus');
        }
        $this->response->setOutput(json_encode($json));
    }

    public function getCategoryChild() {
        $kategoriak = array();
        $category_id = $this->request->get['filter_category_id'];
        $kategoriak[] = $category_id;

        $sql = "SELECT category_id FROM ".DB_PREFIX."category WHERE parent_id='".$category_id."'";
        $query = $this->db->query($sql);
        if ($query->rows) {
            foreach($query->rows as $value1) {
                $kategoriak[] = $value1['category_id'];
                $sql = "SELECT category_id FROM ".DB_PREFIX."category WHERE parent_id='".$value1['category_id']."'";
                $query = $this->db->query($sql);
                if ($query->rows) {
                    foreach($query->rows as $value2) {
                        $kategoriak[] = $value2['category_id'];
                        $sql = "SELECT category_id  FROM ".DB_PREFIX."category WHERE parent_id='".$value2['category_id']."'";
                        $query = $this->db->query($sql);
                        if ($query->rows) {
                            foreach($query->rows as $value3) {
                                $kategoriak[] = $value3['category_id'];
                                $sql = "SELECT category_id  FROM ".DB_PREFIX."category WHERE parent_id='".$value3['category_id']."'";
                                $query = $this->db->query($sql);
                                if ($query->rows) {
                                    foreach($query->rows as $value4) {
                                        $kategoriak[] = $value4['category_id'];
                                        $sql = "SELECT category_id  FROM ".DB_PREFIX."category WHERE parent_id='".$value4['category_id']."'";
                                        $query = $this->db->query($sql);
                                        if ($query->rows) {
                                            foreach($query->rows as $value5) {
                                                $kategoriak[] = $value5['category_id'];
                                                $sql = "SELECT category_id  FROM ".DB_PREFIX."category WHERE parent_id='".$value5['category_id']."'";
                                                $query = $this->db->query($sql);
                                                if ($query->rows) {
                                                    foreach($query->rows as $value6) {
                                                        $kategoriak[] = $value6['category_id'];
                                                        $sql = "SELECT category_id  FROM ".DB_PREFIX."category WHERE parent_id='".$value6['category_id']."'";
                                                        $query = $this->db->query($sql);
                                                        if ($query->rows) {
                                                            foreach($query->rows as $value7) {
                                                                $kategoriak[] = $value7['category_id'];
                                                                $sql = "SELECT category_id  FROM ".DB_PREFIX."category WHERE parent_id='".$value7['category_id']."'";
                                                                $query = $this->db->query($sql);
                                                                if ($query->rows) {
                                                                    foreach($query->rows as $value8) {
                                                                        $kategoriak[] = $value8['category_id'];
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->response->setOutput(json_encode($kategoriak));
    }

    public function modosit() {
        $this->load->language('catalog/product');
        $siker = false;
        $json = array();

        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if (empty($json['error']) && !empty($_POST['eljaras'])) {
            $sql = str_replace('modositas_ertek',$_POST['ertek'],$_POST['eljaras']);
            $siker = $this->db->query($sql);
            if ($siker) {
                $json['success'] =  $this->language->get('success_modositas');
            } else {
                $json['error'] = $this->language->get('error_sql');

            }
        } elseif (empty($json['error'])) {
            $json['error'] = $this->language->get('error_post');

        }

        $this->response->setOutput(json_encode($json));

    }



}
?>