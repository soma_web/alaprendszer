/* A bal oldali szűrőt fixre állítja */
/* Start */
$(document).ready(function(){
    var valtozo = 0;
    $(window).scroll(function(){
        if($(window).width() > 1238){

                var baloszlopmagas = $("#column-left").height();
                var baloszlopszeles = $("#column-left").width();
                var headermagas = $("#header_hatternek").height();
                var paypalmarginbottom = parseInt($(".paypal p:nth-child(2)").css("margin-bottom"));
                var breadcrumb = $(".breadcrumb").height();
                var scrolltop = $("body").scrollTop();
                var innnerheight = $(window).innerHeight();
                var headermargin = 27;

                if($(".breadcrumb").length > 0){
                    var proba2 = (baloszlopmagas+headermagas)+breadcrumb;

                }
                else{
                    var proba2 = (baloszlopmagas+headermagas)-10;
                }



                if(((scrolltop+innnerheight) >= (proba2)) && (proba2 > $(window).height())){
                    if(valtozo == 0){
                        $("#column-left").css("bottom",-paypalmarginbottom);
                        $("#column-left").css("position","fixed");
                        var baloszlop = $(("<div id='baloszlop'></div>"))[0];
                        $("#column-left")[0].parentNode.insertBefore(baloszlop, $("#column-left")[0]);
                        $("#baloszlop").css({
                            "width":baloszlopszeles,
                            "height":baloszlopmagas,
                            "float":"left"
                        });
                        valtozo++;
                    }
                }
                else{
                    $("#baloszlop").remove();
                    $("#column-left").removeAttr( 'style' );
                    valtozo = 0;
                }
        }
    });
});
/* Vége */