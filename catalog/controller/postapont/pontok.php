<?php
class ControllerPostapontPontok extends Controller {
	
	public function sendAJAX(){
		
		$this->load->model('shipping/postapont');
		$postapont_code = $this->request->get['boltdata'];
		
		if (!empty($postapont_code)){
			$this->session->data['postapont_point'] = $postapont_code;
			$this->session->data['shipping_methods']['postapont']['quote']['postapont']['title'] = $postapont_code;
			echo json_encode($postapont_code);
		}
	}
	
}
?>