<?php
/*
Developer: Ekrem KAYA
Web Page: www.e-piksel.com
Free Page: free.e-piksel.com
Lisence: GNU General Public License version 3 (GPLv3)
*/

// Heading
$_['heading_title_normal']= 'Érkeztető oldal Module';
$_['heading_title']       = '<font color="green"><b>Érkeztető oldal modul</b></font>';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikerült: Az Érkeztető oldal modul módosítása sikerült!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_content_header'] = 'Fejléc';
$_['text_kosar']          = 'Kosárba';
$_['text_penztar']        = 'Pénztárba';

$_['text_or']			  = 'vagy';
$_['text_create_paypal']  = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=YS4L83BNSEC6A\');">Create your PayPal account</a>';
$_['text_developer']	  = 'Fejlesztő:';
$_['text_html_version']	  = 'Érkeztető oldal Module 1.1';

// Entry
$_['entry_heading']		  = 'Oldal URL:';
$_['entry_description']   = 'HTML Code / Message:';
$_['entry_layout']        = 'Megjelenés helye:';
$_['entry_position']      = 'Pozíció:';
$_['entry_box_status']    = 'Keret engedélyetés:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_minden_oldal']  = 'Fejléc kikapcsolva:';
$_['entry_kosar_penztar'] = 'Érkeztetés:';
$_['entry_product_id']    = 'Product ID:';

// Error
$_['error_product_id']    = 'Kérem, adja meg at termék azonosítóját!';
$_['error_not_exist_product_id_']= 'Hibás azonosító! Kérem, módosítsa!';
$_['error_permission']    = 'Figyelmeztetés: Önnek nincs jogosultsága a módosításra!';
$_['error_warning']       = 'Figyelem: Kérjük gondosan nézze át a hibákat az űrlapon!';

?>