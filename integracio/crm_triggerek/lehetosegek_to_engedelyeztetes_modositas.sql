begin
  DECLARE result int(10);

  IF (NEW.sales_stage = "Engedelyeztetes" AND OLD.sales_stage != "Engedelyeztetes") THEN

     INSERT INTO vtiger_potential_engedelyeztetes set
      potentialid = NEW.potentialid,
      sales_stage = NEW.sales_stage;

  END IF;

  IF (NEW.sales_stage = "Teljes rendszer" AND OLD.sales_stage != "Teljes rendszer") THEN

        INSERT INTO vtiger_salesorder90 set
            potentialid = NEW.potentialid,
            sales_stage = NEW.sales_stage;

  END IF;

END