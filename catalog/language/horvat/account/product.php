<?php
// Heading
$_['heading_title']          = 'Termékeim';

// Text
$_['text_success']           = 'Sikerült: A termékek módosítása megtörtént!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Alapértelmezett';
$_['text_image_manager']     = 'Kép szerkesztő';
$_['text_browse']            = 'Tallózás';
$_['text_clear']             = 'Törlés';
$_['text_option']            = 'Opció';
$_['text_option_value']      = 'Opció érték';
$_['text_percent']           = 'Százalék';
$_['text_amount']            = 'Fix összeg';
$_['text_kupon_keszit']      = 'Kupon kép elkészítése';
$_['text_prioritas']         = 'Dátum átfedés esetén prioritás:';
$_['text_fizetendo']         = 'Fizetendő: ';
$_['text_osszesen']          = 'Összesen: ';

$_['elhelyezkedesek_kulonbseg_error']         = 'Figyelmeztetés! Az adott időszakra már nincs szabad hely.';
$_['elhelyezkedes_alcsoport_kulonbseg_error'] = 'Figyelmeztetés! Az adott időszakra már nincs szabad hely.';
$_['kiemelesek_kulonbseg_error']              = 'Figyelmeztetés! Az adott időszakban már nincs lehetőség a kiemelésre.';

// Column
$_['column_name']            = 'Termék neve';
$_['column_model']           = 'Cikkszám';
$_['column_image']           = 'Kép';
$_['column_price']           = 'Ár';
$_['column_quantity']        = 'Mennyiség';
$_['column_status']          = 'Státusz';
$_['column_action']          = 'Action';

// Entry
$_['entry_kep_tiltas']       = 'Kupon séma:';
$_['entry_image_download']   = 'Kapcsolódó letölthető kép:';
$_['entry_name']             = 'Termék neve:';
$_['entry_meta_keyword'] 	 = 'Meta Tag kulcsszavak:';
$_['entry_meta_description'] = 'Meta Tag leírás:';
$_['entry_description']      = 'Leírás:';
$_['entry_store']            = 'Áruházak:';
$_['entry_keyword']          = 'SEO kulcsszavak:<br /><span class="help">Ne használjon szóközt helyette elválasztó jeleket - és ellenőrizze, hogy a kulcsszó globálisan egyedi.</span>';
$_['entry_model']            = 'Model:';
$_['entry_sku']              = 'Egyedi azonosító:';
$_['entry_upc']              = 'Vonalkód:';
$_['entry_ean']              = 'EAN:<br/><span class="help">European Article Number</span>';
$_['entry_jan']              = 'JAN:<br/><span class="help">Japanese Article Number</span>';
$_['entry_isbn']             = 'ISBN:<br/><span class="help">International Standard Book Number</span>';
$_['entry_mpn']              = 'MPN:<br/><span class="help">Manufacturer Part Number</span>';
$_['entry_location']         = 'Egyéb:';
$_['entry_manufacturer']     = 'Gyártó:<br /><span class="help">(Autocomplete)</span>';
$_['entry_shipping']         = 'Szállítandó termék:';
$_['entry_date_available']   = 'Mikortól kapható:';
$_['entry_date_kaphato_ig']  = 'Lejárat dátuma:';
$_['entry_quantity']         = 'Mennyiség:';
$_['entry_minimum']          = 'Minimális mennyiség :<br/><span class="help">Minimális vásárlási mennyiség</span>';
$_['entry_stock_status']     = 'Hiánycikk állapot: <br/><span class="help">Az az állaptot, amit megjelenik, ha egy termék nincs raktáron</span>';
$_['entry_price']            = 'Ár:';
$_['entry_tax_class']        = 'Áfakulcs:';
$_['entry_points']           = 'Pontok:<br/><span class="help">Ennyi pontra van szükséged a termék megvásárlásához. Amennyiben nincs rás szüksége 0-val kiléphet.</span>';
$_['entry_option_points']    = 'Pontok:';
$_['entry_subtract']         = 'Készlet kivonása::';
$_['entry_weight_class']     = 'Súlymérték:';
$_['entry_weight']           = 'Súly:';
$_['entry_length']           = 'Hosszmérték:';
$_['entry_dimension']        = 'Méret (Hosszúság × Szélesség × Magasság):';
$_['entry_image']            = 'Kép:<br /><span class="help"></span>';
$_['entry_customer_group']   = 'Ügyfél csoport:';
$_['vevo_kedvezmeny']        = 'Ügyfél:';
$_['entry_date_start']       = 'Kezdő dátum:';
$_['entry_date_end']         = 'Záró dátum:';
$_['entry_priority']         = 'Prioritás:';
$_['entry_attribute']        = 'Tulajdonság:';
$_['entry_attribute_group']  = 'Tulajdonság csoport:';
$_['entry_text']             = 'Szöveg:';
$_['entry_option']           = 'Választék:';
$_['entry_option_value']     = 'Választék érték:';
$_['entry_required']         = 'Szükséges:';
$_['entry_status']           = 'Állapot:';
$_['entry_sort_order']       = 'Sorrend:';
$_['entry_category']         = 'Kategóriák:';
$_['entry_download']         = 'Letöltések:';
$_['entry_related']          = 'Kapcsolódó termékek:<br /><span class="help">(Autocomplete)</span>';
$_['entry_tag']   	         = 'Termék címkék:<br /><span class="help">Vesszővel elválasztva</span>';
$_['entry_reward']           = 'Jutalékpont:';
$_['entry_layout']           = 'Elrendezés felülbírálása:';
$_['entry_filter']           = 'Beváltási helyszín:';
$_['entry_szazalek']         = "<span style='margin-left: 20px;'>Kedvezmény százalékban értendő:</span>";
$_['entry_kaphato']         = "<span style='margin-left: 20px;'>Lejárat nélküli:</span>";

//$_['entry_profile']          = 'Profile:';

$_['text_recurring_help']    = 'Recurring amounts are calculated by the frequency and cycles. <br />For example if you use a frequency of "week" and a cycle of "2", then the user will be billed every 2 weeks. <br />The length is the number of times the user will make a payment, set this to 0 if you want payments until they are cancelled.';
$_['text_recurring_title']   = 'Recurring payments';
$_['text_recurring_trial']   = 'Trial period';
$_['entry_recurring']        = 'Recurring billing:';
$_['entry_recurring_price']  = 'Recurring price:';
$_['entry_recurring_freq']   = 'Recurring frequency:';
$_['entry_recurring_cycle']  = 'Recurring cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_recurring_length'] = 'Recurring length:<span class="help">0 = until cancelled</span>';
$_['entry_trial']            = 'Trial period:';
$_['entry_trial_price']      = 'Trial recurring price:';
$_['entry_trial_freq']       = 'Trial recurring frequency:';
$_['entry_trial_cycle']      = 'Trial recurring cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_trial_length']     = 'Trial recurring length:';

$_['text_length_day']        = 'Nap';
$_['text_length_week']       = 'Hét';
$_['text_length_month']      = 'Hónap';
$_['text_length_month_semi'] = 'Semi Month';
$_['text_length_year']       = 'Év';

// Error
$_['error_warning']          = 'Figyelem: Kérjük gondosan nézze át a hibákat az űrlapon!';
$_['error_permission']       = 'Figyelmeztetés: A termékek módosítása az Ön számára nem engedélyezett';
$_['error_name']             = 'A terméknév legalább 3, és legfeljebb 255 karakterből álljon!';
$_['error_model']            = 'A termék gyártmányának megnevezése legalább 3, és legfeljebb 64 karakterből álljon!';

$_['error_date_ervenyes_ig'] = 'Hibás dátum, kérjük ellenőrizze!';
$_['error_product_filter']   = 'Kérem válasszon a listából!';
$_['error_price']            = 'Kérem adjon meg kedvezményt negatív előjellel!';
$_['error_letoltheto']       = 'Kérem töltsön fel, vagy generáljon kupon képet!';

// Locale
$_['code']                          = 'en';
$_['direction']                     = 'ltr';
$_['date_format_short']             = 'd/m/Y';
$_['date_format_long']              = 'l dS F Y';
$_['time_format']                   = 'h:i:s A';
$_['decimal_point']                 = '.';
$_['thousand_point']                = ',';

// Text
$_['text_yes_kep']                  = 'Kép nélkül';
$_['text_no_kep']                   = 'Képes utalvány';

$_['text_yes']                      = 'Igen';
$_['text_no']                       = 'Nem';
$_['text_enabled']                  = 'Engedélyezett';
$_['text_disabled']                 = 'Letiltott';
$_['text_none']                     = ' --- None --- ';
$_['text_select']                   = ' --- Kérem Válasszon --- ';
$_['text_select_all']               = 'Összes Kiválasztása';
$_['text_unselect_all']             = 'Kiválasztás Törlése';
$_['text_all_zones']                = 'Összes zóna';
$_['text_default']                  = ' <b>(Default)</b>';
$_['text_close']                    = 'Bezár';
$_['text_pagination']               = 'Showing {start} to {end} of {total} ({pages} Pages)';
$_['text_no_results']               = 'Nincs Eredmény';
$_['text_separator']                = ' &gt; ';
$_['text_edit']                     = 'Szerkeszt';
$_['kiemel_text']                   = 'Kiemelések';
$_['text_view']                     = 'Megnéz';
$_['text_home']                     = 'Home';

// Button
$_['button_insert']                 = 'Hozzáadás';
$_['button_delete']                 = 'Törlés';
$_['button_save']                   = 'Mentés';
$_['button_cancel']                 = 'Mégse';
$_['button_clear']                  = 'Napló Törlése';
$_['button_close']                  = 'Bezáe';
$_['button_filter']                 = 'Szűrő';
$_['button_send']                   = 'Küld';
$_['button_edit']                   = 'Szerkesztés';
$_['button_copy']                   = 'Másolás';
$_['button_back']                   = 'Vissza';
$_['button_remove']                 = 'Törlés';
$_['button_backup']                 = 'Backup';
$_['button_restore']                = 'Visszaállítás';
$_['button_repair']                 = 'Javítás';
$_['button_upload']                 = 'Feltöltés';
$_['button_submit']                 = 'Küld';
$_['button_invoice']                = 'Számla nyomtatás';
$_['button_add_address']            = 'Cím Hozzáadása';
$_['button_add_attribute']          = 'Tulajdonság hozzáadása';
$_['button_add_banner']             = 'Banner Hozzáadása Banner';
$_['button_add_custom_field_value'] = 'Egyedi mező Hozzáadása';
$_['button_add_product']            = 'Termék Hozzáadása';
$_['button_add_voucher']            = 'Utalvány Hozzáadása';
$_['button_add_filter']             = 'Szűrő Hozzáadása';
$_['button_add_option']             = 'Opció Hozzáadása';
$_['button_add_option_value']       = 'Opció érték Hozzáadása';
$_['button_add_discount']           = 'Kedvezmény Hozzáadása';
$_['button_add_special']            = 'Akció Hozzáadása';
$_['button_add_image']              = 'Kép Hozzáadása';
$_['button_add_geo_zone']           = 'Geo Zone Hozzáadása';
$_['button_add_history']            = 'Előzményekhez';
$_['button_add_transaction']        = 'Tranzakció Hozzáadása';
$_['button_add_total']              = 'Total Hozzáadása Total';
$_['button_add_reward']             = 'Jutalom pontok Hozzáadása';
$_['button_add_route']              = 'Útvonal Hozzáadása';
$_['button_add_rule' ]              = 'Szabály Hozzáadása';
$_['button_add_module']             = 'Modul Hozzáadása';
$_['button_add_link']               = 'Link Hozzáadása';
$_['button_update_total']           = 'Total Frissítése';
$_['button_approve']                = 'Jóváhagy';
$_['button_reset']                  = 'Vissza';
//$_['button_add_profile']            = 'Add Profile';

// Tab
$_['tab_address']                   = 'Cím';
$_['tab_admin']                     = 'Admin';
$_['tab_attribute']                 = 'Tulajdonság';
$_['tab_customer']                  = 'Vásárlói Adatok';
$_['tab_data']                      = 'Adatok';
$_['tab_design']                    = 'Dizájn';
$_['tab_discount']                  = 'Kedvezmény';
$_['tab_general']                   = 'Általános';
$_['tab_history']                   = 'Előzmények';
$_['tab_fraud']                     = 'Csalás';
$_['tab_ftp']                       = 'FTP';
$_['tab_ip']                        = 'IP Cím';
$_['tab_links']                     = 'Linkek';
$_['tab_log']                       = 'Napló';
$_['tab_image']                     = 'Kép';
$_['tab_option']                    = 'Opció';
$_['tab_server']                    = 'szerver';
$_['tab_store']                     = 'Bolt';
$_['tab_special']                   = 'Akció';
$_['tab_local']                     = 'Helyi';
$_['tab_mail']                      = 'Levél';
$_['tab_marketplace_links']         = 'Áruház Linkek';
$_['tab_module']                    = 'Module';
$_['tab_order']                     = 'Rendelés Részletei';
$_['tab_payment']                   = 'Fizetés Részletei';
$_['tab_product']                   = 'Termékek';
$_['tab_return']                    = 'Visszaváltás Részletei';
$_['tab_reward']                    = 'Jutalom Pontok';
//$_['tab_profile']                   = 'Profiles';
$_['tab_shipping']                  = 'Szálítás Részletei';
$_['tab_total']                     = 'Totals';
$_['tab_transaction']               = 'Tranzakciók';
$_['tab_voucher']                   = 'Utalványok';
$_['tab_voucher_history']           = 'Utalvány Előzmények';
$_['tab_price']                     = 'Ár';

// Error
$_['error_upload_1']                = 'Figyelmeztetés: A feltöltött fájl mérete meghaladja a php.ini-ben megadott maximálisan feltölthető fájl méretét!';
$_['error_upload_2']                = 'Figyelmeztetés: A feltöltött fájl mérete meghaladja a html form-ban megadott maximálisan feltölthető fájl méretét!!';
$_['error_upload_3']                = 'Figyelmeztetés: A fájl csak részben lett feltöltve!!';
$_['error_upload_4']                = 'Figyelmeztetés: Nem sikerült a fájl feltöltése!';
$_['error_upload_6']                = 'Figyelmeztetés: Hiányzó ideiglenes könyvtár!';
$_['error_upload_7']                = 'Figyelmeztetés: Nem sikerült a merevlemezre írni!';
$_['error_upload_8']                = 'Figyelmeztetés: Fájlfeltöltést megállította egy kiterjesztés!';
$_['error_upload_999']              = 'Figyelmeztetés: Nincs megfelelő hibakód!';

$_['entry_elhelyezkedes']           = 'Elhelyezkedés I.:';
$_['entry_elhelyezkedes_alcsoport'] = 'Elhelyezkedés II.:';
$_['entry_kiemelt_keret']           = 'Keret:';
$_['entry_idoszak']          = 'Időszak';
$_['entry_idoszak_darab']    = 'Mennyiség';
$_['button_add_elhelyezkedes']    = 'Elhelyezkedés hozzáadása';

$_['text_nap']               = 'Nap';
$_['text_het']               = 'Hét';
$_['text_honap']             = 'Hónap';
$_['text_ev']                = 'Év';

$_['text_biztonsagi_kod_igenyles']          = 'Biztonsági kód igénylése';
$_['text_biztonsagi_kod']                   = 'Biztonsági kód';
$_['button_sending']                        = 'Küldés';
$_['text_biztonsagi_kod_elkuldve']          = 'A biztonsági kód elküldve!';
$_['text_biztonsagi_kod_nincs_elkuldve']    = 'A biztonsági kód elküldése nem sikerült!';
$_['text_sikeres_biztonsagi_kod']           = 'A biztonsági kód megadása sikeres!';
$_['text_sikertelen_biztonsagi_kod']        = 'A biztonsági kód megadása sikertelen!';
$_['text_biztonsagi_kod_kuldese']           = 'Biztonsági kód küldése folyamatban...';
$_['email_biztonsagi_kod_subject']          = 'Új biztonsági kód igénylése';
$_['email_biztonsagi_kod_message']          = 'Az Ön új biztonsági kódja a következő: %s.';
$_['heading_securitycode']                  = 'Biztonsági kód';
$_['button_back']                           = 'Vissza a termékek oldalára';

$_['error_category_mandatory']              = 'A kategória megadása kötelező!';
$_['error_description_mandatory']           = 'A leírás megadása kötelező!';


?>