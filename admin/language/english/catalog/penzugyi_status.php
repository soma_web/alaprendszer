<?php

// Heading
$_['heading_title']       = 'Financial status';
$_['button_insert']       = 'Insert';
$_['button_delete']       = 'Delete';
$_['error_warning']       = '';
$_['success']             = '';
$_['column_megnevezes']   = 'Financial status';
$_['column_sort_order']   = 'Order';
$_['column_action']       = 'Action';
$_['column_status']       = 'Status';


$_['entry_status']          = 'Status:';
$_['entry_sorrend']         = 'Order:';
$_['entry_teljesitett']     = 'Paid status:';
$_['entry_teljesitett_lista'] = ' [Paid status]';

$_['entry_alapertelmezett_lista'] = ' [Default status]';
$_['entry_alapertelmezett'] = 'Default status:';

$_['text_engedelyezett']    = 'Enabled';
$_['text_letiltott']        = 'Disabled';

?>