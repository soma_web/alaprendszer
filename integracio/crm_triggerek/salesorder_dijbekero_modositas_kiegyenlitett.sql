begin
  declare azonosito int(19) default 0;

  IF (NEW.cf_2437 = "Kiegyenlített" AND OLD.cf_2437 != "Kiegyenlített") THEN

    SELECT salesorderid INTO azonosito FROM vtiger_salesorder WHERE sostatus = "Díjbekérő" AND salesorderid = NEW.salesorderid;

    IF (azonosito > 0) THEN

      INSERT INTO vtiger_salesorder_kiegyenlitve set
        salesorderid = NEW.salesorderid,
        sostatus = NEW.cf_2437;

    END IF;
  END IF;
END