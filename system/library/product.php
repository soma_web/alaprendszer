<?php
class Product extends Controller {

	
	public function productPreparation($result,$setting="",$newproduct=array()) {
        if(!$result)
            return false;


        $setting['image_width']  = isset($setting['image_width'])  ? $setting['image_width']  : $this->config->get('config_image_product_width');
        $setting['image_height'] = isset($setting['image_height']) ? $setting['image_height'] : $this->config->get('config_image_product_height');


        $this->load->model("tool/image");
        $megjelenit_admin_altalanos       = $this->config->get('megjelenit_admin_altalanos');

        $negativ_related= "";
        if ($this->config->get('megjelenit_negativ_ar') == "1" && $result['utalvany'] == 1) {
            $negativ_related = "-";
        }

        if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
            $image = $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']);
        } else {
            $image = $this->model_tool_image->resize('no_image.jpg', $setting['image_width'], $setting['image_height']);
        }

        if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
            $image_lista = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
        } else {
            $image_lista = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
        }

        if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
            $width = ($this->config->get('config_image_product_grid_width') > 0) ? $this->config->get('config_image_product_grid_width') : 100;
            $height = ($this->config->get('config_image_product_grid_height') > 0) ? $this->config->get('config_image_product_grid_height') : 100;
            $image_racs = $this->model_tool_image->resize($result['image'], $width, $height);
        } else {
            $width = ($this->config->get('config_image_product_grid_width') > 0) ? $this->config->get('config_image_product_grid_width') : 100;
            $height = ($this->config->get('config_image_product_grid_height') > 0) ? $this->config->get('config_image_product_grid_height') : 100;
            $image_racs = $this->model_tool_image->resize('no_image.jpg', $width, $height);
        }

        if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
            $image_product = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
        } else {
            $image_product = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
        }

        $thumb_list_width    = $this->config->get('config_image_product_width');
        $thumb_list_height   = $this->config->get('config_image_product_height');

        $thumb_grid_width    = $this->config->get('config_image_product_grid_width');
        $thumb_grid_height   = $this->config->get('config_image_product_grid_height');





        $mutat_arat = ($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price') ? true : false;

        if ($result['szazalek'] == 1 && $result['utalvany'] == 1) {
            if ($mutat_arat) {
                $price = $negativ_related.number_format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')),0,".",",")."%";
            } else {
                $price = false;
            }
        } else {
            if ($mutat_arat) {
                $price = $negativ_related.$this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                $simple_price = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
            } else {
                $price = false;
            }
        }

        if ($mutat_arat && !empty($result['special'])) {
            $special = $negativ_related.$this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
            $simple_special = $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'));
        } else {
            $special = false;
        }

        if ($mutat_arat && $this->config->get('config_tax')) {
            $tax = $this->currency->format(!empty($result['special']) ? $result['special'] : $result['price']);
        } else {
            $tax = false;
        }

        if ($this->config->get('config_review_status')) {
            $rating = $result['rating'];
        } else {
            $rating = false;
        }

        $uj=false;
        if (!empty($newproduct) && array_key_exists($result['product_id'],$newproduct)){
            $uj=1;
        }
        $discount_query = $this->model_catalog_product->getProductDiscounts($result['product_id']);
        $discounts = array();
        $legolcsobb_kulcs=0;
        if($result['price'])  {
            foreach ($discount_query as $discount) {
                $discounts[] = array(
                    'quantity' => $discount['quantity'],
                    'netto'    => $this->currency->format($discount['price']),
                    'brutto'    => $this->currency->format($this->tax->calculate($discount['price'], $result['tax_class_id'], $this->config->get('config_tax'))),
                    'ara'    => $this->tax->calculate($discount['price'], $result['tax_class_id'], $this->config->get('config_tax'))
                );
            }
            if (count($discounts) > 0){
                $legolcsobb=$discounts[0]['ara'];
                foreach ($discounts as $key => $value) {
                    if ($value['ara'] < $legolcsobb ){
                        $legolcsobb_kulcs=$key;
                        $legolcsobb=$value['ara'];
                    }
                }
            }
        }
        if (count($discounts) == 0) $discounts[$legolcsobb_kulcs]=false;


        if ($this->config->get('config_pardarab') ) {
            //$pardarab =  ($result['quantity'] <= $this->config->get('config_pardarab') && $result['quantity'] > 0) ? true : false;
            $pardarab =  ($result['quantity'] <= $this->config->get('config_pardarab') ) ? true : false;
        } else {
            //$pardarab =  ($result['quantity'] <= 4 && $result['quantity'] > 0) ? true : false;
            $pardarab =  ($result['quantity'] <= 4 ) ? true : false;
        }

        if ($result['quantity'] <= 0) {
            $stock = $result['stock_status'];
        } elseif ($this->config->get('config_stock_display')) {
            $stock = $result['quantity'];
        } else {
            $stock = $this->language->get('text_instock');
        }


        if($result['quantity'] > 0) {
            $raktaron = true;
        } else {
            $raktaron = false;
        }


        $lista_karakter = $this->config->get('megjelenit_description_lista_karakterek') > 0 ? $this->config->get('megjelenit_description_lista_karakterek') : 200;
        $racs_karakter  = $this->config->get('megjelenit_description_karakterek') > 0 ? $this->config->get('megjelenit_description_karakterek') : 200;

        $descr= strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'));
        $descr=str_replace("&nbsp;","",$descr);
        $descr=str_replace("\r","",$descr);
        $descr=str_replace("\t","",$descr);
        $descr=str_replace("\n","",$descr);
        $descr=trim($descr);
        $descr=mb_substr($descr, 0, 1000, "UTF-8");

        $short_descr= strip_tags(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'));
        $short_descr=str_replace("&nbsp;","",$short_descr);
        $short_descr=str_replace("\r","",$short_descr);
        $short_descr=str_replace("\t","",$short_descr);
        $short_descr=str_replace("\n","",$short_descr);
        $short_descr=trim($short_descr);
        if(strlen($short_descr) >= $lista_karakter) {
            $short_descr_lista  = mb_substr($short_descr, 0, $lista_karakter, "UTF-8");
            $short_descr_lista  .= '...';
        } else {
            $short_descr_lista = $short_descr;
        }

        if(strlen($short_descr) >= $racs_karakter) {
            $short_descr_racs   = mb_substr($short_descr, 0, $racs_karakter, "UTF-8");
            $short_descr_racs  .= '...';
        } else {
            $short_descr_racs = $short_descr;
        }

        $short_descr_lista = $this->fogalomBeepites($short_descr_lista);

        $garancia_description= (!empty($result['garancia_description'])) ? html_entity_decode($result['garancia_description'], ENT_QUOTES, 'UTF-8') : '';

        if (!isset($_GET['token'])) {
            $this->load->model('catalog/multifilter');
            $kiemelt_template   = false;
            $kiemelt_sorrend    = false;
            $kiemelt_al_sorrend = false;
            $kiemeles   = $this->model_catalog_multifilter->getKiemeltTemplate($result['product_id']);

            if ($kiemeles) {
                $kiemelt_template   = $kiemeles['kiemeles_tpl'];
                $kiemelt_sorrend    = $kiemeles['sort_order'];
                $kiemelt_al_sorrend = $kiemeles['alcsoport_sort_order'];
            }
        }


        $utvonal = 'product_id=' . $result['product_id'];
        if (isset($this->request->request['path'])) {
            $utvonal .= '&path='.$this->request->request['path'];
        }

        $result['garancia_ertek'] =  $result['garancia_egyseg'] == 2 ? $result['garancia_ertek']/12 : $result['garancia_ertek'];


        switch ($result['garancia_egyseg']) {
            case 0:
                $text_garancia = "";
                $garancia_kep = "";
                break;
            case 1:
                $text_garancia = $result['garancia_ertek']. " " . $this->language->get('entry_garancia_honap') . " " . $this->language->get('text_garancia');
                $garancia_kep = $result['garancia_ertek']."_honap.png";
                break;
            case 2:
                $text_garancia = $result['garancia_ertek']. " " . $this->language->get('entry_garancia_ev') . " " . $this->language->get('text_garancia');
                $garancia_kep = $result['garancia_ertek']."_ev.png";
                break;
            default:
                $text_garancia = "";
                $garancia_kep = "";
                break;
        }
        $warehouses =  $this->warehouse($result['product_id']);
        $megjelenit_admin_termekadatok = $this->config->get('megjelenit_admin_termekadatok');
        $megjelenit_stock_date =  isset($megjelenit_admin_termekadatok['stock_date']) ? $megjelenit_admin_termekadatok['stock_date'] : '';
        $today = date("Y-m-d");
        if(isset($megjelenit_stock_date) && $megjelenit_stock_date == 1 && isset($result['stock_in_date']) && $result['stock_in_date'] > 0 && $result['stock_in_date'] > $today) {
            $sum = 0;
            foreach($warehouses as $warehouse) {
                $sum += $warehouse['quantity'];
            }
            if ($sum < 1) {
                $warehouses_out_of_stock = true;
                $result['stock_in_date'] = date($this->language->get('date_format_short'), strtotime( $result['stock_in_date']));

            } else {
                $warehouses_out_of_stock = false;
            }
        } else {
            $warehouses_out_of_stock = false;
        }


        if ($result['egyedi_szallitas'] == 1) {
            if ($result['egyedi_szallitasi_dij'] == 0) {
                $szallitasi_koltseg = "ingyenes";
            } else {
                $szallitasi_koltseg =   $this->currency->format($this->tax->calculate($result['egyedi_szallitasi_dij'], $result['tax_class_id'], $this->config->get('config_tax')));
            }
        } else {
            $szallitasi_koltseg = false;
        }

        $text_szallitas       =  $this->language->get('text_szallitas');

        $not_null = true;
        if ($price > 0 || $special > 0 || $result['price'] > 0 || $result['special'] > 0) {
            $not_null = true;
        } else {
            $not_null = false;
        }

        $arengedmeny_szazalek = 0;
        if ($special > 0 && $price > 0) {
            $arengedmeny_szazalek = round((1-$special/$price)*100);
        }

        $vissza =  array(
            'product_id'            => $result['product_id'],
            'szazalek'              => isset($result['szazalek']) ? $result['szazalek'] : 0,
            'utalvany'              => isset($result['utalvany']) ? $result['utalvany'] : 0,
            'thumb_set'             => $image,
            'thumb'                 => $image_lista,
            'thumb_product'         => $image_product,
            'thumb_racs'            => $image_racs,
            'thumb_list_width'      => isset($thumb_list_width) ? $thumb_list_width : 0,
            'thumb_list_height'     => isset($thumb_list_height) ? $thumb_list_height : 0,
            'thumb_grid_width'      => isset($thumb_grid_width) ? $thumb_grid_width : 0,
            'thumb_grid_height'     => isset($thumb_grid_height) ? $thumb_grid_height : 0,
            'model'                 => isset($result['model']) ? $result['model'] : "",
            'cikkszam'              => isset($result['cikkszam']) ? $result['cikkszam'] : "",
            'weight'                => isset($result['weight']) ? $result['weight'] : "",
            'kifuto'                => isset($result['kifuto']) ? $result['kifuto'] : 0,
            'name'    	            => $result['name'],
            'date_ervenyes_ig'      => isset($result['date_ervenyes_ig']) ? $result['date_ervenyes_ig'] : 0,
            'eredeti_ar'            => isset($result['eredeti_ar']) ? $this->currency->format($result['eredeti_ar']) : "",
            'price'   	            => $price,
            'special' 	            => $special,
            'arengedmeny_szazalek'  => $arengedmeny_szazalek,
            'price_netto'           => $this->currency->format($result['price']),
            'special_netto'         => $result['special'] > 0 ? $this->currency->format($result['special']) : false,
            'not_null'              => $not_null,
            'tax'                   => $tax,
            'szallitasi_koltseg'    => $szallitasi_koltseg,
            'text_szallitas'        => $text_szallitas,
            'text_garancia'         => $text_garancia,
            'garancia_kep'          => $garancia_kep,
            'rating'                => $rating,
            'reviews'               => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
            'href'    	            => $this->url->link('product/product', $utvonal),
            'checkout_href'    	    => $this->url->link('checkout/checkout'),
            'uj'                    => $uj,
            'pardarab'	            => $pardarab,
            'minimum'	            => (isset($result['minimum']) && $result['minimum'] > 0) ? $result['minimum'] : 1,
            'discounts'             => $discounts[$legolcsobb_kulcs],
            'csomagolasi_egyseg'    => isset($result['csomagolasi_egyseg']) ? $result['csomagolasi_egyseg'] : "",
            'megyseg'               => isset($result['megyseg']) ? $result['megyseg'] : "",
            'description'           => $descr,
            'mutat_arat'            => $mutat_arat,
            'short_descr_lista'     => $short_descr_lista,
            'short_descr_racs'      => $short_descr_racs,
            'garancia_description'  => $garancia_description,
            'imagedesabled'         => isset($result['imagedesabled']) ? $result['imagedesabled'] : 0,
            'csomagolasi_mennyiseg' => $result['csomagolasi_mennyiseg'] > 1 ? $result['csomagolasi_mennyiseg'] : 1,
            'show_special_countdown' =>  isset($result['show_special_countdown']) ? $result['show_special_countdown']: 0,
            'kiemelt_tpl'           => (!empty($kiemelt_template) ? $kiemelt_template : ''),
            'kiemelt_sorrend'       => (!empty($kiemelt_sorrend) ? $kiemelt_sorrend : ''),
            'kiemelt_al_sorrend'    => (!empty($kiemelt_al_sorrend) ? $kiemelt_al_sorrend : ''),
            'countdown'             => $this->priceCountdown($result['product_id']),
            'warehouse'             => $this->warehouse($result['product_id']),
            'warehouses_out_of_stock' => $warehouses_out_of_stock,
            'stock_in_date'         => (isset($result['stock_in_date'])) ? $result['stock_in_date'] : 0,
            'manufacturers'         => $this->manufacturer($result['manufacturer_id']),
            'ajandek'               => $this->ajandek($result['product_id']),
            'raktaron'              => $raktaron,
            'stock'                 => $stock,
            'text_popup_checkout'   => $this->language->get('text_popup_checkout'),
            'text_fejlec_szoveg'    => $this->language->get('text_fejlec_szoveg'),
            'ujdonsag'              => (isset($result['ujdonsag']) && $result['ujdonsag']) ? $result['ujdonsag'] : 0,
            'elorendeles'           => (isset($result['elorendeles']) && $result['elorendeles']) ? $result['elorendeles'] : 0,
            'megrendelem'           => !empty($result['megrendelem']) ? $result['megrendelem'] : 0,
            'ar_tiltasa'            => !empty($result['ar_tiltasa']) ? $result['ar_tiltasa'] : 0
        );


        return $vissza;
	}

    public function warehouse($product_id) {
        $this->load->model('warehouse/warehouse');

        $vissza = $this->model_warehouse_warehouse->getWarehousesProduct($product_id);

        return $vissza;
    }

    public function ajandek($product_id) {
        $this->load->model('catalog//product');

        $vissza = $this->model_catalog_product->getProductAjandek($product_id);

        return $vissza;
    }



    public function manufacturer($manufacturer_id) {
        $this->load->model('catalog/manufacturer');

        if ($manufacturer_id) {
            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

            if ($manufacturer_info) {
                return array(
                    'name'	            => $manufacturer_info['name'],
                    'manufacturer_id'	=> $manufacturer_info['manufacturer_id'],
                    'href'	            => $this->url->link('product/manufacturer/product', 'manufacturer_id=' . $manufacturer_id),
                    'image'             => $manufacturer_info['image']
                );
            }
        }


    }

    public function priceCountdown($product_id) {
        $this->document->addScript('catalog/view/javascript/jquery/jquery.countdown.js');

        $this->load->model('module/special_price_countdown');

        $show_special_price_countdown = 0;
        $setting_price_countdown = $this->config->get('special_price_countdown_module');
        $setting_price_countdown = $setting_price_countdown[0];

        $date_stop = $this->model_module_special_price_countdown->getSpecialPriceEndData($product_id);

        if ($date_stop){

            $target_split = preg_split('/\-/', $date_stop);

            $now = time();
            $target = mktime(0,0,0, $target_split[1], $target_split[2], $target_split[0]);
            $diffSecs = $target - $now;

            if ($diffSecs <= $setting_price_countdown['hours_bs'] * 60 * 60 && $diffSecs > 0){

                $show_special_price_countdown = 1;

                $countdown_format = 'dd:hh:mm:ss';
                $show_days = 1;

                if ($setting_price_countdown['show_days'] ==0 && $diffSecs < 24 * 60 * 60 ){
                    $countdown_format = 'hh:mm:ss';
                    $show_days = 0;
                }

                $date = array();
                $date['secs'] = $diffSecs % 60;
                $date['mins'] = floor($diffSecs/60)%60;
                $date['hours'] = floor($diffSecs/60/60)%24;
                $date['days'] = floor($diffSecs/60/60/24);//%7;
                $date['weeks']	= floor($diffSecs/60/60/24/7);

                foreach ($date as $i => $d) {
                    $d1 = $d%10;
                    $d2 = ($d-$d1) / 10;
                    $date[$i] = array(
                        (int)$d2,
                        (int)$d1,
                        (int)$d
                    );
                }

                $secs  = $diffSecs % 60;
                $mins  = floor($diffSecs/60)%60;
                $hours = floor($diffSecs/60/60)%24;
                $days  = floor($diffSecs/60/60/24);//%7;
                $weeks = floor($diffSecs/60/60/24/7);


                /*if ( $this->config->get('megjelenit_allitott') == 1) {
                    $termek_dobozok = $this->config->get('megjelenit_termek_doboz');

                } else {
                    $termek_dobozok = $this->config->get('megjelenit_termek_fektetett');
                }*/

                /*if (isset($termek_dobozok['special_price_countdown']['width'])) {
                    $setting_price_countdown['digit_width'] = $termek_dobozok['special_price_countdown']['width'];
                }
                if (isset($termek_dobozok['special_price_countdown']['height'])) {
                    $setting_price_countdown['digit_width'] = $termek_dobozok['special_price_countdown']['height'];
                }*/

                $digit_width            = $setting_price_countdown['digit_width'];
                $digit_height           = $setting_price_countdown['digit_height'];
                $countdown_image_width  = $setting_price_countdown['digit_width'];
                $countdown_image_height = $setting_price_countdown['digit_height'] *60;
                $search_height          = $setting_price_countdown['digit_height'];

                if ( ($setting_price_countdown['digit_width'] != 53 && $setting_price_countdown['digit_height'] != 77) || $search_height < 10 ){  // original image

                    $calcWidth      = 53 / (4619 / ($search_height * 60));
                    $roundCalcWidth = round($calcWidth);

                    while( $roundCalcWidth - $calcWidth < 0){
                        $search_height--;
                        $calcWidth      = 53 / (4619 / ($search_height * 60));
                        $roundCalcWidth = round($calcWidth);
                    }

                    $countdown_image_width  = $roundCalcWidth;
                    $countdown_image_height = $search_height *60;
                    $digit_height = $search_height;
                    $digit_width  = $roundCalcWidth;

                }


                $this->load->model('tool/image');
                $digit_image  = $this->model_tool_image->resize('data/special_price_countdown/digits.png', $countdown_image_width, $countdown_image_height );

                $new_font_size = round(($setting_price_countdown['digit_height'] * 43 ) / 77);

                return array(
                    'show_special_price_countdown'  => $show_special_price_countdown,
                    'countdown_format'              => $countdown_format,
                    'show_days'                     => $show_days,
                    'target_split'                  => $target_split,
                    'date'                          => $date,
                    'secs'                          => $secs,
                    'mins'                          => $mins,
                    'hours'                         => $hours,
                    'days'                          => $days,
                    'weeks'                         => $weeks,
                    'digit_width'                   => $digit_width,
                    'digit_height'                  => $digit_height,
                    'digit_image'                   => $digit_image,
                    'new_font_size'                 => $new_font_size


                );

            }
        }
    }

    public function  limits($link,$path,$url) {

        $limits = array();

        //$path = $path ? 'path=' . $path : "";
        if ($this->config->get('config_catalog_limit') < 25) {
            $limits[] = array(
                'text'  => $this->config->get('config_catalog_limit'),
                'value' => $this->config->get('config_catalog_limit'),
                'href'  => $this->url->link($link, $path . $url . '&lista=1&limit=' . $this->config->get('config_catalog_limit'))
            );
        }

        $limits[] = array(
            'text'  => 25,
            'value' => 25,
            'href'  => $this->url->link($link, $path . $url . '&lista=1&limit=25')
        );

        if ($this->config->get('config_catalog_limit') > 25 && $this->config->get('config_catalog_limit') < 50) {
            $this->data['limits'][] = array(
                'text'  => $this->config->get('config_catalog_limit'),
                'value' => $this->config->get('config_catalog_limit'),
                'href'  => $this->url->link($link, $path . $url . '&lista=1&limit=' . $this->config->get('config_catalog_limit'))
            );
        }

        $limits[] = array(
            'text'  => 50,
            'value' => 50,
            'href'  => $this->url->link($link, $path . $url . '&lista=1&limit=50')
        );

        if ($this->config->get('config_catalog_limit') > 50 && $this->config->get('config_catalog_limit') < 75) {
            $limits[] = array(
                'text'  => $this->config->get('config_catalog_limit'),
                'value' => $this->config->get('config_catalog_limit'),
                'href'  => $this->url->link($link, $path . $url . '&lista=1&limit=' . $this->config->get('config_catalog_limit'))
            );
        }

        $limits[] = array(
            'text'  => 75,
            'value' => 75,
            'href'  => $this->url->link($link, $path . $url . '&lista=1&limit=75')
        );

        if ($this->config->get('config_catalog_limit') > 75 && $this->config->get('config_catalog_limit') < 100) {
            $limits[] = array(
                'text'  => $this->config->get('config_catalog_limit'),
                'value' => $this->config->get('config_catalog_limit'),
                'href'  => $this->url->link($link, $path . $url . '&lista=1&limit=' . $this->config->get('config_catalog_limit'))
            );
        }

        $limits[] = array(
            'text'  => 100,
            'value' => 100,
            'href'  => $this->url->link($link, $path . $url . '&lista=1&limit=100')
        );


        if ($this->config->get('config_catalog_limit') > 100) {
            $limits[] = array(
                'text'  => $this->config->get('config_catalog_limit'),
                'value' => $this->config->get('config_catalog_limit'),
                'href'  => $this->url->link($link, $path . $url . '&lista=1&limit=' . $this->config->get('config_catalog_limit'))
            );
        }
        return $limits;
    }

    public function fogalomBeepites($szoveg) {


        $fogalmak = $this->registry->get('fogalmak');
        if ($fogalmak) {
            foreach($fogalmak as $value) {
                $position = strpos(strtolower($szoveg),strtolower($value["name"]));
                if ($position !== false) {

                    $csere = substr($szoveg,$position,strlen($value["name"]));
                    $modositott = '<a class="fogalom_szo" onclick="fogalomMagyarazat('.$value['fogalom_magyarazat_id'].',this )" >'.$csere.'</a>';

                    $pattern = preg_quote($value['name']);
                    $szoveg  = preg_replace("/($pattern)/i",$modositott,$szoveg);

                }
            }
        }

        return $szoveg;
    }

}
?>