<?php if ($display_type){ ?>
<div class="banner">
	<a id="show-special-offer-reminder"><img src="<?php echo $image; ?>" class="banner" alt="<?php echo $heading_title; ?>"></a>
</div>	
<?php } else { ?>
<div class="box special_reminder">
    <div class="box-content">
        <a id="show-special-offer-reminder" style="background: url('<?php echo $image; ?>')">
            <div class="box-heading">
            <?php echo $heading_title; ?>
            </div>
        </a>

    </div>

</div>
<?php } ?>

<div id="special-offer-reminder" class="modal-box wrapper"><div class="inside grey">
	<span class="modal-close left" id="close-special-offer-reminder"></span>
	<img src="catalog/view/theme/<?php echo $theme_name; ?>/image/modal_box/price_barcode.png" class="right" />
	<h3 class="small"><?php echo $modal_title; ?></h3>
	<p><?php echo $text_info; ?></p>
	<div class="hr">&nbsp;</div>
	<form>
		<div id="info-message"></div>
		<div class="attention"><?php echo $text_expire_date; ?></div>
		<fieldset class="<?php echo $fieldset_class; ?>">
			<label><span class="required">*</span><?php echo $entry_name; ?></label><br/>
			<input type="text" class="text" name="sor_fullname" value="<?php echo $sor_fullname; ?>" />
		</fieldset>
		<fieldset class="<?php echo $fieldset_class; ?>">
			<label><span class="required">*</span><?php echo $entry_email; ?></label><br/>
			<input type="text" class="text" name="sor_email" value="<?php echo $sor_email; ?>" />
		</fieldset>
		<fieldset>
			<label><span class="required">*</span><?php echo $entry_days_before; ?></label><br/>
			<input type="text" class="text" name="sor_days_before" value="" />
		</fieldset>
		<input type="hidden" name="sor_product_id" value="<?php echo $sor_product_id; ?>" />
		<input type="hidden" name="sor_expire_date" value="<?php echo $sor_expire_date; ?>" />
		
		<fieldset>
			<span class="button white save" id="set-special-offer-reminder"><span><?php echo $text_set_alert; ?></span></span>
		</fieldset>
	</form>
</div></div>

<script type="text/javascript">
$('#show-special-offer-reminder').bind('click', function(){
	$('#special-offer-reminder').slideDown();
});

$('#close-special-offer-reminder').bind('click', function(){
	$('#special-offer-reminder').slideUp();
});

$('#set-special-offer-reminder').bind('click', function(){
	$.ajax({
		type: 'POST',
		url: 'index.php?route=module/special_offer_reminder/save_reminder',
		data: $('#special-offer-reminder input[type=\'text\'], #special-offer-reminder input[type=\'hidden\'] '),
		dataType: 'json',
		success: function(data){
			$('#info-message').html('');
			
			if (data.error) {
				$('#info-message').html('<div class="warning">' + data.error + '</div>');
			}
			
			if (data.success) {
				$('#info-message').html('<div class="success">' + data.success + '</div>');
				
				<?php if (!$customer_logged) { ?>	
					$('#special-offer-reminder input[name=\'sor_fullname\']').val('');
					$('#special-offer-reminder input[name=\'sor_email\']').val('');
				<?php } ?>
				$('#special-offer-reminder input[name=\'sor_days_before\']').val('');
				setTimeout(function() {
					$('#info-message').html('');
					$("#special-offer-reminder").hide('blind', {}, 500)
				}, 3000);
			}
		}
		
	});
});
</script>