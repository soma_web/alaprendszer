<?php

// Heading
$_['heading_title']         = 'Rendeléstörténet';

// Text
$_['text_account']          = 'Fiók';
$_['text_order']            = 'Rendelés információ';
$_['text_order_detail']     = 'Rendelés részletei';
$_['text_invoice_no']       = 'Számlaszám:';
$_['text_order_id']         = 'Rendelésszám:';
$_['text_status']           = 'Státusz:';
$_['text_date_added']       = 'Rendelés dátuma:';
$_['text_customer']         = 'Vevő:';
$_['text_shipping_address'] = 'Szállítási cím';
$_['text_shipping_method']  = 'Szállítási mód:';
$_['text_payment_address']  = 'Számlázási cím';
$_['text_payment_method']   = 'Fizetési mód:';
$_['text_products']         = 'Termékek:';
$_['text_total']            = 'Összesen:';
$_['text_comment']          = 'Megjegyzések';
$_['text_history']          = 'Rendeléstörténet';
$_['text_empty']            = 'Önnek még nem voltak megrendelési!';
$_['text_error']            = 'A keresett rendelés nem található!';
$_['text_action']           = 'Válasszon a lehetőségek közül:';
$_['text_selected']         = 'A kiválasztott.';
$_['text_reorder']          = 'Kosárba';
$_['text_return']           = 'Termék visszaszállítása';
$_['text_success']          = 'Az előző rendelést sikeresen betette a bevásárló kosárba!';

// Column
$_['column_name']           = 'Terméknév';
$_['column_model']          = 'Modell';
$_['column_quantity']       = 'Menny.';
$_['column_price']          = 'Ár';
$_['column_total']          = 'Összesen';
$_['column_date_added']     = 'Rendelés dátuma';
$_['column_status']         = 'Állapot';
$_['column_comment']        = 'Megjegyzés';
$_['button_return']         = 'Visszaküldés';
$_['button_continue']       = 'Tovább';


$_['column_price_netto_egysegar']    = '<span>Egység ár </span></br><span style="font-size: 10px; ">(Nettó)</span>';
$_['column_price_brutto_egysegar']   = '<span>Egység ár</span></br><span style="font-size: 10px; "> (Bruttó)</span>';
$_['column_price_netto']    = '<span>Összesen ár</span></br><span style="font-size: 10px; "> (Nettó)</span>';
$_['column_price_brutto']   = '<span>Összesen ár</span></br><span style="font-size: 10px; "> (Bruttó)</span>';


$_['column_netto']                  = '<span>Egység ár </span></br><span style="font-size: 10px; ">(Nettó)</span>';
$_['column_netto_total_price']      = '<span>Összesen ár</span></br><span style="font-size: 10px; "> (Nettó)</span>';
$_['column_price_netto_ar']         = '<span>Egység ár</span></br><span style="font-size: 10px; "> (Bruttó)</span>';
$_['column_total_netto_ar']         = '<span>Összesen ár</span></br><span style="font-size: 10px; "> (Bruttó)</span>';
$_['termek_ar_osszes']              = '<span>Összesen</span>';
$_['szallitas']                     = '<span><b>Szállítás</b></span>';
$_['szallitas_netto_ar']            = '<span>Ár (Nettó)</span>';
$_['szallitas_brutto_ar']           = '<span>Ár (Bruttó)</span>';

$_['netto_osszes']                  = '<span>Nettó összesen</span>';
$_['afa_osszes']                    = '<span>Áfa összesen</span>';
$_['fizetendo_osszes']              = '<span>Fizetendő összesen</span>';


// Error
$_['error_warning']         = 'Figyelem: Ki kell választania terméket és műveletet a kérés teljesítéséhez';
?>