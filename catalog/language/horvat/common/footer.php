<?php

// Text
$_['text_information']  = 'Információ';
$_['text_service']      = 'Ügyfélszolgálat';
$_['text_message']      = 'Üzenjen nekünk!';
$_['text_extra']        = 'Extrák';
$_['text_contact']      = 'Kontakt';
$_['text_submit']       = 'Elküld';
$_['text_contact']      = 'Elérhetőségeink';
$_['text_address']      = 'Iroda és raktár:';
$_['text_email']        = 'E-mail cím';
$_['text_content']      = 'Ide irhatja az üzenetét!';
$_['text_return']       = 'Garancia';
$_['text_sitemap']      = 'Oldaltérkép';
$_['text_manufacturer'] = 'Gyártók';
$_['text_voucher']      = 'Ajándékutalványok';
$_['text_affiliate']    = 'Partnerprogram';
$_['text_special']      = 'Akciók';
$_['text_kategoriak']   = 'Kategóriák';
$_['text_account']      = 'Fiókom';
$_['text_order']        = 'Rendeléstörténet';
$_['text_wishlist']     = 'Kívánságlista';
$_['text_newsletter']   = 'Hírlevél';
//$_['text_powered'] 	    = '&copy; 2015-%s %s Minden jog fenntartva. | Webáruházat készítette és optimalizálta:  <a target="_blank" href="http://www.ideasol.hu">IDEASOL Magyarország Kft., </a><a target="_blank" href="http://premium-webaruhazkeszites.hu/index.php">a pénztermelő webáruházak specialistája</a>';
$_['text_powered'] 	    = '&copy; 2015-%s %s Minden jog fenntartva. |  VRCS Software - SOMAWEB Kft.';
$_['text_pricelist']    = 'Árlista';
$_['text_kedvezmeny']   = 'Kedvezményem';
$_['text_name']    	    = 'Név';
$_['text_cookie_figyelmeztetes']    	    = 'Weboldalunk használatával jóváhagyod a cookie-k használatát a Cookie-kkal kapcsolatos irányelv értelmében.';
?>