<?php
// Heading
$_['heading_title']       = 'Szűrő';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikerült: Ön módosította szűrő modult!';
$_['text_content_top']    = 'Tartalom felül';
$_['text_content_bottom'] = 'Tartalom alul';
$_['text_column_left']    = 'Bal oldal';
$_['text_column_right']   = 'Jobb oldal';

// Entry
$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_status']        = 'Státusz:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_filter_select'] = 'Szűrő kiválasztás:';

// Error
$_['error_permission']    = 'Figyelmeztetés: Önnek nincs jogosultsága módosítani a szűrő modult!';
?>