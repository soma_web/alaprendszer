<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
            <tr>
                <td><?php echo $entry_menusor_neve; ?></td>
                <td>
                   <input type="text" name="kiemeltmenusor9_module_menusor[fejlec]" value="<?php echo $kiemeltmenusor9_module_menusor['fejlec']?>" size="80" />
                </td>
            </tr>


        <tr>


            <td ><?php echo $entry_sort_order; ?></td>
            <td colspan=1>
                <input type="text" name="kiemeltmenusor9_module_menusor[sorrend]" value="<?php echo $kiemeltmenusor9_module_menusor['sorrend']?>" size="3" />
            </td>


        </tr>
        <tr><td><br/></td>
        </tr>

        <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="kiemeltmenusor9_module_menusor[status]">
                    <option value="1"<?php if ($kiemeltmenusor9_module_menusor['status']) { ?> selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                    <option value="0"<?php if (!$kiemeltmenusor9_module_menusor['status']) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                </select>
            </td>
        </tr>



        <tr>
          <td><?php echo $entry_product; ?></td>
          <td><input type="text" name="product" value="" /></td>
          <td><input type="hidden" name="kiemeltmenusor9_module_store" value="0" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div id="kiemeltmenusor9-product" class="scrollbox" style="height: 300px">
              <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
              <div id="kiemeltmenusor9-product<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product['name']; ?> <img src="view/image/delete.png" />
                <input type="hidden" value="<?php echo $product['product_id']; ?>" />
              </div>
              <?php } ?>
            </div>
            <input type="hidden" name="kiemeltmenusor9_product" value="<?php echo $kiemeltmenusor9_product; ?>" /></td>
        </tr>
      </table>


    </form>
  </div>
</div>
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
		
	}, 
	select: function(event, ui) {
		$('#kiemeltmenusor9-product' + ui.item.value).remove();
		
		$('#kiemeltmenusor9-product').append('<div id="kiemeltmenusor9-product' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" /><input type="hidden" value="' + ui.item.value + '" /></div>');

		$('#kiemeltmenusor9-product div:odd').attr('class', 'odd');
		$('#kiemeltmenusor9-product div:even').attr('class', 'even');
		
		data = $.map($('#kiemeltmenusor9-product input'), function(element){
			return $(element).attr('value');
		});
						
		$('input[name=\'kiemeltmenusor9_product\']').attr('value', data.join());
					
		return false;
	}
});

$('#kiemeltmenusor9-product div img').on('click', function() {
	$(this).parent().remove();
	
	$('#kiemeltmenusor9-product div:odd').attr('class', 'odd');
	$('#kiemeltmenusor9-product div:even').attr('class', 'even');

	data = $.map($('#kiemeltmenusor9-product input'), function(element){
		return $(element).attr('value');
	});
					
	$('input[name=\'kiemeltmenusor9_product\']').attr('value', data.join());	
});
//--></script> 


<?php echo $footer; ?>