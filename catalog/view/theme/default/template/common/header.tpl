<?php if (isset($_SERVER['HTTP_USER_AGENT']) && !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6')) echo '<?xml version="1.0" encoding="UTF-8"?>'. "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
<?php if (!empty($google_webmaster)) { ?>
    <?php echo html_entity_decode($google_webmaster); ?>
<?php } ?>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>

<?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet.min.css')) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/stylesheet.min.css" />
<?php } else { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/stylesheet.css" />
<?php } ?>

<?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_fogalom.css')) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/stylesheet_fogalom.css" />
<?php } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_fogalom.css')) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE; ?>default/stylesheet/stylesheet_fogalom.css" />
<?php } ?>

<?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_checkout.css')) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/stylesheet_checkout.css" />
<?php } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_checkout.css')) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE; ?>default/stylesheet/stylesheet_checkout.css" />
<?php } ?>


    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/autocomplate.css')) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/autocomplate.css" />
    <?php } else { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE; ?>default/stylesheet/autocomplate.css" />
    <?php } ?>

    <?php
    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_elorendeles.php')) {
        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/stylesheet/stylesheet_elorendeles.php';
        include($arkiir);

    } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_elorendeles.php')) {
        $arkiir = DIR_TEMPLATE.'default/stylesheet/stylesheet_elorendeles.php';
        include($arkiir);
    } ?>

<!-- Css (stylesheet_home.css) beolvasása, ha nem a főoldalon vagyunk.  -->
<? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_home.css')) {
    $css_file = DIR_TEMPLATE. $this->config->get('config_template') . '/stylesheet/stylesheet_home.css';
} elseif(file_exists(DIR_TEMPLATE.'default/stylesheet/stylesheet_home.css')) {
    $css_file = DIR_TEMPLATE.'default/stylesheet/stylesheet_home.css';
} ?>

<!-- Css (stylesheet_header.css) beolvasása, mert külön tároljuk a headerhez tartozó css részt.  -->
    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_header.css')) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/stylesheet_header.css" />
    <?php } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_header.css')) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE; ?>default/stylesheet/stylesheet_header.css" />
    <?php } ?>

    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_category.css')) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/stylesheet_category.css" />
    <?php } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_category.css')) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE; ?>default/stylesheet/stylesheet_category.css" />
    <?php } ?>

    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_box_view.css')) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/stylesheet_box_view.css" />
    <?php } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_box_view.css')) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE; ?>default/stylesheet/stylesheet_box_view.css" />
    <?php } ?>

    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_footer.css')) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE.  $this->config->get('config_template')?>/stylesheet/stylesheet_footer.css" />
    <?php } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_footer.css')) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo DIR_TEMPLATE; ?>default/stylesheet/stylesheet_footer.css" />
    <?php } ?>




<?php if (isset($css_file) && $css_file) { ?>
    <?php if ( (isset($this->request->request['route']) && $this->request->request['route'] != "common/home")) { ?>
        <?php if(file_exists($css_file)){ ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $css_file; ?>" />
        <?php } ?>
    <?php } ?>
<?php } ?>

<?php foreach ($styles as $style) { ?>
    <?php if(file_exists($style['href'])){ ?>
        <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
<?php } ?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>


<script type="text/javascript" src="catalog/view/javascript/jquery/ui-1.12/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui-1.12/jquery-ui-1.8.16.custom.css" />




<script type="text/javascript" src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/bxslider/jquery.bxslider.css" />
<?php if ($this->config->get('postapont_status')) { ?>
    <script type="text/javascript" src="http://www.postapont.hu/static/javascripts/postapont-api.js"></script>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/postapont.css" />

<?php } ?>
<?php if ($this->config->get('item_product_gls_status')) { ?>
    <link rel="stylesheet" type="text/css" href="http://online.gls-hungary.com/psmap/default.css">
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>
    <script type="text/javascript" src="http://online.gls-hungary.com/psmap/psmap.js"></script>
<?php } ?>


<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>


<!-- Fancybox for Windowsized Images -->
<link rel="stylesheet" href="catalog/view/javascript/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="catalog/view/javascript/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->


    <?php if ($this->config->get('megjelenit_jobb_egerbolt') == 1) { ?>
        <script type="text/javascript">
            function clickIE() {if (document.all) {return false;}}
            function clickNS(e) {if
                (document.layers||(document.getElementById&&!document.all)) {
                if (e.which==2||e.which==3) {;return false;}}}

            if (document.layers)
            {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
            else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}

            document.oncontextmenu=new Function("return false")
        </script>
    <?php } ?>


    <script type="text/javascript">

        function price_format(n) {
            <?php
            $currency1 = array();
            $currency1['value'] = ($this->currency->getValue() ? $this->currency->getValue() : 1);
            $currency1['symbol_left'] = $this->currency->getSymbolLeft();
            $currency1['symbol_right'] = $this->currency->getSymbolRight();
            $currency1['decimals'] = $this->currency->getDecimalPlace();
            $currency1['decimal_point'] = $this->language->get('decimal_point');
            $currency1['thousand_point'] = $this->language->get('thousand_point');

            if (isset($product_info['tax_class_id'])) {
                $tax_rates1 = $this->tax->getRates(0, $product_info['tax_class_id']);
            }

            ?>
            c = <?php echo (empty($currency1['decimals']) ? "0" : $currency1['decimals'] ); ?>;
            d = '<?php echo $currency1['decimal_point']; ?>'; // decimal separator
            t = '<?php echo $currency1['thousand_point']; ?>'; // thousands separator
            s_left = '<?php echo $currency1['symbol_left']; ?>';
            s_right = ' <?php echo $currency1['symbol_right']; ?>';



            <?php // Process Tax Rates

              if (isset($tax_rates1)) {
                 foreach ($tax_rates1 as $tax_rate1) {
                   if ($tax_rate1['type'] == 'F') {
                     echo 'n += '.$tax_rate1['rate'].';';
                   } elseif ($tax_rate1['type'] == 'P') {
                     echo 'n += (n * '.$tax_rate1['rate'].') / 100.0;';
                   }
                 }
              }
            ?>

            n = n * <?php echo $currency1['value']; ?>;

            sign = (n < 0) ? '-' : '';

            //extracting the absolute value of the integer part of the number and converting to string
            i = parseInt(n = Math.abs(n).toFixed(c)) + '';

            j = ((j = i.length) > 3) ? j % 3 : 0;
            return sign + s_left + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + s_right;
        }

        function disableCtrlKeyCombination(e) {
//list all CTRL + key combinations you want to disable
            var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'j' , 'w' , 'u');
            var key;
            var isCtrl;

            if(window.event) {
                key = window.event.keyCode;     //IE
                if(window.event.ctrlKey)
                    isCtrl = true;
                else
                    isCtrl = false;
            } else {

                key = e.which;     //firefox
                if(e.ctrlKey)
                    isCtrl = true;
                else
                    isCtrl = false;
            }

//if ctrl is pressed check if other key is in forbidenKeys array
            if(isCtrl) {
                for(i=0; forbiddenKeys.length >i; i++) {
//case-insensitive comparation
                    if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase()) {
                        alert('A CTRL + '+String.fromCharCode(key) +' kombináció tiltott!');
                        return false;
                    }
                }
            }
            return true;
        }
    </script>
    <script>
        function onKeyDown() {
// current pressed key
            var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
            if (event.ctrlKey && (pressedKey == "c" || pressedKey == "v")) {
// disable key press porcessing
                event.returnValue = false;
            }
        } // onKeyDown
    </script>

    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/hu_HU/sdk.js#xfbml=1&appId=828794903803316&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>


    <script> //function for to disable ctrl event of the page.
        /*function document.onkeydown()
        {
            if ( event.keyCode==17) //17 is ascii code for ctrl
            {
                event.keyCode = 0;
                event.cancelBubble = true;
                return false;
            }
        }*/
    </script>


    <?php if (!empty($hatterkep_body)) { ?>
        <style>
            body {
                background-image: url('<?php echo $hatterkep_body?>');
                background-position: top;
                background-repeat: no-repeat;

                <?php if (!empty($hatterkep_body_link)) { ?>
                    cursor: pointer;
                <?php } ?>

            }

            <?php if (!empty($hatterkep_body_link)) { ?>

                body * {cursor: auto;}

                #kozepe_hatternek, #header {cursor: pointer;}
                #kozepe_hatternek *, #header * {cursor: auto;}
            <?php } ?>

        </style>
    <?php } ?>


<?php echo $google_analytics; ?>
</head>


<?php if ($this->config->get('megjelenit_masolhato') == 1) { ?>
    <body onkeypress="return disableCtrlKeyCombination(event);" onkeydown="return disableCtrlKeyCombination(event);" ondragstart="return false" onselectstart="return false">
<?php } else {?>
    <body>
<?php } ?>
<!-- Fancybox script -->
<script>
    $(document).ready(function() {
        $(".fancybox").fancybox({
            autoScale: false,
            type: 'iframe',
            padding: 0,
            closeClick: false
        });


        $('.fancy').fancybox({
            openEffect	: 'none',
            closeEffect	: 'none',
            prevEffect		: 'elastic',
            nextEffect		: 'elastic',
            padding		    : [0, 0, 10, 0],


            helpers : {
                title : {
                    type : 'inside'
                }
            }
        });

        $(".various").fancybox({
            openEffect	: 'elastic',
            closeEffect	: 'elastic',
            closeBtn    : true,
            padding     : [10, 10, 10, 10]
        });

        $(".infofancy").fancybox({
            openEffect	: 'elastic',
            closeEffect	: 'elastic',
            minWidth    : '33%',
            minHeight   : '33%',
            maxHeight   : '60%',
            maxWidth    : '80%',
            padding     : [10, 10, 10, 10]
        });
    });

    <?php if (!empty($hatterkep_body_link)) { ?>
        $("body").bind("click",function(event){
            var mehet = false;
            if ( event.target.tagName == "BODY" ) {
                mehet = true;
            }

            if ( event.target.id == "kozepe_hatternek") {
                mehet = true;
            }
            if ( event.target.id == "header") {
                mehet = true;
            }
            if ( event.target.id == "footer") {
            }

            if (mehet) {
                location = '<?php echo str_replace('&amp;','&',$hatterkep_body_link);?>';
            }
        });

    <?php } ?>

</script>
<!-- Fancybox script end -->

<div id="container">
    <?php $megjelenit_product = $this->config->get('megjelenit_product'); ?>
    <input type="hidden" name="keres_autocomplate"  value="<?php echo $this->config->get('megjelenit_keres_autocomplete')?>">
    <input type="hidden" name="megjelenit_cikkszam" value="<?php echo $megjelenit_product['product_cikkszam_termeknevben']?>">
    <input type="hidden" name="megjelenit_model" value="<?php echo $megjelenit_product['product_model_termeknevben']?>">
    <input type="hidden" name="megjelenit_gyarto"   value="<?php echo $megjelenit_product['product_gyarto_termeknevben']?>">

    <div id="header_hatternek" style="margin-bottom: 0; margin-top: 0;">
<div id="header">

    <?php if ($megjelenit_header['modularis'] == 0) { ?>

    <?php if ($logo) { ?>
  <div id="logo">
          <a href="<?php echo $home; ?>">
              <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" />

          </a>
      <?php if ($text_honlapom){ ?>
        <div id="honlapom"><?php echo $text_honlapom; ?></div>
      <?php } ?>
  </div>


  <div id="motto"><?php echo $text_motto; ?></div>
  <?php } ?>

  <div id="felsosor">

      <!--<div><a href="<?php echo $facebook; ?>" target="_blank"><img src="image/data/fb_login.png" title="<?php echo $name; ?>"
                                                   alt="<?php echo $name; ?>" /></a>
      </div>-->



      <div class=pirosatmenet>
              <?php if (!$logged) { ?>
                  <?php echo $text_welcome; ?>
              <?php } else { ?>
                <?php echo $text_kijelentkezes; ?>
              <?php } ?>

      </div>



      <div>
          <?php if (!$logged) { ?>
              <?php echo $text_regisztral; ?>
          <?php } else { ?>
             <?php echo $text_logged; ?>
              <?php echo $text_ajanlat_keres; ?>

          <?php } ?>

      </div>

      <div style="border: none; padding: 0px; top: -5px;">
          <?php echo $currency; ?>
      </div>'

      <div style="border: none; padding: 0px; top: -5px;">
              <?php echo $language; ?>
      </div>



  </div>


  <div style="clear: both"></div>


  <div id="slidecontent" class="slidecontent" >
      <?php echo $cart; ?>
  </div>
  <div id="search">

    <?php if ($filter_name) { ?>
    <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
    <?php } else { ?>
    <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '#000000';" />
    <?php } ?>
      <div class="button-search"></div>

  </div>



<div id="menu" style="">
    <ul>
        <li><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
        <li id="termekek"><a href="<?php echo $product; ?>"><?php echo $text_termekek; ?></a>
            <?php if ($categories) { ?>
            <div class="termek_kategoria">
            <ul>
                <?php foreach ($categories as $category) { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                        <?php if ($category['children']) { ?>
                            <div>
                                <?php for ($i = 0; $i < count($category['children']);) { ?>
                                    <ul>
                                        <?php $j = $i + ceil(count($category['children']) / ($category['column'] > 0 ? $category['column'] : 1)); ?>
                                        <?php for (; $i < $j; $i++) { ?>
                                            <?php if (isset($category['children'][$i])) { ?>
                                                <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
            </div>
            <?php } ?>

        </li>
        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
        <li><a href="<?php echo $pricelist; ?>"><?php echo $text_arlista; ?></a></li>
        <li><a href="<?php echo $kapcsolat; ?>"><?php echo $text_kapcsolat; ?></a></li>
    </ul>
</div>
<?php } else {?>
    <?php
    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header_modularis.tpl')) {
        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/common/header_modularis.tpl';
        include($arkiir);

    } elseif (file_exists(DIR_TEMPLATE . 'default/template/common/header_modularis.tpl')) {
        $arkiir = DIR_TEMPLATE.'default/template/common/header_modularis.tpl';
        include($arkiir);

    } ?>
<?php } ?>

</div>
</div>



    <div id="kozepe_hatternek"  style="">
        <div class="kozepe" id="alap_kozepe" style="">
            <div id="notification"></div>