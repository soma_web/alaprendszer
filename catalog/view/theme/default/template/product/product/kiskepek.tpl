<?php if($images) { ?>

    <?php if((isset($beallitas_termek_aloldal['korhinta']) && $beallitas_termek_aloldal['korhinta']) && ($beallitas_termek_aloldal['korhinta_darab'] <= count($images))) { ?>
        <script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
        <link rel="stylesheet" type="text/css" href="catalog/view/javascript/bxslider/jquery.bxslider.css" />
        <script type="text/javascript">
            $(document).ready(function(){

                $('.bxslider_kiskep').bxSlider({
                    minSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 3?>,
                    maxSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 3?>,
                    slideWidth: <?php echo $this->config->get('config_image_additional_width'); ?>,
                    slideMargin: 5,
                    autoHidePager: true, /*  Eltűnteti a nyilakat, ha minSlides értékénél kevesebb kép jelenik meg. */
                    auto: <?php echo isset($beallitas_termek_aloldal['korhinta_auto']) ? "true" : "false" ?>,
                    pause: <?php echo isset($beallitas_termek_aloldal['korhinta_speed']) ? $beallitas_termek_aloldal['korhinta_speed'] : 1500?>,
                    autoHover: true,
                    infiniteLoop: <?php echo isset($beallitas_termek_aloldal['korhinta_infi']) ? "true" : "false" ?>,
                    responsive: true,
                    hideControlOnEnd: true,
                    adaptiveHeight: false
                });
            });
        </script>
    <?php } ?>

    <div class="image-additional" <?php if((isset($beallitas_termek_aloldal['korhinta']) && $beallitas_termek_aloldal['korhinta']) && ($beallitas_termek_aloldal['korhinta_darab'] <= count($images))) { ?> style="padding: 0; margin: 0; float:none; max-width: <?php echo $thumb_width; ?>px;" <?php } ?> >
        <div class="bxslider_kiskep">
            <?php foreach ($images as $image) { ?>
                    <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="fancy" ref="fancy-pics" data-fancybox-group="gallery-1" ><img  src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
            <?php } ?>
        </div>
    </div>
<?php } ?>