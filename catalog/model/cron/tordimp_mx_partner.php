<?php
class ModelCronTordimpMxPartner extends Model {

	public function getChangeMxPartnerEngedmeny() {
        $vissza = array();

        $sql = "SELECT * FROM trodimp_partner WHERE hivo_tabla LIKE 'mx_partner_engedmeny'";
        $query = $this->db->query($sql);
        if ($query->rows) {
            foreach($query->rows as $value) {
                $vissza[] = array(
                    'RowId' => $value['hivo_RowId'],
                    'PartnerKod' => $value['hivo_PartnerKod'],
                    'trodimp_partner_id' => $value['trodimp_partner_id']
                );
            }
        }

        return $vissza;

	}

    public function getWriteEngedmeny($RowID,$trodimp_partner_id) {

        $vissza = 0;

        if (count($this->db_mindennap)) {
            $sql = "SELECT Engedmeny, PartnerKod FROM mx_partner_engedmeny WHERE RowID='".$RowID."'";
            $query = $this->db_mindennap->query($sql);
            if ($query->row && $query->row['PartnerKod'] > 0) {
                $sql = "UPDATE ".DB_PREFIX."customer SET szazalek='".$query->row['Engedmeny']."' WHERE partner_kod='".$query->row['PartnerKod']."'";
                $this->db->query($sql);
                $vissza = 1;
            }

            $sql = "DELETE FROM ".DB_PREFIX."trodimp_partner WHERE trodimp_partner_id='".$trodimp_partner_id."'";
            $siker = $this->db->query($sql);
        }

        return $vissza;
    }

    public function getChangeMxPartnerTorzs() {
        $vissza = array();

        $sql = "SELECT * FROM trodimp_partner WHERE hivo_tabla LIKE 'mx_partner_torzs'";
        $query = $this->db->query($sql);
        if ($query->rows) {
            foreach($query->rows as $value) {
                $vissza[] = array(
                    'PartnerKod' => $value['hivo_PartnerKod'],
                    'trodimp_partner_id' => $value['trodimp_partner_id']
                );
            }
        }

        return $vissza;
    }



    public function getWriteFizetes($PartnerKod,$trodimp_partner_id) {

        $vissza = 0;

        if (count($this->db_mindennap)) {
            $sql = "SELECT FizetesiMod, PartnerKod FROM mx_partner_torzs WHERE PartnerKod='".$PartnerKod."'";
            $query = $this->db_mindennap->query($sql);

            if ($query->row && $query->row['PartnerKod'] > 0) {

                $sql = "SELECT customer_id FROM ".DB_PREFIX."customer WHERE partner_kod = '".$PartnerKod."'";
                $query_customer = $this->db->query($sql);

                if ($query_customer->row) {
                    $customer_id = $query_customer->row['customer_id'];

                    if ($query->row['FizetesiMod'] == 'A') {
                        $sql = "SELECT * FROM ".DB_PREFIX."customer_extension WHERE customer_id='".$customer_id."'";
                        $query_customer_extension = $this->db->query($sql);

                        if ($query_customer_extension->num_rows == 0) {
                            $sql = "INSERT INTO ".DB_PREFIX."customer_extension SET
                                customer_id ='".$customer_id."',
                                type        ='payment',
                                code        ='bank_transfer',
                                value       ='1'";
                            $siker = $this->db->query($sql);
                            $vissza = 1;

                        }
                    } else {
                        $sql = "DELETE FROM ".DB_PREFIX."customer_extension WHERE customer_id ='".$customer_id."'";
                        $siker = $this->db->query($sql);
                    }
                }
            }

            $sql = "DELETE FROM ".DB_PREFIX."trodimp_partner WHERE trodimp_partner_id='".$trodimp_partner_id."'";
            $siker = $this->db->query($sql);
        }

        return $vissza;
    }

    public function getChangeWebPartnerTorzs() {
        $vissza = array();

        $sql = "SELECT * FROM trodimp_partner WHERE hivo_tabla LIKE 'web_partner_torzs'";
        $query = $this->db->query($sql);
        if ($query->rows) {
            foreach($query->rows as $value) {
                $vissza[] = array(
                    'PartnerKod' => $value['hivo_PartnerKod'],
                    'trodimp_partner_id' => $value['trodimp_partner_id']
                );
            }
        }

        return $vissza;
    }



    public function getWriteFizetesWeb($PartnerKod,$trodimp_partner_id) {

        $vissza = 0;

        if (count($this->db_mindennap)) {
            $sql = "SELECT fizetesi_mod, PartnerKod FROM web_partner_torzs WHERE PartnerKod='".$PartnerKod."'";
            $query = $this->db_mindennap->query($sql);

            if ($query->row && $query->row['PartnerKod'] > 0) {

                $sql = "SELECT customer_id FROM ".DB_PREFIX."customer WHERE partner_kod = '".$PartnerKod."'";
                $query_customer = $this->db->query($sql);

                if ($query_customer->row) {
                    $customer_id = $query_customer->row['customer_id'];

                    if ($query->row['fizetesi_mod'] == 'A') {
                        $sql = "SELECT * FROM ".DB_PREFIX."customer_extension WHERE customer_id='".$customer_id."'";
                        $query_customer_extension = $this->db->query($sql);

                        if ($query_customer_extension->num_rows == 0) {
                            $sql = "INSERT INTO ".DB_PREFIX."customer_extension SET
                                customer_id ='".$customer_id."',
                                type        ='payment',
                                code        ='bank_transfer',
                                value       ='1'";
                            $siker = $this->db->query($sql);
                            $vissza = 1;

                        }
                    } else {
                        $sql = "DELETE FROM ".DB_PREFIX."customer_extension WHERE customer_id ='".$customer_id."'";
                        $siker = $this->db->query($sql);
                    }
                }
            }

            $sql = "DELETE FROM ".DB_PREFIX."trodimp_partner WHERE trodimp_partner_id='".$trodimp_partner_id."'";
            $siker = $this->db->query($sql);
        }

        return $vissza;
    }


}
?>