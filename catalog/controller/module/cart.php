<?php 
class ControllerModuleCart extends Controller {
	public function index() {
		$this->language->load('module/cart');

        $this->data['text_success'] = "";
        if (isset($this->request->get['remove'])) {
            $this->cart->remove($this->request->get['remove']);

            unset($this->session->data['vouchers'][$this->request->get['remove']]);
            $this->load->model('catalog/product');
            $result = $this->model_catalog_product->getProduct($this->request->get['remove']);
            $megjelenit_product = $this->config->get('megjelenit_product');
            $nev = "";

            if ($megjelenit_product['product_model_termeknevben']) {
                if (isset($result['model']) && $result['model']) {
                    $nev = $result['model']." - ";
                }
            }

            if ($megjelenit_product['product_cikkszam_termeknevben']) {
                if (isset($result['cikkszam']) && $result['cikkszam']) {
                    $nev .= $result['cikkszam']." - ";
                }
            }

            if ($megjelenit_product['product_gyarto_termeknevben']) {
                if (isset($result['manufacturer']) && $result['manufacturer']) {
                    $nev .= $result['manufacturer']." - ";
                }
            }

            $nev .= $result['name'];
            $this->data['text_success']   = sprintf($this->language->get('text_success'), $nev);


        }

        // Totals
		$this->load->model('setting/extension');
		
		$total_data = array();					
		$total = 0;
		$taxes = $this->cart->getTaxes();
		
		$sort_order = array(); 
		
		$results = $this->model_setting_extension->getExtensions('total');
		
		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}
		
		array_multisort($sort_order, SORT_ASC, $results);
		
		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('total/' . $result['code']);
	
				$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
			}
			
			$sort_order = array(); 
		  
			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);			
		}

		$this->data['totals'] = $total_data;

		$this->data['heading_title'] = $this->language->get('heading_title');

        $mini_cart_price = 0;
        if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
            foreach ($this->session->data['vouchers'] as $voucher) {
                $mini_cart_price += $voucher['amount'];
            }
        }
        $mini_cart_price += $total;
        $darab = $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0);

        if ($this->config->get('megjelenit_mini_cart') == 1 ){
		    $this->data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($mini_cart_price));
        } else{
		    $this->data['text_items'] = "(" . $darab . ")";
        }

        $this->data['text_items_responsive']    = "(" . $darab . ")";
        $this->data['text_empty']               = $this->language->get('text_empty');
		$this->data['text_cart']                = $this->language->get('text_cart');


		$this->data['text_checkout'] = $this->language->get('text_checkout');

		$this->data['button_remove'] = $this->language->get('button_remove');
		
		$this->load->model('tool/image');


		$this->data['products'] = array();

        $ingyenes = false;
        $fizetos = false;

        foreach ($this->cart->getProducts() as $product) {
            $arat_kiiratni = true;
            if ($product['utalvany'] == null || $product['utalvany'] == 0) {
                $fizetos = true;
            } else {
                $ingyenes = true;
                $arat_kiiratni = false;
            }

			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
			} else {
				$image = '';
			}
							
			$option_data = array();
			
			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['option_value'];	
				} else {
					$filename = $this->encryption->decrypt($option['option_value']);
					
					$value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
				}				
				
				$option_data[] = array(								   
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 40 ? utf8_substr($value, 0, 40) . '...' : $value),
					'type'  => $option['type']
				);
			}
			
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                if ($product['utalvany'] != 1) {
				    $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }
			} else {
				$price = false;
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                if ($product['utalvany'] != 1) {
                    $total = $this->currency->format($this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $total = false;
                }
			} else {
				$total = false;
			}

            if ($product['utalvany'] != 1) {
                $mennyiseg = $product['quantity'];
            } else {
                $mennyiseg = '';
            }

            $this->load->model("catalog/manufacturer");
            $manufacturer = $this->model_catalog_manufacturer->getManufacturer($product['manufacturer_id']);
            $manufacturer = isset($manufacturer['name']) && $manufacturer['name'] ? $manufacturer['name'] : "";

            $this->data['products'][] = array(
				'key'               => $product['key'],
				'thumb'             => $image,
				'name'              => $product['name'],
				'model'             => $product['model'],
				'cikkszam'          => $product['cikkszam'],
				'manufacturer'      => $manufacturer,
				'option'            => $option_data,
				'quantity'          => $mennyiseg,
				'price'             => $price,
				'total'             => $total,
                'arat_kiiratni'     => $arat_kiiratni,
				'href'              => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);
		}

		// Gift Voucher
		$this->data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$this->data['vouchers'][] = array(
					'key'         => $key,
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'])
				);
			}
		}
					
		$this->data['cart'] = $this->url->link('checkout/cart');
						
		$this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

        if ($ingyenes && $fizetos) {
            $this->data['text_checkout'] = $this->language->get('text_ingyenes_fizetos');
        } elseif ($ingyenes) {
            $this->data['text_checkout'] = $this->language->get('text_ingyenes');
        }


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/cart.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/cart.tpl';
		} else {
			$this->template = 'default/template/module/cart.tpl';
		}
				
		$this->response->setOutput($this->render());		
	}

	public function getProductQuantity() {

        $product_id = $this->request->post['product_id'];

		$json = array();

        $quantity = $this->cart->countProduct($product_id)+$this->request->post['quantity'];
        $json['price'] = $this->cart->productPrice($product_id,$quantity);



		$this->response->setOutput(json_encode($json));

	}
}
?>