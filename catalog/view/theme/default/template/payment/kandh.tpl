<form action="index.php?route=payment/kandh" method="post">
  		<input type="hidden" name="order_id" value="<?php echo $order_id;?>" />
  		<input type="hidden" name="user_id" value="<?php echo $user_id;?>" />
  		<div class="buttons">
  			<div class="right">
  				<input class="button" type="submit" value="<?php echo $button_confirm; ?>" />
  			</div>
  		</div>
</form>


<div class="kandh-information">
    <?php if($kandh_logo) { ?>
        <div class="kandh-logo" >
            <img style="max-width: 80px" src="<?php echo $kandh_logo; ?>" alt="K&H Bank" title="K&H Bank" />
        </div>
    <?php } ?>
    <?php if(!empty($text_kandh_information)) { ?>
        <div class="kandh-text" >
            <?php echo $text_kandh_information; ?>
        </div>
    <?php } ?>
</div>

<?php if($error){?>
<div class="error"><?php echo $error;?></div>
<?php }?>