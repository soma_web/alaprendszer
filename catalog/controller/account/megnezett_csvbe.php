<?php
class ControllerAccountMegnezettCsvbe extends Controller {
    public function index() {
        $this->load->language('account/megnezett');

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', '', 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('account/megnezett', $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );


        $data = array(
        );


        $this->load->model('account/megnezett');
        $cOrderID = $this->model_account_megnezett->getcustormProduct();

        $product_viewed_total = $this->model_account_megnezett->getTotalProductsViewed($cOrderID);

        $product_views_total = $this->model_account_megnezett->getTotalProductViews($cOrderID);

        $this->data['products'] = array();

        $results = $this->model_account_megnezett->getProductsViewed($data,$cOrderID);

        foreach ($results as $result) {
            if ($result['viewed']) {
                $percent = round($result['viewed'] / $product_views_total * 100, 2);
            } else {
                $percent = 0;
            }

            $this->data['products'][] = array(
                'name'    => $result['name'],
                'model'   => $result['model'],
                'viewed'  => $result['viewed'],
                'percent' => $percent . '%'
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['column_name']      = $this->language->get('column_name');
        $this->data['column_model']     = $this->language->get('column_model');
        $this->data['column_viewed']    = $this->language->get('column_viewed');
        $this->data['column_percent']   = $this->language->get('column_percent');

        $this->data['button_reset']     = $this->language->get('button_reset');
        $this->data['button_nyomtatas'] = $this->language->get('button_nyomtatas');
        $this->data['button_exportalas'] = $this->language->get('button_exportalas');

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['csv_be']  = $this->url->link('account/megnezett_csvbe', $url, 'SSL');

        $this->data['reset'] = $this->url->link('account/megnezett/reset', $url, 'SSL');

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $this->load->model('tool/ExportCSV');
        $header = array(
            'name'                  => $this->data['column_name'],
            'model'                 => $this->data['column_model'],
            'viewed'                => $this->data['column_viewed'],
            'percent'               => $this->data['column_percent']
        );

        if(array_key_exists('products', $this->data)) {
            $this->model_tool_ExportCSV->initialize($header, $this->data['products']);
        } else {
            $this->model_tool_ExportCSV->initialize($header, array());
        }
    }

    public function reset() {
        $this->load->language('account/megnezett');

        $this->load->model('account/megnezett');
        $cOrderID = $this->model_account_megnezett->getcustormProduct();
        $this->model_account_megnezett->reset($cOrderID);

        $this->session->data['success'] = $this->language->get('text_success');

        $this->redirect($this->url->link('account/megnezett','', 'SSL'));
    }

}
?>
