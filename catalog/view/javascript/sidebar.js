/* A sidebar(ok) helyzetét számolja */
/* jobb oldali */
$(document).ready(function(){
    var jobbszuro = "#pageshare";
    var tomb = ["#pageshare","#sidebar-bal-fix"];
    sidebar(jobbszuro,tomb);
    $(window).resize(function(){
        sidebar(jobbszuro,tomb);
    });
});
/* vége */

/* bal oldali */
$(document).ready(function(){
    var balszuro = "#sidebar-bal-fix";
    var tomb = ["#pageshare","#sidebar-bal-fix"];
    sidebar(balszuro,tomb);
    $(window).resize(function(){
        sidebar(balszuro,tomb);
    });
});
/* vége */

function sidebar(elem,tomb){
    if($(elem).length != 0){
        var magassag = $(elem).height();
        if($(elem).css("border-top-width") != 0){
            magassag += parseInt($(elem).css("border-top-width"));
        }
        if($(elem).css("border-bottom-width") != 0){
            magassag += parseInt($(elem).css("border-bottom-width"));
        }
        if($(elem).css("padding-top") != 0){
            magassag += parseInt($(elem).css("padding-top"));
        }
        if($(elem).css("padding-bottom") != 0){
            magassag += parseInt($(elem).css("padding-bottom"));
        }
        var ablakmagas = $(window).height();
        var bodyszeles = $("body").width();
        var kozepeszeles = $(".kozepe").width();

        $(elem).css("top",(ablakmagas/2)-magassag/2);

        if(bodyszeles <= kozepeszeles+(magassag)*2) {
            for(var i = 0; i<tomb.length; i++){
                $(tomb[i]).css("display","none");
            }
        }
        else{
            for(var i = 0; i<tomb.length; i++){
                $(tomb[i]).css("display", "block");
            }
        }
    }
}