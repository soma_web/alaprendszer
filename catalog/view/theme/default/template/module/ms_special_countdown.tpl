<?php if ($products) { ?>
<div class="android-box">
  <div class="row-title no-mb <?php echo $show_class; ?>"><?php echo $heading_title; ?></div>
  <div class="row-content no-bt <?php echo $show_class; ?>">
    <div class="product-list">
      <?php foreach ($products as $product) { ?>
        <div class="borders-inside">

		  <?php if ($product['show_special_countdown']){ ?>
		  <div class="android-counter">	  
			  <div class="as-counter" id="counter_<?php echo $module;?>_<?php echo $product['product_id']; ?>">
				<?php if ($product['days'] > 0) { ?>
					<span class="digit_time"><?php echo $product['days']; ?></span> <span class="time_unit"><?php echo $heading_days; ?></span>  
				<?php } ?>	
				<span class="digit_time"><?php echo $product['hours']; ?></span> <span class="time_unit"><?php echo $heading_hours; ?></span>
			  </div>	
		  </div>	  
		  <?php } ?>

            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
            } else {
                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
            } ?>
            <?php include($arkiir); ?>

		</div>		
      <?php } ?>
	</div>
  </div>
</div>
<? } ?>