<?php
class ControllerModuleSpecial extends Controller {
	protected function index($setting) {
		$this->language->load('module/special');
 
      	$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
        $this->data['kaphato'] = $this->language->get('kaphato');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
		$this->data['button_bovebben'] = $this->language->get('button_bovebben');
        $this->data['text_tax'] = $this->language->get('text_tax');
        $this->data['text_netto'] = $this->language->get('text_netto');
        $this->data['module_name'] = "special";
        $this->data['button_wishlist'] = $this->language->get('button_wishlist');
        $this->data['button_compare'] = $this->language->get('button_compare');


        $this->data['text_akcio_title']         = $this->language->get('text_akcio_title');
        $this->data['text_pardarab_title']      = $this->language->get('text_pardarab_title');
        $this->data['text_uj_title']            = $this->language->get('text_uj_title');

        if ($this->config->get('special_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');

		$this->data['products'] = array();
		
		$data = array(
			'sort'  => 'pd.name',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit'],
            'kiemelt' => isset($setting['kiemelt']) ? $setting['kiemelt'] : 0

		);
        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );
		$results = $this->model_catalog_product->getProductSpecials($data);
        $results1 = $this->model_catalog_product->getProductsNew($data1);

        $this->data['position'] = $setting['position'];
        if ($this->config->get('megjelenit_korhinta') == 1) {
            if (isset($setting['show']) ) {
                $this->data['show'] = $setting['show'];
            } else {
                $this->data['show'] = 0;
            }
        } else {
            $this->data['show'] = 0;
        }

        $this->data['height'] = $setting['image_height']."px";
        $this->data['width'] = "500px";
        foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']);
			} else {
				$image = false;
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
					
			if ((float)$result['special']) { 
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}
            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
            } else {
                $tax = false;
            }
			
			if ($this->config->get('config_review_status')) {
				$rating = $result['rating'];
			} else {
				$rating = false;
			}
            $uj=false;
            if (array_key_exists($result['product_id'],$results1)){
                $uj=1;
            }

            $discount_query = $this->model_catalog_product->getProductDiscounts($result['product_id']);
            $discounts = array();
            $legolcsobb_kulcs=0;
            if($result['price'])  {
                foreach ($discount_query as $discount) {
                    $discounts[] = array(
                        'quantity' => $discount['quantity'],
                        'netto'    => $this->currency->format($discount['price']),
                        'brutto'    => $this->currency->format($this->tax->calculate($discount['price'], $result['tax_class_id'], $this->config->get('config_tax'))),
                        'ara'    => $this->tax->calculate($discount['price'], $result['tax_class_id'], $this->config->get('config_tax'))
                    );
                }
                if (count($discounts) > 0){
                    $legolcsobb=$discounts[0]['ara'];
                    foreach ($discounts as $key => $value) {
                        if ($value['ara'] < $legolcsobb ){
                            $legolcsobb_kulcs=$key;
                            $legolcsobb=$value['ara'];
                        }
                    }
                }
            }
            if (count($discounts) == 0) $discounts[$legolcsobb_kulcs]=false;

            if ($result['quantity'] > 0 && $result['quantity'] <= 4) {
                $pardarab = true;
            } else {
                $pardarab = false;
            }

            $descr= strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'));
            $descr=str_replace("&nbsp;","",$descr);
            $descr=str_replace("\r","",$descr);
            $descr=str_replace("\t","",$descr);
            $descr=str_replace("\n","",$descr);
            $descr=trim($descr);
            $descr=mb_substr($descr, 0, 1000, "UTF-8");

            $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
            $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');

			$this->data['products'][] = array(
				'product_id' => $result['product_id'],
                'szazalek'    => $result['szazalek'],
                'utalvany'    => $result['utalvany'],
                'thumb' => $image ? $image : $this->model_tool_image->resize('no_image.jpg',  $setting['image_width'], $setting['image_height'], false),
				'name'    	 => $result['name'],
                'date_ervenyes_ig' => $result['date_ervenyes_ig'],
				'price'   	 => $price,
                'price_netto' => $this->currency->format($result['price']),
                'special_netto' => $result['special'] > 0 ? $this->currency->format($result['special']) : false,
				'special' 	 => $special,
                'tax'         => $tax,
				'rating'     => $rating,
				'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
				'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                'uj'         => $uj,
                'discounts' => $discounts[$legolcsobb_kulcs],
                'csomagolasi_egyseg' => $result['csomagolasi_egyseg'],
                'megyseg'    => $result['megyseg'],
                'pardarab'	 => $pardarab,
                'description' => $descr,
                'szazalek'    => $result['szazalek'],
                'imagedesabled' => $result['imagedesabled'],
                'utalvany'    => $result['utalvany'],
                'csomagolasi_mennyiseg' => $result['csomagolasi_mennyiseg'] > 1 ? $result['csomagolasi_mennyiseg'] : 0

			);
		}


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/special.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/special.tpl';
		} else {
			$this->template = 'default/template/module/special.tpl';
		}
        if (isset($result) > 0)
            $this->render();
	}
}
?>