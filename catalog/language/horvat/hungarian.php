<?php

// Locale
$_['code']                  = 'hu';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'Y.m.d.';
$_['date_format_long']      = 'Y. F dS, l';
$_['time_format']           = 'h:i:s A';
$_['decimal_point']         = '.';
$_['thousand_point']        = '.';

// Text
$_['text_home']      	    = 'Web trgovina';
$_['text_instock']          = 'Raktáron';
$_['text_yes']              = 'Igen';
$_['text_no']               = 'Nem';
$_['text_none']             = ' --- Nincs --- ';
$_['text_select']           = ' --- Válasszon --- ';
$_['text_all_zones']        = 'Minden zóna';
$_['text_pagination']       = 'Tételek: {start} - {end} / {total} ({pages} oldal)';
$_['text_separator']        = ' &gt; ';
//$_['text_separator']        = ' -> ';
$_['text_shopping_cart']    = 'Kosár';
$_['text_tax']              = 'Netto:';
$_['text_empty']            = 'Ebben a kategóriában nincsenek termékek.';
$_['text_netto']            = 'Nettó';
$_['text_brutto']           = 'Bruttó';
$_['text_cikkszam']         = 'Cikkszám: ';

// Ikonok szöveges (iconok_szoveges.tpl)
$_['text_uj']                = 'Új';
$_['text_akcios']            = 'Akció';
$_['text_pardarab']          = 'Pár darab';
$_['text_mennyisegi']        = 'Mennyiségi kedvezmény';
$_['text_kifuto_title']      = 'Kifutó termék';
$_['text_raktaron']          = 'Raktáron van';
$_['text_nincs_raktaron']    = 'Jelenleg nincs raktáron';


// Ikonok szöveges vége

// Buttons
$_['button_set_valos_ar']   = 'Valós árak';
$_['button_reorder']        = 'Újrarendelés';
$_['button_add_address']    = 'Cím kitöltése';
$_['button_back']           = 'Vissza';
$_['button_continue_fooldal'] = 'Főoldal';
$_['button_continue']       = 'Tovább';
$_['button_cart']           = 'Kosárba';
$_['button_arajanlat']      = 'Kedvező ajánlatot kérek';
$_['button_bovebben']       = 'Megnézem';
$_['button_download']       = 'Letöltés';
$_['button_send']           = 'Küldés';
$_['button_compare']        = 'Összehasonlítás';
$_['button_wishlist']       = 'Kívánságlista';
$_['button_checkout']       = 'Pénztár';
$_['button_confirm']        = 'Rendelés megerősítése';
$_['button_coupon']         = 'Utalvány érvényesítése';
$_['button_delete']         = 'Törlés';
$_['button_edit']           = 'Módosítás';
$_['button_megrendelem']    = 'Megrendelem';
$_['button_new_address']    = 'Új cím';
$_['button_change_address'] = 'Cím módosítása';
$_['button_add_product']    = 'Termék hozzáadása';
$_['button_remove']         = 'Eltávolítás';
$_['button_reviews']        = 'Vélemények';
$_['button_write']          = 'Vélemény megírása';
$_['button_login']          = 'Belépés';
$_['button_update']         = 'Frissítés';
$_['button_shopping']       = 'Tovább nézelődöm';
$_['button_search']         = 'Keresés';
$_['button_shipping']       = 'Kiválaszt';
$_['button_guest']          = 'Regisztráció nélküli vásárlás befejezése';
$_['button_view']           = 'Megnézem';
$_['button_voucher']        = 'Utalvány felhasználása';
$_['button_upload']         = 'Fájl feltöltése';
$_['button_reward']         = 'Pontok beváltása';
$_['button_quote']          = 'Árajánlat kérése';
$_['button_nyomtatas']      = 'Nyomtatás';
$_['button_exportalas']     = 'Exportálás';
$_['button_arajanlat_keszites'] = 'Árajánlat készítés';

$_['kaphato']               = 'Beváltható';
$_['text_eredeti_ar']       = 'Kedvezményes ár';
$_['text_close_upsell']     = 'Bezárás';
$_['osszes_gomb_title']     = 'Összes kiemelt termék';


// Error
$_['error_upload_1']        = 'Figyelem: A feltölteni kívánt fájl mérete meghaladja a php.ini fájlban megadott maximális méretet!';
$_['error_upload_2']        = 'Figyelem: A feltölteni kívánt fájl mérete meghaladja a HTML lap direktívájit!';
$_['error_upload_3']        = 'Figyelem: A feltöltött fájl csak részben lett feltöltve!';
$_['error_upload_4']        = 'Figyelem: Nem lett fájl feltöltve!';
$_['error_upload_6']        = 'Figyelem: Az ideiglenes mappa hiányzik!';
$_['error_upload_7']        = 'Figyelem: Nem lehet a lemezre írni!';
$_['error_upload_8']        = 'Figyelem: A fájlfeltöltés meghiúsult!';
$_['error_upload_999']      = 'Figyelem: Nincs ilyen hibakód!';
?>
