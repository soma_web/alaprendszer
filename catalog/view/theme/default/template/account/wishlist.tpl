<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <h1><?php echo $heading_title; ?></h1>
  <?php if ($products) { ?>
  <div class="wishlist-info">
    <table>
      <thead>
        <tr>
          <td class="image"><?php echo $column_image; ?></td>
          <td class="name"><?php echo $column_name; ?></td>
          <?php if (isset($megjelenit_product['product_cikkszam_termeknevben']) && $megjelenit_product['product_cikkszam_termeknevben']) { ?>
            <td class="model"><?php echo $column_cikkszam; ?></td>
          <?php } else { ?>
            <td class="model"><?php echo $column_model; ?></td>
          <?php } ?>
          <td class="stock"><?php echo $column_stock; ?></td>
          <td class="price"><?php echo $column_price; ?></td>
          <td class="action"><?php echo $column_action; ?></td>
        </tr>
      </thead>
      <?php foreach ($products as $product) { ?>
      <tbody id="wishlist-row<?php echo $product['product_id']; ?>">
        <tr>
          <td class="image"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
            <?php } ?></td>
          <td class="name">
            <?php if ($megjelenit_product['product_gyarto_termeknevben']) { ?>


              <?php if (isset($product['manufacturers']['name']) && $product['manufacturers']['name']) { ?>
                <a href="<?php echo $product['href']; ?>"><?php echo $product['manufacturers']['name']; ?></a>
                <?php echo " - ";?>
              <?php }?>
            <?php }?>

            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
          </td>

          <?php if (isset($megjelenit_product['product_cikkszam_termeknevben']) && $megjelenit_product['product_cikkszam_termeknevben']) { ?>
            <td class="model"><?php echo $product['cikkszam']; ?></td>
          <?php } else { ?>
            <td class="model"><?php echo $product['model']; ?></td>
          <?php } ?>
          <td class="stock"><?php echo $product['stock']; ?></td>
          <td class="price"><?php if ($product['price']) { ?>
            <div class="price">
              <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
              <?php } else { ?>
              <s><?php echo $product['price']; ?></s> <b><?php echo $product['special']; ?></b>
              <?php } ?>
            </div>
            <?php } ?></td>

          <td class="action">
            <?php if(!isset($_SESSION['ar_atszamol']) || (isset($_SESSION['ar_atszamol']) && !$_SESSION['ar_atszamol']) ) { ?>

              <?php if (!empty($product['elorendeles'])) { ?>
                  <input type="button" style="position: relative; top: -4px; " value="<?php echo $button_arajanlat; ?>"
                       onclick="arajanlatotKerek('<?php echo $product['product_id']; ?>')"/>
              <?php } elseif (!empty($product['megrendelem']) ) { ?>
                    <input type="button" value="<?php echo $button_megrendelem; ?>" class="button arajanlat"
                           onclick="megrendelem('<?php echo $product['product_id']; ?>',1*<? echo $product['csomagolasi_mennyiseg']?>,'<?php echo $this->config->get('config_template')?>','<?php echo "kivansaglista".$product['product_id']?>')" />
              <?php } else { ?>
                  <img src="catalog/view/theme/default/image/cart-add.png" alt="<?php echo $button_cart; ?>" title="<?php echo $button_cart; ?>"
                      onclick="addToCart('<?php echo $product['product_id']; ?>',1*<? echo $product['csomagolasi_mennyiseg']?>,'<?php echo $this->config->get('config_template')?>','<?php echo "kivansaglista".$product['product_id']?>')" />
              <?php } ?>
            <?php } else { ?>
              <input style="padding: 3px 7px;min-width: initial;letter-spacing: 0px;font-size: 12px;" type="button" class="kosar button elorendeles" value="<?php echo $button_set_valos_ar; ?>"
                    onclick="ujratolt(0)"/>
            <?php }  ?>
            &nbsp;&nbsp;<a href="<?php echo $product['remove']; ?>"><img src="catalog/view/theme/default/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" />
            </a>
          </td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
  </div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>