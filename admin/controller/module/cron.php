<?php
class ControllerModuleCron extends Controller {

	private $error = array();


    public function index() {
        $this->load->language('module/cron');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cron');

        $this->getList();
    }

    public function insert() {
        $this->load->language('module/cron');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cron');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_module_cron->addCron($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('module/cron', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function update() {
        $this->load->language('module/cron');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cron');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_module_cron->editCron($this->request->get['cron_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('module/cron', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('module/cron');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cron');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $cron_id) {
                $this->model_module_cron->deleteCron($cron_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('module/cron', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    public function getFutottakList() {
        $this->load->language('module/cron');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cron');

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/cron', 'token=' . $this->session->data['token'] , 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('button_futottak'),
            'href'      => $this->url->link('module/cron/getFutottakList', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['cron_list'] = $this->url->link('module/cron', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['crons'] = array();

        //$cron_total = $this->model_module_cron->getTotalRunCrons();
        $results = $this->model_module_cron->getRunCrons();

        foreach ($results as $result) {
            $url = '';
            $url .= '&cron_id=' . $result['cron_id'];

            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_futtatas_eredmeny'),
                'href' => $this->url->link('module/cron/getEredmenyekList', 'token=' . $this->session->data['token'] . $url, 'SSL'),
                'target' => ''
            );



            $this->data['crons'][] = array(
                'program'    => $result['program'],
                'date_start' => $result['date_start'],
                'date_end'   => $result['date_end'],
                'status'     => $result['status'] ? $this->language->get('text_lefutott') : $this->language->get('text_folyamatban'),
                'action'        => $action

            );

        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['column_program'] = $this->language->get('column_program');
        $this->data['column_date_start'] = $this->language->get('column_date_start');
        $this->data['column_date_end'] = $this->language->get('column_date_end');
        $this->data['column_status'] = $this->language->get('column_status');
        $this->data['column_action'] = $this->language->get('column_action');

        $this->data['button_cron_list'] = $this->language->get('button_cron_list');



        $this->template = 'module/cron_run_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());

    }


    public function getEredmenyekList() {
        $this->document->addStyle(HTTP_CATALOG . 'catalog/view/javascript/jquery/colorbox/colorbox.css');
        $this->document->addScript(HTTP_CATALOG . 'catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');

        $this->load->language('module/cron');


        $this->load->model('module/cron');
        $url = '&cron_id='.$this->request->request['cron_id'];

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/cron', 'token=' . $this->session->data['token'] , 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('button_futottak'),
            'href'      => $this->url->link('module/cron/getFutottakList', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_futtatas_eredmenyek'),
            'href'      => $this->url->link('module/cron/getEredmenyekList', 'token=' . $this->session->data['token'].$url, 'SSL'),
            'separator' => ' :: '
        );


        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $this->data['cron_list'] = $this->url->link('module/cron', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cron_futottak'] = $this->url->link('module/cron/getFutottakList', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['result'] = array();

        $data = array(
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );

        $results_total = $this->model_module_cron->getRunResultCronsTotal($this->request->request['cron_id']);
        $results = $this->model_module_cron->getRunResultCrons($this->request->request['cron_id'],$data);
        $aktualis_cron = $this->model_module_cron->getCron($this->request->request['cron_id']);

        foreach ($results as $result) {

            $this->data['results'][] = array(
                'cron_allapot_id' => $result['cron_allapot_id'],
                'date_start' => $result['date_start'],
                'date_end'   => $result['date_end'],
                'eredmeny'   => $result['futtatas_eredmenyek']
            );
        }

        $this->data['heading_title'] = $this->language->get('text_futtatas_eredmenyek').' - '.str_replace('.php','',$aktualis_cron['program']);

        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['column_program']       = $this->language->get('column_program');
        $this->data['column_date_start']    = $this->language->get('column_date_start');
        $this->data['column_date_end']      = $this->language->get('column_date_end');
        $this->data['column_status']        = $this->language->get('column_status');
        $this->data['column_action']        = $this->language->get('column_action');

        $this->data['button_cron_list']         = $this->language->get('button_cron_list');
        $this->data['button_futottak_vissza']   = $this->language->get('button_futottak_vissza');
        $this->data['text_futtatas_eredmenyek'] = $this->language->get('text_futtatas_eredmenyek');
        $this->data['text_close']               = $this->language->get('text_close');
        $this->data['text_enabled']             = $this->language->get('text_enabled');
        $this->data['text_disabled']            = $this->language->get('text_disabled');
        $this->data['close']                    = 'view/image/close_upsell.png';

        $url = '';
        $url .= '&cron_id=' . $this->request->request['cron_id'];

        $pagination = new Pagination();
        $pagination->total = $results_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');

        $pagination->url = $this->url->link('module/cron/getEredmenyekList', 'token=' . $this->session->data['token'] . '&page={page}'.$url, 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->template = 'module/cron_run_result.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());

    }


    private function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'cron_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/cron', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['futottak'] = $this->url->link('module/cron/getFutottakList', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['insert']   = $this->url->link('module/cron/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['delete']   = $this->url->link('module/cron/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['modositas_route']  = "module/cron";
        $this->data['token']            = $this->session->data['token'];

        $this->data['crons'] = array();

        $data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );

        $cron_total = $this->model_module_cron->getTotalCrons();

        $results = $this->model_module_cron->getCrons($data);

        $run_url = HTTP_CATALOG."index.php";
        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => $this->url->link('module/cron/update', 'token=' . $this->session->data['token'] . '&cron_id=' . $result['cron_id'] . $url, 'SSL')
            );

            $action[] = array(
                'text' => $this->language->get('text_futtatas'),
                'href' => $run_url.'?route=cron/'.str_replace('.php','',$result['program']). '&cron_id=' . $result['cron_id'],
                'target' => '_blank'
            );

            $rendszeresseg = '';
            if ($result['rendszeresseg'] == 1) {
                $rendszeresseg = $this->language->get('text_naponta_rendszeresen');
            } elseif ($result['rendszeresseg'] == 2) {
                $rendszeresseg = $this->language->get('text_naponta_idonkent');
            } elseif ($result['rendszeresseg'] == 3) {
                $rendszeresseg = $this->language->get('text_hetente');
            } elseif ($result['rendszeresseg'] == 4) {
                $rendszeresseg = $this->language->get('text_havonta');
            }
            $status         = $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled');

            $this->data['crons'][] = array(
                'cron_id'       => $result['cron_id'],
                'program'       => $result['program'],
                'rendszeresseg' => $rendszeresseg,
                'status'        => $status,
                'status_ertek'  => $result['status'],
                'status_event'  => 'UPDATE '.DB_PREFIX.'cron set status=\'modositas_ertek\' WHERE cron_id='.$result['cron_id'],
                'selected'      => isset($this->request->post['selected']) && in_array($result['cron_id'], $this->request->post['selected']),
                'action'        => $action
            );

        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['column_program'] = $this->language->get('column_program');
        $this->data['column_rendszeresseg'] = $this->language->get('column_rendszeresseg');
        $this->data['column_status'] = $this->language->get('column_status');
        $this->data['column_action'] = $this->language->get('column_action');
        $this->data['column_crond_id'] = $this->language->get('column_crond_id');

        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['button_futottak'] = $this->language->get('button_futottak');
        $this->data['text_cron_inditas'] = $this->language->get('text_cron_inditas');
        $this->data['text_enabled']             = $this->language->get('text_enabled');
        $this->data['text_disabled']            = $this->language->get('text_disabled');
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['sort_cron_id'] = $this->url->link('module/cron', 'token=' . $this->session->data['token'] . '&sort=cron_id' . $url, 'SSL');
        $this->data['sort_program'] = $this->url->link('module/cron', 'token=' . $this->session->data['token'] . '&sort=program' . $url, 'SSL');
        $this->data['sort_status'] = $this->url->link('module/cron', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
        $this->data['sort_rendszeresseg'] = $this->url->link('module/cron', 'token=' . $this->session->data['token'] . '&sort=rendszeresseg' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $cron_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('module/cron', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;

        $this->template = 'module/cron_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function getForm() {


        $this->data['success'] = isset($this->session->data['success']) ? $this->language->get('text_success') : false;

        $this->data['heading_title']            = $this->language->get('heading_title');

        $this->data['entry_program']            = $this->language->get('entry_program');
        $this->data['entry_rendszeresseg']      = $this->language->get('entry_rendszeresseg');
        $this->data['entry_status']             = $this->language->get('entry_status');

        $this->data['text_disabled']             = $this->language->get('text_disabled');
        $this->data['text_enabled']              = $this->language->get('text_enabled');
        $this->data['text_naponta_rendszeresen'] = $this->language->get('text_naponta_rendszeresen');
        $this->data['text_naponta_idonkent']     = $this->language->get('text_naponta_idonkent');
        $this->data['text_hetente']             = $this->language->get('text_hetente');
        $this->data['text_havonta']             = $this->language->get('text_havonta');

        $this->data['button_save']              = $this->language->get('button_save');
        $this->data['button_cancel']            = $this->language->get('button_cancel');
        $this->data['button_remove']            = $this->language->get('button_remove');
        $this->data['button_add_idonkent']      = $this->language->get('button_add_idonkent');
        $this->data['button_add_nap']           = $this->language->get('button_add_nap');



        $this->data['text_timepicker_currentText']      = $this->language->get('text_timepicker_currentText');
        $this->data['text_timepicker_closeText']        = $this->language->get('text_timepicker_closeText');
        $this->data['text_timepicker_timeOnlyTitle']    = $this->language->get('text_timepicker_timeOnlyTitle');
        $this->data['text_timepicker_timeText']         = $this->language->get('text_timepicker_timeText');
        $this->data['text_timepicker_hourText']         = $this->language->get('text_timepicker_hourText');
        $this->data['text_timepicker_minuteText']       = $this->language->get('text_timepicker_minuteText');
        $this->data['text_timepicker_secondText']       = $this->language->get('text_timepicker_secondText');
        $this->data['text_timepicker_timezoneText']     = $this->language->get('text_timepicker_timezoneText');

        $this->data['text_hetfo']       = $this->language->get('text_hetfo');
        $this->data['text_kedd']        = $this->language->get('text_kedd');
        $this->data['text_szerda']      = $this->language->get('text_szerda');
        $this->data['text_csutortok']   = $this->language->get('text_csutortok');
        $this->data['text_pentek']      = $this->language->get('text_pentek');
        $this->data['text_szombat']     = $this->language->get('text_szombat');
        $this->data['text_vasarnap']    = $this->language->get('text_vasarnap');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }


        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/cron', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );



        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (!isset($this->request->get['cron_id'])) {
            $this->data['action'] = $this->url->link('module/cron/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $this->data['action'] = $this->url->link('module/cron/update', 'token=' . $this->session->data['token'] . '&cron_id=' . $this->request->get['cron_id'] . $url, 'SSL');
        }

        $this->data['cancel'] = $this->url->link('module/cron', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['token'] = $this->session->data['token'];


        if (isset($this->request->get['cron_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $cron_info = $this->model_module_cron->getCron($this->request->get['cron_id']);
        }

        if (isset($this->request->post['program'])) {
            $this->data['program'] = $this->request->post['program'];
        } elseif (!empty($cron_info)) {
            $this->data['program'] = $cron_info['program'];
        } else {
            $this->data['program'] = '';
        }

        if (isset($this->request->post['rendszeresseg'])) {
            $this->data['rendszeresseg'] = $this->request->post['rendszeresseg'];
        } elseif (!empty($cron_info)) {
            $this->data['rendszeresseg'] = $cron_info['rendszeresseg'];
        } else {
            $this->data['rendszeresseg'] = 1;
        }

        if (isset($this->request->post['status'])) {
            $this->data['status'] = $this->request->post['status'];
        } elseif (!empty($cron_info)) {
            $this->data['status'] = $cron_info['status'];
        } else {
            $this->data['status'] = '';
        }



        if (!empty($cron_info['input_elements'])) {
            $this->data['seria_input_elements'] = unserialize($cron_info['input_elements']);
        } else {
            $this->data['seria_input_elements'] = array();
        }



        $this->data['naponta_idonkent'] = array();
        $this->data['idonkent'] = '';
        $this->data['hetente_napok'] = array();

        if ($this->data['rendszeresseg'] == 1) {                                            // Naponta rendszeresen
            if (isset($this->request->post['idonkent'])) {
                $this->data['idonkent'] = $this->request->post['idonkent'];
            } elseif (!empty($cron_info)) {
                $this->data['idonkent'] = $cron_info['idonkent'];
            }

        } elseif ($this->data['rendszeresseg'] == 2) {                                      // naponta időpontonként
            if (isset($this->request->post['naponta_idonkent'])) {
                $this->data['naponta_idonkent'] = $this->request->post['naponta_idonkent'];
            } elseif (!empty($cron_info['idopontok'])) {
                $this->data['naponta_idonkent'] = $cron_info['idopontok'];
            }

        } elseif ($this->data['rendszeresseg'] == 3) {                                      // hetente idopontonként
            if (isset($this->request->post['hetente_nap'])) {
                $this->data['hetente_napok'] = $this->request->post['hetente_nap'];
            } elseif (!empty($cron_info['idopontok'])) {
                $this->data['hetente_napok'] = $cron_info['idopontok'];
            }

        } elseif ($this->data['rendszeresseg'] == 4) {                                      // havonta időpontonként

        }

        $this->data['napi_rendszeressegek'] = array(
            '7'     => $this->language->get('text_egyperc'),
            '1'     => $this->language->get('text_otperc'),
            '2'     => $this->language->get('text_tizperc'),
            '3'     => $this->language->get('text_tizenotperc'),
            '4'     => $this->language->get('text_huszperc'),
            '5'     => $this->language->get('text_felora'),
            '6'     => $this->language->get('text_ora'),
        );






        $cron_files = glob('../catalog/controller/cron/*php');

        $this->data['cron_files'] = array();
        if ($cron_files) {
            foreach($cron_files as $cron_file) {
                $this->data['cron_files'][] = basename($cron_file);
            }
        }

        $cron_input_elements = glob('../catalog/controller/cron/cron_input_elements/*.php');

        $this->data['cron_input_elements'] = array();
        if ($cron_input_elements) {
            foreach($cron_input_elements as $beallitas) {
                $_=array();
                require($beallitas);
                $this->data['cron_input_elements'][basename($beallitas)] = $_;
            }
        }



        /* ----------- Könyvtárasitáshoz folytatni ------*/
        /*$dirs = array_filter(glob('../catalog/controller/cron/*'), 'is_dir');
        array_unshift($dirs,'../catalog/controller/cron');

        foreach($dirs as $dir_key=>$dir) {

            $files = glob($dir.'/*.php');
            if (empty($files)) continue;

            $datas=array();

            foreach ($files as $file) {
                $extension = basename($file, '.php');
                $datas[$extension] = array();
            }
        }*/




        $this->template = 'module/cron_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
				
		$this->response->setOutput($this->render());
	}


    private function validateForm() {
        if (!$this->user->hasPermission('modify', 'module/cron')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
	}

    private function validateDelete() {
        if (!$this->user->hasPermission('modify', 'module/cron')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function modosit() {
        $json = array();

        if (!$this->user->hasPermission('modify', 'module/cron')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if (empty($json['error']) && !empty($_POST['eljaras'])) {
            $sql = str_replace('modositas_ertek',$_POST['ertek'],$_POST['eljaras']);
            $siker = $this->db->query($sql);
            if ($siker) {
                $json['success'] =  $this->language->get('success_modositas');
            } else {
                $json['error'] = $this->language->get('error_sql');

            }
        } elseif (empty($json['error'])) {
            $json['error'] = $this->language->get('error_post');

        }

        $this->response->setOutput(json_encode($json));

    }
}
?>