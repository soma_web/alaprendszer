<?php if ($product['text_garancia']) { ?>

    <?php if ($product['garancia_description']) { ?>
        <div class="garancia" style="cursor: pointer">
            <a class="various" href="#garancia_list<?php echo $product['product_id']?>" >
                <span><?php echo $product['text_garancia'] ?></span>
            </a>
        </div>

        <div id="garancia_list<?php echo $product['product_id']?>" class=""
            style="display: none; min-width: 320; min-height: 150px; background-color: white;">
            <?php echo $product['garancia_description']?>
        </div>

    <?php } else { ?>
        <div class="garancia" >
            <a style="cursor: default">
                <span><?php echo $product['text_garancia'] ?></span>
            </a>
        </div>
    <?php } ?>

<?php } ?>