<?php
// Text
$_['text_pcs']       = 'stock';
$_['text_out_of_stock'] = '<span style="color:#999;">out of stock</span>';

$_['text_facebook_comment'] = "Facebook Comments";
$_['text_search']       = 'Search';
$_['text_brand']        = 'Brand';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Product Code:';
$_['text_reward']       = 'Reward Points:'; 
$_['text_points']       = 'Price in reward points:';
$_['text_stock']        = 'Availability:';
$_['text_instock']      = 'In Stock';
$_['text_price']        = 'Price:'; 
$_['text_tax']          = 'Ex Tax:';
$_['text_discount']     = '%s or more : <b>%s</b>';
$_['text_option']       = 'Available Options';
$_['text_qty']          = 'Qty:';
$_['text_minimum']      = 'This product has a minimum quantity of %s';
$_['text_or']           = '- OR -';
$_['text_reviews']      = '%s reviews'; 
$_['text_write']        = 'Write a review';
$_['text_no_reviews']   = 'There are no reviews for this product.';
$_['text_note']         = '<span style="color: #FF0000;">Note:</span> HTML is not translated!';
$_['text_share']        = 'Share';
$_['text_success']      = 'Thank you for your review. It has been submitted to the webmaster for approval.';
$_['text_upload']       = 'Your file was successfully uploaded!';
$_['text_wait']         = 'Please Wait!';
$_['text_tags']         = 'Tags:';
$_['text_error']        = 'Product not found!';
$_['text_tax_brutto']   = 'Brutto:';
$_['text_temkekdij']    = 'Prices fee is included in the product!';
$_['text_utalvany_erteke']  = 'The voucher value:';
$_['text_meret']        = 'Size (Length × Width × Height):';
$_['text_lizing']             = 'Leasing options';
$_['text_stock_in']     = 'Estimated arrival:';
$_['text_total']        = 'Total:';
$_['text_login']        = 'Prices to view or purchase log in or register';
$_['text_netto_ar']             = 'Net Price';


$_['text_raktar']       = 'Per-store inventory:';
$_['text_garancia']= 'warranty';
$_['text_szallitas']= 'Shipping cost:';
$_['entry_garancia_honap']      = 'months';
$_['entry_garancia_ev']         = 'year';


$_['text_seat']         = 'Seated:';
$_['text_matchdate']    = 'Date:';
$_['text_message']      = 'The issue we will successfully!<br /><br /><br />';
$_['heading_title']     = 'Ask a question about this product';

$_['text_bejelentkezes'] = 'Personalized pricing please';
$_['text_bejelentkezes_button']= 'login';
$_['text_megyseg']= 'unit of Measure';
$_['text_suly']= 'weight of';


$_['entry_email']       = 'E-Mail';
$_['entry_enquiry']     = 'Frage';

// Ikonok szöveges (iconok_szoveges.tpl)
$_['text_uj']                = 'New';
$_['text_ujdonsag_title']    = 'Newness';
$_['text_akcios']            = 'On Sale';
$_['text_pardarab']          = 'Few Pieces';
$_['text_mennyisegi']        = 'Volume Discount';
// Ikonok szöveges vége

$_['informaciok1']           = 'Credit facilities';
$_['informaciok2']           = 'Package deals';
$_['informaciok3']           = 'Delivery receipt';
$_['informaciok4']           = 'Shipping info, prices';
$_['informaciok5']           = 'Payment methods';

// Entry
$_['entry_name']        = 'Your Name:';
$_['entry_review']      = 'Your Review:';
$_['entry_rating']      = 'Rating:';
$_['entry_good']        = 'Good';
$_['entry_bad']         = 'Bad';
$_['entry_captcha']     = 'Enter the code in the box below:';
$_['entry_garancia_honap']      = 'months';
$_['entry_garancia_ev']         = 'year';

// Tabs
$_['tab_description']       = 'Description';
$_['tab_attribute']         = 'Specification';
$_['tab_review']            = 'Reviews (%s)';
$_['tab_related']           = 'Related Products';
$_['tab_image']             = 'Images';
$_['tab_meret']             = 'Product dimension';
$_['tab_test_description']  = 'Tests';
$_['tab_ajanlott']          = 'Recommended accessories';
$_['tab_helyettesito']      = 'Substitute Products';
$_['tab_kereso']            = 'Accessory finder';
$_['tab_ajandek']           = 'Largesse';
$_['tab_downloads']        = 'Downloads';

$_['text_akcio_title']        = 'Special product';
$_['text_pardarab_title']     = 'Few pieces';
$_['text_uj_title']           = 'New product';

// Error
$_['error_name']        = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']        = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']      = 'Warning: Please select a review rating!';
$_['error_captcha']     = 'Warning: Verification code does not match the image!';
$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';


$_['text_ask']              = 'Ask a question';
$_['text_question']         = 'Q:';
$_['text_answer']           = 'A:';
$_['text_no_questions']     = 'No questions have been posted about this product.';
$_['text_no_answer']        = 'Not answered yet.';
$_['text_success_question'] = 'Thank you for your question. It has been submitted to the webmaster for answering.';

$_['entry_question']        = 'Your Question:';
$_['entry_email']           = 'Email:';
$_['entry_phone']           = 'Phone:';

$_['tab_qa']                = 'Q &amp; A';

$_['error_q_author']        = '<strong>Error!</strong> Please enter your name!';
$_['error_q_question']      = '<strong>Error!</strong> Question Text must be between 15 and 1000 characters!';
$_['error_q_email']         = '<strong>Error!</strong> Please enter a valid email address!';
$_['error_q_phone']         = '<strong>Error!</strong> Please enter your phone number!';
$_['error_q_custom']        = '<strong>Error!</strong> %s is required!';
$_['error_q_captcha']       = '<strong>Error!</strong> Verification code does not match the image!';


?>