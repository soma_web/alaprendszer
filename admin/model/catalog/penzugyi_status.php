<?php
class ModelCatalogPenzugyiStatus extends Model {

    public function getPenzugyiStatus() {

        $vissza = array();
        $elhelyezkedesek = $this->config->get("penzugyi_status");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['status'] == 1) {
                    $vissza[] = $value;
                }
            }
        }

        return $vissza;
    }

}
?>